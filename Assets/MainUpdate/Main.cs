using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public delegate void ActionChat(string str);

public delegate void ActionPaint(mGraphics g, int x, int y);

public delegate void ActionUpdate();

public class Main : MonoBehaviour
{
    public static Main main;
    //public static GameCanvas canvas;
    public static mGraphics g;
    public static GameMidlet tMidlet;
    public static string res = "res";
    public static string mainThreadName;
    public static bool started = false, isIpod, isIphone4, isIpad, isExit;	//isExit dung cho windows

    // hai cho quan trong can luu y khi build ban client moi -----------------------------
    public static bool isPC, isWindowsPhone, isIPhone, isSprite, isGdx;
    public static bool IphoneVersionApp;// neu la ban ip cho appstore>> true: nguoc lai la bang false;
    // hai cho quan trong can luu y khi build ban client moi -----------------------------

    public static string IMEI;
    public static int versionIp = 0;//cong len moi khi thay doi
    //------doi icon chuot
    //public static Image cursorImage;
    //--
    // private int cursorWidth=24;
    //  private int cursorHeight=24;    
    //-- doi icon chuot
    // // JAVA=0//ANDROID =1, IPHONE=2, IPHONE APP=3, PC=4,WINDOWS =5;
    public static int numberQuit = 1;
    public static sbyte IPHONE_JB = 2;//Iphone  
    public const sbyte IP_APPSTORE = 3;
    public const sbyte PC_VERSION = 4;
    public const sbyte WINDOWSPHONE = 5;
    public static int level;
    void Start()
    {
        if (!started)
        {

            if (Thread.CurrentThread.Name != "Main")
                Thread.CurrentThread.Name = "Main";
            mainThreadName = Thread.CurrentThread.Name;
            isPC = true;//nếu build cho Iphone thì đóng chỗ này lại-----------------
            started = true;
            level = Rms.loadRMSInt("levelScreenKN");

            ScaleGUI.initScaleGUI();
            g = new mGraphics();
            init();
            //Screen.SetResolution(853, 480, false); //enter res
            //if (level == 1)
            //{
            //    Screen.SetResolution(480, 320, false);
            //}
            //else
            //    Screen.SetResolution(1024, 600, false);
           
        }
    }
    public static int sizeMiniMap = -1;
    public static bool isLoad;
    public void creatMiniMap() { }

    public void init()//ham khoi tao gia tri 
    {
        Cout.println("init Mainnnn");
        tMidlet = new GameMidlet();
       // CRes.init();
    }
    int cout;
    void OnGUI()
    {
        if (cout < 10)
            return;
        checkInput();
        Session_ME.update();
        if (Event.current.type.Equals(EventType.Repaint))
        {

            GameMidlet.gameCanvas.paint(g);
            g.reset();

            // g.enClip();
            //if (isPC && !Char.ischangingMap)        
            // GUI.DrawTexture(new Rect(Input.mousePosition.x, Screen.height - Input.mousePosition.y, 
            //cursorWidth, cursorHeight), cursorImage.texture);        
        }
    }
    public static void closeKeyBoard()
    {
        if (TouchScreenKeyboard.visible)
        {
            TField.kb.active = false;
            TField.kb = null;
        }
    }

    public void doClearRMS()
    {
        if (!isPC) return;
        int z = Rms.loadRMSInt("lastZoomlevel");
        if (z != mGraphics.zoomLevel)
        {
            Rms.deleteAllRecord();
            Rms.saveRMSInt("lastZoomlevel", mGraphics.zoomLevel);
            Rms.saveRMSInt("levelScreenKN", level);
        }
    }
    bool isRun;
    void FixedUpdate()
    {
        Rms.update();
        cout++;
        if (cout < 10)
            return;
        if (!isRun)
        {
            isRun = true;
            Screen.orientation = ScreenOrientation.Landscape;
            Application.runInBackground = true;
            Application.targetFrameRate = 30;
            useGUILayout = false;
            isCompactDevice = detectCompactDevice();
            if (main == null)
                main = this;


            IMEI = SystemInfo.deviceUniqueIdentifier;
            //------------------------------------------------------

            //     	   if(isPC)
            //				TField.isQwerty=true;
            //			else 
            //				TField.isQwerty=false;
            isIPhone = isPC == true ? false : true;

            //IphoneVersionApp = true;// nếu build cho bản iphone appstore thì mở dòng này ra..
            //-------------------------------------------------------
            if (iPhoneSettings.generation == iPhoneGeneration.iPadUnknown)
            {
                isIpad = true;

            }
            int typeClient = IPHONE_JB;
            if (isPC)
                Screen.fullScreen = false;

            //-------------------------------------------------------
            if (isWindowsPhone)
            {
                typeClient = WINDOWSPHONE;//dùng cho bản windowsphone....
            }
            if (isPC)
                typeClient = PC_VERSION;
            if (IphoneVersionApp)
                typeClient = IP_APPSTORE;
            //TemMidlet.DIVICE = (sbyte)typeClient;
            //Debug.Log(" thong tin :" + TemMidlet.DIVICE);

            //-------------------------------------------------------
            if (iPhoneSettings.generation == iPhoneGeneration.iPodTouch4Gen)
                isIpod = true;
            if (iPhoneSettings.generation == iPhoneGeneration.iPhone4)
                isIphone4 = true;

            g.CreateLineMaterial();
            //tMidlet = new GameMidlet();
            if (mGraphics.zoomLevel == 1 && !isWindowsPhone)
            {
                isSprite = true;
            }
            if (isPC)
            {

            }
        }
        ipKeyboard.update();
       
         GameMidlet.gameCanvas.update();
        Image.update();
        DataInputStream.update();
        SMS.update();
        Net.update();
        //if (GameCanvas.saveImage != null)
        //    GameCanvas.saveImage.run();
        //if (CRes.load != null)
        //    CRes.load.run();
        //if (!Main.isPC)
        //{
        //    int c = 1 / a;
        //}
    }

    void Update() { }
    //--------------------------------------------------
    static Material lineMaterial;
    static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            lineMaterial = new Material("Shader \"Lines/Colored Blended\" {" +
                                        "SubShader { Pass { " +
                                        "    Blend SrcAlpha OneMinusSrcAlpha " +
                                        "    ZWrite Off Cull Off Fog { Mode Off } " +
                                        "    BindChannels {" +
                                        "      Bind \"vertex\", vertex Bind \"color\", color }" +
                                        "} } }");

            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
        }
    }
    //----------------------------------------------------------
    private void checkInput()
    {

        if (Input.GetMouseButtonDown(0))
        {
            //   isRightMouseClick = Input.GetMouseButtonDown(0)?false:true;           
            Vector3 touch = Input.mousePosition;

            GameMidlet.gameCanvas.pointerPressed((int)(touch.x), (int)((UnityEngine.Screen.height - touch.y)) + mGraphics.addYWhenOpenKeyBoard * mGraphics.zoomLevel);
            lastMousePos.x = (touch.x);
            lastMousePos.y = (touch.y) + mGraphics.addYWhenOpenKeyBoard * mGraphics.zoomLevel;

        }
        if (Input.GetMouseButton(0))
        {
            //    isRightMouseClick = Input.GetMouseButton(0) ? false : true;    
            Vector3 touch = Input.mousePosition;

            GameMidlet.gameCanvas.pointerDragged((int)(touch.x), (int)((UnityEngine.Screen.height - touch.y)) + mGraphics.addYWhenOpenKeyBoard * mGraphics.zoomLevel);
            lastMousePos.x = (touch.x);
            lastMousePos.y = (touch.y) + mGraphics.addYWhenOpenKeyBoard * mGraphics.zoomLevel;
        }
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 touch = Input.mousePosition;
            lastMousePos.x = touch.x;
            lastMousePos.y = (touch.y) + mGraphics.addYWhenOpenKeyBoard * mGraphics.zoomLevel;

            GameMidlet.gameCanvas.pointerReleased((int)(touch.x), (int)((UnityEngine.Screen.height - touch.y)) + mGraphics.addYWhenOpenKeyBoard * mGraphics.zoomLevel);

        }
        //	Cout.LogError2 (" totot : "+mGraphics.addYWhenOpenKeyBoard);


        if (Input.anyKeyDown && (Event.current.type == EventType.KeyDown))
        {
            int keyCode = MyKeyMap.map(Event.current.keyCode);

            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                switch (Event.current.keyCode)
                {
                    case KeyCode.Alpha2: keyCode = 64; break;
                    case KeyCode.Minus: keyCode = 95; break;

                }
            }

            if (keyCode != 0)

                GameMidlet.gameCanvas.keyPressedd(keyCode);

        }
        if (Event.current.type == EventType.KeyUp)
        {
            int keyCode = MyKeyMap.map(Event.current.keyCode);
            if (keyCode != 0)


                GameMidlet.gameCanvas.keyReleasedd(keyCode);
        }
        //if(Main.isPC){
        //  GameMidlet.gameCanvas.scrollMouse((int)(Input.GetAxis("Mouse ScrollWheel")*10));      
        //  var xxx = Input.mousePosition.x;
        //  var yyy = Input.mousePosition.y;
        //  int mX = (int)xxx/mGraphics.zoomLevel;
        //  int mY = (Screen.height-(int)yyy)/mGraphics.zoomLevel;
        //  GameMidlet.gameCanvas.pointerMouse(mX,mY);
        //}        
    }

    void OnApplicationQuit()
    {
        Debug.LogWarning("APP QUIT");

        Session_ME.gI().close();
        if (isPC)
            Application.Quit();
    }
    public static Image[] imgTileMapLogin;
    public static bool isMiniApp = true, isQuitApp, isReSume;
    void OnApplicationPause(bool paused)
    {
        //---paused=true===== thoat game
        //======paused=false: vao lai
        isReSume = false;
        if (paused)
        {
            if (GameCanvas.currentDialog != null)
            {
                //if (GameCanvas.currentDialomGraphics.LEFT != null)
                //{
                //   // GameCanvas.currentDialomGraphics.LEFT.perform();
                //}
            }
        }
        else
        {
            isReSume = true;
        }
        //if (GameCanvas.isWaiting())
        //{
        //    isQuitApp = true;
        //}

        if (TouchScreenKeyboard.visible)
        {
            TField.kb.active = false;
            TField.kb = null;
        }

        if (isQuitApp)
            Application.Quit();
    }
    //    isMiniApp = paused;

    //    //if (paused)
    //    //{ // thu nho xuong

    //    //    Cout.println("thu nho lai");
    //    //}
    //    //else
    //    //{    // bat lai

    //    //    if (!isPC && GameCanvas.loginScr!=null)
    //    //    {
    //    //        if (GameCanvas.currentScreen == GameCanvas.loginScr)
    //    //        {
    //    //            GameCanvas.loginScr.doLogin();
    //    //        }
    //    //    }
    //    //}
    //}
    Vector2 lastMousePos = new Vector2();
    public static int a = 1;
    public static void exit()
    {
        if (isPC)
            main.OnApplicationQuit();
        else
        {
            a = 0;
        }
    }
    public static bool isCompactDevice = true;
    public static bool detectCompactDevice()
    {
        if (iPhoneSettings.generation == iPhoneGeneration.iPhone || iPhoneSettings.generation == iPhoneGeneration.iPhone3G ||
           iPhoneSettings.generation == iPhoneGeneration.iPodTouch1Gen || iPhoneSettings.generation == iPhoneGeneration.iPodTouch2Gen)
        {
            return false;
        }
        else
            return true;
    }

    public static bool checkCanSendSMS()
    {
        if (iPhoneSettings.generation == iPhoneGeneration.iPhone3GS || iPhoneSettings.generation == iPhoneGeneration.iPhone4
           || iPhoneSettings.generation > iPhoneGeneration.iPodTouch4Gen)
        {
            return true;
        }
        else
            return false;
    }
    public void platformRequest(string url)
    {
        Cout.LogWarning("PLATFORM REQUEST: " + url);
        Application.OpenURL(url);
    }


}
