

using System;
using System.IO;
public class Service {

	ISession session = Session_ME.gI();
	protected static Service instance;

	public static Service gI() {
		if (instance == null)
			instance = new Service();
		return instance;
	}
	
	public void doLogin(string user, string pass,byte zoomlevel){ // login
		Message m;
		try {

            Cout.println("doLogin " + user+"  pass  "+pass);
		//	Cout.println(getClass(), user+" user "+pass);
			m = new Message(Cmd.LOGIN);
			m.writer().writeUTF(user);
			m.writer().writeUTF(pass);
			m.writer().writeByte(zoomlevel);
			session.sendMessage(m);
			m.cleanup();

		} catch (IOException e) {
			//e.printStackTrace();
		}
		
	}
	
	public void createChar(sbyte typeChar, sbyte country , sbyte gender, string charName){ // tạo char
		Message m;
		try{
			Cout.println(charName+" CreateChar "+country+"  typechar "+typeChar+" gen "+gender);
			m = new Message(Cmd.CREATE_CHAR);
			m.writer().writeByte(country);
			m.writer().writeByte(typeChar);
			m.writer().writeByte(gender);
			m.writer().writeUTF(charName);
			session.sendMessage(m);
			m.cleanup();
		}catch (Exception e) {
			//e.printStackTrace();
		}
		
	}
	
	public void selectChar(int charIDDB){ // select char to play
		Message m;
		try{
			m = new Message(Cmd.SELECT_CHAR);
			m.writer().writeInt(charIDDB);
			session.sendMessage(m);
			m.cleanup();
			
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void charMove() { // gui char di chuyen
        int dx = Char.myChar().cx - Char.myChar().cxSend;
        int dy = Char.myChar().cy - Char.myChar().cySend;
        bool isGuilen = (CRes.abs(dx) > 30 || CRes.abs(dy) > 30) ? true : false;
        if (Char.ischangingMap || (dx == 0 && dy == 0) || (!isGuilen && Char.myChar().statusMe != Char.A_STAND && Char.myChar().statusMe != Char.A_DEAD && Char.myChar().statusMe != Char.A_DEADFLY))
        {
            return;
        }
        try
        {
            if (GameScr.isSendMove)
                GameScr.isSendMove = false;
            Message m = new Message(Cmd.PLAYER_MOVE);
            Char.myChar().cxSend = Char.myChar().cx;
            Char.myChar().cySend = Char.myChar().cy;
            Char.myChar().cdirSend = Char.myChar().cdir;
            Char.myChar().cactFirst = Char.myChar().statusMe;
            m.writer().writeShort(Char.myChar().cx);
            //			if (dy != 0)
            m.writer().writeShort(Char.myChar().cy);
            session.sendMessage(m);
            m.cleanup();
        }
        catch (Exception ex)
        {
            //ex.printStackTrace();
        }
    }
    public void requestImage(int id, sbyte sub)
    { // yeu cau hinh tu server
        Message m = null;
        try
        {
            //Cout.println("requestImage --> " + id + " subbbb  " + sub);
            m = new Message(Cmd.REQUEST_IMAGE);
            m.writer().writeByte(sub);// o lÃ  normal 1 la quai.
            m.writer().writeInt(id);
            session.sendMessage(m);
        }
        catch (Exception e)
        {
           // e.printStackTrace();
        }
        finally
        {
            m.cleanup();
        }
    }
	
	
	public void requestChangeMap() { // server tự biết nó đứng đâu mà chuyển đến
		// map nào
		try{
			Message m = new Message(Cmd.CHANGE_MAP);
			session.sendMessage(m);
			m.cleanup();
		}catch (Exception e) {
			//e.printStackTrace();
			// TODO: handle exception
		}
	}
	
	public void requestinventory(){ // yeu cau request Inventory
		try{
			//System.out.println("REQUEST ITEM INVENTORY");
			Message m = new Message(Cmd.GET_ITEM_INVENTORY);
			session.sendMessage(m);
			m.cleanup();
		}catch(Exception e){
			//e.printStackTrace();
		}
	}
	
	public void requestIcon(int id){ // request hinh nho
		Message m = null;
		try {
			//System.out.println("idicon --> "+id);
			m = new Message(Cmd.REQUEST_IMAGE);
			m.writer().writeInt(id);
			session.sendMessage(m);
		} catch (Exception e) {
		////	e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void chat(string text, sbyte type) { // yeu cau chat trong map
		Message m = null;
		try {
			m = new Message(Cmd.CHAT);
			m.writer().writeByte(type);
			m.writer().writeUTF(text);
			session.sendMessage(m);
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			m.cleanup();
		}
	}
	
	public void chatGlobal(string text, sbyte type) { // yeu cau chat kenh the gioi
		Message m = null;
		try {
			m = new Message(Cmd.CHAT);
			m.writer().writeByte(type);
			m.writer().writeUTF(text);
			session.sendMessage(m);
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			m.cleanup();
		}		
		
	}
	
	public void requetsInfoMod(short idMod){ // yeu cau thong tin quai khi focus
		Message m = null;
		try{
			m = new Message(Cmd.REQUEST_MONSTER_INFO);
			m.writer().writeShort(idMod);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		} finally {
			m.cleanup();
		}
	}

    public void sendPlayerAttack(mVector vMob, mVector vChar, int type, int idskill, bool isMob)
    {
        try
        {
            // 0 player
            // 1 monter
            //			Cout.println("attack  "+type);
            Message m = null;
            GameScr.isSendMove = true;
            m = new Message(Cmd.ATTACK);
            m.writer().writeByte((byte)type);
            //			Cout.println("sendPlayerAttack --->type "+type);
            m.writer().writeByte((byte)idskill);// type tan cong 0//chÃ¢//quai 0
            //			Cout.println("sendPlayerAttack --->idskill "+idskill);
            m.writer().writeByte((byte)vMob.size());//ntarget
            //			Cout.println("sendPlayerAttack --->vMob.size() "+vMob.size());
            //			m.writer().writeByte(1);
            //			if (vMob.size() > 0) {
            //				if(Char.myChar().mobFocus != null){
            //					if(Char.myChar().mobFocus != null){
            //						m.writer().writeShort(Char.myChar().mobFocus.mobId);
            //						
            //					}
            //				}

            //		} 

            if (isMob)
            {
                for (int i = 0; i < vMob.size(); i++)
                {
                    Mob b = (Mob)vMob.elementAt(i);
                    if (b != null)
                    {
                        m.writer().writeShort(b.mobId);
                    }
                }
            }
            else
            {
                for (int i = 0; i < vChar.size(); i++)
                {
                    Char b = (Char)vChar.elementAt(i);
                    if (b != null)
                    {
                        m.writer().writeShort(b.charID);
                    }
                }
            }

            //				Cout.println("sendPlayerAttack --mob-> ");
            session.sendMessage(m);

        }
        catch (Exception e)
        {
        }
    }
	public void itemPick(sbyte Type, short idItem){ // yêu cầu nhặt item
		Message m = null;
		try {
			m = new Message(Cmd.PICK_REMOVE_ITEM);
			m.writer().writeByte(Type);
			m.writer().writeShort(idItem);
			//System.out.println("idItem ---> "+idItem);
			session.sendMessage(m);
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			m.cleanup();
		}
	}
	
	public void requestNPC(short IdNpc){
		Message m = null;
		try{
			//System.out.println("REQUEST CHAR INFO");
			m = new Message(Cmd.NPC_REQUEST);
			m.writer().writeShort(IdNpc);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		} finally{
			m.cleanup();
		}
	}
	
	public void giveUpItem(sbyte index){
		Message m = null;
		try{
			//System.out.println("VUC BO ITEM");
			m = new Message(Cmd.GIVEUP_ITEM);
			m.writer().writeByte(index);
			session.sendMessage(m);
		}catch (Exception e) {
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void useItem(sbyte index, Item type){
		//System.out.println("USE ITEM -------> ");
		Message m = null;
		try{
			m = new Message(Cmd.ITEM);
			m.writer().writeByte(type.typeUI);
			m.writer().writeByte(index);
//			for(int i = index; i < Char.myChar().arrItemBag.Length; i++)
//			type = Char.myChar().arrItemBag[index];
			session.sendMessage(m);
		}catch (Exception e) {
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void inviteParty(short charID, sbyte type){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(charID);
			session.sendMessage(m);
		}catch (Exception e) {
			//e.printStackTrace();
			// TODO: handle exception
		}finally{
			m.cleanup();
		}
	}
	
	public void AcceptParty(sbyte type, short charID){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(charID);
			session.sendMessage(m);
		}catch(Exception e){
		////	e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void leaveParty(sbyte type, short charID){
		//System.out.println("lave ");
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(charID);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void kickPlayeleaveParty(sbyte type, short charID){
		//System.out.println("kick ---- >");
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(charID);
			session.sendMessage(m);
		}catch (Exception e){
		////	e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void removeParty(sbyte type){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(Char.myChar().charID);
			session.sendMessage(m);
		}catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void requestjoinParty(sbyte type, short CharID){ // yeu cau vao party
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
			
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestinfoPartynearME(sbyte type, short CharID){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestAddfriend(sbyte type, short CharID){
		//System.out.println("Request add friend");
		Message m = null;
		try{
			m = new Message(Cmd.FRIEND);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void AcceptFriend(sbyte type, short CharID){
		Message m = null;
		try{
			m = new Message(Cmd.FRIEND);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestFriendList(sbyte type, short CharID){
		Message m = null;
		try{
			m = new Message(Cmd.FRIEND);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void deleteFriend(sbyte type,short myCharID ,short CharID){ // xóa bạn 
		Message m = null;
		try{
			m = new Message(Cmd.FRIEND);
			m.writer().writeByte(type);
			m.writer().writeShort(myCharID);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void chatPrivate(sbyte type, short userid, string contend){
		Message m = null;
		try{
		//	Cout.println(getClass(), " gui chat chatPrivate "+contend);
			m = new Message(Cmd.CHAT);
			m.writer().writeByte(type);
			m.writer().writeShort(userid);
			m.writer().writeUTF(contend);
			session.sendMessage(m);
		}catch (Exception e) {
			//e.printStackTrace();
			// TODO: handle exception
		}finally{
			m.cleanup();
		}
	}
	
	public void inviteTrade(sbyte typeTrade, short CharID){ // mời trao đổi
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void acceptTrade(sbyte typeTrade, short CharID){ // đồng ý trao đổi 
		
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void moveItemTrade(sbyte typeTrade, short charID, sbyte typeMoveItem, short Iditem){ // chuyển item trao đổi
		//System.out.println("SendItem move ----> "+typeTrade+", "+charID+", "+typeMoveItem+", "+Iditem);
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(charID);
			m.writer().writeByte(typeMoveItem);
			m.writer().writeShort(Iditem);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void cancelTrade(sbyte typeTrade, short CharID){ // hủy trao đổi 
		Cout.println(" cancelTradeTradeeeeeeeeeeeeeeeeeeeee");
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(Char.myChar().partnerTrade.charID);
			session.sendMessage(m);
			
		}catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void comfirmTrade(sbyte typeTrade, short CharID){ // lock
		//System.out.println("Comfirm Trade");
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
			
		}catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void doTrade(sbyte typeTrade, short CharID){ // giao dịch 
		//System.out.println("TRADE TRADE TRADE");
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestShop(int idMenuShop){ // thong tin shop
		Message m = null;
		try{
		//	Cout.println(getClass(), "sendquest shop "+idMenuShop);
			m = new Message(Cmd.REQUEST_SHOP);
			m.writer().writeByte(idMenuShop);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void BuyItemShop(sbyte idmenu,short iditem){ // thong tin shop
		Message m = null;
		try{
		//	Cout.println(getClass(),idmenu+ "  BuyItemShop "+iditem);
			m = new Message(Cmd.BUY_ITEM_FROM_SHOP);
			m.writer().writeByte(idmenu);
			m.writer().writeShort(iditem);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void requestMenuShop(){
		Message m = null;
		try{
			m = new Message(Cmd.CMD_DYNAMIC_MENU);
			session.sendMessage(m);
		}catch(Exception e){
			//e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}

	public void Quest(sbyte type, short questId, sbyte mainSub)
	{
		Message m= new Message(Cmd.QUEST);		
		try {
		//	Cout.println(getClass(), "type "+type +"  questId  "+questId+"  mainSub  "+mainSub);
			m.writer().writeByte(type);//0 nhan // 1 tra// 2 huy
			m.writer().writeByte(questId);//
			m.writer().writeByte(mainSub);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
    public void PlayerOut()
    {
        Message m = new Message(Cmd.PLAYER_REMOVE);
        try
        {
            session.sendMessage(m);
            m.cleanup();
        }
        catch (Exception e)
        {
        }
    }
    public void HocSkill(sbyte type, int idskill)
    {
        Message m = new Message(Cmd.CHAR_SKILL_STUDIED);
        try
        {
            m.writer().writeByte(type);
            m.writer().writeInt(idskill);
            session.sendMessage(m);
            m.cleanup();
        }
        catch (Exception e)
        {
        }
    }
    public void HoiSinh(sbyte type)//0 ve lang//1 hoi sinh tai cho
    {
        Message m = new Message(Cmd.COME_HOME_DIE);
        try
        {
            m.writer().writeByte(type);//0 nhan // 1 tra// 2 huy
            session.sendMessage(m);
            m.cleanup();
        }
        catch (Exception e)
        {
        }
    }

    public void requestRegister(String user, String text, String version)
    {
        // TODO Auto-generated method stub
        Message m = new Message(Cmd.REGISTER);
        try
        {
            m.writer().writeUTF(user);
            m.writer().writeUTF(text);
            session.sendMessage(m);
            m.cleanup();
        }
        catch (Exception e)
        {
        }
    }
    public void requestPlayerInfo(short idchar)
    {
        Message m = new Message(Cmd.PLAYER_INFO);
        try
        {
            m.writer().writeShort(idchar);
            session.sendMessage(m);
            m.cleanup();
        }
        catch (Exception e)
        {
        }

    }
    public void requestDanhKhu()
    {
        Message m = new Message(Cmd.REQUEST_REGION);
        try
        {
            session.sendMessage(m);
            m.cleanup();
        }
        catch (Exception e)
        {
        }
    }
    public void requestChangeKhu(sbyte idkhu)
    {
        Message m = new Message(Cmd.CHANGE_REGION);
        try
        {
            m.writer().writeByte(idkhu);
            session.sendMessage(m);
            m.cleanup();
        }
        catch (Exception e)
        {
        }
    }
    public void requestADD_BASE_POINT(sbyte idnex, short value)
    {

        Message m = new Message(Cmd.ADD_BASE_POINT);
        try
        {
            m.writer().writeByte(1);
            m.writer().writeByte(idnex);
            m.writer().writeShort(value);
            //			 byte type = message.dis.readByte();//loai cho base point va skill
            //		        byte index = message.dis.readByte();//sinh,van,khi,tri,luc,toc
            //		        short value = message.dis.readShort();//gia tri cong
            session.sendMessage(m);
            m.cleanup();
        }
        catch (Exception e)
        {
        }
    }
    public void ThachDau(sbyte index, short idchar)
    {
        Message m = new Message(Cmd.COMPETED_ATTACK);
        try
        {
            m.writer().writeByte(index); //0 moi 1 ok
            m.writer().writeShort(idchar);
            session.sendMessage(m);
            m.cleanup();
        }
        catch (Exception e)
        {
        }
    }
    public void ChangeFlag(sbyte indexCo)
    {
        Message m = new Message(Cmd.STATUS_ATTACK);
        try
        {
            m.writer().writeByte(indexCo); //0 moi 1 ok
            session.sendMessage(m);
            m.cleanup();
        }
        catch (Exception e)
        {
        }
    }
}