


using System;
public class Controller : IMessageHandler {

	protected static Controller me;
	public Message messWait;

	public static Controller gI() {
		if (me == null)
			me = new Controller();
		return me;
	}

	public void onConnectOK() {
		Cout.println("Connect ok");
	}
	
	public void onConnectionFail() {
//		if (!isTryGetIPFromWap) {
//			new Thread(new Runnable() {
//				public void run() {
//					try {
//						Thread.sleep(1000);
//						System.out.println(GameMidlet.linkGetHost);
//						HttpConnection connection = (HttpConnection) Connector.open(GameMidlet.linkGetHost);
//						connection.setRequestMethod(HttpConnection.GET);
//						connection.setRequestProperty("Content-Type", "//text plain");
//						connection.setRequestProperty("Connection", "close");
//						if (connection.getResponseCode() == HttpConnection.HTTP_OK) {
//							string str = "";
//							InputStream inputstream = connection.openInputStream();
//							int length = (int) connection.getLength();
//							if (length != -1) {
//								sbyte incomingData[] = new byte[length];
//								inputstream.read(incomingData);
//								str = new String(incomingData);
//							}
//							System.out.println("New ip: " + str);
//							Rms.saveIP(str);
//							Session_ME.gI().connect("socket://" + str);
//							isTryGetIPFromWap = true;
//							return;
//						}
//					} catch (Exception e) {
//					//	e.printStackTrace();
//					}
//
//				}
//			}).start();
//			return;
//		}
        GameCanvas.currentScreen.isConnect = false;
        GameCanvas.currentScreen.connectCallBack = true;
	}

    public void onDisconnected()
    {
        GameCanvas.currentScreen.isConnect = false;
        GameCanvas.currentScreen.isCloseConnect = true;
        GameCanvas.currentScreen.connectCallBack = true;
		GameCanvas.instance.resetToLoginScr();
	}
	
	public void onMessage(Message msg) {
		try{
			Char c = new Char();
           // Cout.println("MESSAGE RECIVE ----> "+msg.command);

			Quest q = null;
			switch (msg.command) {
			case Cmd.QUEST:
				GuiQuest.listNhacNv.removeAllElements();

				sbyte typeUpdate = msg.reader().readByte(); // update
				sbyte typeQuest = msg.reader().readByte(); // loại nhiệm vụ
				Cout.println(typeUpdate+" MESSAGE RECIVE ---QUEST typeQuest-> "+typeQuest);
				int[] idquestupdate =null;
				string[] chuoinhacUpdate =null;
				if(typeUpdate==1){
					int nsize = msg.reader().readByte();// msg.reader().readShort();
					idquestupdate = new int [nsize];
					chuoinhacUpdate = new string[nsize];
					for (int i = 0; i < nsize; i++) {
						idquestupdate[i] = msg.reader().readShort();//idquesst

						Cout.println(i+" ---QUEST idquest update-> "+idquestupdate[i]);
						bool isMain = msg.reader().readbool();
						string name = msg.reader().readUTF();
						chuoinhacUpdate[i]= msg.reader().readUTF();
						Cout.println(i+" ---QUEST chuoinhac update-> "+chuoinhacUpdate[i]);
						string[] cat = mFont.tahoma_7_white.splitFontArray(chuoinhacUpdate[i], InfoItem.wcat);
						for (int j = 0; j < cat.Length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
//		                m.dos.writeUTF(info.getName());
//		                //remind khi dang lam nhiem vu(thit tuoi: 1/5)
//		                m.dos.writeUTF(info.getRemindWorking()+": "+q.getInfoWorking(p, info));
					}
				}
				else {
                    if (typeQuest == Task.CAN_RECEIVE_TASK)
                    {
                        Quest.listUnReceiveQuest.removeAllElements();
                        for (int j = 0; j < GameScr.vNpc.size(); j++)
                        {
                            Npc npc = (Npc)GameScr.vNpc.elementAt(j);
                            if (npc != null && npc.typeNV == 0)
                                npc.typeNV = -1;
                        }
                    }
                    else if (typeQuest == Task.COMPLETE_TASK)
                    {
                        Quest.vecQuestFinish.removeAllElements();
                        for (int j = 0; j < GameScr.vNpc.size(); j++)
                        {
                            Npc npc = (Npc)GameScr.vNpc.elementAt(j);
                            if (npc != null && npc.typeNV == 1)
                                npc.typeNV = -1;
                        }
                    }
                    else if (typeQuest == Task.DOING_TASK)
                    {
                        Quest.vecQuestDoing_Main.removeAllElements();
                        Quest.vecQuestDoing_Sub.removeAllElements();
                        for (int j = 0; j < GameScr.vNpc.size(); j++)
                        {
                            Npc npc = (Npc)GameScr.vNpc.elementAt(j);
                            if (npc != null && npc.typeNV == 2)
                                npc.typeNV = -1;
                        }
                    }
					sbyte sizeQuest = msg.reader().readByte(); // size nhiệm vụ
					for(int i = 0; i < sizeQuest; i++){
						int idQuest = msg.reader().readShort(); //  id nhiệm vụ
						bool isMain = msg.reader().readbool(); //  có phải nhiệm vụ chính k
						string nameQuest = msg.reader().readUTF(); // tên nhiệm vụ
						if(typeQuest == Task.CAN_RECEIVE_TASK){ // nhiệm vụ có thể nhận
							short idNPC = msg.reader().readShort(); // idNpc nhận nv
							for (int j = 0; j < GameScr.vNpc.size(); j++) {
								Npc npc = (Npc)GameScr.vNpc.elementAt(j);
								if(npc!=null&&npc.npcId==idNPC)
									npc.typeNV = 0;
							}
		                     // noi dung nc de nhan nhiem vu(trước khi nhận nv)
							string taklNPC = msg.reader().readUTF();// 
							sbyte typeQuestThis = msg.reader().readByte(); // loại nhiệm vụ này
							string shortdis = msg.reader().readUTF(); // //Câu mô tả tóm tắt nhiệm vụ phải làm(sau khi nhận nv)
							string gift = msg.reader().readUTF();// phần thưởng
							gift = gift.Trim().Length==0?"0/0/0":gift;
							string nhacnv = msg.reader().readUTF(); /// nhắc nv paint ở ngoài
							q = new Quest(idQuest, isMain, nameQuest,
									idNPC, taklNPC,
									typeQuestThis, shortdis,gift,nhacnv);
							Quest.listUnReceiveQuest.addElement(q);
						}else if(typeQuest == Task.COMPLETE_TASK){ // nhiệm vụ hoàn thành
							short idNPC = msg.reader().readShort(); // idNPC trả nhiệm vụ
						//	Cout.println(getClass(), "Quest nv hoan thanh "+idNPC);
							for (int j = 0; j < GameScr.vNpc.size(); j++) {
								Npc npc = (Npc)GameScr.vNpc.elementAt(j);
								if(npc!=null&&npc.npcId==idNPC)
									npc.typeNV = 1;
							}
							string taklNPC = msg.reader().readUTF(); // nội dung nói chuyện NPC
							string talknof = msg.reader().readUTF(); // thông báo và hướng dẫn trả nhiệm vụ từ server
							string nhacnv = msg.reader().readUTF(); /// nhắc nv paint ở ngoài
							q = new Quest(idQuest, isMain, nameQuest,
									idNPC, taklNPC,
									talknof,nhacnv);
							Quest.vecQuestFinish.addElement(q);
						}else if(typeQuest == Task.DOING_TASK){ // nhiệm vụ đang làm
							sbyte typeDoingQuest = msg.reader().readByte();// loai nhiem vu 1killqua//2chuyendo//3nc
	
							string contendQuest = msg.reader().readUTF();// mo ta nhiem vu cho nay (paint trong màn hình nv)
							string supContend = msg.reader().readUTF();//get noi dung support khi dang lam nhiem vu

						//	Cout.println(getClass(),contendQuest+ " Quest nv daglam supContend "+supContend);
							short idNpcPay = msg.reader().readShort();// idnpc tra nhiem vu
							string nhacnv = msg.reader().readUTF(); /// nhắc nv paint ở ngoài
							Cout.println(idNpcPay+" idNpcPay nhận nv dg lamtypeDoingQuest "+typeDoingQuest );
							for (int j = 0; j < GameScr.vNpc.size(); j++) {
								Npc npc = (Npc)GameScr.vNpc.elementAt(j);
								if(npc!=null&&npc.npcId==idNpcPay)
									npc.typeNV = 1;
							}
							if(typeDoingQuest == Task.TYPE_DOING_TASK_IP){// vận chuyển
								short idNpc = msg.reader().readShort();// id Npc can chuyen den 
							}else if(typeDoingQuest == Task.TYPE_DOING_TASK_COLLECT){ // thu thập
								sbyte total = msg.reader().readByte();// soluong item
								// id các item cần phải lấy
								short[] item = new short[total];
								// số item cần phải lấy
								short[] totalitem = new short[total];
								// số item đã dc
								short[] nItemGot = new short[total];
								for(int j = 0; j < total; j++){
									item[j] = msg.reader().readShort();//id item
									nItemGot[j] = msg.reader().readShort();// soluong đã nhặt đc 
									totalitem[j] = msg.reader().readShort();// số lượng cần nhặt
								}
	
								string giftforQuest = msg.reader().readUTF();
								giftforQuest = giftforQuest.Trim().Length==0?"0/0/0":giftforQuest;
								Cout.println("MESSAGE RECIVE ---QUEST-> NV NHAT ITEM");
								q = new Quest(idQuest, isMain, nameQuest,
										supContend, Task.DOING_TASK, contendQuest,
										idNpcPay, item, totalitem, nItemGot,
										Quest.TYPE_ITEM,giftforQuest,nhacnv);
							}else if(typeDoingQuest == Task.TYPE_DOING_TASK_SKILL){//giet quai
								sbyte totalKill = msg.reader().readByte();
								short[] idMons = new short[totalKill];
								short[] totalMons = new short[totalKill];
								short[] nMonsKilled = new short[totalKill];
								for(int k = 0; k < totalKill; k++){
									idMons[k] = msg.reader().readShort(); // id quái
									nMonsKilled[k] =msg.reader().readShort(); // số lượng đã giết
									totalMons[k] =msg.reader().readShort(); // số lượng cần giết
								}
	
								string giftforQuest = msg.reader().readUTF();
								giftforQuest = giftforQuest.Trim().Length==0?"0/0/0":giftforQuest;
								q = new Quest(idQuest, isMain, nameQuest,
										supContend, Task.DOING_TASK, contendQuest,
										idNpcPay, idMons, totalMons,
										nMonsKilled, Quest.TYPE_MONSTER,giftforQuest,nhacnv);
							}
							Cout.println(" nv dang lam "+q);
							if (q != null) {
								if (isMain) {
									Cout.println(" nv dang lam adđ  "+q);
									Quest.vecQuestDoing_Main.addElement(q);
								} else {
									Cout.println(" nv dang lam adđ 222 "+q); 
									Quest.vecQuestDoing_Sub.addElement(q);
								}
							}
						}
					}
				}
				if(InfoItem.wcat==-1) 
					InfoItem.wcat =  10*mGraphics.getImageWidth(loadImageInterface.imgChatConner)-10;
				for (int i = 0; i < Quest.listUnReceiveQuest.size(); i++) {
					Quest qe =  (Quest)Quest.listUnReceiveQuest.elementAt(i);
					if(typeUpdate==0){
						string[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
						for (int j = 0; j < cat.Length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}else{
						if(typeQuest==Task.CAN_RECEIVE_TASK)
							for (int j = 0; j < idquestupdate.Length; j++) {
								if(qe.ID!=idquestupdate[j]){
									string[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.Length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));
									}
								}
							}
					}
				}
				for (int i = 0; i < Quest.vecQuestDoing_Main.size(); i++) {
					Quest qe =  (Quest)Quest.vecQuestDoing_Main.elementAt(i);
					if(typeUpdate==0){
						string[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
						for (int j = 0; j < cat.Length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}else{
						if(typeQuest==Task.DOING_TASK)
							for (int j = 0; j < idquestupdate.Length; j++) {
								if(qe.ID!=idquestupdate[j]){
									string[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.Length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));
									}
								}
							}
					}
				}
				for (int i = 0; i < Quest.vecQuestDoing_Sub.size(); i++) {
					Quest qe =  (Quest)Quest.vecQuestDoing_Sub.elementAt(i);
					if(typeUpdate==0){
						string[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
						for (int j = 0; j < cat.Length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}else{
						if(typeQuest==Task.DOING_TASK)
							for (int j = 0; j < idquestupdate.Length; j++) {
								if(qe.ID!=idquestupdate[j]){
									string[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.Length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));
									}
								}
							}
					}
				}
				for (int i = 0; i < Quest.vecQuestFinish.size(); i++) {
					Quest qe =  (Quest)Quest.vecQuestFinish.elementAt(i);
					if(typeUpdate==0){
						string[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
						for (int j = 0; j < cat.Length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}else{
						if(typeQuest==Task.COMPLETE_TASK)
							for (int j = 0; j < idquestupdate.Length; j++) {
								if(qe.ID!=idquestupdate[j]){
									string[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.Length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));
									}
								}
							}
					}
				}
				break;
			case Cmd.SKILL_CHAR:
				int sizeKill = msg.reader().readShort();
				//Cout.println("cmd skillllllllllllll "+sizeKill);
				int idskill0 = 0;
				for(int i = 0; i < sizeKill; i++){
					sbyte id =msg.reader().readByte();
					if(i==0) idskill0 = id;
					short idIcon = msg.reader().readShort();
					short idIconTron = msg.reader().readShort();//icon skill tron
					string name = msg.reader().readUTF();
					sbyte typeskill = msg.reader().readByte();//chu dong or bi dong
					string mota = msg.reader().readUTF(); //mo ta
					sbyte ntaget = msg.reader().readByte();//so luong muc tieu danh
					sbyte typebuff = msg.reader().readByte();//loai skill buff
					sbyte subeff = msg.reader().readByte();//so luong muc tieu danh
					short range = msg.reader().readShort(); //pham vi
					short ideff =  msg.reader().readShort();
					sbyte nlevel = msg.reader().readByte();
					SkillTemplate skilltemplate = new SkillTemplate(id, name, idIcon,typeskill,ntaget,typebuff,subeff,range,mota);
					skilltemplate.iconTron = idIconTron;
					skilltemplate.ideff = ideff;
//					Cout.println(id+" ideff "+ideff);

					Cout.println(id+" SKILL_CHAR nlevel "+nlevel);
					skilltemplate.khoitaoLevel(nlevel);//// tong so level cua 1 skill
					for (int j = 0; j < nlevel; j++) {
						skilltemplate.levelChar = new short[nlevel];
						skilltemplate.levelChar[j] = msg.reader().readShort(); // mp hao
						skilltemplate.mphao[j] = msg.reader().readShort(); // mp hao
						skilltemplate.cooldown[j] = msg.reader().readInt(); // cooldow thoi gian cho skill
//						Cout.println(j+" cooldown "+skilltemplate.cooldown[j]);
						skilltemplate.timebuffSkill[j] = msg.reader().readInt(); /// thoi gian buff cua skill
						skilltemplate.pcSubEffAppear[j]  = msg.reader().readByte();// ty le xuat hien hieu ung phu
						skilltemplate.tExistSubEff[j]  = msg.reader().readShort(); // thoi gian ton tai cua hieu ung phu
						skilltemplate.usehp[j]  = msg.reader().readShort(); // ty le tang % su dung binh hp
						skilltemplate.usemp[j]  = msg.reader().readShort(); // ty le tang % su dung binh mp
						skilltemplate.ntargetlv[j]  = msg.reader().readByte();
						skilltemplate.rangelv[j]  = msg.reader().readShort(); //pháº¡m vi áº£nh hÆ°á»Ÿng cá»§a skill
						skilltemplate.dame[j] = msg.reader().readShort(); 
					}

					SkillTemplates.add(skilltemplate); // lÃ m giá»‘ng nhÆ° itemtemplate
					
					
				}
//				 Char.myChar().mQuickslot[i].setIsSkill(idSkillGan[i], false);
//				for (int i = 0; i < Char.myChar().mQuickslot.length; i++) {
//					if( Char.myChar().mQuickslot[i].idSkill!=-1)
//						return;
//				}
//				//troung hop chua gan phim nao het
//				Char.myChar().mQuickslot[0].setIsSkill(idskill0, false);
//				QuickSlot.SaveRmsQuickSlot();
				break;
			case Cmd.REQUEST_SHOP:
					ShopMain.idItemtemplate = null;
				sbyte menuId = msg.reader().readByte();

				int itemlistSize = msg.reader().readShort();
				ShopMain.indexidmenu = menuId;
				ShopMain.idItemtemplate = new short[itemlistSize];
				for(int i = 0; i < itemlistSize; i++ ){
					short idTemplate = msg.reader().readShort();
					
					ShopMain.idItemtemplate[i] = idTemplate;
				}
				ShopMain.getItemList(ShopMain.indexidmenu,ShopMain.idItemtemplate);
				break;
			case Cmd.CMD_DYNAMIC_MENU:
			//	Cout.println(getClass(), " nhan ve list tabshop");
				sbyte menuShopsize = msg.reader().readByte(); // soluong menu shop
				ShopMain.idMenu = new int[menuShopsize];
				ShopMain.nameMenu = new string[menuShopsize];
				ShopMain.dis = new string[menuShopsize];
				ShopMain.cmdShop = new Command[menuShopsize];
//				ShopMain.idItemtemplate = new int[menuShopsize];
				for(int i = 0; i < menuShopsize; i++){
					sbyte idmenu = msg.reader().readByte();
					ShopMain.idMenu[i] = idmenu;
					string menuname = msg.reader().readUTF();
					ShopMain.nameMenu[i] = menuname;
					string dis = msg.reader().readUTF();
					ShopMain.dis[i] = dis;
				}
				break;
			case Cmd.TRADE:
				sbyte typeTrade = msg.reader().readByte();

				Cout.println(" typeTrade ");
				if(typeTrade == Contans.INVITE_TRADE){ // moi giao dich 
					short CharIDpartner = msg.reader().readShort();
					Char.myChar().partnerTrade = null;
					Char.myChar().partnerTrade = GameScr.findCharInMap(CharIDpartner);
					Cout.println(CharIDpartner+ " moigiao dich "+Char.myChar().partnerTrade);
					GameCanvas.startYesNoDlg(msg.reader().readUTF(), GameScr.cmdAcceptTrade, new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
				}else if(typeTrade == Contans.ACCEPT_INVITE_TRADE){ // dong y giao dich 
					short CharIDpartner = msg.reader().readShort();
					if(Char.myChar().partnerTrade==null)
						Char.myChar().partnerTrade = GameScr.findCharInMap(CharIDpartner);

                    GameScr.isBag = false;
					GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
					GameCanvas.gameScr.guiMain.menuIcon.iconTrade.performAction();

				//	Cout.println(getClass(), CharIDpartner+" moigiao dich2 "+Char.myChar().partnerTrade);
//					
//					GameScr.gI().guiMain.moveClose=false;
//					indexpICon = Contans.ICON_TRADE;
//					trade=new TradeGui(GameCanvas.wd6-20, 20);
//					trade.SetPosClose(cmdClose);
//					paintButtonClose=true;
//					MenuIcon.lastTab.add(""+Contans.ICON_TRADE);
					Service.gI().requestinventory();
					//isPaintTrade = true;
				}else if(typeTrade == Contans.MOVE_ITEM_TRADE){
					sbyte typeItem = msg.reader().readByte(); // loại
					sbyte typeItemmove = msg.reader().readByte(); // đưa xuống hay đưa lên 
					short charId = msg.reader().readShort();
//					if(Char.myChar().partnerTrade==null){
//						Cout.println(charId+ " moigiao dich222 "+Char.myChar().partnerTrade);
//						Char.myChar().partnerTrade= GameScr.findCharInMap(charId);
//					}
                    if (typeItemmove == 0)
                    { // add item
                        if (charId == Char.myChar().charID)
                        { // add vao danh sach item giao dá»‹ch cá»§a mÃ¬nh
                            //							Char.myChar().ItemMyTrade = new Item[8];
                            for (int i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
                            {

                                if (Char.myChar().ItemMyTrade[i] == null)
                                {
                                    Char.myChar().ItemMyTrade[i] = new Item();
                                    short IdItem = msg.reader().readShort();
                                    Char.myChar().ItemMyTrade[i].itemId = IdItem;
                                    short iditemTemplate = msg.reader().readShort();
                                    //Cout.println(IdItem + " iditemTemplate " + iditemTemplate);
                                    if (iditemTemplate != -1)
                                    {
                                        Char.myChar().ItemMyTrade[i].template = ItemTemplates.get(iditemTemplate);
                                    }
                                    //									byte optionsize = msg.reader().readByte();
                                    //									for (int j = 0; j < optionsize; j++) {
                                    //										String info = msg.reader().readUTF();
                                    //										byte colorname = msg.reader().readByte();
                                    //									}
                                    break;
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.Length; i++)
                            { // add vÃ o danh sÃ¡ch item cá»§a parner
                                if (Char.myChar().partnerTrade.ItemParnerTrade[i] == null)
                                {
                                    //System.out.println("ADD ITEM PARNER TRADE");
                                    Char.myChar().partnerTrade.ItemParnerTrade[i] = new Item();
                                    short IdItem = msg.reader().readShort();
                                    Char.myChar().partnerTrade.ItemParnerTrade[i].itemId = IdItem;
                                    short iditemTemplate = msg.reader().readShort();
                                   // Cout.println("partner iditemTemplate " + iditemTemplate);
                                    if (iditemTemplate != -1)
                                    {
                                        Char.myChar().partnerTrade.ItemParnerTrade[i].template = ItemTemplates.get(iditemTemplate);
                                    }
                                    //									byte optionsize = msg.reader().readByte();
                                    //									for (int j = 0; j < optionsize; j++) {
                                    //										String info = msg.reader().readUTF();
                                    //										byte colorname = msg.reader().readByte();
                                    //									}
                                    break;
                                }
                            }
                        }
					}else{ // remove item
					//	Cout.println(getClass(),Char.myChar().charID+ "remove item "+charId);
						if(Char.myChar().charID == charId){
						//	Cout.println(getClass(), "remove Me item ");
							short IdItem = msg.reader().readShort();
							for(int i = 0; i < Char.myChar().ItemMyTrade.Length; i++){
								if(Char.myChar().ItemMyTrade[i] != null){
									if(Char.myChar().ItemMyTrade[i].itemId == IdItem){
									//	Cout.println(getClass(),i+ "remove item null");
										Char.myChar().ItemMyTrade[i] = null;
										GameCanvas.gameScr.guiMain.menuIcon.trade.indexSelect1 = -1;
										break;
									}
								}
							}
						}else{
						//	Cout.println(getClass(), "remove parner item ");
//							Char.partnerTrade.ItemParnerTrade = new Item[15]; 
							short IdItem = msg.reader().readShort();
							for(int i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.Length; i++){
								if(Char.myChar().partnerTrade.ItemParnerTrade[i] != null){
									if(Char.myChar().partnerTrade.ItemParnerTrade[i].itemId == IdItem){
										Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
//										TradeGui.indexSelect1 = -1;
									}
									
								}
							}
						}
					}
					
					
				}else if(typeTrade == Contans.CANCEL_TRADE){
					short charidP = msg.reader().readShort();
					Char charPar = GameScr.findCharInMap(charidP);
					Cout.println(" CANCEL_TRADE ");
					GameCanvas.gameScr.guiMain.menuIcon.trade=null;
					GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
					GameCanvas.startOKDlg(charPar.cName+(msg.reader().readUTF()));
					//GameScr.gI().tradeGui = null;
				}else if(typeTrade == Contans.LOCK_TRADE){
					short charidP = msg.reader().readShort();
//					Char charPar = GameScr.findCharInMap(charidP);
					if(Char.myChar().charID != charidP){

						Cout.println(" LOCK_TRADE other ");
						GameCanvas.gameScr.guiMain.menuIcon.trade.block2 = true;
					}else{

						Cout.println(" LOCK_TRADE me ");
						GameCanvas.gameScr.guiMain.menuIcon.trade.block1 = true;
					}

				}else if(typeTrade == Contans.TRADE){
					short charidP = msg.reader().readShort();
					Char cTrade = GameScr.findCharInMap(charidP);
					GameCanvas.startYesNoDlg(cTrade.cName+msg.reader().readUTF(), TradeGui.cmdTradeEnd, new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
					//GameScr.gI().tradeGui = null;
					for(int i = 0; i < Char.myChar().ItemMyTrade.Length; i++){
						Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
						Char.myChar().ItemMyTrade[i] = null;
					}
				}else if(typeTrade == Contans.END_TRADE){
					Cout.println(" end trade ");
					for(int i = 0; i < Char.myChar().ItemMyTrade.Length; i++){
						Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
						Char.myChar().ItemMyTrade[i] = null;
					}
					Char.myChar().partnerTrade = null;
					GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
					GameCanvas.gameScr.guiMain.menuIcon.trade = null;
				}
				break;
			case Cmd.FRIEND:
				sbyte typeFriend = msg.reader().readByte();
				if(typeFriend == Friend.INVITE_ADD_FRIEND){
					Char.myChar().idFriend = msg.reader().readShort();
					string tempDebug = msg.reader().readUTF();
					//System.out.println("IDfiend ----> "+Char.myChar().idFriend+"  "+tempDebug);
					GameCanvas.startYesNoDlg(tempDebug, GameScr.gI().cmdComfirmFriend, new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
				}else if(typeFriend == Friend.ACCEPT_ADD_FRIEND){
					short CharIDFriend = msg.reader().readShort();
					c = GameScr.findCharInMap(CharIDFriend);
					Char.myChar().vFriend.addElement(c);
				}else if(typeFriend == Friend.REQUEST_FRIEND_LIST){
					Char.myChar().vFriend.removeAllElements();
					sbyte sizeFriendList = msg.reader().readByte();
					for(int i = 0; i < sizeFriendList; i++){
						Char ch = new Char();
						ch.isOnline = msg.reader().readbool();
						if(ch.isOnline){
							ch.charID = msg.reader().readShort(); // không online defaut là 0							
						}
						ch.CharidDB = msg.reader().readShort();
						ch.cName = msg.reader().readUTF();
						ch.clevel = msg.reader().readShort();
						Char.myChar().vFriend.addElement(ch);
					}
				}else if(typeFriend == Friend.UNFRIEND){
					short CharID = msg.reader().readShort();
					short charUser = msg.reader().readShort();
				//	System.out.println("Unfriend -----> "+CharID);
					for(int i = 0; i < Char.myChar().vFriend.size(); i++){
						Char chardel = (Char) Char.myChar().vFriend.elementAt(i);
						if(chardel.charID != 0){ // khi online 
							if(chardel.charID == CharID)
								Char.myChar().vFriend.removeElementAt(i);		
						}else{
							if(chardel.CharidDB == charUser){ // khi off line

								Char.myChar().vFriend.removeElementAt(i);		
							}
						}
					}
				}
				break;
			case Cmd.PARTY:
				sbyte typeParty = msg.reader().readByte();
				Cout.println("type Party  "+typeParty);
				if(typeParty == Type_Party.INVITE_PARTY){
					Party.gI().charId = msg.reader().readShort();
					GameCanvas.startYesNoDlg(msg.reader().readUTF(), GameScr.gI().cmdAcceptParty, new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
				}else if(typeParty == Type_Party.GET_INFOR_PARTY){
					short PartyId = msg.reader().readShort();
					short CharLeaderid = msg.reader().readShort();
					if(CharLeaderid==Char.myChar().charID)
						Party.gI().isLeader = true;
					sbyte membersize = msg.reader().readByte();
					short[] charIDmeber =  new short[membersize];
					short[] lvchar = new short[membersize];
					short[] idhead = new short[membersize];
					for(int i = 0; i < membersize; i++){
						charIDmeber[i] = msg.reader().readShort();
						lvchar[i] = msg.reader().readShort();
						idhead[i] = msg.reader().readShort();
		                //m.dos.writeShort(members.get(i).getLevel());
		                //  m.dos.writeShort(members.get(i).getIdPartHead());
					}
					GameScr.hParty.containsKey(PartyId+"");
					GameScr.hParty.put(PartyId+"",(Object)new Party(PartyId, charIDmeber, CharLeaderid,lvchar,idhead));
				}else if(typeParty == Type_Party.OUT_PARTY){
					short IdChar = msg.reader().readShort();
					if(IdChar==Char.myChar().charID){
						Cout.println("bi kichkkkkkkkkkkkkkk");
						GameScr.hParty.clear();
						Party party  = (Party) GameScr.hParty.get(Char.myChar().idParty+"");
                        Party.vCharinParty.removeAllElements();
					}
						
					Char chaRe = GameScr.findCharInMap(IdChar);
					Party partyy = null;
					if(chaRe != null){
						partyy = (Party) GameScr.hParty.get(chaRe.idParty+"");
                        for (int i = 0; i < Party.vCharinParty.size(); i++)
                        {
                            Char charRemove = (Char)Party.vCharinParty.elementAt(i);
							if(charRemove.charID == IdChar){
								charRemove.idParty = -1;
								charRemove.isLeader = false;
                                Party.vCharinParty.removeElementAt(i);
							}
							
						}
                        if (Party.vCharinParty.size() < 2)
                        {
							GameScr.hParty.clear();
                            Party.vCharinParty.removeAllElements();
						}
						
					}else{
						chaRe = Char.myChar();
						partyy = (Party) GameScr.hParty.get(chaRe.idParty+"");
                        for (int i = 0; i < Party.vCharinParty.size(); i++)
                        {
                            Char charRemove = (Char)Party.vCharinParty.elementAt(i);
							if(charRemove.charID == IdChar){
								charRemove.idParty = -1;
								charRemove.isLeader = false;
                                Party.vCharinParty.removeElementAt(i);
							}
							
						}
					}
                    if (Party.vCharinParty.size() <= 0)
						GameScr.hParty.remove(chaRe.idParty+"");
//					for(int i = 0; i < GameScr.vParty.size(); i++){
//						Party party = (Party) GameScr.vParty.elementAt(i);
//						for(int j = 0; j < party.vCharinParty.size(); j++){
//							Char charre = (Char) party.vCharinParty.elementAt(j);
//							if(charre.charID == IdChar)
//								party.vCharinParty.removeElement(charre);
//							
//						}
//						if(party.vCharinParty.size() <= 1);
//					}
				}else if(typeParty == Type_Party.DISBAND_PARTY){
					//System.out.println("DISBAND_PARTY");
					short idParty = msg.reader().readShort();
					//System.out.println("ID Party ---> "+idParty);
					Party party = (Party) GameScr.hParty.get(idParty+"");

                    for (int i = 0; i < Party.vCharinParty.size(); i++)
                    {
                        Char ch = (Char)Party.vCharinParty.elementAt(i);
						ch.idParty = -1;
						ch.isLeader = false;
                        Party.vCharinParty.removeElementAt(i);
					}
//					part.vCharinParty.removeAllElements();
					GameScr.hParty.remove(idParty+"");
				}else if(typeParty == Type_Party.GET_INFOR_NEARCHAR){
					GameScr.charnearByme.removeAllElements();
					sbyte sizeCharlist = msg.reader().readByte();
					//System.out.println("Size ----> "+sizeCharlist);
					for(int i = 0; i < sizeCharlist; i++){
						Char ch = new Char();
						ch.charID = msg.reader().readShort();
						//System.out.println("IDChar ----> "+ch.charID);
						ch.cName = msg.reader().readUTF();
						//System.out.println("CharName ----> "+ch.cName);
						ch.idParty = msg.reader().readShort();
						//System.out.println("Idparty ----> "+ch.idParty);
						GameScr.charnearByme.addElement(ch);
					}
				}
				break;
			case Cmd.MENU_NPC:
				Npc npcc = null;
				for (int i = 0; i < GameScr.vNpc.size(); i++) {
					npcc = (Npc) GameScr.vNpc.elementAt(i);
//					if (npc.template.npcTemplateId == ncpTemplateId && npc.Equals(Char.myChar().npcFocus)) {
//					for(int j = 0; j < npc.template.menu.Length; j++)
//						ChatPopup.addChatPopupMultiLine(npc.template.contendChat, 1000, npc);
//						break;
//					}
				}
				mVector vmenu = new mVector();
				try {
					int sizee = msg.reader().readByte();
					GameScr.showChatPopup(npcc.template.contendChat, 0, null, 0, null);
					 string[] arrSub = new string[sizee/*msg.reader().readByte()*/];
					for (int i = 0; i < sizee; i++) {
//						for (int j = 0; j < arrSub.Length; j++) {
							arrSub[i] = msg.reader().readUTF();
//						}
						vmenu.addElement(new Command(arrSub[i], GameCanvas.instance,88820,arrSub));
					}
//					GameCanvas.startYesNoDlg(npc.template.contendChat, 0, null, 0, null, arrSub[0], arrSub[1]);
				} catch (Exception e) {
				////	e.printStackTrace();
				}
				if (Char.myChar().npcFocus == null)
					return;
				GameCanvas.menu.startAt(vmenu, 3);
				break;
            case Cmd.NPC:
                GameScr.vNpc.removeAllElements();
				sbyte sizeNpc = msg.reader().readByte();
				for(int i = 0; i < sizeNpc; i++){
					short idNpc = msg.reader().readShort();
					short npcX = msg.reader().readShort();
					short npcY = msg.reader().readShort();
					short npcIdtemplate = msg.reader().readShort();
					short npciDicon = 0;//msg.reader().readShort();
					Npc npcz = new Npc(npcX, npcY, npcIdtemplate, npciDicon);
					npcz.npcId = npcIdtemplate;
					for (int j = 0; j < Quest.listUnReceiveQuest.size(); j++) {
						Quest q1 = (Quest)Quest.listUnReceiveQuest.elementAt(j);
						if(q1.idNPC_From==npcIdtemplate)
							npcz.typeNV = 0;
					}
					for (int j = 0; j < Quest.vecQuestDoing_Main.size(); j++) {
						Quest q2 = (Quest)Quest.vecQuestDoing_Main.elementAt(j);
						if(q2.idNPC_To==npcIdtemplate)
							npcz.typeNV = 1;
					}
					for (int j = 0; j < Quest.vecQuestDoing_Sub.size(); j++) {
						Quest q3 = (Quest)Quest.vecQuestDoing_Sub.elementAt(j);
						if(q3.idNPC_From==npcIdtemplate)
							npcz.typeNV = 1;
					}
					for (int j = 0; j < Quest.vecQuestFinish.size(); j++) {
						Quest q4 = (Quest)Quest.vecQuestFinish.elementAt(j);
						if(q4.idNPC_From==npcIdtemplate||q4.idNPC_To==npcIdtemplate)
							npcz.typeNV = 1;
					}
					GameScr.vNpc.addElement(npcz);
					
				}
				
				break;
			case Cmd.PICK_REMOVE_ITEM:
				sbyte typeItemPick = msg.reader().readByte();
				short idItemPick = msg.reader().readShort();
				if(Char.myChar().itemFocus!=null&&Char.myChar().itemFocus.itemMapID==idItemPick){
					Char.myChar().itemFocus = null;
					Char.myChar().clearFocus(10);
				}
				short idPlayerPick = msg.reader().readShort();
				c = GameScr.findCharInMap(idPlayerPick);
				if(c == null){
					c = Char.myChar();
				}
                for (int i = 0; i < GameScr.vItemMap.size(); i++)
                {
                    ItemMap itempick = (ItemMap)GameScr.vItemMap.elementAt(i);
                    if (itempick.itemMapID == idItemPick)
                    {
                        itempick.setPoint(c.cx, c.cy);
                        if (c.charID == Char.myChar().charID)
                            itempick.isMe = true;
                        //					if (itempick.itemMapID == idItemPick) {
                        //						GameScr.vItemMap.removeElementAt(i);
                        //						break;
                        //					}
                    }
                }
				
				break;
			case Cmd.DROP_ITEM:
				sbyte typeDrop = msg.reader().readByte();
				sbyte typeItemRotRa = msg.reader().readByte();//1 Ä‘Ã¡nh quÃ¡i rá»›t ra, 0 char vá»©t ra.
				short IdMonterDie = msg.reader().readShort();
//				short iconID = msg.reader().readShort();
				short idd = msg.reader().readShort();
				short idTemplatee = msg.reader().readShort();
				//System.out.println("YItem ----> "+yItem);
				ItemMap itemDrop = null;
				if(typeItemRotRa==1){

					Mob mons = GameScr.findMobInMap(IdMonterDie);
					if(mons!=null){
						itemDrop = new ItemMap(idd, idTemplatee, mons.x, mons.yFirst+10);
	//					ServerEffect.addServerEffect(35, mons.x, yItem-20, 3);
					}
					else {
	//					ServerEffect.addServerEffect(35, xItem, yItem-20, 3);
						short xItem = msg.reader().readShort();
						short yItem = msg.reader().readShort();
						itemDrop = new ItemMap(idd, idTemplatee, xItem, yItem);
					}
					GameScr.vItemMap.addElement(itemDrop);
				}else{
					Char ch = GameScr.findCharInMap(IdMonterDie);
					if(ch!=null){
						itemDrop = new ItemMap(idd, idTemplatee, ch.cx, ch.cy);
						GameScr.vItemMap.addElement(itemDrop);
					}
				}
				itemDrop.type = typeDrop;
				break;
			case Cmd.REMOVE_TARGET:
                //				byte typeRemove = msg.reader().readByte();
                //				Cout.println("REMOVE_TARGET "+typeRemove);
                //				if(typeRemove == Type_Object.CAT_MONSTER){
                //					short idMobRetaget = msg.reader().readShort();
                //					for(int i = 0; i < GameScr.vMob.size(); i++){
                //						Mob	mobTg = (Mob) GameScr.vMob.elementAt(i);
                //						if(mobTg.mobId == idMobRetaget)
                //							mobTg.cFocus = null;
                ////						if(mobTargetRe.cFocus != null){
                ////							mobTargetRe.cFocus = null;
                ////						}
                //					}
                //				}
                break;

            case Cmd.DIE:
                sbyte byteDie = msg.reader().readByte(); // 
                if (byteDie == Type_Object.CAT_PLAYER)
                {
                    short playerID = msg.reader().readShort();
                    if (playerID == Char.myChar().charID)
                    {
                        GameScr.gI().guiMain.moveClose = true;
                        if (MenuIcon.isShowTab)
                            GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                        if (GameScr.isBag)
                            TabScreenNew.cmdClose.performAction();
                        //						GameCanvas.startCommandDlg("Báº¡n cÃ³ muá»‘n há»“i sinh táº¡i chá»— (10xu)?", 
                        //								new Command("Há»“i sinh", GameCanvas.instance, GameCanvas.cHoiSinh, null),
                        //								new Command("Vá» lÃ ng", GameCanvas.instance, GameCanvas.cVeLang, null));
                    }
                    c = GameScr.findCharInMap(playerID);

                    if (c != null)
                    {
                        c.cHP = 0;
                        c.statusMe = Char.A_DEAD;
                        //Cout.println("dieeeeeeeeeeeeee   ");
                    }

                }
                else if (byteDie == Type_Object.CAT_MONSTER)
                {
                    short mobID = msg.reader().readShort();
                    for (int i = 0; i < GameScr.vMob.size(); i++)
                    {
                        Mob mob = (Mob)GameScr.vMob.elementAt(i);
                        if (mob.mobId == mobID)
                        {
                            mob.status = Mob.MA_DEADFLY;
                            mob.startDie();

                            if (Char.myChar().mobFocus != null && Char.myChar().mobFocus.mobId == mobID)
                                Char.myChar().mobFocus = null;
                            break;
                        }
                    }
                    //					if(Char.myChar().mobFocus!=null&&Char.myChar().mobFocus.mobId == mobID)
                    //						Char.myChar().mobFocus.status = Mob.MA_DEADFLY;
                    //						Char.myChar().mobFocus.startDie();
                }

                break;
			case Cmd.ATTACK:
                sbyte typeAtt = msg.reader().readByte(); // 0 PLAYER 1 MONTERS
                //				Cout.println("typeAtt  "+typeAtt);
                if (typeAtt == 0)
                {// PLAY

                    sbyte Cat_type = msg.reader().readByte(); // 0 playerAtt monters 1 player player
                    //					Cout.println("Cat_typeAtt  "+Cat_type);
                    short PlayerAttID = msg.reader().readShort();
                    c = GameScr.findCharInMap(PlayerAttID);
                    if (c == null)
                        c = Char.myChar();
                    if (Cat_type == 0)
                    {//player attack monster
                        short mobvictim = msg.reader().readShort();
                        for (int i = 0; i < GameScr.vMob.size(); i++)
                        {
                            Mob mob = (Mob)GameScr.vMob.elementAt(i);
                            if (mob.mobId == mobvictim)
                                c.mobFocus = mob;
                            if (mobvictim == mob.mobId)
                            {
                                c.mobFocus.hp = msg.reader().readInt();
                                c.cMP = msg.reader().readInt();
                                c.cdame = msg.reader().readInt();
                                c.mobFocus.hp = (c.mobFocus.hp < 0 ? 0 : c.mobFocus.hp);
                                if (c.cdame > 0)
                                    GameScr.startFlyText("-" + c.cdame, c.mobFocus.x, c.mobFocus.y - 2 * mob.h - 5, 0, -2, mFont.RED);
                                else GameScr.startFlyText("miss", c.mobFocus.x, c.mobFocus.y - 2 * mob.h - 5, 0, -2, mFont.RED);

                                //							ServerEffect.addServerEffect(91,  c.mobFocus.x, c.mobFocus.y, 1);
                                if (PlayerAttID != Char.myChar().charID)
                                {

                                    c.cdir = (c.cx - c.mobFocus.x) > 0 ? -1 : 1;
                                    c.mobFocus.setInjure();
                                    c.mobFocus.injureBy = c;
                                    c.mobFocus.status = Mob.MA_INJURE;
                                }
                                if (mob.hp <= 0)
                                {
                                    mob.status = Mob.MA_DEADFLY;
                                }
                            }
                        }
                        //byte idskill
                        sbyte idskill = msg.reader().readByte();

                        if (idskill > -1 && idskill < GameScr.sks.Length - 1)
                        {
                            sbyte ideff = msg.reader().readByte();
                            if (c.charID != Char.myChar().charID)
                            {
                                if (idskill == 0)
                                    Music.play(Music.ATTACK_0, 0.5f);
                                else Music.play(Music.SKILL2, 0.5f);
                                c.setSkillPaint(GameScr.sks[ideff], Skill.ATT_STAND);
                            }
                        }
                    }
                    else if (Cat_type == 1)
                    {//player attack player
                        //				    	 msg.dos.writeShort(this.id);
                        c.cMP = msg.reader().readInt();
                        sbyte sizeBeAttack = msg.reader().readByte();
                        //				            msg.dos.writeInt(this.getMp());
                        //				            msg.dos.writeByte(target.size());
                        for (int i = 0; i < sizeBeAttack; i++)
                        {
                            short charBeAttack = msg.reader().readShort();
                            Char charBe = GameScr.findCharInMap(charBeAttack);
                            if (charBe != null)
                            {
                                charBe.cHP = msg.reader().readInt();
                                Cout.println(charBe.cName + "  onAttackkkk charBe.cHP " + charBe.cHP);
                                //				    			 if(charBe.cHP>0)
                                //				    			 charBe.statusMe = Char.A_INJURE;

                                charBe.doInjure(1, 0, false, 1);
                                c.cdir = (c.cx - charBe.cx) > 0 ? -1 : 1;
                                int dam = msg.reader().readInt();
                                ServerEffect.addServerEffect(25, charBe.cx, charBe.cy - 20, 1);
                                GameScr.startFlyText("-" + dam, charBe.cx, charBe.cy - 60, 0, -2, mFont.RED);

                            }
                        }
                        sbyte idskill = msg.reader().readByte();

                        if (idskill > -1 && idskill < GameScr.sks.Length - 1)
                        {
                            sbyte ideff = msg.reader().readByte();
                            if (c.charID != Char.myChar().charID)
                            {
                                c.setSkillPaint(GameScr.sks[ideff], Skill.ATT_STAND);
                                Music.play(Music.ATTACK_0, 0.5f);
                            }
                        }
                    }
                    //paint skill for all char

                    //				     if ((TileMap.tileTypeAtPixel(c.cx, c.cy) & TileMap.T_TOP) == TileMap.T_TOP)
                    //				      c.setSkillPaint(GameScr.sks[2],
                    //				        Skill.ATT_STAND);
                    //				     else
                    //				      c.setSkillPaint(GameScr.sks[2],
                    //				        Skill.ATT_FLY);

                }
                else if (typeAtt == 1)
                {
                    short mobAttId = msg.reader().readShort();
                    Mob mob = null;
                    for (int i = 0; i < GameScr.vMob.size(); i++)
                    {
                        Mob mobs = (Mob)GameScr.vMob.elementAt(i);
                        if (mobs.mobId == mobAttId)
                            mob = mobs;

                    }
                    short CharIdVictim = msg.reader().readShort();
                    //Cout.println(mobAttId+ "  monster attack charID "+CharIdVictim);

                    if (mob != null)
                    {
                        Char charbiattack = GameScr.findCharInMap(CharIdVictim);

                        mob.cFocus = charbiattack;
                        if (mob.cFocus == null)
                        {
                            mob.f = -1;
                            mob.cFocus = Char.myChar();
                        }
                        mob.dir = (mob.x - mob.cFocus.cx > 0 ? -1 : 1);
                        int hpmat = msg.reader().readInt();
                        mob.cFocus.cHP = hpmat;
                        if (charbiattack != null)
                        {
                            charbiattack.cHP = hpmat;
                        }

                        mob.dame = msg.reader().readInt();
                        mob.status = Mob.MA_ATTACK;
                        if (mob.status == Mob.MA_ATTACK)
                        {
                            //ServerEffect.addServerEffect(91, mob.cFocus.cx, mob.cFocus.cy, 1);
                            //Cout.println(getClass(),"   add dame");
                            GameScr.startFlyText("-" + mob.dame, mob.cFocus.cx, mob.cFocus.cy - 60, 0, -3, mFont.RED);
                            //mob.cFocus.statusMe = Char.A_INJURE;						
                        }

                        mob.setAttack(mob.cFocus);
                    }
                    //					if(mob.cFocus != null){
                    //						
                    //					}
                }
                break;
            case Cmd.REQUEST_MONSTER_INFO:
                short idmob = msg.reader().readShort();
                string namemob = msg.reader().readUTF();
                int maxhp = msg.reader().readInt();
                int hp = msg.reader().readInt();
                for (int i = 0; i < GameScr.vMob.size(); i++)
                {
                    Mob modInMap = (Mob)GameScr.vMob.elementAt(i);

                    if (modInMap != null && modInMap.mobId == idmob)
                    {
                        //						Char.myChar().mobFocus.mobId 
                        modInMap.mobName = namemob;
                        modInMap.maxHp = maxhp;
                        modInMap.hp = hp;
                    }
                }
                break;
			case Cmd.CHAT:
				sbyte typeChat = msg.reader().readByte();
				if(typeChat == Type_Chat.CHAT_MAP){ // char map
					
					short useriD = msg.reader().readShort();
					string text = msg.reader().readUTF();
					if (Char.myChar().charID == useriD)
						c = Char.myChar();
					else
						c = GameScr.findCharInMap(useriD);
					if (c == null)
						return;
					ChatPopup.addChatPopup(text, 100, c);
					ChatManager.gI().addChat(mResources.PUBLICCHAT[0], c.cName, text);
					
				}else if(typeChat == Type_Chat.CHAT_WORLD){
					
					ChatWorld.ChatWorldd(msg);
					
				}else if(typeChat == Type_Chat.CHAT_FRIEND){
					
					ChatPrivate.ChatFriend(msg);
				}
				break;
			case Cmd.GET_ITEM_INVENTORY:
                //Char.myChar().arrItemBag = null;
                sbyte typeInventory = msg.reader().readByte();
                Char.myChar().luong = msg.reader().readLong();
                Char.myChar().xu = msg.reader().readLong();
                if (typeInventory == 3)
                { //cat item
                    Char.myChar().arrItemBag = new Item[msg.reader().readByte()];
                    for (int i = 0; i < Char.myChar().arrItemBag.Length; i++)
                    {
                        sbyte typeItem = msg.reader().readByte();
                        short idItem = msg.reader().readShort();
                        short itemTemplateId = msg.reader().readShort();
                        if (itemTemplateId != -1)
                        {
                            Char.myChar().arrItemBag[i] = new Item();
                            Char.myChar().arrItemBag[i].itemId = idItem;
                            Char.myChar().arrItemBag[i].typeUI = Item.UI_BAG;
                            Char.myChar().arrItemBag[i].indexUI = i;
                            Char.myChar().arrItemBag[i].template = ItemTemplates.get(itemTemplateId);
                            if (Item.isBlood(Char.myChar().arrItemBag[i].template.type)
                                    || Item.isMP(Char.myChar().arrItemBag[i].template.type))
                            {
                                Char.myChar().arrItemBag[i].quantity = msg.reader().readShort();//so luong
                                Char.myChar().arrItemBag[i].value = msg.reader().readShort();
                            }
                        }
                    }
                }
                else if (typeInventory == 4)
                {

                }
                break;
			case Cmd.PLAYER_REMOVE:
					short CharIDRemove = msg.reader().readShort();
				if(CharIDRemove==Char.myChar().charID){
//					GameCanvas.instance.resetToLoginScr();
					return;
				}
				for(int i = 0; i < GameScr.vCharInMap.size(); i++){
					c = (Char) GameScr.vCharInMap.elementAt(i);
					if(CharIDRemove == c.charID){
						GameScr.vCharInMap.removeElementAt(i);
					}
				}
				break;
			case Cmd.PLAYER_INFO:
				
				short charID = msg.reader().readShort();

				int cMaxHP = msg.reader().readInt();
				int cHP = msg.reader().readInt();
				short clevel = msg.reader().readShort();
				for(int i = 0; i < GameScr.vCharInMap.size(); i++){
					Char ch = (Char) GameScr.vCharInMap.elementAt(i);
					if(ch.charID == charID){
						ch.cMaxHP = cMaxHP;
						ch.cHP = cHP;
						if(ch.clevel>0&&ch.clevel<clevel){
							Music.play(Music.LENLV, 10);
							ServerEffect.addServerEffect(22,ch.cx,ch.cy,1);
						}
						ch.clevel =clevel;
						if(ch.cMaxHP<=0) ch.cMaxHP = (ch.cHP<=0?1:ch.cHP);
					}
				}
//				c.charID = msg.reader().readShort();
//				System.out.println("Charinmap ----> "+c.charID);
//				c.cMaxHP = msg.reader().readShort();
//				c.cHP = msg.reader().readShort();

//				GameScr.vCharInMap.addElement(c);
				break;
			case Cmd.REQUEST_IMAGE:// yeu cau hinh tu server
				sbyte subImg = -1;
				subImg = msg.reader().readByte();
				int idimg = msg.reader().readInt();
				Cout.println(subImg+ "REQUEST_IMAGE receive  "+idimg);
				if(subImg==0){
					sbyte[] dataImg = Util.readByteArray(msg);
					if(GameCanvas.currentScreen==DownloadImageScreen.gI())
					DownloadImageScreen.isOKNext = true;
                    string path = SmallImage.getPathImage(idimg) + "" + idimg;
                    //Cout.println("REQUEST_IMAGE dataImg  " + dataImg.Length);
					Rms.saveRMS(mSystem.getPathRMS(path), dataImg);
				}else if(subImg==1){
					int sizeImg  = msg.reader().readByte();
					//Cout.println(subImg+ "REQUEST_IMAGE sizeImg  "+sizeImg);
					
					for (int i = 0; i < sizeImg; i++) {
						int lengread = msg.reader().readInt();
						
						sbyte[] dataImg = Util.readByteArray(msg,lengread);
						string path = SmallImage.getPathImage(idimg)+""+idimg+"/_"+(sizeImg==2?(i==0?1:4):(i+1));
						//Cout.println(dataImg.Length+ "  REQUEST_IMAGE path  "+path);
						Rms.saveRMS(mSystem.getPathRMS(path), dataImg);
					}
				}
				break;
			case Cmd.PLAYER_MOVE: // char khac move lai kt
				sbyte typemove = msg.reader().readByte();
				try {
				if(typemove == Const.CAT_MONSTER){
					short idmobb = msg.reader().readShort();
					short idmobx = msg.reader().readShort();
					short idmoby = msg.reader().readShort();
					string namemobb = msg.reader().readUTF();       
					int hpmob = msg.reader().readInt();
					for (int i = 0; i < GameScr.vMob.size(); i++) {
						Mob mobb = (Mob)GameScr.vMob.elementAt(i);
						if(mobb.mobId==idmobb&&mobb.status==Mob.MA_INHELL)
						{
							//Cout.println("moveeee mob  "+mobb.mobId);
							mobb.injureThenDie = false;
							mobb.status = Mob.MA_WALK;
							mobb.x = idmobx;
							mobb.y = idmoby-10;
							mobb.mobName = namemobb;
							ServerEffect.addServerEffect(37, mobb.x, mobb.y-mobb.getH()/4-10, 1);
							mobb.hp = hpmob>mobb.maxHp?mobb.maxHp:hpmob;
						}
					}
//		            m.dos.writeShort(a.id);
//		            m.dos.writeShort(a.x);
//		            m.dos.writeShort(a.y);
//		            m.dos.writeUTF(a.getName(language));
					
				}else if(typemove == Const.CAT_PLAYER){
					
					int charId = msg.reader().readShort();

					short cxMoveLast = msg.reader().readShort();
					short cyMoveLast = msg.reader().readShort();
					string cName = msg.reader().readUTF();
					sbyte typePk = msg.reader().readByte();
					for (int i = 0; i < GameScr.vCharInMap.size(); i++)
					{						
						c = (Char) GameScr.vCharInMap.elementAt(i);				
						if (c.charID == charId)
						{
							c.cxMoveLast =cxMoveLast;
							c.cyMoveLast = cyMoveLast;
							c.cName =cName;
							c.typePk =typePk;
							
							c.moveTo(c.cxMoveLast, c.cyMoveLast);						
							c.lastUpdateTime = mSystem.currentTimeMillis();
						}
					}
				}
				}catch (Exception e) {
					//e.printStackTrace();
				}
				
				break;
			case Cmd.CHANGE_MAP:
                    GameScr.gI().guiMain.moveClose = false;
					GameScr.gI().guiMain.menuIcon.indexpICon =0;
					MenuIcon.isShowTab = false;
					GameScr.vNpc.removeAllElements();
					TileMap.vGo.removeAllElements();
					GameScr.vMob.removeAllElements();
					GameScr.vItemMap.removeAllElements();
					BgItem bgi = new BgItem();
					EffectData effdata = new EffectData();
//					effdata.readData("/map/data");
					Char.ischangingMap = true;
					GameScr.vCharInMap.removeAllElements();
					GameScr.vCharInMap.addElement(Char.myChar());
					TileMap.mapID = msg.reader().readShort();
					Effect2.vEffect2.removeAllElements();
					TileMap.mapName = (String)TileMap.listNameAllMap.get(TileMap.mapID+"");
					TileMap.zoneID = msg.reader().readByte();
					short vsize = msg.reader().readShort();
					//Cout.println("SIZE LINK MAP -----> "+vsize);
					for (int i = 0; i < vsize; i++){
						TileMap.vGo.addElement(new Waypoint(msg.reader().readShort(),msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort()));
						
					}
					Char.myChar().cx = msg.reader().readShort();
					Char.myChar().cy = msg.reader().readShort();
					Char.myChar().statusMe = Char.A_FALL;
					TileMap.tileID = msg.reader().readByte();	
					// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
					sbyte nTile = msg.reader().readByte();
					TileMap.tileIndex = new int[nTile][][];
					TileMap.tileType = new int[nTile][];
//					bgi.loadMapItem();
				
					//bgi.loadMaptable();
					//bgi.loadImgmap();

					for (int i = 0; i < nTile; i++) {
						sbyte nTypeSize = msg.reader().readByte();
						TileMap.tileType[i] = new int[nTypeSize];
						TileMap.tileIndex[i] = new int[nTypeSize][];
						for (int a = 0; a < nTypeSize; a++) {
							TileMap.tileType[i][a] = msg.reader().readInt();
							sbyte sizeIndex = msg.reader().readByte();
							TileMap.tileIndex[i][a] = new int[sizeIndex];
							for (int b = 0; b < sizeIndex; b++) {
								TileMap.tileIndex[i][a][b] = msg.reader()
										.readByte();
							}
						}
					}
					TileMap.loadimgTile(TileMap.tileID); // load hinh tile
					TileMap.loadMapfile(TileMap.mapID); // load file map
					TileMap.loadMap(TileMap.tileID); // load va cham 
					
					GameScr.loadMapItem(); // load toan bo file data object
					GameScr.loadMapTable(TileMap.mapID); // object trong 1 map filedata
					TileMap.vItemBg.removeAllElements();
					BgItem.imgPathLoad.clear();
					Mob.imgMob.clear();
					int[] idObjMap = new int [TileMap.vCurrItem.size()];
					for (int i = 0; i < TileMap.vCurrItem.size(); i++) {
						BgItem biMap = (BgItem)TileMap.vCurrItem.elementAt(i);
						if(biMap!=null){
							idObjMap[i] = biMap.idImage;
						}
					}
					sbyte sizeMod = msg.reader().readByte();
				//	System.out.println("SIZE MOB ---> "+sizeMod);
					Mob.arrMobTemplate = new MobTemplate[sizeMod];
					for(int i = 0; i < Mob.arrMobTemplate.Length; i++){
						Mob.arrMobTemplate[i] = new MobTemplate();
//						Mob.arrMobTemplate[i].imgs = null;
						sbyte idModtemp = msg.reader().readByte();
						short idloadmob = msg.reader().readShort();
						Mob.arrMobTemplate[i].idloadimage = idloadmob;
//						Mob.arrMobTemplate[i].data = new EffectData(); // doc file data quai trong tool
//						Mob.arrMobTemplate[i].data.readData("/x"+mGraphics.zoomLevel+"/mob/"+idloadmob+"/data");
//						effdata.readData("/x"+mGraphics.zoomLevel+"/mob/"+idloadmob+"/data");
						Mob.loadImgMob(idloadmob);
//						Mob.arrMobTemplate[i].data.img = Mob.loadImgMob(idloadmob);
						
						Mob mob = new Mob(idModtemp, msg.reader().readInt(), msg.reader().readByte(),msg.reader().readInt(),
								msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort(),msg.reader().readShort(),
								msg.reader().readShort());

						mob.idloadimage = idloadmob;
						GameScr.vMob.addElement(mob);
					}
					
//					Npc.arrNpcTemplate = new NpcTemplate[msg.reader().readByte()];
//
//					for (byte i = 0; i < Npc.arrNpcTemplate.length; i++) {
//						Npc.arrNpcTemplate[i] = new NpcTemplate();
//						Npc.arrNpcTemplate[i].npcTemplateId = i;
//						Npc.arrNpcTemplate[i].name = msg.reader().readUTF();
//						Npc.arrNpcTemplate[i].headId = msg.reader().readShort();
//						Npc.arrNpcTemplate[i].bodyId = msg.reader().readShort();
//						Npc.arrNpcTemplate[i].legId = msg.reader().readShort();
//						Npc.arrNpcTemplate[i].contendChat = msg.reader().readUTF();
////						Npc.arrNpcTemplate[i].menu = new String[msg.reader().readByte()][];
////						for (int j = 0; j < Npc.arrNpcTemplate[i].menu.length; j++) {
////							Npc.arrNpcTemplate[i].menu[j] = new String[msg.reader().readByte()];
////							for (int j2 = 0; j2 < Npc.arrNpcTemplate[i].menu[j].length; j2++)
////								Npc.arrNpcTemplate[i].menu[j][j2] = msg.reader().readUTF();
////						}
//					}
				
					
					
//					GameScr.gI().loadGameScr();
					Service.gI().requestinventory();
					if(GameCanvas.currentScreen != GameCanvas.gameScr){
						GameCanvas.gameScr =  new GameScr();
//						GameCanvas.gameScr.switchToMe();// mo man hÃ¬nh gamescr
					}
					DownloadImageScreen.gI().switchToMe(SmallImage.ID_ADD_MAPOJECT, idObjMap);
					//XinChoScreen.gI().switchToMe();
					Char.ischangingMap = false;
					Char.myChar().statusMe = Char.A_FALL;
					GameScr.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
					GameCanvas.endDlg();

					Music.play(CRes.random(1, 4), 0.3f);
				break;
			case Cmd.ITEM:
				try{

					GameScr.currentCharViewInfo = Char.myChar();
					sbyte type = msg.reader().readByte();
					if(type == ItemTemplate.ITEM_TEMPLATE){
						short nItemTemplate = msg.reader().readShort(); // sá»‘ template ID
						Cout.println("  nItemTemplate  "+nItemTemplate);
						for(int i = 0; i < nItemTemplate; i++){
							short templateID = msg.reader().readShort(); // id temp
							sbyte typeItem = msg.reader().readByte(); // loai
							sbyte gender = msg.reader().readByte(); //  gioi tinh
							string name = msg.reader().readUTF(); // ten 
							string des = msg.reader().readUTF(); // mo ta
							sbyte level = msg.reader().readByte(); // level
							short iconID = msg.reader().readShort(); // idIcon
							bool isUptoUp = msg.reader().readbool(); // cÃ³ nÃ¢ng cáº¥p Ä‘c k 
							long giaitem = msg.reader().readLong();
							sbyte clazz = msg.reader().readByte();
							sbyte quocgia = msg.reader().readByte();
							short part = msg.reader().readShort(); // part cÆ¡ báº£n cháº¯c cháº¯n tháº±ng nÃ o cÅ©ng pháº£i cÃ³
							short partquan = -1,partdau=-1;
//							ItemTemplate itt = new ItemTemplate(msg.reader().readShort(), msg.reader().readByte(),  msg.reader().readByte(),  msg.reader().readUTF(),  msg.reader().readUTF(),  msg.reader().readByte(),  msg.reader().readShort(),  msg.reader().readShort(),  msg.reader().readBoolean());
							if(typeItem == Item.TYPE_AO){ // Ã¡o Ä‘á»c thÃªm 1 part ná»¯a lÃ  part quáº§n
								partquan = msg.reader().readShort(); 
								partdau = msg.reader().readShort(); 
								
							}
							ItemTemplate itt = null;
							if(typeItem != Item.TYPE_AO)
								itt = new ItemTemplate(templateID, typeItem,  gender,  name,  des,  level,  iconID,  part,  isUptoUp);
							else{
								itt = new ItemTemplate(templateID, typeItem,  gender,  name,  des,  level,  iconID,  part, partquan,partdau,  isUptoUp);
							}
							itt.despaint = mFont.tahoma_7_white.splitFontArray(des, 140);
							itt.gia = giaitem;
							itt.clazz = clazz;
							itt.quocgia = quocgia;
							ItemTemplates.add(itt);
						}
					}else if(type == ItemTemplate.CHAR_WEARING){
						short CharID = msg.reader().readShort();
						sbyte leng = msg.reader().readByte();
						if(Char.myChar().charID != CharID){
							bool isAdded = false;
							for (int i = 0; i < GameScr.vCharInMap.size(); i++) {
								Char dem = (Char)GameScr.vCharInMap.elementAt(i);
								if(dem!=null&&dem.charID==CharID){
									isAdded = true;
									c = dem;
									Cout.println("LOIIIIIIIIIIIIIIIIIIIII ADD THEM CHAR");
								}
							}
							if(!isAdded){
								c = new Char();
								c.charID = CharID;
							}
							readcharInmap(c,msg);
							if(!isAdded)
							GameScr.vCharInMap.addElement(c);
							Cout.println("  char.Id CHAR_WEARING add new char"+GameScr.vCharInMap.size());
						}else{
							Cout.println("leng ----> "+leng);
							
							Char.myChar().arrItemBody = new Item[leng];
							try {
								for (int i = 0; i < Char.myChar().arrItemBody.Length; i++) {
									short idtemplate = msg.reader().readShort();
										if(idtemplate != -1){
										//	Cout.println(" KHAC -1 ----->");
											ItemTemplate template = ItemTemplates.get(idtemplate);
				
											int indexUI = Item.getPosWearingItem(template.type);
											
											
											
//											GameScr.currentCharViewInfo.arrItemBody[indexUI] = new Item();
//										
//											GameScr.currentCharViewInfo.arrItemBody[indexUI].indexUI = indexUI;
//											
//											GameScr.currentCharViewInfo.arrItemBody[indexUI].typeUI = Item.UI_BODY;
//											GameScr.currentCharViewInfo.arrItemBody[indexUI].template = template;
//											GameScr.currentCharViewInfo.arrItemBody[indexUI].isLock = true;
											Char.myChar().arrItemBody[indexUI] = new Item();
											
											Char.myChar().arrItemBody[indexUI].indexUI = indexUI;
											
											Char.myChar().arrItemBody[indexUI].typeUI = Item.UI_BODY;
											Char.myChar().arrItemBody[indexUI].template = template;
											Char.myChar().arrItemBody[indexUI].isLock = true;
			//								GameScr.currentCharViewInfo.arrItemBody[indexUI].upgrade = msg.reader().readByte();
			//								GameScr.currentCharViewInfo.arrItemBody[indexUI].sys = msg.reader().readByte();

//											Char.myChar().leg = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.partquan;
											if (template.type == Item.TYPE_AO){
//												Char.myChar().leg = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.partquan;
												Char.myChar().body = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.part;
												Char.myChar().leg = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.partquan;
												Char.myChar().head = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.partdau;
												Cout.println(Char.myChar().leg+"head ----> "+Char.myChar().head);
												
											}
//											else if (template.type == Item.TYPE_LEG){
//												
//												Char.myChar().leg = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.part;
//												Cout.println("LEG ----> "+Char.myChar().leg);
//
//											}
											else if (template.type == Item.TYPE_NON){
												Char.myChar().head = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.part;
											}
									
									}
									
								}
//							Char.myChar().head = charWearing[0];
								
								
							} catch (Exception e) {
								//e.printStackTrace();
							}
						
					}
//						Char.myChar().charID = msg.reader().readShort();
//						byte lengtharrbody = msg.reader().readByte();
//						short charWearing[] = new short[lengtharrbody];
//						for(int i = 0; i < lengtharrbody; i++ ){
//							short idItemTemplate = msg.reader().readShort();
//							if(idItemTemplate != -1){
//								charWearing[i] = msg.reader().readShort();
//							}
//								
//						}
//						Char.myChar().head = charWearing[0];
//						Char.myChar().body = charWearing[4];
//						Char.myChar().leg = charWearing[11];
						         
					}
				}catch(Exception e){
					//e.printStackTrace();
				}
				
				                        
				
				break;        
				
				break;
			case Cmd.CHAR_INFO:
                //				Cout.println("Char_infoooooooooooooooooooooooooooooooooooo");
                Char.myChar().charID = msg.reader().readShort();
                //				Cout.println("Char_infooooooo  "+Char.myChar().charID);
                Char.myChar().cName = msg.reader().readUTF();
                //				Cout.println("Char_infooooooo  "+Char.myChar().cName);
                Char.myChar().cgender = msg.reader().readByte();
                short clevel2 = msg.reader().readShort();
                if (Char.myChar().clevel > 0 && Char.myChar().clevel < clevel2)
                {
                    Music.play(Music.LENLV, 10);
                    ServerEffect.addServerEffect(22, Char.myChar().cx, Char.myChar().cy, 1);
                }
                Char.myChar().clevel = clevel2;
                Char.myChar().cEXP = msg.reader().readByte();
                //				m.dos.writeByte(p.lvDetail.getPercent()); 
                Char.myChar().cHP = msg.reader().readInt();

                Cout.println(Char.myChar().cName + "  CHAR_INFO Char.myChar().cHP " + Char.myChar().cHP);
                //				Cout.println("Char_infooooo  "+Char.myChar().cHP);
                Char.myChar().cMaxHP = msg.reader().readInt();

                //				Cout.println(Char.myChar().cHP+"Char_infoo maxhp"+Char.myChar().cMaxHP );
                Char.myChar().cMP = msg.reader().readInt();
                Cout.println(Char.myChar().cHP + "Char.myChar().cMP" + Char.myChar().cMP);
                Char.myChar().cMaxMP = msg.reader().readInt();
                //				Cout.println(Char.myChar().cMP+"Char_infoo maxhp"+Char.myChar().cMaxMP);
                //Char.myChar().totalTN = msg.reader().readInt();
                Char.myChar().diemTN = null;
                Char.myChar().diemTN = new int[8];
                Char.myChar().diemTN[0] = msg.reader().readInt();
                Char.myChar().diemTN[1] = msg.reader().readInt();
                Char.myChar().diemTN[2] = msg.reader().readInt();
                Char.myChar().diemTN[3] = msg.reader().readInt();
                Char.myChar().diemTN[4] = msg.reader().readInt();
                Char.myChar().diemTN[5] = msg.reader().readInt();
                Char.myChar().diemTN[6] = msg.reader().readInt();
                Char.myChar().diemTN[7] = msg.reader().readInt();
                Char.myChar().totalTN = msg.reader().readInt();
                //				Cout.println(" Char.myChar().totalTN "+Char.myChar().totalTN);

                //gui ve thong tin sub
                short sizeSub = msg.reader().readShort();
                Char.myChar().subTn = new int[sizeSub];
                //				Cout.println(" sizeSub "+sizeSub);
                for (int i = 0; i < sizeSub; i++)
                {
                    sbyte indexMon = msg.reader().readByte();
                    int valueSub = msg.reader().readInt();
                    //					Cout.println(indexMon+" sizeSub "+valueSub);
                    if (indexMon < Char.myChar().subTn.Length)
                        Char.myChar().subTn[indexMon] = valueSub;
                }
                if (GameScr.findCharInMap((short)Char.myChar().charID) == null)
                    GameScr.vCharInMap.addElement(Char.myChar());
                break;
			case Cmd.CHAR_LIST:
                //Cout.println(getClass(), "  nhan ve charlisst "+Cmd.CHAR_LIST);
                sbyte sizeChar = msg.reader().readByte();
                //Cout.println(getClass(),sizeChar+ "  nhan ve charlisst "+Cmd.CHAR_LIST);
                LoginScr.isLoggingIn = false;
                //				LoginScr.selectCharScr = new SelectCharScr();
                SelectCharScr.gI().initSelectChar();
                //				LoginScr.selectCharScr.switchToMe();
                GameCanvas.endDlg();
                if (sizeChar > 0)
                {
                    //					LoginScr.selectCharScr = new SelectCharScr();
                    // dÃ¹ng biáº¿n riÃªng mÃ n hÃ¬nh login 
                    //					//Char[] c = new Char[sizeChar];

                    for (int i = 0; i < sizeChar; i++)
                    {

                        //						c[i] = new Char();
                        //						c[i].charIdDB = msg.reader().readInt();
                        SelectCharScr.gI().charIDDB[i] = msg.reader().readInt();
                        //Cout.println("id----> "+SelectCharScr.gI().charIDDB[i]);
                        SelectCharScr.gI().name[i] = msg.reader().readUTF();
                        Cout.println("name----> " + SelectCharScr.gI().name[i]);
                        SelectCharScr.gI().gender[i] = msg.reader().readByte();
                        //Cout.println("gender----> "+SelectCharScr.gI().gender[i]);
                        SelectCharScr.gI().type[i] = msg.reader().readByte();
                        Cout.println("SelectCharScr.gI().type[i] ----> " + SelectCharScr.gI().type[i]);
                        SelectCharScr.gI().lv[i] = msg.reader().readShort();
                        //Cout.println("SelectCharScr.gI().lv[i] ----> "+SelectCharScr.gI().lv[i]);
                        sbyte sizePart = msg.reader().readByte();
                        Cout.println("sizePart ----> " + sizePart);
                        SelectCharScr.gI().parthead[i] = msg.reader().readShort();
                        Cout.println("PARTHEAD ----> " + SelectCharScr.gI().parthead[i]);


                        SelectCharScr.gI().partbody[i] = msg.reader().readShort();
                        Cout.println("PARTBODY ---> " + SelectCharScr.gI().partbody[i]);

                        //System.out.println("Headpart "+msg.reader().readShort());
                        SelectCharScr.gI().partleg[i] = msg.reader().readShort();
                        Cout.println("PARTLEG ----> " + SelectCharScr.gI().partleg[i]);

                        //						if (SelectCharScr.gI().partWp[i] == -1)
                        //							SelectCharScr.gI().partWp[i] = 15;
                        //						if (SelectCharScr.gI().partbody[i] == -1) {
                        //							if (SelectCharScr.gI().gender[i] == 0)
                        //								SelectCharScr.gI().partbody[i] = 10;
                        //							else
                        //								SelectCharScr.gI().partbody[i] = 1;
                        //						}
                        //						if (SelectCharScr.gI().partleg[i] == -1) {
                        //							if (SelectCharScr.gI().gender[i] == 0)
                        //								SelectCharScr.gI().partleg[i] = 9;
                        //							else
                        //								SelectCharScr.gI().partleg[i] = 0;
                        //						}
                        //						SelectCharScr.gI().partWp[i] = msg.reader().readShort();
                        //						System.out.println("size part --> "+sizePart);
                        ////						for(int j = 0; j < sizePart; j++){
                        //							c[i].leg = msg.reader().readShort();
                        //							c[i].body = msg.reader().readShort();
                        //							c[i].head = msg.reader().readShort();
                        //							c[i].wp = msg.reader().readShort();
                        ////						}
                    }
                    DownloadImageScreen.gI().switchToMe();
                    GameCanvas.endDlg();
                }
                else
                {
                    DownloadImageScreen.gI().switchToMe(1);
                    GameCanvas.endDlg();
                }


                break;
			case Cmd.LOGIN:
//				short lengtdata = msg.reader().readShort();
//				System.out.println("LENGTDATA ---> "+lengtdata);
//				short sizebyte = msg.reader().readShort();
                createData(new DataInputStream(msg.reader()));
//				createDataEff(msg.reader());
//				createDataImage(msg.reader());
//				createDataPart(msg.reader());
//				createDataKill(msg.reader());

				// SAVE RMS NEW DATA
				msg.reader().reset();
				sbyte[] newData = new sbyte[msg.reader().available()];
				msg.reader().readFully(ref newData);
				// SAVE RMS NEW DATA VERSION
//				sbyte[] newDataVersion = new byte[] { GameScr.vcData };
//				Rms.saveRMS("dataVersion", newDataVersion);
//				 =========================
//				LoginScr.isUpdateData = false;
//				if (GameScr.vsData == GameScr.vcData
//						&& GameScr.vsMap == GameScr.vcMap
//						&& GameScr.vsSkill == GameScr.vcSkill
//						&& GameScr.vsItem == GameScr.vcItem) {
//					System.out.println(GameScr.vsData + "," + GameScr.vsMap
//							+ "," + GameScr.vsSkill + "," + GameScr.vsItem);
					//GameScr.gI().readDart();
					GameScr.readArrow();
					GameScr.readEfect();// doc ef
					SmallImage.readImage();
					GameScr.readPart();
					GameScr.readSkill();
//					Service.gI().clientOk();
//					return;
//				}
				break;
			case Cmd.DIALOG:
				sbyte typeDialog = msg.reader().readByte();
				Cout.println("typeDialog  "+typeDialog);
				if(typeDialog == 0){
					//  ok
					GameCanvas.startOKDlg( msg.reader().readUTF());
				}
				else if(typeDialog == 1){
					GameCanvas.startYesNoDlg(msg.reader().readUTF(),new Command("Yes", GameScr.gI(), 1, null),
							new Command("No", GameScr.gI(), 0, null));
				}else if(typeDialog == 2){
					GameCanvas.startDlgTime(msg.reader().readUTF(),new Command("Yes", GameCanvas.instance, 8882, null),
							msg.reader().readInt());
				}else if(typeDialog == 3){//tren dau
					GameCanvas.StartDglThongBao( msg.reader().readUTF());
				}
				else if(typeDialog == 4){//ben goc phai
					GameCanvas.StartDglThongBao( msg.reader().readUTF());
				}
				break;
			case Cmd.NPC_TEAMPLATE:
					int lengg = msg.reader().readByte();

				for (byte i = 0; i < lengg; i++) {
					NpcTemplate npctem = new NpcTemplate();
					npctem.npcTemplateId = msg.reader().readShort();
					npctem.name = msg.reader().readUTF();
					npctem.headId = msg.reader().readShort();
					npctem.bodyId = msg.reader().readShort();
					npctem.legId = msg.reader().readShort();
					npctem.contendChat = msg.reader().readUTF();
					if(npctem.headId==-1&&npctem.bodyId==-1&&npctem.legId==-1)
						npctem.typeKhu = -1;
					npctem.idavatar = msg.reader().readShort();//avatar
					Npc.arrNpcTemplate.put(npctem.npcTemplateId+"",npctem);
					///*Npc.arrNpcTemplate[i]. = */msg.reader().readShort();
//					Npc.arrNpcTemplate[i].menu = new String[msg.reader().readByte()][];
//					for (int j = 0; j < Npc.arrNpcTemplate[i].menu.length; j++) {
//						Npc.arrNpcTemplate[i].menu[j] = new String[msg.reader().readByte()];
//						for (int j2 = 0; j2 < Npc.arrNpcTemplate[i].menu[j].length; j2++)
//							Npc.arrNpcTemplate[i].menu[j][j2] = msg.reader().readUTF();
//					}
				}
			
				break;
			case Cmd.MAP_TEAMPLATE:
				int sizeee = msg.reader().readShort();
				for (int i = 0; i < sizeee; i++) {
					short idmap = msg.reader().readShort();
					short idtile = msg.reader().readShort();
					string name = msg.reader().readUTF();
					TileMap.listNameAllMap.put(idmap+"", name);
				}
				break;
            case Cmd.CHAR_SKILL_STUDIED:
                short dsSkillDaHoc = msg.reader().readShort();
                Cout.println("CHAR_SKILL_STUDIED size  " + dsSkillDaHoc);
                for (int i = 0; i < dsSkillDaHoc; i++)
                {
                    sbyte idskill = msg.reader().readByte();
                    if (i == 0)
                        QuickSlot.idSkillCoBan = idskill;
                    short idtem = msg.reader().readShort();//icon skill tron
                    short levelskill = msg.reader().readShort();

                    if (TabSkill.skillIndex != null && idskill == TabSkill.skillIndex.id)
                    {
                        if (TabSkill.skillIndex.level == -1 && levelskill > -1)
                        {
                            TabSkill.skillIndex.level = levelskill;
                            TabSkill.isDaHoc = true;
                        }
                        TabSkill.skillIndex.level = levelskill;
                    }
                    SkillTemplate skill = (SkillTemplate)SkillTemplates.hSkilltemplate.get("" + idskill);

                    skill.level = (short)(levelskill);
                    if (skill.level >= skill.nlevelSkill)
                        skill.level = (short)(skill.nlevelSkill - 1);

                }

                QuickSlot.loadRmsQuickSlot();
                break;
            case Cmd.UPDATE_CHAR: //UPDATE_BLOOD = 0;//UPDATE_EXP = 1;//su dung tang exp/// UPDATE_HP = 2; //UPDATE_MP = 3;
                //Cout.println("update charrr ");
                short idchar = msg.reader().readShort();
                sbyte typee = msg.reader().readByte();
                //Cout.println(type+" update charrr  "+idchar);
                Char ccharupdate = GameScr.findCharInMap(idchar);
                if (ccharupdate == null) return;
                switch (typee)
                {
                    case 0:
                        sbyte sizeThucAn = msg.reader().readByte();
                        GuiMain.vecItemOther.removeAllElements();
                        for (int i = 0; i < sizeThucAn; i++)
                        {
                            short idtemplate = msg.reader().readShort();
                            short time = msg.reader().readShort();
                            //Cout.println("time  "+time);
                            GuiMain.vecItemOther.add(new ItemThucAn((short)i,
                                    (sbyte)0, idtemplate, time, 100));
                        }
                        //					msg.dos.writeByte(p.getListItemsBuff().size());
                        //                    for (int i = 0; i < p.getListItemsBuff().size(); i++) {
                        //                        //id template
                        //                        msg.dos.writeShort(p.getListItemsBuff().get(i).idTemplate);
                        //                        //thá»i gian buff . Ä‘Æ¡n vá»‹ tÃ­nh báº±ng giay
                        //                        msg.dos.writeShort((int) (p.getListItemsBuff().get(i).getTimeBuff() / Actor.SECOND));
                        //                    }
                        break;
                    case 1:
                        Char.myChar().cEXP = msg.reader().readByte();
                        Char.myChar().totalTN = msg.reader().readInt();
                        break;
                    case 2: //hp
                        ccharupdate.cMaxHP = msg.reader().readInt(); //maxhp
                        ccharupdate.cHP = msg.reader().readInt(); //hp
                        int hpcong = msg.reader().readInt(); //hp cong them
                        GameScr.startFlyText("+" + hpcong, ccharupdate.cx, ccharupdate.cy - 50, 0, -2, mFont.RED);
                        break;
                    case 3: //hp
                        ccharupdate.cMaxMP = msg.reader().readInt(); //maxhp
                        ccharupdate.cMP = msg.reader().readInt(); //hp
                        int mpcong = msg.reader().readInt(); //hp cong them
                        GameScr.startFlyText("+" + mpcong, ccharupdate.cx, ccharupdate.cy - 50, 0, -2, mFont.GREEN);
                        break;
                }
                break;
            case Cmd.COME_HOME_DIE:
                sbyte typehs = msg.reader().readByte();
                switch (typehs)
                {
                    case 0://ve lang
                        Char.myChar().statusMe = Char.A_FALL;
                        Char.myChar().cHP = msg.reader().readShort();
                        Char.myChar().cMP = msg.reader().readShort();
                        break;

                    case 1://hs tai cho
                        short idcharhs = msg.reader().readShort();
                        Char chs = GameScr.findCharInMap(idcharhs);

                        if (chs != null)
                        {
                            ServerEffect.addServerEffect(34, chs.cx, chs.cy, 3);
                            chs.statusMe = Char.A_FALL;
                            chs.cHP = msg.reader().readShort();
                            chs.cMP = msg.reader().readShort();
                        }
                        break;
                }
                break;
            case Cmd.REGISTER:
                bool isDK = msg.reader().readbool();
                string textt = msg.reader().readUTF();
                GameCanvas.startOK(textt, 8882, null);
                break;
            case Cmd.XP_CHAR:
                short idc = msg.reader().readShort();
                int xp = msg.reader().readInt();//m.dos.writeInt((int) dxp);
                Char acc = GameScr.findCharInMap(idc);
                if (acc != null)
                {
                    GameScr.startFlyText("+" + xp + "exp",
                            acc.cx, acc.cy - acc.ch - 5, 0, -2, mFont.YELLOW);

                }
                break;
            case Cmd.BUY_ITEM_FROM_SHOP:
                Cout.println("nhan ve BUY_ITEM_FROM_SHOP ");
                break;
            case Cmd.REQUEST_REGION:
                ThongTinKhu(msg);
                break;
            case Cmd.CHANGE_REGION:
                ChangeKhu(msg);
                break;
            case Cmd.COMPETED_ATTACK:
                ThachDau(msg);
                break;
            case Cmd.STATUS_ATTACK:
                StatusAttack(msg);
                break;
            case Cmd.SERVER_CHAT:
                string m = msg.reader().readUTF();
                GameScr.listInfoServer.add(new InfoServer("Admin" + ": " + m));
                //GameCanvas.StartDglThongBao(m);
                break;

            }
			
		}catch(Exception e){
			//System.out.println("Error AT ----> "+msg.command);
			//e.printStackTrace();
		}
		finally{
			if(msg != null)
				msg.cleanup();
		}
		
	}
    public void ThongTinKhu(Message msg){
		try {

			Cout.println("ThongTinKhu");
			sbyte nkhu = msg.reader().readByte();
			Cout.println("ThongTinKhu  "+nkhu);
			KhuScreen.gI().listKhu = new sbyte[nkhu][];
            for (int i = 0; i < nkhu; i++)
            {
                KhuScreen.gI().listKhu[i] = new sbyte[1];
            }
			for (int i = 0; i < nkhu; i++) {
				KhuScreen.gI().listKhu[i][0] = msg.reader().readByte();
			}

			Cout.println("ThongTinKhu KhuScreen ");
			KhuScreen.gI().srclist.selectedItem = -1;
			KhuScreen.gI().switchToMe();
		} catch (Exception e) {
			// TODO: handle exception
			//Cout.println("ThongTinKhu KhuScreen e"+e.ToString());
		}
	}
    public void ThachDau(Message msg)
    {
        try
        {
            sbyte type = msg.reader().readByte();
            short idchar = msg.reader().readShort();
            string loimoi = msg.reader().readUTF(); 
            GameCanvas.startYesNoDlg(loimoi, new Command("Đồng ý", GameCanvas.instance, GameCanvas.cDongYThachDau, idchar + ""),
                     new Command("Không", GameCanvas.instance, 8882, null));
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }
    public void StatusAttack(Message msg)
    {
        try
        {
            short idchar = msg.reader().readShort();
            sbyte typeAttack = msg.reader().readByte();
            if (Char.myChar().charID == idchar)
            {
                FlagScreen.time = mSystem.currentTimeMillis();
            }
            Char ch = GameScr.findCharInMap(idchar);
            if (ch != null) ch.typePk = typeAttack;
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }
    public void ChangeKhu(Message msg)
    {
        try
        {
            TileMap.zoneID = msg.reader().readByte();
            Char.myChar().cx = msg.reader().readShort();
            Char.myChar().cy = msg.reader().readShort();

            //			GameScr.vMob.removeAllElements();
            for (int i = 0; i < GameScr.vMob.size(); i++)
            {
                Mob dem = (Mob)GameScr.vMob.elementAt(i);
                dem.ResetToDie();
            }
            GameScr.vItemMap.removeAllElements();
            GameScr.vCharInMap.removeAllElements();
            //Cout.println(GameScr.vCharInMap.size() + "  ChangeKhu size npc ==" + GameScr.vNpc.size());
            XinChoScreen.isChangeKhu = true;
            XinChoScreen.gI().switchToMe();
            GameScr.vCharInMap.add(Char.myChar());
            Char.ischangingMap = false;
            Char.myChar().statusMe = Char.A_FALL;
            GameScr.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
            GameCanvas.endDlg();

        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }
		private void createData(DataInputStream d) {
			try{
				Rms.saveRMS("nj_arrow", Util.readByteArray(d));
				Rms.saveRMS("nj_effect", Util.readByteArray(d));
				Rms.saveRMS("nj_image", Util.readByteArray(d));
				Rms.saveRMS("nj_part", Util.readByteArray(d));
				Rms.saveRMS("nj_skill", Util.readByteArray(d));
			}catch(Exception e){
			//	e.printStackTrace();
			}
//		GameScr.vcData = d.readByte();
		
		}
		
		public void readcharInmap(Char c, Message msg){ // add player trong map
			try{
			
//				c.charID = msg.reader().readShort();
//				sbyte lengtharrbody = msg.reader().readByte();
				c.arrItemBody = new Item[13];
				
					for (int i = 0; i < c.arrItemBody.Length; i++) {
						short idtemlate = msg.reader().readShort();
							if(idtemlate != -1){
								ItemTemplate template = ItemTemplates.get(idtemlate);
								int indexUI = Item.getPosWearingItem(template.type);
								
								
								c.arrItemBody[indexUI] = new Item();
							
								c.arrItemBody[indexUI].indexUI = indexUI;
								
								c.arrItemBody[indexUI].typeUI = Item.UI_BODY;
								c.arrItemBody[indexUI].template = template;
								c.arrItemBody[indexUI].isLock = true;
								
//								GameScr.currentCharViewInfo.arrItemBody[indexUI].upgrade = msg.reader().readByte();
//								GameScr.currentCharViewInfo.arrItemBody[indexUI].sys = msg.reader().readByte();
							
								if (template.type == Item.TYPE_AO){
									
									c.body = c.arrItemBody[indexUI].template.part;
                                    c.leg = c.arrItemBody[indexUI].template.partquan; 
                                    c.head = c.arrItemBody[indexUI].template.partdau;
							
								}
//								else if (template.type == Item.TYPE_LEG){
//									c.leg = c.arrItemBody[indexUI].template.part;
//								}
								else if (template.type == Item.TYPE_NON){
									c.head = c.arrItemBody[indexUI].template.part;
								}
						
						}
						
					}
			
				//return true;
			}catch (Exception e) {
				//e.printStackTrace();
				// TODO: handle exception
			}
			//return false;
		}
		
		private mBitmap createImage(sbyte[] arr) {
			try {
				return new mBitmap(Image.createImage(arr, 0, arr.Length));
			} catch (Exception e) {
				// TODO: handle exception
			}
			return null;
		}
		
		private void createItem(DataInputStream d) {
            try 
	        {
                GameScr.vcItem = d.readByte();
                GameScr.iOptionTemplates = new ItemOptionTemplate[d.readByte()];

                for (int i = 0; i < GameScr.iOptionTemplates.Length; i++)
                {
                    GameScr.iOptionTemplates[i] = new ItemOptionTemplate();
                    GameScr.iOptionTemplates[i].id = i;
                    GameScr.iOptionTemplates[i].name = d.readUTF();
                    GameScr.iOptionTemplates[i].type = d.readByte();
                }
                int nItemTemplate = d.readShort();
                for (int i = 0; i < nItemTemplate; i++)
                {
                    ItemTemplate itt = new ItemTemplate((short)i, d.readByte(), d.readByte(), d.readUTF(), d.readUTF(), d.readByte(), d.readShort(), d.readShort(), d.readbool());
                    //System.out.println(itt.id+", "+itt.name);
                    ItemTemplates.add(itt);
                }
	        }
	        catch (Exception)
	        {
		
		        throw;
	        }
		}
		
	
	
}

