

using System;
public class LanguageScr : Screen , IActionListener {
	int popupW, popupH, popupX, popupY;
	static LanguageScr gi;
	int indexRow = -1;
	public static bool isFromLogin = false;
	public static Command cmdEng, cmdVn;
	public LanguageScr() {
		cmdEng = new Command("English", 1);
		cmdEng.setPos(GameCanvas.w/2 - 120 + 5, GameCanvas.h/2 + 20, loadImageInterface.img_use, loadImageInterface.img_use_focus);
		cmdVn = new Command("Tiếng Việt", 2);
		cmdVn.setPos(GameCanvas.w/2 + 5, GameCanvas.h/2 + 20, loadImageInterface.img_use, loadImageInterface.img_use_focus);
	}
	public override void switchToMe() {
		GameScr.gH = GameCanvas.h;
		GameCanvas.loadBG(0);

		base.switchToMe();
		if (GameScr.instance != null)
			GameScr.instance = null;

		// ==============================
		TileMap.bgID = (sbyte) (mSystem.currentTimeMillis() % 9);

		GameScr.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
		GameScr.cmx = 100;
		// ==============================
		popupW = 170;
		popupH = 175;
		if (GameCanvas.w == 128 || GameCanvas.h <= 208) {
			popupW = 126;
			popupH = 160;
		}
		popupX = GameCanvas.w/2 - popupW / 2;
		popupY = GameCanvas.h/2 - popupH / 2;
		if (GameCanvas.h <= 250)
			popupY -= 10;
		
		indexRow = -1;
		if(!GameCanvas.isTouch)
			indexRow = 0;
		
		
		
	}
	public void paint(mGraphics g) {
//		if(GameCanvas.currentScreen != GameCanvas.languageScr){
//			Paint.SubFrame(GameCanvas.w/2, GameCanvas.h/2, 200, 200, g);
//			cmdEng.paint(g);
//			cmdVn.paint(g);
//			
//		}
		
		g.setColor(0);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
		GameCanvas.paintBGGameScr(g);
//		GameCanvas.paint.paintFrame(popupX , popupY, popupW, popupH, g);
//		g.setColor(Paint.COLORDARK);
//		g.fillRoundRect(GameCanvas.hw - mFont.tahoma_8b.getWidth(mResources.LANGUAGE) / 2 - 12, popupY + 7, mFont.tahoma_8b.getWidth(mResources.LANGUAGE) + 22, 24, 6, 6);
//		g.setColor(Paint.COLORLIGHT);
//		g.drawRoundRect(GameCanvas.hw - mFont.tahoma_8b.getWidth(mResources.LANGUAGE) / 2 - 12, popupY + 7, mFont.tahoma_8b.getWidth(mResources.LANGUAGE) + 22, 24, 6, 6);
//		mFont.tahoma_8b.drawString(g, mResources.LANGUAGE, GameCanvas.hw, popupY + 12, 2);
		
		int yStart = popupY + 50;
		
		Paint.SubFrame(yStart + 35, popupW - 20, 200, 100, g);
		cmdEng.paint(g);
		cmdVn.paint(g);
		//		for (int i = 0; i < mResources.LANGUAGES.Length; i++) {
//
//			g.setColor(Paint.COLORDARK);
//			g.fillRect(popupX + 10, yStart + i * 35, popupW - 20, 28);
//			g.setColor(0xFF574949);// viền nâu
//			g.drawRect(popupX + 10, yStart + i * 35, popupW - 20, 28);
//			if(i==indexRow){
//				g.setColor(Paint.COLORLIGHT);
//				g.fillRect(popupX + 10, yStart + i * 35, popupW - 20, 28);
//				g.setColor(0xFFa8a8a8);// viền nâu
//				g.drawRect(popupX + 10, yStart + i * 35, popupW - 20, 28);
//			}
//			
//			mFont.tahoma_7b_white.drawString(g, mResources.LANGUAGES[i], popupX + popupW/2, yStart + i * 35 + 8, 2);
//		}
		
//		super.paint(g);
//		GameCanvas.paint.paintCmdBar(g, left, center, right);
		
		
	}

	
	private void saveLanguageID(int languageID) {
		Rms.saveRMSInt("indLanguage", languageID);
	}

	public override void update() {
		GameScr.cmx++;
		if (GameScr.cmx > GameCanvas.w * 3 + 100)
			GameScr.cmx = 100;
		
		base.update();
	}

	public override void updateKey() {
		if (GameCanvas.keyPressed[2] || GameCanvas.keyPressed[4] || GameCanvas.keyPressed[6] || GameCanvas.keyPressed[8])
			indexRow = indexRow == 0 ? 1 : 0;
		
		if(GameCanvas.isPointerJustRelease){
			if(GameCanvas.isPointerHoldIn(popupX + 10, popupY + 45, popupW - 10, 70)){
				if(GameCanvas.isPointerClick)
					indexRow = (GameCanvas.py -(popupY + 45))/ 35;
					perform(1000, null);
			}
		}
		if(Screen.getCmdPointerLast(cmdEng)){
			
		}
		if(Screen.getCmdPointerLast(cmdVn)){
			
		}
		base.updateKey();
		GameCanvas.clearKeyPressed();
	}

	public void perform(int idAction, Object p) {
		switch (idAction) {
		case 1:
			break;
		case 2:
			break;
		case 1000:
				GameCanvas.currentDialog = null;
				if(GameMidlet.indexClient == 2)
					mResources.languageID = (indexRow == 0) ? mResources.Lang_CAM : mResources.Lang_EN;
				else
					mResources.languageID = (indexRow == 0) ? mResources.Lang_VI : mResources.Lang_EN;
//				if(GameMidlet.indexClient == 2 && mResources.languageID == mResources.Lang_EN)
//					FontSys.reloadBitmapFont();
				saveLanguageID(mResources.languageID);
				mResources.loadLanguage();
				Rms.clearRMS();	
				GameCanvas.instance.initGameCanvas();
				GameCanvas.loginScrr.switchToMe();
			break;
		}
	}

}
