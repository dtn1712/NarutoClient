

public class DownloadImageScreen : Screen{

	public long timeCoolDownNextImage = 15000,timeStart;
    public static bool isOKNext, isDownLoadedSuccess;
    public int idNext, idTo;
	public bool isTheoList;
	public int idAdd;
	public int indexPhanTram,indexpaintload;
	public int[] listId;
	public mBitmap imgBgLoading,frameLoading,ongLoading;
	public mBitmap[] imgFrameChaLoadingr = new mBitmap[6];
	//@Override
	public override void switchToMe() {
		// TODO Auto-generated method stub
		isDownLoadedSuccess = isTheoList = false;
		idNext = 0;
		idTo=0;
		indexPhanTram = indexpaintload = 0;
		base.switchToMe();
	}
	public void switchToMe(int idScreenTo) {
		// TODO Auto-generated method stub
		isDownLoadedSuccess = isTheoList = false;
		idNext = 0;
		indexPhanTram = indexpaintload = 0;
		this.idTo = idScreenTo;
		base.switchToMe();
	}
	public void switchToMe(int idAdd,int[] listIdd) {
		// TODO Auto-generated method stub
        Cout.println("switchToMe  downnnnnnnnnnnn "+ listIdd.Length);
		this.idAdd = idAdd;
		this.listId = listIdd;
		isDownLoadedSuccess = false;
		 isTheoList = true;
		idNext = 0;
		indexPhanTram = indexpaintload = 0;
		base.switchToMe();
	}
	public bool loadingImg(){
		if(imgBgLoading==null)
			imgBgLoading = GameCanvas.loadImage("/GuiNaruto/loading/imgBgLoading.png");
		if(frameLoading==null)
			frameLoading = GameCanvas.loadImage("/GuiNaruto/loading/ongLoading.png");
		if(ongLoading==null)
			ongLoading = GameCanvas.loadImage("/GuiNaruto/loading/frameLoading.png");
		if(imgFrameChaLoadingr[0]==null){
			for (int i = 0; i < imgFrameChaLoadingr.Length; i++) {
				if(imgFrameChaLoadingr[i]==null)
				imgFrameChaLoadingr[i] = GameCanvas.loadImage("/GuiNaruto/loading/image-"+(i+1)+".png");
			}
		}
		return false;
	}
	public override void update() {
        // TODO Auto-generated method stub
		loadingImg();
		if(idNext<(isTheoList==false?SmallImage.nBigImage:listId.Length)){
			indexPhanTram = (idNext)*frameLoading.getWidth()/(isTheoList==false?SmallImage.nBigImage:listId.Length);
			indexpaintload += (indexPhanTram-indexpaintload)/2;
			if(indexpaintload>=frameLoading.getWidth()) indexpaintload = frameLoading.getWidth();
		}else indexpaintload = frameLoading.getWidth();
		if(!isTheoList&&idNext<SmallImage.nBigImage){
			sbyte[] data = Rms.loadRMS(mSystem.getPathRMS(SmallImage.getPathImage(idNext))+""+idNext);
			if(data!=null){
				idNext++;
				if(idNext>=SmallImage.nBigImage){
					isDownLoadedSuccess = true;
					Rms.saveRMSData(SmallImage.keyOKDownloaded, new sbyte[]{1});
				}
				data = null;
			}
			else if(isOKNext||(mSystem.currentTimeMillis()-timeStart)>timeCoolDownNextImage){
				timeStart = mSystem.currentTimeMillis();
				Service.gI().requestImage(idNext,(sbyte)0);
				isOKNext = false;
				idNext++;
				if(idNext>=SmallImage.nBigImage){
					isDownLoadedSuccess = true;
					Rms.saveRMSData(SmallImage.keyOKDownloaded, new sbyte[]{1});
				}
			}
        }
        else if (isTheoList && idNext < listId.Length)
        {
            sbyte[] data = Rms.loadRMS(mSystem.getPathRMS(SmallImage.getPathImage(listId[idNext] + idAdd) + "" + (listId[idNext] + idAdd)));
			if(data!=null){
				idNext++;
				if(idNext>=listId.Length){
					isDownLoadedSuccess = true;
					Rms.saveRMSData(SmallImage.keyOKDownloaded, new sbyte[]{1});
					//XinChoScreen.gI().switchToMe();
				}
				data = null;
			}
			else if(isOKNext||(mSystem.currentTimeMillis()-timeStart)>timeCoolDownNextImage){
				timeStart = mSystem.currentTimeMillis();
                Service.gI().requestImage(listId[idNext] + idAdd, (sbyte)0);
				isOKNext = false;
				idNext++;
				Rms.saveRMSData(SmallImage.keyOKDownloaded, new sbyte[]{0});
				if(idNext>=listId.Length){
					//XinChoScreen.gI().switchToMe();
					isDownLoadedSuccess = true;
				}
			}
		}
		if(isDownLoadedSuccess&&GameCanvas.gameTick%7==0){
			if(!isTheoList){
				if(idTo==0)
				SelectCharScr.gI().switchToMe(); 
				else if(idTo==1)
					CreatCharScr.gI().switchToMe(); 
			}else XinChoScreen.gI().switchToMe();
		}
		base.update();
	}

	//@Override
	public override void paint(mGraphics g) {
		// TODO Auto-generated method stub
		g.setColor(0x000000);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
		//g.drawImage(imgBgLoading, GameCanvas.w/2, GameCanvas.h/2, mGraphics.VCENTER|mGraphics.HCENTER);
		g.drawRegionScalse(imgBgLoading, 0, 0, imgBgLoading.getWidth(), imgBgLoading.getHeight(), 0,
				GameCanvas.w/2, GameCanvas.h/2, mGraphics.VCENTER|mGraphics.HCENTER, false, (GameCanvas.w*100)/imgBgLoading.getWidth());
		g.setColor(0x3c3a39);
		g.fillRect(GameCanvas.w/2-frameLoading.getWidth()/2,7*GameCanvas.h/8-frameLoading.getHeight()/2,
				frameLoading.getWidth(), frameLoading.getHeight());
		g.drawRegion(frameLoading, 0, 0, indexpaintload, frameLoading.getHeight(),
				0,GameCanvas.w/2-frameLoading.getWidth()/2, 7*GameCanvas.h/8-frameLoading.getHeight()/2, 
				mGraphics.TOP|mGraphics.LEFT);
//		g.drawImage(frameLoading, GameCanvas.w/2, 7*GameCanvas.h/8, mGraphics.VCENTER|mGraphics.HCENTER);
		g.drawImage(ongLoading, GameCanvas.w/2, 7*GameCanvas.h/8, mGraphics.VCENTER|mGraphics.HCENTER);
//		mFont.tahoma_7_white.drawString(g, idNext+"/"+(isTheoList==false?SmallImage.nBigImage:listId.length),
//				GameCanvas.w/2, GameCanvas.h/2, 2);
		if(imgFrameChaLoadingr!=null&&imgFrameChaLoadingr[(GameCanvas.gameTick/2)%6]!=null)
			g.drawImage(imgFrameChaLoadingr[(GameCanvas.gameTick/2)%6],
					GameCanvas.w/2-frameLoading.getWidth()/2+(indexpaintload),
					7*GameCanvas.h/8-18, mGraphics.VCENTER|mGraphics.HCENTER);
		
		base.paint(g);
	}

	public static DownloadImageScreen intansce;
	
	public static DownloadImageScreen gI(){
		if(intansce==null) intansce = new DownloadImageScreen();
		intansce.loadingImg();
		return intansce;
	}
}
