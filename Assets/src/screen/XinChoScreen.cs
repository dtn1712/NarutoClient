
public class XinChoScreen : Screen{

	public static XinChoScreen me;
	public long timeStartOff;
	public int timeSleepLoadTile;
	public bool isRemoveImg;
	public static bool isChangeKhu;
	//@Override
	public override void switchToMe() {
		// TODO Auto-generated method stub
		timeStartOff = mSystem.currentTimeMillis();
		timeSleepLoadTile = 20;
		isRemoveImg = true;
		base.switchToMe();
	}
	public static XinChoScreen gI(){
		if(me==null) me = new XinChoScreen();
		return me;
	}
	//@Override
	public override void update() {
		// TODO Auto-generated method stub
		if(isRemoveImg){
			if(!isChangeKhu)
			Mob.cleanImg();
			isChangeKhu = false;
			BgItem.cleanImg();
			SmallImage.CleanImg();
			mSystem.my_Gc();
			isRemoveImg = false;
		}
		if(TileMap.imgMaptile==null)
		{
			timeSleepLoadTile++;
			if(timeSleepLoadTile>20){
				timeSleepLoadTile = 0;
				TileMap.loadimgTile(TileMap.tileID);
			}
		}
		if((mSystem.currentTimeMillis()-timeStartOff)/1000>3)
			
			if(GameCanvas.gameScr!=null){
				Char.myChar().timeLastchangeMap= mSystem.currentTimeMillis();
				Char.myChar().timedelay = 5000;
				GameCanvas.gameScr.switchToMe();
				GameCanvas.cleanImgBg();
			}
		base.update();
	}

	//@Override
    public override void paint(mGraphics g)
    {
		// TODO Auto-generated method stub
		g.setColor(0x000000);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);

		g.drawImage(loadImageInterface.imgMap, GameCanvas.w/2, GameCanvas.h/2, mGraphics.VCENTER|mGraphics.HCENTER);

		int nCham = (GameCanvas.gameTick/10)%4;
		string text = "";
		for (int i = 0; i < nCham; i++) {
			text+=".";
		}
		for (int i = 0; i < 3-nCham; i++) {
			text+=" ";
		}
		mFont.tahoma_7.drawString(g, TileMap.mapName+text,GameCanvas.w/2, GameCanvas.h/2-4, 2);
//
//		mFont.tahoma_7.drawString(g, text, GameCanvas.w/2+mFont.tahoma_7.getWidth(TileMap.mapName)/2-2,GameCanvas.h/2-4, 0);
		g.drawRegion(loadImageInterface.imgXinCho, 0, 16*((GameCanvas.gameTick/2)%3), 16, 16, 0,
				GameCanvas.w/2, 3*GameCanvas.h/4+20, mGraphics.VCENTER|mGraphics.HCENTER, false);
		
//		mFont.tahoma_7_white.drawString(g, "Xin chờ", GameCanvas.w/2, 3*GameCanvas.h/4+20, 2);

		base.paint(g);
	}

}
