

using System;
public class FlagScreen : Screen , IActionListener{
	
	public static FlagScreen instance;
	
	public const sbyte LOW_LAYER_REGION = 0;
    public const sbyte MED_PLAYER_REGION = 1;
    public const sbyte HIGH_PLAYER_REGION = 2;
    
	public sbyte[] listKhu=new sbyte[]{0,1,2,3,4,5,6,7,8,9,-1};
	Command cmdClose;
	public int wKhung=170,hKhung=150;
	public int xpaint,ypaint;
	public Scroll srclist =new Scroll();
	public int coutFc,lastSelect;
	public int minKhu = 15;
	
	public int day,hour,minute,second,value;
	public static long time;
	public long timeRemove=180000,timeOld; // mili giấy server trả về là s;
	public string sday = "";
	
	public FlagScreen(){
		xpaint = GameCanvas.w/2-wKhung/2;
		ypaint = GameCanvas.h/2-hKhung/2;
		cmdClose = new Command("", this, 2, null);
		cmdClose.setPos(xpaint+wKhung-loadImageInterface.closeTab.width/2, 
				ypaint, loadImageInterface.closeTab, loadImageInterface.closeTab);
	}
	
	public static FlagScreen gI(){
		if(instance==null) instance = new FlagScreen();
		return instance;
	}
	//@Override
	public override void updateKey() {
		// TODO Auto-generated method stub
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdClose)) {
			if (cmdClose != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdClose != null)
				{
					cmdClose.performAction();
				}
			}
		}
		ScrollResult s1 = srclist.updateKey();
		srclist.updatecm();
		if(GameCanvas.isPointerJustRelease&&srclist.selectedItem!=-1&&srclist.selectedItem<listKhu.Length){
			if(listKhu[srclist.selectedItem]!=Char.myChar().typePk){
				if(lastSelect==srclist.selectedItem){
					Service.gI().ChangeFlag((sbyte)listKhu[srclist.selectedItem]);
					GameScr.gI().switchToMe();
				}
				lastSelect = srclist.selectedItem;
				GameCanvas.isPointerJustRelease = false;
			}
		}

        base.updateKey();
	}
	//@Override
	public override void updatePointer() {
		// TODO Auto-generated method stub
		base.updatePointer();
		
	}
	//@Override
	public override void update() {
        // TODO Auto-generated method stub
		base.update();
        GameScr.gI().update();
		if(GameCanvas.gameTick%4 ==0){
			coutFc++;
			if(coutFc>2)
				coutFc =0;
		}
		if(Char.myChar().typePk>-1){
			long currtime = mSystem.currentTimeMillis();
			if (currtime-time>=timeRemove) {
				sday="";
			}
			else{
				int secon = second;
				if((timeRemove-(currtime-time))/1000<=60){
					second = (int)(timeRemove-(currtime-time))/1000;
					day =minute =hour= 0;
				}else if((timeRemove-(currtime-time))/1000<=3600){
					second = (int)(timeRemove-(currtime-time))/1000;
					minute = second/60;
					second = second%60;
					second = hour=day= 0;
				}else if((timeRemove-(currtime-time))/1000<=86400){
					second = (int)(timeRemove-(currtime-time))/1000;
					hour = second/3600;
					second =minute=day= 0;
				}
				else{
					second = (int)(timeRemove-(currtime-time))/1000;
					day = second/86400;
					second =minute =hour= 0;
				}
			}

			sday = (day<=0?"":(day<10?"0"+day:day+"")+"d")+
					(hour<=0?"":(hour<10?"0"+hour:hour+"")+"h")+
					(minute<=0?"":(minute<10?"0"+minute:minute+"")+"'")+
					(second<=0?"":(second<10?"0"+second:second+"")+"s")+"";
		}else sday="";
	}
	//@Override
	public override void paint(mGraphics g) {
		// TODO Auto-generated method stub
		base.paint(g);
		GameScr.gI().paint(g);
		Paint.paintFrameNaruto(xpaint,ypaint,wKhung,hKhung+2,g);
		Paint.PaintBoxName("Cờ",xpaint+wKhung/2-40,ypaint,80,g);
		cmdClose.paint(g);
		if(listKhu!=null){
			srclist.setStyle((listKhu.Length>minKhu?listKhu.Length:minKhu)/5, Image.getWidth(loadImageInterface.ImgItem),
					xpaint+15,ypaint+22+Image.getHeight(loadImageInterface.ImgItem)/4, 
					wKhung-30,hKhung-32, true, 5);
			srclist.setClip(g, xpaint+15,ypaint+22+Image.getHeight(loadImageInterface.ImgItem)/4, 
					wKhung-30,hKhung-32);
			for (int i = 0; i < (listKhu.Length>minKhu?listKhu.Length:minKhu)/5; i++) {
				for(int j=0;j<5;j++)
				{
					g.drawImage(loadImageInterface.ImgItem,xpaint+15+(Image.getWidth(loadImageInterface.ImgItem))*j,ypaint+30+(Image.getHeight(loadImageInterface.ImgItem))*i+2, 0,true);
					if((i*5+j)<listKhu.Length)
					{
						if(listKhu[i*5+j]>-1){
							g.drawRegion(loadImageInterface.iconpk, 0, 12*(listKhu[i*5+j] * 3 + (GameCanvas.gameTick / 3) % 3), 12, 12, 0, 
									xpaint+15+(Image.getWidth(loadImageInterface.ImgItem))*j+Image.getWidth(loadImageInterface.ImgItem)/2,
									ypaint+30+(Image.getHeight(loadImageInterface.ImgItem))*i+2+Image.getWidth(loadImageInterface.ImgItem)/2,
									mGraphics.VCENTER|mGraphics.HCENTER);
							if(listKhu[i*5+j]==Char.myChar().typePk){
								mFont.tahoma_7_white.drawString(g,sday, xpaint+15+(Image.getWidth(loadImageInterface.ImgItem))*j+Image.getWidth(loadImageInterface.ImgItem)/2,
										ypaint+32+(Image.getHeight(loadImageInterface.ImgItem))*i+2+Image.getWidth(loadImageInterface.ImgItem)/2, 2);
							}
						}
						else{
							mFont.tahoma_7_white.drawString(g,"Tháo", xpaint+15+(Image.getWidth(loadImageInterface.ImgItem))*j+Image.getWidth(loadImageInterface.ImgItem)/2,
									ypaint+22+(Image.getHeight(loadImageInterface.ImgItem))*i+2+Image.getWidth(loadImageInterface.ImgItem)/2, 2);
						}
					}
//					if(listKhu[i*5+j][0]==0)
//						mFont.tahoma_7_green.drawString(g, (i*5+j+1)+"",
//								xpaint+15+(Image.getWidth(loadImageInterface.ImgItem))*j+Image.getWidth(loadImageInterface.ImgItem)/2,
//								ypaint+23+(Image.getHeight(loadImageInterface.ImgItem))*i+2+Image.getHeight(loadImageInterface.ImgItem)/2, 2);
//					else if(listKhu[i*5+j][0]==1)
//						mFont.tahoma_7_yellow.drawString(g, (i*5+j+1)+"",
//								xpaint+15+(Image.getWidth(loadImageInterface.ImgItem))*j+Image.getWidth(loadImageInterface.ImgItem)/2,
//								ypaint+23+(Image.getHeight(loadImageInterface.ImgItem))*i+2+Image.getHeight(loadImageInterface.ImgItem)/2, 2);
//					else 
//						mFont.tahoma_7_red.drawString(g, (i*5+j+1)+"",
//							xpaint+15+(Image.getWidth(loadImageInterface.ImgItem))*j+Image.getWidth(loadImageInterface.ImgItem)/2,
//							ypaint+23+(Image.getHeight(loadImageInterface.ImgItem))*i+2+Image.getHeight(loadImageInterface.ImgItem)/2, 2);
				
				}
			}
			if(srclist.selectedItem>=0&&srclist.selectedItem<listKhu.Length)
			Paint.paintFocus(g,
					 (xpaint+15 + ((srclist.selectedItem % 5)*(Image.getWidth(loadImageInterface.ImgItem))))+11-loadImageInterface.ImgItem.getWidth()/4, 
					 ypaint+30+ (srclist.selectedItem / 5) * (Image.getHeight(loadImageInterface.ImgItem) ) +13-loadImageInterface.ImgItem.getWidth()/4
					,loadImageInterface.ImgItem.getWidth()-9,loadImageInterface.ImgItem.getWidth()-9,coutFc);
			GameCanvas.resetTrans(g);
		}
		
	}
	//@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case 2:
			GameScr.gI().switchToMe();
			break;
		}
	}
}
