

using System;
public class KhuScreen : Screen, IActionListener{
	
	public static KhuScreen instance;
	
	public const sbyte LOW_LAYER_REGION = 0;
    public const sbyte MED_PLAYER_REGION = 1;
   public const sbyte HIGH_PLAYER_REGION = 2;
    
	public sbyte[][] listKhu;
	Command cmdClose;
	public int wKhung=170,hKhung=150;
	public int xpaint,ypaint;
	public Scroll srclist =new Scroll();
	public int coutFc;
	public int minKhu = 15;
	
	public KhuScreen(){
		xpaint = GameCanvas.w/2-wKhung/2;
		ypaint = GameCanvas.h/2-hKhung/2;
		cmdClose = new Command("", this, 2, null);
		cmdClose.setPos(xpaint+wKhung-loadImageInterface.closeTab.width/2, 
				ypaint, loadImageInterface.closeTab, loadImageInterface.closeTab);
	}
	
	public static KhuScreen gI(){
		if(instance==null) instance = new KhuScreen();
		return instance;
	}
	//@Override
	public override void updateKey() {
		// TODO Auto-generated method stub

		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdClose)) {
			if (cmdClose != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdClose != null)
				{
					cmdClose.performAction();
				}
			}
		}
		ScrollResult s1 = srclist.updateKey();
		srclist.updatecm();
		if(GameCanvas.isPointerJustRelease&&srclist.selectedItem!=-1&&srclist.selectedItem<listKhu.Length){
			Service.gI().requestChangeKhu((sbyte)srclist.selectedItem);
			GameCanvas.isPointerJustRelease = false;
		}

        base.updateKey();

	}
	//@Override
	public override void updatePointer() {
		// TODO Auto-generated method stub
		base.updatePointer();
		
	}
	//@Override
	public override void update() {
		// TODO Auto-generated method stub
		base.update();
		GameScr.gI().update();
		if(GameCanvas.gameTick%4 ==0){
			coutFc++;
			if(coutFc>2)
				coutFc =0;
		}
	}
	//@Override
	public override void paint(mGraphics g) {
		// TODO Auto-generated method stub
		base.paint(g);
		GameScr.gI().paint(g);
		Paint.paintFrameNaruto(xpaint,ypaint,wKhung,hKhung+2,g);
		Paint.PaintBoxName("Khu",xpaint+wKhung/2-40,ypaint,80,g);
		cmdClose.paint(g);
		if(listKhu!=null){
			srclist.setStyle((listKhu.Length>minKhu?listKhu.Length:minKhu)/5, Image.getWidth(loadImageInterface.ImgItem),
					xpaint+15,ypaint+22+Image.getHeight(loadImageInterface.ImgItem)/4, 
					wKhung-30,hKhung-32, true, 5);
			srclist.setClip(g, xpaint+15,ypaint+22+Image.getHeight(loadImageInterface.ImgItem)/4, 
					wKhung-30,hKhung-32);
			for (int i = 0; i < (listKhu.Length>minKhu?listKhu.Length:minKhu)/5; i++) {
				for(int j=0;j<5;j++)
				{
					g.drawImage(loadImageInterface.ImgItem,xpaint+15+(Image.getWidth(loadImageInterface.ImgItem))*j,ypaint+30+(Image.getHeight(loadImageInterface.ImgItem))*i+2, 0,true);
					if((i*5+j)<listKhu.Length)
					if(listKhu[i*5+j][0]==0)
						mFont.tahoma_7_green.drawString(g, (i*5+j+1)+"",
								xpaint+15+(Image.getWidth(loadImageInterface.ImgItem))*j+Image.getWidth(loadImageInterface.ImgItem)/2,
								ypaint+23+(Image.getHeight(loadImageInterface.ImgItem))*i+2+Image.getHeight(loadImageInterface.ImgItem)/2, 2);
					else if(listKhu[i*5+j][0]==1)
						mFont.tahoma_7_yellow.drawString(g, (i*5+j+1)+"",
								xpaint+15+(Image.getWidth(loadImageInterface.ImgItem))*j+Image.getWidth(loadImageInterface.ImgItem)/2,
								ypaint+23+(Image.getHeight(loadImageInterface.ImgItem))*i+2+Image.getHeight(loadImageInterface.ImgItem)/2, 2);
					else 
						mFont.tahoma_7_red.drawString(g, (i*5+j+1)+"",
							xpaint+15+(Image.getWidth(loadImageInterface.ImgItem))*j+Image.getWidth(loadImageInterface.ImgItem)/2,
							ypaint+23+(Image.getHeight(loadImageInterface.ImgItem))*i+2+Image.getHeight(loadImageInterface.ImgItem)/2, 2);
				
				}
			}
			if(srclist.selectedItem>0&&srclist.selectedItem<listKhu.Length)
			Paint.paintFocus(g,
					 (xpaint+15 + ((srclist.selectedItem % 5)*(Image.getWidth(loadImageInterface.ImgItem))))+11-loadImageInterface.ImgItem.getWidth()/4, 
					 ypaint+30+ (srclist.selectedItem / 5) * (Image.getHeight(loadImageInterface.ImgItem) ) +13-loadImageInterface.ImgItem.getWidth()/4
					,loadImageInterface.ImgItem.getWidth()-9,loadImageInterface.ImgItem.getWidth()-9,coutFc);
			GameCanvas.resetTrans(g);
		}
		
	}
	//@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case 2:
			GameScr.gI().switchToMe();
			break;
		}
	}
}
