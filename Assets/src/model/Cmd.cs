

public class Cmd {
	
	public const sbyte LOGIN = 1;
	public const sbyte CHAR_INFO = 3;
	public const sbyte PLAYER_MOVE = 4;
	public const sbyte PLAYER_INFO = 5;
	public const sbyte REQUEST_MONSTER_INFO = 6;
	public const sbyte PLAYER_REMOVE = 7;
	public const sbyte ATTACK = 8;
	public const sbyte ITEM = 9;
	public const sbyte CHANGE_MAP = 10;
	public const sbyte SELECT_CHAR = 11;
	public const sbyte CREATE_CHAR = 12;
	public const sbyte GIVEUP_ITEM = 13; //vứt bỏ item
	public const sbyte PICK_REMOVE_ITEM = 14;
	public const sbyte ADD_BASE_POINT = 16;
	public const sbyte NPC_REQUEST = 17;
	public const sbyte BUY_ITEM_FROM_SHOP = 18;
	public const sbyte CHAT= 19;
	public const sbyte COME_HOME_DIE = 20;
    public const sbyte REGISTER = 23;
	public const sbyte MENU_NPC = 24;
	public const sbyte CHAR_LIST = 27;
	public const sbyte GET_ITEM_INVENTORY = 30;
	public const sbyte DROP_ITEM = 31;
	public const sbyte PARTY = 32;
	public const sbyte XP_CHAR = 33;//trade
	public const sbyte TRADE = 34;//trade
	public const sbyte SKILL_CHAR= 35;
	public const sbyte DIALOG = 37;
	public const sbyte DIE = 39;
	public const sbyte FRIEND = 40;
	public const sbyte QUEST = 43;//quest
	public const sbyte NPC = 44;
	public const sbyte CHANGE_REGION=45;
	public const sbyte REQUEST_REGION=46;
	public const sbyte SERVER_CHAT  = 52;
	public const sbyte REMOVE_TARGET = 53;
	public const sbyte REQUEST_SHOP = 54; // yeu cau item
	public const sbyte CMD_DYNAMIC_MENU = 55;// menu shop;
	public const sbyte NPC_TEAMPLATE = 57;
	public const sbyte MAP_TEAMPLATE = 58;
	public const sbyte CHAR_SKILL_STUDIED = 60;
	public const sbyte COMPETED_ATTACK = 62;
	public const sbyte STATUS_ATTACK = 63;
	public const sbyte UPDATE_CHAR=64;
	
	

	public const sbyte REQUEST_IMAGE = -56;
	
	
	
	public const sbyte KEY_WINDOWPHONE = -127;
	public const sbyte FULL_SIZE = -126;

    public const sbyte CMD_FULL_SIZE = 127;
}
