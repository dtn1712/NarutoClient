
public class Type_Party {
	
	 public const sbyte INVITE_PARTY = 0;
	 public const sbyte ACCEPT_INVITE_PARTY = 1;
	 public const sbyte OUT_PARTY = 2;
	 public const sbyte REQUEST_JOIN_PARTY= 3;
	 public const sbyte KICK_OUT_PARTY= 4;
	 public const sbyte CHANGE_BOSS_PARTY= 5;
	 public const sbyte GET_INFOR_PARTY= 6;
	 public const sbyte GET_INFOR_NEARCHAR= 7;
	 public const sbyte DISBAND_PARTY= 8;
	
}
