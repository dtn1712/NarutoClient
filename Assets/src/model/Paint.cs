

public class Paint {
	public static int COLORBACKGROUND = 0xf3ae58;
	public static int COLORLIGHT = 0x923200;
	public static int COLORDARK = 0x3c1400;	
	public static int COLORBORDER = 0xe84f00;
	public static int COLORFOCUS = 0xffffff;
	
	
	
	public static mBitmap imgBg, imgLogo, imgLB, imgLT, imgRB, imgRT, imgChuong, imgSelectBoard;
	public static mBitmap imgtoiSmall, imgTayTren, imgTayDuoi;
	public static mBitmap[] imgTick = new mBitmap[2], imgMsg = new mBitmap[2];
	 

	public static int hTab = 24, lenCaption = 0;
	public int[] color = new int[] { 0xF3B060, 0xdafe7db, 0x225544, 0xF9DB83, 0xF2B76D, 0xC55035, 0x2f705a };

	public void paintDefaultBg(mGraphics g) {
		g.setColor(0x880E0E);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
		g.drawImage(imgBg, GameCanvas.w / 2, GameCanvas.h / 2 - hTab / 2 - 1, 3);
		g.drawImage(imgLT, 0, 0, 0);
		g.drawImage(imgRT, GameCanvas.w, 0, mGraphics.TOP | mGraphics.RIGHT);
		g.drawImage(imgLB, 0, GameCanvas.h - hTab - 2, mGraphics.BOTTOM | mGraphics.LEFT);
		g.drawImage(imgRB, GameCanvas.w, GameCanvas.h - hTab - 2, mGraphics.BOTTOM | mGraphics.RIGHT);
		g.setColor(0xFFF6BB);
		g.drawRect(0, 0, GameCanvas.w, 0);
		g.drawRect(0, GameCanvas.h - hTab - 2, GameCanvas.w, 0);
		g.drawRect(0, 0, 0, GameCanvas.h - hTab);
		g.drawRect(GameCanvas.w - 1, 0, 0, GameCanvas.h - hTab);
	}

	public void paintfillDefaultBg(mGraphics g) {
		g.setColor(0x032202);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
	}

	public void repaintCircleBg() {

	}

	public void paintSolidBg(mGraphics g) {

	}

	public void paintDefaultPopup(mGraphics g, int x, int y, int w, int h) {
		g.setColor(0x805802);// nền nâu trong
		g.fillRect(x, y, w, h);
		g.setColor(0xcf9f38);// viền nâu
		g.drawRect(x, y, w, h);

	}

	public void paintWhitePopup(mGraphics g, int y, int x, int width, int height) {
		g.setColor(0xfffcab);
		g.fillRect(x, y, width, height);
		g.setColor(0);
		g.drawRect(x - 1, y - 1, width + 1, height + 1);
	}

	public void paintDefaultPopupH(mGraphics g, int h) {
		g.setColor(0xd9e1f1);
		g.fillRect(8, GameCanvas.h - (h + 37), GameCanvas.w - 16, h + 4);
		g.setColor(0x4772d5);
		g.fillRect(10, GameCanvas.h - (h + 35), GameCanvas.w - 20, h);
	}

	public void paintCmdBar(mGraphics g, Command left, Command center, Command right) {
		mFont f = GameCanvas.isTouch ? mFont.tahoma_7b_yellow : mFont.tahoma_8b;
		int d = GameCanvas.isTouch ? 3 : 1;
		if (!GameCanvas.isTouch) {
			if (left != null) {
				f.drawString(g, left.caption, 5, GameCanvas.h - Screen.cmdH + 4 + d, 0);
			}
			if (center != null) {
				f.drawString(g, center.caption, GameCanvas.hw, GameCanvas.h - Screen.cmdH + 4 + d, 2);
			}
			if (right != null) {
				if (right.img != null)
					g.drawImage(right.img, GameCanvas.w - 5, GameCanvas.h - 11, mGraphics.RIGHT | mGraphics.VCENTER);
				else
					f.drawString(g, right.caption, GameCanvas.w - 5, GameCanvas.h - Screen.cmdH + 4 + d, 1);
			}
		} else {
			if(GameScr.isPaintTeam && GameScr.charnearByme.size() == 0){ //Paint 3 button Party(kich, roi, giai tan)
				int x;
	    		int y;
				if (left != null) { // kich
					lenCaption = f.getWidth(left.caption);
					if (lenCaption > 0) {
								if (Screen.keyTouch == 0)
								g.drawImage(GameScr.imgLbtnFocus, left.x, left.y, 0);
							else
								g.drawImage(GameScr.imgLbtn, left.x, left.y, 0);
							f.drawString(g, left.caption, left.x + 35 ,left.y + 5, 2);
					}
				}
				if (center != null) { // roi
					lenCaption = f.getWidth(center.caption);
					if (lenCaption > 0) {
								if (Screen.keyTouch == 1)
									g.drawImage(GameScr.imgLbtnFocus,  center.x, center.y , 0);
								else
									g.drawImage(GameScr.imgLbtn, center.x, center.y, 0);
								f.drawString(g, center.caption,  center.x + 35, center.y + 5 , 2);

					}

				}
				if (right != null) { // giai tan
					lenCaption = f.getWidth(right.caption);

					if (lenCaption > 0) {
							if (Screen.keyTouch == 2)
								g.drawImage(GameScr.imgLbtnFocus,  right.x, right.y, 0);
							else
								g.drawImage(GameScr.imgLbtn,  right.x, right.y, 0);
							f.drawString(g, right.caption,  right.x + 35 ,right.y + 5, 2);
					}

				}
			}else{
				if (left != null) {
					lenCaption = f.getWidth(left.caption);
					if (lenCaption > 0) {
						if (left.x > 0 && left.y > 0)
							left.paint(g);
						else {
							if (Screen.keyTouch == 0)
								g.drawImage(GameScr.imgLbtnFocus, 1, GameCanvas.h - Screen.cmdH + 1, 0);
							else
								g.drawImage(GameScr.imgLbtn, 1, GameCanvas.h - Screen.cmdH + 1, 0);
							f.drawString(g, left.caption, 35, GameCanvas.h - Screen.cmdH + 4 + d, 2);
						}
					}
				}
				if (center != null) {
					lenCaption = f.getWidth(center.caption);
					if (lenCaption > 0) {
						if (center.x > 0 && center.y > 0)
							center.paint(g);
						else {
							if (Screen.keyTouch == 1)
								g.drawImage(GameScr.imgLbtnFocus, GameCanvas.hw - 35, GameCanvas.h - Screen.cmdH + 1, 0);
							else
								g.drawImage(GameScr.imgLbtn, GameCanvas.hw - 35, GameCanvas.h - Screen.cmdH + 1, 0);
							f.drawString(g, center.caption, GameCanvas.hw, GameCanvas.h - Screen.cmdH + 4 + d, 2);
						}
						
					}
					
				}
				if (right != null) {
					
					lenCaption = f.getWidth(right.caption);
					
					if (lenCaption > 0) {
						if (right.x > 0 && right.y > 0)
							right.paint(g);
						else {
							if (Screen.keyTouch == 2)
								g.drawImage(GameScr.imgLbtnFocus, GameCanvas.w - 71, GameCanvas.h - Screen.cmdH + 1, 0);
							else
								g.drawImage(GameScr.imgLbtn, GameCanvas.w - 71, GameCanvas.h - Screen.cmdH + 1, 0);
							f.drawString(g, right.caption, GameCanvas.w - 35, GameCanvas.h - Screen.cmdH + 4 + d, 2);
						}
					}
					
				}
			}
			}
	}


	public void paintTabSoft(mGraphics g) {
		if (!GameCanvas.isTouch) {
			g.setColor(0);
			g.fillRect(0, GameCanvas.h - hTab, GameCanvas.w, hTab+1);
			g.setColor(0x888888);
			g.fillRect(0, GameCanvas.h - (hTab-1), GameCanvas.w, 1);
		}
//		else {
//			if(GameScr.gI().isNotPaintTouchControl()){
//			if(GameCanvas.currentDialog!=null && GameCanvas.currentScreen != GameScr.gI())
//				return;
//				for (int i = 0; i < GameCanvas.w; i += 30)
//					g.drawImage(GameScr.imgBar, i, GameCanvas.h - 27, 0);
//			}
//		}
	}

	public void paintSelect(mGraphics g, int x, int y, int w, int h) {
		g.setColor(0xFFF6BB);// (0xdaa417);
		g.fillRect(x, y, w, h);
	}

	public void paintLogo(mGraphics g, int x, int y) {

		g.drawImage(imgLogo, x, y, 3);
	}

	public void paintHotline(mGraphics g, string number) {
		// if (!number.Equals(""))
		// Fontsys.tahoma_8b.drawString(g, "Hotline: " + number, GameCanvas.w -
		// 1,
		// GameCanvas.h - hTab - 14, 1);
		// Fontsys.tahoma_8b.drawString(g, GameMidlet.version, GameCanvas.w - 2,
		// 2, Font.RIGHT);
	}

	public void paintInputTf(mGraphics g, bool iss, int x, int y, int w, int h, int xText, int yText, string text) {
		// g.setColor(0x777777);
		g.setColor(0);
		if (iss) {
//			g.drawImage(TField.imgTf2, x + 1, y + 1, Graphics.TOP|Graphics.LEFT);
			// g.setColor(0);
			g.setColor(0x222222);
			g.fillRect(x + 1, y + 1, w - 1, h - 1);
			g.setColor(0);
		} else {
//			g.drawImage(TField.imgTf1, x + 1, y + 1, Graphics.TOP|Graphics.LEFT);
			g.setColor(0x111111);
			g.fillRect(x + 1, y + 1, w - 1, h - 1);
			g.setColor(0);
		}
		g.drawRect(x + 1, y + 1, w - 2, h - 2);
		g.setClip(x + 3, y + 1, w - 4, h - 4);
		mFont.tahoma_8b.drawString(g, text, xText, yText, 0);
	}

	public void paintBackMenu(mGraphics g, int x, int y, int w, int h, bool iss) {
		if (iss) {
			g.setColor(0xFE0000);// viền
			// g.fillRect(x, y, w, h);
			g.fillRoundRect(x, y, w, h, 10, 10);
			g.setColor(0xFFE634);
		} else {
			g.setColor(0xFFF7B9);
			g.fillRoundRect(x, y, w, h, 10, 10);
			g.setColor(0xFFF7B9);
		}
		g.fillRoundRect(x + 3, y + 3, w - 6, h - 6, 10, 10);

	}

	public void paintMsgBG(mGraphics g, int x, int y, int w, int h, string title, string subTitle, string check) {
		g.setClip(x, y, w, h);
		g.setColor(0x4f9375);
		g.fillRect(x, y, w - 1, h - 1);
		g.setColor(0xFFFFFF);
		g.drawRect(x, y, w - 1, h - 1);
		g.setColor(0x357000);
		g.fillRect(x + 1, y + 25, w - 2, Screen.ITEM_HEIGHT);
		// Font.bigFont.drawString(g, title, x + 10, y + 3, 0);
		mFont.tahoma_8b.drawString(g, subTitle, x + 10, y + 28, 0);
		mFont.tahoma_8b.drawString(g, check, x + GameCanvas.w - 45, y + 28, 2);
	}

	public void paintDefaultScrList(mGraphics g, string title, string subTitle, string check) {
		g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
		GameCanvas.paintt.paintDefaultBg(g);
		// Font.bigFont.drawString(g, title, GameCanvas.hw, 3, 2);
		g.setColor(0xFFE634);
		g.fillRect(0, 25, GameCanvas.w, Screen.ITEM_HEIGHT);
		mFont.tahoma_8b.drawString(g, subTitle, 10, 28, 0);
		mFont.tahoma_8b.drawString(g, check, GameCanvas.w - 20, 28, 2);
	}

	public void paintCheck(mGraphics g, int x, int y, int index) {
		g.drawImage(imgTick[1], x, y, 3);
		if (index == 1)

			g.drawImage(imgTick[0], x + 1, y - 3, 3);

	}

	public void paintImgMsg(mGraphics g, int x, int y, int index) {
		g.drawImage(imgMsg[index], x, y, 0);
	}

	public void paintTitleBoard(mGraphics g, int roomId) {
		paintDefaultBg(g);

	}

	public void paintCheckPass(mGraphics g, int x, int y, bool check, bool focus) {
		if (focus) {
			g.setColor(0x93410a);
			g.fillRect(x - 2, y - 2, 14, 14);
		}
		g.setColor(0xffffff);
		g.fillRect(x, y, 10, 10);
		if (check) {
			g.setColor(0x93410a);
			g.fillRect(x + 2, y + 2, 6, 6);
		}

	}

	public void paintInputDlg(mGraphics g, int x, int y, int w, int h, string[] str) {
		paintFrame(x, y, w, h, g);
		int yStart = y + 20 - ( mFont.tahoma_8b.getHeight());
		for (int i = 0, a = yStart; i < str.Length; i++, a += mFont.tahoma_8b.getHeight()) {
			mFont.tahoma_8b.drawString(g, str[i], x + w / 2, a, 2);
		}
	}

	public void paintIconMainMenu(mGraphics g, int x, int y, bool iss, bool isSe, int i, int wStr) {

	}

	public void paintLineRoom(mGraphics g, int x, int y, int xTo, int yTo) {
		g.setColor(0xFFF6BB);// (0x60c406);
		g.drawLine(x, y, xTo, yTo);
	}

	public void paintCellContaint(mGraphics g, int x, int y, int w, int h, bool iss) {
		if (iss) {
			g.setColor(0xc86200);
			g.fillRect(x + 2, y + 2, w - 3, w - 3);
		}
		g.setColor(0x357000);
		g.drawRect(x, y, w, w);
	}

	public void paintScroll(mGraphics g, int x, int y, int h) {
		g.setColor(0x3ab648);
		g.fillRect(x, y, 4, h);
	}

	public int[] getColorMsg() {
		return color;
	}

	public void paintLogo(mGraphics g) {
		// g.setColor(0x012502);
		g.setColor(0x880E0E);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
		g.drawImage(imgLogo, GameCanvas.w >> 1, GameCanvas.h>> 1, 3);
	}

	public void paintTextLogin(mGraphics g, bool isRes) {
		int aa = 0;
		if (!isRes && GameCanvas.h <= 240) {
			aa = 15;
		}
		mFont.tahoma_7b_white.drawString(g, mResources.LOGINLABELS[0], GameCanvas.hw, GameCanvas.hh + 60 - aa, 2);
		mFont.tahoma_7b_white.drawString(g, mResources.LOGINLABELS[1], GameCanvas.hw, GameCanvas.hh + 73 - aa, 2);
	}

	public void paintSellectBoard(mGraphics g, int x, int y, int w, int h) {

		// if(BoardListScr.gI().type==3)
		g.drawImage(imgSelectBoard, x - 7, y, 0);
		// else
		// {
		// g.setColor(0xFAE301);
		// g.drawRect(x, y, w, h);
		// }
	}

	public int isRegisterUsingWAP() {
		return 0;
	}

	public string getCard() {
		return "/vmg/card.on";
	}

	public void paintSellectedShop(mGraphics g, int x, int y, int w, int h) {
		g.setColor(0xffffff);
		g.drawRect(x, y, 40, 40);
		g.drawRect(x + 1, y + 1, 38, 38);
	}

	public string getUrlUpdateGame() {
		return "http://wap.teamobi.com?info=checkupdate&game=3&version=" + GameMidlet.VERSION + "&provider=" + GameMidlet.userProvider;
	}

	public void doSelect(int focus) {
		// TODO Auto-generated method stub

	}

	public static void paintFrame(int x, int y, int w, int h, mGraphics g) {
		g.setColor(COLORBACKGROUND);
		g.fillRect(x, y, w, h);
		g.setColor(0);// viền nâu
		g.drawRect(x - 2, y - 2, w + 3, h + 3);
		g.setColor(0xd4d4d4);// viền nâu
		g.drawRect(x - 1, y - 1, w + 1, h + 1);
		g.setColor(0x574949);// viền nâu
		g.drawRect(x, y, w - 1, h - 1);
//		if (GameCanvas.isTouch) {
//			g.drawImage(GameCanvas.imgBorder[0], x - 4, y - 3, mGraphics.TOP | mGraphics.LEFT);
//			g.drawRegion(GameCanvas.imgBorder[0], 0, 0, GameCanvas.borderConnerW, GameCanvas.borderConnerH, Sprite.TRANS_MIRROR, x + w + 4, y - 3, StaticObj.TOP_RIGHT);
//			g.drawRegion(GameCanvas.imgBorder[0], 0, 0, GameCanvas.borderConnerW, GameCanvas.borderConnerH, Sprite.TRANS_MIRROR_ROT180, x - 4, y + h + 3, StaticObj.BOTTOM_LEFT);
//			g.drawRegion(GameCanvas.imgBorder[0], 0, 0, GameCanvas.borderConnerW, GameCanvas.borderConnerH, Sprite.TRANS_ROT180, x + w + 4, y + h + 3, StaticObj.BOTTOM_RIGHT);
//			g.drawImage(GameCanvas.imgBorder[1], x + w / 2, y - 4, StaticObj.TOP_CENTER);
//		}
		
	}
	
	public static void paintBorder(int x, int y, int w, int h, mGraphics g,int rbg,int width) {
		g.setColor(rbg);
		g.fillRect(x , y,w,width);
		g.drawRect(x, y,w,width);
		
		g.fillRect(x+w ,y,width,h);
		g.drawRect(x+w, y,width,h);
		
		g.fillRect(x , y+h,w,width);
		g.drawRect(x, y+h,w,width);
		
		g.fillRect(x ,y,width,h);
		g.drawRect(x, y,width,h);
	}
	
	public static void paintFrameOther(int x, int y, int w, int h, mGraphics g) {
		g.setColor(COLORBACKGROUND);
		g.fillRect(x, y, w, h);
		g.setColor(0);// viền nâu
		g.drawRect(x - 2, y - 2, w + 3, h + 3);
		g.setColor(0xd4d4d4);// viền nâu
		g.drawRect(x - 1, y - 1, w + 1, h + 1);
		g.setColor(0x574949);// viền nâu
		g.drawRect(x, y, w - 1, h - 1);
		if (GameCanvas.isTouch) {
			g.drawImage(GameCanvas.imgBorder[0], x - 4, y - 3, mGraphics.TOP | mGraphics.LEFT);
			g.drawRegion(GameCanvas.imgBorder[0], 0, 0, GameCanvas.borderConnerW, GameCanvas.borderConnerH, Sprite.TRANS_MIRROR, x + w + 4, y - 3, StaticObj.TOP_RIGHT);
			g.drawRegion(GameCanvas.imgBorder[0], 0, 0, GameCanvas.borderConnerW, GameCanvas.borderConnerH, Sprite.TRANS_MIRROR_ROT180, x - 4, y + h + 3, StaticObj.BOTTOM_LEFT);
			g.drawRegion(GameCanvas.imgBorder[0], 0, 0, GameCanvas.borderConnerW, GameCanvas.borderConnerH, Sprite.TRANS_ROT180, x + w + 4, y + h + 3, StaticObj.BOTTOM_RIGHT);
			g.drawImage(GameCanvas.imgBorder[1], x + w / 2, y - 4, StaticObj.TOP_CENTER);
		}
		
	}

	public void paintFrameBorder(int x, int y, int w, int h, mGraphics g) {

		g.setColor(0);
		g.drawRect(x - 2, y - 2, w + 3, h + 3);
		g.setColor(0xd4d4d4);
		g.drawRect(x - 1, y - 1, w + 1, h + 1);
		g.setColor(0x574949);
		g.drawRect(x, y, w - 1, h - 1);
		if (GameCanvas.isTouch) {			
			g.drawImage(GameCanvas.imgBorder[0], x - 4, y - 3, mGraphics.TOP | mGraphics.LEFT);
			g.drawRegion(GameCanvas.imgBorder[0], 0, 0, GameCanvas.borderConnerW, GameCanvas.borderConnerH, Sprite.TRANS_MIRROR, x + w + 4, y - 3, StaticObj.TOP_RIGHT);
			g.drawRegion(GameCanvas.imgBorder[0], 0, 0, GameCanvas.borderConnerW, GameCanvas.borderConnerH, Sprite.TRANS_MIRROR_ROT180, x - 4, y + h + 3, StaticObj.BOTTOM_LEFT);
			g.drawRegion(GameCanvas.imgBorder[0], 0, 0, GameCanvas.borderConnerW, GameCanvas.borderConnerH, Sprite.TRANS_ROT180, x + w + 4, y + h + 3, StaticObj.BOTTOM_RIGHT);
			g.drawImage(GameCanvas.imgBorder[1], x + w / 2, y - 4, StaticObj.TOP_CENTER);
		}
	}

	public void paintFrameInside(int x, int y, int w, int h, mGraphics g) {
		g.setColor(COLORBACKGROUND);
		g.fillRect(x, y, w, h);

	}

	public void paintFrameInsideSelected(int x, int y, int w, int h, mGraphics g) {
		g.setColor(COLORLIGHT);
		g.fillRect(x, y, w, h);		
	}
	
	//Paint naruto
	
	//paint gui
	public static void paintFrameNaruto(int x, int y, int w, int h, mGraphics g) {
		g.setColor(COLORBACKGROUND);
		g.fillRect(x, y, w, h);
		if (GameCanvas.isTouch) {
//			int ww=loadImageInterface.imgConerGui[0].getWidth();
//			int hh=loadImageInterface.imgConerGui[0].getHeight();
			int ww=Image.getWidth(loadImageInterface.imgConerGui[0]);
			int hh=Image.getHeight(loadImageInterface.imgConerGui[0]);
			
//			int nWidth=((w-ww*2)/(loadImageInterface.imgConerGui[1].getWidth()-1))+2;
//			int nHeight=((h-hh*2)/(loadImageInterface.imgConerGui[1].getWidth()-1))+1;
			
			int nWidth=((w-ww*2)/(Image.getWidth(loadImageInterface.imgConerGui[1])-1))+2;
			int nHeight=((h-hh*2)/(Image.getWidth(loadImageInterface.imgConerGui[1])-1))+1;
			
			for(int i=0;i<nWidth;i++)
			{
				g.drawImage(loadImageInterface.imgConerGui[1], x-3 + ww+i*(Image.getWidth(loadImageInterface.imgConerGui[1])-1), y-3, mGraphics.TOP | mGraphics.LEFT);
			}
			
			for(int i=0;i<nHeight;i++)
			{
				g.drawRegion(loadImageInterface.imgConerGui[2], 0, 0,Image.getWidth(loadImageInterface.imgConerGui[2]),
						Image.getHeight(loadImageInterface.imgConerGui[2]), Sprite.TRANS_ROT180, x + w+3, y+hh -3+i*(Image.getHeight(loadImageInterface.imgConerGui[2])-1), StaticObj.TOP_RIGHT);
			}
			
			for(int i=0;i<nWidth;i++)
			{
					g.drawRegion(loadImageInterface.imgConerGui[1], 0, 0,Image.getWidth(loadImageInterface.imgConerGui[1]),
							Image.getHeight(loadImageInterface.imgConerGui[1]), Sprite.TRANS_ROT180,x -3+ ww+i*(Image.getWidth(loadImageInterface.imgConerGui[1])-1), y+h +3, StaticObj.BOTTOM_LEFT);
			}
			
			for(int i=0;i<nHeight;i++)
			{
					g.drawRegion(loadImageInterface.imgConerGui[2], 0, 0,Image.getWidth(loadImageInterface.imgConerGui[2]),
							Image.getHeight(loadImageInterface.imgConerGui[2]), Sprite.TRANS_NONE, x-3, y+hh - 3+i*(Image.getHeight(loadImageInterface.imgConerGui[2])-1), StaticObj.TOP_LEFT);
			}
			

			g.drawImage(loadImageInterface.imgConerGui[0], x-3, y-3, mGraphics.TOP | mGraphics.LEFT,true);
			
			g.drawRegion(loadImageInterface.imgConerGui[0], 0, 0,ww,
					hh, Sprite.TRANS_MIRROR, x + w+3, y - 3, StaticObj.TOP_RIGHT,true);
			
			g.drawRegion(loadImageInterface.imgConerGui[0], 0, 0,ww,
					hh, Sprite.TRANS_MIRROR_ROT180, x - 3, y + h + 3, StaticObj.BOTTOM_LEFT,true);
			
			g.drawRegion(loadImageInterface.imgConerGui[0], 0, 0,ww, 
					hh, Sprite.TRANS_ROT180, x + w +3, y + h + 3, StaticObj.BOTTOM_RIGHT,true);
		}
	}
	public static void paintFrameNaruto(int x, int y, int w, int h, mGraphics g,bool isClip) {
		g.setColor(COLORBACKGROUND);
		g.fillRect(x, y, w, h);
		
		if (GameCanvas.isTouch) {
//			int ww=loadImageInterface.imgConerGui[0].getWidth();
//			int hh=loadImageInterface.imgConerGui[0].getHeight();
			int ww=Image.getWidth(loadImageInterface.imgConerGui[0]);
			int hh=Image.getHeight(loadImageInterface.imgConerGui[0]);
			
//			int nWidth=((w-ww*2)/(loadImageInterface.imgConerGui[1].getWidth()-1))+2;
//			int nHeight=((h-hh*2)/(loadImageInterface.imgConerGui[1].getWidth()-1))+1;
			
			int nWidth=((w-ww*2)/(Image.getWidth(loadImageInterface.imgConerGui[1])-1))+2;
			int nHeight=((h-hh*2)/(Image.getWidth(loadImageInterface.imgConerGui[1])-1))+1;
			
			
			for(int i=0;i<nWidth;i++)
			{
//				g.drawImage(loadImageInterface.imgConerGui[1], x-3 + ww+i*(loadImageInterface.imgConerGui[1].getWidth()-1), y-3, mGraphics.TOP | mGraphics.LEFT);
				g.drawImage(loadImageInterface.imgConerGui[1], x-3 + ww+i*(Image.getWidth(loadImageInterface.imgConerGui[1])-1), y-3, mGraphics.TOP | mGraphics.LEFT,isClip);
			}
			
			for(int i=0;i<nHeight;i++)
			{
//					g.drawRegion(loadImageInterface.imgConerGui[2], 0, 0,loadImageInterface.imgConerGui[2].getWidth(),
//							loadImageInterface.imgConerGui[2].getHeight(), Sprite.TRANS_ROT180, x + w+3, y+hh -3+i*(loadImageInterface.imgConerGui[2].getHeight()-1), StaticObj.TOP_RIGHT);
				g.drawRegion(loadImageInterface.imgConerGui[2], 0, 0,Image.getWidth(loadImageInterface.imgConerGui[2]),
						Image.getHeight(loadImageInterface.imgConerGui[2]), Sprite.TRANS_ROT180, x + w+3, y+hh -3+i*(Image.getHeight(loadImageInterface.imgConerGui[2])-1), StaticObj.TOP_RIGHT,isClip);
			}
			
			for(int i=0;i<nWidth;i++)
			{
					g.drawRegion(loadImageInterface.imgConerGui[1], 0, 0,Image.getWidth(loadImageInterface.imgConerGui[1]),
							Image.getHeight(loadImageInterface.imgConerGui[1]), Sprite.TRANS_ROT180,x -3+ ww+i*(Image.getWidth(loadImageInterface.imgConerGui[1])-1), y+h +3, StaticObj.BOTTOM_LEFT,isClip);
			}
			
			for(int i=0;i<nHeight;i++)
			{
					g.drawRegion(loadImageInterface.imgConerGui[2], 0, 0,Image.getWidth(loadImageInterface.imgConerGui[2]),
							Image.getHeight(loadImageInterface.imgConerGui[2]), Sprite.TRANS_NONE, x-3, y+hh - 3+i*(Image.getHeight(loadImageInterface.imgConerGui[2])-1), StaticObj.TOP_LEFT,isClip);
			}
			
			g.drawImage(loadImageInterface.imgConerGui[0], x-3, y-3, mGraphics.TOP | mGraphics.LEFT,isClip);
			
			g.drawRegion(loadImageInterface.imgConerGui[0], 0, 0,ww,
					hh, Sprite.TRANS_MIRROR, x + w+3, y - 3, StaticObj.TOP_RIGHT,isClip);
			
			g.drawRegion(loadImageInterface.imgConerGui[0], 0, 0,ww,
					hh, Sprite.TRANS_MIRROR_ROT180, x - 3, y + h + 3, StaticObj.BOTTOM_LEFT,isClip);
			
			g.drawRegion(loadImageInterface.imgConerGui[0], 0, 0,ww, 
					hh, Sprite.TRANS_ROT180, x + w +3, y + h + 3, StaticObj.BOTTOM_RIGHT,isClip);
			
			
		}
	}
	//paint name box
	public  static void PaintBoxName(string strName,int x,int y,int w,mGraphics g)
	{
		int count=(w/10);
		
		g.drawImage(loadImageInterface.imgBoxName,x-25,y,mGraphics.LEFT|mGraphics.TOP);
		for(int i=0;i<count;i++)
		{
			g.drawImage(loadImageInterface.imgBoxName_1,x+Image.getWidth(loadImageInterface.imgBoxName_1)*i,y,mGraphics.LEFT|mGraphics.TOP);
		}
		
		g.drawRegion(loadImageInterface.imgBoxName, 0, 0,25,
				24, Sprite.TRANS_MIRROR,x+25+Image.getWidth(loadImageInterface.imgBoxName_1)*count-1, y , StaticObj.TOP_RIGHT);
		
		mFont.tahoma_7b_white.drawString(g,strName,x+w/2,y+8,2);
		
	}
	
	public  static void PaintBGListQuest(int x,int y,int w,mGraphics g)
	{
		int count=w/40;
		
		g.drawImage(loadImageInterface.bgQuestConner,x-15,y,mGraphics.LEFT|mGraphics.TOP,true);
		for(int i=0;i<count;i++)
		{
			g.drawImage(loadImageInterface.bgQuestLine,x+Image.getWidth(loadImageInterface.bgQuestLine)*i,y,mGraphics.LEFT|mGraphics.TOP,true);
		}
		
		g.drawRegion(loadImageInterface.bgQuestConner, 0, 0,15,
				42, Sprite.TRANS_MIRROR,x+15+Image.getWidth(loadImageInterface.bgQuestLine)*count-1, y , StaticObj.TOP_RIGHT,true);
		
	}
	public  static void PaintBGListQuestFocus(int x,int y,int w,mGraphics g)
	{
		int count=w/40;
		
		g.drawImage(loadImageInterface.bgQuestConnerFocus,x-15,y,mGraphics.LEFT|mGraphics.TOP,true);
		for(int i=0;i<count;i++)
		{
			g.drawImage(loadImageInterface.bgQuestLineFocus,x+Image.getWidth(loadImageInterface.bgQuestLineFocus)*i,y,mGraphics.LEFT|mGraphics.TOP,true);
		}
		
		g.drawRegion(loadImageInterface.bgQuestConnerFocus, 0, 0,15,
				42, Sprite.TRANS_MIRROR,x+15+Image.getWidth(loadImageInterface.bgQuestLineFocus)*count-1, y , StaticObj.TOP_RIGHT,true);
		
	}
	/*
	 * Paint sub frame
	 */
	public static void SubFrame(int x,int y,int w,int h,mGraphics g)
	{
		
		g.setColor(0x454545);
		g.fillRect(x, y, w, h);
		
		if (GameCanvas.isTouch) {
			int ww=Image.getWidth(loadImageInterface.imgsub_info_conner);
			int hh=Image.getHeight(loadImageInterface.imgsub_info_conner);
			
//			int nWidth=((w-ww*2)/(loadImageInterface.imgsub_frame_info.getWidth()-1))+2;
//			int nHeight=((h-hh*2)/(loadImageInterface.imgsub_frame_info_2.getHeight()-1))+2;
			
			int nWidth=((w-ww*2)/(Image.getWidth(loadImageInterface.imgsub_frame_info)-1))+2;
			int nHeight=((h-hh*2)/(Image.getHeight(loadImageInterface.imgsub_frame_info_2)-1))+2;
			
			g.drawImage(loadImageInterface.imgsub_info_conner, x-3, y-3, mGraphics.TOP | mGraphics.LEFT);
			
			g.drawRegion(loadImageInterface.imgsub_info_conner, 0, 0,ww,
					hh, Sprite.TRANS_MIRROR, x + w+3, y - 3, StaticObj.TOP_RIGHT);
			
			g.drawRegion(loadImageInterface.imgsub_info_conner, 0, 0,ww,
					hh, Sprite.TRANS_MIRROR_ROT180, x - 3, y + h + 3, StaticObj.BOTTOM_LEFT);
			
			g.drawRegion(loadImageInterface.imgsub_info_conner, 0, 0,ww, 
					hh, Sprite.TRANS_ROT180, x + w +3, y + h + 3, StaticObj.BOTTOM_RIGHT);
			//width
			for(int i=0;i<nWidth;i++)
			{
				g.drawImage(loadImageInterface.imgsub_frame_info, x-4 + ww+i*(Image.getWidth(loadImageInterface.imgsub_frame_info)-1), y-3, mGraphics.TOP | mGraphics.LEFT);
			}
			
			//height
			for(int i=0;i<nHeight;i++)
			{
					g.drawRegion(loadImageInterface.imgsub_frame_info_2, 0, 0,Image.getWidth(loadImageInterface.imgsub_frame_info_2),
							Image.getHeight(loadImageInterface.imgsub_frame_info_2), Sprite.TRANS_ROT180, x + w+3, y+hh -3+i*(Image.getHeight(loadImageInterface.imgsub_frame_info_2)-1), StaticObj.TOP_RIGHT);
			}
			//width
			for(int i=0;i<nWidth;i++)
			{
					g.drawRegion(loadImageInterface.imgsub_frame_info, 0, 0,Image.getWidth(loadImageInterface.imgsub_frame_info),
							Image.getHeight(loadImageInterface.imgsub_frame_info), Sprite.TRANS_ROT180,x -4+ ww+i*(Image.getWidth(loadImageInterface.imgsub_frame_info)-1), y+h +3, StaticObj.BOTTOM_LEFT);
			}
			
			//height
			for(int i=0;i<nHeight;i++)
			{
					g.drawRegion(loadImageInterface.imgsub_frame_info_2, 0, 0,Image.getWidth(loadImageInterface.imgsub_frame_info_2),
							Image.getHeight(loadImageInterface.imgsub_frame_info_2),0 , x-3, y+hh - 3+i*(Image.getHeight(loadImageInterface.imgsub_frame_info_2)-1), StaticObj.TOP_LEFT);
//				g.drawRegion(loadImageInterface.imgsub_frame_info_2, 0, 0,Image.getWidth(loadImageInterface.imgsub_frame_info_2),
//						Image.getHeight(loadImageInterface.imgsub_frame_info_2), Sprite.TRANS_ROT180, x + w+3, y+hh -3+i*(Image.getHeight(loadImageInterface.imgsub_frame_info_2)-1), StaticObj.TOP_RIGHT);
			}
			//3 nut
			g.drawImage(loadImageInterface.imgsub_stone_01, x+w/2, y-2, mGraphics.VCENTER | mGraphics.HCENTER);
			g.drawImage(loadImageInterface.imgsub_stone_01, x+w/2, y+2+h, mGraphics.VCENTER | mGraphics.HCENTER);
			
//			g.drawImage(loadImageInterface.img_use, x+20, y+w-5, 0);
//			g.drawImage(loadImageInterface.img_use, x+20+Image.getWidth(loadImageInterface.img_use), y+w-5, 0);
		}
	}
	public static void SubFrame(int x,int y,int w,int h,mGraphics g,int color, int percenOpacity)
	{
		
		g.setColor(0x454545,percenOpacity);
		g.fillRect(x, y, w, h);
		g.disableBlending();
		if (GameCanvas.isTouch) {
			int ww=Image.getWidth(loadImageInterface.imgsub_info_conner);
			int hh=Image.getHeight(loadImageInterface.imgsub_info_conner);
			
//			int nWidth=((w-ww*2)/(loadImageInterface.imgsub_frame_info.getWidth()-1))+2;
//			int nHeight=((h-hh*2)/(loadImageInterface.imgsub_frame_info_2.getHeight()-1))+2;
			
			int nWidth=((w-ww*2)/(Image.getWidth(loadImageInterface.imgsub_frame_info)-1))+2;
			int nHeight=((h-hh*2)/(Image.getHeight(loadImageInterface.imgsub_frame_info_2)-1))+2;
			
			g.drawImage(loadImageInterface.imgsub_info_conner, x-3, y-3, mGraphics.TOP | mGraphics.LEFT);
			
			g.drawRegion(loadImageInterface.imgsub_info_conner, 0, 0,ww,
					hh, Sprite.TRANS_MIRROR, x + w+3, y - 3, StaticObj.TOP_RIGHT);
			
			g.drawRegion(loadImageInterface.imgsub_info_conner, 0, 0,ww,
					hh, Sprite.TRANS_MIRROR_ROT180, x - 3, y + h + 3, StaticObj.BOTTOM_LEFT);
			
			g.drawRegion(loadImageInterface.imgsub_info_conner, 0, 0,ww, 
					hh, Sprite.TRANS_ROT180, x + w +3, y + h + 3, StaticObj.BOTTOM_RIGHT);
			//width
			for(int i=0;i<nWidth;i++)
			{
				g.drawImage(loadImageInterface.imgsub_frame_info, x-4 + ww+i*(Image.getWidth(loadImageInterface.imgsub_frame_info)-1), y-3, mGraphics.TOP | mGraphics.LEFT);
			}
			
			//height
			for(int i=0;i<nHeight;i++)
			{
					g.drawRegion(loadImageInterface.imgsub_frame_info_2, 0, 0,Image.getWidth(loadImageInterface.imgsub_frame_info_2),
							Image.getHeight(loadImageInterface.imgsub_frame_info_2), Sprite.TRANS_ROT180, x + w+3, y+hh -3+i*(Image.getHeight(loadImageInterface.imgsub_frame_info_2)-1), StaticObj.TOP_RIGHT);
			}
			//width
			for(int i=0;i<nWidth;i++)
			{
					g.drawRegion(loadImageInterface.imgsub_frame_info, 0, 0,Image.getWidth(loadImageInterface.imgsub_frame_info),
							Image.getHeight(loadImageInterface.imgsub_frame_info), Sprite.TRANS_ROT180,x -4+ ww+i*(Image.getWidth(loadImageInterface.imgsub_frame_info)-1), y+h +3, StaticObj.BOTTOM_LEFT);
			}
			
			//height
			for(int i=0;i<nHeight;i++)
			{
					g.drawRegion(loadImageInterface.imgsub_frame_info_2, 0, 0,Image.getWidth(loadImageInterface.imgsub_frame_info_2),
							Image.getHeight(loadImageInterface.imgsub_frame_info_2),0 , x-3, y+hh - 3+i*(Image.getHeight(loadImageInterface.imgsub_frame_info_2)-1), StaticObj.TOP_LEFT);
//				g.drawRegion(loadImageInterface.imgsub_frame_info_2, 0, 0,Image.getWidth(loadImageInterface.imgsub_frame_info_2),
//						Image.getHeight(loadImageInterface.imgsub_frame_info_2), Sprite.TRANS_ROT180, x + w+3, y+hh -3+i*(Image.getHeight(loadImageInterface.imgsub_frame_info_2)-1), StaticObj.TOP_RIGHT);
			}
			//3 nut
			g.drawImage(loadImageInterface.imgsub_stone_01, x+w/2, y-2, mGraphics.VCENTER | mGraphics.HCENTER,true);
			g.drawImage(loadImageInterface.imgsub_stone_01, x+w/2, y+2+h, mGraphics.VCENTER | mGraphics.HCENTER,true);
			
//			g.drawImage(loadImageInterface.img_use, x+20, y+w-5, 0);
//			g.drawImage(loadImageInterface.img_use, x+20+Image.getWidth(loadImageInterface.img_use), y+w-5, 0);
		}
	}
	public static void SubFrame(int x,int y,int w,int h,mGraphics g,int color, int percenOpacity,bool isUclip)
	{
		g.setColor(0x454545,percenOpacity);
		g.fillRect(x, y, w, h);
		g.disableBlending();
		if (GameCanvas.isTouch) {
			int ww=Image.getWidth(loadImageInterface.imgsub_info_conner);
			int hh=Image.getHeight(loadImageInterface.imgsub_info_conner);
			
			int nWidth=((w-ww*2)/(Image.getWidth(loadImageInterface.imgsub_frame_info)-1))+2;
			int nHeight=((h-hh*2)/(Image.getHeight(loadImageInterface.imgsub_frame_info_2)-1))+2;
			
			g.drawImage(loadImageInterface.imgsub_info_conner, x-3, y-3, mGraphics.TOP | mGraphics.LEFT,isUclip);
			
			g.drawRegion(loadImageInterface.imgsub_info_conner, 0, 0,ww,
					hh, Sprite.TRANS_MIRROR, x + w+3, y - 3, StaticObj.TOP_RIGHT,isUclip);
			
			g.drawRegion(loadImageInterface.imgsub_info_conner, 0, 0,ww,
					hh, Sprite.TRANS_MIRROR_ROT180, x - 3, y + h + 3, StaticObj.BOTTOM_LEFT,isUclip);
			
			g.drawRegion(loadImageInterface.imgsub_info_conner, 0, 0,ww, 
					hh, Sprite.TRANS_ROT180, x + w +3, y + h + 3, StaticObj.BOTTOM_RIGHT,isUclip);
			//width
			for(int i=0;i<nWidth;i++)
			{
				g.drawImage(loadImageInterface.imgsub_frame_info, x-4 + ww+i*(Image.getWidth(loadImageInterface.imgsub_frame_info)-1), y-3, mGraphics.TOP | mGraphics.LEFT,isUclip);
			}
			
			//height
			for(int i=0;i<nHeight;i++)
			{
					g.drawRegion(loadImageInterface.imgsub_frame_info_2, 0, 0,Image.getWidth(loadImageInterface.imgsub_frame_info_2),
							Image.getHeight(loadImageInterface.imgsub_frame_info_2), Sprite.TRANS_ROT180, x + w+3, y+hh -3+i*(Image.getHeight(loadImageInterface.imgsub_frame_info_2)-1), StaticObj.TOP_RIGHT,isUclip);
			}
			//width
			for(int i=0;i<nWidth;i++)
			{
					g.drawRegion(loadImageInterface.imgsub_frame_info, 0, 0,Image.getWidth(loadImageInterface.imgsub_frame_info),
							Image.getHeight(loadImageInterface.imgsub_frame_info), Sprite.TRANS_ROT180,x -4+ ww+i*(Image.getWidth(loadImageInterface.imgsub_frame_info)-1), y+h +3, StaticObj.BOTTOM_LEFT,isUclip);
			}
			
			//height
			for(int i=0;i<nHeight;i++)
			{
					g.drawRegion(loadImageInterface.imgsub_frame_info_2, 0, 0,Image.getWidth(loadImageInterface.imgsub_frame_info_2),
							Image.getHeight(loadImageInterface.imgsub_frame_info_2),0 , x-3, y+hh - 3+i*(Image.getHeight(loadImageInterface.imgsub_frame_info_2)-1), StaticObj.TOP_LEFT,isUclip);
			}
			g.drawImage(loadImageInterface.imgsub_stone_01, x+w/2, y-2, mGraphics.VCENTER | mGraphics.HCENTER,isUclip);
			g.drawImage(loadImageInterface.imgsub_stone_01, x+w/2, y+2+h, mGraphics.VCENTER | mGraphics.HCENTER,isUclip);
		}
	}
	public static void PaintLine(int x,int y,int width,mGraphics g)
	{
		int nCol=width/Image.getWidth(loadImageInterface.imgLineTrade)+6;
		
		for(int i=0;i<nCol;i++)
		{
			g.drawImage(loadImageInterface.imgLineTrade,x+(Image.getWidth(loadImageInterface.imgLineTrade)-1)*i+15,y, 0);
		}
	}
	
	/*
	 * Paint menu
	 * n: n time : 1n= 1 image, ma= 10 image
	 */
	public static void PaintBGMenuIcon(int x,int y,int n,mGraphics g)
	{
		for(int i=0;i<10;i++)
		{
				g.drawImage(loadImageInterface.imgBGMenuIcon[i],x+(Image.getWidth(loadImageInterface.imgBGMenuIcon[i]))*i,y, 0);
		}
		
		//co gian
//		for(int i=0;i<3;i++)
//		{
//				g.drawImage(loadImageInterface.imgBGMenuIcon[i],x+(loadImageInterface.imgBGMenuIcon[i].getWidth())*i,y, 0);
//		}
//		
//		int count=n-3;
//		for(int i=3;i<count;i++)
//		{
//			g.drawImage(loadImageInterface.imgBGMenuIcon[i],x+(loadImageInterface.imgBGMenuIcon[i].getWidth())*i,y, 0);
//		}
//		
//		xx=loadImageInterface.imgBGMenuIcon[0].getWidth()*(count+2);
//		for(int i=7;i<10;i++)
//		{
//			g.drawImage(loadImageInterface.imgBGMenuIcon[i],xx+(loadImageInterface.imgBGMenuIcon[i].getWidth())*(i-7),y, 0);
//		}
	}
	
	/*
	 * Paint menu
	 * n: n time : 1n= 1 image, ma= 10 image
	 */
	public static void PaintBGSubIcon(int x,int y,int n,mGraphics g)
	{
		if(n<=0)
			n=1;

		int xx=x;
		int width=Image.getWidth(loadImageInterface.imgSubMenu[0]);
		int height=Image.getHeight(loadImageInterface.imgSubMenu[0]);
		
		g.drawImage(loadImageInterface.imgSubMenu[0],xx,y, 0);//conner
		
		for(int i=0;i<n;i++)
		{
			xx+=width;
			g.drawImage(loadImageInterface.imgSubMenu[1],xx,y, 0);
		}
		
		xx+=(width);
		g.drawImage(loadImageInterface.imgSubMenu[2],xx,y, 0);//mid
		
		for(int i=0;i<n;i++)
		{
			xx+=width;
			g.drawImage(loadImageInterface.imgSubMenu[1],xx,y, 0);
		}
		
		//conner
		xx+=(width);
		g.drawRegion(loadImageInterface.imgSubMenu[0],0,0,width,height,Sprite.TRANS_MIRROR, xx, y, 0);
	}
	
	// public paint Item Info
    public static void paintItemInfo(mGraphics g, Item itemInfo, int x, int y)
    { // paint thông tin item
        //		loadImageInterface.ImgItem
        if (itemInfo == null) return;
        g.drawImage(loadImageInterface.ImgItem, x + 12, y + 12, mGraphics.VCENTER | mGraphics.HCENTER);
        if (itemInfo != null && itemInfo.template != null)
        {
            SmallImage.drawSmallImage(g, itemInfo.template.iconID, x + 12, y + 12, 0, mGraphics.VCENTER | mGraphics.HCENTER, true);

            if (itemInfo.template.id != -1 && itemInfo.template.name != null)
            {
                mFont.tahoma_7_white.drawString(g, itemInfo.template.name, x + 28, y, 0);
                mFont.tahoma_7_white.drawString(g, "Lv: " + itemInfo.template.level, x + 28, y + 12, 0);

            }
            int ydem = 0;
            if (!(Item.isMP(itemInfo.template.type) || Item.isBlood(itemInfo.template.type)))
            {
                ydem += 12;
                mFont.tahoma_7_white.drawString(g, "Giới tính: " + (itemInfo.template.gender == 0 ? "Nam" : "Nữ"), x, y + 28, 0);
            }
            if (!Item.isBlood(itemInfo.template.type) && !Item.isMP(itemInfo.template.type))
            {
                mFont.tahoma_7_white.drawString(g, "Class: " + (itemInfo.template.clazz > -1 ? T.nameClass[itemInfo.template.clazz] : "Thể, Ảo, Nhẫn"), x, y + 28 + ydem, 0);
                mFont.tahoma_7_white.drawString(g, "Quốc gia: " + (itemInfo.template.quocgia > -1 ? T.typeCountry[itemInfo.template.quocgia] : "Tất cả"), x, y + 40 + ydem, 0);
                ydem += 24;
            }

            if (itemInfo.template.despaint != null)
                for (int i = 0; i < itemInfo.template.despaint.Length; i++)
                {
                    mFont.tahoma_7_white.drawString(g, itemInfo.template.despaint[i], x, y + 28 + ydem + 12 * i, 0);

                }

        }


        if (itemInfo != null && itemInfo.options != null)
            for (int i = 0; i < itemInfo.options.size(); i++)
            {
                ItemOption option = (ItemOption)itemInfo.options.elementAt(i);
                paintMultiLine(g, mFont.tahoma_7_blue, itemInfo.isTypeUIShopView() ? option.getOptionShopString() : option.getOptionString(),
                        x + 8, y += 12, mFont.LEFT);
            }
    }
    public static void paintItemInfo(mGraphics g, Item itemInfo, int x, int y, bool paintGia)
    { // paint thông tin item
        //		loadImageInterface.ImgItem
        if (itemInfo == null) return;
        g.drawImage(loadImageInterface.ImgItem, x + 12, y + 12, mGraphics.VCENTER | mGraphics.HCENTER);
        if (itemInfo != null && itemInfo.template != null)
        {
            if (itemInfo.template.id != -1 && itemInfo.template.name != null)
            {
                mFont.tahoma_7_white.drawString(g, itemInfo.template.name, x + 28, y, 0);
                mFont.tahoma_7_white.drawString(g, "Lv: " + itemInfo.template.level, x + 28, y + 12, 0);

            }

            SmallImage.drawSmallImage(g, itemInfo.template.iconID, x + 12, y + 12, 0, mGraphics.VCENTER | mGraphics.HCENTER, true);

            int ydem = 0;
            if (!Item.isBlood(itemInfo.template.type) && !Item.isMP(itemInfo.template.type))
            {
                ydem = 36;
                mFont.tahoma_7_white.drawString(g, "Giới tính: " + (itemInfo.template.gender == 0 ? "Nam" : "Nữ"), x, y + 28, 0);
                mFont.tahoma_7_white.drawString(g, "Class: " + (itemInfo.template.clazz > -1 ? T.nameClass[itemInfo.template.clazz] : "Tất cả"), x, y + 40, 0);
                mFont.tahoma_7_white.drawString(g, "Quốc gia: " + (itemInfo.template.quocgia > -1 ? T.typeCountry[itemInfo.template.quocgia] : "Tất cả"), x, y + 52, 0);
            }
            if (paintGia)
                mFont.tahoma_7_white.drawString(g, "Giá: " + (itemInfo.template.gia), x, y + 28 + ydem, 0);

            if (itemInfo.template.despaint != null)
                for (int i = 0; i < itemInfo.template.despaint.Length; i++)
                {
                    mFont.tahoma_7_white.drawString(g, itemInfo.template.despaint[i], x, y + 28 + ydem + (paintGia == true ? 12 : 0) + 12 * i, 0);

                }

        }


        if (itemInfo != null && itemInfo.options != null)
            for (int i = 0; i < itemInfo.options.size(); i++)
            {
                ItemOption option = (ItemOption)itemInfo.options.elementAt(i);
                paintMultiLine(g, mFont.tahoma_7_blue, itemInfo.isTypeUIShopView() ? option.getOptionShopString() : option.getOptionString(),
                        x + 8, y += 12, mFont.LEFT);
            }
    }
	public static void paintMultiLine(mGraphics g, mFont f, string str, int x, int y, int align) {
		int a = (GameCanvas.isTouch && GameCanvas.w >= 320) ? 20 : 10;
		int yTemp = y;
		string[] arr = f.splitFontArray(str, 20 - a);
		for (int i = 0; i < arr.Length; i++) {
			if (i == 0)
				f.drawString(g, arr[i], x, y, align);
			else {
				if (i * GameScr.scrMain.ITEM_SIZE + yTemp >= (GameScr.scrMain.cmy - 12) && i * GameScr. scrMain.ITEM_SIZE < GameScr.scrMain.cmy + GameScr.popupH - 44) {
					f.drawString(g, arr[i], x, y += 12, align);
//					GameScr.yPaint += 12;
				} else
					y += 12;
				GameScr.indexRowMax++;
			}
		}
	}

	public void setAntiAlias(bool b) {
		// TODO Auto-generated method stub
		
	}

	public void setStrokeWidth(float a) {
		// TODO Auto-generated method stub
		
	}

	public void setColor(int i) {
		// TODO Auto-generated method stub
		
	}

	public void setAlpha(int a) {
		// TODO Auto-generated method stub
		
	}
    public static void paintFocus(mGraphics g, int x, int y, int w, int h, int coutFc)
    {
        g.drawImage(loadImageInterface.imgFocusSelectItem0, x - 2 + coutFc, y - 2 + coutFc, 0, false);
        g.drawRegion(loadImageInterface.imgFocusSelectItem0,
                0, 0, loadImageInterface.imgFocusSelectItem0.getWidth(), loadImageInterface.imgFocusSelectItem0.getHeight(),
                Sprite.TRANS_MIRROR, x + w + 3 - coutFc, y - 2 + coutFc, mGraphics.TOP | mGraphics.RIGHT, false);

        g.drawRegion(loadImageInterface.imgFocusSelectItem0,
                0, 0, loadImageInterface.imgFocusSelectItem0.getWidth(), loadImageInterface.imgFocusSelectItem0.getHeight(),
                Sprite.TRANS_MIRROR_ROT90, x + w + 3 - coutFc, y + h + 3 - coutFc, mGraphics.BOTTOM | mGraphics.RIGHT, false);

        g.drawRegion(loadImageInterface.imgFocusSelectItem0,
                0, 0, loadImageInterface.imgFocusSelectItem0.getWidth(), loadImageInterface.imgFocusSelectItem0.getHeight(),
                Sprite.TRANS_ROT270, x - 2 + coutFc, y + h + 3 - coutFc, mGraphics.BOTTOM | mGraphics.LEFT, false);
    }
}
