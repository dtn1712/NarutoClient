

using System;
public class ServerEffect : Effect2 {
	public EffectCharPaint eff;
	int i0, x, y, dir = 1;
	Char c;

    public bool isloopForever;
	short loopCount = 0;
	private long endTime = 0;
	
	public static void addServerEffect(int id, int cx, int cy, int loopCount) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScr.efs[id];
		e.x = cx;
		e.y = cy;
		e.loopCount = (short) loopCount;
		vEffect2.addElement(e);

        Cout.println(GameScr.efs.Length + "id  " + id + " add Effect2.vEffect2  " + Effect2.vEffect2.size());
	}

    public static void addServerEffect(int id, int cx, int cy, bool loopForever)
    {
        ServerEffect e = new ServerEffect();
        e.eff = GameScr.efs[id];
        e.x = cx;
        e.y = cy;
        e.isloopForever = loopForever;
        vEffect2.addElement(e);
            Cout.println("add 222Effect2.vEffect2  " + Effect2.vEffect2.size());
    }
    public static void addServerEffect(int id, int cx, int cy, int loopCount, sbyte dir)
    {
        try
        {
            ServerEffect e = new ServerEffect();
            e.eff = GameScr.efs[id];
            e.x = cx;
            e.y = cy;
            e.loopCount = (short)loopCount;
            e.dir = dir;
            vEffect2.addElement(e);
                Cout.println("add 333 Effect2.vEffect2  " + Effect2.vEffect2.size());
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }
	public static void addServerEffect(int id, Char c, int loopCount) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScr.efs[id];
		e.c = c;
		e.loopCount = (short) loopCount;
        vEffect2.addElement(e);
            Cout.println("add 444 Effect2.vEffect2  " + Effect2.vEffect2.size());
	}

	public static void addServerEffectWithTime(int id, int cx, int cy, int timeLengthInSecond) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScr.efs[id];
		e.x = cx;
		e.y = cy;
		e.endTime = mSystem.currentTimeMillis() + timeLengthInSecond * 1000;
        vEffect2.addElement(e);
            Cout.println("add 555 Effect2.vEffect2  " + Effect2.vEffect2.size());
	}

	public static void addServerEffectWithTime(int id, Char c, int timeLengthInSecond) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScr.efs[id];
		e.c = c;
		e.endTime = mSystem.currentTimeMillis() + timeLengthInSecond * 1000;
        vEffect2.addElement(e);
            Cout.println("add 666 Effect2.vEffect2  " + Effect2.vEffect2.size());
	}
	

	public override void paint(mGraphics g) {
		try{
			if (c != null) {
				x = c.cx;
				y = c.cy;
			}
			int xp, yp;
			xp = x + eff.arrEfInfo[i0].dx * dir;
			yp = y + eff.arrEfInfo[i0].dy;
			if (GameCanvas.isPaint(xp, yp)) {
				
				SmallImage.drawSmallImage(g, eff.arrEfInfo[i0].idImg, xp, yp, (dir == 1) ? 0 : 2, mGraphics.VCENTER | mGraphics.HCENTER);
			}
		}catch (Exception e) {
			// TODO: handle exception
		//	e.printStackTrace();
		}
		
		
	}

	public override void update() {



        if (endTime != 0)
        {
            i0++;
            if (i0 >= eff.arrEfInfo.Length)
            {
                i0 = 0;
            }
            if (mSystem.currentTimeMillis() - endTime > 0)
            {
                //				if(eff.idEf == 120)
                //					GameCanvas.isBallEffect = false;
                vEffect2.removeElement(this);
            }
        }
        else if (!isloopForever)
        {
            i0++;
            if (i0 >= eff.arrEfInfo.Length)
            {
                loopCount--;
                if (loopCount <= 0)
                {
                    //					if(eff.idEf == 120)
                    //						GameCanvas.isBallEffect = false;
                    vEffect2.removeElement(this);
                }
                else
                    i0 = 0;
            }
        }
        else
        {
            i0++;
            if (i0 >= eff.arrEfInfo.Length)
                i0 = 0;
        }
        if (GameCanvas.gameTick % 11 == 0 && c != null && c != Char.myChar())
        {
            if (!GameScr.vCharInMap.contains(this.c))
            {

                vEffect2.removeElement(this);
            }
        }
    }

}
