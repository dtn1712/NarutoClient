


using System;
public class Clan {
	

	public const int CREATE_CLAN			= 0;
	public const int MOVE_OUT_MEM		= 1;
	public const int MOVE_INPUT_MONEY	= 2;
	public const int MOVE_OUT_MONEY		= 3;
	public const int FREE_MONEY			= 4;
	public const int UP_LEVEL			= 5;
	
	public const int TYPE_NORMAL			= 0;
	public const int TYPE_UUTU			= 1;
	public const int TYPE_TRUONGLAO		= 2;
	public const int TYPE_TOCPHO			= 3;
	public const int TYPE_TOCTRUONG		= 4;
	
	public string name = "";
	public int exp, expNext;
	public int level, itemLevel;
	public int icon;
	public int openDun;
	public int coin, freeCoin, coinUp;
	public string main_name = "";
	public string assist_name = "";
	public string elder1_name = "";
	public string elder2_name = "";
	public string elder3_name = "";
	public string elder4_name = "";
	public string elder5_name = "";
	public string reg_date = "";
	public string log = "";
	public string alert = "";
	public int total, use_card;
	public mVector members = new mVector();
	public Item[] items;

	public void writeLog(string data){
        string[] strs = Util.split(data, "\n");
		log = "";
		try{
			for (int i = 0; i < strs.Length; i++) {
				string str = strs[i].Trim();
				if(!str.Equals("")){
					try{
						string[] datas = Util.split(str, ",");
						string value = datas[0];
						int type = int.Parse(datas[1]);
						if(type == CREATE_CLAN){
							value = "c0" + value;
							value += mResources.CLAN_ACTIVITY[1]+" " + Util.numberToString(datas[2]) + " "+mResources.CLAN_ACTIVITY[0]+" " + datas[3];
						}
						else if(type == MOVE_OUT_MEM){
							value = "c1" + value;
							value += " " + mResources.CLAN_ACTIVITY[2] + " " + Util.numberToString(datas[2]) + " "+mResources.CLAN_ACTIVITY[0]+" " + datas[3];
						}
						else if(type == MOVE_INPUT_MONEY){
							value = "c2" + value;
							value += " "+mResources.CLAN_ACTIVITY[3]+" " + Util.numberToString(datas[2]) + " "+mResources.CLAN_ACTIVITY[0]+" " + datas[3];
						}
						else if(type == MOVE_OUT_MONEY){
							value = "c1" + value;
							value += " " + mResources.CLAN_ACTIVITY[4]+ " " + Util.numberToString(datas[2]) + " "+mResources.CLAN_ACTIVITY[0]+" " + datas[3];
						}
						else if(type == FREE_MONEY){
							value = "c1" + value;
							value += mResources.CLAN_ACTIVITY[5]+" " + Util.numberToString(datas[2]) + " "+mResources.CLAN_ACTIVITY[0]+" " + datas[3];
						}
						else if(type == UP_LEVEL){
							value = "c2" + value;
							value += " " + mResources.CLAN_ACTIVITY[6] + " " + Util.numberToString(datas[2]) + " " + mResources.CLAN_ACTIVITY[0] + " " + datas[3];
						}
						
						log += value + "\n";
					}catch (Exception e) {}
				}
			}
		}catch (Exception e) {}
	}
}