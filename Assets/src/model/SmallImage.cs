

using System;
public class SmallImage {
	public static int[][] smallImg;
	public static SmallImage instance;
	public static mBitmap[] imgbig;
    public static mHashtable imgNew = new mHashtable();
    public static mHashtable img_big = new mHashtable();
	public static mBitmap imgEmpty = null;

    public static string pathBigImage = "x" + mGraphics.zoomLevel + "/img/big_";
    public static string pathObjectMap = "x" + mGraphics.zoomLevel + "/mapobject/";
    public static string pathAvataNPC = "x" + mGraphics.zoomLevel + "/npc/";
    public static string pathMob = "x" + mGraphics.zoomLevel + "/mob/";
    public static string keyOKDownloaded = "download_bigimg";
    public static int nBigImage = 400;

    public static int ID_ADD_MAPOJECT = 1000;
    public static int ID_ADD_AVATARNPC = 20000;
    public static int ID_ADD_MOB = 21000;

    public static String getPathImage(int id)
    {
        if (id < 1000) return pathBigImage;
        else if (id < 20000)
            return pathObjectMap;
        else if (id < 21000)
            return pathAvataNPC;
        else return pathMob;
    }
	


	public static SmallImage gI() {
		if (instance == null) {
			instance = new SmallImage();
		}
		return instance;
	}
	
	public static void freeBig()
    {
        imgbig = null;
        mSystem.my_Gc();
        img_big.clear();
	}
	public static void loadBigImage() {
        imgbig = null;
        mSystem.gcc();
        imgbig = new mBitmap[31];
        BgItem.imgobj = new mBitmap[400];
        //		imgbig = new mBitmap[] { 
        //				GameCanvas.loadImage("/img/big_0.png"),
        //				GameCanvas.loadImage("/img/big_1.png"), 
        //				GameCanvas.loadImage("/img/big_2.png"),
        //				GameCanvas.loadImage("/img/big_3.png"), 
        //				GameCanvas.loadImage("/img/big_4.png"),
        //				GameCanvas.loadImage("/img/big_5.png"), 
        //				GameCanvas.loadImage("/img/big_6.png"),
        //				GameCanvas.loadImage("/img/big_7.png"), 
        //				GameCanvas.loadImage("/img/big_8.png"),
        //				GameCanvas.loadImage("/img/big_9.png"), 
        //				GameCanvas.loadImage("/img/big_10.png"),
        //				GameCanvas.loadImage("/img/big_11.png"), 
        //				GameCanvas.loadImage("/img/big_12.png"),
        //				GameCanvas.loadImage("/img/big_13.png"), 
        //				GameCanvas.loadImage("/img/big_14.png"),
        //				GameCanvas.loadImage("/img/big_15.png"), 
        //				GameCanvas.loadImage("/img/big_16.png"),
        //				GameCanvas.loadImage("/img/big_17.png"),
        //				GameCanvas.loadImage("/img/big_18.png"),
        //				GameCanvas.loadImage("/img/big_19.png"), 
        //				GameCanvas.loadImage("/img/big_20.png"),
        //				GameCanvas.loadImage("/img/big_21.png"), 
        //				GameCanvas.loadImage("/img/big_22.png"),
        //				GameCanvas.loadImage("/img/big_23.png"),
        //				GameCanvas.loadImage("/img/big_24.png"),
        //				GameCanvas.loadImage("/img/big_25.png"), 
        //				GameCanvas.loadImage("/img/big_26.png"),
        //				GameCanvas.loadImage("/img/big_27.png"),
        //				GameCanvas.loadImage("/img/big_28.png"), 
        //				GameCanvas.loadImage("/img/big_29.png"), 
        //				GameCanvas.loadImage("/img/big_30.png"), 
        //				};
        imgEmpty = new mBitmap(Image.createImage(1, 1));
    }


	public SmallImage() {
		readImage();
	}

	public static void init() {
		instance = null;
		instance = new SmallImage();

	}
    public static int[] IdBigImage;
	public static void readImage() {
		try {
			DataInputStream file;
//			file = new DataInputStream(this.getClass().getResourceAsStream("/img/nj_image"));
			file = new DataInputStream(Rms.loadRMS("nj_image"));
//			file = GameCanvas.readdatafile("file/shinobi_image");
            nBigImage = file.readShort();
			int sum = file.readShort();
			Cout.println("SUM IMAGE ---> "+sum);
//			sum = 7;
			IdBigImage = new int[sum];
			smallImg = new int[sum][];
            for (int i = 0; i < smallImg.Length; i++)
            {
                smallImg[i] = new int[7];
            }
			for (int i = 0; i < sum; i++) {
				IdBigImage[i] = i;
				smallImg[i][0] = file.readShort();// id hình lớn //
				//System.out.println("ID HINH LON ----> "+smallImg[i][0]);
				smallImg[i][1] = file.readShort();// x cắt
				smallImg[i][2] = file.readShort();// y cắt
				smallImg[i][3] = file.readShort();// w cắt
				smallImg[i][4] = file.readShort();// h cắt
			}

			//System.out.println("SUM IMAGE okkkkk---> "+sum);
		} catch (Exception ex) {
			//ex.printStackTrace();
		}
	}
	public static bool isExitsImage(int id){
		if(id >= smallImg.Length
				|| smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth() 
				|| smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
				|| smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
				|| smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()
			){
			
			Image img = (Image) imgNew.get(id + "");
			if (img == null || img.Equals(imgEmpty)) {
				Service.gI().requestIcon(id);
				return false;
			}
		}
		return true;
	}
	
	public static int getWith(int id) {
		return smallImg[id][5];
	}

	public static int getHeight(int id) {
		return smallImg[id][6];
	}


    public static void drawSmallImage(mGraphics g, int id, int x, int y, int transform, int anchor)
    {

        //loadFromServer(id);
        //		System.out.println(smallImg[id][0]+" smallImg[id][0] ----> "+imgbig.length);
        try
        {
            mBitmap img = null;
            if (imgbig[smallImg[id][0]] != null && (id >= smallImg.Length || smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth()
                    || smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
                    || smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
                    || smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()))
            {

                Cout.println(id + " BIG IMAGE ---->  1111111  " );
                //				if(id >= 926 && id <= 1051){
                //					Image img = (Image) imgNew.get(id+"");
                //					if(img == null)
                //						System.out.println("IMAGE NULLL K PAINT");
                //				}

                img = (mBitmap)imgNew.get(id + "");
                //				if(img == null){
                //					System.out.println("ImageNull");
                //					imgNew.put(id+"", imgEmpty);
                //					Service.gI().requestIcon(id);
                //				}
                //				else
                if (img != null)
                {
                    g.drawRegion(img, 0, 0, img.getWidth(), img.getHeight(), transform, x, y, anchor);
                }
            }
            else
            {

                if (id == 1029 || id == 1030 || id == 1028 || id == 1031 || id == 1038)
                {
                    //					System.out.println(smallImg[id][0]+","+smallImg[id][1]+" , "+ smallImg[id][2]+ " , "+smallImg[id][3]+" , "+smallImg[id][4]+" --------> "+imgbig[smallImg[id][0]]+" , "+ x +" , "+y);
                    //					GameScr.startFlyText("FUCK", x, y, 0, 2, mFont.RED);
                    //					g.setColor(0xffffff);
                    //					g.drawRect(x, y, 100, 100);
                }

                if (imgbig[smallImg[id][0]] != null)
                    g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4], transform, x, y, anchor);
                else
                {
                    mBitmap imgput = createBigImage(smallImg[id][0]);
                    if (imgput != null)
                        g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4], transform, x, y, anchor);

                }
            }
        }
        catch (Exception e)
        {
            // e.printStackTrace();

        }

    }
    public static mBitmap createBigImage(int id)
    {
        //		GameCanvas.loadImage("/img/big_0.png")

        //		mBitmap img = (mBitmap)img_big.get(id+"");

        Cout.println(id + " createBigImage ----> " + smallImg.Length);
        if (imgbig[id] == null)
        {
            imgbig[id] = GameCanvas.loadImage("/img/big_" + id + ".png");

            //			if(img!=null){
            //				imgbig[id] = img;
            ////				img_big.put(id+"", img);
            //			}else{
            //				//request serrver
            //			}
        }
        return imgbig[id];
    }
    public static void CleanImg()
    {
        try
        {
            for (int i = 0; i < imgbig.Length; i++)
            {
                if (imgbig[i] != null && imgbig[i].image != null)
                    imgbig[i].cleanImg();
                if (imgbig[i] != null)
                    imgbig[i] = null;
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }
    public static void drawSmallImageIcon(mGraphics g, int id, int x, int y, int transform, int anchor)
    {

        try
        {
            mBitmap img = null;
            if (imgbig[smallImg[id][0]] != null && (id >= smallImg.Length || smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth()
                    || smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
                    || smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
                    || smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()))
            {
                img = (mBitmap)imgNew.get(id + "");
                if (img != null)
                {
                    g.drawRegion(img, 0, 0, img.getWidth(), img.getHeight(), transform, x + img.getWidth() / 2, y + img.getHeight() / 2, anchor);
                }
            }
            else
            {
                if (id == 1029 || id == 1030 || id == 1028 || id == 1031 || id == 1038)
                {
                    //					GameScr.startFlyText("FUCK", x, y, 0, 2, mFont.RED);
                }
                if (imgbig[smallImg[id][0]] != null)
                    g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4], transform, x, y, anchor);
                else
                {
                    mBitmap imgput = createBigImage(smallImg[id][0]);
                    if (imgput != null)
                        g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4], transform, x, y, anchor);

                }
            }
        }
        catch (Exception e)
        {
          //  e.printStackTrace();
        }

    }

public static void drawSmallImage(mGraphics g, int id, int x, int y, int transform, int anchor,bool isUclip) {
		
		//loadFromServer(id);
    try
    {
        mBitmap img = null;
        if (imgbig[smallImg[id][0]] != null && (id >= smallImg.Length
                 || smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth()
                || smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
                || smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
                || smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()
            /*|| smallImg[id][5] >= imgbig[smallImg[id][0]].getHeight()
            || smallImg[id][6] >= imgbig[smallImg[id][0]].getHeight()*/))
        {
            //				if(id >= 926 && id <= 1051){
            //					Image img = (Image) imgNew.get(id+"");
            //					if(img == null)
            //						System.out.println("IMAGE NULLL K PAINT");
            //				}
				
				
				img = (mBitmap) imgNew.get(id+"");
//				if(img == null){
//					System.out.println("ImageNull");
//					imgNew.put(id+"", imgEmpty);
//					Service.gI().requestIcon(id);
//				}
//				else
					if(img != null){
						g.drawRegion(img, 0, 0, img.getWidth(), img.getHeight(), transform, x, y, anchor,isUclip);
					}
			}
        else
        {
            if (id == 1029 || id == 1030 || id == 1028 || id == 1031 || id == 1038)
            {
                //					System.out.println(smallImg[id][0]+","+smallImg[id][1]+" , "+ smallImg[id][2]+ " , "+smallImg[id][3]+" , "+smallImg[id][4]+" --------> "+imgbig[smallImg[id][0]]+" , "+ x +" , "+y);
                //					GameScr.startFlyText("FUCK", x, y, 0, 2, mFont.RED);
                //					g.setColor(0xffffff);
                //					g.drawRect(x, y, 100, 100);
            }
            if (imgbig[smallImg[id][0]] != null)
                g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4], transform, x, y, anchor, isUclip);
            else
            {
                mBitmap imgput = createBigImage(smallImg[id][0]);
                if (imgput != null)
                    g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4], transform, x, y, anchor, isUclip);

            }
        }
		}catch(Exception e){
		//	e.printStackTrace();
		}
		
	}
public static void drawSmallImageScalse(mGraphics g, int id, int x, int y, int transform, int anchor, Boolean isUclip, int perScale)
{

    //loadFromServer(id);
    try
    {
        mBitmap img = null;
        if (imgbig[smallImg[id][0]] != null && (id >= smallImg.Length
                 || smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth()
                || smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
                || smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
                || smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()
            /*|| smallImg[id][5] >= imgbig[smallImg[id][0]].getHeight()
            || smallImg[id][6] >= imgbig[smallImg[id][0]].getHeight()*/))
        {
            img = (mBitmap)imgNew.get(id + "");
            if (img != null)
            {
                g.drawRegion(img, 0, 0, img.getWidth(), img.getHeight(), transform, x, y, anchor, isUclip);
            }
        }
        else
        {
            if (id == 1029 || id == 1030 || id == 1028 || id == 1031 || id == 1038)
            {
            }
            if (imgbig[smallImg[id][0]] != null)
                g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4], transform, x, y, anchor, isUclip);
            else
            {
                mBitmap imgput = createBigImage(smallImg[id][0]);
                if (imgput != null)
                    g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4], transform, x, y, anchor, isUclip);

            }
        }
    }
    catch (Exception e)
    {
        //e.printStackTrace();
    }

}
//drawRegionScalse
public static void drawSmallImage(mGraphics g, int id, int x, int y, int transform, int anchor, Boolean isUclip, int opacity)
{

    try
    {
        mBitmap img = null;
        if (imgbig[smallImg[id][0]] != null && (id >= smallImg.Length
                 || smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth()
                || smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
                || smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
                || smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()
            /*|| smallImg[id][5] >= imgbig[smallImg[id][0]].getHeight()
            || smallImg[id][6] >= imgbig[smallImg[id][0]].getHeight()*/))
        {
            //			if(id >= 926 && id <= 1051){
            //				Image img = (Image) imgNew.get(id+"");
            //				if(img == null)
            //					System.out.println("IMAGE NULLL K PAINT");
            //			}

            img = (mBitmap)imgNew.get(id + "");
            //			if(img == null){
            //				System.out.println("ImageNull");
            //				imgNew.put(id+"", imgEmpty);
            //				Service.gI().requestIcon(id);
            //			}
            //			else
            if (img != null)
            {
                g.drawRegion(img, 0, 0, img.getWidth(), img.getHeight(), transform, x, y, anchor, isUclip, opacity);
            }
        }
        else
        {
            if (id == 1029 || id == 1030 || id == 1028 || id == 1031 || id == 1038)
            {
                //				System.out.println(smallImg[id][0]+","+smallImg[id][1]+" , "+ smallImg[id][2]+ " , "+smallImg[id][3]+" , "+smallImg[id][4]+" --------> "+imgbig[smallImg[id][0]]+" , "+ x +" , "+y);
                //				GameScr.startFlyText("FUCK", x, y, 0, 2, mFont.RED);
                //				g.setColor(0xffffff);
                //				g.drawRect(x, y, 100, 100);
            }
            if (imgbig[smallImg[id][0]] != null)
                g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4], transform, x, y, anchor, isUclip, opacity);
            else
            {
                mBitmap imgput = createBigImage(smallImg[id][0]);
                if (imgput != null)
                    g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4], transform, x, y, anchor, isUclip, opacity);

            }
        }
    }
    catch (Exception e)
    {
       // e.printStackTrace();
    }

}
//	public static void loadFromServer(Part body){
//		short id = GameScr.parts[].template.iconID;
//		if(id == 1891){
//			System.out.println("zzzzzzzzzzz");
//			for(int i = 0; i < 26; i++){
//				id += i;
//				Image img = (Image) imgNew.get(id+"");
//				if(img == null){
//					imgNew.put(id+"", imgEmpty);
//					Service.gI().requestIcon(id);
//			}
//		}
//	}
//	
//	}
}
