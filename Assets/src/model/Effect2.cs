
public abstract class Effect2 {
	public static mVector vEffect2 = new mVector();
	public static mVector vRemoveEffect2 = new mVector();
	public static mVector vEffect2Outside = new mVector();
	public static mVector vAnimateEffect = new mVector();

    public virtual void update() { }
    public virtual void paint(mGraphics g) { }
}
