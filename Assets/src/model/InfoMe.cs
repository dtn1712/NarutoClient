


public class InfoMe {
	public static mVector infoWaitToShow = new mVector();
	public static InfoItem info;
	public static int p1 = 5, p2, p3, x, strWidth;
	public static int limLeft = 2;
	public static int hI = 20;
	public static void paint(mGraphics g) {
		int xI = limLeft, yI = GameCanvas.h - 23, wI = GameCanvas.w;
		if (GameCanvas.isTouch) {
			if (GameCanvas.w >= 450) {
				xI = 130;
				wI = GameCanvas.w - 2 * xI;
			} else {
				xI = 80;
				wI = GameCanvas.w - 2 * xI - 10;
			}
			yI = GameCanvas.h - 60;
			limLeft = xI + 2;
		}
		if (info == null)
			return;
		if ((GameCanvas.currentDialog != null && GameCanvas.currentDialog.center != null))
			return;
		g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
		
		if(GameCanvas.isTouch)
            Paint.paintFrame(xI, yI - 4, wI + 10, hI + 8, g);
		else{
			g.setColor(0);
			g.fillRect(xI - 2, yI, wI + 2, hI);
		}
		g.setClip(xI, yI, wI, hI);
//		info.f.drawString(g, info.s, x, yI + 3, 0);
		
	}

	public static void update() {
	
		if (p1 == 0) // Chay tu phai qua trai
		{
			x += (limLeft - x) / 3;
			if (x - limLeft < 3) {
				x = limLeft + 2;
				p1 = 2;
				p2 = 0;
			}
		} else if (p1 == 2) {
			p2++;
			if (p2 > info.speed) {
				p1 = 3;
				p2 = 0;
			}
		} else if (p1 == 3) {
			if (x + strWidth < limLeft+GameCanvas.w-20)				
				x -= 6;
			else
				x -= 2;
			if (x + strWidth < limLeft) {
				p1 = 4;
				p2 = 0;
			}
		} else if (p1 == 4) {
			p2++;
			if (p2 > 10) {
				p1 = 5;
				p2 = 0;
			}
		} else if (p1 == 5) {
			if (infoWaitToShow.size() > 0) {
				InfoItem next = (InfoItem) infoWaitToShow.firstElement();
				infoWaitToShow.removeElementAt(0);
				if (info != null && next.s.Equals(info.s)) {
					return;
				}
				info = next;
//				strWidth = info.f.getWidth(info.s);
				p1 = p2 = 0;
				x = GameCanvas.w;
			} else{
				info = null;
			
			}	
		}
		
	

	}

	public static void addInfo(string s) {

		if(canMergeString(s))return;
		if (GameCanvas.w == 128)
			limLeft = 1;
		if(infoWaitToShow.size()>10)
			infoWaitToShow.removeElementAt(0);
		infoWaitToShow.addElement(new InfoItem(s));
		
	}

	private static bool canMergeString(string s) {
		if (info != null && info.s != null && s.Equals(info.s))
			return true;
		
		if (infoWaitToShow.size() > 0 && s.Equals(((InfoItem) (infoWaitToShow.lastElement())).s))
			return true;
		if (s.Length < 8)
			return false;
		
		if (info != null && info.s != null && p1 < 3 && info.s.Length >= 8)
		{
			string m1 = mSystem.substring(s,0, 8);
			string m2 = mSystem.substring(info.s,0,8);
			if(m1.Equals(m2))
			{
				int i=7;
				for(;i<s.Length;i++)
				{
					if(i>=info.s.Length)break;
                    if ((int)s[i] >= 48 && (int)s[i] <= 57) break;
                    if (s[i] != info.s[i]) break;
				
				}
				string append = mSystem.substring(s,i,s.Length);
				info.s+=", "+append;
				p1=2;p2=0;
				return true;
			}
		}
		if(infoWaitToShow.size()>0)
		{
			string s2=((InfoItem)(infoWaitToShow.lastElement())).s;
			if (s2.Length >= 8)
			{
				string m1 = mSystem.substring(s,0, 8);
				string m2 = mSystem.substring(s2,0,8);
				if(m1.Equals(m2))
				{
					int i=7;
					for(;i<s.Length;i++)
					{
						if(i>=s2.Length)break;
						if ((int) s[i] >= 48 && (int) s[i] <= 57) break;
						if(s[i]!=s2[i])break;
					}
					string append = mSystem.substring(s,i,s.Length);
					s2+=", "+append;
					return true;
				}
			}
		}
		return false;
	}

	public static void addInfo(string s, int speed, mFont f) {
		if(canMergeString(s))return;
		if (GameCanvas.w == 128)
			limLeft = 1;
		if(infoWaitToShow.size()>10)
			infoWaitToShow.removeElementAt(0);
//		infoWaitToShow.addElement(new InfoItem(s, f, speed));
	}

	public static bool isEmpty() {
		return p1 == 5 && infoWaitToShow.size() == 0;
	}
}
