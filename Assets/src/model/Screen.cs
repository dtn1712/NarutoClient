

public abstract class Screen {
	public static iCommand left1, right1, center1;
	public Command left, center;
	public Command right, cmdClose;
    public bool isConnect = false, connectCallBack = false, isCloseConnect = false;

	public static int ITEM_HEIGHT= 18 * mGraphics.zoomLevel;
	public static int cmdW = 70 * mGraphics.zoomLevel;
	public static int cmdH = 22;
	public static int keyTouch = -1;
	public static int height,width;
	public static Screen lastScreen ;
    public static UnityEngine.ScreenOrientation orientation;
    public static bool fullScreen = false;

    public virtual void switchToMe()
    { // chỉ vào chỗ này 
        GameCanvas.currentScreen = this;
        GameCanvas.clearKeyPressed();
        GameCanvas.clearPointerEvent();
        if (GameCanvas.currentScreen != null)
            GameCanvas.currentScreen.unLoad();
        keyTouch = -1;
	}

	public virtual void unLoad() {
		// OVERRIDE THIS TO FREE RESOURCE
	}

	public static void initPos() {
		if (GameCanvas.isTouch)
			cmdH = 26;
		else
			cmdH = 24;
	}

	public virtual void keyPress(int keyCode) {
	}

	public virtual void update() {
		
	}

	public virtual void updateKey() {
		if (GameCanvas.keyPressed[5] || getCmdPointerLast(GameCanvas.currentScreen.center)) {
			GameCanvas.keyPressed[5] = false;
			Screen.keyTouch = -1;
			GameCanvas.isPointerJustRelease = false;
			if (center != null) {
				center.performAction();
			}
			if (center1 != null) {
				GameCanvas.clearKeyPressed();
				GameCanvas.clearKeyHold();
				center1.perform();

			}
		}
		if (GameCanvas.keyPressed[12] || getCmdPointerLast(GameCanvas.currentScreen.left)) {
			GameCanvas.keyPressed[12] = false;
			Screen.keyTouch = -1;
			GameCanvas.isPointerJustRelease = false;
			if (ChatTextField.gI().isShow){
				if(ChatTextField.gI().left!=null)ChatTextField.gI().left.performAction();
			}
			else if (left != null) {
				left.performAction();
			}
			if (left1 != null) {
				// perform(left);
				GameCanvas.clearKeyPressed();
				GameCanvas.clearKeyHold();
				left1.perform();
			}

		}
		if (GameCanvas.keyPressed[13] || getCmdPointerLast(GameCanvas.currentScreen.right)) {
			GameCanvas.keyPressed[13] = false;
			Screen.keyTouch = -1;
			GameCanvas.isPointerJustRelease = false;
			if (ChatTextField.gI().isShow){
				if(ChatTextField.gI().right!=null)ChatTextField.gI().right.performAction();
			}
			else if (right != null) {
				right.performAction();
			}
			
			if (right1 != null) {
				// perform(right);
				GameCanvas.clearKeyPressed();
				GameCanvas.clearKeyHold();
				right1.perform();

			}
		}
//		updatePointer();
	}


	public static bool getCmdPointerLast(Command cmd) {
        if (cmd == null)
            return false;
        if (true)
        {//cmd.x != 0 && cmd.y != 0
            if (!GameScr.isPaintTeam && !GameScr.isPaintAlert)
                return cmd.input();
        }
        else
        {
            if (GameCanvas.isTouch)
            {

                //			}
                if (GameCanvas.currentDialog != null)
                {
                    if (GameCanvas.currentDialog.center != null)
                    {
                        if (GameCanvas.isPointerHoldIn(GameCanvas.w - cmdW >> 1, GameCanvas.h - cmdH - 5, cmdW, cmdH + 10))
                        {
                            keyTouch = 1;
                            if (cmd == GameCanvas.currentDialog.center && GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                                return true;
                        }
                    }
                    if (GameCanvas.currentDialog.left != null)
                    {
                        if (GameCanvas.isPointerHoldIn(0, GameCanvas.h - cmdH - 5, cmdW, cmdH + 10))
                        {
                            keyTouch = 0;
                            if ((cmd == GameCanvas.currentDialog.left) && GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                                return true;
                        }
                    }

                    if (GameCanvas.currentDialog.right != null)
                    {
                        if (GameCanvas.isPointerHoldIn(GameCanvas.w - cmdW, GameCanvas.h - cmdH - 5, cmdW, cmdH + 10))
                        {
                            keyTouch = 2;
                            if ((cmd == GameCanvas.currentDialog.right || cmd == ChatTextField.gI().right) && GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                                return true;
                        }

                    }
                }
                else
                {

                    if (cmd == GameCanvas.currentScreen.left)
                    {
                        if (GameCanvas.isPointerHoldIn(0, GameCanvas.h - cmdH - 5, cmdW, cmdH + 10))
                        {
                            keyTouch = 0;
                            if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                                return true;
                        }
                    }
                    if (cmd == GameCanvas.currentScreen.right)
                    {
                        if (GameCanvas.isPointerHoldIn(GameCanvas.w - cmdW, GameCanvas.h - cmdH - 5, cmdW, cmdH + 10))
                        {
                            keyTouch = 2;
                            if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                                return true;
                        }

                    }
                    if (cmd == GameCanvas.currentScreen.center || ChatPopup.currentMultilineChatPopup != null)
                    {
                        if (GameCanvas.isPointerHoldIn(GameCanvas.w - cmdW >> 1, GameCanvas.h - cmdH - 5, cmdW, cmdH + 10))
                        {
                            keyTouch = 1;
                            if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                                return true;
                        }

                    }
                }

            }

        }
        if (GameScr.isPaintTeam)
        { // update Button Party (Roi, kich, giai tan)

            if (cmd == GameCanvas.currentScreen.left)
            { // kich
                if (GameCanvas.isPointerHoldIn(cmd.x, cmd.y, cmd.w, cmd.h + 10))
                {
                    keyTouch = 0;
                    if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                        return cmd.input(); ;
                }
            }

            if (cmd == GameCanvas.currentScreen.center)
            { // roi
                if (GameCanvas.isPointerHoldIn(cmd.x, cmd.y, cmd.w, cmd.h + 10))
                {
                    keyTouch = 1;
                    cmd.input();
                    if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                        return cmd.input();
                }
            }

            if (cmd == GameCanvas.currentScreen.right)
            { // giai tan 

                if (GameCanvas.isPointerHoldIn(cmd.x, cmd.y, cmd.w, cmd.h + 10))
                {
                    keyTouch = 2;
                    if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                    {
                        return cmd.input();
                    }
                }

            }
        }
        else if (GameScr.isPaintFriend)
        {
            if (cmd == GameScr.btnChat)
            {
                if (GameCanvas.isPointerHoldIn(cmd.x, cmd.y, cmd.w, cmd.h + 10))
                {
                    keyTouch = 2;
                    if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                    {
                        return cmd.input();
                    }
                }
            }
        }
        else if (GameScr.ispaintChat)
        {
            //			if(GameCanvas.currentScreen == GameCanvas.gameScr){
            if (cmd == GameScr.chat || cmd == GameScr.bntIconChat)
            { // Chat
                if (GameCanvas.isPointerHoldIn(cmd.x, cmd.y, cmd.w, cmd.h + 10))
                {
                    keyTouch = 0;
                    if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                    {
                        return cmd.input();
                    }
                }
            }

            //			}
        }

        if (GameCanvas.currentScreen == GameCanvas.languageScr)
        {
            if (cmd == LanguageScr.cmdEng || cmd == LanguageScr.cmdVn)
            {
                if (GameCanvas.isPointerHoldIn(cmd.x, cmd.y, cmd.w, cmd.h + 10))
                {
                    keyTouch = 0;
                    if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                    {
                        return cmd.input();
                    }
                }
            }
        }
        return false;
	}
	public void commandPointer(int index, int subIndex) {

	}

    public virtual void updatePointer()
    {
		if (GameCanvas.isTouch) {
			if (left1 != null) {
				if (left1.isPosCmd())
					left1.updatePointer();
				else {
					if (GameCanvas.isPointSelect(0, GameCanvas.h
							- GameCanvas.hCommand - 5, GameCanvas.wCommand * 2,
							GameCanvas.hCommand + 10)) {
						left1.perform();
					}
				}
			}
			if (right1 != null) {
				if (right1.isPosCmd())
					right1.updatePointer();
				else {
					if (GameCanvas.isPointSelect(GameCanvas.w
							- GameCanvas.wCommand * 2, GameCanvas.h
							- GameCanvas.hCommand - 5, GameCanvas.wCommand * 2,
							GameCanvas.hCommand + 10)) {
						right1.perform();
					}
				}
			}
			if (center1 != null) {
				if (center1.isPosCmd())
					center1.updatePointer();
				else {
					if (GameCanvas.isPointSelect(GameCanvas.hw
							- GameCanvas.wCommand, GameCanvas.h
							- GameCanvas.hCommand - 5, GameCanvas.wCommand * 2,
							GameCanvas.hCommand + 10)) {
						// perform(center);
						center1.perform();

					}
				}
			}
		}
	}
	
	public void paintCmd(mGraphics g) {
	
			if (left1 != null) {
				left1.paint(g, GameCanvas.wCommand, GameCanvas.h
						- iCommand.hButtonCmd / 2 - 1);
			}
			if (right1 != null) {
				right1.paint(g, GameCanvas.w - GameCanvas.wCommand, GameCanvas.h
						- iCommand.hButtonCmd / 2 - 1);
			}
			if (center1 != null) {
				center1.paint(g, GameCanvas.hw, GameCanvas.h
						- iCommand.hButtonCmd / 2 - 1);
			}
		
	}
	
	public static void resetTranslate(mGraphics g) {
		g.translate(-g.getTranslateX(), -g.getTranslateY());
		g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
	}
    public virtual void paint(mGraphics g)
    {
        g.translate(-g.getTranslateX(), -g.getTranslateY());
        g.setClip(0, 0, GameCanvas.w, GameCanvas.h + 1);
        GameCanvas.paintt.paintTabSoft(g);
        if (ChatPopup.currentMultilineChatPopup != null)
            GameCanvas.paintt.paintCmdBar(g, null, ChatPopup.currentMultilineChatPopup.cmdNextLine, null);
        else if (ChatTextField.gI().isShow)
            GameCanvas.paintt.paintCmdBar(g, ChatTextField.gI().left, ChatTextField.gI().center, ChatTextField.gI().right);
        else if (GameCanvas.currentDialog == null && !GameCanvas.menu.showMenu)
            GameCanvas.paintt.paintCmdBar(g, left, center, right);
        //		else if (GameCanvas.currentDialog == null && !GameCanvas.menu2.isShowMenu) {
        //			paintCmd(g);
        //		s}
    }
}
