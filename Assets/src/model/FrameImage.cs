


//import javax.microedition.lcdui.game.*;

//quan ly cac frame anh duoc xep theo chieu doc
public class FrameImage {
	public int frameWidth;
	public int frameHeight;
	public int nFrame;

    public int Id;
	private mBitmap imgFrame;
    private int[] pos;
	private int totalHeight;

	private mBitmap[] imgList;
	private bool isRotate;

	public FrameImage(Image img, int width, int height) {
        imgFrame.image = img; 
        frameWidth = width;
        frameHeight = height;
        totalHeight = Image.getHeight(imgFrame);
		nFrame = totalHeight / height;
		pos = new int[nFrame];
		for (int i = 0; i < nFrame; i++){
			pos[i] = i * height;
		}
	}
    public FrameImage(int ID, int width, int height)
    {
        // TODO Auto-generated constructor stub
        this.Id = ID;
        imgFrame = ImageEffect.setImage(ID);
        frameWidth = width;
        frameHeight = height;
        nFrame = Image.getHeight(imgFrame) / height;
        pos = new int[nFrame];
        for (int i = 0; i < nFrame; i++)
        {
            pos[i] = i * height;
        }
    }
	public void drawFrame(int idx, int x, int y, int trans, int anchor, mGraphics g) {
        if(imgFrame==null)
            imgFrame = ImageEffect.setImage(Id);
        else 
			if (idx >= 0 && idx < nFrame){
				g.drawRegion(imgFrame, 0, pos[idx], frameWidth, frameHeight, trans, x, y, anchor);
			}
	}

	public void unload() {
		imgFrame = null;
		pos = null;
	}

	
}
