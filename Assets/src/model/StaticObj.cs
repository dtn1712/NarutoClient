


public class StaticObj {
	public static int TOP_CENTER = mGraphics.TOP | mGraphics.HCENTER;
	public static int TOP_LEFT = mGraphics.TOP | mGraphics.LEFT;
	public static int TOP_RIGHT = mGraphics.TOP | mGraphics.RIGHT;
	public static int BOTTOM_HCENTER = mGraphics.BOTTOM | mGraphics.HCENTER;
	public static int BOTTOM_LEFT = mGraphics.BOTTOM | mGraphics.LEFT;
	public static int BOTTOM_RIGHT = mGraphics.BOTTOM | mGraphics.RIGHT;
	public static int VCENTER_HCENTER = mGraphics.VCENTER | mGraphics.HCENTER;
	public static int VCENTER_LEFT = mGraphics.VCENTER | mGraphics.LEFT;
    public static int[] SKYCOLOR = new int[] { 0x55aaee, 0x4880f8, 0x101010,
			0x1fc3f5, 0x1fc3f5, 0, 0xa0d8f8, 0x268dc6, 0x171808, 0xFFBFBC, 0,
			0x139CAA, 0x139CAA };
    public static int[][] TYPEBG = new int[][]{new int[] { 0, 0, 0, 0}, new int[]{ 1, 1, 1, -1 },
			new int[]{ 2, 2, 2, 2 }, new int[]{ 2, 2, 2, -1 },new int[] { 3, 3, 3, 3 }, new int[]{ 4, -1, -1, 4 },
			new int[]{ 5, 5, 5, -1 },new int[] { 6, 6, 6, 5 }, new int[]{ 7, 7, -1, -1 },new int[] { 8, 8, 8, 7 },
			new int[]{ 9, -1, -1, 8 }, new int[]{ 10, -1, -1, 9 }, new int[]{ 11, -1, -1, -1 } };
	// =================
	public static  string SAVE_SKILL = "skill";
	public static  string SAVE_VERSIONUPDATE = "versionUpdate";
	public static  string SAVE_KEYKILL = "keyskill";
	public static  string SAVE_ITEM = "item";
	
	// ====================
	public const int NORMAL = 0;
	public const int UP_FALL = 1;
	public const int UP_RUN = 2;
	public const int FALL_RIGHT = 3;
	public const int FALL_LEFT = 4;// KO BI THUONG
	public const int MOD_ATTACK_ME = 100;

	// =============model obj
	// public const int TYPE_MOD = 1;
	// public const int TYPE_ITEM = 2;
	public const int TYPE_PLAYER = 3;
	// ==============model obj

	// ========item
	public const int TYPE_NON = 0;
	public const int TYPE_VUKHI = 1;
	public const int TYPE_AO = 2;
	public const int TYPE_LIEN = 3;
	public const int TYPE_TAY = 4;
	public const int TYPE_NHAN = 5;
	public const int TYPE_QUAN = 6;
	public const int TYPE_BOI = 7;
	public const int TYPE_GIAY = 8;
	public const int TYPE_PHU = 9;
	public const int TYPE_OTHER = 11;
	public const int TYPE_CRYSTAL = 15;
	// ==============

	// ===============set focus
	public const int FOCUS_MOD = 1;
	public const int FOCUS_ITEM = 2;
	public const int FOCUS_PLAYER = 3;
	public const int FOCUS_ZONE = 4;
	public const int FOCUS_NPC = 5;

	// ==============mang type bg

	

   

}
