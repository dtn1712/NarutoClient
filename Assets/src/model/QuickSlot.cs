
using System;
public class QuickSlot {
	public sbyte quickslotType = -1;// Skill or Potion
	public sbyte idSkill, ItemType, indexPotion;// Must set
	public static sbyte idSkillCoBan=-1;
	public int ideff;
	public const sbyte TYPE_SKILL = 1;
	public const sbyte TYPE_ITEM = 2;
	public const sbyte TYPE_POTION = 2;
	public static mBitmap img = null;
	public bool isBuff = false;
	public short idicon;
	public int cooldown,mp;
	private long timewait,timecoolDown;
	public static sbyte[] idSkillGan = new sbyte[]{-1,-1,-1,-1,-1};
	public static void getImg() {
		img =GameCanvas.loadImage("/interface/delayskill.png");
	}
	public QuickSlot(){
		idSkill=-1;
	}
	public void setIsSkill(int skillType, bool isBuff) {
		SkillTemplate skill=(SkillTemplate)SkillTemplates.hSkilltemplate.get(""+skillType);
		quickslotType = TYPE_SKILL;
		this.idSkill = (sbyte)skillType;
		this.ideff = skill.ideff;
		this.isBuff = isBuff;
		if(skill.level<skill.mphao.Length)
		this.mp = skill.mphao[skill.level];
		else this.mp = skill.mphao[0];
		this.idicon=(short)skill.iconId;
		cooldown=skill.getCoolDown((sbyte)skill.level);
		
	}
	public static void SaveRmsQuickSlot(){
		for (int i = 0; i < Char.myChar().mQuickslot.Length; i++) {
			QuickSlot ql = Char.myChar().mQuickslot[i];
			if(ql!=null&&idSkillGan!=null)
			idSkillGan[i] = ql.idSkill;
		}
		Rms.saveRMS("quickslot", idSkillGan);
	}
	public static void loadRmsQuickSlot(){
		idSkillGan = Rms.loadRMS("quickslot");
//		Cout.println(idSkillGan+" loadRmsQuickSlot idSkillCoBan "+idSkillCoBan);
		if(idSkillGan!=null){
			bool isMyOldChar = false;
			for (int i = 0; i < idSkillGan.Length; i++) {
				SkillTemplate skill=(SkillTemplate)SkillTemplates.hSkilltemplate.get(""+idSkillGan[i]);
				if(skill!=null&&skill.level>=0){
					isMyOldChar = true;
					Char.myChar().mQuickslot[i].setIsSkill(idSkillGan[i], false);
				}
			}
			if(!isMyOldChar){
				idSkillGan = new sbyte[]{-1,-1,-1,-1,-1};
                SkillTemplate skill = (SkillTemplate)SkillTemplates.hSkilltemplate.get("" + (idSkillCoBan > -1 ? idSkillCoBan : (sbyte)0));
				if(skill!=null&&skill.level>=0){
					idSkillGan[0] = (sbyte)skill.id;
					Char.myChar().mQuickslot[0].setIsSkill(0, false);
				}
				Rms.saveRMS("quickslot", idSkillGan);
			}
		}else {

			idSkillGan = new sbyte[]{-1,-1,-1,-1,-1};
            SkillTemplate skill = (SkillTemplate)SkillTemplates.hSkilltemplate.get("" + (idSkillCoBan > -1 ? idSkillCoBan : (sbyte)0));
			if(skill!=null&&skill.level>=0){
				idSkillGan[0] = (sbyte)skill.id;
				Char.myChar().mQuickslot[0].setIsSkill(0, false);
			}
			Rms.saveRMS("quickslot", idSkillGan);
		}
	}
	public void setIsSkill_introgame(int skillType, int clazz, bool isBuff, int idicon) {
		quickslotType = TYPE_SKILL;
		this.idSkill = (sbyte) skillType;
		this.isBuff = isBuff;
		this.idicon=(short)idicon;
		cooldown=500;
	}
	public void paint(mGraphics g, int x, int y) {
		try {
			if (quickslotType == -1)
				return;
		} catch (Exception e) {
			Cout.println("ERROR QUICKSLOT " + e.ToString());
		}
	}

	public sbyte getSkillType() {
		return idSkill;
	}
	public bool getBuffType() {
		return isBuff;
	}
	public void startCoolDown(){
		timewait=(long)(mSystem.currentTimeMillis()+cooldown*1000);
	}
	public bool canfight(){
		return (timewait-mSystem.currentTimeMillis())<0&&quickslotType==TYPE_SKILL;
	}
    public bool isEnoughtMp()
    {
		return (Char.myChar().cMP>=mp?true:false);
	}
    public bool canUse()
    {
		return (timewait-mSystem.currentTimeMillis())<0;
	}
	/*
	 * public void update() { timePaintM_H = mSystem.currentTimeMillis_() -
	 * Canvas.gameScr.mainChar.timeLastUseSkills[skillType];
	 * 
	 * timePaintSkill = mSystem.currentTimeMillis_() -
	 * Canvas.gameScr.mainChar.potionLastTimeUse[potionType]; }
	 */
	public void update() {
		
	}
	public int getPotionType(){
		return 0;
	}
	public sbyte getItemType() {
		return ItemType;
	}
	public String gettimeCooldown(){
		long time=(timewait-mSystem.currentTimeMillis())/1000;
		long t=0;
		if(time==0){
			t = ((timewait-mSystem.currentTimeMillis())%1000)/100;
			if(t>0)
			return "0."+t;
		}else if(time>0) return ""+time;
		return "";
	}
	public void setIsIteam(int potionType) {
		quickslotType = TYPE_ITEM;
		this.ItemType = (sbyte) potionType;
	}

	public void setIsNothing() {
		quickslotType = -1;
	}
}
