

public class MainEffect : MainObject {
	bool isPaint = true;
	public int typeEffect = 0,valueEffect;
	public int fRemove;
	public int levelPaint = 0;
	long timeBegin;
	public FrameImage fraImgEff, fraImgSubEff, fraImgSub2Eff, fraImgSub3Eff;
	public FrameImage fraKhoiThianThach, fraLuaThienThach;
	

	// Direction
	public const int DIR_UP = 1;
	public const int DIR_DOWN = 0;
	public const int DIR_LEFT = 2;
    public const int DIR_RIGHT = 3;

    public virtual void paint(mGraphics g)
    {
        base.paint(g);
	}

	public virtual void update() {
		base.update();
	}

	public void setPosition(int x, int y, int xto, int yto) {
		this.x = x;
		this.y = y;
		this.toX = xto;
		this.toY = yto;
	}

	public static bool isInScreen(int x, int y, int w, int h) {
		if (x < GameScr.cmx - w
				|| x > GameScr.cmx + GameCanvas.w + w
				|| y < GameScr.cmy - h
				|| y > GameScr.cmy + GameCanvas.h + h)
			return false;
		return true;
	}
}
