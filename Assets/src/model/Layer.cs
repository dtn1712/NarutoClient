


using System;
public abstract class Layer {
	public int x;

    public int y;

    public int width;

    public int height;

    public bool visible = true;
    public Layer() { }
    public Layer(int width, int height)
    {
    	width *= GameCanvas.zoomLevel;
    	height *= GameCanvas.zoomLevel;
        setWidthImpl(width);
        setHeightImpl(height);
    }

    public void setPosition(int x, int y)
    {
        this.x = x * GameCanvas.zoomLevel;
        this.y = y  * GameCanvas.zoomLevel;
    }

    public void move(int dx, int dy)
    {  
        x += dx * GameCanvas.zoomLevel;
        y += dy * GameCanvas.zoomLevel;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public  int getWidth() {
         return width;
    }

    public int getHeight()
    {
            return height;
    }

    public void setVisible(bool visible)
    {
        this.visible = visible;
    }

    public bool isVisible()
    {
        return visible;
    }

    public virtual void paint() { }

    public void setWidthImpl(int width)
    { 
        if (width < 0) {
            throw new ArgumentException();
        }
        this.width = width;
    }

    public void setHeightImpl(int height)
    {
        if (height < 0) {
            throw new ArgumentException();
        }
        this.height = height;
    }
    public void update()
    {
    }

    public void paint(mGraphics g, int x, int y)
    {
    }

    public void keyPress(int keyCode)
    {
    }
}
