

using System;
using UnityEngine;
public class TiledLayer : Layer{

    public static int cellHeight; // = 0;

    public static int cellWidth; // = 0;

    public static int rows; // = 0;

    public static int columns; // = 0;

    private int[][] cellMatrix; // = null;

    public mBitmap sourceImage; // = null;

    private int numberOfTiles; // = 0;

    public int[] tileSetX;

    public int[] tileSetY;
    
    private int[] anim_to_static; // = null;

    private int numOfAnimTiles; // = 0
    
    private Paint mPaint;
    private Rect mSrcRect;
    private Rect mDestRect;

    //public TiledLayer(int columnss, int rowss, mBitmap image, int tileWidth, int tileHeight) {
    //    super(columns < 1 || tileWidth < 1 ? -1 : columns * tileWidth, rowss < 1 || tileHeight < 1 ? -1 : rowss * tileHeight);

    //    mPaint = new Paint();
    //    mSrcRect = new Rect();
    //    mDestRect = new Rect();

    //    if (((Image.getWidth(image) % tileWidth) != 0) || ((Image.getHeight(image) % tileHeight) != 0)) {
    //        throw new ArgumentException();
    //    }
    //   columns = columnss;
    //    rows = rowss;

    //    cellMatrix = new int[rows][];
    //    for (int i = 0; i < cellMatrix.Length; i++)
    //        {
    //         cellMatrix[i] = new int[columns];
    //        }

    //    int noOfFrames = (Image.getWidth(image) / tileWidth) * (Image.getHeight(image) / tileHeight);
    //    createStaticSet(image,  noOfFrames + 1, tileWidth, tileHeight, true);        
    //}

    public int createAnimatedTile(int staticTileIndex) {
        // checks static tile 
        if (staticTileIndex < 0 || staticTileIndex >= numberOfTiles) { 
            throw new IndexOutOfRangeException();
        }

        if (anim_to_static == null) {
            anim_to_static = new int[4];
            numOfAnimTiles = 1;
        } else if (numOfAnimTiles == anim_to_static.Length) {
            // grow anim_to_static table if needed 
            int[] new_anim_tbl = new int[anim_to_static.Length * 2];
            System.Array.Copy(anim_to_static, 0, new_anim_tbl, 0, anim_to_static.Length);
            anim_to_static = new_anim_tbl;
        }
        anim_to_static[numOfAnimTiles] = staticTileIndex;
        numOfAnimTiles++;
        
        return (-(numOfAnimTiles - 1));
    }

    public void setAnimatedTile(int animatedTileIndex, int staticTileIndex) {
        // checks static tile
        if (staticTileIndex < 0 || staticTileIndex >= numberOfTiles) {  
            throw new IndexOutOfRangeException();
        }
        // do animated tile index check
        animatedTileIndex = - animatedTileIndex;
        if (anim_to_static == null || animatedTileIndex <= 0 
            || animatedTileIndex >= numOfAnimTiles) { 
            throw new IndexOutOfRangeException();
        }
        anim_to_static[animatedTileIndex] = staticTileIndex;
    }


    public int getAnimatedTile(int animatedTileIndex) {
        animatedTileIndex = - animatedTileIndex;
        if (anim_to_static == null || animatedTileIndex <= 0 || animatedTileIndex >= numOfAnimTiles) { 
            throw new IndexOutOfRangeException();
        }
    
        return anim_to_static[animatedTileIndex];
    }

    public void setCell(int col, int row, int tileIndex) {

        if (col < 0 || col >= columns || row < 0 || row >= rows) {
            throw new IndexOutOfRangeException();
        }

    if (tileIndex > 0) {
            // do checks for static tile 
            if (tileIndex >= numberOfTiles) { 
            throw new IndexOutOfRangeException();
        }
    } else if (tileIndex < 0) {
            // do animated tile index check
        if (anim_to_static == null ||
                (-tileIndex) >= numOfAnimTiles) { 
            throw new IndexOutOfRangeException();
            }
    }

        cellMatrix[row][col] = tileIndex;
 
    }

    public int getCell(int col, int row) {
        if (col < 0 || col >= columns || row < 0 || row >= rows) {
            throw new IndexOutOfRangeException();
        }
        return cellMatrix[row][col];
    }

    public void fillCells(int col, int row, int numCols, int numRows,
                          int tileIndex) {


    if (numCols < 0 || numRows < 0) {
        throw new ArgumentException();
    }

        if (col < 0 || col >= columns || row < 0 || row >= rows ||
        col + numCols > columns || row + numRows > rows) {
            throw new IndexOutOfRangeException();
        }

    if (tileIndex > 0) {
            // do checks for static tile 
            if (tileIndex >= numberOfTiles) { 
            throw new IndexOutOfRangeException();
        }
    } else if (tileIndex < 0) {
            // do animated tile index check
        if (anim_to_static == null || 
                (-tileIndex) >= numOfAnimTiles) { 
                throw new IndexOutOfRangeException();
            }
    }

        for (int rowCount = row; rowCount < row + numRows; rowCount++) {
            for (int columnCount = col; 
                     columnCount < col + numCols; columnCount++) {
                cellMatrix[rowCount][columnCount] = tileIndex;
            }
        }
    }


    public  int getCellWidth() {
        return cellWidth;
    }

    public  int getCellHeight() {
        return cellHeight;
    }

    public  int getColumns() {
        return columns;
    }

    public  int getRows() {
        return rows;
    }

    public void setStaticTileSet(mBitmap image, int tileWidth, int tileHeight) {

        // if img is null img.getWidth() will throw NullPointerException
        if (tileWidth < 1 || tileHeight < 1 ||
        ((Image.getWidth(image) % tileWidth) != 0) || ((Image.getHeight(image) % tileHeight) != 0)) {
            throw new ArgumentException();
        }
        setWidthImpl(columns * tileWidth);
        setHeightImpl(rows * tileHeight);

        int noOfFrames = (Image.getWidth(image) / tileWidth) * (Image.getHeight(image) / tileHeight);

        // the zero th index is left empty for transparent tile
        // so it is passed in  createStaticSet as noOfFrames + 1

		if (noOfFrames >= (numberOfTiles - 1)) {
			// maintain static indices
        createStaticSet(image, noOfFrames + 1, tileWidth, tileHeight, true);
		} else {
            createStaticSet(image, noOfFrames + 1, tileWidth, tileHeight, false);
		}
	}

  public override void paint() {
	  
  }

  private void createStaticSet(mBitmap image, int noOfFrames, int tileWidth, 
                      int tileHeight, bool maintainIndices) {

        cellWidth = tileWidth;
        cellHeight = tileHeight;

    int imageW = Image.getWidth(image);
    int imageH = Image.getHeight(image);

    sourceImage = image;

    numberOfTiles = noOfFrames;
    tileSetX = new int[numberOfTiles];
    tileSetY = new int[numberOfTiles];
    
    if (!maintainIndices) {
            // populate cell matrix, all the indices are 0 to begin with
            for (rows = 0; rows < cellMatrix.Length; rows++) {
                int totalCols = cellMatrix[rows].Length;
                for (columns = 0; columns < totalCols; columns++) {
                    cellMatrix[rows][columns] = 0;
                }
            }
        // delete animated tiles
        anim_to_static = null;
    } 

        int currentTile = 1;

        for (int locY = 0; locY < imageH; locY += tileHeight) {
            for (int locX = 0; locX < imageW; locX += tileWidth) {

        tileSetX[currentTile] = locX;
        tileSetY[currentTile] = locY;

                currentTile++;
            }
        }
    }

}
