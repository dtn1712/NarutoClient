

using System.IO;
using System;
public class FilePack {
	public static FilePack instance;
	private string[] fname;
	private int[] fpos;
	private int[] flen;
	private sbyte[] fullData;
	private int nFile;
	private int hSize;
	private string name;
	private sbyte[] code = { 78, 103, 117, 121, 101, 110, 86, 97, 110, 77, 105,
			110, 104 };
	private int codeLen = 13; //hardcode mang tren luon

	private DataInputStream file;

	public FilePack(sbyte[] DATA) {

		//ByteArrayInputStream array = new ByteArrayInputStream(DATA);
        file = new DataInputStream(DATA);
		int lname;
		sbyte[] data;
		int pos = 0, size = 0;
		hSize = 0;
		try {
			nFile = encode(file.readUnsignedByte());
			// System.out.println("xor " + (100^78));
			hSize += 1;
			fname = new string[nFile];
			fpos = new int[nFile];
			flen = new int[nFile];
			for (int i = 0; i < nFile; i++) {
				lname = encode(file.readByte());
				data = new sbyte[lname];
				file.read(ref data);
				encode(data);
                fname[i] = System.Text.Encoding.UTF8.GetString(mSystem.convetToByte(data), 0, data.Length); ;
				fpos[i] = pos;
				flen[i] = encode(file.readUnsignedShort());
				pos += flen[i];
				size += flen[i];
				hSize += lname + 3; // filenamelen + filename + filesize
			}
			// cap phat bo nho va doc mang data
			fullData = new sbyte[size];
			file.readFully(ref fullData);
			encode(fullData);
		} catch (IOException e) {
		}
		close();
	}

	public FilePack(string name) {
		int lname;
		sbyte[] data;
		int pos = 0, size = 0;
		this.name = name;
		hSize = 0;
		open();
		try {
			nFile = encode(file.readUnsignedByte());
			// System.out.println("xor " + (100^78));
			hSize += 1;
			fname = new string[nFile];
			fpos = new int[nFile];
			flen = new int[nFile];
			for (int i = 0; i < nFile; i++) {
				lname = encode(file.readByte());
				data = new sbyte[lname];
				file.read(ref data);
				encode(data);
                fname[i] = System.Text.Encoding.UTF8.GetString(mSystem.convetToByte(data), 0, data.Length);
				fpos[i] = pos;
				flen[i] = encode(file.readUnsignedShort());
				pos += flen[i];
				size += flen[i];
				hSize += lname + 3; // filenamelen + filename + filesize
			}
			// cap phat bo nho va doc mang data
			fullData = new sbyte[size];
			file.readFully(ref fullData);
			encode(fullData);
		} catch (IOException e) {
		}
		close();
	}

	public static mBitmap getImg(string path) {
		return instance.loadImage(path + ".png");
	}

	public static void reset() {
		instance = null;
	}

	public static void init(string path) {
		instance = null;
		instance = new FilePack(path);
	}

	private int encode(int i) {
		// return i^0xffff;
		return i;
	}

	private void encode(sbyte[] bytes) {
		int len = bytes.Length;
		for (int i = 0; i < len; i++) {
			bytes[i] = (sbyte) (bytes[i] ^ code[i % codeLen]);
		}
	}

	private void open() {
	}

	private void close() {
		try {
			if (file != null)
				file.close();
		} catch (IOException e) {
		}
	}

	public sbyte[] loadFile(string fileName){
        try 
	    {
            for (int i = 0; i < nFile; i++)
            {
                if (fname[i].CompareTo(fileName) == 0)
                {
                    sbyte[] bytes = new sbyte[flen[i]];
                    System.Array.Copy(fullData, fpos[i], bytes, 0, flen[i]);
                    return bytes;
                }
            }
            return null;
	    }
	    catch (System.Exception)
	    {
            return null;
	    }
		
	}
	public string loadFileString(string fileName) {
        try 
	    {	        
		    for (int i = 0; i < nFile; i++) {
			    if (fname[i].CompareTo(fileName) == 0) {
				    byte[] bytes = new byte[flen[i]];
                    System.Array.Copy(fullData, fpos[i], bytes, 0, flen[i]);
				    return bytes.ToString();
			    }
		    }
            return null;
	    }
	    catch (System.Exception)
	    {
		    throw new Exception("File '" + fileName + "' not found!");
	    }
		
	}

	public mBitmap loadImage(string fileName) {
		return null;
	}
}
