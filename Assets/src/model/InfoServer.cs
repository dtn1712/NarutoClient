

public class InfoServer {
    public string text;
    public NodeChat node;
	public int x,y;
	public short idplayer;
	public int type,wString,hpaint;
	public const int CHATSERVER = 0;
	public const int CHATRAO = 1;
	public bool isStop,isThongBaoDuoi;
	public int tgdung = 50;
	public InfoServer(string text)
	{
		this.text = text;
		this.hpaint = 10;
		type = 0;
		wString = mFont.tahoma_7_white.getWidth(text);
		this.x = 3*GameCanvas.w/4+wString/2;
	}
    public InfoServer(NodeChat text)
    {
        this.node = text;
        this.hpaint = 10;
        type = 0;
        wString = mFont.tahoma_7_white.getWidth(text.textdai);
        this.x = 3 * GameCanvas.w / 4 + wString / 2;
    }
	public InfoServer(string text,int hppaint)
	{
		this.hpaint = hppaint;
		this.text = text;
		type = 0;
		isThongBaoDuoi = true;
		wString = mFont.tahoma_7_white.getWidth(text);
		this.x = 3*GameCanvas.w/4+wString/2;
	}
	public void update()
	{
		if(x>(GameCanvas.w/4-wString)&&!isStop){
			if(x>GameCanvas.w/4+wString/2){ 
				x-=10;
			}else{
				if(tgdung<=0){
					tgdung--;
					if(wString<GameCanvas.w/2)
						x+=tgdung/2;
					else x-=2;
				}else{
					x=GameCanvas.w/4+wString/2;
					tgdung--;
				}
			}
		}
		else{
			GameScr.listInfoServer.removeElement(this);
		}
	}
	public void paint(mGraphics g)
	{
		g.setColor(0,60);
		switch (type) {
		case CHATSERVER:
			
			g.setClip(GameCanvas.w/4-4, hpaint, GameCanvas.w/2+8, 20);
			g.fillRect(GameCanvas.w/4-4, hpaint, GameCanvas.w/2+8, 20,true);
			g.disableBlending();
//			if(!isThongBaoDuoi)
            if (this.node == null)
                mFont.tahoma_7_white.drawString(g, text, x, hpaint + 3, 2, true);
            else node.paint(g, x, hpaint + 3, false, false);
//			else 
//				mFont.tahoma_7_red.drawStringBorder(g, text, x, hpaint+5*TCanvas.TileZoom, 2,0xff00000,true);
			break;
		}
		GameCanvas.resetTrans(g);
	}
}
