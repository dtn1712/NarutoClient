

public class Key {
	
	public const int NUM0 = 0;
    public const int NUM1 = 1;
    public const int NUM2 = 2;
    public const int NUM3 = 3;
    public const int NUM4 = 4;
    public const int NUM5 = 5;
    public const int NUM6 = 6;
    public const int NUM7 = 7;
    public const int NUM8 = 8;
    public const int NUM9 = 9;
    public const int STAR = 10;
    public const int BOUND = 11;
    public const int UP = 12;
    public const int DOWN = 13;
    public const int LEFT = 14;
    public const int RIGHT = 15;
    public const int FIRE = 16;
    public const int LEFT_SOFTKEY = 17;
    public const int RIGHT_SOFTKEY = 18;
    public const int CLEAR = 19;
    public const int BACK = 20;
}
