

using System;
public class Timer {
	public static IActionListener timeListener;
	public static int idAction;
	public static long timeExecute;
	public static bool isON;

	public static void setTimer(IActionListener actionListener,int action, long timeEllapse) {
	//	System.out.println("SET TIMER " + timeEllapse);
		timeListener = actionListener;
		idAction = action;
		timeExecute = mSystem.currentTimeMillis() + timeEllapse;
		isON = true;
	}

	public static void update() {
		long now = mSystem.currentTimeMillis();
		if (isON && now > timeExecute) {
			//System.out.println("TIMER EXECUTE");
			isON = false;
			try {
				if (idAction > 0){		    		
		    		GameScr.gI().perform(idAction, null);
		    	}
//				if (timerAction != null )
//					timerAction.perform();
			} catch (Exception e) {
			//	e.printStackTrace();
			}
		}
	}

}
