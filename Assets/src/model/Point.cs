

public class Point {
	public int x, y, g, v, w, h, color = 0, limitY, vx, vy,x2,y2;
	public int dis, f, fRe,frame;
	public mVector vecEffPoint;
	public string name;

	public Point() {
	}

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void update() {
		f++;
		x += vx;
		y += vy;
	}
}
