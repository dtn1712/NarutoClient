


public class MsgDlg : Dialog {
	public string[] info;
    public bool isWait;
    public string text = "";
    public bool  isPaintCham;
	public int timeShow;
	public int w = 240,y;
	public bool isDlgTime = false;
	public int timeLive;
	public long timestart;
	public static mBitmap[] imgpopup = new mBitmap[2];
	public static mBitmap[] imgbtnpopup = new mBitmap[2];
	int h=110;
	private int padLeft;

	public MsgDlg() {
		padLeft = 30;
		if (GameCanvas.w <= 176)
			padLeft = 10;
	}
    public static void loadbegin()
    {
		for (int i = 0; i < imgpopup.Length; i++) {
			imgpopup[i] = GameCanvas.loadImage("/wood_bar"+i+".png");
		}
		for (int i = 0; i < imgbtnpopup.Length; i++) {
			imgbtnpopup[i] = GameCanvas.loadImage("/btnpopup"+i+".png");
		}
		
	}
	public void setTimeLive(int time){
		this.isDlgTime = true;
		this.timeLive = time;
		this.timestart = mSystem.currentTimeMillis();
	}
	public void pleasewait() {
		setInfo(mResources.PLEASEWAIT, null, null, null);
		GameCanvas.currentDialog = this;
	}

	public void show() {
		GameCanvas.currentDialog = this;
	}
	
	public void setInfo(string info){
		this.info = mFont.tahoma_7_white.splitFontArray(info, 240);
		isWait = false;
//		h = 80;
//		if (this.info.Length >= 5)
//			h = this.info.Length * mFont.tahoma_8b.getHeight() + 20;
		if(this.left==null&&this.right==null) {
			this.center.x = GameCanvas.w/2 - 35;
			this. y = GameCanvas.h/2 - h/2+40;
			this.center.y = this. y  + h-54;
        }
        this.isDlgTime = false;
	}

	public void setInfo(string infoo, Command left, Command center, Command right) {
        this.info = mFont.tahoma_7_white.splitFontArray(infoo, 240);
        this.text = infoo;
		this.left = left;
		this.center = center;
		this.right = right;

		this.y = GameCanvas.h - h - 35;
		this. y = GameCanvas.h/2 - h/2+40;
		if(center!=null){
			this.center.x = GameCanvas.w/2 - 35;
			this.center.y = GameCanvas.h - 26;
			
			if(left!=null){
				this.left.x = GameCanvas.w/2 - 115;
				this.left.y = GameCanvas.h - 26;
			}
			
			if(right!=null){
				this.right.x = GameCanvas.w/2 + 45;
				this.right.y = GameCanvas.h - 26;
			}
			if(this.left==null&&this.right==null) {
				this. y = GameCanvas.h/2 - h/2+40;
				this.center.x = GameCanvas.w/2 - imgbtnpopup[1].getWidth()/2;
				this.center.y = this. y  + h-54;
				this.center.img = imgbtnpopup[1];
				this.center.imgFocus = imgbtnpopup[0];
				this.center.w = imgbtnpopup[1].getWidth();
				this.center.h = imgbtnpopup[1].getHeight();
			}
		}else{
			if(left!=null){
				this.left.x = GameCanvas.w/2 - 3*imgbtnpopup[1].getWidth()/2;
				this.left.y = this. y  + h-54;
				this.left.img = imgbtnpopup[1];
				this.left.imgFocus = imgbtnpopup[0];
				this.left.w = imgbtnpopup[1].getWidth();
				this.left.h = imgbtnpopup[1].getHeight();
//				this.left.x = GameCanvas.w/2 - 80;
//				this.left.y = GameCanvas.h - 26;
			}
			if(right!=null){

				this.right.x = GameCanvas.w/2 + imgbtnpopup[1].getWidth()/2;
				this.right.y = this. y  + h-54;
				this.right.img = imgbtnpopup[1];
				this.right.imgFocus = imgbtnpopup[0];
				this.right.w = imgbtnpopup[1].getWidth();
				this.right.h = imgbtnpopup[1].getHeight();
//				this.right.x = GameCanvas.w/2 + 10;
//				this.right.y = GameCanvas.h - 26;
			}
            this.isDlgTime = false;
		}
		
		
	
		isWait = false;
//		h = 80;
//		if (this.info.Length >= 5)
//			h = this.info.Length * mFont.tahoma_8b.getHeight() + 20;
	}

	public override void paint(mGraphics g) {
		int yDlg = y;
		//GameCanvas.paint.paintFrame(padLeft, yDlg, GameCanvas.w - (padLeft * 2), h, g);
		
		g.drawImage(imgpopup[1], GameCanvas.w/2-imgpopup[1].getWidth()/2, yDlg+imgpopup[0].getHeight()/8, mGraphics.VCENTER|mGraphics.HCENTER);
		g.drawRegion(imgpopup[1], 0, 0, imgpopup[1].getWidth(), imgpopup[1].getHeight(), 2,
				GameCanvas.w/2+imgpopup[1].getWidth()/2, yDlg+imgpopup[0].getHeight()/8, mGraphics.VCENTER|mGraphics.HCENTER);
		mFont.tahoma_10b.drawString(g, "Thông báo", GameCanvas.hw,yDlg-imgpopup[0].getHeight()/3, 2);
		g.drawImage(imgpopup[0], GameCanvas.w/2-imgpopup[0].getWidth()/2, yDlg+imgpopup[0].getHeight(), mGraphics.VCENTER|mGraphics.HCENTER);
		g.drawRegion(imgpopup[0], 0, 0, imgpopup[0].getWidth(), imgpopup[0].getHeight(), 2,
				GameCanvas.w/2+imgpopup[0].getWidth()/2, yDlg+imgpopup[0].getHeight(), mGraphics.VCENTER|mGraphics.HCENTER);
		
		g.drawImage(imgpopup[0], GameCanvas.w/2-imgpopup[0].getWidth()/2, yDlg+2*imgpopup[0].getHeight()-1, mGraphics.VCENTER|mGraphics.HCENTER);
		g.drawRegion(imgpopup[0], 0, 0, imgpopup[0].getWidth(), imgpopup[0].getHeight(), 2,
				GameCanvas.w/2+imgpopup[0].getWidth()/2, yDlg+2*imgpopup[0].getHeight()-1, mGraphics.VCENTER|mGraphics.HCENTER);
		int yStart = yDlg + (h - (info.Length==1?1:2) * mFont.tahoma_8b.getHeight()) / 2 - 2-16;
		if (isWait) {
			yStart += 8;
//			GameCanvas.paintShukiren(GameCanvas.hw, yStart - 12, g, false);
		}
        if (isPaintCham)
        {
            int nCham = (GameCanvas.gameTick / 10) % 4;
           string textt = "";
            for (int i = 0; i < nCham; i++)
            {
                textt += ".";
            }
            for (int i = 0; i < 3 - nCham; i++)
            {
                textt += " ";
            }
            mFont.tahoma_7_white.drawString(g, text + textt, GameCanvas.hw, yStart - 10, 2);
        }
        else
        {
            //int yy = yStart;
            for (int i = 0, yyz = yStart; i < info.Length; i++, yyz += mFont.tahoma_8b.getHeight() + 4)
            {
                mFont.tahoma_7_white.drawString(g, info[i], GameCanvas.hw, yyz - 6 + (i > 1 ? mFont.tahoma_7_white.getHeight() : 0), 2);
            }
        }
		base.paint(g);
	}
	
	

	public override void update() {
        if (isDlgTime)
        {
            if ((mSystem.currentTimeMillis() - timestart) / 1000 > timeLive) GameCanvas.endDlg();
        }
        else if (isWait && timeShow > 0)
        {
            timeShow--;
            if (timeShow == 1)
            {
                GameCanvas.endDlg();
                timeShow = 0;
            }
        }
        else if (GameCanvas.isPointerJustRelease && !GameCanvas.isPoint(GameCanvas.w / 2 - imgpopup[1].getWidth(), y, 2 * imgpopup[1].getWidth(), imgpopup[1].getHeight() * 3))
        {
            GameCanvas.clearPointerEvent();
            GameCanvas.endDlg();
        }
		base.update();
	}
	
	
}
