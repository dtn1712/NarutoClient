

using System;
public class EffectData {

	public mBitmap img;
	public ImageInfo[] imgInfo;
	public Frame[] frame;
	public short[] arrFrame;
	public int ID;

	public ImageInfo getImageInfo(sbyte id) {
		for (int i = 0; i < imgInfo.Length; i++)
			if (imgInfo[i].ID == id)
				return imgInfo[i];
		return null;
	}
	
	public static DataInputStream readdatamobFromRes(string path){
		DataInputStream iss = null;
		try {
			iss = new DataInputStream("/mapitem/mapItem");
			if(iss != null)
				return iss;
			else 
				return null;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
		
	}

	public int width, height;

	public void readData(string patch) {
		InputStream iss = null;
		DataInputStream isss = null;
		try {
			//iss = GameMidlet.asset.open(patch);
            isss = new DataInputStream(patch);
		} catch (Exception e) {
			return;
		}
        readData(isss);

	}

	public void readData(DataInputStream isss) {
		int left = 0, top = 0, right = 0, bottom = 0;
		try {
			sbyte smallNumImg = isss.readByte();
			imgInfo = new ImageInfo[smallNumImg];
			for (int i = 0; i < smallNumImg; i++) {
				imgInfo[i] = new ImageInfo();
				imgInfo[i].ID = isss.readByte();
				imgInfo[i].x0 = isss.readByte();
				imgInfo[i].y0 = isss.readByte();
				imgInfo[i].w = isss.readByte();
				imgInfo[i].h = isss.readByte();
			}
			short nFrame = isss.readShort();

			frame = new Frame[nFrame];
			for (int i = 0; i < nFrame; i++) {
				frame[i] = new Frame();
				sbyte nPart = isss.readByte();
				frame[i].dx = new short[nPart];
				frame[i].dy = new short[nPart];
				frame[i].idImg = new sbyte[nPart];
				for (int a = 0; a < nPart; a++) {
					frame[i].dx[a] = isss.readShort();
					frame[i].dy[a] = isss.readShort();
					frame[i].idImg[a] = isss.readByte();
					if (i == 0) {
						if (left > frame[i].dx[a])
							left = frame[i].dx[a];
						if (top > frame[i].dy[a])
							top = frame[i].dy[a];
						if (right < frame[i].dx[a] + imgInfo[frame[i].idImg[a]].w)
							right = frame[i].dx[a] + imgInfo[frame[i].idImg[a]].w;
						if (bottom < frame[i].dy[a] + imgInfo[frame[i].idImg[a]].h)
							bottom = frame[i].dy[a] + imgInfo[frame[i].idImg[a]].h;
						width = right - left;
						height = bottom - top;
						
					}
				}

			}
			short nFrameCount = isss.readShort();
		//	Res.out("nFrame count = " + nFrameCount + " ...........................................................");
			arrFrame = new short[nFrameCount];
			for (int i = 0; i < nFrameCount; i++) {
                arrFrame[i] = isss.readShort();
			}
			

		} catch (Exception e) {
			//e.printStackTrace();
			// TODO: handle exception
		}
	}

	public void readData(sbyte[] data) {

		DataInputStream isss = null;
		//ByteArrayInputStream array = new ByteArrayInputStream(data);
        isss = new DataInputStream(data);
        readData(isss);
	}

	public void paintFrame(mGraphics g, int f, int x, int y, int trans, int layer) {
		
		
		if (frame != null && frame.Length != 0) {
			Frame fr = frame[f];
			for (int i = 0; i < fr.dx.Length; i++) {
				ImageInfo im = getImageInfo(fr.idImg[i]);
				 try{
				if (trans == 0) {
					g.drawRegion(img, im.x0, im.y0, im.w, im.h, 0, x + fr.dx[i], y + fr.dy[i]
							- ((layer < 4 && layer > 0) ? trans : 0), 0);
				}
				if (trans == 1) {
					g.drawRegion(img, im.x0, im.y0, im.w, im.h, Sprite.TRANS_MIRROR, x - fr.dx[i], y + fr.dy[i]
							- ((layer < 4 && layer > 0) ? trans : 0), StaticObj.TOP_RIGHT);
				}
				 }catch (Exception e) {
//				 System.out.println("ERROR AT EFFECTDATA "+ID+" "+im.x0+","+
//				 im.y0+","+im.w+","+ im.h);
				 }
			}

		}
	}
}
