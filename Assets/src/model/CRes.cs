
using System.IO;
public class CRes {
	private static short[] sinn = { 0, 18, 36, 54, 71, 89, 107, 125, 143, 160,
			178, 195, 213, 230, 248, 265, 282, 299, 316, 333, 350, 367, 384,
			400, 416, 433, 449, 465, 481, 496, 512, 527, 543, 558, 573, 587,
			602, 616, 630, 644, 658, 672, 685, 698, 711, 724, 737, 749, 761,
			773, 784, 796, 807, 818, 828, 839, 849, 859, 868, 878, 887, 896,
			904, 912, 920, 928, 935, 943, 949, 956, 962, 968, 974, 979, 984,
			989, 994, 998, 1002, 1005, 1008, 1011, 1014, 1016, 1018, 1020,
			1022, 1023, 1023, 1024, 1024 };
	private static short[] coss;
	private static int[] tann;
    public static void loadbegin(){
		coss = new short[91];
		tann = new int[91];
		for (int i = 0; i <= 90; i++) {
			coss[i] = sinn[90 - i];
			if (coss[i] == 0)
				tann[i] = int.MaxValue;
			else
				tann[i] = (sinn[i] << 10) / coss[i];
        }
    }

    public static int getDistance(int x, int y, int x2, int y2)
    {
        return getDistance(x - x2, y - y2);
    }
    public static int getDistance(int x, int y)
    {
        return (sqrt((x) * (x) + (y) * (y)));
    }

	// tinh sin goc 0-359
	public static int sin(int a) {
		if (a >= 0 && a < 90)
			return sinn[a];
		if (a >= 90 && a < 180)
			return sinn[180 - a];
		if (a >= 180 && a < 270)
			return -sinn[a - 180];
		return -sinn[360 - a];
	}	
	public static int xetVX(int goc, int luc) {
		
		return (cos((goc)) * luc) / 1024;

	}

	public static int xetVY(int goc, int luc) {
		
		return ((sin((goc)) * luc) /1024);
	}

	// tinh cos goc 0-359
	public static int cos(int a) {
		if (a >= 0 && a < 90)
			return coss[a];
		if (a >= 90 && a < 180)
			return -coss[180 - a];
		if (a >= 180 && a < 270)
			return -coss[a - 180];
		return coss[360 - a];
	}

	// tinh tan goc 0-359
	public static int tan(int a) {
		if (a >= 0 && a < 90)
			return tann[a];
		if (a >= 90 && a < 180)
			return -tann[180 - a];
		if (a >= 180 && a < 270)
			return tann[a - 180];
		return -tann[360 - a];
	}

	public static int atan(int a) {
		for (int i = 0; i <= 90; i++)
			if (tann[i] >= a)
				return i;
		return 0;
	}

	// tinh goc tao boi dxWear, dyWear
	public static int angle(int dx, int dy) {
		int angle;
		if (dx != 0) {
			int tan = Math.abs((dy << 10) / dx);
			angle = atan(tan);
			if (dy >= 0 && dx < 0)
				angle = 180 - angle;
			if (dy < 0 && dx < 0)
				angle = 180 + angle;
			if (dy < 0 && dx >= 0)
				angle = 360 - angle;
			// System.out.println(a+" "+angle);
		} else {
			angle = dy > 0 ? 90 : 270;
		}
		return angle;
	}

	// dieu chinh lai goc <0 hoac >360 ve trong khoang 0-360
	public static int fixangle(int angle) {
		if (angle >= 360)
			angle %= 360;
		if (angle < 0)
			angle = 360 + (angle % 360);
		return angle;
	}

	// tinh hieu cua 2 goc
	public static int subangle(int a1, int a2) {
		int a = a2 - a1;
		if (a < -180)
			return a + 360;
		if (a > 180)
			return a - 360;
		return a;
	}

	public static int abs(int a) {
		if (a < 0)
			return -a;
		return a;
	}

	static int value = 1;

	public static mRandom r = new mRandom();

	public static int random(int a) {
		return r.nextInt(a);
	}

	public static int random_Am_0(int a) {
		int t = 0;
		while (t == 0) {
			t = r.nextInt() % a;
		}
		return t;
	}

	public static int random_Am(int a, int b) {
		int t = a + (r.nextInt(b - a));
		if (random(2) == 0)
			t = -t;
		return t;
	}

    public static int random(int a, int b)
    {
        if (b - a < 0) return 0;
        return a + (r.nextInt(b - a));
	}

	public static int sqrt(int a) {
		int x, x1;
		if (a <= 0)
			return 0;
		x = (a + 1) / 2;
		do {
			x1 = x;
			x = x / 2 + a / (2 * x);
		} while (Math.abs(x1 - x) > 1);
		return x;
	}

	public static int setDis(int x1, int y1, int x2, int y2) {
		return (abs(x1 - x2) + abs(y1 - y2));
	}





	


	



	// public static mImage createImgByByteArray(byte[] array) {
	// return mImage.createImage(array, 0, array.Length);
	// }

	public static int readSignByte(InputStream iss) {
		sbyte[] x = new sbyte[1];
		try {
			iss.read(ref x, 0, 1);
		} catch (IOException e) {
			//e.printStackTrace();
			mSystem.outloi("loi Cres 5");
		}
		return x[0];
	}





	// //////////////////////
	
	// or
	// skill]

	
	// public static mImage createImgByHeader(byte[] header, byte[] data) {
	// byte[] total = new byte[header.Length + data.Length];
	// System.arraycopy(header, 0, total, 0, header.Length);
	// System.arraycopy(data, 0, total, header.Length, data.Length);
	// return Image.createImage(total, 0, total.Length);
	// }

	public static sbyte[] encoding(sbyte[] array) {
		if (array != null)
			for (int i = 0; i < array.Length; i++) {
				array[i] = (sbyte) (~array[i]);
			}
		return array;
	}

	public static void saveRMSName(sbyte ID, sbyte[] data)  {
		//GlobalService.gI().Save_RMS_Server((byte) 0, ID, data);
		// mSystem.out("okkkkkkkkkkkkkkkk");
		// saveRMS(namePlayer + "_" + filename, data);

	}

	// public static void loadRMSName(byte ID, byte[] data) {
	// // return loadRMS(namePlayer + "_" + filename);
	// MainRMS.setLoadRMS(ID, data);
	// }
	public static bool CheckDelRMS(string str) {
		if (str.CompareTo("isLowDevice") == 0 || str.CompareTo("isQty") == 0
				|| str.CompareTo("user_pass") == 0) {
			return false;
		}
		return true;
	}
	
	public static bool checkCollider(int x1, int xw1, int x2, int xw2, int y1, int yh1,int y2, int yh2) {	
		if (x1 > xw2 || xw1 < x2 || y1 > yh2 || yh1 < y2){
			return false;
		}
		return true;
	}
	public static int pow(int data, int x) {
		// TODO Auto-generated method stub
		int t=1;
		for(int i=0;i<x;i++){
			t*=data;
		}
		return t;
	}
	
}
