using System;
using System.IO;
using System.Threading;
public class GameCanvas : TCanvas, IActionListener {
	public static bool lowGraphic = false;

    public static int transY = 15;
	public static bool isMoveNumberPad = true;
	public static bool isLoading;
	public static bool isTouch = false; // ---> Màn hình cảm ứng
	public static bool isTouchControl; // ----> Điều khiển bằng cảm ứngo
	public static bool isTouchControlSmallScreen; // ----> Điều khiển bằng
    public static GameScr gameScr;													// cảm ứng màn hình nhỏ
	public static bool isTouchControlLargeScreen; // ----> Điều khiển bằng
    public static TabScreenNew AllInfo/* = new TabScreenNew()*/;
    public static Paint paintt;											// cảm ứng màn hình lớn
	public static GameCanvas instance;
    public static sbyte zoomLevel;
    public static LoginScr loginScrr;
    public static LanguageScr languageScr;
    public static bool paintBG;
    public static int[] imgBGWidth;
    public static int skyColor, idx, timeBallEffect;

	public static int gsskyHeight;
	public static int gsgreenField1Y, gsgreenField2Y, gshouseY, gsmountainY,
			bgLayer0y, bgLayer1y;
	public static mBitmap imgCloud, imgSun;
	public static mBitmap[] imgBorder = new mBitmap[3];
    public static int borderConnerW, borderConnerH, borderLineW, borderLineH, borderCenterW, borderCenterH, sunX, sunY;
	public static int[] cloudX, cloudY;
    public static InputDlg inputDlg;
    public static Input2Dlg input2Dlg;
    public string url = "";
    public static bool isPointerSelect = false, isPointerMove = false,
            isPointerEnd = false;
	// 0 1 2 3 4 5 6 7 8 9 * LSoft RSoft
	public static bool[] keyPressed = new bool[21];
    public static bool[] keyReleased = new bool[21];
    public static bool[] keyHold = new bool[21];
	public static bool isPointerDown;
	public static bool isPointerClick;
	public static bool isPointerJustRelease;
	public static int px, py, pxFirst, pyFirst, pxLast, pyLast;
	public static Position[] arrPos = new Position[4];
	public static int gameTick, taskTick,waitTick,curPos;
	public static bool isEff1, isEff2,isChee=true;
	public static long timeTickEff1, timeTickEff2;
	public static int hCommand = 25;
	public const int ENDDLG  = 8882;
	public const int VELANG  = 8883;
	public const int HOISINH = 8884;
	public const int THOATGAME = 8885;
    public const int TAIGAME = 8886;
    public const int TAIPHIENBANMOI = 8887;
	public const int SUBMITINFO = 8888;
	public const int OKPOPUP = 8889;
    public const int NOPOPUP = 8890;
	// Width Height Half-W, Half-H, width/3, height/3, 2*width/3, 2*height/2,
	// ...

    public static mBitmap[] imgBG;
	public static int w, h, hw, hh, wd3, hd3, w2d3, h2d3, w3d4, h3d4, wd6, hd6;
	public static Screen currentScreen;

	public static Menu menu;
	
	public static Dialog currentDialog;
	public static MsgDlg msgdlg;
	public static mVector currentPopup;
	public static int requestLoseCount;
	public static Paint paintz;	
	static long lastTimePress = 0;
	public static int keyAsciiPress;
	public Command cmdError;
	mGraphics g = new mGraphics();
	//------------------music
	public static bool isResume;
	public static bool isPlaySound=true;

    public static string thongbao = "";
    public static int indexThongBao;
    public static bool isPaintThongBao;
    
	public const int cEndDgl = 8882;
	public const int cLogout = 9990;
	public const int cHoiSinh = 9991;
	public const int cVeLang = 9992;
    public const int cDongYThachDau = 9993;
    public static mVector keybgLoginSelect = new mVector();
	
	//------------------music
	//cho android
	
	 //cho java
	 public GameCanvas(){
	 // super();



      mFont.loadBegin();
      initGameCanvas();
	  initGame();
      GameMidlet.asset = new Asset();
      GameCanvas.currentScreen = new SplashScr();
      languageScr = new LanguageScr();
      AllInfo = new TabScreenNew();
	 }
	public void initGame(){

        listPoint = new mVector();
		w = getWidthz();
		h = getHeightz();
		menu = new Menu();	
		currentPopup = new mVector();
		
		hw = w / 2;
		hh = h / 2;
	

		if (hasPointerEvents()) {
			isTouch = true;
			if (GameCanvas.w >= 240)
				isTouchControl = true;
			if (GameCanvas.w < 320)
				isTouchControlSmallScreen = true;
			if (GameCanvas.w >= 320)
				isTouchControlLargeScreen = true;
		}
		msgdlg = new MsgDlg();
		if (GameCanvas.h <= 160) {
			Paint.hTab = 15;
		}
		instance = this;
		mSystem.gcc();
		initPaint();	
		debugUpdate = new mVector();
		debugPaint = new mVector();
		debugSession = new mVector();			
		loadBegin();
        //mainChar = new MainChar();
        //mainChar.x=150;
        //mainChar.xTo=150;
        //mainChar.y=200;
        //mainChar.yTo=200;
//	    GameScr.actor.addElement(mainChar);
		
		Cout.println(w+":: "+h);
		
		cmdError= new Command("", 999999);
	}	
	
	
	/**
	 * @param resetToLoginScr: hàm dùng để resetLogin;
	 */
    public void resetToLoginScr() // GOI TRONG MAIN THREAD
    {
        //  paintBG = true;

        GuiMain.vecItemOther.removeAllElements();

        Quest.listUnReceiveQuest.removeAllElements();
        Quest.vecQuestFinish.removeAllElements();
        Quest.vecQuestDoing_Main.removeAllElements();
        Quest.vecQuestDoing_Sub.removeAllElements();
        ChatPrivate.vOtherchar.removeAllElements();
        ChatPrivate.nTinChuaDoc = 0;
        Char.myChar().typePk = -1;
        if (GameCanvas.currentScreen != LoginScr.gI())
        {
            LoginScr.gI().switchToMe();
            GameCanvas.gameScr = null;
            GameScr.vCharInMap.removeAllElements();
            Session_ME.gI().close();
            loadBG(0);
           // startOKDlg("Mất kết nối với máy chủ. Vui lòng thử lại !");
        }
        else
        {
           // startOKDlg("Có lỗi đăng nhập xảy ra. Vui lòng thử lại !");
        }
        //  isLoading = false;
        //  resetToLoginScr = true;
    }
	/**
	 * @param loadBegin: Hàm dùng để khởi tạo giá trị BAN ĐẦU của các biến STATIC cho giống Unity
	 */
	public void loadBegin(){
//		string namelang = Locale.getDefault().getLanguage();
//		for (int i = 0; i < mText.nameLv.Length; i++) {
//			if(mText.nameLv[i].Equals(namelang))
//				mText.leVel = (byte)i;
//		}
        paintt = new Paint();
        loadDust();
        TabInfoChar.loadbegin();
        GameScr.loadBegin();
        loadImages();
        TField.loadBegin();
//		SmallImage.readEffect();
//		SmallImage.readSkill();
//		for (int i = 0; i < Char.bongChar.Length; i++) {
//			for (int j = 0; j < Char.bongChar[i].Length; j++) {
//				ImageData small =(ImageData)Char.bongChar[i][j];
//				small = new ImageData();
//				small.id = (short)(i+j);
//			}
//		}
//		sbyte[] part = Actor.loadPart();
//		if(part!=null){
//			SmallImage.readPart(part);
//		}
//		else
//		SmallImage.readPart();
	}
	
	public static GameCanvas gI() {
		return instance;
	}

	public void initPaint() {
		paintz = new Paint();
	}	
	public static void connect() {
        GameMidlet.PORT = 19158;
		//int port = 19158;
        if (!LoginScr.isTest)
            GameMidlet.IP = "127.0.0.1";// ""; 119.81.220.196 //103.15.51.32
        Cout.println(GameMidlet.IP + "  connect  " + GameMidlet.PORT);
		Session_ME.gI().connect(GameMidlet.IP, GameMidlet.PORT);

	}
	public static void resetTrans(mGraphics g){
		  g.translate(-g.getTranslateX(), -g.getTranslateY());
		  g.setClip(0, 0, w, h);  
		 }
	public void initGameCanvas() {	
		w = getWidthz();
		h = getHeightz();		
		hw = w / 2;
		hh = h / 2;
		wd3 = w / 3;
		hd3 = h / 3;
		w2d3 = 2 * w / 3;
		h2d3 = 2 * h / 3;
		w3d4 = 3 * w / 4;
		h3d4 = 3 * h / 4;
		wd6 = w / 6;
		hd6 = h / 6;

        CRes.loadbegin();
        SmallImage.loadBigImage();

        MsgDlg.loadbegin();
        mResources.loadLanguage();
        GameScr.loadImages();
        loadImageInterface.loadImage();
        loginScrr = new LoginScr();
        inputDlg = new InputDlg();
        languageScr = new LanguageScr();
        TField.setVendorTypeMode(TField.NOKIA);
        Screen.ITEM_HEIGHT = 15;
        //Sound.init();
	}

	public static mVector debugUpdate;
	public static mVector debugPaint;
	public static mVector debugSession;
	public override void update() {		
		debugUpdate.removeAllElements();
		long l1 = mSystem.currentTimeMillis_();		
		try {		
			if (l1 - timeTickEff1 >= 780 && !isEff1) {
				timeTickEff1 = l1;
				isEff1 = true;
			} else
				isEff1 = false;
			if (l1 - timeTickEff2 >= 7800 && !isEff2) {
				timeTickEff2 = l1;
				isEff2 = true;
			} else
				isEff2 = false;
			if (taskTick > 0)
				taskTick--;
			gameTick++;
			if (gameTick > 10000) {
				if (mSystem.currentTimeMillis_() - lastTimePress > 20000
						&& GameCanvas.currentScreen == LoginScr.gI()) {
					GameMidlet.instance.exit();
				}
				gameTick = 0;
			}			
			if (currentScreen != null ) {
				 if (currentDialog != null) {
					currentDialog.update();
				}  else if (menu.showMenu) {
					menu.updateMenu();
					menu.updateMenuKey();	
				} 
				if (!isLoading){
					currentScreen.update();
                    if (!menu.showMenu && currentDialog == null)
                    {
                        currentScreen.updateKey();
                        currentScreen.updatePointer();
                    }
				}
				SoundMn.gI().update();
			}
            if (isPaintThongBao)
            {
                indexThongBao++;
                if (indexThongBao > 80)
                {
                    indexThongBao = 0;
                    isPaintThongBao = false;
                }

            }
            if (currentScreen == GameScr.gI() && GameScr.listInfoServer.size() > 0)
            {
                InfoServer info = (InfoServer)GameScr.listInfoServer.elementAt(0);
                if (info != null)
                    info.update();
            }
			InfoDlg.update();
		} catch (Exception e) {
			try {
				Cout.println(this.GetType().Name," " + e.ToString());
			} catch (Exception e2) {
				//e2.printStackTrace();
			}
			try {
				Thread.Sleep(1000);
			} catch (Exception e1) {
				//e1.printStackTrace();
			}
			//e.printStackTrace();
		}

        if (GameCanvas.isPointerJustRelease)
            GameCanvas.isPointerJustRelease = false;
	}	
	public static void fillRect(mGraphics g, int color, int x, int y, int w, int h, int detalY) {
		g.setColor(color);
		g.fillRect(x, y - (detalY != 0 ? (GameScr.cmy >> detalY) : 0), w, h
				+ (detalY != 0 ? (GameScr.cmy >> detalY) : 0));
	}
	public void keyPressedd(int keyCode) {

		lastTimePress = mSystem.currentTimeMillis_();
		if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 122) || keyCode == 10 || keyCode == 8
				|| keyCode == 13 || keyCode == 32 || keyCode==31) {
			keyAsciiPress = keyCode;
		}
		mapKeyPresss(keyCode);
	}

	public static bool isKeyPressed(int index) {
		if (keyPressed[index]) {
			keyPressed[index] = false;
			return true;
		}
		
		return false;
	}
	
	public void mapKeyPresss(int keyCode) {
        GameCanvas.isPointerJustRelease = false;
        if (menu.showMenu)
        {
            //menu.keyPress(keyCode);
        }
        else
        {
            if (currentDialog != null)
            {
                currentDialog.keyPress(keyCode);
                keyAsciiPress = 0;
                return;
            }
            else
                currentScreen.keyPress(keyCode);
        }
		switch (keyCode) {
		case 48:
			keyHold[0] = true;
			keyPressed[0] = true;
			return;
		case 49:// 1
			if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow) {
				keyHold[1] = true;
				keyPressed[1] = true;
			}
			return;
		case 51:// 3
			if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow) {
				keyHold[3] = true;
				keyPressed[3] = true;
			}
			return;
		case 55:// 7
			keyHold[7] = true;
			keyPressed[7] = true;
			return;
		case 57:// 9
			keyHold[9] = true;
			keyPressed[9] = true;
			return;
		case 42:
			keyHold[10] = true;
			keyPressed[10] = true;
			return; // Key [*]
		case 35:
			keyHold[11] = true;
			keyPressed[11] = true;
			return; // Key [#]
		case -6:
		case -21:
			keyHold[12] = true;
			keyPressed[12] = true;
			return; // Soft1
		case -7:
		case -22:
			keyHold[13] = true;
			keyPressed[13] = true;
			return; // Soft2
		case -5:
		case 10:
			 bool result = currentScreen.GetType().IsAssignableFrom(GameScr.gI().GetType());
			
			keyHold[5] = true;
			keyPressed[5] = true;
			return; // [i]
		case -1:
		case -38:
            bool resultz = currentScreen.GetType().IsAssignableFrom(GameScr.gI().GetType());
			
			keyHold[2] = true;
			keyPressed[2] = true;
			return; // UP
		case -2:
		case -39:
            bool resultzz = currentScreen.GetType().IsAssignableFrom(GameScr.gI().GetType());
			
			keyHold[8] = true;
			keyPressed[8] = true;
			return; // DOWN
		case -3:
            bool resultzzz = currentScreen.GetType().IsAssignableFrom(GameScr.gI().GetType());
			
			keyHold[4] = true;
			keyPressed[4] = true;
			return; // LEFT
		case -4:
            bool resultzzzz = currentScreen.GetType().IsAssignableFrom(GameScr.gI().GetType());
			
			keyHold[6] = true;
			keyPressed[6] = true;
			return; // RIGHT

		case 50:// 2
			if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow) {
				keyHold[2] = true;
				keyPressed[2] = true;
			}
			return;
		case 52:// 4
			if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow) {
				keyHold[4] = true;
				keyPressed[4] = true;
			}
			return;
		case 54:// 6
			if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow) {
				keyHold[6] = true;
				keyPressed[6] = true;
			}
			return;
		case 56:// 8
			if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow) {
				keyHold[8] = true;
				keyPressed[8] = true;
			}
			return;
		case 53:// 5
			if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow) {
				keyHold[5] = true;
				keyPressed[5] = true;
			}
			return;

		}

	}

	// Catch key-up input and clear from Input Array
    public void keyReleasedd(int keyCode)
    {
		keyAsciiPress = 0;
		mapKeyReleasee(keyCode);
	}

	public void mapKeyReleasee(int keyCode) {
		switch (keyCode) {
		case 48:
			keyHold[0] = false;
			keyReleased[0] = true;
			return;
		case 49:// 1
			//if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow)
			{
				keyHold[1] = false;
				keyReleased[1] = true;
			}
			return;
		case 51:// 3
		//	if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow)
			{
				keyHold[3] = false;
				keyReleased[3] = true;
			}
			return;
		case 55:// 7
			keyHold[7] = false;
			keyReleased[7] = true;
			return;
		case 57:// 9
			keyHold[9] = false;
			keyReleased[9] = true;
			return;
		case 42:
			keyHold[10] = false;
			keyReleased[10] = true;
			return; // Key [*]
		case 35:
			keyHold[11] = false;
			keyReleased[11] = true;
			return; // Key [#]
		case -6:
		case -21:
			keyHold[12] = false;
			keyReleased[12] = true;
			return; // Soft1
		case -7:
		case -22:
			keyHold[13] = false;
			keyReleased[13] = true;
			return; // Soft2
		case -5:
		case 10:
			keyHold[5] = false;
			keyReleased[5] = true;
			return; // [i]
		case -1:
		case -38:

			keyHold[2] = false;

			return; // UP
		case -2:
		case -39:

			keyHold[8] = false;
			return; // DOWN
		case -3:

			keyHold[4] = false;
			return; // LEFT
		case -4:

			keyHold[6] = false;
			return; // RIGHT

		case 50:// 2
		//	if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow)
			{
				keyHold[2] = false;
				keyReleased[2] = true;
			}
			return;
		case 52:// 4
			//if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow)
			{
				keyHold[4] = false;
				keyReleased[4] = true;
			}
			return;
		case 54:// 6
			//if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow)
			{
				keyHold[6] = false;
				keyReleased[6] = true;
			}
			return;
		case 56:// 8
		//	if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow)
			{
				keyHold[8] = false;
				keyReleased[8] = true;
			}
			return;
		case 53:// 5
		//	if (GameCanvas.currentScreen == GameScr.gI() && isMoveNumberPad && !ChatTextField.gI().isShow) 
			{
				keyHold[5] = false;
				keyReleased[5] = true;
			}
			return;
		}

	}

    public static mVector listPoint;
    public override void onPointerDragged(int x, int y)
    {
        if (isPointerMove)
        {
            listPoint.addElement(new Position(x, y));
        }
        else 
         if (CRes.abs(px - pxLast) >= 10 * mGraphics.zoomLevel || CRes.abs(py - pyLast) >= 10 * mGraphics.zoomLevel)
        {
            isPointerDown = isPointerMove = true;
            isPointerClick = isPointerJustRelease = false;

        }
		px = x;
		py = y;
		curPos++;
		if (curPos > 3)
			curPos = 0;
		arrPos[curPos] = new Position(x, y);		
	}
	public static bool isHoldPress() {
		if (mSystem.currentTimeMillis_() - lastTimePress >= 800)
			return true;
		return false;
	}

	public override void onPointerPressed(int x, int y) {
        isPointerJustRelease = false;
        isPointerJustDown = true;
        isPointerSelect = false;
        isPointerClick =  isPointerDown = true;
        isPointerMove = false;
		lastTimePress = mSystem.currentTimeMillis_();
		pxFirst = x;
		pyFirst = y;
		pxLast = x;
		pyLast = y;
		px = x;
		py = y;

	}

    public override void onPointerReleased(int x, int y)
    {
        if (!isPointerMove && !isPointerEnd)
        {
            isPointerSelect = true;
        }
        isPointerMove = false;
        isPointerEnd = false;
        isPointerDown = false;
       // isPointerClick = true;
        isPointerJustRelease = true;
        px = x;
        py = y;
        clearKeyHold();
        clearKeyPressed();

    }
   
	public static bool isPointerHoldIn(int x, int y, int w, int h) {
		if (!isPointerDown && !isPointerJustRelease)
			return false;
		if (px >= x && px <= x + w && py >= y && py <= y + h)
			return true;
		return false;
	}

	public static void clearKeyPressed() {

		for (int i = 0; i < keyPressed.Length; i++)
			keyPressed[i] = false;
		//isPointerJustRelease = false;

	}

	public static void clearKeyHold() {
		for (int i = 0; i < keyHold.Length; i++)
			keyHold[i] = false;

	}

	static Image imgSignal;
	public static mVector flyTexts = new mVector();
	public override void paint(mGraphics gx) {
//		try {
			resetTrans(g);
			g.setColor(0);
			g.fillRect(0, 0, w, h);
			GameCanvas.debugPaint.removeAllElements();
			if (currentScreen != null ) {
				currentScreen.paint(g);				
			}
			g.translate(-g.getTranslateX(), -g.getTranslateY());
			g.setClip(0, 0, w, h);
            if (isPaintThongBao)
            {
                g.setColor(0x000000, 80);
                g.fillRect(GameCanvas.w - mFont.tahoma_7_white.getWidth(thongbao) - 20, 10 - (indexThongBao > 10 ? 10 : indexThongBao), mFont.tahoma_7_white.getWidth(thongbao) + 20, 20);
                mFont.tahoma_7_white.drawString(g, thongbao, GameCanvas.w - mFont.tahoma_7_white.getWidth(thongbao) - 15, 12 - (indexThongBao > 10 ? 10 : indexThongBao), 0);
                g.disableBlending();
            }
			InfoDlg.paint(g);
			if (currentDialog != null) {

                g.setColor(0x000000, 80);
                g.fillRect(0, 0, w, h);
				currentDialog.paint(g);
			} else if (menu.showMenu) {
				menu.paintMenu(g);
			}
            if (currentScreen == GameScr.gI() && GameScr.listInfoServer.size() > 0)
            {
                InfoServer info = (InfoServer)GameScr.listInfoServer.elementAt(0);
                if (info != null)
                    info.paint(g);
            }
            //for (int i = 0; i < FontSys.listTimeGetFont.size(); i++)
            //{
            //    string a = (string)FontSys.listTimeGetFont.elementAt(i);
            //    mFont.tahoma_7_redsmall.drawString(g, a, 10, 10 + 20 * i, 0);
            //}
            //mFont.tahoma_7b_red.drawString(g, TabNhanVat.nameThread+ TabNhanVat.demloadAnh, 80, 80, 0);
//		} catch (Exception e) {
//			Cout.println("LOI HAM PAINT GAMECANVAS "+ e.toString());
//		}
	}
	/**
	 * 
	 * @param paintChangeMap: paint màn hình màu đen khi đang chuyển Map;
	 */
	public void paintChangeMap(mGraphics g)
    {
    }	
	public static bool isPointerJustDown = false;

	public static int hText=12;
	int count = 1;

	/**
	 * @param i
	 * @return
	 */

	public static void endDlg() {
		GameCanvas.inputDlg.tfInput.setMaxTextLenght(500);
		currentDialog = null;
		InfoDlg.hide();
	}

    public static void startOKDlg(string info)
    {
        currentDialog = null;
		msgdlg.setInfo(info, null, new Command("OK", GameCanvas.instance, ENDDLG, null), null);
		currentDialog = msgdlg;
	}
	public static void startDlgMainCharDie()
	{
		
	}
    public static void startDlgExitGame()
    {
       
    }
	public static void startDlgMainCharDieKoDuXu()
	{
		
	}
	public static void startWaitDlg(string info) {
        //msgdlg.setInfo(info, null, new Command("No", GameCanvas.instance, ENDDLG, null), null);
        //currentDialog = msgdlg;
        //msgdlg.isWait = true;

        msgdlg.setInfo(info, null, new Command(mResources.CANCEL, GameCanvas.instance, 8882, null), null);
        //msgdlg.isPaintCham = true;
        currentDialog = msgdlg;
       // msgdlg.isWait = true;
	}
	public static void startOKDlg(string info, bool isError) {
		startOKDlg(info);
	}

	public static void startWaitDlg() {
		startWaitDlg("Chờ đợi");
	}

	public void openWeb(string strLeft, string strRight, string url, string str) {
		msgdlg.setInfo(str, new Command(strLeft, this, 8881, url), null, new Command(strRight, this, ENDDLG, null));
		currentDialog = msgdlg;
	}

	
	public static void startOK(string info, int actionID, Object p) {
		// BB: move command from left to center
		msgdlg.setInfo(info, null, new Command("OK", instance, actionID, p), null);
        msgdlg.show(); 
        GameCanvas.currentDialog = GameCanvas.msgdlg;
	}

	public static void startYesNoDlg(string info, int iYes, Object pYes, int iNo, Object pNo) {
        msgdlg.setInfo(info, new Command("OK", instance, iYes, pYes), new Command("", instance, iYes, pYes),
                new Command("No", instance, iNo, pNo));
		msgdlg.show();
        GameCanvas.currentDialog = GameCanvas.msgdlg;
	}

    public static void startYesNoDlg(string info, Command cmdYes, Command cmdNo)
    {
		msgdlg.setInfo(info, cmdYes, null, cmdNo);
		msgdlg.show();
        GameCanvas.currentDialog = GameCanvas.msgdlg;
        Cout.println("startYesNoDlg  " + currentDialog);
	}

	public static string getMoneys(int m) {
		string str = "";
		int mm = m / 1000 + 1;
		for (int i = 0; i < mm; i++) {
			if (m >= 1000) {
				int a = m % 1000;
				if (a == 0)
					str = ".000" + str;
				else if (a < 10)
					str = ".00" + a + str;
				else if (a < 100)
					str = ".0" + a + str;
				else
					str = "." + a + str;
				m /= 1000;
			} else {
				str = m + str;
				break;
			}
		}
		return str;
	}

	
	public static int getX(int start, int w) {
		return (px - start) / w;
	}

	public static int getY(int start, int w) {
		return (py - start) / w;
	}

	protected void sizeChanged(int w, int h) {
	}	
//	public static Image loadImageRMS(string path) {
//		path = "/x" + mGraphics.zoomLevel + path;
//		Image img = null;
//		try {
//			img = Image.createImage(path);
//		} catch (IOException e) {
//			try {
//				string[] splitPng=Res.split(path, ".", 0);
//				string[] pathSplit= Res.split(splitPng[0], "/", 0);
//				string rmsPath="x"+mGraphics.zoomLevel+pathSplit[pathSplit.Length-1];
//				sbyte[] data=Rms.loadRMS(rmsPath);
//				if(data!=null){
//					Cout.println("data lent= "+data.Length);
//					img=Image.createImage(data, 0, data.Length);
//					Cout.println("rms path= "+rmsPath+"img w= "+img.getWidth()+" imgH= "+img.getHeight());
//				}
//				else{
//					Cout.println("loadImageRMS null");
//				}
//			} catch (Exception e2) {
//				// TODO: handle exception
//			}
//		}
//		return img;
//	}	
	public static bool isRMS(string path)
	{
		bool rms = false;
		 sbyte []data = Rms.loadRMS(path);
		  if(data!=null){
			  rms = true;
		  }
		  return rms;
	}
    public static void loadImages() {
       // imgShuriken = GameCanvas.loadImage("/u/f.png");
      //  imgCheckPass = GameCanvas.loadImage("/u/check.png");

        //for (int i = 0; i < 3; i++)
        //{
        //    imgBorder[i] = GameCanvas.loadImage("/hd/bd" + i + ".png");
        //}
        CreatCharScr.loadImage();
        borderConnerW = mGraphics.getImageWidth(imgBorder[0]);
        borderConnerH = mGraphics.getImageHeight(imgBorder[0]);
        borderLineW = mGraphics.getImageWidth(imgBorder[1]);
        borderLineH = mGraphics.getImageHeight(imgBorder[1]);
        borderCenterW = mGraphics.getImageWidth(imgBorder[2]);
        borderCenterH = mGraphics.getImageHeight(imgBorder[2]);

        SplashScr.imgLogo = loadImage("/logo.png");
	
    }
	public static mBitmap loadImage(string path) {
		  //String a=lib.Main.isPC?"":".png";
        mBitmap imgg = null;
        imgg = (mBitmap)Image.listImgLoad.get(path);
        if (imgg != null)
        {
            return imgg;
        }

        mBitmap img = new mBitmap();
		  sbyte []data = Rms.loadRMS(mSystem.getPathRMS(cutPng("/x" +mGraphics.zoomLevel + path)));
         
		  if(data!=null){
              img = new mBitmap();
              img.image = Image.createImage(data, 0, data.Length);
              data = null;
              if (img != null && img.image.texture != null)
              {
                  img.w = img.image.width = img.image.texture.width;
                  img.h = img.height = img.image.height = img.image.texture.height;
                  img.width = img.w / mGraphics.zoomLevel;
                  img.height = img.h / mGraphics.zoomLevel;
              }
		      return img;
		      
		//  System.out.println("YES PATH = "+mSystem.getPathRMS(path));
		  }//else System.out.println("PATH = "+mSystem.getPathRMS(path));
		  //string x="/x";
		  //path = x + (/*mGraphics.zoomLevel==3?2:*/TCanvas.TileZoom) + path; 
		  try {
              path = Main.res + cutPng("/x" +mGraphics.zoomLevel + path);

              img.image = Image.createImage(path);
              if (img != null && img.image != null && img.image.texture != null)
              {
                  img.image.width = img.image.texture.width;
                  img.image.height = img.image.texture.height;
                  img.width = img.image.width / mGraphics.zoomLevel;
                  img.height = img.image.height / mGraphics.zoomLevel;
              }
              if (img.image==null||img.image.texture == null)
              {
                  //Cout.println("path nullllll   "+path);
                  return null;
              }
		  } catch (IOException e) {

              return null;
		  }    
		  return img;
	}
    public static string cutPng(string str)
    {
        string tam = str;
        if (str.Contains(".png"))
        {
            tam = str.Replace(".png", "");
        }
        if (str.Contains(".jpg"))
        {
            tam = str.Replace(".jpg", "");
        }
        if (str.Contains(".bytes"))
        {
            tam = str.Replace(".bytes", "");
        }
        return tam;
    }
	public static bool CheckRMSloadImage(string path) {
       // return true; //chi true khi tat ca anh duoi client
        sbyte[] data = Rms.loadRMS(mSystem.getPathRMS(path));
        if (data != null)
        {
            return true;
        }
        return false;
	}

	public static bool isPointer(int x, int y, int w, int h) {
        //if (!isPointerDown && !isPointerJustRelease)
        //    return false;
		if (px >= x && px <= x + w && py >= y && py <= y + h)
			return true;
		return false;
	}
    public static bool isPointSelect(int x, int y, int w, int h)
    {
        if (!isPointerJustRelease)
        {
            return false;
        }
        return isPointer(x, y, w, h);
    }
	public static bool isPointer2(int x, int y, int w, int h) {		
		if (px >= x && px <= x + w && py >= y && py <= y + h)
			return true;
		return false;
	}
	
	public static bool TouchX(){
		if(Math.abs(px- pxLast)<=10)
			return true;
		return false;
	}
	
	public static bool TouchY(){
		if(Math.abs(py- pyLast)<=10)
			return true;
		return false;
	}

    public void perform(int idAction, Object p)
    {
        currentDialog = null;
        Cout.println("performmmmmmmmmmm  "+idAction);
        switch (idAction)
        {
            case cLogout:
                LoginScr.isLogout = true;
                Service.gI().PlayerOut();
                resetToLoginScr(true);
                break;
            case cHoiSinh:
                Service.gI().HoiSinh((sbyte)1);
                break;
            case cVeLang:
                Service.gI().HoiSinh((sbyte)0);
                break;
            case cDongYThachDau:
                string id = (string)p;
                Service.gI().ThachDau((sbyte)1, short.Parse(id));
                break;
            case ENDDLG:
                GameCanvas.endDlg();
                break;
                
        }
    }
        public void resetToLoginScr(bool isLogout) {
		//  paintBG = true;
		Char.myChar().typePk = -1;

		GuiMain.vecItemOther.removeAllElements();
		Char.myChar().clearAllFocus();
		Char.myCharr = null;
		Quest.listUnReceiveQuest.removeAllElements();
		Quest.vecQuestFinish.removeAllElements();
		Quest.vecQuestDoing_Main.removeAllElements();
		Quest.vecQuestDoing_Sub.removeAllElements();
		ChatPrivate.vOtherchar.removeAllElements();
		ChatPrivate.nTinChuaDoc = 0;
		if(GameCanvas.currentScreen!=LoginScr.gI()){
			LoginScr.gI().switchToMe();
			GameCanvas.gameScr = null;
			loadBG(0);
			GameScr.vCharInMap.removeAllElements();
			Session_ME.gI().close();
			if(!isLogout)
			startOKDlg("Mất kết nối với máy chủ. Vui lòng thử lại !");
		}else{
			startOKDlg("Có lỗi đăng nhập xảy ra. Vui lòng thử lại !");
		}
	}
	public static void clearAllPointerEvent() {
        isPointerClick = false;
        isPointerDown = false;

        isPointerMove = false;
        isPointerSelect = false;
        isPointerJustDown = false;
        isPointerJustRelease = false;
	}




    internal static Image loadImageRMS(string p)
    {
        return null;
    }

    internal static bool checkImageReource(string path)
    {
        path = Main.res + cutPng("/x" + TCanvas.TileZoom + path);
        return Image.CheckTonTaiAnhDuoiReSource(path);
    }
    public static void XoaToanBoRms()
    {
        Rms.deleteAllRecord();
    }

    public static void clearPointerEvent()
    {
        isPointerMove = isPointerClick = isPointerDown = isPointerJustRelease = false;
    }

    public static int wCommand { get; set; }


    internal static bool isPointerInGame(int p, int p_2, int p_3, int p_4)
    {
        throw new NotImplementedException();
    }
    public static int[] bgSpeed;
	public int[] dustX, dustY, dustState;

	public bool startDust(int dir, int x, int y) {
		if (lowGraphic)
			return false;
		int i = dir == 1 ? 0 : 1;
		if (dustState!=null&&dustState[i] != -1)
			return false;
		if (dustState!=null)
		dustState[i] = 0;
		dustX[i] = x;
		dustY[i] = y;
		return true;
	}

	public static int[] wsX, wsY, wsState, wsF;
    public static mBitmap[] imgWS;
     public static mBitmap    imgShuriken, imgCheckPass;
	public static mBitmap[][] imgDust;


	public void loadDust() {
		// if (lowGraphic || level == 9 || level == 10 || level == 8
		// || level == 14)
		if (lowGraphic)
			return;
		if (imgDust == null) {
			imgDust = new mBitmap[2][];
            for (int i = 0; i < imgDust.Length; i++)
            {
                imgDust[i] = new mBitmap[5];
            }
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < 5; j++)
					imgDust[i][j] = GameCanvas.loadImage(("/e/d" + i) + (j + ".png"));
		}
		dustX = new int[2];
		dustY = new int[2];
		dustState = new int[2];
		dustState[0] = dustState[1] = -1;
	}
	
	public void updateDust() {
		// if (lowGraphic || level == 9 || level == 10 || level == 8
		// || level == 14)
		if (lowGraphic)
			return;

		for (int i = 0; i < 2; i++) {
			if (dustState[i] != -1) {
				dustState[i]++;
				if (dustState[i] >= 5)
					dustState[i] = -1;
				if (i == 0)
					dustX[i]--;
				else
					dustX[i]++;
				dustY[i]--;
			}
		}
	}

	public static bool isPaint(int x, int y) {
		if (x < GameScr.cmx)
			return false;
		if (x > GameScr.cmx + GameScr.gW)
			return false;
		if (y < GameScr.cmy)
			return false;
		if (y > GameScr.cmy + GameScr.gH + 30)
			return false;
		return true;
	}

	public void paintDust(mGraphics g) {
		// if (lowGraphic || level == 9 || level == 10 || level == 8
		// || level == 14)
		if (lowGraphic)
			return;
		for (int i = 0; i < 2; i++) {
			if (dustState[i] != -1) {
				if(isPaint(dustX[i], dustY[i]))
					g.drawImage(imgDust[i][dustState[i]], dustX[i], dustY[i], 3);
			}
		}
	}

    public static int typeBg = -1;
    public static bool loadBG(int typeBG) {
        int deltaLayer1 = 0;
        int deltaLayer2 = 0;
        int deltaLayer3 = 0;
        typeBg = typeBG;
        switch (typeBG)
        {
            case 0:
                // typeBg = 0;

                break;
            case 2:
                deltaLayer1 = 16;
                deltaLayer2 = 10;
                deltaLayer3 = 10;
                // typeBg = 2;
                bgSpeed = new int[] { 1, 2, 3, 4 };
                break;
            case 3:
                // typeBg = 3;
                bgSpeed = new int[] { 1, 2, 3, 4 };
                break;
            case 4:
                // typeBg = 4;
                deltaLayer1 = 9;
                deltaLayer2 = 6;
                bgSpeed = new int[] { 1, 2, 3, 4 };
                break;
            case 5:
                // typeBg = 5;
                bgSpeed = new int[] { 1, 1, 1, 1 };
                break;
            case 6:
                // typeBg = 6;
                deltaLayer1 = 12;
                bgSpeed = new int[] { 1, 2, 3, 4 };
                break;
            case 12:
                // typeBg = 12;
                bgSpeed = new int[] { 1, 2, 3, 4 };
                break;
            case 11:
                // typeBg = 11;
                bgSpeed = new int[] { 1, 2, 3, 4 };
                break;
            case 10:
                // typeBg = 10;
                bgSpeed = new int[] { 1, 1, 1, 1 };
                break;
            case 9:
                // typeBg = 9;
                deltaLayer1 = 16;
                deltaLayer2 = 10;
                deltaLayer3 = 10;
                bgSpeed = new int[] { 1, 2, 3, 4 };
                break;
            case 8:
                // typeBg = 8;
                bgSpeed = new int[] { 1, 2, 3, 4 };
                break;
            case 7:
                // typeBg = 7;
                bgSpeed = new int[] { 1, 2, 3, 4 };
                break;
        }

        GameCanvas.skyColor = StaticObj.SKYCOLOR[typeBg];

        try
        {
            if (!lowGraphic)
            {
                imgBG = new mBitmap[4];
                imgBGWidth = new int[4];
                for (int i = 0; i < 4; i++)
                {
                    try
                    {
                        if (StaticObj.TYPEBG[typeBg][i] != -1)
                        {
                            String path = "/bg/bg" + i + StaticObj.TYPEBG[typeBg][i] + ".png";
                            if (currentScreen != GameScr.gI())
                                keybgLoginSelect.add(path);
                            imgBG[i] = GameCanvas.loadImage("/bg/bg" + i + StaticObj.TYPEBG[typeBg][i] + ".png");
                            if (imgBG[i] == null)
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        //e.printStackTrace();
                        return false;

                    }
                    if (imgBG[i] != null)
                        imgBGWidth[i] = mGraphics.getImageWidth(imgBG[i]);
                }

                if (typeBg == 10)
                {
                    imgBG[1] = GameCanvas.loadImage("/bg/bg09.png");
                    imgBG[2] = GameCanvas.loadImage("/bg/bg09.png");
                    imgBGWidth[1] = mGraphics.getImageWidth(imgBG[1]);
                    imgBGWidth[2] = mGraphics.getImageWidth(imgBG[2]);
                }
                if (typeBg == 12)
                {
                    imgBG[3] = GameCanvas.loadImage("/bg/bg39.png");
                    imgBGWidth[3] = mGraphics.getImageWidth(imgBG[3]);
                }
            }

            if (typeBg >= 0 && typeBg <= 1)
            {
                imgCloud = GameCanvas.loadImage("/bg/cl0.png");
                imgSun = GameCanvas.loadImage("/bg/sun0.png");
            }
            else
            {
                imgCloud = null;
                imgSun = null;
            }
            //			if (typeBg == 2) {
            //				imgCloud = GameCanvas.loadImage("/bg/cl1.png");
            //				imgSun = GameCanvas.loadImage("/bg/sun1.png");
            //			}
            if (typeBg == 7 || typeBg == 11 || typeBg == 12)
            {
                if (TileMap.mapID == 20)
                {
                    imgCloud = null;
                }
                else
                    imgCloud = GameCanvas.loadImage("/bg/cl0.png");
            }
        }
        catch (Exception e)
        {
            Cout.println("falseeeeeeeeeeeeeee  ");
            return false;
        }

        paintBG = false;
        if (!lowGraphic)
        {
            paintBG = true;
            //			if (imgBG[0] != null && imgBG[1] != null && imgBG[2] != null)
            //				gsskyHeight = GameScr.gH - (mGraphics.getImageHeight(imgBG[0]) + mGraphics.getImageHeight(imgBG[1]) + mGraphics.getImageHeight(imgBG[2])) /*+ 11*/;
            //			if (imgBG[0] != null)
            //				gsgreenField1Y = GameScr.gH - mGraphics.getImageHeight(imgBG[0]);
            //			if (imgBG[1] != null)
            //				gsgreenField2Y = gsgreenField1Y - mGraphics.getImageHeight(imgBG[1]);
            //			if (imgBG[2] != null)
            //				gshouseY = gsgreenField2Y - mGraphics.getImageHeight(imgBG[2]);
            //			if (imgBG[3] != null)
            //				gsmountainY = gsgreenField2Y - mGraphics.getImageHeight(imgBG[3]) /*- 10*/;
            if (imgBG[0] != null && imgBG[1] != null && imgBG[2] != null)
                gsskyHeight = GameScr.gH - (mGraphics.getImageHeight(imgBG[0]) + mGraphics.getImageHeight(imgBG[1]) + mGraphics.getImageHeight(imgBG[2])) /*+ 11*/;
            if (imgBG[0] != null)
                gsgreenField1Y = GameScr.gH - Image.getHeight(imgBG[0]);
            if (imgBG[1] != null)
                gsgreenField2Y = gsgreenField1Y - Image.getHeight(imgBG[1]);
            if (imgBG[2] != null)
                gshouseY = gsgreenField2Y - Image.getHeight(imgBG[2]);
            if (imgBG[3] != null)
                gsmountainY = gsgreenField2Y - Image.getHeight(imgBG[3]) /*- 10*/;
            // ================
            if (typeBg >= 2 && typeBg <= 12)
            {
                int tem = GameScr.gH - mGraphics.getImageHeight(imgBG[0]);
                bgLayer0y = tem;
                if (imgBG[1] != null)
                    tem = tem - mGraphics.getImageHeight(imgBG[1]) + deltaLayer1;
                bgLayer1y = tem;
                if (imgBG[3] != null)
                    tem = tem - mGraphics.getImageHeight(imgBG[3]) + deltaLayer3;
                gsmountainY = tem;
                //
                gsskyHeight = tem;

                if (imgBG[2] != null)
                    gshouseY = bgLayer1y - mGraphics.getImageHeight(imgBG[2]) + deltaLayer2;
                if (typeBg == 2)
                    gsskyHeight = GameCanvas.h;
            }

            if (typeBG == 3)
            {
                int tem = GameScr.gH - mGraphics.getImageHeight(imgBG[0]);
                bgLayer0y = tem;
                if (imgBG[1] != null)
                    tem = tem - mGraphics.getImageHeight(imgBG[1]) + 30 + deltaLayer1;
                bgLayer1y = tem;
                if (imgBG[3] != null)
                    tem = tem - mGraphics.getImageHeight(imgBG[3]) + deltaLayer3;
                gsmountainY = tem;
                //
                gsskyHeight = tem;

                if (imgBG[2] != null)
                    gshouseY = bgLayer1y - mGraphics.getImageHeight(imgBG[2]) + 20 + deltaLayer2;
                if (typeBg == 2)
                    gsskyHeight = GameCanvas.h;

            }

        }
        int bgDelta = 0;
        if (typeBg >= 2 && typeBg <= 12)
        {
            bgDelta = (2 * GameScr.gH / 3) - bgLayer1y;
        }
        else
        {
            bgDelta = (2 * GameScr.gH / 3) - gsgreenField2Y;
        }
        if (bgDelta < 0)
            bgDelta = 0;
        if (TileMap.mapID == 48 && TileMap.mapID == 51)
            bgLayer0y += bgDelta;
        if (typeBg >= 2 && typeBg <= 6)
            bgLayer1y += bgDelta;

        gsskyHeight += bgDelta;
        gsgreenField1Y += bgDelta;
        gsgreenField2Y += bgDelta;
        gshouseY += bgDelta;
        gsmountainY += bgDelta;
        sunX = 3 * GameScr.gW / 4;
        sunY = gsskyHeight / 3;
        cloudX = new int[2];
        cloudY = new int[2];
        cloudX[0] = GameScr.gW / 3;
        cloudY[0] = gsskyHeight / 2 - 8;
        cloudX[1] = 2 * GameScr.gW / 3;
        cloudY[1] = gsskyHeight / 2 + 8;
        if (typeBg == 2)
        {
            sunY = gsskyHeight / 5;
            cloudX = new int[5];
            cloudY = new int[5];
            cloudX[0] = GameScr.gW / 3;
            cloudY[0] = gsskyHeight / 3 - 35;
            cloudX[1] = 3 * GameScr.gW / 4;
            cloudY[1] = gsskyHeight / 3 + 12;
            cloudX[2] = GameScr.gW / 3 - 15;
            cloudY[2] = gsskyHeight / 3 + 12;
            cloudX[3] = GameScr.gW / +15;
            cloudY[3] = gsskyHeight / 2 + 12;
            cloudX[4] = 2 * GameScr.gW / 3 + 25;
            cloudY[4] = gsskyHeight / 3 + 12;
        }

        if (!lowGraphic)
        {
            if (typeBg == 8)
                bgLayer0y = bgLayer1y = GameScr.gH2 - 50;
            if (typeBg == 10)
            {
                if (imgBG[3] != null)
                    gsmountainY = gshouseY - mGraphics.getImageHeight(imgBG[3]);
            }
            if (typeBg == 11 || typeBg == 12)
            {
                gsmountainY = 0;

            }
        }
        return true;
    }


    public static bool isPoint(int x, int y, int w, int h)
    {
        if (px >= x && px <= x + w && py >= y && py <= y + h)
            return true;
        return false;
    }
    public static void paintBGGameScr(mGraphics g)
    {
        // Paint the Sky
        if (paintBG && !lowGraphic)
        {
            //			g.setColor(skyColor);
            //			g.fillRect(0, 0, GameScr.gW, gsskyHeight);
            // Paint the GreenField1
            if (typeBg >= 0 && typeBg <= 1)
            {
                if (GameCanvas.currentScreen == SelectCharScr.gI() || GameCanvas.currentScreen == CreatCharScr.gI())
                {
                    if (imgBG[3] != null)
                        for (int i = -((GameScr.cmx >> 4) % 64); i < GameScr.gW; i += 64)
                            g.drawImage(imgBG[3], i, gsmountainY, 0);
                    if (imgBG[2] != null)
                        for (int i = -((GameScr.cmx >> 3) % 192); i < GameScr.gW; i += Image.getWidth(imgBG[2])/*192*/)
                            g.drawImage(imgBG[2], i, gshouseY, 0);

                    // // Paint the GreenField2
                    if (imgBG[1] != null)
                    {

                        //						for (int i = -((GameScr.cmx >> 2) % 24); i < GameScr.gW; i += Image.getWidth(imgBG[1])/*24*/)
                        for (int i = GameCanvas.w / 2; i < GameCanvas.w + Image.getWidth(imgBG[1]); i += (Image.getWidth(imgBG[1])))
                        {

                            g.drawImage(imgBG[1], i, gsgreenField2Y + 100, mGraphics.BOTTOM | mGraphics.HCENTER);
                        }

                        for (int i = GameCanvas.w / 2; i > -Image.getWidth(imgBG[1]); i -= (Image.getWidth(imgBG[1])))
                        {
                            g.drawImage(imgBG[1], i, gsgreenField2Y + 100, mGraphics.BOTTOM | mGraphics.HCENTER);
                        }
                        //					for(int j = 0; j > 0; j -= (Image.getWidth(imgBG[1]))){
                        //						g.drawImage(imgBG[1], j, gsgreenField2Y, mGraphics.BOTTOM | mGraphics.HCENTER);
                        //					}
                        //						for(int i = GameCanvas.w/2; i > 0; i += (Image.getWidth(imgBG[1]) - 100))
                        //							g.drawImage(imgBG[1], i, gsgreenField2Y + 100, mGraphics.BOTTOM | mGraphics.HCENTER);
                        //						for(int k = GameCanvas.w/2; k < GameCanvas.w; k -= Image.getWidth(imgBG[1]) )
                        //							g.drawImage(imgBG[1], k, gsgreenField2Y - 120, mGraphics.HCENTER | mGraphics.HCENTER);

                    }
                    if (imgBG[0] != null)
                        for (int i = -((GameScr.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScr.gW; i += Image.getWidth(imgBG[0])/*24*/)
                            g.drawImage(imgBG[0], i, gsgreenField1Y - 150, 0);
                    // // Paint Mountain
                    //					if (imgBG[3] != null)
                    //						for (int i = -((GameScr.cmx >> 4) % 64); i < GameScr.gW; i += 64)
                    //							g.drawImage(imgBG[3], i, gsmountainY, 0);
                    // Paint sun and cloud
                    if (imgSun != null)
                        g.drawImage(imgSun, sunX, sunY, 3);
                    if (imgCloud != null)
                        for (int i = 0; i < 2; i++)
                            g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);
                    // Paint House

                }
                else if (GameCanvas.currentScreen == GameCanvas.loginScrr || GameCanvas.currentScreen == GameCanvas.languageScr)
                {
                    if (imgBG[3] != null)
                        for (int i = -((GameScr.cmx >> 4) % Image.getWidth(imgBG[3])); i < GameScr.gW; i += Image.getWidth(imgBG[3]))
                            g.drawImage(imgBG[3], i, gsmountainY, 0);
                    //					if (imgBG[0] != null)
                    //						for (int i = -((GameScr.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScr.gW; i += Image.getWidth(imgBG[0]))
                    //							g.drawImage(imgBG[0], i, gsgreenField1Y - 100, 0);
                    // // Paint the GreenField2
                    if (imgBG[2] != null)
                        for (int i = -((GameScr.cmx >> 3) % Image.getWidth(imgBG[2])); i < GameScr.gW; i += Image.getWidth(imgBG[2]))
                            g.drawImage(imgBG[2], Image.getWidth(imgBG[1]) - 50, gshouseY, 0);
                    if (imgBG[1] != null)
                        for (int i = -((GameScr.cmx >> 2) % Image.getWidth(imgBG[1])); i < GameScr.gW; i += Image.getWidth(imgBG[1]))
                            g.drawImage(imgBG[1], 0, gshouseY + 3, 0);
                    // // Paint Mountain

                    // Paint sun and cloud
                    if (imgSun != null)
                        g.drawImage(imgSun, sunX, sunY, 3);
                    if (imgCloud != null)
                        for (int i = 0; i < 2; i++)
                            g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);
                    // Paint House

                    if (imgBG[0] != null)
                        for (int i = -((GameScr.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScr.gW; i += Image.getWidth(imgBG[0]))
                            g.drawImage(imgBG[0], i, gsgreenField1Y - 100, 0);
                }


                // ==========================
            }
            else if (typeBg >= 2 && typeBg <= 6)
            {
                //				System.out.println("PAINT BG GAME SCR");
                //				if (imgSun != null) {
                //					// Paint sun and cloud
                //					g.drawImage(imgSun, sunX, sunY, 3);
                //				}
                //				if (imgCloud != null)
                //					for (int i = 0; i < cloudX.length; i++)
                //						g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);

                //				if(typeBg!=2){
                //					 Paint Mountain
                //					if (imgBG[3] != null)
                //						for (int i = -((GameScr.cmx >> bgSpeed[3]) % imgBGWidth[3]); i < GameScr.gW; i += imgBGWidth[3])
                //							g.drawImage(imgBG[3], i, gsmountainY, 0);
                //					// Paint House
                //					if (imgBG[2] != null){
                //						for (int i = -((GameScr.cmx >> bgSpeed[2]) % imgBGWidth[2]); i < GameScr.gW; i += imgBGWidth[2])
                //							g.drawImage(imgBG[2], i, gshouseY + 40, 0);
                //					}
                // Paint the GreenField2
                if (imgBG[1] != null)
                {
                    for (int i = -((GameScr.cmx >> bgSpeed[1]) % imgBGWidth[1]); i < GameScr.gW; i += imgBGWidth[1])
                        g.drawImage(imgBG[1], i, GameCanvas.h - imgBG[1].getHeight(), 0);
                }
                //					// Paint the GreenField1
                //					if (imgBG[0] != null) {
                //						for (int i = -((GameScr.cmx >> bgSpeed[0]) % imgBGWidth[0]); i < GameScr.gW; i += imgBGWidth[0])
                //							g.drawImage(imgBG[0], i, bgLayer0y, 0);
                //					}
                //				}

            }
            else if (typeBg >= 7 && typeBg <= 12)
            {
                g.setColor(skyColor);
                g.fillRect(0, 0, GameScr.gW, GameScr.gH);
                if (typeBg != 8 && imgBG[3] != null)
                {

                    for (int i = -((GameScr.cmx >> bgSpeed[3]) % imgBGWidth[3]); i < GameScr.gW; i += imgBGWidth[3])
                        if (typeBg == 11 || typeBg == 12)
                        {
                            g.drawImage(imgBG[3], i, GameScr.gH - mGraphics.getImageHeight(imgBG[3]), 0);

                        }
                        else
                            g.drawImage(imgBG[3], i, gsmountainY, 0);

                }

                if (typeBg != 8 && typeBg != 11 && typeBg != 12 && imgBG[2] != null)
                {

                    if (TileMap.mapID == 45)
                        g.drawImage(imgBG[2], GameScr.gW, gshouseY, 0);
                    else
                        for (int i = -((GameScr.cmx >> bgSpeed[2]) % imgBGWidth[2]); i < GameScr.gW; i += imgBGWidth[2])
                            g.drawImage(imgBG[2], i, gshouseY, 0);
                }
                if (typeBg != 11 && typeBg != 12 && imgBG[1] != null)
                {

                    if (TileMap.mapID != 52)
                        for (int i = -((GameScr.cmx >> bgSpeed[1]) % imgBGWidth[1]); i < GameScr.gW; i += imgBGWidth[1])
                            g.drawImage(imgBG[1], i, bgLayer1y, 0);

                }
                if (TileMap.mapID == 45 || TileMap.mapID == 55)
                {
                    g.setColor(0x110000);
                    g.fillRect(0, bgLayer0y + 20, GameScr.gW, GameScr.gH);
                }

                if (imgBG[0] != null)
                {

                    for (int i = -((GameScr.cmx >> bgSpeed[0]) % imgBGWidth[0]); i < GameScr.gW; i += imgBGWidth[0])
                        g.drawImage(imgBG[0], i, bgLayer0y, 0);

                }

                if (imgCloud != null)
                {

                    for (int i = 0; i < 2; i++)
                        g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);

                }
            }
            // ==============================
        }
        else
        {
            g.setColor(skyColor);
            g.fillRect(0, 0, GameScr.gW, GameScr.gH);
        }
    }
    public static void startDlgTime(string info, Command cmdYes, int time)
    {
        cmdYes.caption = mResources.OK;
        msgdlg.setInfo(info, null, cmdYes, null);
        msgdlg.setTimeLive(time);
        msgdlg.show(); 
        GameCanvas.currentDialog = GameCanvas.msgdlg;
    }
    public static void startWaitDlgWithoutCancel()
    {
        msgdlg.timeShow = 500;
        msgdlg.setInfo(mResources.PLEASEWAIT, null, null, null);
        currentDialog = msgdlg;
        msgdlg.isWait = true;
    }
    public bool startWaterSplash(int x, int y)
    {
        if (lowGraphic)
            return false;
        int i = wsState[0] == -1 ? 0 : 1;
        if (wsState[i] != -1)
            return false;
        wsState[i] = 0;
        wsX[i] = x;
        wsY[i] = y;
        return true;
    }
    public static void StartDglThongBao(string text)
    {
        isPaintThongBao = true;
        indexThongBao = 0;
        thongbao = text;
    }
    public static void startCommandDlg(string info, Command cmdYes, Command cmdNo)
    {
        msgdlg.setInfo(info, cmdYes, null, cmdNo);
        msgdlg.isPaintCham = false;
        msgdlg.show();
        GameCanvas.currentDialog = GameCanvas.msgdlg;
    }
    public static void cleanImgBg()
    {
        for (int i = 0; i < keybgLoginSelect.size(); i++)
        {
            string key = (string)keybgLoginSelect.elementAt(i);
            if (key != null)
            {
                mBitmap imgg = (mBitmap)Image.listImgLoad.get(key);
                if (imgg != null)
                {
                    imgg.cleanImg();
                }
            }
        }
        keybgLoginSelect.removeAllElements();
    }
}
