public class Menu2 {


    public bool isShowMenu;
    public mVector menuItems;
    public int menuSelectedItem, SelectFocus;
    public int menuX, menuY, menuW, menuH, menuTemY, hPlus;
    public static int cmtoX, cmx, cmdy, cmvy, cmxLim, xc;
    int pos, sizeMenu;
    string nameMenu = "";
    string[] mStrTalk;
    public RunText runText;
    public static bool isHelp = false, isGiaotiep = false, isPaint = true,
            isLoadData = true;
    public static sbyte isNPCMenu;
    // ///////
    public const sbyte NORMAL = 0;
    public const sbyte NPC_MENU = 1;
    public const sbyte QUICK_MENU = 2;
    mVector vecFocus;
    //	public static MainObject objSelect = null;
    // Dynamic
    int IdMenu, IdNpc;
    public static int IdNPCLast;
    byte typeO;
    //
    byte timeShow = 0;
    int hShow, maxShow;

    bool disableClose;

    public void startAt(mVector menuItems, int pos, string name,
            bool isFocus, mVector mfocus)
    {
        //..Cout.println("MenuStart Attt ----> " + menuItems.size());
        isLoadData = false;
        waitToPerform = 0;
        //		runText = null;
        //		right = null;
        isNPCMenu = NORMAL;
        isGiaotiep = isFocus;
        this.vecFocus = mfocus;
        SelectFocus = 0;
        //		if (isGiaotiep && (vecFocus == null || vecFocus.size() == 0))
        //			return;
        nameMenu = name;
        isHelp = false;
        disableClose = false;
        menuSelectedItem = 0;
        // isDyna = false;
        this.pos = pos;
        // ChatPopup.currentMultilineChatPopup = null;
        // ChatPopup.vEffect2.removeAllElements();
        // ChatPopup.vEffect2Outside.removeAllElements();
        // InfoDlg.hide();
        if (menuItems == null || menuItems.size() == 0)
            return;
        this.menuItems = menuItems;

        isShowMenu = true;

        if (pos == -1)
        {
            this.menuItems.addElement(new Command("Close", 1, this));
            hPlus = 0;
            menuW = 60;
            menuH = 60;
            for (int i = 0; i < menuItems.size(); i++)
            {
                Command c = (Command)menuItems.elementAt(i);
				int w = mFont.tahoma_7_white.getWidth(c.caption);
                if (w > menuW - 8)
                {
                    c.subCaption = mFont.tahoma_7b_yellow.splitFontArray(
                            c.caption, menuW - 8);
                }
            }
            ww = menuItems.size() * menuW - 1;
            if (ww > GameCanvas.w - 2)
                ww = GameCanvas.w - 2;
            menuX = GameCanvas.hw - ww / 2;
            if (menuX < 1)
                menuX = 1;
            menuY = GameCanvas.h - menuH - (GameCanvas.hCommand + 1);
            if (GameCanvas.isTouch)
            {
                menuY -= 3;
            }
            menuY += 27;
            menuTemY = menuY;
            cmxLim = this.menuItems.size() * menuW - GameCanvas.w;
            if (cmxLim < 0)
                cmxLim = 0;
            cmtoX = 0;
            cmx = 0;
            xc = 50;
        }
        else
        {
            //			if (isGiaotiep) {
            //				objSelect = (MainObject) vecFocus.elementAt(0);
            //			}
            menuW = GameCanvas.hCommand;
            if (GameCanvas.isTouch)
                menuW = 32;
            sizeMenu = (GameCanvas.h / 4 * 3) / menuW - (isFocus ? 2 : 1);
            if (GameCanvas.isTouch)
                sizeMenu++;
            ww = GameCanvas.w / 3;
            if (ww < mFont.tahoma_7b_white.getWidth(name) + 30)
                ww = mFont.tahoma_7b_white.getWidth(name) + 30;
            hPlus = GameCanvas.hCommand;
            if (isFocus)
                hPlus += menuW;
            int maxW = 120;
            int sizePlus = 30;
            if (isFocus)
                sizePlus = 50;
            for (int i = 0; i < menuItems.size(); i++)
            {
                Command cmd = (Command)menuItems.elementAt(i);
				int t = mFont.tahoma_7_white.getWidth(cmd.caption) + sizePlus;
                if (t > maxW)
                {
                    maxW = t;
                }
            }
            if (ww < maxW)
                ww = maxW;
            if (ww > GameCanvas.w)
                ww = GameCanvas.w;
            cmtoX = 0;
            cmx = 0;
            Command cmdd = null;
            if (GameCanvas.isTouch)
            {
                cmdd = new Command("Close", 1, this);
            }
            else
            {
                this.menuItems.addElement(new Command("Close", 1, this));
            }
            if (menuItems.size() > sizeMenu)
            {
                menuH = sizeMenu * menuW + 8;
                cmxLim = (menuItems.size() - sizeMenu) * menuW;
            }
            else
            {
                menuH = menuItems.size() * menuW + 8;
                cmxLim = 0;
            }
            setPos();
            menuTemY = menuY;
            //			if (cmd != null) {
            //				cmd.setPos(menuX + w - 11, menuY - hPlus + GameCanvas.hCommand
            //						/ 2 - 2, PaintInfoGameScreen.fraCloseMenu, "");
            //				right = cmd;
            //			}
            // if (!GameCanvas.isTouch) {
            // left = new iCommand(T.select, 0,this);
            // right = new iCommand(T.close, 1,this);
            Cout.println("Menu -----> X:: " + menuX + " ::: " + menuY);
            // }
        }

        if (GameCanvas.isTouch)
            menuSelectedItem = -1;
        isLoadData = true;
        resetBegin();
    }

    public void setinfoDynamic(mVector menulist, int pos, int idmenu,
            int idnpc, string name)
    {
        isLoadData = false;
        waitToPerform = 0;
        //		right = null;
        //		runText = null;
        isGiaotiep = false;
        vecFocus = null;
        if (menulist == null)
            return;
        nameMenu = name;
        isHelp = false;
        isNPCMenu = NORMAL;
        menuSelectedItem = 0;
        this.IdMenu = idmenu;
        this.IdNpc = idnpc;
        this.pos = pos;
        disableClose = false;
        isShowMenu = true;
        this.menuItems = new mVector();
        menuW = GameCanvas.hCommand;
        if (GameCanvas.isTouch)
            menuW = 32;
        sizeMenu = (GameCanvas.h / 4 * 3) / menuW - 1;
        if (GameCanvas.isTouch)
            sizeMenu++;
        ww = GameCanvas.w / 3;
        hPlus = menuW;
        int maxW = 120;
        if (maxW < mFont.tahoma_7b_white.getWidth(name) + 30)
            maxW = mFont.tahoma_7b_white.getWidth(name) + 30;
        for (int i = 0; i < menulist.size(); i++)
        {
            Command cmd = (Command)menulist.elementAt(i);
            Command cmdnew = new Command(cmd.caption, 2, this);
            int t = mFont.tahoma_7b_white.getWidth(cmd.caption) + 20;
            if (t > maxW)
            {
                maxW = t;
            }
            menuItems.addElement(cmdnew);
        }
        Command cmdd = null;
        if (GameCanvas.isTouch)
        {
            cmdd = new Command("Close", 1, this);
        }
        else
        {
            this.menuItems.addElement(new Command("Close", 1, this));
        }
        ww = maxW;
        if (ww > GameCanvas.w)
            ww = GameCanvas.w;
        if (menuItems.size() > sizeMenu)
        {
            menuH = sizeMenu * menuW + 8;
            cmxLim = (menuItems.size() - sizeMenu) * menuW;
        }
        else
        {
            menuH = menuItems.size() * menuW + 8;
            cmxLim = 0;
        }
        cmtoX = 0;
        cmx = 0;
        setPos();
        menuTemY = menuY;
        //		if (cmd != null) {
        //			cmd.setPos(menuX + w - 11, menuY - hPlus + GameCanvas.hCommand / 2
        //					- 2, PaintInfoGameScreen.fraCloseMenu, "");
        //			right = cmd;
        //		}
        if (GameCanvas.isTouch)
            menuSelectedItem = -1;
        isLoadData = true;
        // if (!GameCanvas.isTouch) {
        // left = new iCommand(T.select, 2,this);
        // right = new iCommand(T.close, 1,this);
        // }
        resetBegin();
    }

    int archorRunText;

    public void startAt_NPC(mVector menuItems, string name, int idNPC,
            byte typeO, bool isQuest, int archor)
    {
        isLoadData = false;
        waitToPerform = 0;
        //		right = null;
        isNPCMenu = NPC_MENU;
        SelectFocus = 0;
        nameMenu = name;
        isHelp = false;
        disableClose = false;
        isGiaotiep = false;
        this.IdNpc = idNPC;
        IdNPCLast = idNPC;
        this.typeO = typeO;
        this.archorRunText = archor;
        menuSelectedItem = 0;
        if (menuItems == null || menuItems.size() == 0)
        {
            this.menuItems = new mVector();
        }
        else
            this.menuItems = menuItems;
        isShowMenu = true;
        menuW = GameCanvas.hCommand;
        if (GameCanvas.isTouch)
            menuW = 32;
        sizeMenu = 0;
        ww = GameCanvas.w - 10;
        if (ww > 300)
            ww = 300;
        //		mStrTalk = mFont.tahoma_7_black.splitFontArray(name, w - 20);
        hPlus = GameCanvas.hCommand;
        cmtoX = 0;
        cmx = 0;
        int size = mStrTalk.Length;
        if (!isQuest)
            this.menuItems.addElement(new Command("Close", 1, this));
        else if (size == 1)
        {
            size = 2;
        }
        //		menuH = (size + 2) * GameCanvas.hText
        //				+ (((this.menuItems.size() - 1) / 2) + 1)
        //				* (Command.hButtonCmd + 5) + 5;
        cmxLim = 0;
        menuX = GameCanvas.hw - ww / 2;
        menuY = GameCanvas.h - menuH - 10;
        menuTemY = menuY;
        //		runText = new RunWord();
        //		runText.startDialogChain(name, 0, menuX + 10, menuY + 10
        //				+ GameCanvas.hText, w - 20, mFont.tahoma_7_white);
        setPosNPC();
        // if (cmd != null) {
        // cmd.setPos(menuX + w - 11, menuY - hPlus + GameCanvas.hCommand / 2
        // - 2, PaintInfoGameScreen.fraCloseMenu, "");
        // right = cmd;
        // }
        if (GameCanvas.isTouch)
            menuSelectedItem = -1;
        isLoadData = true;
        resetBegin();
    }

    bool isPosPoint = false;

    public void setAt_Quick()
    {
        //		isPosPoint = PaintInfoGameScreen.isLevelPoint;
        timeShow = 1;
        isLoadData = false;
        waitToPerform = 0;
        //		right = null;
        isNPCMenu = QUICK_MENU;
        SelectFocus = 0;
        nameMenu = "";
        isHelp = false;
        disableClose = false;
        isGiaotiep = false;
        menuSelectedItem = 0;
        ww = GameCanvas.w - 40;
        isShowMenu = true;
        menuW = 40;
        cmtoX = 0;
        cmx = 0;
        hShow = 0;
        menuH = 40;
        maxShow = (byte)(ww / menuH);
        menuItems = new mVector();
        iCommand cmdAuto = new iCommand(T.auto, 3, 2);
        iCommand cmdDSat = new iCommand(T.dosat, 4, 4);
        iCommand cmdPk = new iCommand(T.setPk, 5, 3);
        iCommand cmdEvent = new iCommand(T.eventt, 6, 0);
        iCommand cmdFriend = new iCommand(T.listFriend, 7, 1);
        iCommand cmdTouch = new iCommand(T.touch + "/" + T.keypad, 8, 5);
        menuItems.addElement(cmdEvent);
        menuItems.addElement(cmdFriend);
        //		if (GameScr.Char.my.myClan != null) {
        //			Command cmdClan = new Command(T.clan, 9,6, this);
        //			menuItems.addElement(cmdClan);
        //		}
        //		if (Char.party != null) {
        //			Command cmdParty = new Command(T.party, 10,7, this);
        //			menuItems.addElement(cmdParty);
        //		}
        menuItems.addElement(cmdAuto);
        menuItems.addElement(cmdPk);
        menuItems.addElement(cmdDSat);
        menuItems.addElement(cmdTouch);
        if (maxShow > menuItems.size())
            maxShow = menuItems.size();
        ww = maxShow * menuH;
        cmxLim = 0;
        if (!isPosPoint)
        {
            menuX = GameCanvas.hw - ww / 2;
        }
        else
            menuX = 20;
        menuY = GameCanvas.h - 40;
        menuTemY = menuY;
        if (GameCanvas.isTouch)
            menuSelectedItem = -1;
        isLoadData = true;

        for (int i = 0; i < menuItems.size(); i++)
        {
            Command cmd = (Command)menuItems.elementAt(i);
            //			cmd.setPos_ShowName(menuX + menuH / 2 + i * menuH, menuY + menuH / 2,
            //					PaintInfoGameScreen.mfraIconQuick[cmd.subIndex], cmd.caption,0,-32);
        }
        cmxLim = (menuItems.size() - maxShow) * menuH;
        menuSelectedItem = -1;
        resetBegin();

    }

    public void resetBegin()
    {
        for (int i = 0; i < pointerDownLastX.Length; i++)
        {
            pointerDownLastX[i] = 0;
        }
        pointerDownTime = 0;
        pointerDownFirstX = 0;
        pointerIsDowning = false;
        isDownWhenRunning = false;
        waitToPerform = 0;
        cmRun = 0;
    }

    int xBegin, w2cmd;

    public void setPosNPC()
    {
        int t = menuItems.size();
        if (t == 1)
        {
            xBegin = menuX + ww / 2;
            w2cmd = 0;
        }
        else if (t == 2)
        {
            w2cmd = 20;
            //			xBegin = menuX + w / 2 - w2cmd / 2 - Command.wButtonCmd / 2;
        }
        else
        {
            w2cmd = 20;
            //			xBegin = menuX + w / 2 - w2cmd / 2 - Command.wButtonCmd / 2;
        }
        //		for (int i = 0; i < t; i++) {
        //			Command cmd = (Command) menuItems.elementAt(i);
        //			if (t == 3 && i == 2) {
        //				cmd.setPos(menuX + w / 2, menuY + menuH - Command.hButtonCmd
        //						- ((t - 1) / 2) * (iCommand.hButtonCmd + 5) + 7
        //						+ (i / 2) * (iCommand.hButtonCmd + 5), null,
        //						cmd.caption);
        //			} else
        //				cmd.setPos(xBegin + (i % 2) * (iCommand.wButtonCmd + w2cmd),
        //						menuY + menuH - iCommand.hButtonCmd - ((t - 1) / 2)
        //								* (iCommand.hButtonCmd + 5) + 7 + (i / 2)
        //								* (iCommand.hButtonCmd + 5), null, cmd.caption);
        //			if (i == 0)
        //				cmd.isSelect = true;
        //		}
    }

    public void setPos()
    {
        switch (pos)
        {
            case 0:
                menuX = 2;
                menuY = GameCanvas.h - GameCanvas.hCommand - menuH - 2;
                if (GameCanvas.isTouch)
                {
                    menuY += GameCanvas.hCommand;
                }
                break;
            case 1:
                menuX = GameCanvas.w - ww - 2;
                menuY = GameCanvas.h - GameCanvas.hCommand - menuH - 2;
                if (GameCanvas.isTouch)
                {
                    menuY += GameCanvas.hCommand;
                }
                break;
            case 2:
            case 4:
                menuX = GameCanvas.hw - ww / 2;
                menuY = GameCanvas.h / 2 - menuH / 2 - 2 + menuW / 2 + 6;
                break;
            case 3:// menu cua man hinh ingame touch
                menuX = 2;
                menuY = 2;
                break;
        }
    }

    // public void commandMenu(int index, int subIndex) {
    // switch (index) {
    // case 0:
    // isShowMenu = false;
    // iCommand cmd = (iCommand) menuItems.elementAt(menuSelectedItem);
    // perform(cmd);
    // break;
    // case 2:
    // GlobalService.gI().Dynamic_Menu((short) IdNpc, (byte) IdMenu,
    // (byte) menuSelectedItem);
    // isShowMenu = false;
    // GameCanvas.isPointerSelect = false;
    // break;
    //		
    // }
    // super.commandTab(index, subIndex);
    // }

    public void commandPointer(int index, int subIndex)
    {
        switch (index)
        {
            case 0:
                isShowMenu = false;
                Command cmd = (Command)menuItems.elementAt(menuSelectedItem);
                //			perform(cmd);
                break;
            case 2:
                //			GlobalService.gI().Dynamic_Menu((short) IdNpc, (byte) IdMenu,
                //					(byte) menuSelectedItem);
                //			isShowMenu = false;
                //			GameCanvas.isPointerSelect = false;
                break;
            case 3:
            //			isShowMenu = false;
            //			GameScreen.gI().doMenuAuto();
            //			break;
            case 4:
                //			isShowMenu = false;
                //			GameScreen.gI().cmdSetDoSat.perform();
                break;
            case 5:
                isShowMenu = false;
                //			GameScreen.gI().cmdSetPk.perform();
                break;
            case 6:
                isShowMenu = false;
                //			TabConfig.cmdEvent.perform();
                break;
            case 7:
                //			GameScreen.gI().cmdListFriend.perform();
                break;
            case 8:
                //			TabConfig.cmdKeypad.perform();
                break;
            case 9:
                //			TabConfig.cmdShowClan.perform();
                break;
            case 10:
                //				GameScreen.gI().cmdParty.perform();
                break;
            case 1:
                doCloseMenu();
                break;
        }
        //		super.commandPointer(index, subIndex);
    }

    int pa = 0;
    bool trans = false;

    public static int ww;
    private int pointerDownTime;
    private int pointerDownFirstX;
    private int[] pointerDownLastX = new int[3];
    private bool pointerIsDowning, isDownWhenRunning;// , wantUpdateList;
    private int waitToPerform, cmRun;

    public void updateMenuKey()
    {
        if (!isPaint)
            return;
        if (!isShowMenu)
            return;
        bool changeFocus = false;
        if (isGiaotiep)
        {
            if (GameCanvas.keyPressed[2])
            {
                changeFocus = true;
                menuSelectedItem--;
                if (menuSelectedItem < 0)
                    menuSelectedItem = menuItems.size() - 1;
                GameCanvas.clearKeyPressed();

            }
            else if (GameCanvas.keyPressed[8])
            {
                changeFocus = true;
                menuSelectedItem++;
                if (menuSelectedItem > menuItems.size() - 1)
                    menuSelectedItem = 0;

                GameCanvas.clearKeyPressed();
            }
            int t = SelectFocus;
            if (GameCanvas.keyPressed[4])
            {
                SelectFocus--;
                GameCanvas.clearKeyPressed();
            }
            if (GameCanvas.keyPressed[6])
            {
                SelectFocus++;
                GameCanvas.clearKeyPressed();
            }
            //			SelectFocus = resetSelect(SelectFocus, vecFocus.size() - 1, true);
            //			if (SelectFocus != t) {
            //				objSelect = (MainObject) vecFocus.elementAt(SelectFocus);
            //			}
        }
        else
        {
            if (isNPCMenu == NPC_MENU)
            {
                int t = menuSelectedItem;
                if (GameCanvas.keyHold[4] || GameCanvas.keyHold[2])
                {
                    menuSelectedItem--;
                    GameCanvas.clearKeyHold();
                    GameCanvas.clearKeyHold();
                }
                else if (GameCanvas.keyHold[6] || GameCanvas.keyHold[8])
                {
                    menuSelectedItem++;
                    GameCanvas.clearKeyHold();
                    GameCanvas.clearKeyHold();
                }
                //				menuSelectedItem = resetSelect(menuSelectedItem, menuItems
                //						.size() - 1, false);
                if (t != menuSelectedItem)
                {
                    for (int i = 0; i < menuItems.size(); i++)
                    {
                        Command cmd = (Command)menuItems.elementAt(i);
                        //						if (i == menuSelectedItem)
                        //							cmd.isSelect = true;
                        //						else
                        //							cmd.isSelect = false;
                    }
                }
                if (GameCanvas.keyHold[5])
                {
                    GameCanvas.clearKeyHold();
                    //					if (menuSelectedItem < menuItems.size()
                    //							&& menuSelectedItem >= 0) {
                    //						((Command) menuItems.elementAt(menuSelectedItem))
                    //								.perform();
                    //					}
                }
            }
            else if (isNPCMenu == NORMAL)
            {
                if (GameCanvas.keyPressed[2] || GameCanvas.keyPressed[4])
                {
                    changeFocus = true;
                    menuSelectedItem--;
                    if (menuSelectedItem < 0)
                        menuSelectedItem = menuItems.size() - 1;
                    GameCanvas.clearKeyPressed();
                    GameCanvas.clearKeyPressed();
                }
                else if (GameCanvas.keyPressed[8]
                      || GameCanvas.keyPressed[6])
                {
                    changeFocus = true;
                    menuSelectedItem++;
                    if (menuSelectedItem > menuItems.size() - 1)
                        menuSelectedItem = 0;
                    GameCanvas.clearKeyPressed();
                    GameCanvas.clearKeyPressed();
                }
            }
        }
        if (changeFocus)
        {
            if (pos == -1)
            {
                cmtoX = menuSelectedItem * menuW + menuW - GameCanvas.w / 2;
            }
            else
            {
                cmtoX = (menuSelectedItem + 1) * menuW - menuH / 2;
            }
            if (cmtoX > cmxLim)
                cmtoX = cmxLim;
            if (cmtoX < 0)
                cmtoX = 0;
            if (menuSelectedItem == menuItems.size() - 1
                    || menuSelectedItem == 0)
            {
                cmx = cmtoX;
            }
        }
        if (isNPCMenu == NORMAL)
        {
            if (pos == -1)
            {
                updatePos_LEFT_RIGHT();
            }
            else
            {
                update_Pos_UP_DOWN();
            }
            //			if (GameCanvas.isPointerSelect) {
            //				if (!GameCanvas.isPoint(menuX - 5, menuTemY - 5 - hPlus,
            //						w + 10, menuH + 10 + hPlus)) {
            //					doCloseMenu();
            //				}
            //			}
        }
        else if (isNPCMenu == QUICK_MENU)
        {
            updatePos_LEFT_RIGHT();
            //			if (GameCanvas.isPointerSelect) {
            //				if (!GameCanvas.isPoint(menuX - 5, menuY - 5, w + 10,
            //						menuH + 10)) {
            //					timeShow = -1;
            //				}
            //			}
        }
        //		super.updatekey();
    }

    public void updatePos_LEFT_RIGHT()
    {
        if (GameCanvas.isPointerDown)
        {
            if (!pointerIsDowning
                    && GameCanvas.isPointer(menuX, menuY, ww, menuH))
            {
                for (int i = 0; i < pointerDownLastX.Length; i++)
                    pointerDownLastX[0] = GameCanvas.px;
                pointerDownFirstX = GameCanvas.px;
                pointerIsDowning = true;
                isDownWhenRunning = cmRun != 0;
                cmRun = 0;
            }
            else if (pointerIsDowning)
            {
                pointerDownTime++;
                if (pointerDownTime > 5 && pointerDownFirstX == GameCanvas.px
                        && !isDownWhenRunning)
                {
                    pointerDownFirstX = -1000;
                    menuSelectedItem = (cmtoX + GameCanvas.px - menuX) / menuW;
                }
                int dx = GameCanvas.px - pointerDownLastX[0];
                if (dx != 0 && menuSelectedItem != -1)
                {
                    menuSelectedItem = -1;
                }
                for (int i = pointerDownLastX.Length - 1; i > 0; i--)
                    pointerDownLastX[i] = pointerDownLastX[i - 1];
                pointerDownLastX[0] = GameCanvas.px;

                cmtoX -= dx;
                if (cmtoX < 0)
                    cmtoX = 0;
                if (cmtoX > cmxLim)
                    cmtoX = cmxLim;
                if (cmx < 0 || cmx > cmxLim)
                    dx /= 2;
                cmx -= dx;
                // if (cmx < -(GameCanvas.h / 3))
                // wantUpdateList = true;
                // else
                // wantUpdateList = false;
            }

        }
        if (GameCanvas.isPointerClick && pointerIsDowning)
        {
            int dx = GameCanvas.px - pointerDownLastX[0];
            GameCanvas.isPointerClick = false;
            if (Res.abs(dx) < 20
                    && Res.abs(GameCanvas.px - pointerDownFirstX) < 20
                    && !isDownWhenRunning)
            {
                cmRun = 0;
                cmtoX = cmx;
                pointerDownFirstX = -1000;
                menuSelectedItem = (cmtoX + GameCanvas.px - menuX) / menuW;
                pointerDownTime = 0;
                waitToPerform = 1;
            }
            else if (menuSelectedItem != -1 && pointerDownTime > 5)
            {
                pointerDownTime = 0;
                waitToPerform = 1;
            }
            else if (menuSelectedItem == -1 && !isDownWhenRunning)
            {
                if (cmx < 0)
                    cmtoX = 0;
                else if (cmx > cmxLim)
                    cmtoX = cmxLim;
                else
                {
                    int s = ((GameCanvas.px - pointerDownLastX[0])
                            + (pointerDownLastX[0] - pointerDownLastX[1]) + (pointerDownLastX[1] - pointerDownLastX[2]));
                    if (s > 10)
                        s = 10;
                    else if (s < -10)
                        s = -10;
                    else
                        s = 0;
                    cmRun = -s * 100;
                }
            }
            pointerIsDowning = false;
            pointerDownTime = 0;
            GameCanvas.isPointerClick = false;
        }
        if (GameCanvas.isPointerJustRelease && pointerIsDowning)
        {
            pointerIsDowning = false;
        }
    }

    private void update_Pos_UP_DOWN()
    {
        if (GameCanvas.keyPressed[5])
        {
            GameCanvas.clearKeyHold();
            GameCanvas.clearKeyPressed();
            doCloseMenu();
            Command c = ((Command)menuItems.elementAt(menuSelectedItem));
            //			perform(c);
        }
        else if (GameCanvas.keyPressed[12])
        {
            GameCanvas.clearKeyHold();
            GameCanvas.clearKeyPressed();
            doCloseMenu();
            Command c = ((Command)menuItems.elementAt(menuSelectedItem));
            //			perform(c);
        }
        // if (!disableClose && (GameCanvas.keyMyPressed[13])) {
        // doCloseMenu();
        // }
        //		if (GameCanvas.isPointerSelect) {
        //			if (isGiaotiep) {
        //				int t = SelectFocus;
        //				if (GameCanvas.isPoint(menuX + 13 - 14, menuTemY - hPlus
        //						+ GameCanvas.hCommand + menuW / 2 - 14, 28, 28)) {
        //					SelectFocus--;
        //					GameCanvas.isPointerSelect = false;
        //				}
        //				if (GameCanvas.isPoint(menuX + w - 13 - 14, menuTemY - hPlus
        //						+ GameCanvas.hCommand + menuW / 2 - 14, 28, 28)) {
        //					SelectFocus++;
        //					GameCanvas.isPointerSelect = false;
        //				}
        //				SelectFocus = resetSelect(SelectFocus, vecFocus.size() - 1,
        //						true);
        //				if (SelectFocus != t) {
        //					objSelect = (MainObject) vecFocus.elementAt(SelectFocus);
        //				}
        //			}
        //		}
        if (GameCanvas.isPointerDown)
        {
            if (!pointerIsDowning
                    && GameCanvas.isPointer(menuX, menuY, ww, menuH))
            {
                for (int i = 0; i < pointerDownLastX.Length; i++)
                    pointerDownLastX[0] = GameCanvas.py;
                pointerDownFirstX = GameCanvas.py;
                pointerIsDowning = true;
                isDownWhenRunning = cmRun != 0;
                cmRun = 0;
            }
            else if (pointerIsDowning)
            {
                pointerDownTime++;
                if (pointerDownTime > 5 && pointerDownFirstX == GameCanvas.py
                        && !isDownWhenRunning)
                {
                    pointerDownFirstX = -1000;
                    menuSelectedItem = (cmtoX + GameCanvas.py - menuY) / menuW;
                }
                int dx = GameCanvas.py - pointerDownLastX[0];
                if (dx != 0 && menuSelectedItem != -1)
                {
                    menuSelectedItem = -1;
                }
                for (int i = pointerDownLastX.Length - 1; i > 0; i--)
                    pointerDownLastX[i] = pointerDownLastX[i - 1];
                pointerDownLastX[0] = GameCanvas.py;

                cmtoX -= dx;
                if (cmtoX < 0)
                    cmtoX = 0;
                if (cmtoX > cmxLim)
                    cmtoX = cmxLim;
                if (cmx < 0 || cmx > cmxLim)
                    dx /= 2;
                cmx -= dx;
                // if (cmx < -(GameCanvas.h / 3))
                // wantUpdateList = true;
                // else
                // wantUpdateList = false;
            }

        }
        //		mSystem.outz("Click="+GameCanvas.isPointerClick+" Downing="+pointerIsDowning+" Release"+GameCanvas.isPointerRelease);
        if (GameCanvas.isPointerClick && pointerIsDowning)
        {
            //			mSystem.outz("777777777777777777777777");
            int dx = GameCanvas.py - pointerDownLastX[0];
            GameCanvas.isPointerClick = false;
            //			if (Res.abs(dx) < 20
            //					&& Res.abs(GameCanvas.py - pointerDownFirstX) < 20
            //					&& !isDownWhenRunning&&GameCanvas.isPointerSelect) {
            //				cmRun = 0;
            //				cmtoX = cmx;
            //				pointerDownFirstX = -1000;
            //				menuSelectedItem = (cmtoX + GameCanvas.py - menuY) / menuW;
            //				pointerDownTime = 0;
            //				waitToPerform = 1;
            //			} else 
            if (menuSelectedItem != -1 && pointerDownTime > 5)
            {
                pointerDownTime = 0;
                waitToPerform = 1;
            }
            else if (menuSelectedItem == -1 && !isDownWhenRunning)
            {
                if (cmx < 0)
                    cmtoX = 0;
                else if (cmx > cmxLim)
                    cmtoX = cmxLim;
                else
                {
                    int s = ((GameCanvas.py - pointerDownLastX[0])
                            + (pointerDownLastX[0] - pointerDownLastX[1]) + (pointerDownLastX[1] - pointerDownLastX[2]));
                    if (s > 10)
                        s = 10;
                    else if (s < -10)
                        s = -10;
                    else
                        s = 0;
                    cmRun = -s * 100;
                }
            }
            pointerIsDowning = false;
            pointerDownTime = 0;
            GameCanvas.isPointerClick = false;
        }
        //		if(GameCanvas.isPointerRelease&&pointerIsDowning){
        //			pointerIsDowning = false;
        //		}
    }

    int cmvx, cmdx;
    private RunText runtext;

    public void moveCamera()
    {
        //		mSystem.outz("pointerIsDowning="+pointerIsDowning);
        if (cmRun != 0 && !pointerIsDowning)
        {
            cmtoX += cmRun / 100;
            if (cmtoX < 0)
                cmtoX = 0;
            else if (cmtoX > cmxLim)
                cmtoX = cmxLim;
            else
                cmx = cmtoX;
            cmRun = cmRun * 9 / 10;
            if (cmRun < 100 && cmRun > -100)
                cmRun = 0;

        }
        if (cmx != cmtoX && !pointerIsDowning)
        {
            cmvx = (cmtoX - cmx) << 2;
            cmdx += cmvx;
            cmx += cmdx >> 4;
            cmdx = cmdx & 0xf;
        }
    }

    public void paintMenu(mGraphics g)
    {
        //		GameCanvas.resetTrans(g);
        //		if (!isLoadData)
        //			return;
        if (isNPCMenu == NPC_MENU)
        {
            paint_NPC_MENU(g);
        }
        else if (isNPCMenu == NORMAL)
        {
            //			GuiCommunication.paintDialog(g, menuX - 6, menuTemY - hPlus - 6, w + 12, menuH
            //					+ hPlus + 12, 0);
            //\ paintDialog(g, menuX+w/2, menuTemY - hPlus, w, menuH + hPlus,
            // 12);
            paintRectNice(g, menuX, menuTemY, ww, menuH, (byte)2);


            //			AvMain.Font3dColor(g, nameMenu, menuX + w / 2, menuTemY - hPlus
            //					+ GameCanvas.hCommand / 4, 2,MainTabNew.COLOR_BLACK);
            mFont.tahoma_7b_white.drawString(g, nameMenu, menuX + ww / 2, menuTemY - hPlus
                    + GameCanvas.hCommand / 4, 2);
            if (isGiaotiep)
            {
                if (GameCanvas.lowGraphic)
                {
                    MainTabNew.paintRectLowGraphic(g, menuX, menuY - hPlus
                            + GameCanvas.hCommand + 2, ww, menuW - 4, 1);
                }
                else
                {
                    for (int i = 0; i <= ww / 32; i++)
                    {
                        if (i < ww / 32)
                            g.drawRegion(MainTabNew.imgTab[1], 0, 0, 32,
                                    menuW - 4, 0, menuX + i * 32, menuTemY
                                            - hPlus + GameCanvas.hCommand + 2,
                                    0);
                        else
                            g.drawRegion(MainTabNew.imgTab[1], 0, 0, 32,
                                    menuW - 4, 0, menuX + ww - 32, menuTemY
                                            - hPlus + GameCanvas.hCommand + 2,
                                    0);
                    }
                }
                if (vecFocus.size() > 1)
                {
                    g.drawRegion(MainTabNew.imgTab[7], 0, 16, 13, 8, 6, menuX
                            + 8 - GameCanvas.gameTick % 3, menuTemY - hPlus
                            + GameCanvas.hCommand + menuW / 2,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    g.drawRegion(MainTabNew.imgTab[7], 0, 24, 13, 8, 6, menuX
                            + ww - 8 + GameCanvas.gameTick % 3, menuTemY - hPlus
                            + GameCanvas.hCommand + menuW / 2,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                }
                //				AvMain.FontBorderColor(g, objSelect.name, menuX + w
                //						/ 2 + 13, menuTemY - hPlus + GameCanvas.hCommand
                //						+ menuW / 4 + 3, 2, MainTabNew.COLOR_YELLOW);
                //				int wname = mFont.tahoma_7b_black.getWidth(objSelect
                //						.name) / 2 + 1;
                //				objSelect.paintAvatarFocus(g, menuX + w / 2 - wname - 2,
                //						menuTemY - hPlus + GameCanvas.hCommand + menuW / 4 + 8);
            }
            if (!isPaint)
                return;
            if (pos == -1)
            {
                g.setClip(menuX + 2, menuTemY + 2, ww - 4, menuH - 4);
                g.translate(-cmx, 0);
                for (int i = 0; i < menuItems.size(); i++)
                {
                    if (i == menuSelectedItem)
                    {
                        g.setColor(0x000000);
                        g.fillRoundRect(menuX + i * menuW + 3, menuTemY + 3,
                                menuW - 8, menuH - 6, 6, 6);
                    }
                    string[] sc = ((Command)menuItems.elementAt(i)).subCaption;
                    if (sc == null)
                        sc = new string[] { ((Command)menuItems.elementAt(i)).caption };
                    int yCaptionStart = menuTemY + (menuH - (sc.Length * 14))
                            / 2 + 1;
                    for (int k = 0; k < sc.Length; k++)
                    {
                        if (i == menuSelectedItem)
                        {
                            mFont.tahoma_7b_white.drawString(g, sc[k], menuX
                                    + i * menuW + menuW / 2 - 1, yCaptionStart
                                    + k * 14, 2);
                        }
                        else
                            mFont.tahoma_7_white.drawString(g, sc[k], menuX
                                    + i * menuW + menuW / 2 - 1, yCaptionStart
                                    + k * 14, 2);
                    }

                }
            }
        }
        //				else {
        //				g.setClip(menuX + 3, menuY + 3, w - 6, menuH - 6);
        //				g.translate(0, -cmx);
        //				g.setColor(color[4]);
        //				if (pos == 2 || pos == 4)
        //					for (int i = 0; i < menuItems.size() - 1; i++) {
        //						g.setColor(color[4]);
        //						g.fillRect(menuX + 8, menuY + 3 + menuW + i * menuW,
        //								w - 16, 1);
        //					}
        //				int min = cmx / menuW - 1;
        //				if (min < 0)
        //					min = 0;
        //				int max = min + sizeMenu + 2;
        //				if (max > menuItems.size()) {
        //					max = menuItems.size();
        //					min = max - sizeMenu - 2;
        //					if (min < 0)
        //						min = 0;
        //				}
        //				if (menuSelectedItem > -1)
        //					paintSelect(g, menuX + 3, menuY + 3 + menuSelectedItem
        //							* menuW, w - 6, menuW + 1);
        //				for (int i = min; i < max; i++) {
        //					iCommand cmd = (iCommand) menuItems.elementAt(i);
        //					if (pos == 2) {
        //						cmd.paintCaptionImageMenu(g, menuX + w / 2, menuY + 6
        //								+ menuW / 4 + i * menuW, 2);
        //					} else if (pos == 0 || pos == 3) {
        //						cmd.paintCaptionImageMenu(g, menuX + 6, menuY + 6
        //								+ menuW / 4 + i * menuW, 0);
        //					} else if (pos == 1) {
        //						cmd.paintCaptionImageMenu(g, menuX + w - 6, menuY + 6
        //								+ menuW / 4 + i * menuW, 1);
        //					} else if (pos == 4) {
        //						cmd.paintCaptionImageMenu(g, menuX + 10, menuY + 6
        //								+ menuW / 4 + i * menuW, 0);
        //					}
        //				}
        //
        //				GameCanvas.resetTrans(g);
        //				if (GameScreen.help.Step >= 0 && isHelp) {
        //					int t = GameScreen.help.itemMenuHelp();
        //					if (t >= 0) {
        //						MainHelp
        //								.paintPopup(g, menuX - 6 - 70, menuY + 16 + t
        //										* menuW - GameCanvas.hText, 70,
        //										GameCanvas.hText, MainHelp.POS_NULL,
        //										T.helpMenu);
        //						g.drawRegion(AvMain.imgSelect, 0, 0, 12, 16, 4, menuX
        //								+ 4 + (GameCanvas.gameTick / 2) % 4, menuY + 14
        //								+ t * menuW, mGraphics.VCENTER
        //								| mGraphics.RIGHT);
        //					}
        //				}
        //				paintCmd(g);
        //			}
        //		} 
        //	else if (isNPCMenu == QUICK_MENU) {
        //			if (isPosPoint) {
        //				g.drawRegion(PaintInfoGameScreen.imgOther[4], 0, 0, 20, 16, 0,
        //						menuX - 20, menuY + menuH - 16, 0);
        //				g.drawRegion(PaintInfoGameScreen.imgOther[4], 20, 0, 20, 16, 0,
        //						menuX + hShow, menuY + menuH - 16, 0);
        //				if (hShow == menuH) {
        //					g.drawRegion(PaintInfoGameScreen.imgBackQuick, 20, 0, 20,
        //							40, 2, menuX, menuY, 0);
        //					g.drawRegion(PaintInfoGameScreen.imgBackQuick, 20, 0, 20,
        //							40, 0, menuX + 20, menuY, 0);
        //				} else {
        //					for (int i = 0; i < hShow; i += menuH) {
        //						if (i == 0)
        //							g.drawRegion(PaintInfoGameScreen.imgBackQuick, 0,
        //									0, 40, 40, 2, menuX, menuY, 0);
        //						else if (i + menuH >= hShow)
        //							g.drawRegion(PaintInfoGameScreen.imgBackQuick, 0,
        //									0, 40, 40, 0, menuX + i, menuY, 0);
        //						else
        //							g.drawRegion(PaintInfoGameScreen.imgBackQuick, 0,
        //									20, 40, 40, 0, menuX + i, menuY, 0);
        //					}
        //				}
        //				g.setClip(menuX + 5, menuY-20, hShow - 10, menuH+20);
        //			} 
        //			else {
        //				g.drawRegion(PaintInfoGameScreen.imgOther[4], 0, 0, 20,
        //								16, 0, menuX + w / 2 - (hShow) / 2 - 20, menuY
        //										+ menuH - 16, 0);
        //				g.drawRegion(PaintInfoGameScreen.imgOther[4], 20, 0, 20, 16, 0,
        //						menuX + w / 2 + (hShow) / 2, menuY + menuH - 16, 0);
        //				if (hShow == menuH) {
        //					g.drawRegion(PaintInfoGameScreen.imgBackQuick, 20, 0, 20,
        //							40, 2, menuX + w / 2 - 20, menuY, 0);
        //					g.drawRegion(PaintInfoGameScreen.imgBackQuick, 20, 0, 20,
        //							40, 0, menuX + w / 2, menuY, 0);
        //				} else {
        //					for (int i = 0; i < hShow; i += menuH) {
        //						if (i == 0)
        //							g.drawRegion(PaintInfoGameScreen.imgBackQuick, 0,
        //									0, 40, 40, 2,
        //									menuX + w / 2 - hShow / 2 + i, menuY, 0);
        //						else if (i + menuH >= hShow)
        //							g.drawRegion(PaintInfoGameScreen.imgBackQuick, 0,
        //									0, 40, 40, 0,
        //									menuX + w / 2 - hShow / 2 + i, menuY, 0);
        //						else
        //							g.drawRegion(PaintInfoGameScreen.imgBackQuick, 0,
        //									20, 40, 40, 0, menuX + w / 2 - hShow / 2
        //											+ i, menuY, 0);
        //					}
        //				}
        //				g.setClip(menuX + w / 2 - hShow / 2 + 5, menuY-20, hShow - 10,
        //						menuH+20);
        //			}

        //			g.translate(-cmx, 0);
        ////			for (int i = 0; i < menuItems.size(); i++) {
        ////				Command cmd = (Command) menuItems.elementAt(i);
        ////				cmd.paint(g, cmd.xCmd, cmd.yCmd);
        ////			}
        ////			GameCanvas.resetTrans(g);
        //			g.translate(-cmx, 0);
        for (int i = 0; i < menuItems.size(); i++)
        {
            iCommand cmd = (iCommand)menuItems.elementAt(i);
            cmd.paintClickCaption(g, cmd.xCmd, cmd.yCmd, 2); ;
        }
        //		}
    }

    private void paint_NPC_MENU(mGraphics g)
    {
        // TODO Auto-generated method stub
        int xpaint = menuX + 6, ypaint = menuY + 8;
        // g.drawImage(img, xpaint+5, menuY, mGraphics.BOTTOM|mGraphics.LEFT);
        //		paintDialog(g, menuX, menuTemY, w, menuH, 12);
        //		MainObject obj = MainObject.get_Object(IdNpc, typeO);
        //		if (obj == null)
        //			return;
        //		obj.paintBigAvatar(g, menuX + w - 10, menuY);
        //		Font3dWhite(g, obj.name, xpaint + 10, ypaint, 0);
        if (runtext != null)
            runtext.paintText(g, archorRunText);
        GameScr.resetTranslate(g);
        for (int i = 0; i < menuItems.size(); i++)
        {
            iCommand cmd = (iCommand)menuItems.elementAt(i);
            cmd.paint(g, cmd.xCmd, cmd.yCmd);
        }

    }

    public void doCloseMenu()
    {
        isShowMenu = false;
        //		GameCanvas.isPointerSelect = false;
        GameCanvas.isPointerClick = false;
        //		GameCanvas.isPointerEnd = true;
    }

    public void updateMenu()
    {
        if (timeShow > 0)
        {
            timeShow++;
            if (hShow < ww)
            {
                hShow += menuH;
                if (hShow >= ww)
                {
                    hShow = ww;
                    timeShow = 0;
                }
            }
        }
        else if (timeShow < 0)
        {
            timeShow--;
            if (hShow > 0)
            {
                hShow -= menuH;
                if (hShow <= 0)
                {
                    hShow = 0;
                    timeShow = 0;
                    doCloseMenu();
                }
            }
        }
        if (!isLoadData)
            return;
        moveCamera();
        //		if (isNPCMenu == NPC_MENU) {
        ////			if (runText != null)
        ////				runText.updateDlg();
        //			for (int i = 0; i < menuItems.size(); i++) {
        //				Command cmd = (Command) menuItems.elementAt(i);
        //				cmd.updatePointer();
        //			}
        //		} else if (isNPCMenu == QUICK_MENU) {
        //			if (!GameCanvas.isPointerMove && timeShow == 0)
        //				for (int i = 0; i < menuItems.size(); i++) {
        //					Command cmd = (Command) menuItems.elementAt(i);
        //					cmd.updatePointerShow(cmx, 0);
        //				}
        //		}
        if (menuTemY > menuY)
        {
            int delta = ((menuTemY - menuY) >> 1);
            if (delta < 1)
                delta = 1;
            menuTemY -= delta;
        }
        if (xc != 0)
        {
            xc >>= 1;
            if (xc < 0)
                xc = 0;
        }
        if (waitToPerform > 0)
        {
            waitToPerform--;
            if (waitToPerform == 0)
            {
                isShowMenu = false;
                if (menuSelectedItem >= 0)
                {
                    Command c = ((Command)menuItems
                            .elementAt(menuSelectedItem));
                    //					perform(c);
                    GameCanvas.clearKeyHold();
                    GameCanvas.clearKeyPressed();
                    //					GameCanvas.isPointerEnd = true;
                    //					GameCanvas.isPointerSelect = false;

                }
            }

        }
        //		super.updatePointer();
    }

    //	public void perform(iCommand cmd) {
    //		if (cmd != null) {
    //			if (cmd.action != null)
    //				cmd.action.perform();
    //			else {
    //				if (cmd.Pointer != null) {
    //					cmd.Pointer.commandPointer(cmd.indexMenu, cmd.subIndex);
    //				} else
    //					GameCanvas.currentScreen.commandMenu(cmd.indexMenu,
    //							cmd.subIndex);
    //			}
    //			GameCanvas.isPointerSelect = false;
    //		}
    //	}


    public static void paintRectNice(mGraphics g, int xTab, int yTab, int wTab,
            int hTab, byte colorBack)
    {
        if (hTab % 2 == 1)
            hTab += 1;
        if (GameCanvas.lowGraphic)
        {
            if (colorBack > 2)
            {
                if (colorBack == 8 || colorBack == 12)
                    colorBack = 4;
                else
                    colorBack = 3;
            }
            MainTabNew
                    .paintRectLowGraphic(g, xTab, yTab, wTab, hTab, colorBack);
        }
        else
        {
            for (int i = 0; i <= (wTab) / 32; i++)
            {
                for (int j = 0; j <= (hTab) / 32; j++)
                {
                    if (i == (wTab) / 32)
                    {
                        if (j == (hTab) / 32)
                        {
                            //							System.out.println("Image ----> "+MainTabNew.imgTab[colorBack]);
                            g.drawImage(MainTabNew.imgTab[colorBack], xTab
                                    + wTab - 32, yTab + hTab - 32, 0);
                        }
                        else
                        {
                            g.drawImage(MainTabNew.imgTab[colorBack], xTab
                                    + wTab - 32, yTab + j * 32, 0);
                        }
                    }
                    else
                    {
                        if (j == (hTab) / 32)
                        {
                            g.drawImage(MainTabNew.imgTab[colorBack], xTab + i
                                    * 32, yTab + hTab - 32, 0);
                        }
                        else
                        {
                            g.drawImage(MainTabNew.imgTab[colorBack], xTab + i
                                    * 32, yTab + j * 32, 0);
                        }
                    }
                }
            }
        }
    }
}
