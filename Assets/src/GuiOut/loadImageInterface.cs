

public class loadImageInterface {
	
	//chat
	public static mBitmap[] imgInside = new mBitmap[13];
	public static mBitmap[] imgLine =  new mBitmap[4];
	public static mBitmap[] imgEmo =  new mBitmap[64];
	public static mBitmap[] imgOnline =  new mBitmap[2];
	public static mBitmap imgPopup,imgGocChat;
	public static mBitmap imgChat;//background chat
	public static mBitmap imgfocusActor;//
	
	//tab main inventory
	public static mBitmap[] imgConerGui= new mBitmap[3];//background tab
	public static mBitmap imgBoxName,imgBoxName_1;//background chat
	public static mBitmap closeTab;
	public static mBitmap btnTab;
	public static mBitmap btnTabFocus;
    public static mBitmap ImgItem;
    public static mBitmap imgFocusSelectItem, imgFocusSelectItem0, imgFocusSelectItem1;
	
	
	//frame sub info
	public static mBitmap imgsub_frame_info;
	public static mBitmap imgsub_frame_info_2;
    public static mBitmap imgsub_info_conner;
    public static mBitmap imgsub_stone_01;

	public static mBitmap img_use;
	public static mBitmap img_use_focus;
	
	//bg trang bi
	public static mBitmap img_bg_char_wearing;
	
	//quest
	public static mBitmap imgShortQuest,imgShortQuest_Close;
	public static mBitmap[] coins = new mBitmap[2];
	
	//gui main
	public static mBitmap imgAttack,imgAttack_1,imgCharacter_info,imgBlood,imgMp,imgExp;
	
	//gui frame chat
	public static mBitmap imgChatConner,imgChatRec,imgChatRec_1,imgChatConner_1,imgChatButton,imgChatButtonFocus,btnSendChat,btnSendChatFocus;
	
	public static mBitmap imgCharPoint;
	
	//gui move
	public static mBitmap imgMoveCenter, imgMoveNormal, imgMoveFocus,imgName,imgLineTrade;
	
	// gui eight gate
	public static mBitmap[] imgHumanEightGate = new mBitmap[25];
	
	public static mBitmap[] imgBGMenuIcon= new mBitmap[10];
	
	public static mBitmap charPic,smallTest;
	
	public static mBitmap bgQuestConner,bgQuestLine;
	public static mBitmap bgQuestConnerFocus,bgQuestLineFocus;
	
	//sub menu
	public static mBitmap[] imgSubMenu = new mBitmap[3];

    //icon  menu
    public static mBitmap imgCharIcon, imgMissionIcon, imgShopIcon, imgContactIcon, imgImproveIcon,
    imgTradeIcon, imgFriendIcon, imgTeamIcon, imgLogout, imgIconDeoCo;
	
	// image textfield
	
	public static mBitmap imgTf;
	public static mBitmap imgTf0, imgTf1, imgTf2, imgTf3;

    // image Screen 
    public static mBitmap imgTatus, imgRock, imgTrangtri, khu, btnChangeFocus0, btnChangeFocus1;

    public static mBitmap imgXinCho, imgMap, chardie, bongChar, iconpk, icn_Mail;
    public static mBitmap imgIconFb0, imgIconFb1, imgIconGoogle0, imgIconGoogle1, btnLogin0, btnLogin1;

	public static void loadImage(){
		// load img textfield
        Cout.println("loadImage Interface ");
		imgTf = GameCanvas.loadImage("/screen/tf.png");
		imgTf0 = GameCanvas.loadImage("/screen/tf/0.png");
		imgTf1 = GameCanvas.loadImage("/screen/tf/1.png");
		imgTf2 = GameCanvas.loadImage("/screen/tf/2.png");
		imgTf3 = GameCanvas.loadImage("/screen/tf/3.png");
		
		//load Screen 
		imgRock = GameCanvas.loadImage("/screen/rockchar1.png");
		imgTatus = GameCanvas.loadImage("/screen/statue.png");
		imgTrangtri = GameCanvas.loadImage("/screen/trangtri.png");


        bongChar = GameCanvas.loadImage("/GuiNaruto/bong.png");
        khu = GameCanvas.loadImage("/GuiNaruto/khu.png");
        imgIconDeoCo = GameCanvas.loadImage("/GuiNaruto/iconMap/pk.png");
		//load sub menu
		for(int i=0;i<3;i++)
		{
			imgSubMenu[i]=GameCanvas.loadImage("/GuiNaruto/iconMap/frame_sub_icon_"+(i+1)+".png");
		}
		imgfocusActor=GameCanvas.loadImage("/GuiNaruto/focusActor.png");
//		GameScr.ypaintFocus = -imgfocusActor.getHeight();
		imgCharIcon=GameCanvas.loadImage("/GuiNaruto/iconMap/char.png");
		imgMissionIcon=GameCanvas.loadImage("/GuiNaruto/iconMap/mission.png");
		imgShopIcon=GameCanvas.loadImage("/GuiNaruto/iconMap/shop.png");
		imgContactIcon=GameCanvas.loadImage("/GuiNaruto/iconMap/contact.png");
		imgImproveIcon=GameCanvas.loadImage("/GuiNaruto/iconMap/improve.png");
		imgTradeIcon=GameCanvas.loadImage("/GuiNaruto/iconMap/trade.png");
		imgLogout=GameCanvas.loadImage("/GuiNaruto/iconMap/logout.png");
		imgFriendIcon=GameCanvas.loadImage("/GuiNaruto/iconMap/bestfriend.png");
		imgTeamIcon=GameCanvas.loadImage("/GuiNaruto/iconMap/team.png");
		
		bgQuestConner=GameCanvas.loadImage("/GuiNaruto/quest/listConner.png");
		bgQuestLine=GameCanvas.loadImage("/GuiNaruto/quest/list.png");

		bgQuestConnerFocus=GameCanvas.loadImage("/GuiNaruto/quest/listConner_focus.png");
		bgQuestLineFocus=GameCanvas.loadImage("/GuiNaruto/quest/list_focus.png");
		for (int i = 0; i < coins.Length; i++) {
			coins[i]=GameCanvas.loadImage("/GuiNaruto/myseft/coins"+(i+1)+".png");
		}
		smallTest=GameCanvas.loadImage("/GuiNaruto/Small20.png");
		charPic=GameCanvas.loadImage("/GuiNaruto/char_pic.png");
		imgLineTrade=GameCanvas.loadImage("/GuiNaruto/Trade/line.png");
		imgName=GameCanvas.loadImage("/GuiNaruto/Trade/name.png");
		//imgPopup = GameCanvas.loadImage("/interface/popup.png");
		imgGocChat = GameCanvas.loadImage("/gocChat.png");
		//imgChat=GameCanvas.loadImage("/interface/img_Chat.png");
		for (int i = 0; i < imgOnline.Length; i++) {
			imgOnline[i] = GameCanvas.loadImage("/GuiNaruto/"+(i==0?"offline":"online")+".png");
		}
		for (int i = 0; i < imgBGMenuIcon.Length; i++) {
			imgBGMenuIcon[i] = GameCanvas.loadImage("/GuiNaruto/MapMenu/map_icon_" + (i+1) + ".png");
		}
		
        //for (int i = 0; i < imgLine.Length; i++) {
        //    imgLine[i] = GameCanvas.loadImage("/interface/tab" + i + ".png");
        //}
		
        //for(int i = 0; i < imgInside.Length; i++){
        //    imgInside[i] = GameCanvas.loadImage("/interface/screentab" + i + ".png");
        //}
		//load image icon chat
		for(int i = 0; i < imgEmo.Length; i++){
			imgEmo[i] = GameCanvas.loadImage("/iconChat/emo" + i + ".png");
		}
		
		//quest
		imgShortQuest = GameCanvas.loadImage("/GuiNaruto/show.png");
		imgShortQuest_Close = GameCanvas.loadImage("/GuiNaruto/hide.png");
		
		//tab inventory main
		imgConerGui[0]=GameCanvas.loadImage("/GuiNaruto/frame_conner.png");
		imgConerGui[1]=GameCanvas.loadImage("/GuiNaruto/frame.png");
		imgConerGui[2]=GameCanvas.loadImage("/GuiNaruto/frame_01.png");
		imgBoxName=GameCanvas.loadImage("/GuiNaruto/box_name.png");
		imgBoxName_1=GameCanvas.loadImage("/GuiNaruto/box_name_1.png");
		closeTab=GameCanvas.loadImage("/GuiNaruto/close.png");
		btnTab=GameCanvas.loadImage("/GuiNaruto/button_tab.png");
		btnTabFocus=GameCanvas.loadImage("/GuiNaruto/button_TabFocus.png");
		ImgItem=GameCanvas.loadImage("/GuiNaruto/item_box.png");
		
		//tab info sub
		imgsub_frame_info=GameCanvas.loadImage("/GuiNaruto/sub_frame_info.png");
		imgsub_frame_info_2=GameCanvas.loadImage("/GuiNaruto/sub_frame_info_2.png");
		imgsub_info_conner=GameCanvas.loadImage("/GuiNaruto/sub_info_conner.png");
		imgsub_stone_01=GameCanvas.loadImage("/GuiNaruto/sub_stone_01.png");
		
		//use
		img_use=GameCanvas.loadImage("/GuiNaruto/button2.png");
		img_use_focus=GameCanvas.loadImage("/GuiNaruto/button1.png");
		
		//main gui
		imgCharacter_info=GameCanvas.loadImage("/GuiNaruto/charBoard/character_info.png");
		imgBlood=GameCanvas.loadImage("/GuiNaruto/charBoard/blood.png");
		imgMp=GameCanvas.loadImage("/GuiNaruto/charBoard/mana.png");
		imgExp=GameCanvas.loadImage("/GuiNaruto/charBoard/exp.png");;
		
		imgAttack=GameCanvas.loadImage("/GuiNaruto/attack.png");
		imgAttack_1=GameCanvas.loadImage("/GuiNaruto/attack_1.png");

        imgFocusSelectItem = GameCanvas.loadImage("/GuiNaruto/focus.png");
        imgFocusSelectItem0 = GameCanvas.loadImage("/GuiNaruto/focusitem.png");//focus0
        imgFocusSelectItem1 = GameCanvas.loadImage("/GuiNaruto/focus1.png");
		
		imgChatConner=GameCanvas.loadImage("/GuiNaruto/frameChat/chat_frame.png");
		imgChatRec=GameCanvas.loadImage("/GuiNaruto/frameChat/chat_frame_1.png");
		imgChatConner_1=GameCanvas.loadImage("/GuiNaruto/frameChat/chat_frame_2.png");
		imgChatRec_1=GameCanvas.loadImage("/GuiNaruto/frameChat/chat_frame3.png");
		imgChatButton=GameCanvas.loadImage("/GuiNaruto/frameChat/chat_button2.png");
		imgChatButtonFocus=GameCanvas.loadImage("/GuiNaruto/frameChat/chat_button1.png");
//		img_bg_char_wearing=GameCanvas.loadImage("/GuiNaruto/frameChat/screentab6.png");
		btnSendChat=GameCanvas.loadImage("/GuiNaruto/frameChat/btnChat.png");
		btnSendChatFocus=GameCanvas.loadImage("/GuiNaruto/frameChat/btnChatFocus.png");
		// move
		imgMoveCenter = GameCanvas.loadImage("/GuiNaruto/movecenter.png");
		imgMoveNormal = GameCanvas.loadImage("/GuiNaruto/move.png");
		imgMoveFocus = GameCanvas.loadImage("/GuiNaruto/movefocus.png");
		
		// hunmen eight gate 
        imgXinCho = GameCanvas.loadImage("/GuiNaruto/imgXinCho.png");
        imgMap = GameCanvas.loadImage("/GuiNaruto/imgMap.png");
        chardie = GameCanvas.loadImage("/GuiNaruto/chardie.png");
        iconpk = GameCanvas.loadImage("/GuiNaruto/iconpk.png");
        icn_Mail = GameCanvas.loadImage("/GuiNaruto/icn_Mail.png");
        btnChangeFocus0 = GameCanvas.loadImage("/GuiNaruto/btnChangeFocus0.png");//
        btnChangeFocus1 = GameCanvas.loadImage("/GuiNaruto/btnChangeFocus1.png");//

        imgIconFb0 = GameCanvas.loadImage("/GuiNaruto/iconFb0.png");//
        imgIconFb1 = GameCanvas.loadImage("/GuiNaruto/iconFb1.png");//
        imgIconGoogle0 = GameCanvas.loadImage("/GuiNaruto/iconGoogle0.png");//
        imgIconGoogle1 = GameCanvas.loadImage("/GuiNaruto/iconGoogle1.png");//
        btnLogin0 = GameCanvas.loadImage("/GuiNaruto/btnLogin0.png");//
        btnLogin1 = GameCanvas.loadImage("/GuiNaruto/btnLogin1.png");//

	}
	///////// Image Screen  
	
	
	
	public static void loadImageHuman(){ // load image eight gate
		for(int i = 0; i < imgHumanEightGate.Length; i++){
			imgHumanEightGate[i] = GameCanvas.loadImage("/GuiNaruto/human/"+(i+1)+".png");
		}
	}
	

}
