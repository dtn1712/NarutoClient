

using System;
public class BgItem {
	public int id;
	public int trans;
	public short idImage;
	public int x;
	public int y;
	public int dx;
	public int dy;

	public sbyte layer;
	public int nTilenotMove;
	public int[] tileX;
	public int[] tileY;

    public static mHashtable imgNew = new mHashtable();
    public static mHashtable imgPathLoad = new mHashtable();
	public static mVector vKeysNew = new mVector();
	public static mVector vKeysLast = new mVector();
	public static sbyte[] newSmallVersion;
    public static mBitmap[] imgobj;
	
	public static void clearHashTable() {
		// Res.out("vKey size= " + vKeys.size() + " imgsize= " + imgNew.size() +
		// "---------------------------------------");
		// if (imgNew.size() > 20) {
		// // while (vKeys.size() > 10) {
		// // imgNew.remove((string) vKeys.elementAt(0));
		// // vKeys.removeElementAt(0);
		// // }
		// for (int i = 0; i < 10; i++) {
		// imgNew.remove((string) vKeys.elementAt(i));
		//
		// }
		// for (int i = 0; i < 10; i++) {
		// vKeys.removeElementAt(0);
		// }
		// }
		 imgNew.clear();
		 vKeysNew.removeAllElements();
		 vKeysLast.removeAllElements();
	}

	public static bool isExistKeyNews(string keyNew) {
		for (int a = 0; a < vKeysNew.size(); a++) {
			string strkeyNew = (string) BgItem.vKeysNew.elementAt(a);
			if (strkeyNew.Equals(keyNew))
				return true;
		}
		return false;
	}

	public static bool isExistKeyLast(string keyLast) {
		for (int a = 0; a < vKeysLast.size(); a++) {
			string keyLaString = (string) BgItem.vKeysLast.elementAt(a);
			if (keyLaString.Equals(keyLast))
				return true;
		}
		return false;
	}

	

	bool isBlur = false;
	public int transX = 0;
	public int transY = 0;
	public static int[] idMiniBg = new int[] { 79, 80, 81, 85, 86, 90, 91, 92, 99, 100, 101, 102, 103, 104, 105, 106,
			107, 108};

	public bool isMiniBg() {
		
		for (int i = 0; i < idMiniBg.Length; i++)
			if (idImage == idMiniBg[i])
				return true;
		return false;
	}

	public static bool isCorlorNotChange(int id) {
		for (int i = 0; i < idMiniBg.Length; i++)
			if (id >= 78 && id <= 118)
				return true;
		return false;
	}
	public BgItem(){
		
	}
	public void paint(mGraphics g) {
		
	if(BgItem.imgobj[idImage]==null&&imgPathLoad.get("/mapobject/"+(idImage+SmallImage.ID_ADD_MAPOJECT)+".png")==null)
			createBgItem(idImage);
		if (BgItem.imgobj[idImage] != null) {
			if (CRes.checkCollider(x, 
					x +BgItem.imgobj[idImage].getWidth(), GameScr.cmx, GameScr.cmx + GameCanvas.w, y,
					y + BgItem.imgobj[idImage].getHeight(),GameScr.cmy, GameScr.cmy + GameCanvas.h))
			{
				g.drawRegion(BgItem.imgobj[idImage]/*GameScr.imgBgItem[idImage]*/, 0, 0, BgItem.imgobj[idImage].getWidth(), BgItem.imgobj[idImage].getHeight(), trans, x + dx + transX, y + dy + transY, 0);
				
			}
		}
	
	}
	public static mBitmap createBgItem(int id)
	{
		imgPathLoad.put("/mapobject/"+(id+SmallImage.ID_ADD_MAPOJECT)+".png",0);
		mBitmap img = (mBitmap)imgNew.get(id+"");
		if(img==null){
			img =  GameCanvas.loadImage("/mapobject/"+(id+SmallImage.ID_ADD_MAPOJECT)+".png");
			if(img!=null){
				BgItem.imgobj[id] = img;
				//imgNew.put(id+"", img);
			}else{
				//request serrver
			}
		}
		return img;
	}

	public static void cleanImg()
	{
//		Enumeration k = imgNew.keys();
//		mVector vkey = new mVector();
//	   while (k.hasMoreElements()) {
//		   String key = (String) k.nextElement();
//		   vkey.addElement(key);
//			
//	   }
//		for (int i = 0; i < vkey.size(); i++) {
//			   String key = (String) vkey.elementAt(i);
//			   mBitmap img = (mBitmap)imgNew.get(key);
//			   if(img!=null){
//					try {
//						if(img.image!=null)
//						img.image.recycle();
//						img.image = null;
//					} catch (Exception e) {
//						// TODO: handle exception
//					}
//					imgNew.remove(key); 
//				}
//		   }
//		imgNew.clear();
		try {
			for (int i = 0; i < BgItem.imgobj.Length; i++) {
				if( BgItem.imgobj[i]!=null&&BgItem.imgobj[i].image!=null)
					BgItem.imgobj[i].cleanImg();
				if( BgItem.imgobj[i]!=null)
					BgItem.imgobj[i] = null;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}


	public void changeColor() {
		// TODO Auto-generated method stub
		
	}
	
	public void loadMapItem(){ // đọc dữ liệu data Map item
		//		DataInputStream dis;
//		try{
//			dis = new DataInputStream(MyStream.readFile("/mapitem/mapItem"));
//			short Count = dis.readShort();
//			for(int i = 0; i < Count; i++){
//				BgItem bi = new BgItem();
//				bi.id = i;
//				bi.idImage = dis.readShort();
//				bi.layer = dis.readByte();
//				bi.dx = dis.readShort();
//				bi.dy = dis.readShort();
//				byte nTileNotMove = dis.readByte();
//				bi.tileX = new int[nTileNotMove];
//				bi.tileY = new int[nTileNotMove];
//				for (int j = 0; j < nTileNotMove; j++) {
//					bi.tileX[i] = (int) dis.readByte(); 
//					bi.tileY[i] = (int) dis.readByte();
//				}
//				TileMap.vItemBg.addElement(bi);
//
//			}
//			
//			
//		}catch(Exception e){
//			e.printStackTrace();
//		}
	}
	public void loadMaptable(){
		DataInputStream dis = null;
		try{
			dis = new DataInputStream("/mapitem/mapTable0");
			short nTile = dis.readShort();
			TileMap.tileIndex = new int[nTile][][];
			TileMap.tileType = new int[nTile][];
			for (int i = 0; i < nTile; i++) {
				short nTypeSize = dis.readShort();
//				if(i==0)
//					System.out.println("set tile >>>>>>>>"+nTypeSize);
				TileMap.tileType[i] = new int[nTypeSize];
				TileMap.tileIndex[i] = new int[nTypeSize][];
				for (int a = 0; a < nTypeSize; a++) {
					TileMap.tileType[i][a] = dis.readInt();
					short sizeIndex = dis.readShort();
					TileMap.tileIndex[i][a] = new int[sizeIndex];
					for (int b = 0; b < sizeIndex; b++) {
						TileMap.tileIndex[i][a][b] = dis.readByte();
					}
				}
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
    public mBitmap[] Imgmap = new mBitmap[230];
	public void loadImgmap(){
		try{
			for(int i = 0; i < Imgmap.Length; i++ ){
				Imgmap[i] = GameCanvas.loadImage("/mapobject/"+i+".png");
				
			}
			
		}catch(Exception e){
		//	e.printStackTrace();
		}
		
		
	}
}
