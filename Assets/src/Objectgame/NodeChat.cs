

using System;
public class NodeChat {
	public int type;
	public const sbyte t_TEXT = 0;// text
	public const sbyte t_EMO = 1;// icon
	public const sbyte t_TEXTEMO = 2;//vua test vua icon
	public const sbyte HSTRING=12;//chieu cao font chu
	
	public string[][] textEmo;
	public string[] text;
	public string textdai,name;
	public static string mtextConvertEmo = "zpzp";
	public int hnode,wname;
	public bool isMe;
	public static int wnode ;
	public mVector listEmoLine = new mVector();
	
	public static string[] maEmo = new string[]{ //14,
			"z($)z","z:Oz","(highfive)","z8-)","z:Sz","(wait)","(oliver)","z(nod)","(facepalm)","z:^)",
			"(muscle)","z:)z" ,"z|-)","(rofl)","(lala)","z(ske)","z:$z","z(n)z","z>o)","z;(z",
			"(bandit)","z(y)z","z(ci)","z:?z","(giggle)","z(a)z","(makeup)","z>2)","(swear)","(wave)",
			"zz(v)z","z(bow)","z(*)z","(emo)","z:]z","z:@z","(doh)","zz(6)","z:&z","(happy)",
			"z;)z","|-(","z(t)z","(whew)","(rock)","z:|z","(yawn)",":P","(hande)","(tmi)z",
			"(fingers)","z:Dz","(smirk)","(punch)","z:(z","z:*z","(wasntme)","8-|z","z::|","z:xz",
			"(waiting)","(clap)","z(mm)","(headbang)"
	};
	
	/*khong cho vao khung
	 * cat chuoi vua add icon hinh va doan chat
	 * isMee: true: la minh, false: thang khac
	 */
	public NodeChat(string textt, bool isMee,string namee)
    {	//isMee = false;
        this.textdai = textt;
        this.isMe = isMee;
        if (this.isMe)
            namee = Char.myChar().cName;
        this.text = mFont.tahoma_7_yellow.splitFontArray((isMee == false ? namee + ": " : namee + ": ") + textt, wnode);
        this.type = t_TEXT;
        int[] typetext = new int[text.Length];
		
		//update icon
		if(text.Length>=1){
			for (int m = 0; m < text.Length; m++) {
				mVector listEmo = new mVector();
				typetext[m] = t_TEXT;
				for (int i = 0; i < maEmo.Length; i++) {

					if(text[0].IndexOf(maEmo[i])>=0){
						mVector nEmo = findAllEmoInText(text[m], maEmo[i], i);
						for (int j = 0; j < nEmo.size(); j++) {
							EmoText emo = (EmoText)nEmo.elementAt(j);
							listEmo.addElement(emo);
						}
						typetext[m] = t_EMO;
						if(this.type==t_TEXT)
						this.type = t_EMO;
					}
				}
				if(listEmo.size()>1)
				listEmo = XepThuTuEmo(listEmo);
				if(typetext[m] == t_EMO){
					int lengEmo = 0;
					for (int i = 0; i < listEmo.size(); i++) {
						EmoText emo = (EmoText)listEmo.elementAt(i);
						lengEmo+= maEmo[emo.loai].Length;
					}
					if(lengEmo<text[m].Length){
						if(this.type==t_EMO)
						this.type = t_TEXTEMO;
						typetext[m] = t_TEXTEMO;
						
					}else if(lengEmo<text[m].Length){
						if(this.type==t_TEXT)
						this.type = t_EMO;
						typetext[m] = t_EMO;
					}
				}
				if(typetext[m] == t_TEXTEMO){
					string[] listext = slitFontWithEmo(text[m], listEmo);
					EmoLineText emoline = new EmoLineText(t_TEXTEMO,listext, listEmo);
					listEmoLine.addElement(emoline);
				} else if(typetext[m] == t_TEXT){
					string[] textline = new string[1];
					textline[0] = text[m];
					EmoLineText emoline = new EmoLineText(t_TEXT,textline, listEmo);
					listEmoLine.addElement(emoline);
				} 
				else if(typetext[m] == t_EMO){
					string[] textline = new string[1];
					textline[0] = "";
					EmoLineText emoline = new EmoLineText(t_EMO,textline, listEmo);
					listEmoLine.addElement(emoline);
				} 
			}
		}
		bool[] isType = new bool[3];
		for (int i = 0; i < typetext.Length; i++) {
			if(typetext[i]==t_TEXT){
				isType[t_TEXT] = true;
			}
			if(typetext[i]==t_EMO){
				isType[t_EMO] = true;
			}
			if(typetext[i]==t_TEXTEMO){
				isType[t_TEXTEMO] = true;
			}
		}
		if(isType[t_TEXTEMO])
			this.type = t_TEXTEMO;
		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&!isType[t_EMO]){
			this.type = t_TEXT;
		}
		else if(!isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
			this.type = t_EMO;
		}
		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
			this.type = t_TEXTEMO;
		}
		if(this.text!=null)
		this.hnode =HSTRING*(this.text.Length)+HSTRING/4;
		this.name = namee;
		wname = mFont.tahoma_7_yellow.getWidth(name);
	}
	
	/*cho vao khung
	 * cat theo w,h de xuong dong
	 * wnodePainInfoGameScr: do rong cua khung
	 */
	public NodeChat(string textt, bool isMee,String namee,int wnodePainInfoGameScr)
	{
		this.textdai = textt;
		this.isMe = isMee;
		this.text  = mFont.tahoma_7_yellow.splitFontArray((isMee==false?namee+": ":"")+textt, wnodePainInfoGameScr);
		this.type = t_TEXT;
		int[] typetext = new int[text.Length];
		
		//update icon
		if(text.Length>=1){
			for (int m = 0; m < text.Length; m++) {
				mVector listEmo = new mVector();
				typetext[m] = t_TEXT;
				for (int i = 0; i < maEmo.Length; i++) {
					if(text[m].IndexOf(maEmo[i])>=0){
						mVector nEmo = findAllEmoInText(text[m], maEmo[i], i);
						for (int j = 0; j < nEmo.size(); j++) {
							EmoText emo = (EmoText)nEmo.elementAt(j);
							listEmo.addElement(emo);
						}
						typetext[m] = t_EMO;
						if(this.type==t_TEXT)
						this.type = t_EMO;
					}
				}
				if(listEmo.size()>1)
				listEmo = XepThuTuEmo(listEmo);
				if(typetext[m] == t_EMO){
				int lengEmo = 0;
					for (int i = 0; i < listEmo.size(); i++) {
						EmoText emo = (EmoText)listEmo.elementAt(i);
						lengEmo+= maEmo[emo.loai].Length;
					}
					if(lengEmo<text[m].Length){
						if(this.type==t_EMO)
						this.type = t_TEXTEMO;
						typetext[m] = t_TEXTEMO;
						
					}else if(lengEmo<text[m].Length){
						if(this.type==t_TEXT)
						this.type = t_EMO;
						typetext[m] = t_EMO;
					}
				}
				if(typetext[m] == t_TEXTEMO){
					string[] listext = slitFontWithEmo(text[m]+"   ", listEmo);
					EmoLineText emoline = new EmoLineText(t_TEXTEMO,listext, listEmo);
					listEmoLine.addElement(emoline);
				} else if(typetext[m] == t_TEXT){
					string[] textline = new string[1];
					textline[0] = text[m];
					EmoLineText emoline = new EmoLineText(t_TEXT,textline, listEmo);
					listEmoLine.addElement(emoline);
				} 
				else if(typetext[m] == t_EMO){
					string[] textline = new string[1];
					textline[0] = "";
					EmoLineText emoline = new EmoLineText(t_EMO,textline, listEmo);
					listEmoLine.addElement(emoline);
				} 
			}
		}
		bool[] isType = new bool[3];
		for (int i = 0; i < typetext.Length; i++) {
			if(typetext[i]==t_TEXT){
				isType[t_TEXT] = true;
			}
			if(typetext[i]==t_EMO){
				isType[t_EMO] = true;
			}
			if(typetext[i]==t_TEXTEMO){
				isType[t_TEXTEMO] = true;
			}
		}
		if(isType[t_TEXTEMO])
			this.type = t_TEXTEMO;
		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&!isType[t_EMO]){
			this.type = t_TEXT;
		}
		else if(!isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
			this.type = t_EMO;
		}
		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
			this.type = t_TEXTEMO;
		}
		if(this.text!=null)
		this.hnode = HSTRING*(this.text.Length)+HSTRING/4;
		this.name = namee;
		wname = mFont.tahoma_7_yellow.getWidth(name);
	}
	
	public void paint(mGraphics g,int x,int y,bool isMe,bool isWorld){
		switch (type) {
		case t_TEXT:
			for (int j = 0; j < text.Length; j++) {
				if(!isMe)
				mFont.tahoma_7.drawString(g, text[j],x,y+j*HSTRING+1, 0);
				else if(isWorld)
					mFont.tahoma_7_white.drawString(g, text[j],x,y+j*HSTRING+1, 0);
				else mFont.tahoma_7_blue.drawString(g, text[j],x,y+j*HSTRING+1, 0);
			}
			break;
		case t_TEXTEMO:
			for (int i = 0; i < listEmoLine.size(); i++) {
				EmoLineText emoline = (EmoLineText)listEmoLine.elementAt(i);
				switch (emoline.loai) {
				case t_TEXTEMO:
					int indexemo=0;
					for (int j = 0; j < emoline.listtext.Length; j++) {
						if(emoline.listtext[j].Equals(mtextConvertEmo))
						{
							EmoText emo = (EmoText)emoline.listEmo.elementAt(indexemo);
							try {
								indexemo++;
								g.drawImage(loadImageInterface.imgEmo[emo.loai],
										emoline.wlist[j]+x,
										y+i*HSTRING+1, 0);
							} catch (Exception e) {
								// TODO: handle exception
							//	e.printStackTrace();
							}
						}
						else {
							if(emoline.listtext[j]!=null&&!isMe){
								mFont.tahoma_7.drawString(g,emoline.listtext[j],emoline.wlist[j]+  x,y+i*HSTRING+1, 0);
							}
							else if(emoline.listtext[j]!=null&&isWorld)
							mFont.tahoma_7_white.drawString(g,emoline.listtext[j],emoline.wlist[j]+  x,y+i*HSTRING+1, 0);
							else if(emoline.listtext[j]!=null&&isMe)
								mFont.tahoma_7_blue.drawString(g,emoline.listtext[j],emoline.wlist[j]+  x,y+i*HSTRING+1, 0);
						}
					}
				break;
				case t_TEXT:
					if(emoline.listtext[0]!=null&&!isMe)
						mFont.tahoma_7.drawString(g,emoline.listtext[0],x,y+i*HSTRING+1,0);
					else if(emoline.listtext[0]!=null&&isWorld)
						mFont.tahoma_7_white.drawString(g,emoline.listtext[0],x,y+i*HSTRING+1,0);
					else if(emoline.listtext[0]!=null&&isMe)
						mFont.tahoma_7_blue.drawString(g,emoline.listtext[0],x,y+i*HSTRING+1,0);
					break;
				case t_EMO:
					for (int j = 0; j < emoline.listEmo.size(); j++) {
						EmoText emo = (EmoText)emoline.listEmo.elementAt(j);
						g.drawImage(loadImageInterface.imgEmo[emo.loai],x+
								(j*mGraphics.getImageWidth(loadImageInterface.imgEmo[0])),
								y+i*HSTRING+1, 0);
					}
					break;
				default:
					break;
				}
			}
			break;
		case t_EMO:
			for (int i = 0; i < listEmoLine.size(); i++) {
				EmoLineText emoline = (EmoLineText)listEmoLine.elementAt(i);
				for (int j = 0; j < emoline.listEmo.size(); j++) {
					EmoText emo = (EmoText)emoline.listEmo.elementAt(j);
					g.drawImage(loadImageInterface.imgEmo[emo.loai],
							x+wnode-emoline.listEmo.size()*mGraphics.getImageWidth(loadImageInterface.imgEmo[0])-HSTRING/2+(j*mGraphics.getImageWidth(loadImageInterface.imgEmo[0])),
							y+i*HSTRING+1, 0);
				}
			}
			break;
		default:
			break;
		}
		
	}
	 public  string replace(string _text, string _searchStr, string _replacementStr) {
	        // string buffer to store str
         System.Text.StringBuilder sb = new System.Text.StringBuilder();
          
	        try {
	        	 // Search for search
		        int searchStringPos = _text.IndexOf(_searchStr);
		        int startPos = 0;
		        int searchStringLength = _searchStr.Length;

		        // Iterate to add string
		        while (searchStringPos != -1) {
		            sb.Append(mSystem.substring(_text,startPos, searchStringPos)).Append(_replacementStr);
		            startPos = searchStringPos + searchStringLength;
		            searchStringPos = _text.IndexOf(_searchStr, startPos);
		        }

		        // Create string
		        sb.Append(mSystem.substring(_text,startPos,_text.Length));
			} catch (Exception e) {
				// TODO: handle exception
				//e.printStackTrace();
			}
	        return sb.ToString();
	    } 
	 
	/*
	 *  kiem tra ma icon
	 */
	public string[] slitFontWithEmo(string text,mVector nEmo){
		mVector listtext = new mVector();
		for (int i = 0; i < nEmo.size(); i++) {
			EmoText emo = (EmoText)nEmo.elementAt(i);
			
			text=Util.replace(text,maEmo[emo.loai],mtextConvertEmo+" ");
		}
		string[] mangcat = mFont.split(text,mtextConvertEmo);	

		if(mangcat==null) return null;
		else {
			int nemo = nEmo.size();
			for (int i = 0; i < mangcat.Length; i++) {
				if(!(i==0&&mangcat[i].Length==0)){
					listtext.addElement(mangcat[i]);
					if(nemo>0){
						nemo--;
						listtext.addElement(mtextConvertEmo);
					}
				}
				else {
					if(nemo>0){
						nemo--;
						listtext.addElement(mtextConvertEmo);
					}
				}
			}
		}
		string[] chuoitextt = new string[listtext.size()];
		for (int i = 0; i < listtext.size(); i++) {
			chuoitextt[i] = (string)listtext.elementAt(i);
		}
		return chuoitextt;
	}
	
	/*
	 * su dung de tim ma icon
	 */
	public mVector findAllEmoInText(string text,string maemo,int loaiemo){
		int index = 0;
		mVector nemo = new mVector();
		for (int i = 0; i < text.Length; i++) {
			if(text[i]==maemo[0]&&i+maemo.Length<=text.Length){
				bool isMa = true;
				try {
					for (int j = 0; j < maemo.Length; j++) {
						if(maemo[j]!=text[i+j]){
							isMa = false;
						}
					}
				} catch (Exception e) {
					//e.printStackTrace();
				}
				
				if(isMa){
					index = maemo.Length;
					nemo.addElement(new EmoText(loaiemo,i));
					i +=index-1;
				}
			}
		}
		return nemo;
	}
	
	/*
	 * sort emo
	 */
	public mVector XepThuTuEmo(mVector listEmo){
		int vitri=0,loai=0;
		for (int i = 0; i < listEmo.size()-1; i++) {
			for (int j = i+1; j < listEmo.size(); j++) {
				EmoText emoI =(EmoText)listEmo.elementAt(i);
				EmoText emoJ =(EmoText)listEmo.elementAt(j);
				if(emoI.vitri>emoJ.vitri){
					vitri = emoI.vitri;
					loai = emoI.loai;
					//
					emoI.loai = emoJ.loai;
					emoI.vitri = emoJ.vitri;
					
					emoJ.loai = loai;
					emoJ.vitri = vitri;
				}
				
			}
		}
		return listEmo;
	}
	
	/*
	 * Paint background text
	 */
	public void paintNodeChat(mGraphics g,int xmainTab, int ymainTab,int wmainTab, int hmainTab,int trai)
	{
		try {
			//0 trai 1 phai
			int loai = trai*7;
			int sizeTile = 8;
			int sizewT = (wmainTab-sizeTile*2)/sizeTile;
			int wthieu = (wmainTab-sizeTile*2)%sizeTile;
			int sizewL = (hmainTab-sizeTile*2)/sizeTile;

			int hthieu = (hmainTab-sizeTile*2)%sizeTile;
//			if(sizewL<=0 || hthieu<=0) return;
			
			for (int i = 0; i < sizewT; i++) {
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(1+loai), sizeTile, sizeTile, 0, xmainTab+sizeTile*(i+1), ymainTab, mGraphics.TOP|mGraphics.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(3+loai), sizeTile, sizeTile, 0, xmainTab+sizeTile*(i+1), ymainTab+hmainTab, mGraphics.BOTTOM|mGraphics.LEFT);
			}
			//2 duong dap vao cho du
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(1+loai), wthieu, sizeTile, 0, xmainTab+wmainTab-sizeTile, ymainTab, mGraphics.TOP|mGraphics.RIGHT);
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(3+loai), wthieu, sizeTile, 0, xmainTab+wmainTab-sizeTile, ymainTab+hmainTab, mGraphics.BOTTOM|mGraphics.RIGHT);
		
			
			
			
			for (int i = 0; i < sizewL; i++) {
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai),sizeTile,sizeTile, 0, xmainTab, ymainTab+sizeTile*(i+1), mGraphics.TOP|mGraphics.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai),sizeTile,sizeTile, 2,  xmainTab+wmainTab, ymainTab+sizeTile*(i+1), mGraphics.TOP|mGraphics.RIGHT);
			}
			//2 duong dap vao hang doc
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai),sizeTile, hthieu, 0,
					xmainTab,  ymainTab+sizeTile*(sizewL+1), mGraphics.TOP|mGraphics.LEFT);
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai), sizeTile,hthieu, 2,
					xmainTab+wmainTab,  ymainTab+sizeTile*(sizewL+1), mGraphics.TOP|mGraphics.RIGHT);
			for (int i = 0; i < sizewT; i++) {
				for (int j = 0; j < sizewL; j++) {
					g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(4+loai), sizeTile, sizeTile, 0, xmainTab+sizeTile*(i+1),ymainTab+sizeTile*(j+1), mGraphics.TOP|mGraphics.LEFT);	
				}
			}
			for (int i = 0; i < sizewL; i++) {
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(4+loai), wthieu, sizeTile, 0,
						xmainTab+wmainTab-sizeTile-wthieu, ymainTab+sizeTile*(i+1), mGraphics.TOP|mGraphics.LEFT);
			}
			for (int i = 0; i < sizewT; i++) {
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(4+loai), sizeTile, hthieu, 0,
						xmainTab+sizeTile*(i+1)          , ymainTab+hmainTab-sizeTile-hthieu, mGraphics.TOP|mGraphics.LEFT);
			}
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(4+loai), wthieu, hthieu, 0, xmainTab+wmainTab-sizeTile-wthieu, ymainTab+hmainTab-sizeTile-hthieu, mGraphics.TOP|mGraphics.LEFT);
				
			if(loai==0){
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(1+loai), sizeTile, sizeTile/2, 2, xmainTab, ymainTab, mGraphics.TOP|mGraphics.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai)+sizeTile/2, sizeTile, sizeTile/2, 0, xmainTab, ymainTab+sizeTile/2, mGraphics.TOP|mGraphics.LEFT);
			
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(1+loai), sizeTile, sizeTile/2, 2, xmainTab, ymainTab, mGraphics.TOP|mGraphics.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai)+sizeTile/2, sizeTile, sizeTile/2, 0, xmainTab, ymainTab+sizeTile/2, mGraphics.TOP|mGraphics.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(0+loai), sizeTile, sizeTile, 0, xmainTab-sizeTile, ymainTab, mGraphics.TOP|mGraphics.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(5+loai), sizeTile, sizeTile, 2, xmainTab+wmainTab, ymainTab+hmainTab, mGraphics.BOTTOM|mGraphics.RIGHT);
				
			}else {
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(1+loai), sizeTile, sizeTile/2, 1, xmainTab+wmainTab-sizeTile, ymainTab+hmainTab-sizeTile/2, mGraphics.TOP|mGraphics.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai)+sizeTile/2, sizeTile, sizeTile/2, 2, xmainTab+wmainTab-sizeTile, ymainTab+hmainTab-sizeTile, mGraphics.TOP|mGraphics.LEFT);
			
//				g.drawRegion(imgChat, 0, sizeTile*(1+loai), sizeTile, sizeTile/2, 2, xmainTab+wmainTab-sizeTile, ymainTab+hmainTab-sizeTile, mGraphics.TOP|mGraphics.LEFT,true);
//				g.drawRegion(imgChat, 0, sizeTile*(2+loai)+sizeTile/2, sizeTile, sizeTile/2, 0, xmainTab+wmainTab-sizeTile, ymainTab+hmainTab-sizeTile, mGraphics.TOP|mGraphics.LEFT,true);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(0+loai), sizeTile, sizeTile, 3, xmainTab+wmainTab, ymainTab+hmainTab-sizeTile, mGraphics.TOP|mGraphics.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(6+loai), sizeTile, sizeTile, 2, xmainTab, ymainTab, mGraphics.TOP|mGraphics.LEFT);
			}
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(6+loai), sizeTile, sizeTile, 0, xmainTab+wmainTab, ymainTab, mGraphics.TOP|mGraphics.RIGHT);
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(5+loai), sizeTile, sizeTile, 0, xmainTab, ymainTab+hmainTab, mGraphics.BOTTOM|mGraphics.LEFT);
			
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
		}
		
	}
}
