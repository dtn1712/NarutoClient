

public class Skill {

	public const sbyte ATT_STAND = 0;
	public const sbyte ATT_FLY = 1;

	public const sbyte SKILL_AUTO_USE = 0;
	public const sbyte SKILL_CLICK_USE_ATTACK = 1;
	public const sbyte SKILL_CLICK_USE_BUFF = 2;
	public const sbyte SKILL_CLICK_NPC		= 3;
	public const sbyte SKILL_CLICK_LIVE		= 4;

	public SkillTemplate template;
	public short skillId;
	public int point;
	public int level;
	public int coolDown;
	public long lastTimeUseThisSkill;
	public int dx;
	public int dy;
	public int maxFight;
	public int manaUse;
	public SkillOption[] options;
	public bool paintCanNotUseSkill = false;

	public string strTimeReplay() {
		if (coolDown % 1000 == 0)
			return coolDown / 1000 + "";
		int time = coolDown % 1000;
		
		return (coolDown / 1000) + "." + (time % 100 == 0 ? time / 100 : time / 10);
	}

	public void paint(int x, int y, mGraphics g) {
		
		SmallImage.drawSmallImage(g, template.iconId, x, y, Sprite.TRANS_NONE, StaticObj.VCENTER_HCENTER);
		long now = mSystem.currentTimeMillis();
		long d = now - lastTimeUseThisSkill;
		
		if (d < coolDown) {
			g.setColor(0x333333);
			if (paintCanNotUseSkill && GameCanvas.gameTick % 6 > 2)
				g.setColor(0x444444);
			int pxCooldown = (int) (d * 18 / coolDown);
			g.fillRect(x - 9, y - 9 + pxCooldown, 18, 18 - pxCooldown);
		} else
			// Làm ẩu, nhưng k sao
			paintCanNotUseSkill = false;
	}
	
	public bool isCooldown() {
		long now = mSystem.currentTimeMillis();
		long d = now - lastTimeUseThisSkill;
		if (d < coolDown)
			return true;
		else
			return false;
	}
}
