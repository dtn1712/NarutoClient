

public class Task {
	//loại nhiệm vụ
	public static sbyte DOING_TASK = 1;
	public static sbyte CAN_RECEIVE_TASK = 0;
	public static sbyte COMPLETE_TASK = 2;
	public static sbyte TYPE_DOING_TASK_IP = 2; // vận chuyển
	public static sbyte TYPE_DOING_TASK_COLLECT = 0;
	public static sbyte TYPE_DOING_TASK_SKILL = 1;
	public static sbyte TYPE_DOING_TASK_TALK = 3;
	//
	public int index, max;
	public short[] counts;
	public short taskId;
	public string[] names, details;
	public string[] subNames;
	public short count;
	
	
	public Task(short taskId, sbyte index, string name, string detail, string[] subNames, short[] counts, short count){
		this.taskId = taskId;
		this.index = index;
		this.names = mFont.tahoma_7b_white.splitFontArray(name, 155) ;
		this.details = mFont.tahoma_7.splitFontArray(detail, 155) ;
		this.subNames = subNames;
		this.counts = counts;
		this.count = count;
	}
}
