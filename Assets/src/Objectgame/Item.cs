

using System;
public class Item {

	// những item có option
	public const int TYPE_BODY_MIN = 0;
	public const int TYPE_BODY_MAX = 15;
	
//	public const int TYPE_VUKHI 				= 1;
//	public const int TYPE_LIEN 				= 3;
//	public const int TYPE_GANGTAY 			= 4;
//	public const int TYPE_QUAN 				= 23;
//	public const int TYPE_NGOCBOI			= 7;
//	public const int TYPE_PHU 				= 9;
//	public const int TYPE_THUNUOI			= 10;
//	public const int TYPE_MATNA				= 11;
//	public const int TYPE_AOCHOANG			= 12;
//	public const int TYPE_BAOTAY				= 13;
//	public const int TYPE_UNKNOW2			= 14;
//	public const int TYPE_BIKIP				= 15;
//	public const int TYPE_HP					= 16;
//	public const int TYPE_MP					= 17;
//	public const int TYPE_EAT				= 18;
//	public const int TYPE_MONEY 				= 19;
//	public const int TYPE_TUI_TIEN			= 20;
//	public const int TYPE_MEAT				= 21;
//	public const int TYPE_DRAGONBALL 		= 22; // ngọc rồng
//	public const int TYPE_TASK_SAVE 			= 23; // vật phẩm nhiệm vụ có lưu
//	public const int TYPE_TASK_WAIT 			= 24; // vật phẩm nhiệm vụ chờ
//	public const int TYPE_TASK 				= 25; // vật phẩm nhiệm vụ
//	public const int TYPE_CRYSTAL 			= 26; // huyền tinh
//	public const int TYPE_ORDER				= 27; // những thứ khác
//	public const int TYPE_PROTECT			= 28; // bảo hiểm
//	public const int TYPE_MON0 				= 29; // thú cưỡi
//	public const int TYPE_MON1 				= 30; // thú cưỡi
//	public const int TYPE_MON2 				= 31; // thú cưỡi
//	public const int TYPE_MON3 				= 32; // thú cưỡi
//	public const int TYPE_MON4 				= 33; // thú cưỡi
//	
	
	public const sbyte UI_WEAPON				= 2; //  giao diện binh khí
	public const sbyte UI_BAG					= 3; //  giao diện túi
	public const sbyte UI_BOX					= 4; //  giao diện rương
	public const sbyte UI_BODY 				= 5; //  giao diện nhân vật
	public const sbyte UI_STACK 				= 6; //  giao diện dược phẩm
	public const sbyte UI_STACK_LOCK 			= 7; //  giao diện dược phẩm khóa
	public const sbyte UI_GROCERY 			= 8; // giao diện tạp hóa
	public const sbyte UI_GROCERY_LOCK 		= 9; // giao diện tạp hóa khóa
	public const sbyte UI_UPGRADE 			= 10; // giao diện cường hóa
	public const sbyte UI_UPPEARL 			= 11; // giao diện ghép ngọc
	public const sbyte UI_UPPEARL_LOCK 		= -12; // giao diện ghép ngọc khoá
	public const sbyte UI_SPLIT 				= 13; // giao diện tách huyền tinh
	public const sbyte UI_STORE 				= 14; // giao diện cửa hàng
	public const sbyte UI_BOOK 				= 15; // giao diện quầy sách
	public const sbyte UI_LIEN				= 16;
	public const sbyte UI_NHAN				= 17;
	public const sbyte UI_NGOCBOI				= 18;
	public const sbyte UI_PHU					= 19;
	public const sbyte UI_NONNAM				= 20;
	public const sbyte UI_NONNU				= 21;
	public const sbyte UI_AONAM				= 22;
	public const sbyte UI_AONU				= 23;
	public const sbyte UI_GANGTAYNAM			= 24;
	public const sbyte UI_GANGTAYNU			= 25;
	public const sbyte UI_QUANNAM				= 26;
	public const sbyte UI_QUANNU				= 27;
	public const sbyte UI_GIAYNAM				= 28;
	public const sbyte UI_GIAYNU				= 29;
	public const sbyte UI_TRADE				= 30;
	public const sbyte UI_UPGRADE_GOLD 		= 31; // giao diện cường hóa có lượng
	public const sbyte UI_FASHION				= 32; // giao diện thời trang
	public const sbyte UI_CONVERT 			= 33; // giao diện hoán chuyển trang bị
	public const sbyte UI_CLANSHOP 			= 34; // giao diện đồ gia tộc
	public const sbyte UI_ELITES	 			= 35; // giao diện shop tinh tú
	public const sbyte UI_AUCTION_SALE		= 36; // bán vật phẩm
	public const sbyte UI_AUCTION_BUY	 		= 37; // mua vật phẩm
	public const sbyte UI_LUCKY_SPIN	 		= 38; // Quay số may mắn
	public const sbyte UI_CLAN	 			= 39; // Quay số may mắn
	public const sbyte UI_AUTO	 			= 40; // Quay số may mắn
	public const sbyte UI_MON    				= 41; //  giao diện thú cưỡi
	public const sbyte UI_NAP_GOOGLE   	= 42;
	public const sbyte UI_LUYEN_THACH  	= 43;
	public const sbyte UI_TINH_LUYEN_AO  	= 44;
	public const sbyte UI_TINH_LUYEN_THU 	= 45;

    private static sbyte[][] POS_WEARING = new sbyte[][]{
		 new sbyte[]  {Item.TYPE_NON},
		 new sbyte[]  {Item.TYPE_NGOC_BOI},
		 new sbyte[]  {Item.TYPE_DAY_CHUYEN},
		 new sbyte[]  {Item.TYPE_GANG},
		 new sbyte[]  {Item.TYPE_AO},
		  new sbyte[] {Item.TYPE_PHAT_TRAN,Item.TYPE_TRUONG,Item.TYPE_HOAN,Item.TYPE_QUYEN_DAU,Item.TYPE_KIEM,Item.TYPE_QUAT,Item.TYPE_BUA_CHU,Item.TYPE_SACH,Item.TYPE_THUONG,Item.TYPE_CUNG,Item.TYPE_DAO_NGAN,Item.TYPE_CHUY},
		  new sbyte[] {Item.TYPE_NHAN},// nhan trai
		 new sbyte[]  {Item.TYPE_DAI_LUNG},
		 new sbyte[]  {Item.TYPE_NHAN},// nhan phai
		 new sbyte[]  {Item.TYPE_GIAY},  
		 new sbyte[]  {Item.TYPE_BLOOD},// chua dùng 
		 new sbyte[]  {Item.TYPE_LEG},// chua dung
		 new sbyte[]  {Item.TYPE_OTHER},// chua dung
		 };
	
	 public static int getPosWearingItem(int type){
		  for(int i=0;i<POS_WEARING.Length;i++){
		   for(int j=0;j<POS_WEARING[i].Length;j++){
		    if(POS_WEARING[i][j]==type)
		     return i;
		   }
		  }
		  return -1;// item ko hợp lệ
		 }
	
	 public const sbyte TYPE_BUA_CHU=0;
	 public const sbyte TYPE_SACH=1;
	 public const sbyte TYPE_TRUONG=2;
	 public const sbyte TYPE_PHAT_TRAN=3;
	 public const sbyte TYPE_KIEM=4;
	 public const sbyte TYPE_CUNG=5;
	 public const sbyte TYPE_QUYEN_DAU=6;
	 public const sbyte TYPE_HOAN=7;
	 public const sbyte TYPE_THUONG=8;
	 public const sbyte TYPE_CHUY=9;
	 public const sbyte TYPE_DAO_NGAN=10;
	 public const sbyte TYPE_QUAT=11;
	 
	 public const sbyte TYPE_AO=12;
	 public const sbyte TYPE_GANG=13;
	 public const sbyte TYPE_GIAY=14;
	 public const sbyte TYPE_DAY_CHUYEN=15;
	 public const sbyte TYPE_NHAN=16;
	 public const sbyte TYPE_DAI_LUNG=17;
	 public const sbyte TYPE_NGOC_BOI=18;
	 public const sbyte TYPE_NON = 19;
	 public const sbyte TYPE_BLOOD=22;
	 public const sbyte TYPE_BLOOD_LOW = 24;
     public const sbyte TYPE_MP = 25;
	 public const sbyte TYPE_LEG=23;
	 public const sbyte TYPE_OTHER=24;
	public ItemTemplate template;
	public mVector options;
	public int itemId;
	public int playerId;
    public int indexUI;
    public int quantity, value;
	public long expires;
	public bool isLock;
	public int sys; // hệ
	public int upgrade; // cường hóa
	//public int tinhluyen;
	public int buyCoin, buyCoinLock, buyGold, buyGoldLock, saleCoinLock;
	public int typeUI;
	public bool isExpires;
	public EffectCharPaint eff;
	public int indexEff;
	public mBitmap img;
	

	
	public Item clone(){
		Item item = new Item();
		item.template = this.template;
		if(options != null){
			item.options = new mVector();
			for (int i = 0; i < options.size(); i++) {
				ItemOption option = new ItemOption();
				option.optionTemplate = ((ItemOption)options.elementAt(i)).optionTemplate;
				option.param =  ((ItemOption)options.elementAt(i)).param;
				item.options.addElement(option);
			}
		}
		item.itemId = this.itemId;
		item.playerId = this.playerId;
		item.indexUI = this.indexUI;
		item.quantity = this.quantity;
		item.expires = this.expires;
		item.isLock = this.isLock;
		item.sys = this.sys; // hệ
		item.upgrade = this.upgrade; // cường hóa
		//item.tinhluyen = this.tinhluyen;
		item.buyCoin = this.buyCoin;
		item.buyCoinLock = this.buyCoinLock;
		item.buyGold = this.buyGold;
		item.buyGoldLock = this.buyGoldLock;
		item.saleCoinLock = this.saleCoinLock;
		item.typeUI = this.typeUI;
		item.isExpires = this.isExpires;
		return item;
	}
	
	public Item viewNext(int up){
		Item item = clone();
		item.isLock = true;
		int delta = up - item.upgrade;
		if(delta == 0)
			return item;
		item.upgrade = up;
		if(item.options != null){
			for (int i = 0; i < item.options.size(); i++) {
				ItemOption option = (ItemOption) item.options.elementAt(i);
				if(option.optionTemplate.id == 6 || option.optionTemplate.id == 7)
					option.param += 15 * delta;
				else if(option.optionTemplate.id == 8 
						|| option.optionTemplate.id == 9
						|| option.optionTemplate.id == 19)
					option.param += 10 * delta;
				else if(option.optionTemplate.id == 10
						|| option.optionTemplate.id == 11
						|| option.optionTemplate.id == 12
						|| option.optionTemplate.id == 13
						|| option.optionTemplate.id == 14
						|| option.optionTemplate.id == 15
						|| option.optionTemplate.id == 17
						|| option.optionTemplate.id == 18
						|| option.optionTemplate.id == 20)
					option.param += 5 * delta;
				else if(option.optionTemplate.id == 21
						|| option.optionTemplate.id == 22
						|| option.optionTemplate.id == 23
						|| option.optionTemplate.id == 24
						|| option.optionTemplate.id == 25
						|| option.optionTemplate.id == 26)
					option.param += 150 * delta;
				else if(option.optionTemplate.id == 16)
					option.param += 3 * delta;
			}
		}
		return item;
	}
	
	public bool isTypeBody(){
		if(TYPE_BODY_MIN <= template.type && template.type <= TYPE_BODY_MAX)
			return true;
		return false;
	}
	
//	public bool isTypeMounts(){
//		if(TYPE_MON0 <= template.type && template.type <= TYPE_MON4)
//			return true;
//		return false;
//	}
	
	
	public string getLockString(){
		return isLock?mResources.LOCKED:mResources.NOLOCK;
	}
	
//	public string getUpgradeString(){
//		if(template.type == Item.TYPE_AOCHOANG)
//			return mResources.SPECUPGRADE;
//		if(template.level < 10 || template.type >= 10)
//			return mResources.NOTUPGRADE;
//		if(upgrade == 0)
//			return mResources.NOUPGRADE;
//		return null;
//	}
	
	public string getExpiresString(){
		if(expires <= 0)
			return mResources.FOREVER;
        TimeZone localZone = TimeZone.CurrentTimeZone;
        DateTime currentDate = DateTime.Now;
        //Calendar cal = Calendar.getInstance();
        //cal.setTimeZone(TimeZone.CurrentTimeZone. getTimeZone("GMT+7"));
        //cal.setTime(new Date(expires));
        int h = currentDate.Hour;//cal.get(Calendar.HOUR_OF_DAY);
        int m = currentDate.Minute; ;//cal.get(Calendar.MINUTE);
        int day = currentDate.Day;//cal.get(Calendar.DAY_OF_MONTH);
        int month = currentDate.Month+/*cal.get(Calendar.MONTH) +*/ 1;
        int year = currentDate.Year;//cal.get(Calendar.YEAR);
		return day + "/" + month + "/" + year + " " + h + "h" + m + "'";
	}
	
	public string getExpiresShopString(){
		if(expires <= 0)
			return mResources.FOREVER;
		if(expires/1000 >= 2592000)
			return expires/1000/2592000 + " " + mResources.MONTH; 
		else if(expires/1000 >= 604800)
			return expires/1000/604800 + " " + mResources.WEEK;
		else if(expires/1000 >= 86400)
			return expires/1000/86400 + " " + mResources.DAY;
		else if(expires/1000 >= 3600)
			return expires/1000/3600 + " " + mResources.HOUR;
		else if(expires/1000 >= 60)
			return expires/1000/60 + " " + mResources.MINUTE;
		return "";
	}
//	public void clearExpire() {
//		if (!isTypeMounts())
//			expires = 0;
//	}
	
	public bool isTypeUIMe(){
		if(typeUI == Item.UI_BODY || typeUI == Item.UI_BAG || typeUI == Item.UI_BOX || typeUI == Item.UI_CLAN)
			return true;
		return false;
	}
	public bool isTypeUIShopView(){
		if(isTypeUIShop())
			return true;
		if(isTypeUIStore() || isTypeUIBook() || isTypeUIFashion() || isTypeUIClanShop())
			return true;
		return false;
	}
	public bool isTypeUIShop(){
		if(typeUI == Item.UI_NONNAM || typeUI == Item.UI_NONNU
				|| typeUI == Item.UI_AONAM || typeUI == Item.UI_AONU
				|| typeUI == Item.UI_GANGTAYNAM || typeUI == Item.UI_GANGTAYNU
				|| typeUI == Item.UI_QUANNAM || typeUI == Item.UI_QUANNU
				|| typeUI == Item.UI_GIAYNAM || typeUI == Item.UI_GIAYNU
				|| typeUI == Item.UI_LIEN || typeUI == Item.UI_NHAN 
				|| typeUI == Item.UI_NGOCBOI || typeUI == Item.UI_PHU 
				|| typeUI == Item.UI_WEAPON || typeUI == Item.UI_STACK 
				|| typeUI == Item.UI_GROCERY
				|| typeUI == Item.UI_CLANSHOP)
			return true;
		return false;
	}
	public bool isTypeUIShopLock(){
		if(typeUI == Item.UI_STACK_LOCK || typeUI == Item.UI_GROCERY_LOCK)
			return true;
		return false;
	}
	public bool isTypeUIStore(){
		if(typeUI == Item.UI_STORE)
			return true;
		return false;
	}
	public bool isTypeUIBook(){
		if(typeUI == Item.UI_BOOK)
			return true;
		return false;
	}
	public bool isTypeUIFashion(){
		if(typeUI == Item.UI_FASHION)
			return true;
		return false;
	}
	public bool isTypeUIClanShop(){
		if(typeUI == Item.UI_CLANSHOP)
			return true;
		return false;
	}
	public bool isTypeUIClan(){
		if(typeUI == Item.UI_CLAN)
			return true;
		return false;
	}
	public bool isUpMax(){
		if(getUpMax() == upgrade)
			return true;
		return false;
	}
	public int getUpMax(){
		if(template.level >= 1 && template.level < 20)
			return 4;
		else if(template.level >= 20 && template.level < 40)
			return 8;
		else if(template.level >= 40 && template.level < 50)
			return 12;
		else if(template.level >= 50 && template.level < 60)
			return 14;
		else
			return 16;
	}
	
//	public bool isTypeClothe(){
//		if(template.type == Item.TYPE_NON
//				|| template.type == Item.TYPE_AO
//				|| template.type == Item.TYPE_GANGTAY
//				|| template.type == Item.TYPE_QUAN
//				|| template.type == Item.TYPE_GIAY)
//			return true;
//		return false;
//	}
//	public bool isTypeAdorn(){
//		if(template.type == Item.TYPE_LIEN
//				|| template.type == Item.TYPE_NHAN
//				|| template.type == Item.TYPE_NGOCBOI
//				|| template.type == Item.TYPE_PHU)
//			return true;
//		return false;
//	}
//	public bool isTypeStack(){
//		if(expires == -1 && (template.type == Item.TYPE_HP || template.type == Item.TYPE_MP))
//			return true;
//		return false;
//	}
//	public bool isTypeCrystal(){
//		if(template.type == Item.TYPE_CRYSTAL)
//			return true;
//		return false;
//	}
//	public bool isTypeWeapon(){
//		if(template.type == Item.TYPE_VUKHI)
//			return true;
//		return false;
//	}
//	
//	public bool isItemClass0(){
//		if(itemId == 194 || itemId == 94 || itemId == 95 || itemId == 96 || itemId == 97 || itemId == 98)
//			return true;
//		return false;
//	}
//	public bool isItemClass1(){
//		if(itemId == 194 || itemId == 94 || itemId == 95 || itemId == 96 || itemId == 97 || itemId == 98)
//			return true;
//		return false;
//	}
//	public bool isItemClass2(){
//		if(itemId == 99 || itemId == 100 || itemId == 101 || itemId == 102 || itemId == 103)
//			return true;
//		return false;
//	}
//	public bool isItemClass3(){
//		if(itemId == 104 || itemId == 105 || itemId == 106 || itemId == 107 || itemId == 108)
//			return true;
//		return false;
//	}
//	public bool isItemClass4(){
//		if(itemId == 109 || itemId == 110 || itemId == 111 || itemId == 112 || itemId == 113)
//			return true;
//		return false;
//	}
//	public bool isItemClass5(){
//		if(itemId == 114 || itemId == 115 || itemId == 116 || itemId == 117 || itemId == 118)
//			return true;
//		return false;
//	}
//	public bool isItemClass6(){
//		if(itemId == 119 || itemId == 120 || itemId == 121 || itemId == 122 || itemId == 123)
//			return true;
//		return false;
//	}
    public static bool isBlood(int type)
    {
        if (type == Item.TYPE_BLOOD || type == Item.TYPE_BLOOD_LOW)
            return true;
        return false;
    }
    public static bool isMP(int type)
    {
        if (type == Item.TYPE_MP)
            return true;
        return false;
    }

    public void paintItem(mGraphics g, int x, int y)
    {
        //		if(Char.myChar().arrItemBag != null){
        //			for(int i = 0; i < item.length; i++){
        //				System.out.println(i+ " item ---> "+item[i].template.iconID);
        //				Item it = item[i];
        //				System.out.println(" <---- it ---> "+this.template.iconID);
        if (this.template != null)
        {
            SmallImage.drawSmallImage(g, this.template.iconID, x, y, 0, mGraphics.VCENTER | mGraphics.HCENTER, true);
        }
        if (quantity > 1 && (Item.isBlood(this.template.type)
                 || Item.isMP(this.template.type)))
        {
            mFont.tahoma_7_white.drawString(g, quantity + "", x + 12, y + 2, 1);
        }
        //			}

        //		}
        //		item = getItem(Char.myChar().arrItemBag);

    }

}
