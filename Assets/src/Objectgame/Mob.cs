

using System;
public class Mob : IMapObject {

    public int idloadimage;
    public int ybong;
	public const sbyte TYPE_DUNG = 0;
	public const sbyte TYPE_DI = 1;
	public const sbyte TYPE_NHAY = 2;
	public const sbyte TYPE_LET = 3;
	public const sbyte TYPE_BAY = 4;
	public const sbyte TYPE_BAY_DAU = 5;

    public static mHashtable imgMob = new mHashtable();
    public static MobTemplate[] arrMobTemplate;
    public static mBitmap[][] imgMobTem;

	// ========================
	public const sbyte MA_INHELL = 0; // trạng thái lần đầu tiên
	public const sbyte MA_DEADFLY = 1; // chết
	public const sbyte MA_STANDWAIT = 2; // nghỉ
	public const sbyte MA_ATTACK = 3; // đánh trái
	public const sbyte MA_STANDFLY = 4; // đậu khi bay
	public const sbyte MA_WALK = 5;// bay
	public const sbyte MA_FALL = 6;// rớt
	public const sbyte MA_INJURE = 7;// bị thương
    public const sbyte type_MOCNHAN = 43;
	// ========================
	// public const sbyte MA_MOVE_LEFT = 5;
	// public const sbyte MA_MOVE_RIGHT = 6;
	// public const sbyte MA_MOVE_UP = 7;
	// public const sbyte MA_MOVE_DOWN = 8;

	// FLYING TEXT
	public string flyString;
    public int flyx, flyy, flyIndex;
    public int indexRequestImg = 0;
    public bool isRequest;

	// mobP1: bien dem;
	// mobF: loai quai

	// mobCharIndex:
	public int hp, maxHp=1, x, y, frame, dir = 1, dirV = 1, status, p1, p2, p3, xFirst, yFirst, vy, exp, w, h, hpInjure, charIndex, timeStatus;
	public short mobId;
	public bool isx, isy, isDisable, isDontMove, isFire, isIce, isWind;
	public mVector vMobMove = new mVector();
	public bool isGo;
	public string mobName;
	public int templateId;
	public short pointx, pointy;
	public Char cFocus;
	public int dame, dameMp;
	public int sys, typeAtt;
	public sbyte levelBoss, level;
	public bool isBoss;
	private long timeStartDie = 0;
	private int frameIndex = 0;

    public sbyte idEffectAttack;
    public bool isGetInfo = false;
	public int minX, minY, maxX, maxY;
	
	public Mob(byte mobId, bool isDisable, bool isDontMove, bool isFire, bool isIce, bool isWind, int templateId, int sys, int hp, sbyte level, int maxp, short pointx, short pointy, sbyte status, sbyte levelBoss, bool isBos) {
		this.isDisable = isDisable;
		this.isDontMove = isDontMove;
		this.isFire = isFire;
		this.isIce = isIce;
		this.isWind = isWind;
		this.sys = sys;
		this.mobId = mobId;
		this.templateId = templateId;
		this.hp = hp;
		this.level = level;
		this.xFirst = this.x = this.pointx = pointx;
		this.yFirst = this.y = this.pointy = pointy;
		this.status = status;
		this.maxHp = maxp;
		this.levelBoss = levelBoss;
        this.isBoss = isBos;
        this.idEffectAttack = (sbyte)CRes.random(5);
//		if (arrMobTemplate[templateId].imgs == null) {
//			arrMobTemplate[templateId].imgs = new Bitmap[0];
////			Service.gI().requestModTemplate(templateId);
//		}
	}
	public Mob(sbyte mobId, int hp, sbyte level, int maxhp, short x, short y,
			short minX, short minY, short maxX, short maxY) { // test thôi chưa
                y -= 10;															// đủ
		this.mobId = mobId;
		this.hp = hp;
		this.level = level;
		this.maxHp = maxhp;
		this.x = x;
		xFirst = x;
//		this.x = 175;
//		Cout.println("XXXXXXXXX MOD ----> "+x);
		this.y = y;
		yFirst = y;
//		this.y = 395;
		Cout.println(mobId+"  xxx  "+x+ "YYYYYYYYY MOD -----> "+y);
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
		this.dir = 1;
		this.dir = (CRes.random(2)%2==0?-1:1);
		status = MA_WALK;
        this.idEffectAttack = (sbyte)CRes.random(5);
        if (arrMobTemplate[templateId].imgs == null)
        {
            arrMobTemplate[templateId].imgs = new mBitmap[8];
            //			Service.gI().requestModTemplate(templateId);
        }
	}
    public mBitmap createImage(int id, int sub)
    {
        if (imgMobTem == null) imgMobTem = new mBitmap[100][];
        //		mBitmap img = (mBitmap)imgMob.get(id+"");
        if (imgMobTem[id] == null)
        {
            imgMobTem[id] = new mBitmap[7];
            //			if(img!=null){
            //				imgMobTem[id] = img;
            ////				imgMob.put(id+"", img);
            //			}else{
            //				//request serrver
            //			}
        }

        //		Cout.println(id+" load  imgmmmmm   "+sub+" imgggg "+(imgMobTem[id][sub]));
        if (sub < imgMobTem[id].Length && imgMobTem[id][sub] == null)
        {

            string path = "/mob/" + (id + SmallImage.ID_ADD_MOB) + "/_" + (sub + 1) + ".png";
            string pathRequest = "/mob/" + (id + SmallImage.ID_ADD_MOB);
            imgMobTem[id][sub] = GameCanvas.loadImage(path);
            if (imgMobTem[id][sub] == null)
            {
                string path2 = "/mob/" + (id) + "/" + (sub + 1) + ".png";
                imgMobTem[id][sub] = GameCanvas.loadImage(path2);
            }
            if (imgMobTem[id][sub] == null && imgMob.get(pathRequest) == null)
            {
                sbyte[] data = Rms.loadRMS(mSystem.getPathRMS(SmallImage.getPathImage((id + SmallImage.ID_ADD_MOB))) + "" + ((id + SmallImage.ID_ADD_MOB) + "/_" + (sub + 1)));
                if (data == null)
                {
                    imgMob.put(pathRequest, "" + 0);
                    Service.gI().requestImage(id + SmallImage.ID_ADD_MOB, (sbyte)(1));
                }
            }

        }
        else if (imgMobTem[id][sub] == null)
        {
            return null;
        }
        return imgMobTem[id][sub];
    }
    public static void cleanImg()
    {
        //		Enumeration k = imgMob.keys();
        //		mVector vkey = new mVector();
        //	   while (k.hasMoreElements()) {
        //		   String key = (String) k.nextElement();
        //		   vkey.addElement(key);
        //			
        //	   }
        //		for (int i = 0; i < vkey.size(); i++) {
        //			   String key = (String) vkey.elementAt(i);
        //			   mBitmap img = (mBitmap)imgMob.get(key);
        //				if(img!=null){
        //					try {
        //						if(img.image!=null)
        //						img.cleanImg();
        //						img.image = null;
        //					} catch (Exception e) {
        //						// TODO: handle exception
        //					}
        //					imgMob.remove(key); 
        //				}
        //		   }
        //		imgMob.clear();
        if (imgMobTem != null)
            for (int i = 0; i < imgMobTem.Length; i++)
            {
                if (imgMobTem[i] != null)
                {
                    for (int j = 0; j < imgMobTem[i].Length; j++)
                    {
                        if (imgMobTem[i][j] != null)
                            imgMobTem[i][j].cleanImg();
                        imgMobTem[i][j] = null;
                    }
                }
            }
    }
	public void update() {
        //		if(GameCanvas.gameTick%39==0)
        //			setAttack(Char.myChar());
        //		for(int i = 0; i < GameScr.vMob.size(); i++){
        //			Mob mobCheck  = (Mob) GameScr.vMob.elementAt(i);
        //			if(mobCheck.arrMobTemplate[mobCheck.mobId].imgs[0] == null){
        //				for (int k = 0; k < mobCheck.arrMobTemplate[mobCheck.mobId].imgs.length; k++) {
        //					if(mobCheck.arrMobTemplate[mobCheck.mobId].imgs[k]==null)
        //					mobCheck.arrMobTemplate[mobCheck.mobId].imgs[k] = GameCanvas.loadImage("/mob/"+mobCheck.mobId+"/"+k+".png"); 
        ////					Cout.println(("/mob/"+mobCheck.mobId+"/"+k+".png")+" load "+mobCheck.arrMobTemplate[mobCheck.mobId].imgs[k]);
        //				}
        //			}
        //		}
        //		status = Mob.MA_ATTACK;
        //		cFocus = Char.myChar();
        if (arrMobTemplate[mobId] == null)
        {
            arrMobTemplate[mobId] = new MobTemplate();
        }
        else
        {
            if (arrMobTemplate[mobId] != null && arrMobTemplate[mobId].imgs != null && arrMobTemplate[mobId].imgs[(GameCanvas.gameTick / 2) % 7] == null)
            {
                try
                {

                    indexRequestImg = (indexRequestImg + 1) % 1000;
                    if (indexRequestImg % 4 == 0)
                    {
                        for (int i = 0; i < arrMobTemplate[mobId].imgs.Length; i++)
                        {
                            if (arrMobTemplate[mobId].imgs[i] == null)
                            {
                                if (idloadimage == type_MOCNHAN && i > 0 && i < 3)//moc nhan
                                    arrMobTemplate[mobId].imgs[i] = arrMobTemplate[mobId].imgs[0];
                                else if (idloadimage == type_MOCNHAN && i > 3)//moc nhan
                                    arrMobTemplate[mobId].imgs[i] = arrMobTemplate[mobId].imgs[3];
                                else
                                {
                                    arrMobTemplate[mobId].imgs[i] = createImage(idloadimage, i);//GameCanvas.loadImage("/mob/"+idloadimage+"/"+(i+1)+".png"); 
                                }
                            }
                            if (arrMobTemplate != null && arrMobTemplate[mobId] != null && arrMobTemplate[mobId].imgs[i] != null)
                            {
                                h = mGraphics.getImageHeight(arrMobTemplate[mobId].imgs[i]);
                                w = mGraphics.getImageWidth(arrMobTemplate[mobId].imgs[i]);
                            }
                            //							
                        }
                    }

                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    //					Cout.println(arrMobTemplate+"  arrMobTemplate[mobId]  "+arrMobTemplate[mobId]);
                    //					Cout.println("  arrMobTemplate[mobId] 2 "+arrMobTemplate[mobId].imgs);
                }

            }
        }
        //		if (!isUpdate())
        //			return;
        //
        //
        //		if (vMobMove == null && arrMobTemplate[templateId].rangeMove != 0)
        //			return;
        if (status != MA_ATTACK && isBusyAttackSomeOne)
        {
            if (cFocus != null)
            {

                cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
                cFocus = null;
            }
            isBusyAttackSomeOne = false;
        }
        updateShadown();
        //		Cout.println("UPDATE STATUS ---> "+status);
        switch (status)
        {

            case MA_DEADFLY:
                //			isDisable = false;
                //			isDontMove = false;
                //			isFire = false;
                //			isIce = false;
                //			isWind = false;
                //			if (templateId != 98 && templateId != 99) {
                p1++;

                y += p1;
                if (GameCanvas.gameTick % 2 == 0)
                {
                    if (p2 > 1)
                        p2--;
                    else if (p2 < -1)
                        p2++;
                }
                x += p2;
                frame = isBoss ? 10 : 2;
                if (y > GameScr.gssye * 24 || x < GameScr.gssx * 24 || x > GameScr.gssxe * 24)
                {
                    p1 = 0;
                    p2 = 0;
                    x = TileMap.pxw + 100 + getW(); y = TileMap.pxh + 100 + getH();
                    hp = getTemplate().hp;
                    status = MA_INHELL;
                    frame = 0;
                    timeStatus = 0;
                    return;
                }

                if (p3 == 0 && (TileMap.tileTypeAtPixel(x, y) & TileMap.T_TOP) == TileMap.T_TOP)
                {
                    p1 = p1 > 4 ? -4 : -p1;
                    p3 = 16;
                }
                if (p3 > 0)
                    p3--;


                break;
            case MA_STANDWAIT:
                timeStatus = 0;
                updateMobStandWait();
                break;
            case MA_STANDFLY:
                timeStatus = 0;
                frame = 0;
                p1++;
                if (p1 > 40 + mobId % 5)
                {
                    y -= 2;
                    status = MA_WALK;
                    p1 = 0;
                }
                break;
            case MA_ATTACK:
                updateMobAttack();
                break;
            case MA_WALK:
                try
                {
                    if (GameCanvas.gameTick % 4 == 0 && isBoss)
                    {
                        frameIndex++;
                        if (frameIndex > arrMobTemplate[templateId].frameBossMove.Length - 1)
                            frameIndex = 0;
                    }
                }
                catch (Exception e)
                {
                }
                timeStatus = 0;
                updateMobWalk();
                break;
            case MA_FALL:
                timeStatus = 0;
                p1++;
                y += p1;
                if (y >= yFirst)
                {
                    y = yFirst;
                    p1 = 0;
                    status = MA_WALK;
                }
                break;
            case MA_INJURE:
                updateInjure();
                break;
        }
        if (mobToAttack != null)
        {
            int dx = mobToAttack.x - x;
            int dy = mobToAttack.y - y;
            x += dx / 4;
            y += dy / 4;
            if (mobToAttack.status == MA_DEADFLY || mobToAttack.status == MA_INHELL || (Res.abs(dx) < 20 && Res.abs(dy) < 20))
            {
                ServerEffect.addServerEffect(59, mobToAttack.x, mobToAttack.y, 1);
                mobToAttack = null;
            }
        }
    }

	public void setInjure() {
		if (hp > 0) {
			f = 0;
			timeStatus = 12;
			status = MA_INJURE;
		}
	}
	public bool isBusyAttackSomeOne=true;
	public void setAttack(Char cFocus) {
		isBusyAttackSomeOne = true;
		this.cFocus = cFocus;
		p1 = 0;
        p2 = 0;
        f = 0;
		status = MA_ATTACK;
		frameIndex = 0;
		typeAtt = 0;
	}

//	public void setAttack(BuNhin bFocus) {
//		this.bFocus = bFocus;
//		p1 = 0;
//		p2 = 0;
//		status = MA_ATTACK;
//		typeAtt = 1;
//	}

	private void updateInjure() {
        frame = isBoss ? (getTemplate().mobTemplateId == 139) ? 4 : (getTemplate().mobTemplateId == 160) ? 12 : 10 : 2;
        f++;
        if (f == frameBeAttack.Length / 2)
            ServerEffect.addServerEffect(25, x, y - h / 2 - 10, 1);
        frame = frameBeAttack[f % frameBeAttack.Length];
        if (getTemplate().mobTemplateId == 141)
            frame = 13;
        timeStatus--;
        if (timeStatus <= 0)
        {
            if ((injureBy != null && injureThenDie) || hp == 0)
            {
                //Cout.println("------MA_DEADFLY---------"+mobId);
                status = Mob.MA_DEADFLY;
                p2 = injureBy.cdir << 3;
                p1 = -5;
                p3 = 0;
            }
            else
            {
                status = MA_WALK;
                if (injureBy != null)
                {
                    dir = -injureBy.cdir;
                    if (Res.abs(x - injureBy.cx) < 24)
                        status = MA_STANDWAIT;
                }
                p1 = p2 = p3 = 0;
                timeStatus = 0;
            }
            injureBy = null;
            return;
        }
        if (arrMobTemplate[templateId].type != 0)
        {
            int fallBack = -injureBy.cdir << 1;
            if (x > xFirst - arrMobTemplate[templateId].rangeMove && x < xFirst + arrMobTemplate[templateId].rangeMove)
                x -= fallBack;
        }
    }

	private void updateMobStandWait() {
		switch (arrMobTemplate[templateId].type) {
		case TYPE_DUNG:
		case TYPE_DI:
		case TYPE_NHAY:
		case TYPE_LET:
			frame = 0;
			p1++;
			if (p1 > 10 + mobId % 10) {
				status = MA_WALK;
			}
			if(isBoss)
				frame = GameCanvas.gameTick % 101 > 1 ? 0 : 1;
			break;
		case TYPE_BAY:
		case TYPE_BAY_DAU:
			frame = GameCanvas.gameTick % 4 > 1 ? 0 : 1;
			p1++;
			if (p1 > mobId % 3) {
				status = MA_WALK;
			}
			break;
		}

	}
	
	private void updateMobAttack() {
        f++;
        if (f < 2)
        {
            GameScr.addEffectKillMobAttack(this, cFocus, EffectKill.EFF_NORMAL, idEffectAttack);
        }
		if(f>=frameAttack.Length) 
			status = Mob.MA_WALK;
		frame = frameAttack[f%frameAttack.Length];
		if (p1 == 0) {
			int xx = 0, yy = 0;
			if (typeAtt == 0) {
				xx = cFocus.cx;
				yy = cFocus.cy;
			} 
//			else if (typeAtt == 1) {
//				xx = bFocus.x;
//				yy = bFocus.y;
//			}
			if (Res.abs(xx - x) < 24 || Res.abs(xx - x) < 5 || arrMobTemplate[templateId].type == 0)
				frame = arrMobTemplate[templateId].imgs.Length == 3 ? 0 : 3;
			
			if(isBoss && (Res.abs(xx - x) < 48 || Res.abs(xx - x) < 10 || arrMobTemplate[templateId].type == 0)) //level boss
				frame = arrMobTemplate[templateId].imgs.Length == 3 ? 0 : 3;
			
			if(isBoss ){
				frameIndex++;
				if((Res.abs(xx - x) < 48 || Res.abs(yy - y) < 10)){
					if(frameIndex >= arrMobTemplate[templateId].frameBossAttack[0].Length)
						frameIndex = 0;
					frame = arrMobTemplate[templateId].frameBossAttack[0][frameIndex];
				}
				else{
					if(frameIndex >= arrMobTemplate[templateId].frameBossAttack[1].Length)
						frameIndex = 0;
					frame = arrMobTemplate[templateId].frameBossAttack[1][frameIndex];
				}
			}
			if(frame == 3 )
				p1 = 1;
			
			if (arrMobTemplate[templateId].type != 0 && !isDontMove && isIce && isWind)
				x += (xx - x) / 3;
			
			if (x > xFirst + arrMobTemplate[templateId].rangeMove) {
				p1 = 1;
			}
			
			if (x < xFirst - arrMobTemplate[templateId].rangeMove) {
				p1 = 1;
			}
			
			if ((arrMobTemplate[templateId].type == 4 || arrMobTemplate[templateId].type == 5)&& !isDontMove)
				y += (yy - y) / 20;
			p2++;
			if (( isBoss && Res.abs(xx - x) < 48 && Res.abs(yy - y) < 15) || (Res.abs(xx - x) < 12 && Res.abs(yy - y) < 12) || p2 > 12 || p1 == 1) {
				p1 = 1;
				if (typeAtt == 0) {
					if(isBoss && Res.abs(xx - x) < 48 && Res.abs(yy - y) < 15){
					//59, 52
						cFocus.doInjure(dame, dameMp,isBoss, getTemplate().mobTemplateId);
						isBusyAttackSomeOne = false;
						if( getTemplate().mobTemplateId == 114) // Boss Th�?
							ServerEffect.addServerEffect(79, cFocus, 3);
						else if( getTemplate().mobTemplateId == 115){ // Boss Rồng
//							if(cFocus == Char.myChar())
//								GameScr.shaking = 1;
//								GameScr.count = 0;
							ServerEffect.addServerEffect(81, cFocus.cx, yFirst + TileMap.size, 2);
							ServerEffect.addServerEffect(81, cFocus.cx - 40, yFirst + TileMap.size, 2);
							ServerEffect.addServerEffect(81, cFocus.cx + 40, yFirst + TileMap.size, 2);
						}
						else if( getTemplate().mobTemplateId == 116){ // Boss Robo xanh
//							if(cFocus == Char.myChar()){
//								GameScr.shaking = 1;
//								GameScr.count = 0;
//							}
							if(cFocus.cx > x)
								ServerEffect.addServerEffect(86, x, y - h / 2 + 5, 1);
							else
								ServerEffect.addServerEffect(88, x, y - h / 2 + 5, 1);
							ServerEffect.addServerEffect(87,cFocus.cx, cFocus.cy - cFocus.ch/2 , 2);
							ServerEffect.addServerEffect(87, cFocus.cx - 40, cFocus.cy - cFocus.ch/2, 2);
							ServerEffect.addServerEffect(87, cFocus.cx + 40, cFocus.cy - cFocus.ch/2, 2);
						}else if(getTemplate().mobTemplateId == 138){ //Sumo : 138, Deamon: 139
							if (cFocus.cx > x)
								ServerEffect.addServerEffect(89, x + w/2 , y - h / 2 - 5, 1);
							else
								ServerEffect.addServerEffect(89, x - w/2 , y - h / 2 - 5, 1, (sbyte)-1);
							ServerEffect.addServerEffect(90, cFocus, 2);
						}else if(getTemplate().mobTemplateId == 139){ //Sumo : 138, Deamon: 139
//							if(cFocus == Char.myChar()){
//								GameScr.shaking = 1;
//								GameScr.count = 0;
//							}
							ServerEffect.addServerEffect(91, cFocus, 2);
						}else if(getTemplate().mobTemplateId == 140 || getTemplate().mobTemplateId == 161){ //Rồng băng : 140
//							if(cFocus == Char.myChar()){
//								GameScr.shaking = 1;
//								GameScr.count = 0;
//							}
							//ServerEffect.addServerEffect(109, cFocus, 2);
							ServerEffect.addServerEffect(112, cFocus, 2);
							ServerEffect.addServerEffect(109, cFocus.cx - 40, cFocus.cy - 40, 1);
							ServerEffect.addServerEffect(109, cFocus.cx + 40, cFocus.cy - 40, 1);
							ServerEffect.addServerEffect(109, cFocus.cx - 20, cFocus.cy, 2);
							ServerEffect.addServerEffect(109, cFocus.cx + 20, cFocus.cy, 2);
						} else if (getTemplate().mobTemplateId == 141 || getTemplate().mobTemplateId == 162) { // Kỳ lân
//							if (cFocus == Char.myChar()) {
//								GameScr.shaking = 1;
//								GameScr.count = 0;
//							}
							// ServerEffect.addServerEffect(109, cFocus, 2);
							if (cFocus.cx > x)
								ServerEffect.addServerEffect(108, x + w / 2, y, 1);
							else
								ServerEffect.addServerEffect(108, x - w / 2, y, 1, (sbyte) -1);
							ServerEffect.addServerEffect(122, x, y, 1, (sbyte)dir);
							ServerEffect.addServerEffect(91, cFocus, 1);
						} else if (getTemplate().mobTemplateId == 144 || getTemplate().mobTemplateId == 163) { // Bí Ngô bay
//							if(cFocus == Char.myChar()){
//								GameScr.shaking = 1;
//								GameScr.count = 0;
//							}
							//ServerEffect.addServerEffect(109, cFocus, 2);
							ServerEffect.addServerEffect(112, cFocus, 2);
							ServerEffect.addServerEffect(109, cFocus.cx - 40, cFocus.cy - 40, 1);
							ServerEffect.addServerEffect(109, cFocus.cx + 40, cFocus.cy - 40, 1);
							ServerEffect.addServerEffect(109, cFocus.cx - 20, cFocus.cy, 2);
							ServerEffect.addServerEffect(109, cFocus.cx + 20, cFocus.cy, 2);
						} else if (getTemplate().mobTemplateId == 160) { // Bí Ngô Bự
							if (cFocus.cx > x)
								ServerEffect.addServerEffect(123, x + w / 2, y - 5, 1);
							else
								ServerEffect.addServerEffect(123, x - w / 2, y - 5, 1, (sbyte) -1);
							ServerEffect.addServerEffect(91, cFocus, 1);
						} else if (getTemplate().mobTemplateId == 164 || getTemplate().mobTemplateId == 165) { // Người băng
							if (cFocus.cx > x)
								ServerEffect.addServerEffect(125, x + w / 2, y, 1);
							else
								ServerEffect.addServerEffect(125, x - w / 2, y, 1, (sbyte) -1);
							ServerEffect.addServerEffect(90, cFocus, 1);
						}
						
						
						
					}
					else if (Res.abs(xx - x) < 24 && Res.abs(yy - y) < 15){
						cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
						isBusyAttackSomeOne = false;
					}else
					{
						if(isBoss){
//							if(getTemplate().mobTemplateId == 114 || getTemplate().mobTemplateId == 115)
//								MonsterDart.addMonsterDart(x + (dir - 1)* 15, y - 20, isBoss, levelBoss, getTemplate().mobTemplateId, dame, dameMp, cFocus);
//							else 
								if(getTemplate().mobTemplateId == 116){
								ServerEffect.addServerEffect(84, cFocus, 2);
								isBusyAttackSomeOne = false;
								cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
							}else if(getTemplate().mobTemplateId == 138){
//								if(cFocus == Char.myChar())
//								{
//									GameScr.shaking = 1;
//									GameScr.count = 0;
//								}
								ServerEffect.addServerEffect(83, cFocus, 2);
								isBusyAttackSomeOne = false;
								cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
							}
//							else if(getTemplate().mobTemplateId == 139){
//								MonsterDart.addMonsterDart(x + (dir - 1)* 30, y - 30, isBoss, levelBoss, getTemplate().mobTemplateId, dame, dameMp, cFocus);
//							}
							else if(getTemplate().mobTemplateId == 140 || getTemplate().mobTemplateId == 161){
//								if(cFocus == Char.myChar()){
//									GameScr.shaking = 1;
//									GameScr.count = 0;
//								}
								ServerEffect.addServerEffect(110, cFocus, 2);
								ServerEffect.addServerEffect(104, cFocus.cx - 20, cFocus.cy, 2);
								ServerEffect.addServerEffect(104, cFocus.cx + 20, cFocus.cy, 2);
								isBusyAttackSomeOne = false;
								cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
							}else if(getTemplate().mobTemplateId == 141 || getTemplate().mobTemplateId == 162){
//								if(cFocus == Char.myChar()){
//									GameScr.shaking = 1;
//									GameScr.count = 0;
//								}
								
								ServerEffect.addServerEffect(121, cFocus, 1);
								isBusyAttackSomeOne = false;
								cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
							}
//							else if(getTemplate().mobTemplateId == 144 || getTemplate().mobTemplateId == 163){
//								MonsterDart.addMonsterDart(x + (dir - 1)* 15, y - 20, isBoss, levelBoss, getTemplate().mobTemplateId, dame, dameMp, cFocus);
//							}
							else if(getTemplate().mobTemplateId == 160){
								ServerEffect.addServerEffect(124, cFocus, 2);
								isBusyAttackSomeOne = false;
								cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
							}else if(getTemplate().mobTemplateId == 164 || getTemplate().mobTemplateId == 165){
								ServerEffect.addServerEffect(126, cFocus, 1);
								isBusyAttackSomeOne = false;
								cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
							}				 										
						}
//						else
//							MonsterDart.addMonsterDart(x - 5, y + dir * 10,  isBoss, levelBoss, getTemplate().mobTemplateId, dame, dameMp, cFocus);
						isBusyAttackSomeOne = false;
					}
				} else if (typeAtt == 1) {
					if (Res.abs(xx - x) < 24 && Res.abs(yy - y) < 15)
					{
						isBusyAttackSomeOne = false;
					}
					else
					{
//						if(isBoss)
//							MonsterDart.addMonsterDart(x - 5, y + dir * 10 - 20, bFocus);
//						else
//							MonsterDart.addMonsterDart(x - 5, y + dir * 10, bFocus);
						isBusyAttackSomeOne = false;
					}
				}
			}
			dir = x < xx ? 1 : -1;
		} else if (p1 == 1) {
			if (arrMobTemplate[templateId].type != 0 && !isDontMove && !isIce && !isWind) {
				x += (xFirst - x) / 4;
				y += (yFirst - y) / 4;
			}
			if (Res.abs(xFirst - x) < 5 && Res.abs(yFirst - y) < 5) {
				status = MA_STANDWAIT;
				p1 = 0;
				p2 = 0;
			}
		}
	}

	public void updateMobWalk() {
        if (templateId > arrMobTemplate.Length - 1) return;
        if (idloadimage != type_MOCNHAN)
        {
            arrMobTemplate[templateId].type = 3;
            arrMobTemplate[templateId].speed = 1;
            arrMobTemplate[templateId].rangeMove = 20;
        }
        //		if(true)
        //			return;
        int line = 0;
        try
        {
            if (injureThenDie)
            {
                status = Mob.MA_DEADFLY;
                Cout.println("------MA_DEADFLY----2-------" + mobId);
                p2 = injureBy.cdir << 3;
                p1 = -5;
                p3 = 0;
            }
            line = 1;
			if(isIce)
				return;
			if(isDontMove || isWind){

				Cout.println(isDontMove+" isUpdate   "+status+"  isWind "+isWind);
				frame = 0;
				return;
			}
			switch (arrMobTemplate[templateId].type) {
			case TYPE_DUNG: // dung yen
				line = 2;
				frame = 0;
				break;
			case TYPE_DI:
			case TYPE_NHAY:
			case TYPE_LET:
//				dir = 1;
				line = 3;
				sbyte speed = arrMobTemplate[templateId].speed;
				if (speed == 1) {
					if (GameCanvas.gameTick % 2 == 1)
						break;
				} else if (speed > 2)
					speed += (sbyte)(mobId % 2);
				else if (GameCanvas.gameTick % 2 == 1)
					speed--;
				x += speed * dir;
				if (x > xFirst + arrMobTemplate[templateId].rangeMove)
					dir = -1;
				else if (x < xFirst - arrMobTemplate[templateId].rangeMove)
					dir = 1;
				if(!isBoss){
					updatechangeframe();
					frame = framemove[f];
					
				}
//					frame = GameCanvas.gameTick % 4 > 1 ? 1 : 2;
				else{
					frame =  arrMobTemplate[templateId].frameBossMove[frameIndex];
				}
				break;
			case TYPE_BAY:
				line = 4;
				sbyte speed2 = arrMobTemplate[templateId].speed;
				speed2 += (sbyte)(mobId % 2);
				x += speed2 * dir;
				if (GameCanvas.gameTick % 10 > 2)
					y += (speed2) * dirV;
				speed2 += (sbyte)((GameCanvas.gameTick + mobId) % 2);
				if (x > xFirst + arrMobTemplate[templateId].rangeMove) {
					dir = -1;
					status = MA_STANDWAIT;
					p1 = 0;
				} else if (x < xFirst - arrMobTemplate[templateId].rangeMove) {
					dir = 1;
					status = MA_STANDWAIT;
					p1 = 0;
				}
				if (y > yFirst + 24)
					dirV = -1;
				else if (y < yFirst - (20 + GameCanvas.gameTick % 10))
					dirV = 1;
				if(!isBoss)
					frame = GameCanvas.gameTick % 4 > 1 ? 0 : 1;
				else
					frame =  arrMobTemplate[templateId].frameBossMove[frameIndex];
				
				break;
			case TYPE_BAY_DAU:
				line = 5;
				sbyte speed3 = arrMobTemplate[templateId].speed;
				speed3 += (sbyte)(mobId % 2);
				x += speed3 * dir;
				speed3 += (sbyte)((GameCanvas.gameTick + mobId) % 2);
				if (GameCanvas.gameTick % 10 > 2)
					y += (speed3) * dirV;

				if (x > xFirst + arrMobTemplate[templateId].rangeMove) {
					dir = -1;
					status = MA_STANDWAIT;
					p1 = 0;
				} else if (x < xFirst - arrMobTemplate[templateId].rangeMove) {
					dir = 1;
					status = MA_STANDWAIT;
					p1 = 0;
				}
				if (y > yFirst + 24)
					dirV = -1;
				else if (y < yFirst - (20 + GameCanvas.gameTick % 10))
					dirV = 1;
				if (TileMap.tileTypeAt(x, y, TileMap.T_TOP)) {
					if (GameCanvas.gameTick % 10 > 5) {
						y = TileMap.tileYofPixel(y);
						status = MA_STANDFLY;
						p1 = 0;
						dirV = -1;
					} else
						dirV = -1;
				}
				if(!isBoss)
					frame = GameCanvas.gameTick % 4 > 1 ? 3 : 1;
				else
					frame =  arrMobTemplate[templateId].frameBossMove[frameIndex];
				break;
			}
		} catch (Exception e) {
//			System.out.println("lineee: " + line);
		}
	}

	public MobTemplate getTemplate() {
		return arrMobTemplate[templateId];
	}

	public bool isPaint() {
		if (x < GameScr.cmx)
			return false;
		if (x > GameScr.cmx + GameScr.gW)
			return false;
		if (y < GameScr.cmy)
			return false;
		if (y > GameScr.cmy + GameScr.gH + 30)
			return false;
		if (arrMobTemplate[templateId] == null)
			return false;
		if(!isBoss){
			if (frame >= arrMobTemplate[templateId].imgs.Length)
				return false;
			if (arrMobTemplate[templateId].imgs[frame] == null)
				return false;
		}
		if (status == MA_INHELL)
			return false;
		return true;
	}

	public bool isUpdate() {
		if (arrMobTemplate[templateId] == null){
			return false;
		}
		if (arrMobTemplate[templateId].imgs == null){
			return false;
		}
		if(!isBoss){
			if (frame >= arrMobTemplate[templateId].imgs.Length){
				return false;
			}
			if (arrMobTemplate[templateId].imgs[frame] == null){
				return false;
			}
		}
		if (status == MA_INHELL){
			return false;
		}
		return true;
	}
	public void paint(mGraphics g) {
        //		if (!isPaint())
        //			return;

        //		g.translate(0, GameCanvas.transY);
        if (status != MA_INHELL)
        {
            paintShadow(g);
            MobTemplate mt = arrMobTemplate[mobId];
            if (mt.imgs[frame] != null)
            {
                if (CRes.checkCollider(x,
                        x + mt.imgs[frame].getWidth(), GameScr.cmx, GameScr.cmx + GameCanvas.w, y,
                        y + mt.imgs[frame].getHeight(), GameScr.cmy, GameScr.cmy + GameCanvas.h))
                {
                    g.drawRegion(mt.imgs[frame], 0, 0, mGraphics.getImageWidth(mt.imgs[frame]), mGraphics.getImageHeight(mt.imgs[frame]), dir > 0 ? 2 : 0, x, y - mGraphics.getImageHeight(mt.imgs[frame]) / 2, mGraphics.VCENTER | mGraphics.HCENTER);
                }
            }
        }
        //		mFont.tahoma_7_yellow.drawString(g, mobId + "", x, y - h - 20, 2, mFont.tahoma_7_grey);
        //		if(!isBoss){
        //			if (w == 0)
        //				w = mGraphics.getImageWidth(mt.imgs[0]);
        //			if (h == 0)
        //				h = mGraphics.getImageHeight(mt.imgs[0]);
        //		}else{
        //			w = 40;
        //			h = 40;
        //		}
        //		try {
        //			if ((templateId == 98 || templateId == 99) && status == MA_DEADFLY) {
        //					long now = System.currentTimeMillis();
        //					if(now-timeStartDie < 400){
        //						g.drawRegion(mt.imgs[frame], 0, 0, mGraphics.getImageWidth(mt.imgs[frame]), mGraphics.getImageHeight(mt.imgs[frame]), dir > 0 ? 0 : 2, x, y, StaticObj.BOTTOM_HCENTER);
        //					} else if (now - timeStartDie < 800){
        //						g.drawRegion(mt.imgs[frame], 0, 0, mGraphics.getImageWidth(mt.imgs[frame]), 3*mGraphics.getImageHeight(mt.imgs[frame])/5, dir > 0 ? 0 : 2, x, y, StaticObj.BOTTOM_HCENTER);
        //					} else if (now - timeStartDie < 1200){
        //						g.drawRegion(mt.imgs[frame], 0, 0, mGraphics.getImageWidth(mt.imgs[frame]), mGraphics.getImageHeight(mt.imgs[frame])/3, dir > 0 ? 0 : 2, x, y, StaticObj.BOTTOM_HCENTER);
        //					}
        //					if (GameCanvas.gameTick % 8 < 2)
        //						SmallImage.drawSmallImage(g, 457, x, y, 0, StaticObj.BOTTOM_HCENTER);
        //					else if (GameCanvas.gameTick % 8 < 4)
        //						SmallImage.drawSmallImage(g, 458, x, y, 0, StaticObj.BOTTOM_HCENTER);
        //					else if (GameCanvas.gameTick % 8 < 6)
        //						SmallImage.drawSmallImage(g, 459, x, y, 0, StaticObj.BOTTOM_HCENTER);
        //					
        //			} else
        //				if(isBoss){
        //					if(getTemplate().frameBoss!=null){
        //						Frame fr = getTemplate().frameBoss[frame];
        //						for (int i = 0; i < fr.dx.length; i++) {
        //							ImageInfo im = getTemplate().getImgInfo(fr.idImg[i]);
        //
        //							if(dir > 0)
        //								g.drawRegion(getTemplate().imgs[0], im.x0, im.y0, im.w, im.h, 0, x + fr.dx[i], y + fr.dy[i] - 1 , mGraphics.TOP | mGraphics.LEFT);
        //							else{
        //								g.drawRegion(getTemplate().imgs[0], im.x0, im.y0, im.w, im.h, 2, x - fr.dx[i], y + fr.dy[i] - 1, mGraphics.TOP | mGraphics.RIGHT);
        //							}
        //						}
        //					}
        //				}
        //				else{
        ////					g.translate(0, GameCanvas.transY);
        //					Cout.println("IMage ----> "+mt.imgs[0]);
        //					g.drawRegion(mt.imgs[0/*frame*/], 0, 0, mGraphics.getImageWidth(mt.imgs[frame]), mGraphics.getImageHeight(mt.imgs[frame]), dir > 0 ? 0 : 2, x, y, StaticObj.BOTTOM_HCENTER);
        //				}
        ////			
        //			
        //		} catch (Exception e) {
        ////			GameCanvas.debug("MobPaint " + templateId + " " + frame + " " + mt.imgs.length + " " + mt.imgs[frame], 1);
        //		}
        //		
        //		int yy = y;
        //		if (Char.myChar().mobFocus != null && Char.myChar().mobFocus.equals(this) && status != MA_DEADFLY) {
        //			int mx = maxHp;
        //			if (mx < hp)
        //				mx = hp;
        //			int per = (int) (((long) hp * 100) / mx);
        //			int ww = w;
        //			int hh = 4;
        //			if (levelBoss == 1 || levelBoss == 2 || levelBoss == 3 || isBoss) {
        //				hh += hh / 2;
        //				ww += ww / 2;
        //			}
        //			ww += 2;
        //			int hpw = (ww * per) / 100;
        //			if (hpw < 2)
        //				hpw = 2;
        //			if (status == MA_DEADFLY)
        //				per = 0;
        //
        //			if(templateId == 140 || templateId == 160)
        //				yy -= 20;
        //			
        //			if(templateId != 142 && templateId != 143){
        //				g.setColor(0xFFFFFF);
        //				g.fillRect(x - ww / 2 - 1, yy - h - 12, ww, hh);
        //				g.setColor(getHPColor());
        //				g.fillRect(x - ww / 2 - 1, yy - h - 12, hpw, hh);
        //				g.setColor(0);
        //				g.drawRect(x - ww / 2 - 1, yy - h - 12, ww, hh);
        //			}
        //			else{
        //				SmallImage.drawSmallImage(g, 988, x, yy - h, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
        //			}
        //			if (levelBoss > 0) {
        //				if (levelBoss == 1)
        //					mFont.tahoma_7_yellow.drawString(g, mResources.BOSS[levelBoss], x, yy - h - 26, 2, mFont.tahoma_7_grey);
        //				else if (levelBoss == 2)
        //					mFont.tahoma_7_yellow.drawString(g, mResources.BOSS[levelBoss], x, yy - h - 26, 2, mFont.tahoma_7_grey);
        //				else if (levelBoss == 3)
        //					mFont.tahoma_7_blue1.drawString(g, mResources.BOSS[levelBoss], x, yy - h - 26, 2, mFont.tahoma_7_grey);
        //				if (isDisable)
        //					SmallImage.drawSmallImage(g, 494, x, yy - h - 28, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
        //			} else if (isDisable) {
        //				SmallImage.drawSmallImage(g, 494, x, yy - h - 15, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
        //			}
        //		} else if (levelBoss > 0) {
        //			if (levelBoss == 1)
        //				mFont.tahoma_7_yellow.drawString(g, mResources.BOSS[levelBoss], x, yy - h - 20, 2, mFont.tahoma_7_grey);
        //			else if (levelBoss == 2)
        //				mFont.tahoma_7_yellow.drawString(g, mResources.BOSS[levelBoss], x, yy - h - 20, 2, mFont.tahoma_7_grey);
        //			else if (levelBoss == 3)
        //				mFont.tahoma_7_blue1.drawString(g, mResources.BOSS[levelBoss], x, yy - h - 20, 2, mFont.tahoma_7_grey);
        //			if (isDisable)
        //				SmallImage.drawSmallImage(g, 494, x, yy - h - 22, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
        //		} else if (isDisable) {
        //			SmallImage.drawSmallImage(g, 494, x, y - h - 5, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
        //		}
        ////		mFont.tahoma_7_yellow.drawString(g, mobId + "", x, y - h - 20, 2, mFont.tahoma_7_grey);
        //		if(isDontMove){
        //			if(GameCanvas.gameTick % 2 == 0)
        //				SmallImage.drawSmallImage(g, 1082, x, y - h/2, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else
        //				SmallImage.drawSmallImage(g, 1084, x, y - h/2, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //		}
        //		if(isIce){
        //			SmallImage.drawSmallImage(g, 290, x, y, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
        //		}
        //		if(isWind){
        //			int type = GameCanvas.gameTick % 6;
        //			if(type == 0 || type == 1)
        //				SmallImage.drawSmallImage(g, 998, x, y - h - 5, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 2 || type == 3)
        //				SmallImage.drawSmallImage(g, 999, x, y - h - 5, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 4 || type == 5)
        //				SmallImage.drawSmallImage(g, 1000, x, y - h - 5, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //		}
        //		if(isFire){
        //			int type = GameCanvas.gameTick % 16;
        //			if(type == 0)
        //				SmallImage.drawSmallImage(g, 1013, x - w/2, y - h + h/4, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 1)
        //				SmallImage.drawSmallImage(g, 1014, x - w/2, y - h + h/4, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 2)
        //				SmallImage.drawSmallImage(g, 1015, x - w/2, y - h + h/4, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 3)
        //				SmallImage.drawSmallImage(g, 1016, x - w/2, y - h + h/4, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 4)
        //				SmallImage.drawSmallImage(g, 1013, x + w/2, y - h, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 5)
        //				SmallImage.drawSmallImage(g, 1014, x + w/2, y - h, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 6)
        //				SmallImage.drawSmallImage(g, 1015, x + w/2, y - h, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 7)
        //				SmallImage.drawSmallImage(g, 1016, x + w/2, y - h, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 8)
        //				SmallImage.drawSmallImage(g, 1013, x - w/2, y, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 9)
        //				SmallImage.drawSmallImage(g, 1014, x - w/2, y, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 10)
        //				SmallImage.drawSmallImage(g, 1015, x - w/2, y, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 11)
        //				SmallImage.drawSmallImage(g, 1016, x - w/2, y, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 12)
        //				SmallImage.drawSmallImage(g, 1013, x + w/2, y - h/4, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 13)
        //				SmallImage.drawSmallImage(g, 1014, x + w/2, y - h/4, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 14)
        //				SmallImage.drawSmallImage(g, 1015, x + w/2, y - h/4, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if(type == 15)
        //				SmallImage.drawSmallImage(g, 1016, x + w/2, y - h/4, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //		}
    }

	public int getHPColor() {
		if (sys <= 1)
			return 0xFF0000;
		else if (sys == 2)
			return 0x0080FF;
		else if (sys == 3)
			return 0x719563;
		return 0xFF0000;
	}

	sbyte[] cou = new sbyte[] { -1, 1 };

	public Char injureBy;

	public bool injureThenDie = false;

	public void startDie() {
        hp = 0;
        //		this.timeStartDie = System.currentTimeMillis();
        //		if (injureBy != null) {
        //			injureThenDie = true;
        //		} else {
        injureThenDie = true;
        hp = 0;
        status = Mob.MA_DEADFLY;
        p1 = -5;
        p2 = -dir << 3;
        p3 = 0;
        //		}
    }

	public Mob mobToAttack;

	public void attackOtherMob(Mob mobToAttack) {
		this.mobToAttack = mobToAttack;
	}
	
	public void attackOtherInRange() {
		if (templateId == 116)
			ServerEffect.addServerEffect(84, Char.myChar(), 1);
		else if (templateId == 115)
			ServerEffect.addServerEffect(81, Char.myChar(), 1);
		else if (templateId == 138)
			ServerEffect.addServerEffect(90, Char.myChar(), 1);
		else if (templateId == 139)
			ServerEffect.addServerEffect(91, Char.myChar(), 1);
		else if (templateId == 140 || templateId == 161)
			ServerEffect.addServerEffect(110, Char.myChar(), 2);
		else if (templateId == 141 || templateId == 162)
			ServerEffect.addServerEffect(121, Char.myChar(), 1);
		else if (templateId == 144 || templateId == 163)
			ServerEffect.addServerEffect(121, Char.myChar(), 1);
		else if (templateId == 160)
			ServerEffect.addServerEffect(124, Char.myChar(), 1);
		else if(templateId == 164 || templateId == 165)
				ServerEffect.addServerEffect(126, cFocus, 1);
		
	}
	public  int getX() {
		// TODO Auto-generated method stub
		return 0;
	}
	public  int getY() {
		// TODO Auto-generated method stub
		return 0;
	}
	public   int getW() {
		// TODO Auto-generated method stub
        return w;
	}
    public  int getH()
    {
		// TODO Auto-generated method stub
		return h;
	}
	public  void stopMoving() {
		// TODO Auto-generated method stub
		
	}
	public  bool isInvisible() {
		// TODO Auto-generated method stub
		return false;
	}


    public static void loadImgMob(int idtemplateMob)
    {
        //		if(idtemplateMob>arrMobTemplate.length-1) return;
        //		arrMobTemplate[idtemplateMob] = new MobTemplate();
        //		for(int i = 0; i < 8; i++){
        //			arrMobTemplate[idtemplateMob].imgs[i] = GameCanvas.loadImage("/mob/"+idtemplateMob+"/"+i+".png"); 
        //		}
    }
	public static int[] framemove =     {0,0,0,1,1,1,2,2,2};
	public static int[] frameBeAttack = {4,4,4,4,6,6,6,6,4,4,4,4};
	public static int[]  frameAttack=   {3,3,3,3,4,4,4,4,5,5,5,5};
	 public int f = 1; 
	 public void updatechangeframe(){
	   f++;
	  if(f > framemove.Length-1){
	   f = 0;
	  }
	 }
	
	public void loadDatafile(){	
		
	}
		public void paintShadow(mGraphics g) {
		int size = TileMap.size;
		if (TileMap.tileTypeAt(x + size / 2, ybong + 1, TileMap.T_LEFT)) {
			g.setClip((x / size) * size, ((ybong - 30) / size) * size, size,
					100);
		} else if (TileMap
				.tileTypeAt((x - size / 2) / size, (ybong + 1) / size) == 0) {
			g
					.setClip((x / size) * size, ((ybong - 30) / size) * size,
							100, 100);
		} else if (TileMap
				.tileTypeAt((x + size / 2) / size, (ybong + 1) / size) == 0) {
			g.setClip((x / size) * size, ((ybong - 30) / size) * size, size,
					100);
		} else if (TileMap.tileTypeAt(x - size / 2, ybong + 1, TileMap.T_RIGHT)) {
			g.setClip((x / 24) * size, ((ybong - 30) / size) * size, size, 100);
		}

		g.drawImage(loadImageInterface.bongChar, x,
				ybong-2, mGraphics.VCENTER|mGraphics.HCENTER);
		g.setClip(GameScr.cmx, GameScr.cmy - GameCanvas.transY, GameScr.gW,
				GameScr.gH + 2 * GameCanvas.transY);
	}
	public void updateShadown() {
		  int wCount = 0;
		  // if (ySd <= 0)
		  // return;
		  if (TileMap.tileTypeAt(x, y, TileMap.T_TOP)) {

		   ybong = y;
		   return;
		  } else {

			  ybong = y;
		   while (wCount < 30) {
		    wCount++;
		    ybong += 24;

		    if (TileMap.tileTypeAt(x, ybong, TileMap.T_TOP)) {
		     if (ybong % 24 != 0) {
//		    	 ybong -= (ybong % 24);
		     }
		     break;
		    }
		   }
		  }

		 }
	public void ResetToDie(){

		injureThenDie = true;
		hp = 0;
		status = Mob.MA_INHELL;
		x = xFirst;
		y = yFirst;
	}
}
