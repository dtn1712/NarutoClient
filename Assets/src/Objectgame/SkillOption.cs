


public class SkillOption {
	public int param;
	public SkillOptionTemplate optionTemplate;
	public string optionString;
	
	public string getOptionString(){
		if(optionString == null){
			if(optionTemplate.id == 62 || optionTemplate.id == 64){
				float value = param / 1000f;
				optionString = Util.replace(optionTemplate.name, "#", value + "");
			} 
			else 
				optionString = Util.replace(optionTemplate.name, "#", param + "");
		}
		return optionString;
	}
	
}

