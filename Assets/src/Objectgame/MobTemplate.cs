

public class MobTemplate {
    public sbyte rangeMove, speed, type;
    public short mobTemplateId, idloadimage;
	public int hp;
	public string name;
    public mBitmap[] imgs = new mBitmap[8];
	public ImageInfo[] imginfo;
	public Frame[] frameBoss;
	public sbyte[] frameBossMove;
	public sbyte[][] frameBossAttack;
	
	public ImageInfo getImgInfo(sbyte frame){
		return imginfo[frame];
	}

}
