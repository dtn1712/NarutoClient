

using System;
public class Npc : Char , IActionListener {

	public const sbyte BINH_KHI = 0;
	public const sbyte PHONG_CU = 1;
	public const sbyte TRANG_SUC = 2;
	public const sbyte DUOC_PHAM = 3;
	public const sbyte TAP_HOA = 4;
	public const sbyte THU_KHO = 5;
	public const sbyte DA_LUYEN = 6;
	public const sbyte XA_PHU_LANG = 7;
	public const sbyte XA_PHU_TRUONG = 8;
	public const sbyte CHU_NHIEM_HOA = 9;
	public const sbyte CHU_NHIEM_THUY = 10;
	public const sbyte CHU_NHIEM_GIO = 11;
	public const sbyte TRUONG_LANG_TONE = 12;
	public const sbyte KHU_VUC = 13;
	public const sbyte GIAO_THU_1 = 14;
	public const sbyte GIAO_THU_2 = 15;
	public const sbyte GIAO_THU_3 = 16;
	public const sbyte CHAU_BA_REI = 17;
	public const sbyte TRUONG_LANG_CHAI = 18;
	public const sbyte TRUONG_LANG_KOJIN = 19;
	public const sbyte TRUONG_LANG_CHAKUMI = 20;
	public const sbyte TRUONG_LANG_ECHIGO = 21;
	public const sbyte TRUONG_LANG_SANZU = 22;
	public const sbyte TRUONG_LANG_OSHIN = 23;
	public const sbyte TIEN_TRANG = 24;
	public const sbyte DAU_TRUONG = 25;
	public const sbyte THUONG_NHAN = 26;
	public const sbyte TRU_CO_QUAN = 27;
	public const sbyte KY_TRAN = 28;

	public NpcTemplate template;
	public int npcId;
	public sbyte typeNV = -1;
	public bool isFocus = true;//dg có nv khong
	public static mHashtable arrNpcTemplate = new mHashtable();
	public int sys;
	public short avatar;

//	public Npc(int npcId, int status, int cx, int cy, int templateId) {
//		this.npcId = npcId;
//
//		this.cx = cx;
//		this.cy = cy;
//		this.statusMe = status;
//		template = Npc.arrNpcTemplate[templateId];
//	}
	
	public Npc(short cx, short cy, short templateId, short avatar) {
		Cout.println("NPC: "+cx+" :: "+cy+" :: "+templateId+" ::: "+avatar);
		this.npcId = npcId;
		this.avatar = avatar;
		this.cx = cx;
		this.cy = cy-20;
		template = (NpcTemplate)Npc.arrNpcTemplate.get(templateId+"");
	//	Cout.println(getClass(), templateId+ " npc  "+template);
	}

	public static void clearEffTask() {
		for (int i = 0; i < GameScr.vNpc.size(); i++) {
			Npc npc = (Npc) GameScr.vNpc.elementAt(i);
			npc.effTask = null;
			npc.indexEffTask = -1;
		}
	}

	

	public void paintHead(mGraphics g) {

		Part ph = GameScr.parts[template.headId];
		if (cdir == 1)
			SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, GameCanvas.w - 31 - g.getTranslateX(), 2 - g.getTranslateY(), 0, 0);
		else
			SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, GameCanvas.w - 31 - g.getTranslateX(), 2 - g.getTranslateY(), 2, 24);
	}

	public override void paint(mGraphics g) {
        //		cy-=20;
        if (!isPaint()) return;
        if (statusMe == A_HIDE)
            return;
        if (cTypePk != PK_NORMAL)
        {
            base.paint(g);
            return;
        }
        if (template == null)
            return;
        if (template.typeKhu != -1)
            g.drawImage(loadImageInterface.bongChar, cx,
                    cy + 8, mGraphics.VCENTER | mGraphics.HCENTER);
        //		if (template.npcTemplateId == 13) {
        //			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
        //				SmallImage.drawSmallImage(g, 988, cx, cy - ch - 1, 0, Graphics.BOTTOM | Graphics.HCENTER);
        //			}
        //			SmallImage.drawSmallImage(g, 1060, cx, cy, 0, Graphics.BOTTOM | Graphics.HCENTER);
        //			mFont.tahoma_7_white.drawString(g, TileMap.zoneID + "", cx, cy - 10 - 2 - mFont.tahoma_7.getHeight(), mFont.CENTER);
        //		}else if (template.npcTemplateId == 31) {
        //			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
        //				SmallImage.drawSmallImage(g, 988, cx, cy - ch - 1, 0, Graphics.BOTTOM | Graphics.HCENTER);
        //			}
        //			SmallImage.drawSmallImage(g, 1291, cx, cy, 0, Graphics.BOTTOM | Graphics.HCENTER);
        //		}
        //		else if (template.npcTemplateId == 27) {
        //			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
        //				SmallImage.drawSmallImage(g, 988, cx, cy - ch - 1, 0, Graphics.BOTTOM | Graphics.HCENTER);
        //			}
        //			SmallImage.drawSmallImage(g, 1224, cx, cy, 0, Graphics.BOTTOM | Graphics.HCENTER);
        //		} else {
        try
        {
            if (template.typeKhu == -1)
            {
                g.drawImage(loadImageInterface.khu, cx, cy - loadImageInterface.khu.getHeight(), mGraphics.VCENTER | mGraphics.HCENTER);
                mFont.tahoma_7.drawString(g, (TileMap.zoneID + 1) + "", cx, cy - loadImageInterface.khu.getHeight() - 6, 2);
                return;
            }
            Part ph = GameScr.parts[template.headId], pl = GameScr.parts[template.legId], pb = GameScr.parts[template.bodyId], pw = null;
            if (template.npcTemplateId == 34)
                pw = GameScr.parts[167];
            if (cdir == 1)
            {
                SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                if (pw != null)
                    SmallImage.drawSmallImage(g, pw.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][3][1] + pw.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pw.pi[CharInfo[cf][3][0]].dy, 0, 0);

            }
            else
            {
                SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                if (pw != null)
                    SmallImage.drawSmallImage(g, pw.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][3][1] - pw.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pw.pi[CharInfo[cf][3][0]].dy, 2, 24);
            }
            if (typeNV > -1)
            {
                g.drawRegion(GameScr.imgQuest, 0, (typeNV * 2 + (GameCanvas.gameTick / 10) % 2) * GameScr.imgQuest.getHeight() / 4, GameScr.imgQuest.getWidth(), GameScr.imgQuest.getHeight() / 4, 0, cx, cy - ch - 3 - mFont.tahoma_7.getHeight() - 10, mGraphics.VCENTER | mGraphics.HCENTER, false);
            }
            if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.Equals(this))
            {
                mFont.tahoma_7_yellow.drawString(g, template.name, cx, cy - ch - mFont.tahoma_7.getHeight() - 7, mFont.CENTER, mFont.tahoma_7_grey);
                //				SmallImage.drawSmallImage(g, 988, cx, cy - ch - 2, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
            }
            else
            {
                mFont.tahoma_7_yellow.drawString(g, template.name, cx, cy - ch - 3 - mFont.tahoma_7.getHeight(), mFont.CENTER, mFont.tahoma_7_grey);
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }

        //		}
    }
	public void paint(mGraphics g,int cx,int cy) {
        //		cy-=20;
        if (!isPaint()) return;
        if (statusMe == A_HIDE)
            return;
        if (cTypePk != PK_NORMAL)
        {
            base.paint(g);
            return;
        }
        if (template == null)
            return;

        //		if (template.npcTemplateId == 13) {
        //			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
        //				SmallImage.drawSmallImage(g, 988, cx, cy - ch - 1, 0, Graphics.BOTTOM | Graphics.HCENTER);
        //			}
        //			SmallImage.drawSmallImage(g, 1060, cx, cy, 0, Graphics.BOTTOM | Graphics.HCENTER);
        //			mFont.tahoma_7_white.drawString(g, TileMap.zoneID + "", cx, cy - 10 - 2 - mFont.tahoma_7.getHeight(), mFont.CENTER);
        //		}else if (template.npcTemplateId == 31) {
        //			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
        //				SmallImage.drawSmallImage(g, 988, cx, cy - ch - 1, 0, Graphics.BOTTOM | Graphics.HCENTER);
        //			}
        //			SmallImage.drawSmallImage(g, 1291, cx, cy, 0, Graphics.BOTTOM | Graphics.HCENTER);
        //		}
        //		else if (template.npcTemplateId == 27) {
        //			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
        //				SmallImage.drawSmallImage(g, 988, cx, cy - ch - 1, 0, Graphics.BOTTOM | Graphics.HCENTER);
        //			}
        //			SmallImage.drawSmallImage(g, 1224, cx, cy, 0, Graphics.BOTTOM | Graphics.HCENTER);
        //		} else {
        Part ph = GameScr.parts[template.headId], pl = GameScr.parts[template.legId], pb = GameScr.parts[template.bodyId], pw = null;
        if (template.npcTemplateId == 34)
            pw = GameScr.parts[167];
        if (cdir == 1)
        {
            SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
            SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
            SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
            if (pw != null)
                SmallImage.drawSmallImage(g, pw.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][3][1] + pw.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pw.pi[CharInfo[cf][3][0]].dy, 0, 0);

        }
        else
        {
            SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
            SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
            SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
            if (pw != null)
                SmallImage.drawSmallImage(g, pw.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][3][1] - pw.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pw.pi[CharInfo[cf][3][0]].dy, 2, 24);
        }
        //		}

        //			cy+=20;
    }
	public void NhiemVu(bool ischeckNPC) { //true ko hien popup chi de check paint icon NV tren dau NPC
		mVector menu = new mVector();
		int idnpc = this.npcId;
	//	Cout.println(getClass(), Quest.listUnReceiveQuest.size()+ "npc nhiemvu");
		for (int i = 0; i < Quest.listUnReceiveQuest.size(); i++) {
			 Quest quest = (Quest) Quest.listUnReceiveQuest
					.elementAt(i);
			if (quest.idNPC_From == idnpc) {
				Command cmd = new Command(quest.name,this, 1,new MenuObject(0,i,npcId));
				//cmd.setFraCaption(fraQuest, 1, 1);
				menu.addElement(cmd);
			}
			else {
			//	Cout.println(getClass(),quest.idNPC_From+ "  idnpc "+idnpc);
			}
		}
		for (int i = 0; i < Quest.vecQuestDoing_Main.size(); i++) {
			 Quest quest = (Quest)Quest.vecQuestDoing_Main
					.elementAt(i);
			if (quest.idNPC_To == idnpc) {
				Command cmd = new Command(quest.name,this, 3,new MenuObject(2,i,npcId));
//				cmd.setFraCaption(fraQuest, 1, 2);
				menu.addElement(cmd);
			}
		}
		for (int i = 0; i <Quest.vecQuestDoing_Sub.size(); i++) {
			 Quest quest = (Quest)Quest.vecQuestDoing_Sub
					.elementAt(i);
			if (quest.idNPC_To == idnpc) {
				Command cmd = new Command(quest.name,this, 4,new MenuObject(3,i,npcId));
//				cmd.setFraCaption(fraQuest, 1, 2);
				menu.addElement(cmd);
			}
		}
		for (int i = 0; i <Quest.vecQuestFinish.size(); i++) {
			 Quest quest = (Quest)Quest.vecQuestFinish
					.elementAt(i);
			if (quest.idNPC_To == idnpc) {
				Command cmd = new Command(quest.name,this, 2,new MenuObject(1,i,npcId));
				//cmd.setFraCaption(fraQuest, 1, 3);
				menu.addElement(cmd);
			}
		}

	//	Cout.println(getClass(), "npc nhiemvu menu "+menu.size());
		if (menu.size() == 0||ischeckNPC) {
//			GameCanvas.clearKeyHold();
//			GameCanvas.clearKeyPressed();
			// cmdTroChuyen.perform();
//            if(!HuongDan.isHuongDan)
//			GameCanvas.menu.doCloseMenu();
		} else {
			//GameCanvas.menu.doCloseMenu();
			GameCanvas.clearKeyHold();
			GameCanvas.clearKeyPressed();
			if(!ischeckNPC&&menu.size()!=0){
//				GameCanvas.menu.startAt(menu,0);
				GameCanvas.menu.startAtNPC(menu, 0,idnpc, this, "");
//				if(menu.size()==1){
//					Command cmdnv = (Command)menu.elementAt(0);
//					cmdnv.performAction();
//				}
//				GameCanvas.menu.startAt(menu, 0);
			}
		}
	}
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub

	//	Cout.println(getClass(), "npc perform "+idAction);
		// TODO Auto-generated method stub
		int subIndex = ((MenuObject)p).idOption;
		switch (idAction) {
		case 1:// co the nhan
			if (subIndex >= 0 && subIndex <= Quest.listUnReceiveQuest.size()) {
				Quest quest = (Quest) Quest.listUnReceiveQuest
						.elementAt(subIndex);
				quest.beginQuest();
			}
			break;
		case 2:// hoan thanh
			if (subIndex >= 0 && subIndex <= Quest.vecQuestFinish.size()) {
				Quest quest = (Quest) Quest.vecQuestFinish
						.elementAt(subIndex);
				quest.beginQuest();
//				Service.gI().ReQuest((byte)1, (short)quest.ID, (byte)(quest.isMain==true?0:1));
			}
			break;
		case 3:// dang lam main
			if (subIndex >= 0
					&& subIndex <= Quest.vecQuestDoing_Main.size()) {
				Quest quest = (Quest) Quest.vecQuestDoing_Main
						.elementAt(subIndex);

				//quest.beginQuest();
				string[] help = new string[1];
				help[0] = quest.strDetailHelp;
				quest.mstrTalk= help;
				quest.show_Info_Quest_Doing();
			}
			break;
		case 4:// // dang lam sub
			if (subIndex >= 0 && subIndex <= Quest.vecQuestDoing_Sub.size()) {
				Quest quest = (Quest) Quest.vecQuestDoing_Sub
						.elementAt(subIndex);
				quest.show_Info_Quest_Doing();
			}
			break;
//		case 4:
//			GlobalService.gI().getlist_from_npc((byte) ID);
//			break;
		case 5:
			NhiemVu(false);
			break;
		case 6:// gui thong tin confirm menu
			MenuObject menu=(MenuObject)p;
//			Service.gI().doSelectMenu(menu.idActor,menu.idMenu,menu.idOption);
			break;
		case 7:// lay menu ve

			break;
		}
		
	
	}
	public void hide() {
		statusMe = Char.A_HIDE;
		chatPopup = null;
	}
}