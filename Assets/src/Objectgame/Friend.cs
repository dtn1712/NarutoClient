

public class Friend {
	public const sbyte INVITE_ADD_FRIEND= 0;
	public const sbyte ACCEPT_ADD_FRIEND = 1;
	public const sbyte UNFRIEND = 2;
	public const sbyte REQUEST_FRIEND_LIST = 3;
}
