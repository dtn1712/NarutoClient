


using System;
public class TileMap {
	public const int T_EMPTY = 0;
	public const int T_TOP = 2;
	public const int T_LEFT = 2 << 1;
	public const int T_RIGHT = 2 << 2;
	public const int T_TREE = 2 << 3;
	public const int T_WATERFALL = 2 << 4;
	public const int T_WATERFLOW = 2 << 5;
	public const int T_TOPFALL = 2 << 6;
	public const int T_OUTSIDE = 2 << 7;
	public const int T_DOWN1PIXEL = 2 << 8;
	public const int T_BRIDGE = 2 << 9;
	public const int T_UNDERWATER = 2 << 10;
	public const int T_SOLIDGROUND = 2 << 11;
	public const int T_BOTTOM = 2 << 12;
	public const int T_DIE = 2 << 13;
	public const int T_HEBI = 2 << 14;
	public const int T_BANG = 2 << 15;
	public const int T_JUM8 = 2 << 16;
	public const int T_NT0 = 2 << 17;
	public const int T_NT1 = 2 << 18;
	public const int T_CENTER = 1;

	public static int tmw, tmh, pxw, pxh, tileID, lastTileID = -1;
	public static int[] maps;
	public static int[] types;
	// Tileo
	public static mBitmap imgMaptile;
	public static mBitmap[] imgTile;
	public static mBitmap imgTileSmall, imgMiniMap;
	public static mBitmap imgWaterfall;
	public static mBitmap imgTopWaterfall;
	public static mBitmap imgWaterflow, imgWaterlowN, imgWaterlowN2, imgWaterF;

	public static mBitmap imgLeaf;
	public static byte size = 24;
	private static int bx, dbx, fx, dfx;
	//
	public static string[] instruction;
	public static int[] iX, iY, iW;
	public static int iCount;

	public static string mapName = "";
	public static byte versionMap = 1;
	public static short mapID;
	public static sbyte lastBgID = -1, zoneID, bgID,bgType,lastType=-1, typeMap;
	public static sbyte planetID, lastPlanetId = -1;
	public static long timeTranMini;
	public static mVector vGo = new mVector();
	public static mVector vItemBg = new mVector();
	public static mVector vCurrItem = new mVector();
	// public static int nItemBg;
	// public static int nItemGet;
	public static string[] mapNames;
	public const sbyte MAP_NORMAL = 0;
	public static mBitmap bong;
	public const int TRAIDAT_DOINUI = 0;
	public const int TRAIDAT_RUNG = 1;
	public const int TRAIDAT_DAORUA = 2;
	public const int TRAIDAT_DADO = 3;

	public const int NAMEK_DOINUI = 4;
	public const int NAMEK_RUNG = 6;
	public const int NAMEK_DAO = 7;
	public const int NAMEK_THUNGLUNG = 5;

	public const int SAYAI_DOINUI = 8;
	public const int SAYAI_RUNG = 9;
	public const int SAYAI_CITY = 10;
	public const int SAYAI_NIGHT = 11;
	public const int KAMISAMA=12;
	public const int TIME_ROOM=13;
	
	public const int ARENA=14;
	public const int DOCNHAN_1=15;
	public const int DOCNHAN_3=16;
	public const int DOCNHAN_2=17;
	public const int FIZE=18;
	public const int FIZE2=19;
	public const int FIZE3=20;
	public static mHashtable listNameAllMap = new mHashtable();
	public static mBitmap[] bgItem = new mBitmap[8];
//    static {

//        for (int i = 0; i < 3; i++) {

//        }

////		bong = GameCanvas.loadImage("/hd/bong.png");

//    };

	public static bool isTrainingMap() {
		if (mapID == 39 || mapID == 40 || mapID == 41)
			return true;
		return false;
	}

	public static mVector vObject = new mVector();

	public static BgItem getBIById(int id) {
		for (int i = 0; i < vItemBg.size(); i++) {
			BgItem bi = (BgItem) vItemBg.elementAt(i);
			if (bi.id == id)
				return bi;
		}
		return null;
	}

	public static int[] offlineId = new int[] { 21, 22, 23, 39, 40, 41 };
	public static int[] highterId = { 21, 22, 23, 24, 25, 26 };

	public static bool isOfflineMap() {
		for (int i = 0; i < offlineId.Length; i++)
			if (mapID == offlineId[i])
				return true;
		return false;
	}

	public static bool isHighterMap()

	{
		for (int i = 0; i < offlineId.Length; i++)
			if (mapID == highterId[i])
				return true;
		return false;
	}

	public static int[] toOfflineId = new int[] { 0, 7, 14 };

	public static bool isToOfflineMap() {
		for (int i = 0; i < toOfflineId.Length; i++)
			if (mapID == toOfflineId[i])
				return true;
		return false;
	}

	public static void freeTilemap() {
		imgTile = null;
		mSystem.gcc();
	}

	public static bool isExistMoreOne(int id) {
		if(id==156)
			return false;
		if(mapID==54 || mapID==55 || mapID==56 || mapID==57 || mapID==58 || mapID==59)
			return false;
		int dem = 0;
		for (int i = 0; i < vCurrItem.size(); i++) {
			BgItem b = (BgItem) vCurrItem.elementAt(i);
			if (b.id == id)
				dem++;
		}
		if (dem > 2)
			return true;
		return false;
	}

//	static final void loadTileImage() {
//		
//		if (imgWaterfall == null)
//			imgWaterfall = GameCanvas.loadImageRMS("/tWater/wtf.png");
//		if (imgTopWaterfall == null)
//			imgTopWaterfall = GameCanvas.loadImageRMS("/tWater/twtf.png");
//		if (imgWaterflow == null)
//			imgWaterflow = GameCanvas.loadImageRMS("/tWater/wts.png");
//		if (imgWaterlowN == null)
//			imgWaterlowN = GameCanvas.loadImageRMS("/tWater/wtsN.png");
//		if (imgWaterlowN2 == null)
//			imgWaterlowN2 = GameCanvas.loadImageRMS("/tWater/wtsN2.png");
//		System.gc();
//	}

	public static void setTile(int index, int[] mapsArr, int type) {
//		System.out.println("MAP ARR ----> "+mapsArr.Length+" ---- "+type);
		for (int i = 0; i < mapsArr.Length; i++) {
			if (maps[index] == mapsArr[i]) {
//				if(maps[index] != -1){ // sửa để test
//					types[index] |= T_TOP;
//				}else
					types[index] |= type;
//				types[index] |= 2;
				
//				types[index] |= T_TOP;
				return;
			}
		}
	}
	public static int[][] tileType;
	public static int[][][] tileIndex;
	
	static int defaultSolidTile;
	
	
		// for (int j = 0; j < types.Length; j++) {
		// System.out.println(types.Length + "," + maps.Length + "," + tileID +
		// "," + types[j]);
		// }
	public static void loadMap(int tileId) {
//		tileId = 2;
		Cout.println(tileId+" size "+size+"leng "+tileType.Length+"TILEMAP WIDTH ::: HIEGHT ----> "+tmw+" :::: "+tmh);
		pxh = tmh * size;
		pxw = tmw * size;
		
		
		int tile = tileId - 1;
//		int tile = tileId;
		
		try {
			for (int i = 0; i <tmw * tmh; i++) {
				for(int a=0;a<tileType[tile].Length;a++){
					setTile(i, tileIndex[tile][a], tileType[tile][a]);
				}
			}

		} catch (Exception e) {
			//System.out.println("Error Load Map");
			//e.printStackTrace();
			GameMidlet.instance.exit();
		}
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		
		
		
	}
	public static bool isInAirMap(){
		if(mapID==45 || mapID==46 || mapID==48)
			return true;
		return false;
	}
	public static bool isDoubleMap(){
		if(mapID==45 || mapID==46 || mapID==48 || mapID==51 || mapID==52)
			return true;
		return false;
	}
//	public static void getTile(){
//	{
//			Bitmap img=GameCanvas.loadImageRMS("/t/" + TileMap.tileID + "$1.png");
//			if(img!=null){
//				Rms.DeleteStorage("x"+mGraphics.zoomLevel+"t"+tileID);
//				imgTile=new Image[100];
//				for(int i=0;i<imgTile.Length;i++)
//					imgTile[i]=GameCanvas.loadImageRMS("/t/" + TileMap.tileID + "$"+(i+1)+ ".png");
//				
//			}
//			else{
//				img=GameCanvas.loadImageRMS("/t/" + TileMap.tileID + ".png");
//				if(img!=null){
//					Rms.DeleteStorage("$");
//					imgTile=new Image[1];
//					imgTile[0]=img;
//				}
//			}
//			
//		}
//		
//	}
	public static  void paintTile(mGraphics g,int frame,int indexX,int indexY){
		
		if(imgTile==null)
			return;
		if(imgTile.Length==1)
			g.drawRegion(imgTile[0], 0, frame * size, size, size, 0, indexX* size, indexY * size, 0);
		else
			g.drawImage(imgTile[frame], indexX*size, indexY*size,0);
	}
	public static  void paintTile(mGraphics g,int frame,int x,int y,int w,int h){
		
		if(imgTile==null)
			return;
		if(imgTile.Length==1)
			g.drawRegion(imgTile[0], 0, frame * w, w, w, 0, x, y , 0);
		else
			g.drawImage(imgTile[frame], x, y,0);
	}
	public static  void paintTilemapLOW(mGraphics g) {

		for (int a = GameScr.gssx; a < GameScr.gssxe; a++) {
			for (int b = GameScr.gssy; b < GameScr.gssye; b++) {

				int t = maps[b * tmw + a] - 1;
				if (t != -1)
					//g.drawRegion(imgTile, 0, t * size, 24, 24, 0, a * size, b * size, 0);
					paintTile(g, t, a, b);
				if ((tileTypeAt(a, b) & T_WATERFALL) == T_WATERFALL) {
					g.drawRegion(imgWaterfall, 0, 24 * (GameCanvas.gameTick % 4), 24, 24, 0, a * size, b * size, 0);
				} else if ((tileTypeAt(a, b) & T_WATERFLOW) == T_WATERFLOW) {
					if ((tileTypeAt(a, b - 1) & T_WATERFALL) == T_WATERFALL)
						g.drawRegion(imgWaterfall, 0, 24 * (GameCanvas.gameTick % 4), 24, 24, 0, a * size, b * size, 0);
					else if ((tileTypeAt(a, b - 1) & T_SOLIDGROUND) == T_SOLIDGROUND)
					//	g.drawRegion(imgTile, 0, 24 * 21, 24, 24, 0, a * size, b * size, 0);
						paintTile(g, 21, a, b);
					mBitmap imgWater = null;
					if (tileID == 5)
						imgWater = imgWaterlowN;
					else if (tileID == 8)
						imgWater = imgWaterlowN2;
					else
						imgWater = imgWaterflow;
					g.drawRegion(imgWater, 0, ((GameCanvas.gameTick % 8) >> 2) * 24, 24, 24, 0, a * size, b * size, 0);
				}
				if ((tileTypeAt(a, b) & T_UNDERWATER) == T_UNDERWATER) {
					if ((tileTypeAt(a, b - 1) & T_WATERFALL) == T_WATERFALL)
						g.drawRegion(imgWaterfall, 0, 24 * (GameCanvas.gameTick % 4), 24, 24, 0, a * size, b * size, 0);
					else if ((tileTypeAt(a, b - 1) & T_SOLIDGROUND) == T_SOLIDGROUND)
						//g.drawRegion(imgTile, 0, 24 * 21, 24, 24, 0, a * size, b * size, 0);
						paintTile(g, 21, a, b);
					//g.drawRegion(imgTile, 0, (maps[b * tmw + a] - 1) * size, 24, 24, 0, a * size, b * size, 0);
					paintTile(g, (maps[b * tmw + a] - 1), a, b);
				}
			}
		}
	}
//	public static Bitmap imgLight=GameCanvas.loadImage("/bg/light.png");
	public static  void paintTilemap(mGraphics g) {
	
		int replaceTile;
//		if(Char.isLoadingMap)
//			return;
		// GameCanvas.debug("map " + mapID + " tile " + tileID, 1);
		GameScr.gI().paintBgItem(g, 1);
		
		
		
		for (int a = GameScr.gssx; a < GameScr.gssxe; a++) {
			for (int b = GameScr.gssy; b < GameScr.gssye; b++) {
				if(a==0)continue;
				if(a==tmw-1)continue;
				int t = maps[b * tmw + a] - 1;

				if ((tileTypeAt(a, b) & T_OUTSIDE) == T_OUTSIDE)
					continue;
				if ((tileTypeAt(a, b) & T_WATERFALL) == T_WATERFALL) {
					g.drawRegion(imgWaterfall, 0, 24 * ((GameCanvas.gameTick % 8) >> 1), 24, 24, 0, a * size, b * size,
							0);
					continue;
				} else if ((tileTypeAt(a, b) & T_TOPFALL) == T_TOPFALL) {
					g.drawRegion(imgTopWaterfall, 0, 24 * ((GameCanvas.gameTick % 8) >> 1), 24, 24, 0, a * size, b
							* size, 0);
					continue;
				}
				
				if (tileID == 13 ) {
					if(!GameCanvas.lowGraphic)
						return;
					else{
						if(t!=-1){}
							//g.drawRegion(imgTile, 0, 24, 24, 24, 0, a * size, b * size, 0);
							paintTile(g, 0, a, b);
						continue;
					}
				}
				if (tileID == 2) {
				
					if ((tileTypeAt(a, b) & T_DOWN1PIXEL) == T_DOWN1PIXEL) {
						if (t != -1) {
							//g.drawRegion(imgTile, 0, t * size, 24, 1, 0, a * size, b * size, 0);
							//g.drawRegion(imgTile, 0, t * size, 24, 24, 0, a * size, b * size + 1, 0);
							paintTile(g, t, a * size, b * size, 24, 1);
							paintTile(g, t, a * size, b * size+1, 24, 24);
						}
					}
				}
				if (tileID == 3) {
				}
               //System.out.println(GameScr.cmx+"  paint tile map>>>>>>  "+(fx + GameScr.cmx));
				if ((tileTypeAt(a, b) & T_TREE) == T_TREE) {
					bx = a * size - GameScr.cmx;
					dbx = bx - GameScr.gW2;
					dfx = ((size - 2) * dbx) / size;
					fx = dfx + GameScr.gW2;
					//g.drawRegion(imgTile, 0, t * size, 24, 24, 0, fx + GameScr.cmx, b * size, 0);
					paintTile(g, t,fx + GameScr.cmx, b * size, 24, 24);
				} else if ((tileTypeAt(a, b) & T_DOWN1PIXEL) == T_DOWN1PIXEL) {
					if (t != -1) {
						paintTile(g, t, a * size, b * size, 24, 1);
						paintTile(g, t, a * size, b * size+1, 24, 24);
					}
				} else if (t != -1) {
					paintTile(g, t, a, b);
				}
			}
		}
		// paint outside camera when drag mouse (cmy<0 and cmy>cmyLim)
		if(GameScr.cmx<24)
		{
			for (int b = GameScr.gssy; b < GameScr.gssye; b++) {
				int t = maps[b * tmw + 1] - 1;
				if(t!=-1)
				{
					//g.drawRegion(imgTile, 0, t * size, 24, 24, 0, 0, b * size, 0);
					paintTile(g, t, 0, b);
				}
			}
		}
		if(GameScr.cmx > GameScr.cmxLim)
		{
			int a = tmw-2;
			for (int b = GameScr.gssy; b < GameScr.gssye; b++) {
				int t = maps[b * tmw + a] - 1;
				if(t!=-1)
				{
				//	g.drawRegion(imgTile, 0, t * size, 24, 24, 0, (a+1)*size , b * size, 0);
					paintTile(g, t, a+1, b);
				}
			}
		}		
	}

	public static int sizeMiniMap = 2, gssx, gssxe, gssy, gssye, countx, county;
	private static int[] colorMini = new int[] { 0x503a0a, 0x866318 };
	public static int yWater = 0;
	public static bool isWaterEff(){
		if(mapID==54 || mapID==55 || mapID==56 || mapID==57)
			return false;
		return true;
	}
	
	public static  void paintOutTilemap(mGraphics g) {
		if (GameCanvas.lowGraphic)
			return;

		for (int a = GameScr.gssx; a < GameScr.gssxe; a++) {
			for (int b = GameScr.gssy; b < GameScr.gssye; b++) {

				if ((tileTypeAt(a, b) & T_WATERFLOW) == T_WATERFLOW ) {
					mBitmap imgWater = null;
					if (tileID == 5)
						imgWater = imgWaterlowN;
					else if (tileID == 8)
						imgWater = imgWaterlowN2;
					else
						imgWater = imgWaterflow;
					if(!isWaterEff()){
						g.drawRegion(imgWater, 0, 0* 24, 24, 24, 0, a * size,
								b * size-1 , 0);
						g.drawRegion(imgWater, 0, 0* 24, 24, 24, 0, a * size,
								b * size-3 , 0);
					}
					g.drawRegion(imgWater, 0, ((GameCanvas.gameTick % 8) >> 2) * 24, 24, 24, 0, a * size,
							b * size - 12, 0);
					if (yWater == 0&& isWaterEff()) {
						yWater = b * size - 12;
						int waterColor = 0xffffff;
						if (GameCanvas.typeBg == TRAIDAT_DAORUA)
							waterColor = 0xa5e1f7;
						else if (GameCanvas.typeBg == NAMEK_DOINUI)
							waterColor = 0x7bc56e;
						else if (GameCanvas.typeBg == NAMEK_DAO)
							waterColor = 0x56dec5;
//						BackgroudEffect.addWater(waterColor, yWater + 15);
					}
				}

			}
		}

//		BackgroudEffect.paintWaterAll(g);
	}

	public static void loadMapFromResource(int mapID)  {
        try 
	    {	        
		    InputStream iss = null;
		    DataInputStream isss = null;
		    Cout.println("TileMap loadma "+("map/" + TileMap.mapID));
		  //  iss =  GameMidlet.asset.open("map/" + TileMap.mapID);
            isss = new DataInputStream("/map/" + TileMap.mapID);
		    TileMap.tmw = (char) isss.read();
		    TileMap.tmh = (char) isss.read();

		    Cout.println(TileMap.tmw+ "  TileMap loadma "+TileMap.tmh);
		    TileMap.maps = new int[isss.available()];
		    for (int i = 0; i < TileMap.tmw * TileMap.tmh; i++){
			    TileMap.maps[i] = (char) isss.read();
		    }
		    TileMap.types = new int[TileMap.maps.Length];
	    }
	    catch (Exception)
	    {
		
		    throw;
	    }
		
	}

	public static int tileAt(int x, int y) {
		try {
			return maps[y * tmw + x];
		} catch (System.Exception ex) {
			return 1000;
		}
	}

	public static int tileTypeAt(int x, int y) {
		try {
			return types[y * tmw + x];
		} catch (Exception ex) {
			return 1000;
		}
	}

	public static int tileTypeAtPixel(int px, int py) {
		try {

			return types[(py / size) * tmw + (px / size)];
		} catch (Exception ex) {
			return 1000;
		}
	}

	public static  bool tileTypeAt(int px, int py, int t) {
		try {

			return (types[(py / size) * tmw + (px / size)] & t) == t;
		} catch (Exception ex) {
			return false;
		}
	}

	public static  void setTileTypeAtPixel(int px, int py, int t) {

		types[(py / size) * tmw + (px / size)] |= t;
	}

	public static  void setTileTypeAt(int x, int y, int t) {
		types[y * tmw + x] = t;
	}

	public static  void killTileTypeAt(int px, int py, int t) {
		types[(py / size) * tmw + (px / size)] &= ~t;
	}

	public static int tileYofPixel(int py) {
		return (py / size) * size;
	}

	public static int tileXofPixel(int px) {
		return (px / size) * size;
	}


	public static void loadMainTile() {
		// imgTile = null;
		mSystem.gcc();
		if (lastTileID != tileID) {
			//imgTile = GameCanvas.loadImageRMS("/t/" + TileMap.tileID + ".png");
//			getTile();
			lastTileID = tileID;
		}

		// TileMap.imgTileSmall = GameCanvas.loadImage("/t/mini_" +
		// TileMap.tileID + ".png");
	}
	
	public static void loadMapFromResource()  {
        try 
	    {	        
		    InputStream iss = null;
		    DataInputStream isss = null;
		   // iss = MyStream.readFile("/map/0");
    //		System.out.println("iss ---> "+iss);
            isss = new DataInputStream("/map/0");

		    Cout.println("TileMap loadma222 "+iss);
    //		System.out.println("is ---> "+is);
    //		TileMap.tmw = (char) is.read();
    //		TileMap.tmh = (char) is.read();
		    TileMap.tmw = (char) isss.read();
		    TileMap.tmh = (char) isss.read();
		    pxh = tmh * size;
		    pxw = tmw * size;
		    TileMap.maps = new int[isss.available()];
		    TileMap.types = new int[TileMap.maps.Length];
		    for (int i = 0; i < TileMap.tmw * TileMap.tmh; i++)
			    {TileMap.maps[i] = (char) isss.read();
			    TileMap.types[i]=0;// chi co trai dat
			    }
	    }
	    catch (System.Exception)
	    {
		
		    throw;
	    }
		
		
	}

	public static void loadMapfile(int MapID){
		try{
			TileMap.vCurrItem.removeAllElements();
			TileMap.vItemBg.removeAllElements();
			loadMapFromResource(MapID);

			
						
			
		}catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
		}
	}
	
	public static void loadimgTile(int TileID){
        TileID = TileID - 1;
        if (imgMaptile != null && imgMaptile.image != null)
            imgMaptile.cleanImg();
        imgMaptile = null;
        imgMaptile = GameCanvas.loadImage("/tile" + TileID + ".png");
        Cout.println("loadimgTile ---> " + imgMaptile);


    }
	
	public static void paintMap(mGraphics g){

        GameScr.gI().paintBgItem(g, 1);
        GameScr.gssw = GameCanvas.w / TileMap.size + 2;
        GameScr.gssh = GameCanvas.h / TileMap.size + 2;
        if (GameCanvas.w % 24 != 0)
            GameScr.gssw += 1;

        GameScr.gssx = GameScr.cmx / TileMap.size - 1;
        if (GameScr.gssx < 0)
            GameScr.gssx = 0;
        GameScr.gssy = GameScr.cmy / TileMap.size;
        GameScr.gssxe = GameScr.gssx + GameScr.gssw;
        GameScr.gssye = GameScr.gssy + GameScr.gssh;
        if (GameScr.gssy < 0)
            GameScr.gssy = 0;
        if (GameScr.gssye > TileMap.tmh - 1)
            GameScr.gssye = TileMap.tmh - 1;
        //		loadMap(1);
        //		positionY=GameScr.gssy*size;
        if (imgMaptile != null)
            for (int a = GameScr.gssx; a < GameScr.gssxe; a++)
            {
                for (int b = GameScr.gssy; b < GameScr.gssye; b++)
                {

                    try
                    {

                        int t = maps[b * tmw + a] - 1;
                        if ((t + 1) * size > imgMaptile.getHeight())
                        {
                            return;
                        }
                        if (t >= 0)
                        {
                            g.drawRegion(imgMaptile, 0, t * size, size, size, 0, a * size, (b * size), 0);
                        }
                    }
                    catch (Exception e)
                    {
                        // TODO: handle exception

                       // e.printStackTrace();
                    }
                }
            }
    }

}

