

// có x,y, paint theo lối BOTTOM-CENTER (MapItem, Mob, Char)
public interface IMapObject {
   int getX();
   int getY();
   int getW();
   int getH();
   void stopMoving();
   bool isInvisible();
}
