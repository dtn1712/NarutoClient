

public class SkillTemplate {
    public sbyte id;
    public int classId, ideff;
    public string name;
    public int maxPoint;
    public int type;
    public int iconId, iconTron;
    public string[] description;
    public Skill[] skills;
    public sbyte typeskill, ntarget, typebuffSkill, subeff;
    public int range, level = -1;
    public short[] levelChar, mphao, tExistSubEff, usehp, usemp, rangelv, dame;
    public int[] cooldown, timebuffSkill;
    public sbyte[] pcSubEffAppear, ntargetlv;
    public sbyte nlevelSkill;

    public SkillTemplate(sbyte id, string name, int idIcon, sbyte typeskilll, sbyte ntargett,
            sbyte typebuffSkilll, sbyte subEfff, short rangee, string des)
    { // moi them vo Ä‘á»ƒ add vÃ o vector template láº¥y thÃ´ng tin
        this.id = id;
        this.name = name;
        this.iconId = idIcon;
        this.typeskill = typeskilll;
        this.ntarget = ntargett;
        this.typebuffSkill = typebuffSkilll;
        this.subeff = subEfff;
        this.range = rangee;
        this.description = mFont.tahoma_7_white.splitFontArray(des, 149);
    }
    public void khoitaoLevel(sbyte nlevelSkill)
    {
        this.nlevelSkill = nlevelSkill;
        levelChar = new short[nlevelSkill];
        mphao = new short[nlevelSkill];
        tExistSubEff = new short[nlevelSkill];
        usehp = new short[nlevelSkill];
        usemp = new short[nlevelSkill];
        rangelv = new short[nlevelSkill];
        cooldown = new int[nlevelSkill];
        timebuffSkill = new int[nlevelSkill];
        pcSubEffAppear = new sbyte[nlevelSkill];
        ntargetlv = new sbyte[nlevelSkill];
        dame = new short[nlevelSkill];

    }
    public int getCoolDown(sbyte level)
    {
        // TODO Auto-generated method stub
        if (level >= cooldown.Length)
        {
            return cooldown[0];
        }
        return cooldown[level];
    }
}