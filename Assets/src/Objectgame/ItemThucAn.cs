

public class ItemThucAn  { // item paint ngoài màn hình, scale nho lại so với hình item trong hành trang
	public static int tileScale = 75; //50%
	public short id,idIcon,iditem;
	public bool isUsed,isNullIdicon;
	public sbyte type;
	public string sday = "";
	public int day,hour,minute,second,value;
	public long time,timeRemove,timeOld; // mili giấy server trả về là s;
	
	public ItemThucAn(short idd,sbyte typee, short iditem, short timeR,int valuee){
		ItemTemplate dem = ItemTemplates.get(iditem);
		this.iditem = iditem;
		if(dem!=null)
		this.idIcon = dem.iconID;
		else isNullIdicon = true;
		this.type = typee;
		this.id = idd;
		this.value = valuee;
		time = mSystem.currentTimeMillis();
		this.timeRemove = timeR*1000; // đổi sang miligiay
		timeOld =  timeR;
		this.isUsed = false;
	}
	
	
	public void update(){
		if(isNullIdicon&&GameCanvas.gameTick%4==0){
			ItemTemplate dem = ItemTemplates.get(iditem);
			if(dem!=null)
				this.idIcon = dem.iconID;
		}
		long currtime = mSystem.currentTimeMillis();
		if (currtime-time>=timeRemove) {
			GuiMain.vecItemOther.removeElement(this);
		}
		else{
			int secon = second;
			if((timeRemove-(currtime-time))/1000<=60){
				second = (int)(timeRemove-(currtime-time))/1000;
				day =minute =hour= 0;
			}else if((timeRemove-(currtime-time))/1000<=3600){
				second = (int)(timeRemove-(currtime-time))/1000;
				minute = second/60;
				second = second%60;
				second = hour=day= 0;
			}else if((timeRemove-(currtime-time))/1000<=86400){
				second = (int)(timeRemove-(currtime-time))/1000;
				hour = second/3600;
				second =minute=day= 0;
			}
			else{
				second = (int)(timeRemove-(currtime-time))/1000;
				day = second/86400;
				second =minute =hour= 0;
			}
			if((currtime - timeOld)/1000>4){
//				timeOld = currtime;
//				if(GameCanvas.mainChar.hp<GameCanvas.mainChar.maxhp)
//				GameScr.gI().Cong_hp(GameCanvas.mainChar, value);
//				int hp = GameCanvas.mainChar.hp+value;
//				GameCanvas.mainChar.hp = (hp>GameCanvas.mainChar.maxhp?GameCanvas.mainChar.maxhp:hp);
//				int mp = GameCanvas.mainChar.mp+value;
//				GameCanvas.mainChar.mp = (mp>GameCanvas.mainChar.maxmp?GameCanvas.mainChar.maxmp:mp);
			}
			sday = (day<=0?"":(day<10?"0"+day:day+"")+"d")+
					(hour<=0?"":(hour<10?"0"+hour:hour+"")+"h")+
					(minute<=0?"":(minute<10?"0"+minute:minute+"")+"'")+
					(second<=0?"":(second<10?"0"+second:second+"")+"s")+"";
		}
	}
	public void paint(mGraphics g,int x, int y){
//		 g.drawRegionScale(MainTabScreen.imgSlotItem, 0, 0, 
//				 mGraphics.getImageWidth(MainTabScreen.imgSlotItem),
//				 mGraphics.getImageHeight(MainTabScreen.imgSlotItem),
//				 0, x, y,  g.TOP|g.LEFT, tileScale, true);
//		SmallImage.drawSmallImageItemOther(g,idIcon,12*Item.nItemThuong+SmallImage.IMG_ITEM_MAP, SmallImage.T_PNG,
//				x,
//				y,
//				0, 0,SmallImage.TYPE_ITEM,false,tileScale,true);	

		SmallImage.drawSmallImageScalse(g, idIcon, x+6, y+8, 0, mGraphics.VCENTER | mGraphics.HCENTER,true,tileScale);
		mFont.tahoma_7.drawString(g,sday,
				x+6,
				y+20
				, 2,true);
		
	}


	public static bool isBLOOD_LOW(byte typee) {
		// TODO Auto-generated method stub
		return typee==0;
	}
}
