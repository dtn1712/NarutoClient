

using System;
public class ChatWorld {
	
	public static mVector listNodeChat= new mVector();
	
	public ChatWorld()
	{
		
	}
	//add chat world
	public  static void ChatWorldd (Message msg)
	{
		 try {
			 	short idplayerGui = msg.reader().readShort();
				string namethanggui =  msg.reader().readUTF();
                string mess = msg.reader().readUTF();
                NodeChat note = new NodeChat(mess, false, namethanggui);
                //GameScr.listInfoServer.add(new InfoServer(namethanggui+": "+mess));
                GameScr.listInfoServer.add(new InfoServer(note));
				
				//show message on screen
				ChatManager.gI().addChat(mResources.GLOBALCHAT[0], namethanggui, mess);
				if(!ChatManager.blockGlobalChat)
					Info.addInfo(namethanggui+": "+mess, 80, mFont.tahoma_7b_yellow);
				
				mVector dem2 = new mVector();
				for (int j = ChatWorld.listNodeChat.size()-1; j >=0 ; j--) {
					NodeChat nodechat =(NodeChat)ChatWorld.listNodeChat.elementAt(j);
					if(nodechat!=null)
					dem2.addElement(nodechat);
				}
				ChatWorld.listNodeChat.removeAllElements();
				ChatWorld.listNodeChat = dem2;
				ChatWorld.listNodeChat.addElement(note);
				mVector dem = new mVector();
				for (int j = ChatWorld.listNodeChat.size()-1; j >=0 ; j--) {
					NodeChat nodechat =(NodeChat)ChatWorld.listNodeChat.elementAt(j);
					if(nodechat!=null)
					dem.addElement(nodechat);
				}
				ChatWorld.listNodeChat.removeAllElements();
				ChatWorld.listNodeChat = dem;
				int demt = 0;
				for (int j = ChatWorld.listNodeChat.size()-1; j >=0; j--) {
					NodeChat node2 = (NodeChat)ChatWorld.listNodeChat.elementAt(j);
					if(node2!=null){
						demt+=node2.hnode+NodeChat.HSTRING/2;
					}
				}
//				if(idplayerGui ==Char.myChar().charID)//chinh no
//				{
//					NodeChat node = new NodeChat(mess, true,namethanggui);
//					ChatWorld.listNodeChat.addElement(node);
//				}
//				else// orther player
//				{
//					NodeChat node = new NodeChat(mess, false,namethanggui);
//					ChatWorld.listNodeChat.addElement(node);
//				}
				
				if(ChatWorld.listNodeChat.size()>=200)//limit 200 message
				{
					ChatWorld.listNodeChat.removeElementAt(ChatWorld.listNodeChat.size()-1);
				}
				
				
		} catch (Exception e) {
			// TODO: handle exception
		//	e.printStackTrace();
		} 
	}
	
	public static void Paint(mGraphics g,int xTab,int yTab,int width,Scroll scrMain )
	{
		int notehdem = 1;
		for (int i =ChatWorld.listNodeChat.size()-1;i>=0;i--) 
		{
		    NodeChat node = (NodeChat)ChatWorld.listNodeChat.elementAt(i);
		    if(node!=null)
		    {
			     if(!node.isMe)
			     {
//				      if(node.type!=NodeChat.t_EMO)
//				      {
//				    	  node.paintNodeChat(g, xTab+NodeChat.HSTRING/2, yTab+ NodeChat.HSTRING/2+ (notehdem), node.wnode+NodeChat.HSTRING/2, node.hnode+1,0);
//				      }
                     node.paint(g, xTab + NodeChat.HSTRING, yTab + NodeChat.HSTRING / 2 + (notehdem), true, true);
                 }
			     else
			     {
//				      if(node.type!=NodeChat.t_EMO)
//				      {
//				    	  node.paintNodeChat(g, xTab-5*NodeChat.HSTRING/4+width-node.wnode+40, yTab+ NodeChat.HSTRING/2+ (notehdem), node.wnode+NodeChat.HSTRING/2, node.hnode+1,1);
//				      }
                     node.paint(g, xTab + width - NodeChat.HSTRING - NodeChat.wnode + 40, yTab + NodeChat.HSTRING / 2 + (notehdem), true, true);
                 }
			     	notehdem+=node.hnode+NodeChat.HSTRING/2;
			     	
		    }
		}
		if(notehdem>scrMain.height)
     		scrMain.cmtoY=-scrMain.height+notehdem;//dung de scroll tu duoi len
	}
	public static int update()
	{
		int dem = 0;
		for (int i = ChatWorld.listNodeChat.size()-1; i >=0; i--) {
			NodeChat node = (NodeChat)ChatWorld.listNodeChat.elementAt(i);
			if(node!=null){
				dem+=node.hnode+NodeChat.HSTRING/2;
			}
		}
		return dem;
	}
	
}
