


public class ItemTemplate {
	
	public const sbyte ITEM_TEMPLATE = 5;
	public const sbyte CHAR_WEARING = 0;
	
	public short id;
	public sbyte type;
	public sbyte gender; // 1 là đồ nam, 0 là đồ nữ, 2 là đồ chung
	public string name;
	public string description;
	public sbyte level;
    public short iconID; // icon small image id
    public sbyte clazz, quocgia;
    public short part;
    public short partquan, partdau;
	public bool isUpToUp;
	public int w, h;
	public long gia;
	public string[] despaint;
	
	// ngược(xu)

	public ItemTemplate(short templateID, sbyte type, sbyte gender, string name, string description, sbyte level, short iconID, short part, bool isUpToUp) {
		//super();
		this.id = templateID;
		this.type = type;
		this.gender = gender;
		this.name = name;
		this.description = description;
		this.level = level;
		this.iconID = iconID;
		this.part = part;
		this.isUpToUp = isUpToUp;
	}

    public ItemTemplate(short templateID, sbyte type, sbyte gender, string name, string description, sbyte level, short iconID, short part, short partquan, short partdau, bool isUpToUp)
    {
        //super();
        this.id = templateID;
        this.type = type;
        this.gender = gender;
        this.name = name;
        this.description = description;
        this.level = level;
        this.iconID = iconID;
        this.part = part;
        this.partquan = partquan;
        this.partdau = partdau;
        //System.out.println("PART QUAN THEM VAO SAU KHI THANH 1 SET ----> "+partquan+" AOOOOO "+part);
        this.isUpToUp = isUpToUp;
    }
	
	public ItemTemplate(short templateID, sbyte type,string name, short iconID) {
		//super();
		this.id = templateID;
//		this.type = type;
//		this.gender = gender;
		this.name = name;
//		this.description = description;
//		this.level = level;
		this.iconID = iconID;
//		this.part = part;
//		this.isUpToUp = isUpToUp;
	}
}
