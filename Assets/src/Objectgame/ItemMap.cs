﻿

using System;
public class ItemMap {

	public int x, y, xEnd, yEnd, f, vx, vy;
	public int itemMapID;
	public int IdCharMove;
	public ItemTemplate template;
    public sbyte status, type;
    public bool isMe;
    public long timeStartAdd;
    int dem = 0;
	
	public ItemMap(short itemMapID, short itemTemplateID,
			int x, int y, int xEnd, int yEnd) {
		this.itemMapID = itemMapID;
		this.template = ItemTemplates.get(itemTemplateID);
		this.x = xEnd;
		this.y = y;
		this.xEnd = xEnd;
		this.yEnd = yEnd;

        this.timeStartAdd = mSystem.currentTimeMillis();
		vx = (xEnd - x) >> 2;
		vy = 5;
	}
	public ItemMap(short itemID, byte type,string name, short iconID){
		this.itemMapID = itemID;
		
	}
	
	public ItemMap(short itemMapID, short itemTemplateID, int x, int y) {
		this.itemMapID = itemMapID;
		this.template = ItemTemplates.get(itemTemplateID);
		this.x = this.xEnd = x;
		this.y = this.yEnd = y;
        this.status = 1;
        this.timeStartAdd = mSystem.currentTimeMillis();
        dem = 0;
	}
	
	public void setPoint(int xEnd, int yEnd){
		this.xEnd = xEnd;
		this.yEnd = yEnd;
		vx = (xEnd - x) >> 2;
		vy = (yEnd - y) >> 2;
		status = 2;
	}

    public void update()
    {

        dem++;
        if (dem % 25 == 0)
        {
            ServerEffect.addServerEffect(36, x, y - 20, 1);
        }
        if (status == 2 && x == xEnd && y == yEnd)
        {
            GameScr.vItemMap.removeElement(this);
            if (Char.myChar().itemFocus != null && Char.myChar().itemFocus.Equals(this))
                Char.myChar().itemFocus = null;
            if (isMe)
                GameCanvas.StartDglThongBao("Đã nhặt được " + template.name.ToLower());
            return;
        }
        else if (status == 1)
            updateItem();
        if (status == 2)
        {
            if (vx == 0)
                x = xEnd;
            if (vy == 0)
                y = yEnd;
            if (x != xEnd)
            {
                x += vx;
                if ((vx > 0 && x > xEnd) || (vx < 0 && x < xEnd))
                    x = xEnd;
            }
            if (y != yEnd)
            {
                y += vy;
                if ((vy > 0 && y > yEnd) || (vy < 0 && y < yEnd))
                    y = yEnd;
            }
        }
        if ((mSystem.currentTimeMillis() - timeStartAdd) / 1000 > 60)
        {
            GameScr.vItemMap.removeElement(this);
            if (Char.myChar().itemFocus != null && Char.myChar().itemFocus.Equals(this))
                Char.myChar().itemFocus = null;

        }
        //		else {
        //			status -= 4;
        //			if(status < -12){
        //				y -= 12;
        //				status = 1;
        //			}
        //		}

    }
	public void paint(mGraphics g) {
		 try{
		   //if(status <= 0)
		    //SmallImage.drawSmallImage(g, template.iconID, x, y + status, 0, mGraphics.BOTTOM|mGraphics.HCENTER);
		   //else
		//   g.translate(0, -GameCanvas.transY);
			  if(status!=2){
					g.drawImage(loadImageInterface.bongChar, x,
							y-10, mGraphics.VCENTER|mGraphics.HCENTER);
			  }
		   SmallImage.drawSmallImage(g, template.iconID, x, y - 7, 0, mGraphics.BOTTOM|mGraphics.HCENTER);
		//   if (Char.myChar().itemFocus != null && Char.myChar().itemFocus.equals(this) && status != 2) {
//		    SmallImage.drawSmallImage(g, 988, x, y - 20, 0, mGraphics.VCENTER|mGraphics.HCENTER);
		//   }
		   
		  }catch(Exception e){
		  // e.printStackTrace();
		  }
		 }
	public void updateItem() {
			//ServerEffect.addServerEffectWithTime(36, x, y, 5000);
		
		  int wCount = 0;
		  // if (ySd <= 0)
		  // return;
		  if (TileMap.tileTypeAt(x, y-7, TileMap.T_TOP)) {

		   return;
		  } else {

		   while (wCount < 30) {
		    wCount++;
		    y += 24;
		    if (TileMap.tileTypeAt(x, y-7, TileMap.T_TOP)) {
		     if (y % 24 != 0) {
//		    	 ybong -= (ybong % 24);
		     }
		     break;
		    }
		   }
		  }

		 }
}