

public class ItemStands {
	public Item item;
	public int price, timeEnd, timeStart;
	public string seller = "";

	public ItemStands(Item item, string seller, int price) {
		this.item = item;
		this.price = price;
	}

	public ItemStands() {
	}
}
