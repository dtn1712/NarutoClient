

public class SkillTemplates {
    public static mHashtable hSkilltemplate = new mHashtable();


    public static void add(SkillTemplate it)
    {
        hSkilltemplate.put("" + it.id, it);
    }
    public static ItemTemplate get(short id)
    {
        return (ItemTemplate)hSkilltemplate.get("" + id);
    }

    public static short getIcon(short itemTemplateID)
    {
        return get(itemTemplateID).iconID;
    }

}