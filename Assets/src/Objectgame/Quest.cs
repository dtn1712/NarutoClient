

using System;
public class Quest : IActionListener{

	public const int Q_NHAT_ITEM = 0;
	public const int Q_KILL_MONSTER = 1;
	public const int Q_CHUYEN_DO = 2;
	public const int Q_TALK = 3;
	//
	public const int TYPE_ITEM = 0;
	public const int TYPE_MONSTER = 1;
	//
	public const int NHAN = 0;
	public const int TRA = 1;
	//
	// Quest co thể nhận
	public static mVector listUnReceiveQuest = new mVector();
	// Quest đã hoàn thành
	public static mVector vecQuestFinish = new mVector();
	// Quest đang làm
	public static mVector vecQuestDoing_Main = new mVector();
	public static mVector vecQuestDoing_Sub = new mVector();
	public sbyte typeQuest;
	public int ID, idNPC_To, idNPC_From, idNPCChat, nhantra;
	public bool isComplete = false, isMain;
	public string name, strDetail, strShortDetail;
	public string[] mDetail, mstrTalk, mstrHelp;
	public string strDetailTalk, strDetailHelp, strShowDialog,strGift,strNhacNv;
    public string[] strPaintDetailHelp, strPaintShortDetail;
	public int xu,xukhoa,luong;
	// Quest nhat item / đánh quái
	public short[] mIdQuest; // id item hay quai can lay
	public short[] mtotalQuest;// so luong item hay quai can lay
	public short[] mQuestGot;// so luong item hay quai da co
	
	// Quest chuyển đồ
	// Quest co thể nhận
	public Quest(int ID, bool isMain, string name, int idNpcFrom,
			string strDetailTalk, sbyte typeQuest, string strDetailHelp,string strGift,string nhacnv) {
		this.ID = ID;
		this.isMain = isMain;
		// System.out.println(isMain);
		this.idNPC_From = idNpcFrom;
		this.idNPCChat = idNpcFrom;
		this.name = name;
		this.strDetailTalk = strDetailTalk;
		this.strDetailHelp = strDetailHelp;
		this.strPaintDetailHelp = mFont.tahoma_7_white.splitFontArray(strDetailHelp, 130);
		this.typeQuest = typeQuest;
		nhantra = typeQuest==3?TRA:NHAN;
		this.strGift=strGift;
		this.strNhacNv = nhacnv;
		string[] infoqua = strGift.Split('/');
		try {
			for (int i = 0; i < infoqua.Length; i++) {
				if(i==0) xu = int.Parse(infoqua[i]);
                else if (i == 1) xukhoa = int.Parse(infoqua[i]);
                else if (i == 2) luong = int.Parse(infoqua[i]);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		mstrTalk = mFont.split(strDetailTalk, ">");
	}
	
	// Quest đã xong
	public Quest(int ID, bool isMain, string name, int idNpcTo,
			string strDetailTalk, string strDetailHelp,String nhacnv) {
		this.ID = ID;
		this.isMain = isMain;
		this.idNPC_To = idNpcTo;
		this.idNPCChat = idNpcTo;
		this.name = name;
		this.strDetailTalk = strDetailTalk;
		this.strDetailHelp = strDetailHelp;

		this.strNhacNv = nhacnv;

		this.strPaintDetailHelp = mFont.tahoma_7_white.splitFontArray(strDetailHelp, 130);
//		mstrHelp = mFont.tahoma_7_white.splitFontArray(strDetailHelp,
//				MainTabNew.wblack - 35);
		nhantra = TRA;
		mstrTalk = mFont.split(strDetailTalk, ">");
	}
	
	// Quest đang làm nhat item va danh quai
	public Quest(int ID, bool isMain, string name, string strDetailHelp,
			sbyte typeQuest, string strShortDetail, int idNpcTo, short[] mid,
			short[] mtotal, short[] mget, int typeItem_Monster,String strGift,String nhacnv) {
		this.ID = ID;
		this.isMain = isMain;
		this.idNPC_To = idNpcTo;
		this.idNPCChat = idNpcTo;
		this.name = name;
		this.typeQuest = typeQuest;
		this.strShortDetail = strShortDetail;// paint trong guiquesst
		this.strDetailHelp = strDetailHelp; //paint tai npc hỗ trợ
        this.strPaintShortDetail = mFont.tahoma_7_white.splitFontArray(strDetailHelp, 130);
	
		this.strPaintDetailHelp = mFont.tahoma_7_white.splitFontArray(strShortDetail, 130);
		this.mIdQuest = mid;
		this.strNhacNv = nhacnv;
		// System.out.println(mIdQuest.Length+"   size bang may");
		this.mtotalQuest = mtotal;
		this.mQuestGot = mget;
		this.strGift=strGift;
		string[] infoqua = strGift.Split('/');
		try {
			for (int i = 0; i < infoqua.Length; i++) {
				if(i==0) xu = int.Parse(infoqua[i]);
                else if (i == 1) xukhoa = int.Parse(infoqua[i]);
                else if (i == 2) luong = int.Parse(infoqua[i]);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		mstrTalk = mFont.split(strDetailHelp, ">");
		Cout.println("nv dg lam "+mstrTalk);
	}
	
	// Quest chuyển đồ
	public Quest(int ID, bool isMain, string name, string strDetailHelp,
			int idNpcTo, string strShortDetail) {
		this.ID = ID;
		this.isMain = isMain;
		this.name = name;
		this.strShortDetail = strShortDetail;
		this.strDetailHelp = strDetailHelp;
		this.idNPC_To = idNpcTo;
		this.idNPCChat = idNpcTo;
	}
	int step = 0;

	public void beginQuest() {
		step = 0;
//		MainChar.currentQuest = this;
		nextStep(false);
	}
	public void nextStep(bool isHelp) {
		
		if (mstrTalk[step].Trim().StartsWith("0")) {
			mVector menu = new mVector();
//			if(GameCanvas.mainChar.level<levelyecau||(GameCanvas.mainChar.level==levelyecau&&GameCanvas.mainChar.star<saoyeucau))
//			{
//				//nhiemvukhongthenhan
//				Command cmdthoat = new Command(mText.CLOSE,this, THOAT, null);
//				menu.addElement(cmdthoat);
//				GameCanvas.menu.startAt_NPC(menu, mText.banphaidat+" "+mText.nameLv[levelyecau]+mText.nameSao[saoyeucau]+" "+mText.moicothenhannvnay,
//						GameCanvas.mainChar.ID,
//						Actor.CAT_PLAYER, true, 0,0,isGioiThieu);//dg kiem tra
//				
//				return;
//			}
			Command cmd = setCmd(isHelp);
			menu.addElement(cmd);
			mSystem.substring(mstrTalk[step],(isHelp==false?1:0),
					mstrTalk[step].Length);
			for (int i = 0; i < GameScr.vNpc.size(); i++) {
				Npc npc = (Npc)GameScr.vNpc.elementAt(i);
				if(nhantra==0&&!isHelp&&npc!=null&&npc.npcId==idNPC_From)
				{
					ChatPopup.addChatPopup(mSystem.substring(mstrTalk[step],(isHelp==false?1:0),
							mstrTalk[step].Length),200,npc);
				}else if(nhantra==0&&isHelp&&npc!=null&&npc.npcId==idNPC_To)
				{
						ChatPopup.addChatPopup(mSystem.substring(mstrTalk[step],(isHelp==false?1:0),
								mstrTalk[step].Length),200,npc);
				}
				else if(nhantra==1&&npc!=null)
				{
					ChatPopup.addChatPopup(mSystem.substring(mstrTalk[step],(isHelp==false?1:0),
							mstrTalk[step].Length),200,npc);
				}
			}
			
			Cout.println("  start menu npc tiep  ");
			GameCanvas.menu.startAt(menu,0);//dg kiem tra
		} else {
			mVector menu = new mVector();
//			if(GameCanvas.mainChar.level<levelyecau||(GameCanvas.mainChar.level==levelyecau&&GameCanvas.mainChar.star<saoyeucau))
//			{
//				//nhiemvukhongthenhan
//				Command cmdthoat = new Command(mText.CLOSE,this, THOAT, null);
//				menu.addElement(cmdthoat);
//
//				NPC ac = (NPC)GameScr.findNPC_idQuest(idNPCChat);
//				short idhinh = -1;
//				if(ac!=null) idhinh = ac.idHinh;
//				GameCanvas.menu.startAt_NPC(menu, mText.banphaidat+" "+mText.nameLv[levelyecau]+mText.nameSao[saoyeucau]+" "+mText.moicothenhannvnay,
//						GameCanvas.mainChar.ID,
//						Actor.CAT_PLAYER, true, 0,idhinh,isGioiThieu);//dg kiem tra
//				return;
//			}
			Command cmd = setCmd(isHelp);
//			else 
//			{
//				cmd.x =GameCanvas.hw-mGraphics.getImageWidth(GameScr.imgButton[1])/2;
//				cmd.y = GameCanvas.h-mGraphics.getImageHeight(GameScr.imgButton[1]);
//				
//			}

            Npc npcfocus = null;
            for (int i = 0; i < GameScr.vNpc.size(); i++)
            {
                Npc npc = (Npc)GameScr.vNpc.elementAt(i);
                if (nhantra == 0 && !isHelp && npc != null && npc.npcId == idNPC_From)
                {
                    npcfocus = npc;
                    //					ChatPopup.addChatPopup(mstrTalk[step].substring((isHelp==false?1:0),
                    //							mstrTalk[step].length()),200,npc);
                }
                else if (nhantra == 0 && isHelp && npc != null && npc.npcId == idNPC_To)
                {
                    npcfocus = npc;
                    //						ChatPopup.addChatPopup(mstrTalk[step].substring((isHelp==false?1:0),
                    //								mstrTalk[step].length()),200,npc);
                }
                else if (nhantra == 1 && npc != null && npc.npcId == idNPC_From)
                {
                    npcfocus = npc;
                    //					ChatPopup.addChatPopup(mstrTalk[step].substring((isHelp==false?1:0),
                    //							mstrTalk[step].length()),200,npc);
                }
                else if (nhantra == 1 && npc != null && npc.npcId == idNPC_To)
                {
                    npcfocus = npc;
                    //					ChatPopup.addChatPopup(mstrTalk[step].substring((isHelp==false?1:0),
                    //							mstrTalk[step].length()),200,npc);
                }
            }

            if (cmd != null && npcfocus != null)
            {
                menu.addElement(cmd);
                GameCanvas.menu.startAtNPC(menu, 0, npcfocus.npcId, npcfocus, mstrTalk[step]);
            }
            else
            {
                GameCanvas.menu.startAtNPC(menu, 0, npcfocus.npcId, npcfocus, mstrTalk[step]);
            }
            //			GameCanvas.menu.startAt_NPC(menu, mstrTalk[step].substring((isHelp==false?1:0),
            //					mstrTalk[step].length()), idNPCChat, Actor.CAT_NPC,
            //					true, 0,idhinh,isGioiThieu);
        }
        step++;
    }
	public Command setCmd(bool isHelp) {
		Command cmd = null;
		// - 2, null, cmd.caption);
		// }
		if (step < mstrTalk.Length - 1) {
			//dua qua Paint infogamesCREEN
			cmd = new Command("",this, 1, null);//Tiếp
			mVector menuItems = new mVector();
			
			GameCanvas.menu.startAt(menuItems, 0);
		} else {
//			if(isHelp||typeQuest==nvNoiChuyen) cmd = new Command(isHelp==true?mText.CLOSE:mText.xong,this, (typeQuest==nvNoiChuyen?3:THOAT), null);
//			else
//			if(step==0)
//			{
//				for (int i = 0; i < GameScr.vNpc.size(); i++) {
//					Npc npc = (Npc)GameScr.vNpc.elementAt(i);
//					if(npc!=null&&npc.npcId==idNPC_From)
//					{
//						ChatPopup.addChatPopup(mstrTalk[step],60,npc);
//					}
//				}
//			}
			if(isHelp==true){}
			else cmd = new Command((nhantra==0?"":""),this, (nhantra==0?2:3), null);//Nhận//Trả
		}
		return cmd;
	}
	
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case 1:
			nextStep(false);
			break;
		case 2:
			//GameCanvas.menu.doCloseMenu();
			Service.gI().Quest((sbyte)0, (short)ID, (sbyte)(isMain==true?0:1));
//			if (GameCanvas.menu.runText != null) {
//				if (!GameCanvas.menu.runText.nextDlgStep()) {
//					return;
//				}
//			}
//			NPC npc = GameScr.findNPC(idNPCChat
//					);
			break;
		case 3:
			//GameCanvas.menu.doCloseMenu();
			Service.gI().Quest((sbyte)1, (short)ID, (sbyte)(isMain==true?0:1));
			break;
		}
	}

	public static void onReceiveInfoQuest(Message m) {
		try {
			// nhóm các nhiêm vụ 0- các nhiem vu co the nhan, 1- các nhiệm vụ
			// finish,2- các nv đang làm
			sbyte type_Group_Quest = m.reader().readByte();
			if (type_Group_Quest < 3) {
				sbyte totalQuest = m.reader().readByte();
				short idQuest = 0;
				bool main_quest = false;// main_quest=true, subQuest=false
				sbyte type_quest = 0;
				sbyte idNpc_response = 0;
				string name_quest = "";
				Quest q = null;
				if (type_Group_Quest == 0) {
					Quest.listUnReceiveQuest.removeAllElements();
				} else if (type_Group_Quest == 1) {
					Quest.vecQuestFinish.removeAllElements();
				} else if (type_Group_Quest == 2) {
					Quest.vecQuestDoing_Main.removeAllElements();
					Quest.vecQuestDoing_Sub.removeAllElements();
				}
				for (byte i = 0; i < totalQuest; i++) {
					idQuest = m.reader().readShort();
					main_quest = m.reader().readbool();
					name_quest = m.reader().readUTF();
					switch (type_Group_Quest) {
					case 0:// nhóm các nhiệm vụ có thể nhận
						// idnpc nhận nv
						short idNpc_receive = m.reader().readShort();
						// noi dung noi chuyện để nhận nv
						string content_conversation = m.reader().readUTF();
						// loai nhiệm vụ 0: nhat item--- 1: kill monster---2:
						// chuyển đồ--- 3: noi chuyện
						type_quest = m.reader().readByte();
						string in_help_response = m.reader().readUTF();
						string infoGift=m.reader().readUTF();
						
						// TODO: tạo ra 1 đối tượng quest add vao vector các
						// nhiem vu co the nhan cho nay
						q = new Quest(idQuest, main_quest, name_quest,
								idNpc_receive, content_conversation,
								type_quest, in_help_response,infoGift,"");
						Quest.listUnReceiveQuest.addElement(q);
						break;
					case 1:// nhóm các nhiệm vụ đã hoàn thành mà chưa trả nv
						idNpc_response = m.reader().readByte();// id npc tra
						// noi dung noi chuyện để trả nv
						string info_response = m.reader().readUTF();
						// nội dung hướng dẫn đi đến đâu để trả nv ( hiện trong
						// khung nv)
						string info_help_response = m.reader().readUTF();
						// TODO: tao 1 doi tuong quest add vao vector các nhiem
						// vu da hoan thanh cho nay
						q = new Quest(idQuest, main_quest, name_quest,
								idNpc_response, info_response,
								info_help_response,"");
						Quest.vecQuestFinish.addElement(q);
						break;
					case 2:// nhóm các nhiệm vụ đang làm
						Cout.println("nhiem vu dang lam");
						type_quest = m.reader().readByte();// loai nhiem vu
						// mô tả tóm tắ nhiệm vụ
						string short_decript = m.reader().readUTF();
						string help_decript = m.reader().readUTF();
						// npc trả nhiệm vụ sau khi hoàn thành
						idNpc_response = m.reader().readByte();
						Cout.println("nhiem vu dang lam  "+type_quest);
						if (type_quest == 0) {// nhat item
							// tổng số loại item phải làm
							sbyte totalItem = m.reader().readByte();
							// id các item cần phải lấy
							short[] item = new short[totalItem];
							// số item cần phải lấy
							short[] totalitem = new short[totalItem];
							// số item đã dc
							short[] nItemGot = new short[totalItem];
							for (sbyte j = 0; j < totalItem; j++) {
								item[j] = m.reader().readShort();
								nItemGot[j] = m.reader().readShort();
								totalitem[j] = m.reader().readShort();
							}
							q = new Quest(idQuest, main_quest, name_quest,
									help_decript, type_quest, short_decript,
									idNpc_response, item, totalitem, nItemGot,
									Quest.TYPE_ITEM,"0/0/0","");
						} else if (type_quest == 1) {// kill mons
							sbyte totalMonsBeKilled = m.reader().readByte();
							// tuong tự bản item
							short[] idMons = new short[totalMonsBeKilled];
							short[] totalMons = new short[totalMonsBeKilled];
							short[] nMonsKilled = new short[totalMonsBeKilled];
							for (sbyte j = 0; j < totalMonsBeKilled; j++) {
								idMons[j] = m.reader().readShort();
								nMonsKilled[j] = m.reader().readShort();
								totalMons[j] = m.reader().readShort();
							}
							q = new Quest(idQuest, main_quest, name_quest,
									help_decript, type_quest, short_decript,
									idNpc_response, idMons, totalMons,
									nMonsKilled, Quest.TYPE_MONSTER,"0/0/0","");

						} else if (type_quest == 2) {// chuyen item
							// idnpc trả đồ cũng như nv
							idNpc_response = m.reader().readByte();
							// nội dung nói chuyện khi trả đồ
							string info_talk = m.reader().readUTF();
							q = new Quest(idQuest, main_quest, name_quest,
									help_decript, idNpc_response, info_talk);
						}

						Cout.println("nhiem vu dang lam q "+q);
						// TODO: tao ra doi tuong quest add vao vector quest
						// đang làm
						if (q != null) {
							if (main_quest) {
								Quest.vecQuestDoing_Main.addElement(q);
							} else {
								Quest.vecQuestDoing_Sub.addElement(q);
							}
						}
						break;
					}
				}
				if (type_Group_Quest == 0) {

				} else if (type_Group_Quest == 1 || type_Group_Quest == 2) {
					((MainTabNew) GameCanvas.AllInfo.VecTabScreen
							.elementAt(MainTabNew.QUEST)).init();
				}
			} else {// client showMenu Nhận or trả nhiệm vụ tương ứng chỗ này
				sbyte idNpc = m.reader().readByte();// idNpc tra or nhan nhiem vu
				sbyte type = m.reader().readByte();// 0-nhận nv, 1 - trả nv
				// mSystem.outz("chua dung 2 bien nay idNPC=" + idNpc + " type="
				// + type);
				// TODO: showMenu response or receive here
			}
//			for (int i = 0; i < GameScr.Vecplayers.size(); i++) {
//				MainObject obj = (MainObject) GameScreen.Vecplayers
//						.elementAt(i);
//				if (obj.typeObject == MainObject.CAT_NPC) {
//					if (type_Group_Quest == 0) {
//						if (obj.typeNPC == 1)
//							obj.typeNPC = 0;
//					} else if (type_Group_Quest == 1) {
//						if (obj.typeNPC == 3)
//							obj.typeNPC = 0;
//					} else if (type_Group_Quest == 2) {
//						if (obj.typeNPC == 2)
//							obj.typeNPC = 0;
//					}
//					for (int j = 0; j < MainQuest.vecQuestList.size(); j++) {
//						MainQuest quest = (MainQuest) MainQuest.vecQuestList
//								.elementAt(j);
//						if (quest.idNPC_From == obj.ID) {
//							if (obj.typeNPC != 3)
//								obj.typeNPC = 1;
//							// mSystem.out("list " + obj.name);
//						}
//					}
//
//					for (int j = 0; j < MainQuest.vecQuestFinish.size(); j++) {
//						MainQuest quest = (MainQuest) MainQuest.vecQuestFinish
//								.elementAt(j);
//
//						if (quest.idNPC_To == obj.ID) {
//							obj.typeNPC = 3;
//							// mSystem.out("finish " + obj.name);
//						}
//					}
//
//					for (int j = 0; j < MainQuest.vecQuestDoing_Main.size(); j++) {
//						MainQuest quest = (MainQuest) MainQuest.vecQuestDoing_Main
//								.elementAt(j);
//
//						if (quest.idNPC_To == obj.ID) {
//							// mSystem.out("main " + obj.name);
//							if (obj.typeNPC == 0)
//								obj.typeNPC = 2;
//						}
//					}
//
//					for (int j = 0; j < MainQuest.vecQuestDoing_Sub.size(); j++) {
//						MainQuest quest = (MainQuest) MainQuest.vecQuestDoing_Sub
//								.elementAt(j);
//
//						if (quest.idNPC_To == obj.ID) {
//							// mSystem.out("sub " + obj.name);
//							if (obj.typeNPC == 0)
//								obj.typeNPC = 2;
//						}
//					}
//				}
//			}
//			for (int k = 0; k < MiniMap.vecNPC_Map.size(); k++) {
//				NPCMini npc = (NPCMini) MiniMap.vecNPC_Map.elementAt(k);
//				if (type_Group_Quest == 0) {
//					if (npc.type == 1)
//						npc.type = 0;
//				} else if (type_Group_Quest == 1) {
//					if (npc.type == 3)
//						npc.type = 0;
//				} else if (type_Group_Quest == 2) {
//					if (npc.type == 2)
//						npc.type = 0;
//				}
//				MainObject obj = MainObject.get_Object(npc.ID,
//						MainObject.CAT_NPC);
//				if (obj != null && !obj.isRemove)
//					npc.type = obj.typeNPC;
//			}

		} catch (Exception e) {
		}
	}

	public void show_Info_Quest_Doing() {
		// TODO Auto-generated method stub

		step = 0;
		nextStep(true);//dag lam
	}
}
