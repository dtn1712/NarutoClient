

public class MobAttack {
	public int x, y, vx, vy, indexM, dam, dirInjure, indexChar, cx, cy, type,
			xTo, yTo, d, statusM;
	public static int[] w = new int[] { 10, 5, 11 }, h = new int[] { 10, 5,
			11 }, maxIndex = new int[] { 4, 1, 4 };

	// state==0: rượt bình thường, 1: lên trên rớt xuống,2: lên trên rượt,3:

	public MobAttack(int type, int x, int y, int xTo, int yTo, int indexChar,
			int state) {

		this.x = x;
		this.y = y;
		this.type = type;
		this.xTo = xTo;
		this.yTo = yTo;
		this.statusM = state;
		dirInjure = this.xTo > this.x ? 1 : -1;
		this.indexChar = indexChar;
		d = 10;
		indexM = -1;
		if (state == StaticObj.NORMAL) {
			vx = Res.xetVX(Res.angle(xTo - x, (yTo - y)), d);
			vy = Res.xetVY(Res.angle(xTo - x, (yTo - y)), d);
		} else {

			vy = -12;
			if (state == StaticObj.UP_RUN) {
				vx = 0;
				vy = -14;
			}
			if (state == StaticObj.UP_FALL)
				vx = -5;
			if (state == StaticObj.FALL_RIGHT)
				vx = 5;

		}
		if (state == StaticObj.UP_RUN) {
			GameScr.vMobAttack.addElement(new MobAttack(type, x, y, xTo, yTo,
					indexChar, 1));
			GameScr.vMobAttack.addElement(new MobAttack(type, x, y, xTo, yTo,
					indexChar, 3));
		}
	}

	public void update() {

		if (statusM > StaticObj.NORMAL && statusM < StaticObj.FALL_LEFT) {
			x += vx;
			y += vy;
			vy += 2;

			if (vx < -2)
				vx++;
			if (vx > 2)
				vx--;
			if (vy == 6) {
				if (statusM == StaticObj.UP_RUN)
					statusM = StaticObj.NORMAL;
				else
					statusM = StaticObj.FALL_LEFT;
				d = 11;
				vx = Res.xetVX(Res.angle(xTo - x, (yTo - y)), d);
				vy = Res.xetVY(Res.angle(xTo - x, (yTo - y)), d);
			}
			return;
		}
		Char c = null;
		if (indexChar == StaticObj.MOD_ATTACK_ME)
			c = Char.myChar();
		else
			c = ((Char) GameScr.vCharInMap.elementAt(indexChar));
		xTo = c.cx;
		yTo = c.cy - c.chh;
		
		x += vx;

		y += vy;

		d++;

		vx = Res.xetVX(Res.angle(xTo - x, (yTo - y)), d);
		vy = Res.xetVY(Res.angle(xTo - x, (yTo - y)), d);
		indexM++;
		if (indexM == maxIndex[type])
			indexM = 0;
		if (x >= xTo + c.chw || x <= xTo - c.chw || y >= yTo + c.chh
				|| y <= yTo - c.chh)
			return;
//		if (statusM == StaticObj.NORMAL || statusM == StaticObj.UP_RUN)
//			if (indexChar == StaticObj.MOD_ATTACK_ME)
//				Char.myChar().injureChar(dam, dirInjure);

		GameScr.vMobAttack.removeElement(this);

	}

	public void paint(mGraphics g) {
	}

	public static int getType(int mobID) {
		return 0;
	}

}