

using System;
public class Char
{
    public sbyte typePk = -1;
    public QuickSlot[] mQuickslot;
	public Char partnerTrade; 
	public bool isMob, isCrit, isDie;
	public bool isHumann , isNhanbann;
	private int tickEffWolf = 0, timeBocdau = 0;
	public long lastUpdateTime;
	public const sbyte A_STAND = 1, A_RUN = 2, A_JUMP = 3, A_FALL = 4, A_DEADFLY = 5, A_NOTHING = 6, A_ATTK = 7, A_INJURE = 8, A_AUTOJUMP = 9, A_WATERRUN = 10, A_WATERDOWN = 11, SKILL_STAND = 12, SKILL_FALL = 13, A_DEAD = 14, A_HIDE = 15;
	public ChatPopup chatPopup;
    public long cEXP = 0, cExpDown;
    public int cx = 24, cy = 24 /*52 * 24 - 48*/, cybong;
    public int lcx, lcy, cvx, cvy, cp1, cp2, cp3, statusMe = 5, cdir = 1, charID, cgender, ctaskId;
    public int cBonusSpeed, cspeed, ccurrentAttack, cdame, cdameDown, clevel, cClass, cMP, cMaxMP = 100, cHP, cHpNew, cMaxHP = 100, cMaxEXP = 100, HPShow, xReload, yReload,
            cyStartFall, saveStatus, eff5BuffHp, eff5BuffMp, autoUpHp;
    public long cExpR;
	public int pPoint, sPoint, pointUydanh, pointNon, pointVukhi, pointAo, pointLien, pointGangtay, pointNhan, pointQuan, pointNgocboi, pointGiay,
			pointPhu, pointTinhTu, countFinishDay, countLoopBoos, limitTiemnangso, limitKynangso, limitPhongLoi, limitBangHoa, countPB;
	public short[] potential = new short[4];
	public string cName, cClanName = "";
	public byte ctypeClan;
	public static Clan clan;
	public int cw = 22, ch = 50, chw = 11, chh = 16;
    public bool canJumpHigh = true, cmtoChar, me, isStartSoundRun, cchistlast, isAttack, isAttFly;
    public int cf, tick; // Weapon
    public static bool fallAttack;
    public bool isJump, autoFall, attack = true, isMoto = false, isWolf = false, isMotoBehind, isBocdau;
    //	public int xu;
    public long xu, luong;
    public int xuInBox;
    public int yen;
    public int gold_lock;

	public NClass nClass;
	public mVector vSkill = new mVector(), vSkillFight = new mVector(), vEff = new mVector(), vDomsang = new mVector();
	public Skill myskill;
	public Task taskMaint;
	public bool paintName = true;
	public Item[] ItemMyTrade = new Item[8];
	public Item[] ItemParnerTrade = new Item[8];
	public Item[] arrItemBag;
	public Item[] arrItemBox;
	public Item[] arrItemBody;
	public Item[] arrItemMounts = new Item[5];
	public byte[] mountsPoint = new byte[10];
	public short cResFire;
	public short cResIce;
	public short cResWind;
	public short cMiss;
	public short cExactly;
	public short cFatal;
	public byte cPk, cTypePk;
	public short cReactDame;
	public short sysUp;
	public short sysDown;

	public int skillTemplateId;

	public Mob mobFocus, mobMe;
	public Npc npcFocus;
	public Char charFocus;
	public ItemMap itemFocus;
	public mVector focus = new mVector();
	public Mob[] attMobs;

	public Char[] attChars;
	public short[] moveFast;
	public int testCharId = -9999;
	public int killCharId = -9999;
	public byte resultTest;
	public int countKill, countKillMax, tickCoat , tickEffmoto, tickEffFireW;
	public bool isInvisible;

	public static bool isAHP;
	public static bool isAMP;
	public static bool isAFood;
	public static bool isABuff;
	public static bool isAPickYen;
	public static bool isAPickYHM;
	public static bool isAPickYHMS;
	public static bool isANoPick;
	public static bool isAResuscitate;
	public static bool isAFocusDie;

	public static int aHpValue = 20;
	public static int aMpValue = 20;
	public static int aFoodValue = 70;
	public static short aCID; // int sua lai short
	public long lastTimeUseSkill = 0;

	public const sbyte PK_NORMAL = 0;
	public const sbyte PK_NHOM = 1;
	public const sbyte PK_BANG = 2;
	public const sbyte PK_DOSAT = 3;
	public const sbyte PK_PHE1 = 4;
	public const sbyte PK_PHE2 = 5;
	public mVector taskOrders = new mVector();
	public static int pointPB, pointChienTruong;
	public long timeStartBlink, timeSummon;
    public long timeChatReturn = 0, timeRequesChangeMap; // time cấm chat
	public bool isPolycy = false; // quyền cầm chat
	public bool isLeaderParty = false;
	
	public short idParty = -1;
	public bool isLeader = false;
	
	public mVector vFriend =  new mVector();
	public short idFriend = -1;
	public short CharidDB;
    public bool isOnline;
    public int totalTN = 0;
    public int[] diemTN, subTn;
	
	public static Char toCharChat;
	public static OtherChar toCharChatSelected;

    public static int[][][] CharInfo = new int[][][] {
	// Head, Leg, Body, Weapon
		new int [][]	{new int [] { 0, -10, 32 }, new int []{ 1, -7, 7 },new int [] { 1, -11, 15 },new int [] { 1, -9, 45 } }, // Stand0
			// -
			// [0]
		new int [][]	{new int [] { 0, -10, 33 },new int [] { 1, -7, 7 }, new int []{ 1, -11, 16 },new int [] { 1, -9, 46 } }, // Stand1
			// -
			// [1]
			// ==
		new int [][]	{new int [] { 1, -10, 33 }, new int []{ 2, -10, 11 }, new int []{ 2, -9, 16 }, new int []{ 1, -12, 49 } }, // Run0
			// -
			// [2]
		new int [][]	{ new int []{ 1, -10, 32 },new int [] { 3, -11, 9 },new int [] { 3, -11, 16 },new int [] { 1, -13, 47 } }, // Run1
			// -
			// [3]
		new int [][]	{ new int []{ 1, -10, 34 },new int [] { 4, -9, 9 }, new int []{ 4, -8, 16 }, new int []{ 1, -12, 47 } }, // Run2
			// -
			// [4]
		new int [][]	{new int [] { 1, -10, 34 }, new int []{ 5, -11, 11 }, new int []{ 5, -10, 17 }, new int []{ 1, -13, 49 } }, // Run3
			// -
			// [5]
		new int [][]	{new int [] { 1, -10, 33 }, new int []{ 6, -9, 9 }, new int []{ 6, -8, 16 }, new int []{ 1, -12, 47 } }, // Run4
			
//			{ { 1, -10, 32 }, { 6, -9, 9 }, { 6, -8, 16 }, { 1, -12, 47 } }, // Run them 1 frame
			// -
			// [6]
			// ==
		new int [][]	{new int [] { 0, -9, 36 },new int [] { 7, -5, 15 },new int [] { 7, -10, 21 },new int [] { 1, -8, 49 } }, // Jump0
			// -
			// [7]
		new int [][]	{ new int []{ 4, -13, 26 }, new int []{ 0, 0, 0 },new int [] { 0, 0, 0 }, new int []{ 0, 0, 0 } }, // JumpRotate0
			// - [8]
		new int [][]	{new int [] { 5, -13, 25 },new int [] { 0, 0, 0 }, new int []{ 0, 0, 0 }, new int []{ 0, 0, 0 } }, // JumpRotate1
			// - [9]
		new int [][]	{ new int []{ 6, -12, 26 }, new int []{ 0, 0, 0 }, new int []{ 0, 0, 0 },new int [] { 0, 0, 0 } }, // JumpRotate2
			// -
			// [10]
		new int [][]	{new int [] { 7, -13, 25 }, new int []{ 0, 0, 0 }, new int []{ 0, 0, 0 }, new int []{ 0, 0, 0 } }, // JumpRotate3
			// -
			// [11]
		new int [][]	{ new int []{ 0, -9, 35 },new int [] { 8, -4, 13 }, new int []{ 8, -14, 27 },new int [] { 1, -9, 49 } }, // Fall0
			// -
			// [12]
			// ==
		new int [][]	{ new int []{ 0, -9, 31 }, new int []{ 9, -11, 8 },new int [] { 10, -10, 17 }, new int []{ 0, 0, 0 } }, // Attak0
			// -
			// [13]
		new int [][]	{ new int []{ 2, -7, 33 }, new int []{ 9, -11, 8 },new int [] { 11, -8, 15 }, new int []{ 0, 0, 0 } }, // Attak1
			// -
			// [14]
		new int [][]	{ new int []{ 2, -8, 32 }, new int []{ 9, -11, 8 }, new int []{ 12, -8, 14 }, new int []{ 0, 0, 0 } }, // Attak2
			// -
			// [15]
		new int [][]	{ new int []{ 2, -7, 32 },new int [] { 9, -11, 8 }, new int []{ 13, -12, 15 }, new int []{ 0, 0, 0 } }, // Attak3
			// -
			// [16]
		new int [][]	{ new int []{ 0, -11, 31 }, new int []{ 9, -11, 8 }, new int []{ 14, -15, 18 }, new int []{ 0, 0, 0 } }, // Attak4
			// -
			// [17]
		new int [][]	{ new int []{ 2, -9, 32 }, new int []{ 9, -11, 8 }, new int []{ 15, -13, 19 },new int [] { 0, 0, 0 } }, // Attak5
			// -
			// [18]
		new int [][]	{ new int []{ 2, -9, 31 },new int [] { 9, -11, 8 }, new int []{ 16, -7, 22 },new int [] { 0, 0, 0 } }, // Attak6
			// -
			// [19]
		new int [][]	{ new int []{ 2, -9, 32 },new int [] { 9, -11, 8 },new int [] { 17, -11, 18 },new int [] { 0, 0, 0 } }, // Attak7
			// -
			// [20]
			// ==
		new int [][]	{ new int []{ 3, -12, 34 },new int [] { 8, -4, 13 },new int [] { 8, -15, 25 }, new int []{ 1, -10, 46 } }, // Injure
			// -
			// [21]
			// ==
		new int [][]	{new int [] { 0, -9, 32 }, new int []{ 8, -4, 9 }, new int []{ 10, -10, 18 },new int [] { 0, 0, 0 } }, // FallAttak0
			// -
			// 22
		new int [][]	{ new int []{ 2, -7, 34 }, new int []{ 8, -4, 9 }, new int []{ 11, -8, 16 }, new int []{ 0, 0, 0 } }, // FallAttak1
			// -
			// 23
		new int [][]	{new int [] { 2, -8, 33 }, new int []{ 8, -4, 9 }, new int []{ 12, -8, 15 }, new int []{ 0, 0, 0 } }, // FallAttak2
			// -
			// 24
		new int [][]	{ new int []{ 2, -7, 33 },new int [] { 8, -4, 9 }, new int []{ 13, -12, 16 },new int [] { 0, 0, 0 } }, // FallAttak3
			// -
			// 25
		new int [][]	{ new int []{ 0, -11, 32 }, new int []{ 7, -5, 9 },new int [] { 14, -15, 19 }, new int []{ 0, 0, 0 } }, // FallAttak4
			// -
			// 26
		new int [][]	{ new int []{ 2, -9, 33 },new int [] { 7, -5, 9 },new int [] { 15, -13, 20 },new int [] { 0, 0, 0 } }, // FallAttak5
			// -
			// 27
		new int [][]	{ new int []{ 2, -9, 32 }, new int []{ 7, -5, 9 }, new int []{ 16, -7, 23 },new int [] { 0, 0, 0 } }, // FallAttak6
			// -
			// 28
		new int [][]	{ new int []{ 2, -9, 33 }, new int []{ 7, -5, 9 },new int [] { 17, -11, 19 }, new int []{ 0, 0, 0 } }, // FallAttak7
	};
	public int getdxSkill() {
        if (Char.myChar().mQuickslot[0] != null)
        {
            QuickSlot ql = Char.myChar().mQuickslot[0];
            if (ql.idSkill != -1)
            {
                SkillTemplate skill = (SkillTemplate)SkillTemplates.hSkilltemplate.get("" + ql.idSkill);
                if (skill != null && skill.level >= 0 && skill.level < skill.rangelv.Length)
                {
                    return skill.rangelv[skill.level];
                }
                else if (skill != null)
                {
                    return skill.rangelv[0];
                }
            }
        }
        else if (myskill != null)
            return myskill.dx;
        return 0;
    }
	public int getdySkill() {
		if (myskill != null)
			return myskill.dy;
		return 0;
	}

	

	public static int[] CHAR_WEAPONX = new int []{ -2, -6, 22, 21, 19, 22, 10, -2, -2, 5, 19 };
	public static int[] CHAR_WEAPONY = new int []{ 9, 22, 25, 17, 26, 37, 36, 49, 50, 52, 36 };
	public static Char myCharr;
	// Focus
	public static int[] listAttack;// danh sach nhung con qv co the bi danh
	public static int[][] listIonC;// danh sach nhung do da duoc trang bi tren
	public int cvyJump;
	int indexUseSkill = -1;

	public int cxSend, cySend, cdirSend = 1, cxFocus, cyFocus, cxMoveLast, cyMoveLast, cactFirst = 5;
	//

	public mVector vMovePoints = new mVector();// luu lai nhung vi tri nguoi choi ko phai la minh phai di qua.

	public static bool flag;
	public static bool ischangingMap, isLockKey;
	public bool isLockMove, isLockAttack, isBlinking;

	public Char() {
//		statusMe = A_NOTHING;
		statusMe = A_STAND;
	}

	public int getSys() {
		if (nClass.classId == 1 || nClass.classId == 2)
			return 1;
		else if (nClass.classId == 3 || nClass.classId == 4)
			return 2;
		else if (nClass.classId == 5 || nClass.classId == 6)
			return 3;
		return 0;
	}
	public int getSpeed() {
		if(isMoto && isBocdau &&cspeed < 7)
			return 7;
		else if((isWolf || isMoto) && cspeed < 6)
			return 6;
		else 
			return 6;
	}

	public bool isUseLongRangeWeapon() {
		return (nClass.classId == 2 || nClass.classId == 4 || nClass.classId == 6);
	}

	public static Char myChar() {
		if (myCharr == null) {
			myCharr = new Char();
			myCharr.me = true;
			myCharr.cmtoChar = true;
            myCharr.mQuickslot = new QuickSlot[5];
            for (int i = 0; i < 5; i++)
            {
                myCharr.mQuickslot[i] = new QuickSlot();
            }
		}
		return myCharr;
	}

	public static void clearMyChar() {
		isABuff = isAFocusDie = isAFood = isAHP = isAMP = isANoPick = isAPickYen = isAPickYHM = isAPickYHMS = isAResuscitate = false;
		myCharr = null;
	}

	public void readParam(Message msg) {
		try {
			cspeed = msg.reader().readByte();
			cMaxHP = msg.reader().readUnsignedShort();
			cMaxMP = msg.reader().readUnsignedShort();
		} catch (Exception e) {
            //e.printStackTrace();
            //System.out.println("Char.readParam()");
		}
	}

	public void bagSort() {
		try {
			mVector items = new mVector();
			for (int i = 0; i < arrItemBag.Length; i++) {
				Item item = arrItemBag[i];
				if (item != null && item.template.isUpToUp && !item.isExpires) {
					items.addElement(item);
				}
			}
			for (int i = 0; i < items.size(); i++) {
				Item itemi = (Item) items.elementAt(i);
				if (itemi != null) {
					for (int j = i + 1; j < items.size(); j++) {
						Item itemj = (Item) items.elementAt(j);
						if (itemj != null && itemi.template.Equals(itemj.template) && itemi.isLock == itemj.isLock) {
							itemi.quantity += itemj.quantity;
							arrItemBag[itemj.indexUI] = null;
							items.setElementAt(null, j);
						}
					}
				}
			}
			for (int i = 0; i < arrItemBag.Length; i++) {
				if (arrItemBag[i] != null) {
					for (int j = 0; j <= i; j++) {
						if (arrItemBag[j] == null) {
							arrItemBag[j] = arrItemBag[i];
							arrItemBag[j].indexUI = j;
							arrItemBag[i] = null;
							break;
						}
					}
				}
			}
		} catch (Exception e) {
            //e.printStackTrace();
            //System.out.println("Char.bagSort()");
		}
	}

	public void boxSort() {
		try {
			mVector items = new mVector();
			for (int i = 0; i < arrItemBox.Length; i++) {
				Item item = arrItemBox[i];
				if (item != null && item.template.isUpToUp && !item.isExpires) {
					items.addElement(item);
				}
			}
			for (int i = 0; i < items.size(); i++) {
				Item itemi = (Item) items.elementAt(i);
				if (itemi != null) {
					for (int j = i + 1; j < items.size(); j++) {
						Item itemj = (Item) items.elementAt(j);
						if (itemj != null && itemi.template.Equals(itemj.template) && itemi.isLock == itemj.isLock) {
							itemi.quantity += itemj.quantity;
							arrItemBox[itemj.indexUI] = null;
							items.setElementAt(null, j);
						}
					}
				}
			}
			for (int i = 0; i < arrItemBox.Length; i++) {
				if (arrItemBox[i] != null) {
					for (int j = 0; j <= i; j++) {
						if (arrItemBox[j] == null) {
							arrItemBox[j] = arrItemBox[i];
							arrItemBox[j].indexUI = j;
							arrItemBox[i] = null;
							break;
						}
					}
				}
			}
		} catch (Exception e) {
            //e.printStackTrace();
            //System.out.println("Char.boxSort()");
		}
	}

//	public void useItem(int indexUI) {
//		Item item = arrItemBag[indexUI];
//		if (item.isTypeBody()) {
//			item.isLock = true;
//			item.typeUI = Item.UI_BODY;
//			Item itemBody = arrItemBody[item.template.type];
//			arrItemBag[indexUI] = null;
//			if (itemBody != null) {
//				itemBody.typeUI = Item.UI_BAG;
//				arrItemBody[item.template.type] = null;
//				itemBody.indexUI = indexUI;
//				arrItemBag[indexUI] = itemBody;
//			}
//			item.indexUI = item.template.type;
//			arrItemBody[item.indexUI] = item;
//			for (int i = 0; i < arrItemBody.Length; i++) {
//				Item ib = arrItemBody[i];
//				if (ib != null) {
//					if (ib.template.type == Item.TYPE_VUKHI)
//						wp = ib.template.part;
//					else if (ib.template.type == Item.TYPE_AO)
//						body = ib.template.part;
//					else if (ib.template.type == Item.TYPE_QUAN)
//						leg = ib.template.part;
//				}
//			}
//		} else if (item.isTypeMounts()) {
//			item.isLock = true;
//			item.typeUI = Item.UI_MON;
//			Item itemMon;
//			arrItemBag[indexUI] = null;
//			for (int i = 0; i < arrItemMounts.Length; i++) {
//				int index = item.template.type - Item.TYPE_MON0;
//				if (index == i) {
//					itemMon = arrItemMounts[index];
//					if (itemMon != null) {
//						itemMon.typeUI = Item.UI_MON;
//						arrItemMounts[index] = null;
//						itemMon.indexUI = indexUI;
//						arrItemBag[indexUI] = itemMon;
//					}
//					item.indexUI = item.template.type;
//					arrItemMounts[index] = item;
//					break;
//				}
//			}
//		}
//	}

	public Skill getSkill(SkillTemplate skillTemplate) {
		for (int i = 0; i < vSkill.size(); i++) {
//			System.out.println("vSkill.elementAt(i)).template ---> "+(vSkill.elementAt(i))).);
			if (((Skill) vSkill.elementAt(i)).template.Equals(skillTemplate)) {
				return (Skill) vSkill.elementAt(i);
			}
		}
		return null;
	}

	public bool isInWaypoint() {
		int size = TileMap.vGo.size();
		for (byte i = 0; i < size; i++) {
			Waypoint wp = (Waypoint) TileMap.vGo.elementAt(i);
			if (cx < wp.minX || cx > wp.maxX || cy < wp.minY || cy > wp.maxY)
				continue;
			return true;
		}
		return false;
	}

	public MovePoint currentMovePoint;
	public int bom = 0, count = 0;
	public bool isEffBatTu;
	public long lastUseHP = mSystem.currentTimeMillis();
	public int vitaWolf = 0;
	public static long timedelayloadmap = 0;
    public long timedelay = 5000;
	public long timeLastchangeMap = 0;
	public virtual void update() {
        if (ischangingMap)
            return;
        if (statusMe == A_DEAD)
        {
            if (!((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP))
            {
                cvy++;
                cy += cvy;
                Service.gI().charMove();
                if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP)
                {
                    cvy = 0;
                    cy = TileMap.tileXofPixel(cy + 3);
                    Service.gI().charMove();
                }
                if (cmtoChar)
                {
                    //					Char.myChar().cx = 300;
                    //					Char.myChar().cy = 300;
                    GameScr.cmtoX = cx - GameScr.gW2;
                    GameScr.cmtoY = cy - GameScr.gH23;
                    //					System.out.println("CHAR X,Y -----> "+cx+" "+cy+" "+GameScr.gW2+" "+GameScr.gH23);
                    //					System.out.println("CAMERACHAR -----> "+GameScr.cmtoX+" ::: "+GameScr.cmtoY);

                    if (!GameCanvas.isTouchControl)
                    {
                        GameScr.cmtoX += GameScr.gW6 * cdir;
                    }

                }

            }
            return;
        }
        updateShadown();
        //updateEffectWolf();
//		loadFromServer();

		if (arr != null) {
			arr.update();
		}
//		if (isMoto && System.currentTimeMillis() - timeSummon > 1000) {
//			isMoto = false;
//		}
		
			
	
		

		if (cmtoChar) {
//			Char.myChar().cx = 300;
//			Char.myChar().cy = 300;
			GameScr.cmtoX = cx - GameScr.gW2;
			GameScr.cmtoY = cy - GameScr.gH23;
//			System.out.println("CHAR X,Y -----> "+cx+" "+cy+" "+GameScr.gW2+" "+GameScr.gH23);
//			System.out.println("CAMERACHAR -----> "+GameScr.cmtoX+" ::: "+GameScr.cmtoY);

			if (!GameCanvas.isTouchControl) {
				GameScr.cmtoX += GameScr.gW6 * cdir;
			}

		}
		updateSkillPaint();
		tick = (tick + 1) % 100;
		if (me) {
            if (statusMe != A_RUN && isStartSoundRun && GameCanvas.gameTick % 11 == 0)
            {
                isStartSoundRun = false;
                Music.stopSound(Music.RUN);
            }
            if (charFocus != null && (charFocus.isNhanban() || !GameScr.vCharInMap.contains(charFocus)))
                charFocus = null;

            if (cx < 40)
            {
                cvx = 0;
                cx = 40;
            }
            else if (cx > TileMap.pxw - 10)
            {
                cx = TileMap.pxw - 10;
                cvx = 0;

            }
            if (!ischangingMap && isInWaypoint())
            {
                timeRequesChangeMap--;
                if (timeRequesChangeMap <= 0)
                {
                    timeRequesChangeMap = 20;
                    Service.gI().charMove();
                }
                if (mSystem.currentTimeMillis() - (timeLastchangeMap + timedelay) > 0)
                {
                    timeLastchangeMap = mSystem.currentTimeMillis();
                    Service.gI().requestChangeMap();
                }
                timedelayloadmap = mSystem.currentTimeMillis();
                //				isLockKey = true;
                //				ischangingMap = true;
                GameCanvas.clearKeyHold();
                GameCanvas.clearKeyPressed();
                //				return;
            }
            else if (isBlinking)
            {
                isBlinking = ((mSystem.currentTimeMillis() - timeStartBlink) < 2000);
            }
            else if (statusMe != A_FALL)
            {
                if (Res.abs(cx - cxSend) >= 40 || Res.abs(cy - cySend) >= 90)
                {
                    if ((cy - cySend) <= 0)
                    {
                        if (me)
                            Service.gI().charMove();
                    }
                }
            }
            if (isLockMove)
            {
                currentMovePoint = null;
            }

			if (currentMovePoint != null && (statusMe == A_STAND || statusMe == A_RUN)) {

				statusMe = A_RUN;
				if (cx - currentMovePoint.xEnd > 0) {
					cdir = -1;
					if (cx - currentMovePoint.xEnd <= 10) {
						currentMovePoint = null;
					}
				} else {
					cdir = 1;
					if (cx - currentMovePoint.xEnd >= -10) {
						currentMovePoint = null;
					}
				}
				if (currentMovePoint != null) {
					cvx = getSpeed() * cdir;
					cvy = 0;
				}
			}

			autoPickItemMap();
			searchFocus();

		} else // char khác không phải là mình
		{


			if (GameCanvas.gameTick % 20 == 0 && charID >= 0) // Auto hide name
			{
				paintName = true;
				for (int i = 0; i < GameScr.vCharInMap.size(); i++) {
					Char c = null;
					try {
						c = (Char) GameScr.vCharInMap.elementAt(i);
					} catch (Exception e) {
					}
					if (c == null || c.Equals(this))
						continue;
					if ((c.cy == cy && Res.abs(c.cx - cx) < 35) || (cy - c.cy < 32 && cy - c.cy > 0 && Res.abs(c.cx - cx) < 24))
						paintName = false;
				}
// for (int i = 0; i < GameScr.vNpc.size(); i++) {
// Npc c = null;
// try {
// c = (Npc) GameScr.vNpc.elementAt(i);
// } catch (Exception e) {
// }
// if (c == null)
// continue;
// if (c.cy == cy && Res.abs(c.cx - cx) < 24)
// paintName = false;
// }
			}
		
			if ((statusMe == A_STAND || statusMe == A_NOTHING)) {
				bool processNextPoint = false;
				if (currentMovePoint != null) {
					if (abs(currentMovePoint.xEnd - cx) < 4 && abs(currentMovePoint.yEnd - cy) < 4) {
						cx = currentMovePoint.xEnd;
						cy = currentMovePoint.yEnd;
						currentMovePoint = null;
						
						if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP) {
							changeStatusStand();
							GameCanvas.gI().startDust(-1, cx - (-1 << 3), cy);
							GameCanvas.gI().startDust(1, cx - (1 << 3), cy);
						} else {
							statusMe = A_FALL;
							cvy = 0;
						}
						processNextPoint = true;
					} else {
						if (cy == currentMovePoint.yEnd) // RUNNING
						{
							if (cx != currentMovePoint.xEnd) {

								cx = (cx + currentMovePoint.xEnd) / 2;
								cf = GameCanvas.gameTick % 5 + 2;
							}

						} else if (cy < currentMovePoint.yEnd) {
							cf = 12;
							cx = (cx + currentMovePoint.xEnd) / 2;
							if (cvy < 0)
								cvy = 0;
							cy += cvy;
							if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP) {
								GameCanvas.gI().startDust(-1, cx - (-1 << 3), cy);
								GameCanvas.gI().startDust(1, cx - (1 << 3), cy);
							}
							cvy++;
							if (cvy > 16) {
								cy = (cy + currentMovePoint.yEnd) / 2;
							}
						} else {
							cf = 7;
							cx = (cx + currentMovePoint.xEnd) / 2;
							cy = (cy + currentMovePoint.yEnd) / 2;
						}

					}
				} else
					processNextPoint = true;
				if (processNextPoint && vMovePoints.size() > 0) {
					currentMovePoint = (MovePoint) vMovePoints.firstElement();
					vMovePoints.removeElementAt(0);
					if (currentMovePoint.status == A_RUN) {
						statusMe = A_RUN;
						if (cx - currentMovePoint.xEnd > 0)
							cdir = -1;
						else if (cx - currentMovePoint.xEnd < 0)
							cdir = 1;
						cvx = 5 * cdir;
						cvy = 0;
					} else if (currentMovePoint.status == A_JUMP) {
						statusMe = A_JUMP;
						GameCanvas.gI().startDust(-1, cx - (-1 << 3), cy);
						GameCanvas.gI().startDust(1, cx - (1 << 3), cy);
						if (cx - currentMovePoint.xEnd > 0)
							cdir = -1;
						else if (cx - currentMovePoint.xEnd < 0)
							cdir = 1;
						cvx = abs(cx - currentMovePoint.xEnd) / 9 * cdir;
						cvy = -10;
					} else if (currentMovePoint.status == A_FALL) {
						statusMe = A_FALL;
						if (cx - currentMovePoint.xEnd > 0)
							cdir = -1;
						else if (cx - currentMovePoint.xEnd < 0)
							cdir = 1;
						cvx = abs(cx - currentMovePoint.xEnd) / 9 * cdir;
						cvy = 0;
					} else {
						cx = currentMovePoint.xEnd;
						cy = currentMovePoint.yEnd;
						currentMovePoint = null;
					}
				}
				if (statusMe == A_NOTHING) {
					if (cf >= 8 && cf <= 11) {
						cf++;
						cp1++;
						if (cf > 11)
							cf = 8;
						if (cp1 > 5)
							cf = 0;
					}
					if (cf <= 1) {
						cp1++;
						if (cp1 > 6)
							cf = 0;
						else
							cf = 1;
						if (cp1 > 10)
							cp1 = 0;
					}
				}
				lcx = cx;
				lcy = cy;
			}
		}
		
	
		if (isInjure > 0) {
			cf = 21;
			isInjure--;
		} else
			switch (statusMe) {
			case A_STAND:
				updateCharStand();
				break;
			case A_RUN:
                if (me)
                {
                    if (GameCanvas.gameTick % 33 == 0)
                    {
                        Music.play(Music.RUN, 12);
                        isStartSoundRun = true;
                    }
                }
				updateCharRun();
				break;
			case A_JUMP:
				updateCharJump();
				break;
			case A_FALL:
				updateCharFall();
				break;
			case A_DEADFLY:
				updateCharDeadFly();
				break;
			case A_AUTOJUMP:
				updateCharAutoJump();
				break;
			case A_WATERRUN:
				updateCharWaterRun();
				break;
			case A_WATERDOWN:
				updateCharWaterDown();
				break;
			case SKILL_STAND:
				updateSkillStand();
				break;
			case SKILL_FALL:
				updateSkillFall();
				break;
			case A_DEAD:

				break;
			case A_NOTHING:
				if (cf == 21 && isInjure <= 0)
					cf = 0;
				break;
            case A_INJURE:
                if (cf == 21 && isInjure <= 0)
                    cf = 0;
                break;
			}
		if (wdx != 0 || wdy != 0) {
			startDie(wdx, wdy);
			wdx = 0;
			wdy = 0;
		}
		if (moveFast != null) {
			if (moveFast[0] == 0) {
				moveFast[0]++;
//				ServerEffect.addServerEffect(60, this, 1);
			} else if (moveFast[0] < 10)
				moveFast[0]++;
			else {
				cx = moveFast[1];
				cy = moveFast[2];
				moveFast = null;
//				ServerEffect.addServerEffect(60, this, 1);
				if (me) {
					if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) != TileMap.T_TOP) {
						statusMe = A_FALL;
//						Char.myChar().setAutoSkillPaint(GameScr.sks[38], Skill.ATT_FLY);
					} else {
						Service.gI().charMove();
//						Char.myChar().setAutoSkillPaint(GameScr.sks[38], Skill.ATT_STAND);
					}
				}
			}
		}
		if (!me && vMovePoints.size() == 0 && cxMoveLast != 0 && cyMoveLast != 0 && currentMovePoint == null) {
			if (cxMoveLast != cx)
				cx = cxMoveLast;
			if (cyMoveLast != cy)
				cy = cyMoveLast;
			if (cHP > 0)
				statusMe = A_NOTHING;
		}
//		if(arrItemBody != null){
//			//for(int i = 0; i < arrItemBody.Length; i++){ //effect áo mới
//					if(arrItemBody[Item.TYPE_AO] != null && arrItemBody[Item.TYPE_QUAN] != null){
//						if (GameCanvas.gameTick % 5 == 0){
//							ServerEffect.addServerEffect(136, this, 1);
//							ServerEffect.addServerEffect(134, this, 1);
//						}
//					}
//			//}
//		}
		
//		updateEffPhanthan();
	}

    public void updateShadown()
    {
        int wCount = 0;
        xSd = cx;
        // if (ySd <= 0)
        // return;
        if (TileMap.tileTypeAt(cx, cy, TileMap.T_TOP))
        {

            cybong = cy;
            return;
        }
        else
        {

            cybong = cy;
            while (wCount < 30)
            {
                wCount++;
                cybong += 24;

                if (TileMap.tileTypeAt(xSd, cybong, TileMap.T_TOP))
                {
                    if (cybong % 24 != 0)
                    {
                        cybong -= (cybong % 24);
                    }
                    break;
                }
            }
        }

    }

	private void autoPickItemMap() {
		if (!me || cHP <= 0 || statusMe == A_DEAD || statusMe == A_DEADFLY || testCharId != -9999)
			return;

		int rangePick = 30, dxx, dyy;
		if (isAPickYen || isAPickYHM || isAPickYHMS) {
			for (int i = 0; i < GameScr.vItemMap.size(); i++) {
				ItemMap itemMap = (ItemMap) GameScr.vItemMap.elementAt(i);
				if (itemMap == null)
					continue;
				dxx = Math.abs(Char.myChar().cx - itemMap.x);
				dyy = Math.abs(Char.myChar().cy - itemMap.y);
//				if (dxx <= rangePick && dyy <= rangePick) {
//					if ((isAPickYen || isAPickYHM || isAPickYHMS) && itemMap.template.type == Item.TYPE_MONEY)
//						Service.gI().pickItem(itemMap.itemMapID);
//					else if ((itemMap.template.type == Item.TYPE_HP || itemMap.template.type == Item.TYPE_MP) && (isAPickYHM || isAPickYHMS)) {
//						Service.gI().pickItem(itemMap.itemMapID);
//					} else if (itemMap.template.type == Item.TYPE_CRYSTAL && isAPickYHMS) {
//						Service.gI().pickItem(itemMap.itemMapID);
//					}
//
//				}
			}
		}
	}

	private void updateMobMe() {
		mobMe.xFirst = cx + (3 - GameCanvas.gameTick % 6) * 6;
		mobMe.yFirst = cy - 60;

		int dx = mobMe.xFirst - mobMe.x;
		int dy = mobMe.yFirst - mobMe.y;
		if (dx > 50 || dx < -50)
			mobMe.x += dx / 10;

		if (dy > 50 || dy < -50)
			mobMe.y += dy / 10;
		mobMe.update();
	}

	private void updateSkillPaint() {
        //		if (statusMe == A_DEAD || statusMe == A_DEADFLY)
        //			return;
        //		if (skillPaint != null && ((mobFocus != null))) {
        //			if (!me) {
        //				if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP){
        //					changeStatusStand();
        //				}
        //				else
        //					statusMe = A_NOTHING;
        //			}
        //			System.out.println("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
        //			indexSkill = 0;
        //			skillPaint = null;
        //			eff0 = eff1 = eff2 = null;
        //			i0 = i1 = i2 = 0;
        //			mobFocus = null;
        //			effPaints = null;
        //			arr = null;
        //		}
        if (GameCanvas.gameTick % 2 == 0 && skillPaint != null)
        {
            indexSkill++;
        }

        if (skillPaint != null && indexSkill >= skillInfoPaint().Length)
        {
            if (!me)
            {
                if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP)
                    changeStatusStand();
                else
                    statusMe = A_NOTHING;
            }
            indexSkill = -1;
            skillPaint = null;
            eff0 = eff1 = eff2 = null;
            i0 = i1 = i2 = 0;
            arr = null;



        }
		// Gui message tan cong hoac buff khi trinh dien nua chung
//		SkillInfoPaint[] skillInfoPaint = skillInfoPaint();
//		System.out.println("BAVVVVVVVVV");
//		System.out.println("Buffde ---> "+(skillInfoPaint.Length - skillInfoPaint.Length / 3));
//		if (skillInfoPaint != null) {
////			if (me && myskill.template.type == Skill.SKILL_CLICK_USE_BUFF) {
////				if (indexSkill == skillInfoPaint.Length - skillInfoPaint.Length / 3) {
//////					Service.gI().sendUseSkillMyBuff();
////					saveLoadPreviousSkill();
////				}
////			} else {
//				if (mobFocus != null
//						&& arr == null) {
//					if (indexSkill == skillInfoPaint.Length - skillInfoPaint.Length / 3) {
//						setAttack();
//						if (me)
//							saveLoadPreviousSkill();
//					}
//				}
////		}
//		}
	}

	public void saveLoadPreviousSkill() {
//		if (myskill.template.type != 1) // Khong phai chieu danh
//		{
//			if (lastNormalSkill != null) {
//				myskill = lastNormalSkill;
////				Service.gI().selectSkill(myskill.template.id);
//			}
//		}
//		if (currentFireByShortcut) {
//			if (lastNormalSkill != null) {
//				myskill = lastNormalSkill;
////				Service.gI().selectSkill(myskill.template.id);
//			}
//		} else
//			lastNormalSkill = myskill;

	}

	private void updateCharDeadFly() {
		cp1++;
		cx += (cp2 - cx) / 4;
		if (cp1 > 7)
			cy += (cp3 - cy) / 4;
		else
			cy += (cp1 - 10);
		if (Res.abs(cp2 - cx) < 4 && Res.abs(cp3 - cy) < 10) {
			cx = cp2;
			cy = cp3;
			statusMe = A_DEAD;
			callEff(60);
			if (me) {
				GameScr.gI().resetButton();
			}
		}
		cf = 21;
	}

	public void setAttk() {
		cp1++;
		if (cdir == 1) {
			if ((TileMap.tileTypeAtPixel(cx + chw, cy - chh) & TileMap.T_LEFT) == TileMap.T_LEFT) {
				cvx = 0;
			}
		} else {
			if ((TileMap.tileTypeAtPixel(cx - chw, cy - chh) & TileMap.T_RIGHT) == TileMap.T_RIGHT) {
				cvx = 0;
			}
		}
		//
		if (cy > ch && TileMap.tileTypeAt(cx, cy - ch, TileMap.T_BOTTOM)) {
			if (!TileMap.tileTypeAt(cx, cy, TileMap.T_TOP)) {

				statusMe = A_FALL;
				cp1 = 0;
				cp2 = 0;
				cvy = 1;
			} else
				cy = TileMap.tileYofPixel(cy);
		}
		//

		cx += cvx;
		cy += cvy;
		if (cy < 0) {
			cy = cvy = 0;
		}
		if (cvy == 0) { // Stand + Attack
			if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) != TileMap.T_TOP) {
				statusMe = A_FALL;

				cvx = (getSpeed() >> 1) * cdir;
				cp1 = cp2 = 0;
			}

		} else if (cvy < 0) { // Jump + Attack
			cvy++;
			if (cvy == 0)
				cvy = 1;
		} else { // Fall + Attack
			if (cvy < 20 && cp1 % 5 == 0)
				cvy++;
			if (cvy > 3)
				cvy = 3;
			// if (cvy > 2) {
			if ((TileMap.tileTypeAtPixel(cx, cy + 3) & TileMap.T_TOP) == TileMap.T_TOP && cy <= TileMap.tileXofPixel(cy + 3)) {
				cvx = cvy = 0;
				cy = TileMap.tileXofPixel(cy + 3);

			}
			// }
			if (TileMap.tileTypeAt(cx, cy, TileMap.T_WATERFLOW) && cy % TileMap.size > 8) {
				// startWaterSplash(cx, TileMap.tileYofPixel(cy) + 8);
				statusMe = A_WATERRUN;
				cvx = cdir << 1;
				cvy = cvy >> 2;
				cy = TileMap.tileYofPixel(cy) + 12;
// if (cMP == 0)
// ;// || !canWaterRun)
				statusMe = A_WATERDOWN;
				return;
			}
			if (TileMap.tileTypeAt(cx, cy, TileMap.T_UNDERWATER)) {
				statusMe = A_WATERDOWN;
				return;
			}
			// checkCharFallToBox();
		}
		if (cvx > 0)
			cvx--;
		else if (cvx < 0)
			cvx++;
		// if (cvx != 0)
		// checkCharRunToBox();

	}

	public void updateSkillFall() {

	}

	public void updateSkillStand() {
		setAttk();

	}

	public void updateCharAutoJump() {
		cx += cvx * cdir;
		cy += cvyJump;
		cvyJump++;
		if (cp1 == 0)
			cf = 7;
		else
			cf = 23;
		if (canJumpHigh) {
			if (cvyJump == -3)
				cf = 8;
			else if (cvyJump == -2)
				cf = 9;
			else if (cvyJump == -1)
				cf = 10;
			else if (cvyJump == 0)
				cf = 11;
		}

		if (cvyJump == 0) {

			statusMe = A_NOTHING;
			((MovePoint) vMovePoints.firstElement()).status = A_FALL;
			isJump = true;
			cp1 = 0;
			cvy = 1;

		}
	}

	public int getVx(int size, int dx, int dy) {
		if (dy > 0 && !TileMap.tileTypeAt(cx, cy, TileMap.T_TOP)) {
			if (dx - dy <= 10)
				return 5;

			if (dx - dy <= 30)

				return 6;

			if (dx - dy <= 50)

				return 7;

			if (dx - dy <= 70)

				return 8;

		}

		if (dx <= 30)
			return 4;
		if (dx <= 160)
			return 5;
		if (dx <= 270)
			return 6;
		if (dx <= 320)
			return 7;
		return 8;
		// if (size == 1)
		// return 4;
		// if (size <= 4)
		// return 5;
		// if (size <= 6)
		// return 6;
		// if (size <= 8)
		// return 7;
		// return 8;

	}

	public int getVy(int size, int dx, int dy) {

		// if (size <= 2)
		// return 7;
		// else if (size < 5)
		// return 8;
		// else if (size < 7)
		// return 9;
		// return 11;
		if (dy <= 10)
			return 5;
		if (dy <= 20)
			return 6;
		if (dy <= 30)
			return 7;
		if (dy <= 40)
			return 8;
		if (dy <= 50)
			return 9;
		return 10;
	}

	public int returnAct(int xFirst, int yFirst, int xEnd, int yEnd) {
		int dx = xEnd - xFirst;
		int dy = yEnd - yFirst;
		if (dx == 0 && dy == 0)
			return A_STAND;
		if (dy == 0 && yFirst % 24 == 0 && TileMap.tileTypeAt(xFirst, yFirst, TileMap.T_TOP))
			return A_RUN;
		if (dy > 0 && (yFirst % 24 != 0 || !TileMap.tileTypeAt(xFirst, yFirst, TileMap.T_TOP))) {
			return A_FALL;
		}
		cvy = -10;
		cp1 = 0;
		cdir = dx > 0 ? 1 : -1;
		if (dx <= 5)
			cvx = 0;
		else if (dx <= 10)
			cvx = 3;
		else
			cvx = 5;
		return A_AUTOJUMP;
	}

	public void setAutoJump() {
		int dx = ((MovePoint) vMovePoints.firstElement()).xEnd - cx;
		if (canJumpHigh)
			cvyJump = -10;
		else
			cvyJump = -8;
		cp1 = 0;
		cdir = dx > 0 ? 1 : -1;
		if (dx <= 6)
			cvx = 0;
		else if (dx <= 20)
			cvx = 3;
		else
			cvx = 5;
	}

	public void updateCharStand() {
        //		if(cdir == 1){
        //			EffdefautX = cx + 16;
        //			EffdefautY = cy - 23;
        //		}else{
        //			EffdefautX = cx - 27;
        //			EffdefautY = cy - 24;
        //		}

        isAttack = false;
        isAttFly = false;
        cvx = 0;
        cvy = 0;
        cp1++;
        lcx = cx;
        lcy = cy;
        if (cp1 > 30)
            cp1 = 0;
        if (cp1 % 15 < 5)
            cf = 0;
        else
            cf = 1;
        // cf = 5;
        updateCharInBridge();
        // isAttFly = false;

        if (me && !ischangingMap && isInWaypoint())
        {
            Service.gI().charMove();
            if (mSystem.currentTimeMillis() - (timeLastchangeMap + timedelay) > 0)
            {
                timeLastchangeMap = mSystem.currentTimeMillis();
                Cout.println("vao day---gui 22chuyen map");
                Service.gI().requestChangeMap();
            }
            //			isLockKey = true;
            //			ischangingMap = true;
            GameCanvas.clearKeyHold();
            GameCanvas.clearKeyPressed();
            return;
        }
    }

    public int sleepaddEffBui = 0;
    public void updateCharRun()
    {

        sleepaddEffBui--;
        if (sleepaddEffBui <= 0)
        {
            ServerEffect.addServerEffect(26, cx, cy + 8, -1, (sbyte)cdir);
            sleepaddEffBui = 10;
        }
		
		
		
		int spaceBefore = 0;
		if (!me) {
			if (currentMovePoint != null)
				spaceBefore = abs(cx - currentMovePoint.xEnd);
		}
		cp1++;

		if (cp1 >= 5) {
			cp1 = 0;
			cBonusSpeed = 0;
		}
		
//		cf = (cp1 >> 2) + 2;
		cf = (cp1 >> 1) + 2;

		if ((TileMap.tileTypeAtPixel(cx, cy - 1) & TileMap.T_WATERFLOW) == TileMap.T_WATERFLOW)
			cx += (cvx >> 1);
		else {
			cx += cvx;
		}

		if (cdir == 1) {
			if (TileMap.tileTypeAt(cx + chw, cy - chh, TileMap.T_LEFT)) {
				if (me) {
					cvx = 0;
					cx = TileMap.tileXofPixel(cx + chw) - chw;
				} else
					stop();
			}
		} else {
			if (TileMap.tileTypeAt(cx - chw - 1, cy - chh, TileMap.T_RIGHT)) {
				if (me) {
					cvx = 0;
					cx = TileMap.tileXofPixel(cx - chw - 1) + TileMap.size + chw;
				} else
					stop();
			}
		}

		if (me) {
            if (cvx >= getSpeed() && CRes.abs(cvx) % 2 == 0) //auto move main char toi
                Service.gI().charMove();
			if (cvx > 0)
				cvx--;
			else if (cvx < 0)
				cvx++;
			else {

//				if (cx - cxSend != 0) {
//					if (me)
//						Service.gI().charMove();
//				}
				changeStatusStand();
				cBonusSpeed = 0;
			}

		}

		if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) != TileMap.T_TOP) {
			if (me) {
				if ((cx - cxSend) != 0 || (cy - cySend) != 0)
					Service.gI().charMove();
				cf = 7;
				statusMe = A_FALL;
				cvx = 3 * cdir;
				cp2 = 0;
			} else
				stop();
		}
		//	
		if (me) {

		} else {
			if (currentMovePoint != null) {
				int spaceAfter = abs(cx - currentMovePoint.xEnd);
				if (spaceAfter > spaceBefore)
					stop();
			}
		}

        if (me && !ischangingMap && isInWaypoint())
        {
            Service.gI().charMove();
            if (mSystem.currentTimeMillis() - (timeLastchangeMap + timedelay) > 0)
            {
                timeLastchangeMap = mSystem.currentTimeMillis();
                Cout.println("vao day---gui 22chuyen map");
                Service.gI().requestChangeMap();
            }
            //			isLockKey = true;
            //			ischangingMap = true;
            GameCanvas.clearKeyHold();
            GameCanvas.clearKeyPressed();
            return;
        }

    }
	private void stop() {
		statusMe = A_NOTHING;
		cvx = 0;
		cvy = 0;
		cp1 = cp2 = 0;
	}

	public static int abs(int i) {
		return i > 0 ? i : -i;
	}

	public void updateCharJump() {
		
		
	
		cx += cvx;
		cy += cvy;
		if (cy < 0) {
			cy = 0;
			cvy = -1;

		}
		cvy++;
		if (!me) {
			if (currentMovePoint != null) {
				int d = currentMovePoint.xEnd - cx;
				if (d > 0) {
					if (cvx > d)
						cvx = d;
					if (cvx < 0)
						cvx = d;
				} else if (d < 0) {
					if (cvx < d)
						cvx = d;
					if (cvx > 0)
						cvx = d;
				} else
					cvx = d;
			}
		}
		if (cp1 == 0)
			cf = 8;
		else
			cf = 24;
		if (canJumpHigh) {
			if (cvy == -3)
				cf = 9;
			else if (cvy == -2)
				cf = 10;
			else if (cvy == -1)
				cf = 11;
			else if (cvy == 0)
				cf = 12;
		}
		if (cdir == 1) {
			if ((TileMap.tileTypeAtPixel(cx + chw, cy - 1) & TileMap.T_LEFT) == TileMap.T_LEFT && cx <= TileMap.tileXofPixel(cx + chw) + 12) {
				cx = TileMap.tileXofPixel(cx + chw) - chw;
				cvx = 0;

			}
		} else {
			if ((TileMap.tileTypeAtPixel(cx - chw, cy - 1) & TileMap.T_RIGHT) == TileMap.T_RIGHT && cx >= TileMap.tileXofPixel(cx - chw) + 12) {
				cx = TileMap.tileXofPixel(cx + 24 - chw) + chw;
				cvx = 0;
			}
		}
		if (cvy == 0) {
			if (!isAttFly) {
				if (me)
					setCharFallFromJump();
				else
					stop();
			} else {
				// UseSkill us = NinjaUtil.findSkillUse();
				// int l = cx - us.range;
				// int r = cx + us.range;
				// int up = cy - us.range;
				// int dow = cy + us.range;
				// if (setInsc(l, r, ((Mob)
				// GameScr.vMob.elementAt(focusIndex)).x, up, dow, ((Mob)
				// GameScr.vMob.elementAt(focusIndex)).y)) {
				//
				// attackCloseMob();
				// statusMe = SKILL_FALL;
				// } else {
				// setCharFallFromJump();
				// }

				setCharFallFromJump();
			}

		}
		if (me && !ischangingMap && isInWaypoint()) {
			Service.gI().charMove();
			Service.gI().requestChangeMap();
//			isLockKey = true;
//			ischangingMap = true;
			GameCanvas.clearKeyHold();
			GameCanvas.clearKeyPressed();
			return;
		}
		if (cp3 < 0)
			cp3++;
		if (cy > ch && TileMap.tileTypeAt(cx, cy - ch, TileMap.T_BOTTOM)) {
			if (me) {
				if ((cx - cxSend) != 0 || (cy - cySend) != 0)
					Service.gI().charMove();
				statusMe = A_FALL;
				cp1 = 0;
				cp2 = 0;
				cvy = 1;
			} else
				stop();

		}
		if (me) {

		} else {
			if (currentMovePoint != null) {
				if (cy < currentMovePoint.yEnd) {
					stop();
				}
			}
		}
	}

	public bool checkInRangeJump(int x1, int xw1, int xmob, int y1, int yh1, int ymob) {
		if (xmob > xw1 || xmob < x1 || ymob > y1 || ymob < yh1)
			return false;
		return true;

	}

	public void setCharFallFromJump() {
		cyStartFall = cy;
		statusMe = A_FALL;
		cp1 = 0;
		if (canJumpHigh)
			cp2 = 1; // Rotate in STrong Jump only
		else
			cp2 = 0;
		cvy = 1;
		updateCharFall();
		if (me) {
			if ((cx - cxSend) != 0 || (cy - cySend) != 0) {
				Service.gI().charMove();
			}
		}
	}

	public void updateCharFall() {
        //		stop();
        //		if(cdir == 1){
        //			EffdefautX = cx + 16; 
        //			EffdefautY = cy - 23;
        //		}else{
        //			EffdefautX = cx - 27;
        //			EffdefautY = cy - 24;
        //		}
        if (cy + 4 >= TileMap.pxh)
        {
            changeStatusStand();
            cvx = cvy = 0;
            return;
        }
        //
        //		Cout.println((cy % 24)+"updatecharrr fall stop "+( (TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP));
        if (cy % 24 == 0 && (TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP)
        {
            if (me)
            {
                if ((cy - cySend) > 0)
                {
                    Service.gI().charMove();
                }
                else if ((cx - cxSend) != 0 || (cy - cySend) < 0)
                {
                    Service.gI().charMove();
                }
                cvx = cvy = 0;
                cp1 = cp2 = 0;
                //				Cout.println("-----------------stand---------");

                changeStatusStand();
                return;
            }
            else
            { // roi xuong tile
                stop();
                cf = 0;
            }
        }
        //
        cf = 13;
        cx += cvx;
        if (!me)
        {
            if (currentMovePoint != null)
            {
                int d = currentMovePoint.xEnd - cx;
                if (d > 0)
                {
                    if (cvx > d)
                        cvx = d;
                    if (cvx < 0)
                        cvx = d;
                }
                else if (d < 0)
                {
                    if (cvx < d)
                        cvx = d;
                    if (cvx > 0)
                        cvx = d;
                }
                else
                    cvx = d;
            }
        }

        cy += cvy;

        if (me && cvy % 8 == 0)
            Service.gI().charMove();
        //		Cout.println("--updatefall----cvy---  "+cvy);
        if (cvy < 20)
            cvy++;

        if (cdir == 1)
        {
            if ((TileMap.tileTypeAtPixel(cx + chw, cy - 1) & TileMap.T_LEFT) == TileMap.T_LEFT && cx <= TileMap.tileXofPixel(cx + chw) + 12)
            {
                cx = TileMap.tileXofPixel(cx + chw) - chw;
                cvx = 0;
            }
        }
        else
        {
            if ((TileMap.tileTypeAtPixel(cx - chw, cy - 1) & TileMap.T_RIGHT) == TileMap.T_RIGHT && cx >= TileMap.tileXofPixel(cx - chw) + 12)
            {
                cx = TileMap.tileXofPixel(cx + 24 - chw) + chw;//
                cvx = 0;

            }
        }

        if (cvy > 4)
        {
            if ((cyStartFall == 0 || cyStartFall <= TileMap.tileYofPixel(cy + 3))
                    && (TileMap.tileTypeAtPixel(cx, cy + 3) & TileMap.T_TOP) == TileMap.T_TOP)
            {
                if (me)
                {
                    cyStartFall = 0;
                    cvx = cvy = 0;
                    cp1 = cp2 = 0;
                    cy = TileMap.tileXofPixel(cy + 3);
                    changeStatusStand();
                    // =====================
                    if ((cy - cySend) > 0)
                    {
                        if (me)
                            Service.gI().charMove();
                    }
                    else if ((cx - cxSend) != 0 || (cy - cySend) < 0)
                    {
                        if (me)
                            Service.gI().charMove();
                    }

                    //					Cout.println("-----------------stand----22222222-----");
                }
                else
                {
                    stop();
                    cy = TileMap.tileXofPixel(cy + 3);
                    cf = 0;
                }
                return;
            }
        }
        if (cp2 == 1) // That means fall rotate
        {
            if (cvy == 3)
                cf = 12;
            else if (cvy == 2)
                cf = 9;
            else if (cvy == 1)
                cf = 10;
            else if (cvy == 0)
                cf = 11;
        }
        else
        {
            cf = 13;
        }
        if (cvy > 6)
            if (TileMap.tileTypeAt(cx, cy, TileMap.T_WATERFLOW) && cy % TileMap.size > 8)
            {

                // attackAshiko = false;
                // Canvas.startWaterSplash(cx, TileMap.tileYofPixel(cy) + 8);
                cy = TileMap.tileYofPixel(cy) + 8;
                statusMe = A_WATERRUN;
                cvx = cdir << 1;
                cvy = cvy >> 2;
                cy = TileMap.tileYofPixel(cy) + 12;
                if ((cx - cxSend) != 0 || (cy - cySend) != 0)
                {
                    if (me)
                        Service.gI().charMove();
                }
                // if (cMP == 0 || !canJumpHigh)
                // statusMe = A_WATERDOWN;

            }
        if (me)
        {
            if (isAttack)
            {
                return;
            }
        }
        else
        {
            if ((TileMap.tileTypeAtPixel(cx, cy + 1) & TileMap.T_TOP) == TileMap.T_TOP)
            {
                cf = 0;
            }
            if (currentMovePoint != null)
            {
                if (cy > currentMovePoint.yEnd)
                {
                    stop();
                }
            }
        }

    }

	public void updateCharWaterRun() {
		if (!TileMap.tileTypeAt(cx, cy, TileMap.T_WATERFLOW)) {
			if ((cx - cxSend) != 0 || (cy - cySend) != 0) {
				if (me)
					Service.gI().charMove();
			}
			statusMe = A_FALL;
		}
		cp1++;
		if (cp1 >= 5) {
			cp1 = 0;
			cBonusSpeed = 0;
		}
		cf = (cp1) + 2;
		if (cdir == 1) {
			if (TileMap.tileTypeAt(cx + chw, cy - 1, TileMap.T_LEFT)) {
				cvx = 0;
				cx = TileMap.tileXofPixel(cx + chw) - chw;
			}
		} else {
			if (TileMap.tileTypeAt(cx - chw - 1, cy - 1, TileMap.T_RIGHT)) {
				cvx = 0;
				cx = TileMap.tileXofPixel(cx - chw - 1) + TileMap.size + chw;
			}
		}
		cx += cvx;
		if (cvx > 0)
			cvx--;
		else if (cvx < 0)
			cvx++;
		else {
			if (cx - cxSend != 0) {
				if (me)
					Service.gI().charMove();
			}
			statusMe = A_WATERDOWN;
			cBonusSpeed = 0;

		}
		if (GameCanvas.gameTick % 8 == 0) {

			// cMP -= Canvas.MPLOST_WATERRUN;
			// if (cMP <= 0) {
			// cMP = 0;
			// cvx = 0;
			// cact = A_WATERDOWN;
			// }
			// Canvas.effectChangeMPHP(1, Canvas.hpBarX + Char.cMP *
			// Canvas.hpBarW
			// / Char.cmaxMP);
			// Canvas.paintCmdBar = true;
		}
		// Checking Conflict with Box
		// for (int i = 0; i < Canvas.m_bcount; i++) {
		// if (Canvas.abs(cx - Canvas.m_bx[i]) < 20) {
		// if (Canvas.m_by[i] > cy && Canvas.m_by[i] - 24 < cy) {
		// cvx = 0;
		// if (cx < Canvas.m_bx[i])
		// cx = Canvas.m_bx[i] - 20;
		// else
		// cx = Canvas.m_bx[i] + 20;
		// cact = A_WATERDOWN;
		// break;
		// }
		// }
		// }
		// GameScr.startWaterSplash(cx, cy);
		// GameScr.startDust(cdir, cx - (cdir << 3), cy);
		// ==================================buiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
		GameCanvas.gI().startWaterSplash(cx, cy);
		GameCanvas.gI().startDust(cdir, cx - (cdir << 3), cy);
		// =============================buiiiiiiiiiiiiiiiiiiiiiiiiiiiii
	}

	public void updateCharWaterDown() {
		// if (canDuoiNuoc)
		// Canvas.cspeed = 3;
		cy += cvy;
		if (cvy < 20 && GameCanvas.gameTick % 2 == 0)
			cvy++;
		cf = 7;
		if (cy >= TileMap.pxh) {
			cHP = 0;
			statusMe = A_DEADFLY;
			return;
		}
		if (TileMap.tileTypeAt(cx, cy, TileMap.T_TOP)) {
			changeStatusStand();
			cvx = cvy = 0;
			cp1 = cp2 = 0;
			cy = TileMap.tileXofPixel(cy);
			if ((cy - cySend) > 0) {
				if (me)
					Service.gI().charMove();
			}
			return;
		}
		// Check Die
		if (TileMap.tileTypeAt(cx, cy, TileMap.T_UNDERWATER)) {
			cHP = 0;
			statusMe = A_DEADFLY;
		}

	}

	public short head, leg, body, wp, coat = -1, glove = -1;
	public int indexEff = -1, indexEffTask = -1;
	public EffectCharPaint eff, effTask;
	public int indexSkill, i0, i1, i2, dx0, dx1, dx2, dy0, dy1, dy2;
	public EffectCharPaint eff0 = null, eff1 = null, eff2 = null;
	public Arrow arr = null;
	public SkillPaint skillPaint;
	public EffectPaint[] effPaints;
	public int sType;

	public byte isInjure;

	public void setDefaultPart() {
		setDefaultWeapon();
		setDefaultBody();
		setDefaultLeg();
	}

	public void setDefaultWeapon() {
		wp = 15;
	}

	public Part getDefaultHead(int gender) {
		try {
			if (gender == 0)
				return GameScr.parts[27];
			else
				return GameScr.parts[2];

		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	public void setDefaultBody() {
		if (cgender == 0)
			body = 10;
		else
			body = 1;
	}

	public void setDefaultLeg() {
		if (cgender == 0)
			leg = 9;
		else
			leg = 0;
	}

	public void setSkillPaint(SkillPaint skillPaint, int sType) {
		try{
			long now = mSystem.currentTimeMillis();
//			if (me) {
//				if (now - myskill.lastTimeUseThisSkill < myskill.coolDown) {
//					myskill.paintCanNotUseSkill = true;
//					return;
//				}
//				myskill.lastTimeUseThisSkill = now;
//				cMP -= myskill.manaUse;
//				if (cMP < 0) {
//					cMP = 0;
//				}
//			}
			setAutoSkillPaint(skillPaint, sType);
		}catch (Exception e) {
			// TODO: handle exception
		//	e.printStackTrace();
		}
	}

	public void setAutoSkillPaint(SkillPaint skillPaint, int sType) {
        try
        {
            this.skillPaint = skillPaint;
            this.sType = sType;
            indexSkill = -1;
            i0 = i1 = i2 = dx0 = dx1 = dx2 = dy0 = dy1 = dy2 = 0;
            eff0 = null;
            eff1 = null;
            eff2 = null;

        }
        catch (Exception e)
        {
            // TODO: handle exception
            //e.printStackTrace();
        }
	}
	public SkillInfoPaint[] skillInfoPaint() {
        try
        {
            if (skillPaint == null)
                return null;
            if (sType == 0)
            {
                return skillPaint.skillStand;
            }
            return skillPaint.skillfly;
        }
        catch (Exception e)
        {
          //  e.printStackTrace();
        }

        return null;
    }

	public void paintHead(mGraphics g, int xStart, int yStart) {
		Part ph = GameScr.parts[head], pl = GameScr.parts[leg], pb = GameScr.parts[body];
//		if (arrItemBody != null && arrItemBody[Item.TYPE_MATNA] != null) {
//			ph = GameScr.parts[arrItemBody[Item.TYPE_MATNA].template.part];
//		}
		if (cdir == 1) {
			SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, xStart + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, yStart - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
			SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, xStart + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, yStart - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
			SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, xStart + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, yStart - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
		} else {
			SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, xStart - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, yStart - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
			SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, xStart - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, yStart - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
			SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, xStart - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, yStart - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
		}
	}

	public void setAttack() {
		int line = 0;
		try {
//			if (me) {
				line = 1;
//				if (myskill.template.type == Skill.SKILL_CLICK_USE_BUFF)
//					return;
//				if (myskill.template.id == 42 && !myskill.isCooldown()) {
//					isBlinking = true;
//					timeStartBlink = System.currentTimeMillis();
//				}

				if (skillPaint != null && mobFocus != null/* || (charFocus != null *//*&& isMeCanAttackOtherPlayer(charFocus)*//*))*/) {
					int rangex = 100;
					int rangey = 30;
//					if (isUseLongRangeWeapon()) {
//						rangex = myskill.dx;
//						rangey = myskill.dy;
//					}
					mVector vMob = new mVector();
					mVector vChar = new mVector();
//					 if (mobFocus != null && mobFocus.status != Mob.MA_DEADFLY && mobFocus.status != Mob.MA_INHELL) {
//						vMob.addElement(mobFocus);
//						for (int i = 0; i < GameScr.vMob.size(); i++) {
//							if (vMob.size() + vChar.size() >= myskill.maxFight)
//								break;
//							Mob m = (Mob) GameScr.vMob.elementAt(i);
//							if (m.status != Mob.MA_DEADFLY && m.status != Mob.MA_INHELL && !m.Equals(mobFocus) && mobFocus.x - rangex <= m.x && m.x <= mobFocus.x + rangex && mobFocus.y - rangey <= m.y && m.y <= mobFocus.y + rangey) {
//								if ((cdir == -1 && m.x <= cx) || (cdir == 1 && m.x >= cx))
//									vMob.addElement(m);
//							}
//						}
//					
//					}
					effPaints = new EffectPaint[vMob.size() + vChar.size()];
					for (int i = 0; i < vMob.size(); i++) {
						effPaints[i] = new EffectPaint();
						effPaints[i].effCharPaint = GameScr.efs[skillPaint.effId - 1];
						effPaints[i].eMob = (Mob) vMob.elementAt(i);
					}
					for (int i = 0; i < vChar.size(); i++) {
						effPaints[i + vMob.size()] = new EffectPaint();
						effPaints[i + vMob.size()].effCharPaint = GameScr.efs[skillPaint.effId - 1];
						effPaints[i + vMob.size()].eChar = (Char) vChar.elementAt(i);
					}
					if (effPaints.Length > 1) {
						EPosition p1 = new EPosition();
						if (effPaints[0].eMob != null)
							p1 = new EPosition(effPaints[0].eMob.x, effPaints[0].eMob.y);
						else if (effPaints[0].eChar != null)
							p1 = new EPosition(effPaints[0].eChar.cx, effPaints[0].eChar.cy);

						mVector vTo = new mVector();
						for (int i = 1; i < effPaints.Length; i++) {
							if (effPaints[i].eMob != null)
								vTo.addElement(new EPosition(effPaints[i].eMob.x, effPaints[i].eMob.y));
							else if (effPaints[i].eChar != null)
								vTo.addElement(new EPosition(effPaints[i].eChar.cx, effPaints[i].eChar.cy));
							if (i > 5)
								break;
						}
						//Lightning.addLight(vTo, p1, true, getClassColor());
					}
					int type = 0;
					if (mobFocus != null)
						type = 1;
					else if (charFocus != null)
						type = 2;
					if (me){
						//System.out.println("sendAtt");
						//Service.gI().sendPlayerAttack(vMob, vChar, type);
					}
				}
//			}
			
			else if (skillPaint != null && (mobFocus != null || charFocus != null)) {
				line = 2;
				if (attMobs != null && attChars != null) {
					line = 3;
					effPaints = new EffectPaint[attMobs.Length + attChars.Length];
					for (int i = 0; i < attMobs.Length; i++) {
						effPaints[i] = new EffectPaint();
						effPaints[i].effCharPaint = GameScr.efs[skillPaint.effId - 1];
						effPaints[i].eMob = attMobs[i];
					}
					for (int i = 0; i < attChars.Length; i++) {
						effPaints[i + attMobs.Length] = new EffectPaint();
						effPaints[i + attMobs.Length].effCharPaint = GameScr.efs[skillPaint.effId - 1];
						effPaints[i + attMobs.Length].eChar = attChars[i];
					}
					attMobs = null;
					attChars = null;
				} else if (attMobs != null) {
					line = 4;
					effPaints = new EffectPaint[attMobs.Length];
					for (int i = 0; i < attMobs.Length; i++) {
						effPaints[i] = new EffectPaint();
						effPaints[i].effCharPaint = GameScr.efs[skillPaint.effId - 1];
						effPaints[i].eMob = attMobs[i];
					}
					attMobs = null;
				} else if (attChars != null) {
					line = 5;
					effPaints = new EffectPaint[attChars.Length];
					for (int i = 0; i < attChars.Length; i++) {
						effPaints[i] = new EffectPaint();
						effPaints[i].effCharPaint = GameScr.efs[skillPaint.effId - 1];
						effPaints[i].eChar = attChars[i];
					}
					attChars = null;
				}
				line = 6;
				// Ve tia set
				if (effPaints.Length > 1 && effPaints[0] != null) {
					line = 7;
					EPosition p1 = new EPosition();
					if (effPaints[0].eMob != null)
						p1 = new EPosition(effPaints[0].eMob.x, effPaints[0].eMob.y);
					else if (effPaints[0].eChar != null)
						p1 = new EPosition(effPaints[0].eChar.cx, effPaints[0].eChar.cy);
					line = 8;
					mVector vTo = new mVector();
					for (int i = 1; i < effPaints.Length; i++) {
						if (effPaints[i].eMob != null)
							vTo.addElement(new EPosition(effPaints[i].eMob.x, effPaints[i].eMob.y));
						else if (effPaints[i].eChar != null)
							vTo.addElement(new EPosition(effPaints[i].eChar.cx, effPaints[i].eChar.cy));
						if (i > 5)
							break;
					}
					line = 9;
					//Lightning.addLight(vTo, p1, true, getClassColor());

				}
			}
		} catch (Exception e) {
            //System.out.println("lineeeeeeeeee: " + line);
            //e.printStackTrace();
		}
	}

	public bool isHaveWolf() {
		if (arrItemMounts != null && arrItemMounts[4] != null && (arrItemMounts[4].template.id == 443 || arrItemMounts[4].template.id == 523))
			return true;
		return false;
	}
	
	public bool isHaveMoto() {
		if (arrItemMounts != null && arrItemMounts[4] != null && ((arrItemMounts[4].template.id == 485)|| (arrItemMounts[4].template.id == 524)))
			return true;
		return false;
	}
	public bool isPaint() {
		if (cx < GameScr.cmx)
			return false;
		if (cx > GameScr.cmx + GameScr.gW)
			return false;
		if (cy < GameScr.cmy)
			return false;
		if (cy > GameScr.cmy + GameScr.gH + 30)
			return false;
		return true;
	}
    public void paintShadow(mGraphics g)
    {
        int size = TileMap.size;
        if (TileMap.tileTypeAt(cx + size / 2, cybong + 1, TileMap.T_LEFT))
        {
            g.setClip((xSd / size) * size, ((cybong - 30) / size) * size, size,
                    100);
        }
        else if (TileMap
              .tileTypeAt((cx - size / 2) / size, (cybong + 1) / size) == 0)
        {
            g
                    .setClip((cx / size) * size, ((cybong - 30) / size) * size,
                            100, 100);
        }
        else if (TileMap
              .tileTypeAt((cx + size / 2) / size, (cybong + 1) / size) == 0)
        {
            g.setClip((cx / size) * size, ((cybong - 30) / size) * size, size,
                    100);
        }
        else if (TileMap.tileTypeAt(xSd - size / 2, cybong + 1, TileMap.T_RIGHT))
        {
            g.setClip((cx / 24) * size, ((cybong - 30) / size) * size, size, 100);
        }

        g.drawImage(loadImageInterface.bongChar, cx,
                cybong + 15, mGraphics.VCENTER | mGraphics.HCENTER);
        g.setClip(GameScr.cmx, GameScr.cmy - GameCanvas.transY, GameScr.gW,
                GameScr.gH + 2 * GameCanvas.transY);
    }
	public virtual void paint(mGraphics g) {
        //		g.setColor(0xff000000);
        //		g.fillRect(cx-5, cy-5, 10, 10);
        paintCharName_HP_MP_Overhead(g);
        //		paintCharWithoutSkill(g);
        //		loadFromServer();
        ////		System.err.println(cx + ":" + cy);
        //cy+=8;

        if (typePk >= 0)

            g.drawRegion(loadImageInterface.iconpk, 0, 12 * (typePk * 3 + (GameCanvas.gameTick / 3) % 3), 12, 12, 0,
                    cx,
                    cy - 75, mGraphics.VCENTER | mGraphics.HCENTER);
        if (statusMe == A_DEAD || statusMe == A_DEADFLY)
        {
            g.drawImage(loadImageInterface.chardie, cx - loadImageInterface.chardie.getWidth() / 2,
                    cy + 10 - loadImageInterface.chardie.getHeight() + 4, mGraphics.TOP | mGraphics.LEFT);

            return;
        }
        paintShadow(g);

        if (!isPaint())
        {
            if (skillPaint != null)
            {
                indexSkill = skillInfoPaint().Length;
                skillPaint = null;
                effPaints = null;
                eff = null;
                effTask = null;
                indexEff = -1;
                indexEffTask = -1;
            }
            return;
        }
        //		if (statusMe == A_HIDE || (moveFast != null && moveFast[0] > 0))
        //			return;
        //
        //		paintCharName_HP_MP_Overhead(g);
        //	
        if (skillPaint != null)
        {
            //			paintCharWithSkill(g);
        }
        else
        {
            paintCharWithoutSkill(g);
        }
        paintArrowAttack(g);


        if (effPaints != null)
        {
            for (int i = 0; i < effPaints.Length; i++)
            {
                if (effPaints[i] != null)
                {
                    if (effPaints[i].eMob != null)
                    {
                        if (!effPaints[i].isFly)
                        {
                            effPaints[i].eMob.setInjure();
                            effPaints[i].eMob.injureBy = this;
                            //							if (me) {
                            //								effPaints[i].eMob.hpInjure = Char.myChar().cdame / 2 - (Char.myChar().cdame * Util.randomNumber(11)) / 100;
                            // effPaints[i].eMob.hp -=
                            // effPaints[i].eMob.hpInjure;
                            // if (effPaints[i].eMob.hp < 1)
                            // effPaints[i].eMob.hp = 1;
                            //							}
                            //							if (effPaints[i].eMob.templateId != 98 && effPaints[i].eMob.templateId != 99) // ko lÃƒÂ  cÃ¡Â»â„¢t chiÃ¡ÂºÂ¿n trÃ†Â°Ã¡Â»ï¿½ng
                            //								GameScr.startSplash(effPaints[i].eMob.x, effPaints[i].eMob.y - (effPaints[i].eMob.h >> 1), cdir);
                            effPaints[i].isFly = true;
                        }
                        SmallImage.drawSmallImage(g, effPaints[i].getImgId(), effPaints[i].eMob.x, effPaints[i].eMob.y, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
                    }
                    else if (effPaints[i].eChar != null)
                    {
                        if (!effPaints[i].isFly)
                        {
                            if (effPaints[i].eChar.charID >= 0)
                                effPaints[i].eChar.doInjure();
                            GameScr.startSplash(effPaints[i].eChar.cx, effPaints[i].eChar.cy + 8 - (effPaints[i].eChar.ch >> 1), cdir);
                            effPaints[i].isFly = true;
                        }
                        SmallImage.drawSmallImage(g, effPaints[i].getImgId(), effPaints[i].eChar.cx, effPaints[i].eChar.cy + 8, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
                    }
                    effPaints[i].index++;
                    if (effPaints[i].index >= effPaints[i].effCharPaint.arrEfInfo.Length)
                    {
                        effPaints[i] = null;
                    }
                }
            }
        }
        if (indexEff >= 0 && eff != null)
        {
            SmallImage.drawSmallImage(g, eff.arrEfInfo[indexEff].idImg, cx + eff.arrEfInfo[indexEff].dx, cy + 8 + eff.arrEfInfo[indexEff].dy, 0, mGraphics.VCENTER | mGraphics.HCENTER);
            if (GameCanvas.gameTick % 2 == 0)
            {
                indexEff++;
                if (indexEff >= eff.arrEfInfo.Length)
                {
                    indexEff = -1;
                    eff = null;
                }
            }
        }


        if (indexEffTask >= 0 && effTask != null)
        {
            SmallImage.drawSmallImage(g, effTask.arrEfInfo[indexEffTask].idImg, cx + effTask.arrEfInfo[indexEffTask].dx, cy + 8 + effTask.arrEfInfo[indexEffTask].dy, 0, mGraphics.VCENTER | mGraphics.HCENTER);
            if (GameCanvas.gameTick % 2 == 0)
            {
                indexEffTask++;
                if (indexEffTask >= effTask.arrEfInfo.Length)
                {
                    indexEffTask = -1;
                    effTask = null;
                }
            }
        }
        //		// -----EFFECT BATTU
        //		if (isEffBatTu) {
        //			if (count < 2)
        //				SmallImage.drawSmallImage(g, 1366, cx, cy - chh, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if (count < 4)
        //				SmallImage.drawSmallImage(g, 1367, cx, cy - chh, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if (count < 8)
        //				SmallImage.drawSmallImage(g, 1368, cx, cy - chh, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else if (GameCanvas.gameTick % 2 == 0)
        //				SmallImage.drawSmallImage(g, 1369, cx, cy - chh, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //			else
        //				SmallImage.drawSmallImage(g, 1370, cx, cy - chh, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        //
        //		}
        //
        //		if (mobMe != null) {
        //			mobMe.paint(g);
        //		}
        //System.out.println("Domsang ---> "+vDomsang.size());
        //paintEffEyesWolf(g);
        //cy-=8;
        //		mFont.tahoma_7_green.drawString(g, "id "+charID, cx, cy, 2);

        if (typePk >= 0)

            g.drawRegion(loadImageInterface.iconpk, 0, 12 * (typePk * 3 + (GameCanvas.gameTick / 3) % 3), 12, 12, 0,
                    cx,
                    cy - 75, mGraphics.VCENTER | mGraphics.HCENTER);

    }

	private void paintArrowAttack(mGraphics g) {
		if (arr != null) {
			arr.paint(g);
		}
	}

	public void paintHp(mGraphics g, int px, int py) {
		int hpWidth = cHP * 26 / cMaxHP;
		if (statusMe != A_DEADFLY && statusMe != A_DEAD && hpWidth < 2)
			hpWidth = 2;
		else if (statusMe == A_DEADFLY || statusMe == A_DEAD)
			hpWidth = 0;
		if (hpWidth > 26)
			hpWidth = 0;
		g.setColor(0xFFFFFF);
		g.fillRect(px, py, 26, 4);
		g.setColor(getClassColor());
		g.fillRect(px, py, hpWidth, 4);
		g.setColor(0);
		g.drawRect(px, py, 26, 4);
	}


	
	public int[] geteffOngbo(){
		int[] imgId = null;
		if(isMoto){
			if(arrItemMounts[4].template.id == 485 && arrItemMounts[4].sys >= 3)
				imgId = new int[] {2094, 2095, 2096};
		}
		return imgId;
	}
	
	public int[] getEffmoto() {
		int[] imgId = null;
		if(isMoto)
			imgId = new int[] {2082, 2083, 2084, 2089};
		return imgId;
	}
	
	public int[] getEffwolf(){
		int[] imgId = null;
		if(isWolf)
			imgId = new int[] {2085, 2086, 2087, 2088};
		return imgId;
	}

	public int[] getClassCoat() {

		int[] imgId = null;
		int typeCoat = -1;
		
//		if (me) {
//			if (arrItemBody[Item.TYPE_AOCHOANG] != null)
//				typeCoat = arrItemBody[Item.TYPE_AOCHOANG].template.id;
//
//		} else
//			typeCoat = coat;
		
		if (typeCoat == -1)
			return null;
		
		if (typeCoat == 420){
			if(isWolf || isMoto){
				imgId = new int[] {2029, 2030, 2031, 2030};
			}else
				imgId = new int[] { 1635, 1636, 1637, 1636 };
		}
		else if (typeCoat == 421){
			if(isWolf || isMoto){
				imgId = new int[] { 2035, 2036, 2037, 2036 };
			}else
				imgId = new int[] { 1652, 1653, 1654, 1653 };
		}
		else if (typeCoat == 422){
			if(isWolf || isMoto){
				imgId = new int[] { 2032, 2033, 2034, 2033 };
			}else
				imgId = new int[] { 1655, 1656, 1657, 1656 };
		}

		return imgId;
	}

	public int getClassColor() {
		int colorHP = 0x8b8b8b;
		if (nClass.classId == 1 || nClass.classId == 2) // Kiem, Phitieu - Hoa
			colorHP = 0xFF0000;
		else if (nClass.classId == 3 || nClass.classId == 4) // kunai, cung -
			// Bang
			colorHP = 0x0080FF;
		else if (nClass.classId == 5 || nClass.classId == 6) // Dao - quat -
			// Phong
			colorHP = 0x719563;
		return colorHP;
	}

	public void paintNameInSameParty(mGraphics g) {
		if (isPaint())
			if (Char.myChar().charFocus == null || !Char.myChar().charFocus.Equals(this))
				mFont.tahoma_7_yellow.drawString(g, cName, cx, cy - ch - mFont.tahoma_7_green.getHeight() - 5, mFont.CENTER, mFont.tahoma_7_grey);
			else if (Char.myChar().charFocus != null && Char.myChar().charFocus.Equals(this))
				mFont.tahoma_7_yellow.drawString(g, cName, cx, cy - ch - mFont.tahoma_7_green.getHeight() - 10, mFont.CENTER, mFont.tahoma_7_grey);
	}

	public void paintCharName_HP_MP_Overhead(mGraphics g) {
		int height = ch + 5;
		mFont.tahoma_7_white.drawString(g, cName, cx, cy - height - 15, mFont.CENTER, mFont.tahoma_7_grey);
//		if (isInvisible && !me)
//			return;
//		bool isPaintClan = false;
//		if (me) {
//			if(charID < -1000000000){
//				height += mFont.tahoma_7.getHeight();
//				mFont.tahoma_7_yellow.drawString(g,Char.myChar().cName , cx, cy - height, mFont.CENTER,
//						mFont.tahoma_7_grey);
//				height += 1;
//			}else{
//				if (GameScr.gI().auto == 1) {
//					if (!GameScr.gI().lockAutoMove) {
//						height += mFont.tahoma_7.getHeight();
//						mFont.tahoma_7_yellow.drawString(g, mResources.AUTO_FIRE, cx, cy - height, mFont.CENTER, mFont.tahoma_7_grey);
//						height += 1;
//					} else {
//						height += mFont.tahoma_7.getHeight();
//						mFont.tahoma_7_yellow.drawString(g, mResources.UNMOVE, cx, cy - height, mFont.CENTER, mFont.tahoma_7_grey);
//						height += 1;
//					}
//				} else if (npcFocus == null && charFocus == null && mobFocus == null && itemFocus == null) {
//					isPaintClan = true;
//					height += mFont.tahoma_7.getHeight();
//					if(!isHuman())
//						mFont.tahoma_7_blue1.drawString(g, cName, cx, cy - height, mFont.CENTER, mFont.tahoma_7_grey);
//					else
//						mFont.tahoma_7_white.drawString(g, cName, cx, cy - height, mFont.CENTER, mFont.tahoma_7_grey);
//					height += 1;
//				} 
//			}
//		} else if (Char.myChar().charFocus != null && Char.myChar().charFocus.Equals(this)) {
//			isPaintClan = true;
//			height += 5;
//			paintHp(g, cx - 13, cy - height);
//			height += mFont.tahoma_7.getHeight();
//			if(!isHuman())
//				mFont.tahoma_7_blue1.drawString(g, cName, cx, cy - height, mFont.CENTER, mFont.tahoma_7_grey);
//			else
//				mFont.tahoma_7_white.drawString(g, cName, cx, cy - height, mFont.CENTER, mFont.tahoma_7_grey);
//			height += 1;
//		} else if (paintName) {
//			isPaintClan = true;
//			height += mFont.tahoma_7.getHeight();
//			if(!isHuman())
//				mFont.tahoma_7_blue1.drawString(g, cName, cx, cy - height, mFont.CENTER, mFont.tahoma_7_grey);
//			else
//				mFont.tahoma_7_white.drawString(g, cName, cx, cy - height, mFont.CENTER, mFont.tahoma_7_grey);
//			height += 1;
//		}
//		if (charID == -Char.myChar().charID) {
//			height += mFont.tahoma_7.getHeight();
//			mFont.tahoma_7_yellow.drawString(g, mResources.BY + " " + Char.myChar().cName + " " + mResources.PROTECT, cx, cy - height, mFont.CENTER,
//					mFont.tahoma_7_grey);
//			height += 1;
//		}
//		
//			
//		if (!cClanName.Equals("") && isPaintClan) {
//			height += mFont.tahoma_7.getHeight() - 1;
//			int a = 0;
//			if (ctypeClan > 0)
//				a = 5;
//			mFont.tahoma_7_white.drawString(g, cClanName, cx + a, cy - height, mFont.CENTER, mFont.tahoma_7_blue);
//
//			if (ctypeClan == Clan.TYPE_TOCPHO)
//				SmallImage.drawSmallImage(g, 1215, cx - (mFont.tahoma_7_white.getWidth(cClanName) / 2 + (7 - a)), cy - height + 1, 0,
//						mGraphics.HCENTER | mGraphics.TOP);
//			else if (ctypeClan == Clan.TYPE_TOCTRUONG)
//				SmallImage.drawSmallImage(g, 1216, cx - (mFont.tahoma_7_white.getWidth(cClanName) / 2 + (7 - a)), cy - height + 1, 0,
//						mGraphics.HCENTER | mGraphics.TOP);
//			else if (ctypeClan == Clan.TYPE_TRUONGLAO)
//				SmallImage.drawSmallImage(g, 1217, cx - (mFont.tahoma_7_white.getWidth(cClanName) / 2 + (7 - a)), cy - height + 1, 0,
//						mGraphics.HCENTER | mGraphics.TOP);
//			height += 1;
//		}
//		if (resultTest > 0 && resultTest < 30) {
//			height += SmallImage.smallImg[1117][4];
//			SmallImage.drawSmallImage(g, 1117, cx, cy - height, 0, mGraphics.HCENTER | mGraphics.TOP);
//			height += 1;
//		} else if (resultTest > 30 && resultTest < 60) {
//			height += SmallImage.smallImg[1117][4];
//			SmallImage.drawSmallImage(g, 1126, cx, cy - height, 0, mGraphics.HCENTER | mGraphics.TOP);
//			height += 1;
//		} else if (resultTest > 60 && resultTest < 90) {
//			height += SmallImage.smallImg[1117][4];
//			SmallImage.drawSmallImage(g, 1118, cx, cy - height, 0, mGraphics.HCENTER | mGraphics.TOP);
//			height += 1;
//		} else if (charID >= 0) {
//			if (killCharId >= 0) {
//				height += SmallImage.smallImg[1122][4];
//				SmallImage.drawSmallImage(g, 1122, cx, cy - height, 0, mGraphics.HCENTER | mGraphics.TOP);
//				height += 1;
//			} else if (cTypePk == PK_DOSAT) {
//				height += SmallImage.smallImg[1121][4];
//				SmallImage.drawSmallImage(g, 1121, cx, cy - height, 0, mGraphics.HCENTER | mGraphics.TOP);
//				height += 1;
//			} else if (cTypePk == PK_BANG) {
//				height += SmallImage.smallImg[1124][4];
//				SmallImage.drawSmallImage(g, 1124, cx, cy - height, 0, mGraphics.HCENTER | mGraphics.TOP);
//				height += 1;
//			} else if (cTypePk == PK_NHOM) {
//				height += SmallImage.smallImg[1123][4];
//				SmallImage.drawSmallImage(g, 1123, cx, cy - height, 0, mGraphics.HCENTER | mGraphics.TOP);
//				height += 1;
//			} else if (cTypePk == PK_PHE1) {
//				height += SmallImage.smallImg[1240][4];
//				SmallImage.drawSmallImage(g, 1240, cx, cy - height, 0, mGraphics.HCENTER | mGraphics.TOP);
//				height += 1;
//			} else if (cTypePk == PK_PHE2) {
//				height += SmallImage.smallImg[1241][4];
//				SmallImage.drawSmallImage(g, 1241, cx, cy - height, 0, mGraphics.HCENTER | mGraphics.TOP);
//				height += 1;
//			} else if (testCharId > 0) {
//				height += SmallImage.smallImg[1116][4];
//				SmallImage.drawSmallImage(g, 1116, cx, cy - height, 0, mGraphics.HCENTER | mGraphics.TOP);
//				height += 1;
//			}
//		}
	}
	int dxHead = 0, dyHead = 0, dxBody = 0, dyBody = 0;

	public void loadFromServer(){
			for(int i = 0; i < 25; i++){
				int id = getBodyPaintId() + i;
				mBitmap img = (mBitmap) SmallImage.imgNew.get(id+"");
				if(img == null){
					SmallImage.imgNew.put(id+"", SmallImage.imgEmpty);
//					Service.gI().requestIcon(id);
				}
				if(i == 24){
					break;
				}
		}
	}
	int tickWolf = 0, dy = 0;
	int hdx = 0, hdy = 0;
	int[] idWolfW = new int[]{ 1715, 1737,  1714, 1738}; // chó trắng 
//	private void paintCharWithoutSkill(mGraphics g) {
//		try {
//			
//			int[] idCoat = null;
//			Part ph = GameScr.parts[head], pl = GameScr.parts[leg], pb = GameScr.parts[body], pW = GameScr.parts[wp];
//			if (arrItemBody != null && arrItemBody[Item.TYPE_MATNA] != null) {
//				ph = GameScr.parts[arrItemBody[Item.TYPE_MATNA].template.part];
//				head = arrItemBody[Item.TYPE_MATNA].template.part;
//			}
//			
//			if (ph.pi == null || ph.pi.Length < 8) {
//				ph = getDefaultHead(cgender);
//			} else {
//				for (int i = 0; i < ph.pi.Length; i++) {
//					if (ph.pi[i] == null || !SmallImage.isExitsImage(ph.pi[i].id)) {
//						ph = getDefaultHead(cgender);
//						break;
//					}
//				}
//			}
//			if (ph.pi[CharInfo[cf][0][0]].id <= 4)
//				idCoat = null;
//			else
//				idCoat = (ph.pi[CharInfo[cf][0][0]].id <= 4) ? null : getClassCoat();
//			if (((statusMe == A_STAND || statusMe == A_NOTHING) && GameCanvas.gameTick % 10 == 0)
//					|| ((statusMe == A_RUN || statusMe == A_WATERRUN) && GameCanvas.gameTick % 2 == 0)
//					|| (GameCanvas.gameTick % 3 == 0 && (statusMe == A_FALL || statusMe == A_JUMP))) {
//				if (idCoat != null) {
//					tickCoat++;
//					if (tickCoat >= idCoat.Length) {
//						tickCoat = 0;
//					}
//				}
//			}
//
//			if (statusMe == A_DEAD) {
//				if (isHaveMoto()){
//					if(arrItemMounts[4].template.id == 485)
//						SmallImage.drawSmallImage(g, 1709, lcx, lcy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//					else if(arrItemMounts[4].template.id == 524){
//						SmallImage.drawSmallImage(g, 2056, lcx, lcy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//					}
//				}
//				SmallImage.drawSmallImage(g, 1040, cx, cy, 0, mGraphics.HCENTER | mGraphics.BOTTOM);
//			} else if (isInvisible) {
//				if (me) {
//					if (GameCanvas.gameTick % 50 == 48 || GameCanvas.gameTick % 50 == 90)
//						SmallImage.drawSmallImage(g, 1196, cx, cy - 18, 0, mGraphics.VCENTER | mGraphics.HCENTER);
//					else
//						SmallImage.drawSmallImage(g, 1195, cx, cy - 18, 0, mGraphics.VCENTER | mGraphics.HCENTER);
//				}
//			} else {
//				
//				if (isMoto) {
//					if(arrItemMounts[4].template.id == 485){ // xe xanh
////						int[] idEff = getEffmoto();
////						if (
////								((statusMe == A_RUN || statusMe == A_WATERRUN) && GameCanvas.gameTick % 2 == 0)
////								/*|| (GameCanvas.gameTick % 3 == 0 && (statusMe == A_FALL || statusMe == A_JUMP))*/) {
////							if (idEff != null) {
////								tickEffmoto++;
////								if (tickEffmoto >= idEff.Length) {
////									tickEffmoto = 0;
////								}
////							}
////						}
////						if (statusMe == A_STAND || statusMe == A_NOTHING)
////							tick = GameCanvas.gameTick % 20 > 12 ? 1 : 0;
////							else if (statusMe == A_RUN || statusMe == A_WATERRUN)
////								tick = GameCanvas.gameTick % 6 > 3 ? 1 : 0;
////								if (statusMe == A_JUMP)
////									hdx = -5 * cdir;
////								if (cdir == 1) { // Phải
//////						
//////									if (idCoat != null) {
//////										if (tickCoat == 0)
//////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 14, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
//////										else if (tickCoat == 1)
//////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 22, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
//////										else if (tickCoat == 2)
//////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 25, cy - 30 , 0, mGraphics.TOP | mGraphics.LEFT);
//////										else
//////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 22, cy - 30 , 0, mGraphics.TOP | mGraphics.LEFT);
//////									}
////									if (statusMe == A_JUMP)
////										SmallImage.drawSmallImage(g, 2070 /*2057*/, cx, cy + 2, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////										//g.drawImage(GameScr.imgxe3, cx, cy + 2,  mGraphics.BOTTOM | mGraphics.HCENTER);
////									else{
////										if(statusMe != A_RUN)
////											SmallImage.drawSmallImage(g, (tick == 0) ? 2068 : 2069 /* 2056 : 2055 */, cx, cy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////										else{
////											SmallImage.drawSmallImage(g, (tick == 0) ? 2068 : 2069 /* 2056 : 2055 */, cx, cy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////											///// bánh trước 
////											if(tickEffmoto == 0)
////												SmallImage.drawSmallImage(g, idEff[0], cx + 19, cy , 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////											else if(tickEffmoto == 1)
////												SmallImage.drawSmallImage(g, idEff[1], cx + 15, cy , 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////											else if(tickEffmoto == 2)
////												SmallImage.drawSmallImage(g, idEff[2], cx + 15, cy , 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////											else
////												SmallImage.drawSmallImage(g, idEff[3], cx + 14, cy , 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////											////// bánh sau
////											
////											if(tickEffmoto == 0)
////												SmallImage.drawSmallImage(g, idEff[0], cx - 22, cy , 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////											else if(tickEffmoto == 1)
////												SmallImage.drawSmallImage(g, idEff[1], cx - 25, cy , 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////											else if(tickEffmoto == 2)
////												SmallImage.drawSmallImage(g, idEff[2], cx - 26, cy , 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////											else 
////												SmallImage.drawSmallImage(g, idEff[3], cx - 27, cy , 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////										}
////										
////										//g.drawImage((tick == 0)? GameScr.imgxe1 : GameScr.imgxe2, cx, cy ,  mGraphics.BOTTOM | mGraphics.HCENTER);
////									}
////						//SmallImage.drawSmallImage(g, getLegId(), cx + 2 * cdir, cy - 7+ tick, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////									SmallImage.drawSmallImage(g, getHeadId(), cx + hdx + dxHead*cdir, cy - CharInfo[0][0][2] + ph.pi[CharInfo[0][0][0]].dy - 12+ tick+ dyHead, 0, mGraphics.TOP | mGraphics.HCENTER);
////									SmallImage.drawSmallImage(g, getBodyPaintId(), cx + dxHead * cdir, (cy - SmallImage.getHeight(getLegId()) - 8 + tick) + dyBody, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////						
////								} else { // Trái
//////									if (idCoat != null) {
//////										if (tickCoat == 0)
//////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 30, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
//////										else if (tickCoat == 1)
//////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 30, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
//////										else if (tickCoat == 2)
//////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 30, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
//////										else
//////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 30, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
//////									}
////									if (statusMe == A_JUMP)
////										SmallImage.drawSmallImage(g, 2070 /*2057*/, cx, cy + 2, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////									else{
////										if(statusMe != A_RUN)
////											SmallImage.drawSmallImage(g, (tick == 0) ? 2068 : 2069 /* 2056 : 2055*/, cx, cy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////										else {
////											SmallImage.drawSmallImage(g, (tick == 0) ? 2068 : 2069 /* 2056 : 2055*/, cx, cy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////											///////////////////////////// banh truoc
////											if(tickEffmoto == 0)
////												SmallImage.drawSmallImage(g, idEff[0], cx - 21, cy , 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////											else if(tickEffmoto == 1)
////												SmallImage.drawSmallImage(g, idEff[1], cx - 18, cy , 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////											else if(tickEffmoto == 2)
////												SmallImage.drawSmallImage(g, idEff[2], cx - 17, cy , 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////											else 
////												SmallImage.drawSmallImage(g, idEff[3], cx - 16, cy , 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////											///////////////////////////// banh sau
////											
////											if(tickEffmoto == 0)
////												SmallImage.drawSmallImage(g, idEff[0], cx + 22 , cy , 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////											else if(tickEffmoto == 1)
////												SmallImage.drawSmallImage(g, idEff[1], cx + 25, cy , 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////											else if(tickEffmoto == 2)
////												SmallImage.drawSmallImage(g, idEff[2], cx + 26, cy , 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////											else 
////												SmallImage.drawSmallImage(g, idEff[3], cx + 27, cy , 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////										}
////											
////									}
////
////						//SmallImage.drawSmallImage(g, getLegId(), cx + 2 * cdir, cy - 7+ tick, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////									SmallImage.drawSmallImage(g, getHeadId(), cx + hdx + dxHead * cdir, cy - CharInfo[0][0][2] + ph.pi[CharInfo[0][0][0]].dy - 12+ tick + dyHead, 2, mGraphics.TOP | mGraphics.HCENTER);
////									SmallImage.drawSmallImage(g, getBodyPaintId(), cx + dxBody * cdir, (cy - SmallImage.getHeight(getLegId()) - 8 + tick) + dyHead, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
////						
////								}
//						if (statusMe == A_STAND || statusMe == A_NOTHING)
//							tick = GameCanvas.gameTick % 20 > 12 ? 1 : 0;
//							else if (statusMe == A_RUN || statusMe == A_WATERRUN)
//								tick = GameCanvas.gameTick % 6 > 3 ? 1 : 0;
//								if (statusMe == A_JUMP)
//									hdx = -5 * cdir;
//								if (cdir == 1) { // Phải
////						
////									if (idCoat != null) {
////										if (tickCoat == 0)
////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 14, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
////										else if (tickCoat == 1)
////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 22, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
////										else if (tickCoat == 2)
////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 25, cy - 30 , 0, mGraphics.TOP | mGraphics.LEFT);
////										else
////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 22, cy - 30 , 0, mGraphics.TOP | mGraphics.LEFT);
////									}
//									if (statusMe == A_JUMP)
//										SmallImage.drawSmallImage(g, 1711 /*2057*/, cx, cy + 2, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//									else
//										SmallImage.drawSmallImage(g, (tick == 0) ? 1709 : 1710 /* 2056 : 2055 */, cx, cy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
////										g.drawRegion(GameScr.imgmoto, 0, 0,58 , 22, 0, cx, cy, mGraphics.BOTTOM | mGraphics.HCENTER);
//						
//						//SmallImage.drawSmallImage(g, getLegId(), cx + 2 * cdir, cy - 7+ tick, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//									SmallImage.drawSmallImage(g, getHeadId(), cx + hdx + dxHead*cdir, cy - CharInfo[0][0][2] + ph.pi[CharInfo[0][0][0]].dy - 12+ tick+ dyHead, 0, mGraphics.TOP | mGraphics.HCENTER);
//									SmallImage.drawSmallImage(g, getBodyPaintId(), cx, cy - SmallImage.getHeight(getLegId()) - 8 + tick, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//						
//								} else { // Trái
////									if (idCoat != null) {
////										if (tickCoat == 0)
////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 30, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////										else if (tickCoat == 1)
////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 30, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////										else if (tickCoat == 2)
////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 30, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////										else
////											SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 30, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////									}
//									if (statusMe == A_JUMP)
//										SmallImage.drawSmallImage(g, 1711 /*2057*/, cx, cy + 2, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//									else
//										SmallImage.drawSmallImage(g, (tick == 0) ? 1709 : 1710 /* 2056 : 2055*/, cx, cy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//										//g.drawRegion(GameScr.imgmoto, 0, 0,58 , 22, 2, cx, cy, mGraphics.BOTTOM | mGraphics.HCENTER);
//						
//
//						//SmallImage.drawSmallImage(g, getLegId(), cx + 2 * cdir, cy - 7+ tick, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//									SmallImage.drawSmallImage(g, getHeadId(), cx + hdx + dxHead * cdir, cy - CharInfo[0][0][2] + ph.pi[CharInfo[0][0][0]].dy - 12+ tick + dyHead, 2, mGraphics.TOP | mGraphics.HCENTER);
//									SmallImage.drawSmallImage(g, getBodyPaintId(), cx, cy - SmallImage.getHeight(getLegId()) - 8 + tick, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//						
//								}
//					}else if(arrItemMounts[4].template.id == 524){ // xe đỏ
//						if (statusMe == A_STAND || statusMe == A_NOTHING)
//							tick = GameCanvas.gameTick % 20 > 12 ? 1 : 0;
//							else if (statusMe == A_RUN || statusMe == A_WATERRUN)
//								tick = GameCanvas.gameTick % 6 > 3 ? 1 : 0;
//								if (statusMe == A_JUMP)
//									hdx = -5 * cdir;
//								if (cdir == 1) { // Phải
////						if (idCoat != null) {
////							if (tickCoat == 0)
////								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 14, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
////							else if (tickCoat == 1)
////								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 22, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
////							else if (tickCoat == 2)
////								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 25, cy - 30 , 0, mGraphics.TOP | mGraphics.LEFT);
////							else
////								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 22, cy - 30 , 0, mGraphics.TOP | mGraphics.LEFT);
////						}
////						
//									if (statusMe == A_JUMP)
//										SmallImage.drawSmallImage(g, 2057, cx, cy + 2, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//									else
//										SmallImage.drawSmallImage(g, (tick == 0) ? 2056 : 2055, cx, cy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//						
//						//SmallImage.drawSmallImage(g, getLegId(), cx + 2 * cdir, cy - 7+ tick, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//									SmallImage.drawSmallImage(g, getHeadId(), cx + hdx + dxHead*cdir, cy - CharInfo[0][0][2] + ph.pi[CharInfo[0][0][0]].dy - 12+ tick+ dyHead, 0, mGraphics.TOP | mGraphics.HCENTER);
//									SmallImage.drawSmallImage(g, getBodyPaintId(), cx, cy - SmallImage.getHeight(getLegId()) - 8 + tick, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//						
//								} else { // Trái
////						if (idCoat != null) {
////							if (tickCoat == 0)
////								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 30, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////							else if (tickCoat == 1)
////								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 30, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////							else if (tickCoat == 2)
////								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 30, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////							else
////								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 30, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////						}
//									if (statusMe == A_JUMP)
//										SmallImage.drawSmallImage(g, 2057, cx, cy + 2, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//									else
//										SmallImage.drawSmallImage(g, (tick == 0) ? 2056 : 2055, cx, cy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//						
//
//						//SmallImage.drawSmallImage(g, getLegId(), cx + 2 * cdir, cy - 7+ tick, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//									SmallImage.drawSmallImage(g, getHeadId(), cx + hdx + dxHead * cdir, cy - CharInfo[0][0][2] + ph.pi[CharInfo[0][0][0]].dy - 12+ tick + dyHead, 2, mGraphics.TOP | mGraphics.HCENTER);
//									SmallImage.drawSmallImage(g, getBodyPaintId(), cx, cy - SmallImage.getHeight(getLegId()) - 8 + tick, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//						
//								}
//					}
//				} else if (isWolf) {
//					
//					if (statusMe == A_STAND || statusMe == A_NOTHING) {
//						tickWolf = GameCanvas.gameTick % 20 > 12 ? 1 : 0;
//						dy = - tickWolf;
//					} else if (statusMe == A_RUN || statusMe == A_WATERRUN) {
//						if (GameCanvas.gameTick % 12 <= 3)
//							tickWolf = 0;
//						else if (GameCanvas.gameTick % 12 <= 6){
//							tickWolf = 1;
//							dy = 2;
//						}
//						else if (GameCanvas.gameTick % 12 <= 9){
//							tickWolf = 2;
//							dy = 4;
//						}
//						else{ 
//							tickWolf = 3;
//							dy = 2;
//						}
//					
//						
//					}
//					
//					int[] idWolfR = new int[]{ 2050, 2053, 2049, 2052}; // chó đen 
//					int[] YeffRight =  new int []{ cy - 22, cy - 23, cy - 22, cy - 23};
//					int[] YeffLeft =  new int []{ cy - 19, cy - 23, cy - 19, cy - 19};
//					if (statusMe == A_JUMP){
//						hdx = -5 * cdir;
//						hdy = 5;
//					}
//					else hdx = -3*cdir;
//					if(arrItemMounts[4].template.id == 523){ // chó đen
//						if (cdir == 1) { // Phải
//					
////							if (idCoat != null) {
////								if (tickCoat == 0)
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 27, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
////								else if (tickCoat == 1)
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 27, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
////								else if (tickCoat == 2)
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 27, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
////								else
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 27, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
////							}
//							SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx + CharInfo[cf][3][1] + pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy - 10, 0, 0);
//							if (statusMe == A_JUMP)
//								SmallImage.drawSmallImage(g, 2051, cx, cy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//							else if (statusMe == A_FALL)
//								SmallImage.drawSmallImage(g, 2052, cx, cy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//							else if (statusMe == A_STAND || statusMe == A_NOTHING)
//								SmallImage.drawSmallImage(g, (tick == 0) ? 2047 : 2048, cx, cy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//							else if (statusMe == A_RUN || statusMe == A_WATERRUN) {
//								SmallImage.drawSmallImage(g, idWolfR[tickWolf], cx, cy - dy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//							}
//						
//
////						SmallImage.drawSmallImage(g, getLegId(), cx - 4 * cdir + hdx, cy - 9, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//							SmallImage.drawSmallImage(g, getHeadId(), cx + hdx + dxHead * cdir, cy - CharInfo[0][0][2] + ph.pi[CharInfo[0][0][0]].dy - 12 - hdy - dy+ dyHead, 0, mGraphics.TOP | mGraphics.HCENTER);
//							SmallImage.drawSmallImage(g, getBodyPaintId(), cx + hdx, cy - SmallImage.getHeight(getLegId()) - 9- hdy - dy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//						} else { // Trái
//						
////							if (idCoat != null) {
////								if (tickCoat == 0)
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 3, cy - 32, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////								else if (tickCoat == 1)
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 3, cy - 32, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////								else if (tickCoat == 2)
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 3, cy - 32, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////								else
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 3, cy - 32, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////							}
//							SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx - CharInfo[cf][3][1] - pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy - 10, 2, 24);
//							if (statusMe == A_JUMP)
//								SmallImage.drawSmallImage(g, 2051, cx, cy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//							else if (statusMe == A_FALL)
//								SmallImage.drawSmallImage(g, 2052, cx, cy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//							else if (statusMe == A_STAND || statusMe == A_NOTHING)
//								SmallImage.drawSmallImage(g, (tick == 0) ? 2047 : 2048, cx, cy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//							else if (statusMe == A_RUN || statusMe == A_WATERRUN) {
//								SmallImage.drawSmallImage(g, idWolfR[tickWolf], cx, cy - dy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//							}
//						
//
////						SmallImage.drawSmallImage(g, getLegId(), cx - 4 * cdir + hdx, cy - 9, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//							SmallImage.drawSmallImage(g,  getHeadId(), cx + hdx+ dxHead*cdir, cy - CharInfo[0][0][2] + ph.pi[CharInfo[0][0][0]].dy - 12 - hdy - dy+ dyHead, 2, mGraphics.TOP | mGraphics.HCENTER);
//							SmallImage.drawSmallImage(g, getBodyPaintId(), cx + hdx, cy - SmallImage.getHeight(getLegId()) - 9- hdy  - dy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//						}
//					} else if(arrItemMounts[4].template.id == 443){ // chó trắng
//						if (cdir == 1) { // Phải
//							
////							if (idCoat != null) {
////								if (tickCoat == 0)
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 27, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
////								else if (tickCoat == 1)
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 27, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
////								else if (tickCoat == 2)
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 27, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
////								else
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 27, cy - 30, 0, mGraphics.TOP | mGraphics.LEFT);
////							}
//						
//							SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx + CharInfo[cf][3][1] + pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy - 10, 0, 0);
//							if (statusMe == A_JUMP){
//								SmallImage.drawSmallImage(g, 1716, cx, cy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//								g.drawRegion(GameScr.imgMatcho, 0, tickEffWolf * 3, 3, 3, 0, cx + 21, cy - 30, 0);
//							}
//							else if (statusMe == A_FALL){
//								SmallImage.drawSmallImage(g, 1717, cx, cy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//								g.drawRegion(GameScr.imgMatcho, 0, tickEffWolf * 3, 3, 3, 0,  cx + 21, cy - 19, 0);
//							}
//							else if (statusMe == A_STAND || statusMe == A_NOTHING){
//								//tick = 1;
//								SmallImage.drawSmallImage(g, (tick == 0) ? 1712 : 1713, cx, cy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//								//for(int i = 0; i < 6; i++)
//								g.drawRegion(GameScr.imgMatcho, 0, tickEffWolf * 3, 3, 3, 0,  cx + 21, cy - 19, 0);
//							}
//							else if (statusMe == A_RUN || statusMe == A_WATERRUN) {
//								SmallImage.drawSmallImage(g, idWolfW[tickWolf], cx, cy - dy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//								g.drawRegion(GameScr.imgMatcho, 0, tickEffWolf * 3, 3, 3, 0,  cx + 21, YeffRight[tickWolf], 0);
//							}
//						
//
////						SmallImage.drawSmallImage(g, getLegId(), cx - 4 * cdir + hdx, cy - 9, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//							SmallImage.drawSmallImage(g, getHeadId(), cx + hdx + dxHead * cdir, cy - CharInfo[0][0][2] + ph.pi[CharInfo[0][0][0]].dy - 12 - hdy - dy+ dyHead, 0, mGraphics.TOP | mGraphics.HCENTER);
//							SmallImage.drawSmallImage(g, getBodyPaintId(), cx + hdx, cy - SmallImage.getHeight(getLegId()) - 9- hdy - dy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//						} else { // Trái
//						
//						
////							if (idCoat != null) {
////								if (tickCoat == 0)
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 3, cy - 32, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////								else if (tickCoat == 1)
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 3, cy - 32, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////								else if (tickCoat == 2)
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 3, cy - 32, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////								else
////									SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 3, cy - 32, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
////							}
//							SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx - CharInfo[cf][3][1] - pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy - 10, 2, 24);
//							if (statusMe == A_JUMP){
//								SmallImage.drawSmallImage(g, 1716, cx, cy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//								g.drawRegion(GameScr.imgMatcho, 0, tickEffWolf * 3, 3, 3, 0, cx - 23, cy - 30, 0);
//							}
//							else if (statusMe == A_FALL){
//								SmallImage.drawSmallImage(g, 1717, cx, cy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//								g.drawRegion(GameScr.imgMatcho, 0, tickEffWolf * 3, 3, 3, 0, cx - 24, cy - 20, 0);
//							}
//							else if (statusMe == A_STAND || statusMe == A_NOTHING){
//								//tick = 1;
//								SmallImage.drawSmallImage(g, (tick == 0) ? 1712 : 1713, cx, cy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//								//for(int i = 0; i < 6; i++)
//								g.drawRegion(GameScr.imgMatcho, 0, tickEffWolf * 3, 3, 3, 0, cx - 24, cy - 20, 0);
//							}
//							else if (statusMe == A_RUN || statusMe == A_WATERRUN) {
//								SmallImage.drawSmallImage(g, idWolfW[tick], cx, cy - dy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//								g.drawRegion(GameScr.imgMatcho, 0, tickEffWolf * 3, 3, 3, 0, cx - 24, YeffLeft[tickWolf], 0);
//							}
//						
//
////						SmallImage.drawSmallImage(g, getLegId(), cx - 4 * cdir + hdx, cy - 9, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//							SmallImage.drawSmallImage(g,  getHeadId(), cx + hdx+ dxHead*cdir, cy - CharInfo[0][0][2] + ph.pi[CharInfo[0][0][0]].dy - 12 - hdy - dy+ dyHead, 2, mGraphics.TOP | mGraphics.HCENTER);
//							SmallImage.drawSmallImage(g, getBodyPaintId(), cx + hdx, cy - SmallImage.getHeight(getLegId()) - 9- hdy  - dy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//						}
//					}
//				} else {
//					if (cdir == 1) { // Phải
//						if(isMotoBehind && !isMoto && !isWolf){
//							if(arrItemMounts[4].template.id == 485)
//								SmallImage.drawSmallImage(g,  1709 /*2055*/, lcx, lcy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//							else if(arrItemMounts[4].template.id == 524)
//								SmallImage.drawSmallImage(g,  2055, lcx, lcy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//						}
//						if (idCoat != null) {
//							if (tickCoat == 0)
//								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 14, cy - 18, 0, mGraphics.TOP | mGraphics.LEFT);
//							else if (tickCoat == 1)
//								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 22, cy - 18, 0, mGraphics.TOP | mGraphics.LEFT);
//							else if (tickCoat == 2)
//								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 25, cy - 18, 0, mGraphics.TOP | mGraphics.LEFT);
//							else
//								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 22, cy - 18, 0, mGraphics.TOP | mGraphics.LEFT);
//						}
//						
//						
//						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx + CharInfo[cf][3][1] + pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 0, 0);
//						SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
//						if (statusMe != A_RUN)
//							paintClanEffect(g, cx + 7, cy - 2);
//						SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
//						SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
//						
//						if (statusMe == A_RUN) {
//							paintClanEffectRun(g, cx - 14, cy - 2);
//							paintClanEffect2(g, cx + 7, cy - 1);
//						} else {
//							paintClanEffect(g, cx - 7, cy - 2);
//							paintClanEffect2(g, cx + 11, cy - 2);
//						}
//
//					} else { // Trái
//						if(isMotoBehind && !isMoto && !isWolf){
//							if(arrItemMounts[4].template.id == 485)
//								SmallImage.drawSmallImage(g, 1709, lcx, lcy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//							else if(arrItemMounts[4].template.id == 524)
//								SmallImage.drawSmallImage(g, 2055, lcx, lcy, 2, mGraphics.BOTTOM | mGraphics.HCENTER);
//						}
//						if (idCoat != null) {
//							if (tickCoat == 0)
//								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 18, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
//							else if (tickCoat == 1)
//								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 18, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
//							else if (tickCoat == 2)
//								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 18, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
//							else
//								SmallImage.drawSmallImage(g, idCoat[tickCoat], cx - 7, cy - 18, Sprite.TRANS_MIRROR, mGraphics.TOP | mGraphics.LEFT);
//						}
//						
//						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx - CharInfo[cf][3][1] - pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 2, 24);
//						SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
//						if (statusMe != A_RUN)
//							paintClanEffect(g, cx - 7, cy - 2);
//						SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
//						SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
//
//						
//						if (statusMe == A_RUN) {
//							paintClanEffectRun(g, cx + 14, cy - 2);
//							paintClanEffect2(g, cx - 3, cy - 2);
//						} else {
//							paintClanEffect(g, cx + 5, cy - 2);
//							paintClanEffect2(g, cx - 7, cy - 2);
//						}
//					}
//				}
//			}
//			
//			if (isLockAttack)
//				SmallImage.drawSmallImage(g, 290, cx, cy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
//		} catch (Exception e) {
//		}
//	}
	static int cxtemp; 
	private void paintCharWithoutSkill(mGraphics g) {
//		SmallImage.readImage();
		try {
			if(ischangingMap)
				return;
//			head = 0;
//			leg = 2;
//			body = 1;
//			System.out.println("head ---->"+head+" leg -----> "+leg +" body ---->"+body+ " wp ---> "+wp);
//			Cout.println("cx ----> "+cx+" ::: "+cy);
//			cy = GameScr.cmy + 200;
//			cy = 235;
//			for(int i = 0; i < GameScr.vCharInMap.size(); i++){
//				Char c = (Char) GameScr.vCharInMap.elementAt(i);
//				
//			}
//			System.out.println("CX---------> "+cx+" :::::: "+cy);
			int[] idCoat = null;
			Part ph = GameScr.parts[head], pl = GameScr.parts[leg], pb = GameScr.parts[body], pW = GameScr.parts[wp];
			cxtemp =  cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx;
			
			
//			if (arrItemBody != null && arrItemBody[Item.TYPE_MATNA] != null) {
//				ph = GameScr.parts[arrItemBody[Item.TYPE_MATNA].template.part];
//				head = arrItemBody[Item.TYPE_MATNA].template.part;
//			}
			
//			if (ph.pi == null || ph.pi.Length < 8) {
//				ph = getDefaultHead(cgender);
//			} else {
//				for (int i = 0; i < ph.pi.Length; i++) {
//					if (ph.pi[i] == null || !SmallImage.isExitsImage(ph.pi[i].id)) {
//						ph = getDefaultHead(cgender);
//						break;
//					}
//				}
//			}
			
					
					
//				if(cdir == 1)	{	
//						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][1][0]].id, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
////						SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
//						
//						SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
//						SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
//						
//					} else{
//						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][1][0]].id, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
////						SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
//						
//						SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
//						SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
//
//						
//						
//					}
//			if(statusMe == A_RUN)
//				System.out.println("PL ---> "+pl.pi[CharInfo[cf][1][0]].id);

            if (cdir == 1)
            {
                //				System.out.println("-------------> "+charID);
                if (statusMe != A_RUN)
                {
                    //					SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx + CharInfo[cf][3][1] + pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 0, 0);
                    SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                    SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                    SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                }
                else
                {
                    if (cf != 7)
                    {
                        //						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx + CharInfo[cf][3][1] + pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                    }
                    else
                    {
                        //						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx + CharInfo[cf][3][1] + pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id + 1, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id + 1, cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                    }
                }

            }
            else
            {
                if (statusMe != A_RUN)
                {
                    //				SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx - CharInfo[cf][3][1] - pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 2, 24);
                    SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                    SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                    SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                }
                else
                {
                    if (cf != 7)
                    {
                        //						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx - CharInfo[cf][3][1] - pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                    }
                    else
                    {
                        //						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx - CharInfo[cf][3][1] - pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id + 1, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id + 1, cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                    }
                }


            }


            //			if (isLockAttack)
            //				SmallImage.drawSmallImage(g, 290, cx, cy, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
            //			if(statusMe == A_STAND)
            //				paintEffPhanthan(g);
        }
        catch (Exception e)
        {
        }
    }
    private void paintCharWithoutSkill(mGraphics g, int cx, int cy)
    {
        //		SmallImage.readImage();
        try
        {
            if (ischangingMap)
                return;
            int[] idCoat = null;
            Part ph = GameScr.parts[head], pl = GameScr.parts[leg], pb = GameScr.parts[body], pW = GameScr.parts[wp];
            cxtemp = cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx;

            if (cdir == 1)
            {
                //				System.out.println("-------------> "+charID);
                if (statusMe != A_RUN)
                {
                    //					SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx + CharInfo[cf][3][1] + pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 0, 0);
                    SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                    SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                    SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                }
                else
                {
                    if (cf != 7)
                    {
                        //						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx + CharInfo[cf][3][1] + pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                    }
                    else
                    {
                        //						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx + CharInfo[cf][3][1] + pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id + 1, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id + 1, cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                    }
                }

            }
            else
            {
                if (statusMe != A_RUN)
                {
                    //				SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx - CharInfo[cf][3][1] - pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 2, 24);
                    SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                    SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                    SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                }
                else
                {
                    if (cf != 7)
                    {
                        //						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx - CharInfo[cf][3][1] - pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                    }
                    else
                    {
                        //						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx - CharInfo[cf][3][1] - pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id + 1, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id + 1, cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                    }
                }


            }
        }
        catch (Exception e)
        {
        }
    }

	private int getLegId() {

		switch (leg) {
		case 0:
			return 26;
		case 4:
			return 58;
		case 6:
			return 86;
		case 8:
			return 114;
		case 9:
			return 123;
		case 17:
			return 353;
		case 19:
			return 379;
		case 21:
			return 405;
		case 30:
			return 484;
		case 33:
			return 518;
		case 35:
			return 544;
		case 37:
			return 571;
		case 39:
			return 810;
		case 43:
			return 982;
		case 95:
			return 1156;
		case 142:
			return 1360;
		case 155:
			return 1494;
		case 157:
			return 1519;
		default:
			return 26;
		}
	}
	private int getBodyPaintId() {
		if (statusMe == A_JUMP) {
			//System.out.println("Body ---> "+body);
			switch (body) {
			case 1:
				return 13;
			case 3:
				return 45;
			case 5:
				return 73;
			case 7:
				return 101;
			case 10:
				return 137;
			case 18:
				return 365;
			case 20:
				return 391;
			case 22:
				return 417;
			case 29:
				return 472;
			case 32:
				return 506;
			case 34:
				return 531;
			case 36:
				return 559;
			case 38:
				return 798;
			case 42:
				return 970;
			case 94:
				return 1142;
			case 141:
				return 1348;
			case 154:
				return 1482;
			case 156:
				return 1507;
			case 157:
				return 1811;
			case 180:
				return 1959;
			case 183:
				return 1987;
			default:
				return 13;
			}
		} else {
			if(!isBocdau){
			switch (body) {
			case 1:
				return 9;
			case 3:
				return 41;
			case 5:
				return 70;
			case 7:
				return 97;
			case 10:
				return 133;
			case 18:
				return 369;
			case 20:
				return 395;
			case 22:
				return 421;
			case 29:
				return 468;
			case 32:
				return 502;
			case 34:
				return 540;
			case 36:
				return 555;
			case 38:
				return 794;
			case 42:
				return 966;
			case 94:
				return 1139;
			case 141:
				return 1344;
			case 154:
				return 1479;
			case 156:
				return 1502;
			case 157:
				return 1807;
			case 173:
				return 1892;
			case 180:
				return 1955;
			case 183:
				return 1983;
			default:
				return 9;
			}
			}else{
				switch (body) {
				case 1:
					return 13;
				case 3:
					return 45;
				case 5:
					return 73;
				case 7:
					return 101;
				case 10:
					return 137;
				case 18:
					return 365;
				case 20:
					return 391;
				case 22:
					return 417;
				case 29:
					return 472;
				case 32:
					return 506;
				case 34:
					return 531;
				case 36:
					return 559;
				case 38:
					return 798;
				case 42:
					return 970;
				case 94:
					return 1142;
				case 141:
					return 1348;
				case 154:
					return 1482;
				case 156:
					return 1507;
				case 157:
					return 1811;
				case 180:
					return 1959;
				case 183:
					return 1987;
				default:
					return 13;
				}
			}
		}
	}

	private int getHeadId() {
		dxHead = dyHead = 0;
		//System.out.println("Head ID --> "+head);
		if(statusMe == A_JUMP){
			switch (head) {
			case 2:
				return 33;
			case 11:
				dyHead = 3;
				return 147;
			case 23:
				return 427;
			case 24:
				return 430;
			case 25:
				return 433;
			case 26:
				return 436;
			case 27:
				/////////////////
//				dxHead = -6;
//				dyHead = -5;
				////////////////
//				dxHead = -4;
//				dyHead = -4;
				return 439;
			case 28:
				return 442;
			case 112:
//				dxHead = -2;
				dyHead = -1;
				return 148;
			case 113:
				dxHead = -5;
				return 443;
			case 124:
				return 1235;
			case 125:
				dyHead = 2;
				//dxHead = 2;
				return 1237;
			case 126:
				return 1255;
			case 127:
				dxHead = -2;
				return 1257;
			case 137:
				return 1309;
			case 138:
				return 1311;
			case 139:
				return 1315;
			case 140:
				return 1313;
			case 146:
				return 1416;
			case 147:
				return 1418;
			case 148:
				dyHead = -1;
				return 1422;
			case 149:
				dxHead = -2;
//				dxHead = -3;
				return 1424;
			case 150:
				return 1441;
			case 151:
//				dxHead =-1;
				dyHead = 1;
				return 1439;
			case 152:
				dyHead = 1;
				return 1447;
			case 153:
				dxHead =-1;
				dyHead = 2;
				return 1445;
			case 158:
				dxHead = -4;
				return 1585;
			case 159:
				dxHead = -1;
				return 1589;
			case 160:
				dxHead = 2;
				return 1587;
			case 161:
				dyHead = -1;
				dxHead = -2;
				return 1595;
			case 162:
				dxHead = -6;
				dyHead = 2;
				return 1597;
			case 163:
				dxHead = -3;
				dyHead = 2;
				return 1604;
			case 179:
				return 1978;
			case 182:
				return 2006;
			default:
				return 33;
			}
		}else{
			if(!isBocdau){
				switch (head) {
				case 2:
					return 33;
				case 11:
					dyHead = 3;
					return 147;
				case 23:
					return 427;
				case 24:
					return 430;
				case 25:
					return 433;
				case 26:
					return 436;
				case 27:
	//				dxHead = -4;
	//				dyHead = 2;
					return 439;
				case 28:
					return 442;
				case 112:
					return 148;
				case 113:
					dxHead = -2;
					return 443;
				case 124:
					return 1235;
				case 125:
					dyHead = 2;
					//dxHead = 2;
					return 1237;
				case 126:
					return 1255;
				case 127:
					dxHead = -2;
					return 1257;
				case 137:
					return 1309;
				case 138:
					return 1311;
				case 139:
					return 1315;
				case 140:
					return 1313;
				case 146:
					return 1416;
				case 147:
					return 1418;
				case 148:
					return 1422;
				case 149:
					dxHead = -3;
					return 1424;
				case 150:
					return 1441;
				case 151:
	//				dxHead =-1;
					dyHead = 1;
					return 1439;
				case 152:
					return 1447;
				case 153:
					dxHead =-1;
					dyHead = 2;
					return 1445;
				case 158:
					dxHead = -4;
					return 1585;
				case 159:
					dxHead = -1;
					return 1589;
				case 160:
					return 1587;
				case 161:
					return 1595;
				case 162:
					dxHead = -6;
					dyHead = 2;
					return 1597;
				case 163:
					dxHead = -3;
					dyHead = 2;
					return 1604;
				case 179:
					return 1978;
				case 182:
					return 2006;
				default:
					return 33;
				}
			}else{
				switch (head) {
				case 2:
					return 33;
				case 11:
					dyHead = 3;
					return 147;
				case 23:
					return 427;
				case 24:
					return 430;
				case 25:
					return 433;
				case 26:
					return 436;
				case 27:
					/////////////////
					dxHead = -6;
//					dyHead = -5;
					////////////////
//					dxHead = -4;
//					dyHead = -4;
					return 439;
				case 28:
					return 442;
				case 112:
//					dxHead = -2;
					dyHead = -1;
					return 148;
				case 113:
					dxHead = -5;
					return 443;
				case 124:
					return 1235;
				case 125:
					dyHead = 2;
					//dxHead = 2;
					return 1237;
				case 126:
					return 1255;
				case 127:
					dxHead = -2;
					return 1257;
				case 137:
					return 1309;
				case 138:
					return 1311;
				case 139:
					return 1315;
				case 140:
					return 1313;
				case 146:
					return 1416;
				case 147:
					return 1418;
				case 148:
					dyHead = -1;
					return 1422;
				case 149:
					dxHead = -2;
//					dxHead = -3;
					return 1424;
				case 150:
					return 1441;
				case 151:
//					dxHead =-1;
					dyHead = 1;
					return 1439;
				case 152:
					dyHead = 1;
					return 1447;
				case 153:
					dxHead =-1;
					dyHead = 2;
					return 1445;
				case 158:
					dxHead = -4;
					return 1585;
				case 159:
					dxHead = -1;
					return 1589;
				case 160:
					dxHead = 2;
					return 1587;
				case 161:
					dyHead = -1;
					dxHead = -2;
					return 1595;
				case 162:
					dxHead = -6;
					dyHead = 2;
					return 1597;
				case 163:
					dxHead = -3;
					dyHead = 2;
					return 1604;
				case 179:
					return 1978;
				case 182:
					return 2006;
				default:
					return 33;
				}
			}
		}
			
	}

//	private int[] getClanEffect() {
//		if (statusMe != A_NOTHING && statusMe != A_STAND && statusMe != A_RUN && statusMe != A_WATERRUN && statusMe != A_WATERDOWN)
//			return null;
//		int[] imgId = null;
//		if (me) {
//			if (arrItemBody[Item.TYPE_BAOTAY] != null) {
//				if (arrItemBody[Item.TYPE_BAOTAY].template.id == 425)
//					imgId = new int[] { 1687, 1688, 1689, 1690, 1691 };
//				else if (arrItemBody[Item.TYPE_BAOTAY].template.id == 426)
//					imgId = new int[] { 1682, 1683, 1684, 1685, 1686 };
//				else if (arrItemBody[Item.TYPE_BAOTAY].template.id == 427)
//					imgId = new int[] { 1677, 1678, 1679, 1680, 1681 };
//			}
//		} else {
//			if (glove == -1)
//				return null;
//			if (glove == 425)
//				imgId = new int[] { 1687, 1688, 1689, 1690, 1691 };
//			else if (glove == 426)
//				imgId = new int[] { 1682, 1683, 1684, 1685, 1686 };
//			else if (glove == 427)
//				imgId = new int[] { 1677, 1678, 1679, 1680, 1681 };
//		}
//		return imgId;
//	}

//	public void paintClanEffect(mGraphics g, int x, int y) {
//
//		int[] id = getClanEffect();
//		if (id == null)
//			return;
//		int trans = 0;
//
//		int tick = GameCanvas.gameTick % 13;
//		if (tick > 9)
//			SmallImage.drawSmallImage(g, id[0], x, y, trans, mGraphics.HCENTER | mGraphics.BOTTOM);
//		else if (tick > 6)
//			SmallImage.drawSmallImage(g, id[1], x, y + 2, trans, mGraphics.HCENTER | mGraphics.BOTTOM);
//		else if (tick > 3)
//			SmallImage.drawSmallImage(g, id[2], x - 2, y + 1, trans, mGraphics.HCENTER | mGraphics.BOTTOM);
//		else
//			SmallImage.drawSmallImage(g, id[3], x - 2, y, trans, mGraphics.HCENTER | mGraphics.BOTTOM);

//	}

//	private void paintClanEffectRun(mGraphics g, int x, int y) {
//
//		int[] id = getClanEffect();
//		if (id == null)
//			return;
//		int trans = (cdir == 1) ? Sprite.TRANS_ROT270 : Sprite.TRANS_ROT90;
//		int anchor = (cdir == -1) ? mGraphics.BOTTOM | mGraphics.RIGHT : mGraphics.BOTTOM | mGraphics.LEFT;
//
//		int tick = GameCanvas.gameTick % 13;
//		if (tick > 9)
//			SmallImage.drawSmallImage(g, id[0], x, y, trans, anchor);
//		else if (tick > 6)
//			SmallImage.drawSmallImage(g, id[1], x, y, trans, anchor);
//		else if (tick > 3)
//			SmallImage.drawSmallImage(g, id[2], x, y, trans, anchor);
//		else
//			SmallImage.drawSmallImage(g, id[3], x, y, trans, anchor);
//
//	}

//	public void paintClanEffect2(mGraphics g, int x, int y) {
//		int[] id = getClanEffect();
//		if (id == null)
//			return;
//		SmallImage.drawSmallImage(g, id[4], x - 2, y, 0, mGraphics.HCENTER | mGraphics.BOTTOM);
//
//	}

    public void paintCharWithSkill(mGraphics g)
    {
        int line = 0;
        try
        {
            SkillInfoPaint[] skillInfoPaintt = skillInfoPaint();
            cf = skillInfoPaintt[indexSkill].status;
            if (skillInfoPaintt[indexSkill].effS0Id != 0)
            {
                eff0 = GameScr.efs[skillInfoPaintt[indexSkill].effS0Id - 1];
                i0 = dx0 = dy0 = 0;
            }
            line = 1;
            if (skillInfoPaintt[indexSkill].effS1Id != 0)
            {
                eff1 = GameScr.efs[skillInfoPaintt[indexSkill].effS1Id - 1];
                i1 = dx1 = dy1 = 0;
            }
            if (skillInfoPaintt[indexSkill].effS2Id != 0)
            {
                eff2 = GameScr.efs[skillInfoPaintt[indexSkill].effS2Id - 1];
                i2 = dx2 = dy2 = 0;
            }
            SkillInfoPaint[] sp = skillInfoPaintt;

            if (sp != null && sp[indexSkill] != null && sp[indexSkill].arrowId != 0)
            {
                arr = new Arrow(Char.myChar(), GameScr.arrs[sp[indexSkill].arrowId - 1]);
                arr.life = 10;
                arr.ax = cx + sp[indexSkill].adx;
                arr.ay = cy + sp[indexSkill].ady + 8;
            }
            paintCharWithoutSkill(g);
            line = 1;
            if (cdir == 1)
            {
                if (eff0 != null)
                {
                    if (dx0 == 0)
                        dx0 = skillInfoPaintt[indexSkill].e0dx;
                    if (dy0 == 0)
                        dy0 = skillInfoPaintt[indexSkill].e0dy;
                    SmallImage.drawSmallImage(g, eff0.arrEfInfo[i0].idImg, cx + dx0 + eff0.arrEfInfo[i0].dx, cy + 8 + dy0 + eff0.arrEfInfo[i0].dy, 0,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    i0++;
                    if (i0 >= eff0.arrEfInfo.Length)
                    {
                        eff0 = null;
                        i0 = dx0 = dy0 = 0;
                    }
                }
                if (eff1 != null)
                {
                    if (dx1 == 0)
                        dx1 = skillInfoPaintt[indexSkill].e1dx;
                    if (dy1 == 0)
                        dy1 = skillInfoPaintt[indexSkill].e1dy;
                    SmallImage.drawSmallImage(g, eff1.arrEfInfo[i1].idImg, cx + dx1 + eff1.arrEfInfo[i1].dx, cy + 8 + dy1 + eff1.arrEfInfo[i1].dy, 0,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    i1++;
                    if (i1 >= eff1.arrEfInfo.Length)
                    {
                        eff1 = null;
                        i1 = dx1 = dy1 = 0;
                    }
                }
                if (eff2 != null)
                {
                    if (dx2 == 0)
                        dx2 = skillInfoPaintt[indexSkill].e2dx;
                    if (dy2 == 0)
                        dy2 = skillInfoPaintt[indexSkill].e2dy;
                    SmallImage.drawSmallImage(g, eff2.arrEfInfo[i2].idImg, cx + dx2 + eff2.arrEfInfo[i2].dx, cy + 8 + dy2 + eff2.arrEfInfo[i2].dy, 0,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    i2++;
                    if (eff2.arrEfInfo != null)
                    {
                        if (i2 >= eff2.arrEfInfo.Length)
                        {
                            eff2 = null;
                            i2 = dx2 = dy2 = 0;
                        }
                    }
                }
            }
            else
            {
                if (eff0 != null)
                {
                    if (dx0 == 0)
                        dx0 = skillInfoPaintt[indexSkill].e0dx;
                    if (dy0 == 0)
                        dy0 = skillInfoPaintt[indexSkill].e0dy;
                    SmallImage.drawSmallImage(g, eff0.arrEfInfo[i0].idImg, cx - dx0 - eff0.arrEfInfo[i0].dx, cy + 8 + dy0 + eff0.arrEfInfo[i0].dy, 2,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    i0++;
                    if (i0 >= eff0.arrEfInfo.Length)
                    {
                        eff0 = null;
                        i0 = 0;
                        dx0 = 0;
                        dy0 = 0;
                    }
                }
                if (eff1 != null)
                {
                    if (dx1 == 0)
                        dx1 = skillInfoPaintt[indexSkill].e1dx;
                    if (dy1 == 0)
                        dy1 = skillInfoPaintt[indexSkill].e1dy;
                    SmallImage.drawSmallImage(g, eff1.arrEfInfo[i1].idImg, cx - dx1 - eff1.arrEfInfo[i1].dx, cy + 8 + dy1 + eff1.arrEfInfo[i1].dy, 2,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    i1++;
                    if (i1 >= eff1.arrEfInfo.Length)
                    {
                        eff1 = null;
                        i1 = 0;
                        dx1 = 0;
                        dy1 = 0;
                    }
                }
                if (eff2 != null)
                {
                    if (dx2 == 0)
                        dx2 = skillInfoPaintt[indexSkill].e2dx;
                    if (dy2 == 0)
                        dy2 = skillInfoPaintt[indexSkill].e2dy;
                    SmallImage.drawSmallImage(g, eff2.arrEfInfo[i2].idImg, cx - dx2 - eff2.arrEfInfo[i2].dx, cy + 8 + dy2 + eff2.arrEfInfo[i2].dy, 2,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    i2++;
                    if (eff2.arrEfInfo != null)
                    {
                        if (i2 >= eff2.arrEfInfo.Length)
                        {
                            eff2 = null;
                            i2 = 0;
                            dx2 = 0;
                            dy2 = 0;
                        }
                    }
                }
            }
            line = 2;
        }
        catch (Exception e)
        {
            //			Cout.println("loi paint charskill ---> "+line);
            // e.printStackTrace();
        }
    }

	public void callEff(int effId) {
		indexEff = 0;
		eff = GameScr.efs[effId];
	}

	public void callEffTask(int effId) {
		indexEffTask = 0;
		effTask = GameScr.efs[effId];
	}

	public static int getIndexChar(int ID) {
		for (int i = 0; i < GameScr.vCharInMap.size(); i++) {
			Char c = (Char) GameScr.vCharInMap.elementAt(i);
			if (c.charID == ID)
				return i;

		}
		return -1;
	}

	public void moveTo(int toX, int toY) {
        //		Cout.println(cy+" moveto  "+toY);
        if (Res.abs(toX - cx) > 200 || Res.abs(toY - cy) > 200)
        {
            cx = toX;
            cy = toY;
            vMovePoints.removeAllElements();
            statusMe = A_NOTHING;
            return;
        }

        int dirX = 0, cact = 0;
        int dx = toX - cx;
        int dy = toY - cy;
        if (dx == 0 && dy == 0)
            cact = A_STAND;
        else if (dy == 0)
        {
            cact = A_RUN;
            //updateEffwolfMove();
            //			for(int i = 0; i < vDomsang.size(); i++){
            //				Domsang ds = ((Domsang) vDomsang.elementAt(i));
            //				ds.update();
            //				if(ds.frame <= 6)
            //					vDomsang.removeElementAt(i);
            //			}
            if (vMovePoints.size() > 0)
            {
                MovePoint mp = null;
                try
                {
                    mp = (MovePoint)vMovePoints.lastElement();
                }
                catch (Exception e)
                {
                }
                if (mp != null && TileMap.tileTypeAt(mp.xEnd, mp.yEnd, TileMap.T_WATERFLOW) && mp.yEnd % TileMap.size > 8)
                    cact = A_WATERRUN;
            }
            if (dx > 0)
                dirX = 1;

            if (dx < 0)
                dirX = -1;
        }
        else if (dy != 0)
        {
            if (dy < 0)
                cact = A_JUMP;
            if (dy > 0)
            {
                cact = A_FALL;
            }

            if (dx < 0)
                dirX = -1;
            if (dx > 0)
                dirX = 1;
        }
        int x = 0, y = 0;
        x = cx + dx;
        y = cy + dy;
        vMovePoints.addElement(new MovePoint(x, y, cact, dirX));

        //		statusMe = A_NOTHING;

    }


	public static void getcharInjure(int cID, int dx, int dy, int HP) {
		Char charchar = ((Char) GameScr.vCharInMap.elementAt(cID));
		if (charchar.vMovePoints.size() == 0)
			return;
		MovePoint move = ((MovePoint) charchar.vMovePoints.lastElement());
		int x = move.xEnd + dx;
		int y = move.yEnd + dy;
		((Char) GameScr.vCharInMap.elementAt(cID)).cHP -= HP;
		if (((Char) GameScr.vCharInMap.elementAt(cID)).cHP < 0) {
			((Char) GameScr.vCharInMap.elementAt(cID)).cHP = 0;
		}
		((Char) GameScr.vCharInMap.elementAt(cID)).statusMe = A_NOTHING;
		// ((Char) GameScr.vCharInMap.elementAt(cID)).cShowHP = ((Char)
		// GameScr.vCharInMap.elementAt(cID)).cHP;
		((Char) GameScr.vCharInMap.elementAt(cID)).HPShow = ((Char) GameScr.vCharInMap.elementAt(cID)).cHP - HP;

		((Char) GameScr.vCharInMap.elementAt(cID)).vMovePoints.addElement(new MovePoint(x, y, A_INJURE,
				((Char) GameScr.vCharInMap.elementAt(cID)).cdir));

	}

	public void searchFocus() {
        //		if(mobFocus != null){
        //					}
        //		System.out.println("CharFocus ---> "+charFocus);

        //		if (isManualFocus) {
        //			if (charFocus != null && (charFocus.statusMe == A_HIDE || charFocus.isInvisible))
        //				charFocus = null;
        //		}
        //		if (GameCanvas.gameTick % 2 == 0)
        //			return;
        //		if (isMeCanAttackOtherPlayer(charFocus))
        //			return;// NOT AUTO CHANGE FOCUS WHEN PK
        int deltaH = 0;
        //		if (nClass.classId == 0 || nClass.classId == 1 || nClass.classId == 3 || nClass.classId == 5)
        //			deltaH = 40;
        int[] d = new int[] { -1, -1, -1, -1 };
        int minx = Char.myChar().cx - Char.myChar().getdxSkill();
        int maxx = Char.myChar().cx + Char.myChar().getdxSkill();
        int miny = GameScr.cmy - 15;
        int maxy = GameScr.cmy + GameCanvas.h - GameScr.cmdBarH + 100 - 15;
        if (isManualFocus)
        {
            if ((mobFocus != null /*&& mobFocus.status != Mob.MA_DEADFLY && mobFocus.status != Mob.MA_INHELL*/ && minx <= mobFocus.x
                    && mobFocus.x <= maxx && miny <= mobFocus.y && mobFocus.y <= maxy)
                    )
                return;
            else
                isManualFocus = false;
        }
        if (itemFocus == null)
        {
            for (int i = 0; i < GameScr.vItemMap.size(); i++)
            {
                ItemMap itemMap = (ItemMap)GameScr.vItemMap.elementAt(i);
                int dxx = Math.abs(Char.myChar().cx - itemMap.x);
                int dyy = Math.abs(Char.myChar().cy - itemMap.y);
                int dd = dxx > dyy ? dxx : dyy;
                if (dxx <= 50 && dyy <= 80)
                {
                    if (itemFocus == null || dd < d[3])
                    {
                        //						if (isAPickYen || isAPickYHM || isAPickYHMS || isANoPick) {
                        //							if ((isAPickYen && itemMap.template.type == Item.TYPE_MONEY)
                        //									|| (isAPickYHM && (itemMap.template.type == Item.TYPE_MONEY || itemMap.template.type == Item.TYPE_HP || itemMap.template.type == Item.TYPE_MP))
                        //									|| (isAPickYHMS && (itemMap.template.type == Item.TYPE_MONEY || itemMap.template.type == Item.TYPE_HP
                        //											|| itemMap.template.type == Item.TYPE_MP || itemMap.template.type == Item.TYPE_CRYSTAL))) {
                        itemFocus = itemMap;
                        d[3] = dd;
                        //							}
                        //						} else {
                        //							itemFocus = itemMap;
                        //							d[3] = dd;
                        //						}
                    }
                }
            }
        }
        else if (minx > itemFocus.x || itemFocus.x > maxx || miny > itemFocus.y || itemFocus.y > maxy)
        {
            itemFocus = null;
            for (int i = 0; i < GameScr.vItemMap.size(); i++)
            {
                ItemMap itemMap = (ItemMap)GameScr.vItemMap.elementAt(i);
                int dxx = Math.abs(Char.myChar().cx - itemMap.x);
                int dyy = Math.abs(Char.myChar().cy - itemMap.y);
                int dd = dxx > dyy ? dxx : dyy;
                if (minx <= itemMap.x && itemMap.x <= maxx && miny <= itemMap.y && itemMap.y <= maxy)
                {
                    if (itemFocus == null || dd < d[3])
                    {
                        //						if (isAPickYen || isAPickYHM || isAPickYHMS || isANoPick) {
                        //							if ((isAPickYen && itemMap.template.type == Item.TYPE_MONEY)
                        //									|| (isAPickYHM && (itemMap.template.type == Item.TYPE_MONEY || itemMap.template.type == Item.TYPE_HP || itemMap.template.type == Item.TYPE_MP))
                        //									|| (isAPickYHMS && (itemMap.template.type == Item.TYPE_MONEY || itemMap.template.type == Item.TYPE_HP
                        //											|| itemMap.template.type == Item.TYPE_MP || itemMap.template.type == Item.TYPE_CRYSTAL))) {
                        itemFocus = itemMap;
                        d[3] = dd;
                        //							}
                        //						} else {
                        //							itemFocus = itemMap;
                        //							d[3] = dd;
                        //						}
                    }
                }
            }
        }
        else
        {
            clearFocus(3);
            return;
        }
        //		if (TileMap.typeMap == TileMap.MAP_CHIENTRUONG || TileMap.mapID == 111) {
        //			// ------mob auto focus
        //			minx = Char.myChar().cx - Char.myChar().getdxSkill();
        //			maxx = Char.myChar().cx + Char.myChar().getdxSkill();
        //			miny = Char.myChar().cy - Char.myChar().getdySkill() - deltaH;
        //			maxy = Char.myChar().cy + Char.myChar().getdySkill();
        //			if (maxy > Char.myChar().cy + 30)
        //				maxy = Char.myChar().cy + 30;
        //
        //			if (mobFocus == null) {
        //				for (int i = 0; i < GameScr.vMob.size(); i++) {
        //					Mob mob = (Mob) GameScr.vMob.elementAt(i);
        //					int dxx = Math.abs(Char.myChar().cx - mob.x);
        //					int dyy = Math.abs(Char.myChar().cy - mob.y);
        //					int dd = dxx > dyy ? dxx : dyy;
        //					if ((mob.templateId == 97 && Char.myChar().cTypePk == Char.PK_PHE1)
        //							|| (mob.templateId == 96 && Char.myChar().cTypePk == Char.PK_PHE2)
        //							|| (mob.templateId == 98 && Char.myChar().cTypePk == Char.PK_PHE1)
        //							|| (mob.templateId == 167 && Char.myChar().cTypePk == Char.PK_PHE1)
        //							|| (mob.templateId == 99 && Char.myChar().cTypePk == Char.PK_PHE2)
        //							|| (mob.templateId == 166 && Char.myChar().cTypePk == Char.PK_PHE2))
        //						continue;
        //					if (minx <= mob.x && mob.x <= maxx && miny <= mob.y && mob.y <= maxy && mob.status != Mob.MA_INHELL
        //							&& mob.status != Mob.MA_DEADFLY) {
        //						if (mobFocus == null || dd < d[0]) {
        //							mobFocus = mob;
        //							d[0] = dd;
        //						}
        //					}
        //				}
        //			} else if (mobFocus.status == Mob.MA_DEADFLY || mobFocus.status == Mob.MA_INHELL || minx > mobFocus.x || mobFocus.x > maxx
        //					|| miny > mobFocus.y || mobFocus.y > maxy) {
        //				mobFocus = null;
        //				for (int i = 0; i < GameScr.vMob.size(); i++) {
        //					Mob mob = (Mob) GameScr.vMob.elementAt(i);
        //					int dxx = Math.abs(Char.myChar().cx - mob.x);
        //					int dyy = Math.abs(Char.myChar().cy - mob.y);
        //					int dd = dxx > dyy ? dxx : dyy;
        //					if ((mob.templateId == 97 && Char.myChar().cTypePk == Char.PK_PHE1)
        //							|| (mob.templateId == 96 && Char.myChar().cTypePk == Char.PK_PHE2)
        //							|| (mob.templateId == 98 && Char.myChar().cTypePk == Char.PK_PHE1)
        //							|| (mob.templateId == 167 && Char.myChar().cTypePk == Char.PK_PHE1)
        //							|| (mob.templateId == 99 && Char.myChar().cTypePk == Char.PK_PHE2)
        //							|| (mob.templateId == 166 && Char.myChar().cTypePk == Char.PK_PHE2))
        //						continue;
        //					if (minx <= mob.x && mob.x <= maxx && miny <= mob.y && mob.y <= maxy && mob.status != Mob.MA_INHELL
        //							&& mob.status != Mob.MA_DEADFLY) {
        //						if (mobFocus == null || dd < d[0]) {
        //							mobFocus = mob;
        //							d[0] = dd;
        //						}
        //					}
        //				}
        //			} else {
        //				clearFocus(0);
        //				return;
        //			}
        //
        //			minx = Char.myChar().cx - 80;
        //			maxx = Char.myChar().cx + 80;
        //			miny = Char.myChar().cy - 30;
        //			maxy = Char.myChar().cy + 30;
        //			if (npcFocus != null && npcFocus.template.npcTemplateId == 13) {
        //				minx = Char.myChar().cx - 20;
        //				maxx = Char.myChar().cx + 20;
        //				miny = Char.myChar().cy - 10;
        //				maxy = Char.myChar().cy + 10;
        //			}
        //			if (npcFocus == null) {
        //				for (int i = 0; i < GameScr.vNpc.size(); i++) {
        //					Npc npc = (Npc) GameScr.vNpc.elementAt(i);
        //					if (npc.statusMe == A_HIDE)
        //						continue;
        //					int dxx = Math.abs(Char.myChar().cx - npc.cx);
        //					int dyy = Math.abs(Char.myChar().cy - npc.cy);
        //					int dd = dxx > dyy ? dxx : dyy;
        //					minx = Char.myChar().cx - 80;
        //					maxx = Char.myChar().cx + 80;
        //					miny = Char.myChar().cy - 30;
        //					maxy = Char.myChar().cy + 30;
        //					if (npc.template.npcTemplateId == 13) {
        //						minx = Char.myChar().cx - 20;
        //						maxx = Char.myChar().cx + 20;
        //						miny = Char.myChar().cy - 10;
        //						maxy = Char.myChar().cy + 10;
        //					}
        //					if (minx <= npc.cx && npc.cx <= maxx && miny <= npc.cy && npc.cy <= maxy) {
        //						if (npcFocus == null || dd < d[1]) {
        //							npcFocus = npc;
        //							d[1] = dd;
        //						}
        //					}
        //				}
        //			} else if (minx > npcFocus.cx || npcFocus.cx > maxx || miny > npcFocus.cy || npcFocus.cy > maxy) {
        //				deFocusNPC();
        //				for (int i = 0; i < GameScr.vNpc.size(); i++) {
        //					Npc npc = (Npc) GameScr.vNpc.elementAt(i);
        //					if (npc.statusMe == A_HIDE)
        //						continue;
        //					int dxx = Math.abs(Char.myChar().cx - npc.cx);
        //					int dyy = Math.abs(Char.myChar().cy - npc.cy);
        //					int dd = dxx > dyy ? dxx : dyy;
        //					minx = Char.myChar().cx - 80;
        //					maxx = Char.myChar().cx + 80;
        //					miny = Char.myChar().cy - 30;
        //					maxy = Char.myChar().cy + 30;
        //					if (npc.template.npcTemplateId == 13) {
        //						minx = Char.myChar().cx - 20;
        //						maxx = Char.myChar().cx + 20;
        //						miny = Char.myChar().cy - 10;
        //						maxy = Char.myChar().cy + 10;
        //					}
        //					if (minx <= npc.cx && npc.cx <= maxx && miny <= npc.cy && npc.cy <= maxy) {
        //						if (npcFocus == null || dd < d[1]) {
        //							npcFocus = npc;
        //							d[1] = dd;
        //						}
        //					}
        //				}
        //			} else {
        //				clearFocus(1);
        //				return;
        //			}
        //			minx = Char.myChar().cx - 40;
        //			maxx = Char.myChar().cx + 40;
        //			miny = Char.myChar().cy - 30;
        //			maxy = Char.myChar().cy + 30;
        //			if (charFocus == null) {
        //				for (int i = 0; i < GameScr.vCharInMap.size(); i++) {
        //					Char c = (Char) GameScr.vCharInMap.elementAt(i);
        //					if(c.isNhanban())
        //						continue;
        //					if (TileMap.mapID != 111) {
        //						if (c.statusMe == A_HIDE || c.isInvisible || c.cTypePk == myChar.cTypePk)
        //							continue;
        //						if (wdx != 0 || wdy != 0 || c.statusMe == A_DEAD || c.statusMe == A_DEADFLY)
        //							continue;
        //					} else {
        //
        //						if (c.statusMe == A_HIDE || c.isInvisible)
        //							continue;
        //						if (wdx != 0 || wdy != 0)
        //							continue;
        //						if (myChar.nClass.classId == 6) {
        //							if (myChar.cTypePk == c.cTypePk) {
        //								if (c.statusMe != A_DEAD || c.statusMe != A_DEADFLY)
        //									continue;
        //							} else if (c.statusMe == A_DEAD || c.statusMe == A_DEADFLY) {
        //								continue;
        //							}
        //						} else {
        //							if (myChar.cTypePk == c.cTypePk || c.statusMe == A_DEAD || c.statusMe == A_DEADFLY)
        //								continue;
        //						}
        //					}
        //					int dxx = Math.abs(Char.myChar().cx - c.cx);
        //					int dyy = Math.abs(Char.myChar().cy - c.cy);
        //					int dd = dxx > dyy ? dxx : dyy;
        //					if (minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy) {
        //						if (charFocus == null || dd < d[2]) {
        //							charFocus = c;
        //							d[2] = dd;
        //						}
        //					}
        //				}
        //			} else if (minx > charFocus.cx || charFocus.cx > maxx || miny > charFocus.cy || charFocus.cy > maxy || charFocus.statusMe == A_HIDE
        //					|| charFocus.isInvisible || charFocus.statusMe == A_DEAD || charFocus.statusMe == A_DEADFLY) {
        //				charFocus = null;
        //				for (int i = 0; i < GameScr.vCharInMap.size(); i++) {
        //					Char c = (Char) GameScr.vCharInMap.elementAt(i);
        //					if(c.isNhanban())continue;
        //					if (TileMap.mapID != 111) {
        //						if (c.statusMe == A_HIDE || c.isInvisible || c.cTypePk == myChar.cTypePk)
        //							continue;
        //						if (wdx != 0 || wdy != 0 || c.statusMe == A_DEAD || c.statusMe == A_DEADFLY)
        //							continue;
        //					} else {
        //						if (c.statusMe == A_HIDE || c.isInvisible)
        //							continue;
        //						if (wdx != 0 || wdy != 0)
        //							continue;
        //						if (myChar.nClass.classId == 6) {
        //							if (myChar.cTypePk == c.cTypePk) {
        //								if (c.statusMe != A_DEAD || c.statusMe != A_DEADFLY)
        //									continue;
        //							} else if (c.statusMe == A_DEAD || c.statusMe == A_DEADFLY) {
        //								continue;
        //							}
        //						} else {
        //							if (myChar.cTypePk == c.cTypePk || c.statusMe == A_DEAD || c.statusMe == A_DEADFLY)
        //								continue;
        //						}
        //					}
        //					int dxx = Math.abs(Char.myChar().cx - c.cx);
        //					int dyy = Math.abs(Char.myChar().cy - c.cy);
        //					int dd = dxx > dyy ? dxx : dyy;
        //					if (minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy) {
        //						if (charFocus == null || dd < d[2]) {
        //							charFocus = c;
        //							d[2] = dd;
        //						}
        //					}
        //				}
        //			} else {
        //				clearFocus(2);
        //				return;
        //			}
        //			int index = -1;
        //			for (int i = 0; i < d.length; i++) {
        //
        //				if (index == -1) {
        //					if (d[i] != -1) {
        //						index = i;
        //					}
        //				} else if (d[i] < d[index] && d[i] != -1) {
        //					index = i;
        //				}
        //			}
        //			clearFocus(index);
        //		} else { // ---MAP_NORMAL
        // ------mob auto focus
        minx = Char.myChar().cx - Char.myChar().getdxSkill();
        maxx = Char.myChar().cx + Char.myChar().getdxSkill();
        miny = Char.myChar().cy - Char.myChar().getdySkill() - 80;
        maxy = Char.myChar().cy + Char.myChar().getdySkill() + 60;

        if (maxy > Char.myChar().cy + 30)
            maxy = Char.myChar().cy + 30;

        if (mobFocus == null)
        {
            for (int i = 0; i < GameScr.vMob.size(); i++)
            {
                Mob mob = (Mob)GameScr.vMob.elementAt(i);
                int dxx = Math.abs(Char.myChar().cx - mob.x);
                int dyy = Math.abs(Char.myChar().cy - mob.y);
                int dd = dxx > dyy ? dxx : dyy;
                //					if ((mob.templateId == 97 && Char.myChar().cTypePk == Char.PK_PHE1)
                //							|| (mob.templateId == 96 && Char.myChar().cTypePk == Char.PK_PHE2)
                //							|| (mob.templateId == 98 && Char.myChar().cTypePk == Char.PK_PHE1)
                //							|| (mob.templateId == 167 && Char.myChar().cTypePk == Char.PK_PHE1)
                //							|| (mob.templateId == 99 && Char.myChar().cTypePk == Char.PK_PHE2)
                //							|| (mob.templateId == 166 && Char.myChar().cTypePk == Char.PK_PHE2))
                //						continue;
                if (minx <= mob.x && mob.x <= maxx && miny <= mob.y && mob.y <= maxy /*&& mob.status != Mob.MA_INHELL
							&& mob.status != Mob.MA_DEADFLY*/)
                {
                    if (mobFocus == null || dd < d[0])
                    {
                        mobFocus = mob;
                        if (!mob.isGetInfo)
                        {
                            mob.isGetInfo = true;
                            Service.gI().requetsInfoMod((short)mob.mobId);
                        }
                        d[0] = dd;
                    }
                }
            }
        }
        else if (/*mobFocus.status == Mob.MA_DEADFLY || mobFocus.status == Mob.MA_INHELL ||*/ minx > mobFocus.x || mobFocus.x > maxx
              || miny > mobFocus.y || mobFocus.y > maxy)
        {
            mobFocus = null;
            for (int i = 0; i < GameScr.vMob.size(); i++)
            {
                Mob mob = (Mob)GameScr.vMob.elementAt(i);
                int dxx = Math.abs(Char.myChar().cx - mob.x);
                int dyy = Math.abs(Char.myChar().cy - mob.y);
                int dd = dxx > dyy ? dxx : dyy;
                //					if ((mob.templateId == 97 && Char.myChar().cTypePk == Char.PK_PHE1)
                //							|| (mob.templateId == 96 && Char.myChar().cTypePk == Char.PK_PHE2)
                //							|| (mob.templateId == 98 && Char.myChar().cTypePk == Char.PK_PHE1)
                //							|| (mob.templateId == 167 && Char.myChar().cTypePk == Char.PK_PHE1)
                //							|| (mob.templateId == 99 && Char.myChar().cTypePk == Char.PK_PHE2)
                //							|| (mob.templateId == 166 && Char.myChar().cTypePk == Char.PK_PHE2))
                //						continue;
                if (minx <= mob.x && mob.x <= maxx && miny <= mob.y && mob.y <= maxy /*&& mob.status != Mob.MA_INHELL
							&& mob.status != Mob.MA_DEADFLY*/)
                {
                    if (mobFocus == null || dd < d[0])
                    {
                        if (!mob.isGetInfo)
                        {
                            mob.isGetInfo = true;
                            Service.gI().requetsInfoMod((short)mob.mobId);
                        }
                        mobFocus = mob;
                        d[0] = dd;
                    }
                }
            }
        }
        else
        {
            clearFocus(0);
            return;
        }
        minx = Char.myChar().cx - 50;
        maxx = Char.myChar().cx + 50;
        miny = Char.myChar().cy - 30;
        maxy = Char.myChar().cy + 30;
        if (npcFocus != null && npcFocus.template.npcTemplateId == 13)
        {
            minx = Char.myChar().cx - 20;
            maxx = Char.myChar().cx + 20;
            miny = Char.myChar().cy - 10;
            maxy = Char.myChar().cy + 10;
        }
        if (npcFocus == null)
        {
            for (int i = 0; i < GameScr.vNpc.size(); i++)
            {
                Npc npc = (Npc)GameScr.vNpc.elementAt(i);
                if (npc.statusMe == A_HIDE)
                    continue;
                //					if (TileMap.typeMap == TileMap.MAP_DAUTRUONG)
                //						continue;
                int dxx = Math.abs(Char.myChar().cx - npc.cx);
                int dyy = Math.abs(Char.myChar().cy - npc.cy);
                int dd = dxx > dyy ? dxx : dyy;
                minx = Char.myChar().cx - 80;
                maxx = Char.myChar().cx + 80;
                miny = Char.myChar().cy - 30;
                maxy = Char.myChar().cy + 30;
                if (npc.template != null && npc.template.npcTemplateId == 13)
                {
                    minx = Char.myChar().cx - 20;
                    maxx = Char.myChar().cx + 20;
                    miny = Char.myChar().cy - 10;
                    maxy = Char.myChar().cy + 10;
                }
                if (minx <= npc.cx && npc.cx <= maxx && miny <= npc.cy && npc.cy <= maxy)
                {
                    if (npcFocus == null || dd < d[1])
                    {
                        npcFocus = npc;
                        d[1] = dd;
                    }
                }
            }
        }
        else if (minx > npcFocus.cx || npcFocus.cx > maxx || miny > npcFocus.cy || npcFocus.cy > maxy)
        {
            deFocusNPC();
            for (int i = 0; i < GameScr.vNpc.size(); i++)
            {
                Npc npc = (Npc)GameScr.vNpc.elementAt(i);
                if (npc.statusMe == A_HIDE)
                    continue;
                //					if (TileMap.typeMap == TileMap.MAP_DAUTRUONG)
                //						continue;
                int dxx = Math.abs(Char.myChar().cx - npc.cx);
                int dyy = Math.abs(Char.myChar().cy - npc.cy);
                int dd = dxx > dyy ? dxx : dyy;
                minx = Char.myChar().cx - 80;
                maxx = Char.myChar().cx + 80;
                miny = Char.myChar().cy - 30;
                maxy = Char.myChar().cy + 30;
                if (npc.template.npcTemplateId == 13)
                {
                    minx = Char.myChar().cx - 20;
                    maxx = Char.myChar().cx + 20;
                    miny = Char.myChar().cy - 10;
                    maxy = Char.myChar().cy + 10;
                }
                if (minx <= npc.cx && npc.cx <= maxx && miny <= npc.cy && npc.cy <= maxy)
                {
                    if (npcFocus == null || dd < d[1])
                    {
                        npcFocus = npc;
                        d[1] = dd;
                    }
                }
            }
        }
        else
        {
            clearFocus(1);
            return;
        }
        //
        if (charFocus == null)
        {
            for (int i = 0; i < GameScr.vCharInMap.size(); i++)
            {
                Char c = (Char)GameScr.vCharInMap.elementAt(i);
                if (c.isNhanban() || c.Equals(Char.myChar())) continue;
                // if (TileMap.typeMap != TileMap.MAP_DAUTRUONG) {
                if (c.statusMe == A_HIDE || c.isInvisible)
                    continue;
                if (c.charID >= -1)
                    continue;
                // } else if (c.cTypePk == 0)
                // continue;
                if (wdx != 0 || wdy != 0 || c.statusMe == A_DEAD || c.statusMe == A_DEADFLY)
                    continue;
                int dxx = Math.abs(Char.myChar().cx - c.cx);
                int dyy = Math.abs(Char.myChar().cy - c.cy);
                int dd = dxx > dyy ? dxx : dyy;
                if (minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy && !c.Equals(Char.myChar()))
                {
                    if (charFocus == null)
                        Service.gI().requestPlayerInfo((short)c.charID);
                    if (charFocus == null || dd < d[2])
                    {
                        charFocus = c;
                        d[2] = dd;
                    }
                }
            }
        }
        else if (minx > charFocus.cx || charFocus.cx > maxx || miny > charFocus.cy || charFocus.cy > maxy || charFocus.statusMe == A_HIDE
              || charFocus.isInvisible)
        {
            charFocus = null;
            for (int i = 0; i < GameScr.vCharInMap.size(); i++)
            {
                Char c = (Char)GameScr.vCharInMap.elementAt(i);
                if (c.isNhanban() || c.Equals(Char.myChar())) continue;
                // if (TileMap.typeMap != TileMap.MAP_DAUTRUONG) {
                if (c.statusMe == A_HIDE || c.isInvisible)
                    continue;
                if (c.charID >= 0)
                    continue;
                // } else if (c.cTypePk == 0)
                // continue;
                if (wdx != 0 || wdy != 0 || c.statusMe == A_DEAD || c.statusMe == A_DEADFLY)
                    continue;
                int dxx = Math.abs(Char.myChar().cx - c.cx);
                int dyy = Math.abs(Char.myChar().cy - c.cy);
                int dd = dxx > dyy ? dxx : dyy;
                if (minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy)
                {

                    if (charFocus == null)
                        Service.gI().requestPlayerInfo((short)c.charID);
                    if (charFocus == null || dd < d[2])
                    {
                        charFocus = c;
                        d[2] = dd;
                    }
                }
            }
        }
        else
        {
            clearFocus(2);
            return;
        }
        int index = -1;

        for (int i = 0; i < d.Length; i++)
        {
            if (index == -1)
            {
                if (d[i] != -1)
                {
                    index = i;
                }
            }
            else if (d[i] < d[index] && d[i] != -1)
            {
                index = i;
            }

        }
        clearFocus(index);
        //		}
    }
    public void clearAllFocus()
    {

        charFocus = null;
        itemFocus = null;
        mobFocus = null;
        npcFocus = null;
    }

	public void clearFocus(int index) {
		if (index == 0) {
			deFocusNPC();
			charFocus = null;
			itemFocus = null;
		} else if (index == 1) {
			mobFocus = null;
			charFocus = null;
			itemFocus = null;
		} else if (index == 2) {
			mobFocus = null;
			deFocusNPC();
			itemFocus = null;
		} else if (index == 3) {
			mobFocus = null;
			deFocusNPC();
			charFocus = null;
		}
//		if(charFocus==null&&mobFocus==null&&npcFocus==null){
//			GuiMain.indexshow = GameScr.isShowFocus==true?0:GuiMain.indexshow;
//			GameScr.isShowFocus = false;
//		}
	}

	public static bool isCharInScreen(Char c) {
		int minx = GameScr.cmx;
		int maxx = GameScr.cmx + GameCanvas.w;
		int miny = GameScr.cmy + 10;
		int maxy = GameScr.cmy + GameScr.gH;
		if (c.statusMe != A_HIDE && !c.isInvisible && minx <= c.cx
				&& c.cx <= maxx && miny <= c.cy && c.cy <= maxy)
			return true;
		return false;
	}

	public static bool isManualFocus = false;

	public void findNextFocusByKey() {
	//		if (Char.myChar().skillPaint != null || Char.myChar().arr != null)
//			return;
		int deltaH = 0;
		if (nClass.classId == 0 || nClass.classId == 1 || nClass.classId == 3 || nClass.classId == 5)
			deltaH = 40;

		focus.removeAllElements();
		int indexNext = 0;
		int minx = GameScr.cmx + 10;
		int maxx = GameScr.cmx + GameCanvas.w - 10;
		int miny = GameScr.cmy + 10;
		int maxy = GameScr.cmy + GameScr.gH;

//		if ( TileMap.mapID == 111) {} else {
			for (int i = 0; i < GameScr.vItemMap.size(); i++) {
				ItemMap itemMap = (ItemMap) GameScr.vItemMap.elementAt(i);
				if (minx <= itemMap.x && itemMap.x <= maxx && miny <= itemMap.y && itemMap.y <= maxy) {
					focus.addElement(itemMap);
					if (itemFocus != null && itemMap.Equals(itemFocus)) {
						indexNext = focus.size();
					}
				}
			}

			for (int i = 0; i < GameScr.vMob.size(); i++) {
				Mob mob = (Mob) GameScr.vMob.elementAt(i);
//				if ((mob.templateId == 97 && Char.myChar().cTypePk == Char.PK_PHE1)
//						|| (mob.templateId == 96 && Char.myChar().cTypePk == Char.PK_PHE2)
//						|| (mob.templateId == 98 && Char.myChar().cTypePk == Char.PK_PHE1)
//						|| (mob.templateId == 99 && Char.myChar().cTypePk == Char.PK_PHE2))
//					continue;
				if (/*mob.status != Mob.MA_DEADFLY && mob.status != Mob.MA_INHELL &&*/ minx <= mob.x && mob.x <= maxx && miny <= mob.y && mob.y <= maxy) {
					focus.addElement(mob);
					if (mobFocus != null && mob.Equals(mobFocus)) {
						indexNext = focus.size();
					}
				}
			}
			for (int i = 0; i < GameScr.vNpc.size(); i++) {
				Npc npc = (Npc) GameScr.vNpc.elementAt(i);
				if (npc.statusMe != A_HIDE && minx <= npc.cx && npc.cx <= maxx && miny <= npc.cy && npc.cy <= maxy) {
					focus.addElement(npc);
					if (npcFocus != null && npc.Equals(npcFocus)) {
						indexNext = focus.size();
					}
				}
			}

			for (int i = 0; i < GameScr.vCharInMap.size(); i++) {
				Char c = (Char) GameScr.vCharInMap.elementAt(i);
				if(c.isNhanban())continue;
				if (c.statusMe != A_HIDE && !c.isInvisible && minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy) {
					focus.addElement(c);
					if (charFocus != null && c.Equals(charFocus)&&!c.Equals(Char.myChar())) {
						indexNext = focus.size();
					}
				}
			}

			if (focus.size() > 0) {
				if (indexNext >= focus.size())
					indexNext = 0;
				if (focus.elementAt(indexNext) is Mob) {
					mobFocus = (Mob) focus.elementAt(indexNext);
					deFocusNPC();
					charFocus = null;
					itemFocus = null;
					isManualFocus = true;
				}

			} 
//		}
	}
	public void deFocusNPC() {
		if (me && npcFocus != null) {
			npcFocus.chatPopup = null;
			npcFocus = null;
		}
	}

	public void updateCharInBridge() {
		if (GameCanvas.lowGraphic)
			return;
		if (TileMap.tileTypeAt(cx, cy + 1, TileMap.T_BRIDGE)) // Bridge
		{
			TileMap.setTileTypeAtPixel(cx, cy + 1, TileMap.T_DOWN1PIXEL);
			TileMap.setTileTypeAtPixel(cx, cy - 2, TileMap.T_DOWN1PIXEL);
		}
		if (TileMap.tileTypeAt(cx - TileMap.size, cy + 1, TileMap.T_DOWN1PIXEL)) // Bridge
		{
			TileMap.killTileTypeAt(cx - TileMap.size, cy + 1, TileMap.T_DOWN1PIXEL);
			TileMap.killTileTypeAt(cx - TileMap.size, cy - 2, TileMap.T_DOWN1PIXEL);
		}
		if (TileMap.tileTypeAt(cx + TileMap.size, cy + 1, TileMap.T_DOWN1PIXEL)) // Bridge
		{
			TileMap.killTileTypeAt(cx + TileMap.size, cy + 1, TileMap.T_DOWN1PIXEL);
			TileMap.killTileTypeAt(cx + TileMap.size, cy - 2, TileMap.T_DOWN1PIXEL);
		}
	}

	// public static int[] resultMyskillId() {
	// int arr[] = new int[useSkill.size()];
	// for (int i = 0; i < useSkill.size(); i++) {
	// UseSkill us = (UseSkill) Char.useSkill.elementAt(i);
	// arr[i] = us.skillId;
	// }
	// return arr;
	// }

	// public static void isLearn() {
	// for (int i = 0; i < mySkill.size(); i++) {
	// SkillInfo skill = (SkillInfo) mySkill.elementAt(i);
	// for (int j = 0; j < resultMyskillId().Length; j++) {
	// if (skill.id == resultMyskillId()[j]) {
	// skill.islearn = true;
	// }
	// }
	// }
	// }

	// public static bool useResult(UseSkill data) {// true la da o trong
	// combo
	// for (int i = 0; i < skillselect.Length; i++) {
	// if (data.skillId == skillselect[i]) {
	// return true;
	// }
	// }
	// return false;
	// }
    public static void sort(int[] data)
    {
		int i, j, k;
		int n = 5;
		for (i = 0; i < n - 1; i++) {
			for (j = i + 1; j < n; j++) {
				if (data[i] < data[j]) {
					k = data[j];
					data[j] = data[i];
					data[i] = k;

				}
			}

		}
	}

	public static bool setInsc(int cmX, int cmWx, int x, int cmy, int cmyH, int y) {
		if (x > cmWx || x < cmX || y > cmyH || y < cmy)
			return false;
		return true;

	}

	public void itemMonToBag(Message msg) {
		try {
			readParam(msg);
			Char.myChar().eff5BuffHp = msg.reader().readShort();
			Char.myChar().eff5BuffMp = msg.reader().readShort();
			int index = msg.reader().readUnsignedByte();
			
			Item itemMon = arrItemMounts[index];
			itemMon.typeUI = Item.UI_BAG;
			arrItemMounts[index] = null;
			itemMon.indexUI = msg.reader().readUnsignedByte();
			arrItemBag[itemMon.indexUI] = itemMon;
			if (index == 4) {
				isWolf = isMoto = isMotoBehind = false;
			}
			GameScr.isPaintItemInfo = false;
//			GameScr.gI().setLCR();
		} catch (Exception e) {
		//	e.printStackTrace();
		}
	}

	public void itemBodyToBag(Message msg) {
//		try {
//			readParam(msg);
//			Char.myChar().eff5BuffHp = msg.reader().readShort();
//			Char.myChar().eff5BuffMp = msg.reader().readShort();
//			Item itemBody = arrItemBody[msg.reader().readUnsignedByte()];
//			itemBody.typeUI = Item.UI_BAG;
//			if (itemBody.indexUI == Item.TYPE_VUKHI)
//				setDefaultWeapon();
//			else if (itemBody.indexUI == Item.TYPE_AO)
//				setDefaultBody();
//			else if (itemBody.indexUI == Item.TYPE_QUAN)
//				setDefaultLeg();
//			arrItemBody[itemBody.indexUI] = null;
//			itemBody.indexUI = msg.reader().readUnsignedByte();
//			Char.myChar().head = msg.reader().readShort();
//			arrItemBag[itemBody.indexUI] = itemBody;
//			GameScr.gI().left = GameScr.gI().center = null;
////			GameScr.gI().setLCR();
//		} catch (Exception e) {
//		//	e.printStackTrace();
//			System.out.println("Char.itemBodyToBag()");
//		}
	}

	public void itemBagToBox(Message msg) {
//		try {
//			int bagIndex = msg.reader().readUnsignedByte();
//			int boxIndex = msg.reader().readUnsignedByte();
//			Item itBag = arrItemBag[bagIndex];
//			if (itBag != null) {
//				if (itBag.template.type == Item.TYPE_HP)
//					GameScr.hpPotion -= itBag.quantity;
//				if (itBag.template.type == Item.TYPE_MP)
//					GameScr.mpPotion -= itBag.quantity;
//
//				arrItemBag[bagIndex] = null;
//				if (arrItemBox[boxIndex] == null) {
//					itBag.indexUI = boxIndex;
//					itBag.typeUI = Item.UI_BOX;
//					arrItemBox[boxIndex] = itBag;
//				} else {
//					arrItemBox[boxIndex].quantity += itBag.quantity;
//				}
//			}
//			GameScr.gI().left = GameScr.gI().center = null;
////			GameScr.gI().updateKeyBuyItemUI();
//		} catch (Exception e) {
//		//	e.printStackTrace();
//			System.out.println("Char.itemBagToBox()");
//		}
	}

	public void itemBoxToBag(Message msg) {
//		try {
//			int boxIndex = msg.reader().readUnsignedByte();
//			int bagIndex = msg.reader().readUnsignedByte();
//			Item itBox = arrItemBox[boxIndex];
//			if (itBox != null) {
//				if (itBox.template.type == Item.TYPE_HP)
//					GameScr.hpPotion += itBox.quantity;
//				if (itBox.template.type == Item.TYPE_MP)
//					GameScr.mpPotion += itBox.quantity;
//
//				arrItemBox[boxIndex] = null;
//				if (arrItemBag[bagIndex] == null) {
//					itBox.indexUI = bagIndex;
//					itBox.typeUI = Item.UI_BAG;
//					arrItemBag[bagIndex] = itBox;
//				} else {
//					arrItemBag[bagIndex].quantity += itBox.quantity;
//				}
//			}
//			GameScr.gI().left = GameScr.gI().center = null;
////			GameScr.gI().updateKeyBuyItemUI();
//		} catch (Exception e) {
//		//	e.printStackTrace();
//			System.out.println("Char.itemBoxToBag()");
//		}
	}

	public void crystalCollect(Message msg, bool isCoin) {
		try {
			for (int i = 0; i < GameScr.arrItemUpPeal.Length; i++) {
				GameScr.arrItemUpPeal[i] = null;
			}
			int typeResult = msg.reader().readByte();
			Item item = new Item();
			item.typeUI = Item.UI_BAG;
			item.indexUI = msg.reader().readByte();
			item.template = ItemTemplates.get(msg.reader().readShort());
			item.isLock = msg.reader().readbool();
			item.isExpires = msg.reader().readbool();
			item.quantity = 1;

			if (isCoin)
				Char.myChar().xu = msg.reader().readInt();
			else {
				Char.myChar().yen = msg.reader().readInt();
				try {
					Char.myChar().xu = msg.reader().readInt();
				} catch (Exception e) {
				}
			}
			GameScr.arrItemUpPeal[0] = item;
//			GameScr.effUpok = GameScr.efs[53];
			GameScr.indexEff = 0;
			GameScr.gI().left = GameScr.gI().center = null;
//			GameScr.gI().updateCommandForUI();
			GameCanvas.endDlg();
//			if (typeResult == 1)
//				InfoMe.addInfo(mResources.UPGRADE_SUCCESS + " " + item.template.name);
//			else
//				InfoMe.addInfo(mResources.UPGRADE + " " + ItemTemplates.get((short) (item.template.id + 1)).name + " " + mResources.UPGRADE_FAIL
//						+ " " + item.template.name, (byte) 25, mFont.tahoma_7_red);
		} catch (Exception e) {
            //e.printStackTrace();
            //System.out.println("Char.itemBagToBox()");
		}
	}

	public void kickOption(Item item, int maxKick) {
		int kick = 0;
		if (item != null && item.options != null) {
			for (int i = 0; i < item.options.size(); i++) {
				ItemOption itemOption = (ItemOption) item.options.elementAt(i);
				itemOption.active = 0;
				if (itemOption.optionTemplate.type == 2) {
					if (kick < maxKick) {
						itemOption.active = 1;
						kick++;
					}
				} else if (itemOption.optionTemplate.type == 3 && item.upgrade >= 4)
					itemOption.active = 1;
				else if (itemOption.optionTemplate.type == 4 && item.upgrade >= 8)
					itemOption.active = 1;
				else if (itemOption.optionTemplate.type == 5 && item.upgrade >= 12)
					itemOption.active = 1;
				else if (itemOption.optionTemplate.type == 6 && item.upgrade >= 14)
					itemOption.active = 1;
				else if (itemOption.optionTemplate.type == 7 && item.upgrade >= 16)
					itemOption.active = 1;
// if(itemOption.active == 1 && item.upgrade >= 10){
// System.out.println("item.options.get(i).optionTemplate.type: " + itemOption.optionTemplate.type);
// }
			}
		}
	}

	public void updateKickOption() {
//		// non, quan, nhan
//		// ao, giay ngoc boi
//		// gang tay, lien phu
//		int kich0 = 2, kich1 = 2, kich2 = 2;
//		if (arrItemBody[Item.TYPE_NON] == null)
//			kich0--;
//		if (arrItemBody[Item.TYPE_QUAN] == null)
//			kich0--;
//		if (arrItemBody[Item.TYPE_NHAN] == null)
//			kich0--;
//		kickOption(arrItemBody[Item.TYPE_NON], kich0);
//		kickOption(arrItemBody[Item.TYPE_QUAN], kich0);
//		kickOption(arrItemBody[Item.TYPE_NHAN], kich0);
//
//		if (arrItemBody[Item.TYPE_AO] == null)
//			kich1--;
//		if (arrItemBody[Item.TYPE_GIAY] == null)
//			kich1--;
//		if (arrItemBody[Item.TYPE_NGOCBOI] == null)
//			kich1--;
//		kickOption(arrItemBody[Item.TYPE_AO], kich1);
//		kickOption(arrItemBody[Item.TYPE_GIAY], kich1);
//		kickOption(arrItemBody[Item.TYPE_NGOCBOI], kich1);
//
//		if (arrItemBody[Item.TYPE_GANGTAY] == null)
//			kich2--;
//		if (arrItemBody[Item.TYPE_LIEN] == null)
//			kich2--;
//		if (arrItemBody[Item.TYPE_PHU] == null)
//			kich2--;
//
//		if (arrItemBody[Item.TYPE_VUKHI] != null) {
//			if (arrItemBody[Item.TYPE_VUKHI].sys == getSys()) {
//				if (arrItemBody[Item.TYPE_VUKHI].options != null) {
//					for (int i = 0; i < arrItemBody[Item.TYPE_VUKHI].options.size(); i++) {
//						ItemOption itemOption = (ItemOption) arrItemBody[Item.TYPE_VUKHI].options.elementAt(i);
//						if (itemOption.optionTemplate.type == 2) {
//							itemOption.active = 1;
//						}
//					}
//				}
//			} else {
//				if (arrItemBody[Item.TYPE_VUKHI].options != null) {
//					for (int i = 0; i < arrItemBody[Item.TYPE_VUKHI].options.size(); i++) {
//						ItemOption itemOption = (ItemOption) arrItemBody[Item.TYPE_VUKHI].options.elementAt(i);
//						if (itemOption.optionTemplate.type == 2) {
//							itemOption.active = 0;
//						}
//					}
//				}
//			}
//		}
//
//		kickOption(arrItemBody[Item.TYPE_GANGTAY], kich2);
//		kickOption(arrItemBody[Item.TYPE_LIEN], kich2);
//		kickOption(arrItemBody[Item.TYPE_PHU], kich2);
	}
	public int xSd, ySd;
	int nInjure;
	public void doInjure(int HPShow, int MPShow, bool isCrit, bool isMob) {

		this.isCrit = isCrit;
		this.isMob = isMob;
//		Res.out("CHP= " + cHP + " dame -= " + HPShow + " HP FULL= " + cHPFull);
		cHP -= HPShow;
		cMP -= MPShow;
//		GameScr.gI().isInjureHp = true;
//		GameScr.gI().twHp = 0;
//		GameScr.gI().isInjureMp = true;
//		GameScr.gI().twMp = 0;
		// if (!me)
		// cHP = cHPNew;
		if (cHP < 0)
			cHP = 0;
		if (cMP < 0)
			cMP = 0;
		// if (cHP < 1 && statusMe != A_DEAD && statusMe != A_DEADFLY)
		// cHP = 1;
//		if (isMob || (!isMob && cTypePk != PK_LUYENTAP) && damMP != -100) {
//			if (HPShow <= 0) {
//				if (me)
//					GameScr.startFlyText(mResources.miss, cx, cy - ch, 0, -2,
//							mFont.MISS_ME);
//				else
//					GameScr.startFlyText(mResources.miss, cx, cy - ch, 0, -2,
//							mFont.MISS);
//			} else
//				GameScr.startFlyText("-" + HPShow, cx, cy - ch, 0, -2,
//						!isCrit ? mFont.RED : mFont.FATAL);
//		}

		if (HPShow > 0)
			isInjure = 6;

		ServerEffect.addServerEffect(80, this, 1);

		if (isDie) {
			isDie = false;
			Char.isLockKey = false;
			startDie((short) xSd, (short) ySd);
		}
	}
	public void doInjure(int HPShow, int MPShow, bool isBoss, int idBoss) {
        //		cHP -= HPShow;
        //		cMP -= MPShow;
        //		if (!me)
        //			cHP = cHpNew;
        //		if (cHP < 0)
        //			cHP = 0;
        //		if (cMP < 0)
        //			cMP = 0;
        //		if (cHP < 1 && statusMe != A_DEAD && statusMe != A_DEADFLY) {
        //			cHP = 1;
        //		}
        //		if (HPShow <= 0) {
        //			if (me)
        //				GameScr.startFlyText("", cx, cy - ch, 0, -2, mFont.MISS_ME);
        //			else
        //				GameScr.startFlyText("", cx, cy - ch, 0, -2, mFont.MISS);
        //		} else
        //			GameScr.startFlyText("-" + HPShow, cx, cy - ch, 0, -2, mFont.RED);

        //		if (HPShow > 0)
        isInjure = 4;
        cf = 21;
        //		if (isBoss) {
        //			if (idBoss == 114)
        //				ServerEffect.addServerEffect(32, cx, cy - chh, 1);
        //			else if (idBoss == 115)
        //				ServerEffect.addServerEffect(85, cx, cy, 1);
        //			else if (idBoss == 139) {
        //				GameScr.shaking = 1;
        //				GameScr.count = 0;
        //				ServerEffect.addServerEffect(91, this, 2);
        //			}else if (idBoss == 144) {
        //				ServerEffect.addServerEffect(91, this, 1);
        //			}
        //		} else
        //			callEff(49);
    }

	public void doInjure() {
		isInjure = 4;
		callEff(49);
	}

	public short wdx, wdy;
	public bool isDirtyPostion;
	public Skill lastNormalSkill;
	public bool currentFireByShortcut;

	public void startDie(short toX, short toY) {
		if (me) {
			isLockKey = true;
			for (int i = 0; i < GameScr.vCharInMap.size(); i++) {
				Char c = (Char) GameScr.vCharInMap.elementAt(i);
				c.killCharId = -9999;
			}
		}
		statusMe = A_DEADFLY;
		cp2 = toX;
		cp3 = toY;
		cp1 = 0;
		cHP = 0;
		testCharId = -9999;
		killCharId = -9999;
	}

	public void waitToDie(short toX, short toY) {
		wdx = toX;
		wdy = toY;
	}

	public void changeStatusStand() {
		timeBocdau = 0;
		statusMe = A_STAND;
		timeSummon = mSystem.currentTimeMillis();
	}

	public void liveFromDead() {
		cHP = cMaxHP;
		cMP = cMaxMP;
		changeStatusStand();
		cp1 = cp2 = cp3 = 0;
//		ServerEffect.addServerEffect(20, this, 2);
		GameScr.gI().center = null;
	}

	public bool doUsePotion(int type) {
		if (arrItemBag == null) {
			return false;
		}
		for (int i = 0; i < arrItemBag.Length; i++) {
			if (arrItemBag[i] == null) {
				continue;
			}
			if (arrItemBag[i].template.type == type) {
				if (arrItemBag[i].template.level > Char.myCharr.clevel)
					continue;
//				Service.gI().useItem(i);
				return true;
			}
		}
		return false;
	}

	public bool isLang() {
		if (TileMap.mapID == 1 || TileMap.mapID == 27 || TileMap.mapID == 72 || TileMap.mapID == 10 || TileMap.mapID == 17 || TileMap.mapID == 22
				|| TileMap.mapID == 32 || TileMap.mapID == 38 || TileMap.mapID == 43 || TileMap.mapID == 48)
			return true;
		return false;
	}

//	public bool isMeCanAttackOtherPlayer(Char cAtt) {
//		if(cAtt!=null&&cAtt.isNhanban())
//			return false;
//		if (cAtt == null || Char.myChar().myskill == null || Char.myChar().myskill.template.type == Skill.SKILL_CLICK_USE_BUFF
//				|| Char.myChar().myskill.template.type == Skill.SKILL_CLICK_NPC
//				|| (Char.myChar().myskill.template.type == Skill.SKILL_CLICK_LIVE && cAtt.statusMe != A_DEAD && cAtt.statusMe != A_DEADFLY))
//			return false;
//		return ((((Char.myChar().cTypePk == Char.PK_PHE1 && cAtt.cTypePk == Char.PK_PHE2) || (Char.myChar().cTypePk == Char.PK_PHE2 && cAtt.cTypePk == Char.PK_PHE1))
//				&& !Char.myChar().isTeam(cAtt) && !isLang())
//				|| (cAtt.cTypePk == Char.PK_DOSAT && !Char.myChar().isTeam(cAtt) && !isLang())
//				|| (Char.myChar().cTypePk == Char.PK_DOSAT && !Char.myChar().isTeam(cAtt) && !isLang())
//				|| (Char.myChar().cTypePk == Char.PK_NHOM && cAtt.cTypePk == Char.PK_NHOM && !Char.myChar().isTeam(cAtt) && !isLang())
//				|| (Char.myChar().testCharId >= 0 && Char.myChar().testCharId == cAtt.charID)
//				|| (Char.myChar().killCharId >= 0 && Char.myChar().killCharId == cAtt.charID && !isLang()) || (cAtt.killCharId >= 0
//				&& cAtt.killCharId == Char.myChar().charID && !isLang()))
//				&& cAtt.statusMe != Char.A_DEAD && cAtt.statusMe != Char.A_DEADFLY;
//	}

//	public bool isTeam(Char c) {
//		for (int i = 0; i < GameScr.vParty.size(); i++) {
//			Party party = (Party) GameScr.vParty.elementAt(i);
//			if (c.charID == party.charId)
//				return true;
//		}
//		return false;
//	}

	public void clearTask() {
//		Char.myChar().callEffTask(21);
//		Char.myChar().taskMaint = null;
//		for (int i = 0; i < Char.myChar().arrItemBag.Length; i++) {
//			if (Char.myChar().arrItemBag[i] != null
//					&& (Char.myChar().arrItemBag[i].template.type == Item.TYPE_TASK
//							|| Char.myChar().arrItemBag[i].template.type == Item.TYPE_TASK_SAVE || Char.myChar().arrItemBag[i].template.type == Item.TYPE_TASK_WAIT))
//				Char.myChar().arrItemBag[i] = null;
//		}
//		Npc.clearEffTask();
	}

	public static int getCT() {
		if (pointChienTruong >= 4000)
			return 4;
		if (pointChienTruong >= 1500)
			return 3;
		if (pointChienTruong >= 600)
			return 2;
		if (pointChienTruong >= 200)
			return 1;
		return 0;
	}
	private int EffdefautX, EffdefautY; // y x cố định của eff trên mắt chó 
	//private int tickEffWolf = 0;
	private int Effx, Effy; // x y vẽ lại effect chỗ đi qa
	private void updateEffectWolf(){
		
		tickEffWolf++;
		if(tickEffWolf > 5)
			tickEffWolf = 0;
		
		
	}
	private int tickEffyesWolfmove = 0;
	//private int Xefftemp, Yefftemp;
	private void updateEffwolfMove(){
		if(arrItemMounts[4].template.id == 443){
			if(arrItemMounts[4].sys >= 3){
				
				Effx = EffdefautX;
				Effy = EffdefautY;
				
				if(idWolfW[1] == 1737 ){
					if(cdir != 1)
						EffdefautY -= 5;
					else 
						EffdefautY -= 5;
				}
			
				//Domsang domsang2 = new Domsang( , (Effy - EffdefautY)/ 2);
				//Domsang domsang1 = new Domsang((EffdefautX + 6 /2), (EffdefautY));
//		System.out.println("X1 ---> "+Effx+" Y1 ----> "+Effy);
//		System.out.println("X2 ---> "+(EffdefautX - Effx)/2+" Y2 ----> "+(EffdefautY - Effy)/2);
//		if(cdir == 1)
//			Effx = (Effx + EffdefautX)/2;
//		else
//			Effx = (Effx )
				//for(int i = 0; i <= vDomsang.size(); i++)
			
//		tickEffyesWolfmove++;
//		if(tickEffyesWolfmove == 5)
//			tickEffyesWolfmove = 0;
			}
			}
			
	}
//	private void paintEffEyesWolf(mGraphics g){
//		g.drawRegion(GameScr.imgMatcho, 0, tickEffyesWolfmove * 3 ,3 , 3, 0, Effx, Effy, 0);
//	}
	private int EffdefautX1, EffdefautY1;
	
	int tickviBody = 0;
	int dxmove1, dxmove2 ;

	public void updateEffPhanthan(){
		//System.out.println("-------------> update");
		//dxmove1 = dxmove2 = cx;
		//System.out.println("dxmove1 ---> "+dxmove1 +" "+dxmove2);
		tickviBody++;
			if(tickviBody > 1)
				tickviBody = 0;

		dxmove1--;
//			if(dxmove1 == cxtemp)
//				dxmove1--;
//			else if(dxmove1 < cxtemp - 10)
//				dxmove1++;
		dxmove2++;
//			if(dxmove2 == cxtemp)
//				dxmove2++;
//			else if(dxmove2 > cxtemp + 10)
//				dxmove2--;
		
		
//		dxmove1 = dxmove2 = cx;
	}
	
	public void paintChar(mGraphics g, int x, int y){
        try
        {
            int a = GameCanvas.isTouchControlLargeScreen ? -25 : 16;
            Part ph = GameScr.parts[GameScr.currentCharViewInfo.head], pl = GameScr.parts[GameScr.currentCharViewInfo.leg], pb = GameScr.parts[GameScr.currentCharViewInfo.body], pw = GameScr.parts[GameScr.currentCharViewInfo.wp];
            SmallImage.drawSmallImage(g, pl.pi[Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].id, x + Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][1] + pl.pi[Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].dx, y + a - Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][2] + pl.pi[Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].dy, 0, 0);
            SmallImage.drawSmallImage(g, pb.pi[Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].id, x + Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][1] + pb.pi[Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].dx, y + a - Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][2] + pb.pi[Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].dy, 0, 0);
            SmallImage.drawSmallImage(g, ph.pi[Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][0]].id, x + Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][1] + ph.pi[Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][0]].dx, y + a - Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][2] + ph.pi[Char.CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][0]].dy, 0, 0);
        }
        catch (Exception e)
        {
            // TODO: handle exception
          //  e.printStackTrace();
        }
    }
	
	
	public bool isHuman(){
		return isHumann;
	}
	public bool isNhanban(){
		return isNhanbann;
	}

    public void paintCharWithSkill(mGraphics g, int index, int xchar, int ychar)
    {
        int line = 0;
        try
        {
            SkillInfoPaint[] skillInfoPaintt = skillInfoPaint();
            if (skillInfoPaintt == null)
            {
                return;
            }
            cf = skillInfoPaintt[index].status;
            if (skillInfoPaintt[index].effS0Id != 0)
            {
                eff0 = GameScr.efs[skillInfoPaintt[index].effS0Id - 1];
                i0 = dx0 = dy0 = 0;
            }
            line = 1;
            if (skillInfoPaintt[index].effS1Id != 0)
            {
                eff1 = GameScr.efs[skillInfoPaintt[index].effS1Id - 1];
                i1 = dx1 = dy1 = 0;
            }
            if (skillInfoPaintt[index].effS2Id != 0)
            {
                eff2 = GameScr.efs[skillInfoPaintt[index].effS2Id - 1];
                i2 = dx2 = dy2 = 0;
            }
            SkillInfoPaint[] sp = skillInfoPaintt;

            if (sp != null && sp[index] != null && sp[index].arrowId != 0)
            {
                arr = new Arrow(Char.myChar(), GameScr.arrs[sp[index].arrowId - 1]);
                arr.life = 10;
                arr.ax = xchar + sp[index].adx;
                arr.ay = ychar + sp[index].ady;
            }
            paintCharWithoutSkill(g, xchar, ychar);
            line = 1;
            if (cdir == 1)
            {
                if (eff0 != null)
                {
                    if (dx0 == 0)
                        dx0 = skillInfoPaintt[index].e0dx;
                    if (dy0 == 0)
                        dy0 = skillInfoPaintt[index].e0dy;
                    SmallImage.drawSmallImage(g, eff0.arrEfInfo[i0].idImg, xchar + dx0 + eff0.arrEfInfo[i0].dx, ychar + dy0 + eff0.arrEfInfo[i0].dy, 0,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    i0++;
                    if (i0 >= eff0.arrEfInfo.Length)
                    {
                        eff0 = null;
                        i0 = dx0 = dy0 = 0;
                    }
                }
                if (eff1 != null)
                {
                    if (dx1 == 0)
                        dx1 = skillInfoPaintt[index].e1dx;
                    if (dy1 == 0)
                        dy1 = skillInfoPaintt[index].e1dy;
                    SmallImage.drawSmallImage(g, eff1.arrEfInfo[i1].idImg, xchar + dx1 + eff1.arrEfInfo[i1].dx, ychar + dy1 + eff1.arrEfInfo[i1].dy, 0,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    i1++;
                    if (i1 >= eff1.arrEfInfo.Length)
                    {
                        eff1 = null;
                        i1 = dx1 = dy1 = 0;
                    }
                }
                if (eff2 != null)
                {
                    if (dx2 == 0)
                        dx2 = skillInfoPaintt[index].e2dx;
                    if (dy2 == 0)
                        dy2 = skillInfoPaintt[index].e2dy;
                    SmallImage.drawSmallImage(g, eff2.arrEfInfo[i2].idImg, xchar + dx2 + eff2.arrEfInfo[i2].dx, ychar + dy2 + eff2.arrEfInfo[i2].dy, 0,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    i2++;
                    if (eff2.arrEfInfo != null)
                    {
                        if (i2 >= eff2.arrEfInfo.Length)
                        {
                            eff2 = null;
                            i2 = dx2 = dy2 = 0;
                        }
                    }
                }
            }
            else
            {
                if (eff0 != null)
                {
                    if (dx0 == 0)
                        dx0 = skillInfoPaintt[index].e0dx;
                    if (dy0 == 0)
                        dy0 = skillInfoPaintt[index].e0dy;
                    SmallImage.drawSmallImage(g, eff0.arrEfInfo[i0].idImg, xchar - dx0 - eff0.arrEfInfo[i0].dx, ychar + dy0 + eff0.arrEfInfo[i0].dy, 2,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    i0++;
                    if (i0 >= eff0.arrEfInfo.Length)
                    {
                        eff0 = null;
                        i0 = 0;
                        dx0 = 0;
                        dy0 = 0;
                    }
                }
                if (eff1 != null)
                {
                    if (dx1 == 0)
                        dx1 = skillInfoPaintt[index].e1dx;
                    if (dy1 == 0)
                        dy1 = skillInfoPaintt[index].e1dy;
                    SmallImage.drawSmallImage(g, eff1.arrEfInfo[i1].idImg, xchar - dx1 - eff1.arrEfInfo[i1].dx, ychar + dy1 + eff1.arrEfInfo[i1].dy, 2,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    i1++;
                    if (i1 >= eff1.arrEfInfo.Length)
                    {
                        eff1 = null;
                        i1 = 0;
                        dx1 = 0;
                        dy1 = 0;
                    }
                }
                if (eff2 != null)
                {
                    if (dx2 == 0)
                        dx2 = skillInfoPaintt[index].e2dx;
                    if (dy2 == 0)
                        dy2 = skillInfoPaintt[index].e2dy;
                    SmallImage.drawSmallImage(g, eff2.arrEfInfo[i2].idImg, xchar - dx2 - eff2.arrEfInfo[i2].dx, ychar + dy2 + eff2.arrEfInfo[i2].dy, 2,
                            mGraphics.VCENTER | mGraphics.HCENTER);
                    i2++;
                    if (eff2.arrEfInfo != null)
                    {
                        if (i2 >= eff2.arrEfInfo.Length)
                        {
                            eff2 = null;
                            i2 = 0;
                            dx2 = 0;
                            dy2 = 0;
                        }
                    }
                }
            }
            line = 2;
        }
        catch (Exception e)
        {
            // e.printStackTrace();
        }
    }

}