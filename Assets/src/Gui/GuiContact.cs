
/*
 * Giao dien giao tiep giua cac nhan vat voi nhau
 */
using System;
public class GuiContact : FatherChat{
	
	int x,y;
	int xM;//toan do giua cua ma hinh
	string [] listName={"Trò chuyện","Kết bạn","Thông tin","Mời nhóm","Giao dịch","Thách đấu"};
	
	public const int cTroChuyen = 2;
	public const int cKetban = 3;
	public const int cthongtin = 4;
	public const int cMoinhom = 5;
	public const int cGiaodich = 6;
    public const int cThachdau = 7;
	
	public GuiContact(int x,int y) {
		
		popw=156;
		poph=203;
		this.xM=x;
		this.x=x-popw/2;
		this.y=y;
		scrMain.selectedItem = -1;
	}
	public void perform(int idAction, Object p) {
		
		
	}
	
	void PaintList(mGraphics g, string[] list,int x,int y,int width)
	{
		int nCol=width/Image.getWidth(loadImageInterface.imgLineTrade);
//		System.out.println("");
		int yy=0;
		for(int i=0;i<list.Length;i++)
		{
			yy+=30;
			mFont.tahoma_7b_red.drawString(g, list[i], x+width/2, y+yy-17, 2);
			for(int j=0;j<nCol;j++)
			{
				g.drawImage(loadImageInterface.imgLineTrade,x+(Image.getWidth(loadImageInterface.imgLineTrade)-1)*j+13,y+yy, 0,true);
			}
		}
	}
	public void Update() {
        // TODO Auto-generated method stub
        if (scrMain != null)
        {
            scrMain.updatecm();
        }
        if (scrMain.selectedItem > -1)
        {
            switch (scrMain.selectedItem + 2)
            {
                case cTroChuyen:
                    //Cout.println(getClass(), " cTroChuyen ");
                    scrMain.selectedItem = -1;
                    bool isAdded = false;
                    for (int i = 0; i < ChatPrivate.vOtherchar.size(); i++)
                    {
                        OtherChar other = (OtherChar)ChatPrivate.vOtherchar.elementAt(i);
                        if (other.name.Equals(Char.myChar().charFocus.cName) || other.id == Char.myChar().charFocus.charID)
                        {
                            if (other.id != Char.myChar().charFocus.charID)
                                other.id = (short)Char.myChar().charFocus.charID;
                            isAdded = true;
                            break;
                        }

                    }
                    if (!isAdded && Char.myChar().charFocus != null)
                        ChatPrivate.AddNewChater((short)Char.myChar().charFocus.charID, Char.myChar().charFocus.cName);
                    TabChat.gI().switchToMe();
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
                case cKetban:
                    //Cout.println(getClass(), " cKetban ");
                    scrMain.selectedItem = -1;
                    if (Char.myChar().charFocus != null)
                        Service.gI().requestAddfriend((sbyte)0, (short)Char.myChar().charFocus.charID);
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
                case cthongtin:
                    //Cout.println(getClass(), " cthongtin ");
                    scrMain.selectedItem = -1;
                    GameCanvas.StartDglThongBao("Chức năng đang phát triển");
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
                case cMoinhom:
                    //Cout.println(getClass(), " cMoinhom ");
                    scrMain.selectedItem = -1;
                    if (Char.myChar().charFocus != null)
                        Service.gI().inviteParty((short)Char.myChar().charFocus.charID, (sbyte)0);
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
                case cGiaodich:
                   // Cout.println(getClass(), " cGiaodich ");
                    scrMain.selectedItem = -1;
                    Char.myChar().partnerTrade = null;
                    if (Char.myChar().charFocus != null)
                        Service.gI().inviteTrade((sbyte)0, (short)Char.myChar().charFocus.charID);
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
                case cThachdau:
                    //Cout.println(getClass(), " cThachdau ");
                    if (Char.myChar().charFocus != null)
                        Service.gI().ThachDau((sbyte)0, (short)Char.myChar().charFocus.charID);
                    scrMain.selectedItem = -1;
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    //				Service.gI().
                    break;
            }
        }
    }
	public void UpdateKey() {
		// TODO Auto-generated method stub
		if (GameCanvas.isTouch) {
			   ScrollResult r = scrMain.updateKey();
			   if (r.isDowning || r.isFinish) {
				   
			    indexRow = r.selected;
			   }
		}
	}
	public void Paintt(mGraphics g)
	{
        GameScr.resetTranslate(g);
        Paint.paintFrameNaruto(x, y, popw, poph, g);

        g.drawImage(loadImageInterface.charPic, xM, y + poph / 3 - 22, mGraphics.VCENTER | mGraphics.HCENTER);
        try
        {
            Part ph = GameScr.parts[Char.myChar().charFocus.head];
            SmallImage.drawSmallImage(g, ph.pi[Char.CharInfo[0][0][0]].id,
                    xM - 1, y + poph / 3 - 24, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
        //		g.drawImage(Char.myChar().charFocus.,xM-3,y+poph/3-24,g.VCENTER|g.HCENTER);

        g.drawImage(loadImageInterface.imgName, xM, y + poph / 3 + 8, mGraphics.VCENTER | mGraphics.HCENTER);

        if (Char.myChar().charFocus != null && Char.myChar().charFocus.cName != null)
            mFont.tahoma_7b_white.drawString(g, Char.myChar().charFocus.cName, xM, y + poph / 3 + 3, mGraphics.VCENTER);


        Paint.PaintBoxName("Giao Tiếp", xM - 40, y, 80, g);
        if (scrMain != null)
        {
            scrMain.setStyle(listName.Length, 30, x, y + 80, popw, 100, true, 0);
            scrMain.setClip(g);
        }

        if (indexRow >= 0)
        {
            g.setColor(0x964210);
            g.fillRect(x + 10, y + 87 + 30 * indexRow, popw - 20, 20);
        }
        PaintList(g, listName, x, y + 80, popw);
        GameCanvas.resetTrans(g);
    }
	public void SetPosClose(Command cmdClose) {
		// TODO Auto-generated method stub
		cmdClose.setPos(x+popw-Image.getWidth(loadImageInterface.closeTab), y, loadImageInterface.closeTab, loadImageInterface.closeTab);
		
	}

}
