

public class PaintInfoGameScreen {
	
	int xPaintInfo = 0, xPaintInfoChar = 0, xmaxInfo = 0, xmaxInfoChar = 0,
	speedInfo = 2, speedInfoChar = 2, timepaintServer,
	timePaintInfoChar;
// info Quest Char
	public int timeInfoCharCline, maxtimeChar, ydInfoChar, hImgInfo = 22;
	bool vipInfoChar;
	// Pointer
	public static int xPointMove = 55, yPointMove = GameCanvas.h - 55, xMess,// 80
		yMess, numMess, yBeginInfo, wInfoServer;
	public static int wArrowMove = 30, wPointArrow = 38;
	public static FrameImage fraClose, fraFocusIngame, fraBack, fraMenu,
		fraButton, fraStatusArea, fraCloseMenu, fralevelup, fraButton2,
		fraContact, fraEvent;
	public static FrameImage[] mfraIconQuick;
	int[] mRotateMove = { 2, 0, 3, 1 };
	int[] mKeyMove = { 4, 6, 2, 8 };
	public static int[] mKeySkill = { 1, 3, 7, 9 }, mValueHotKey = { 1, 3, 5,
		7, 9 };
	public static string[] mValueChar = { "R", "T", "Y", "U", "I" };
	int[] mKeyOther = { 100, 101, 102, 103 };
	public static int[][] mPosKill = mSystem.new_M_Int(4, 2),
		mPosMove = mSystem.new_M_Int(4, 2), mPosOther = mSystem.new_M_Int(
				5, 2), mSizeImgOther = mSystem.new_M_Int(5, 2);
	public static int timePointer = 0, keyPoint, timeChange, timeNameMap,
		wNameMap, vyNameMap, yNameMap;
	public static string namemap = "";
	public static int xPointKill = GameCanvas.w - 35,
		yPointKill = GameCanvas.h - 50, wSkill = 24, wMainSkill = 50;// 70
	static int gocBegin = 285, lSkill = 50;
	static int xPaintSkill = GameCanvas.hw - 60, yPaintSkill = GameCanvas.h
		- GameCanvas.hCommand - 14;
	//
	public string strInfoServer = null, strInfoCharCline, strInfoCharServer;
	int indexTab = 0;
	public static bool isLevelPoint = false;
	public static bool isShowInfoAuto = true;
	public static bool isPaintInfoFocus = false;
	public static int hShowInGame = 0;
	public static bool isShowInGame = true;
	public static long timeDoNotClick = 0;
	//
	// object Focus
	public static int xFocus;
	static int yFocus;
	// Party
	static int xParty, yParty;
	
	public static void loadPaintInfo() {
	wInfoServer = (GameCanvas.w * 2) / 3;
	yBeginInfo = GameCanvas.h / 4 - 30;
	xFocus = GameCanvas.w - 52;
	yFocus = 0;
	xParty = 2;
	yParty = 60;
	xMess = 70;
	yMess = 45;
	if (GameCanvas.isTouch) {
		loadImagePointer();
		xFocus = GameCanvas.hw;
	}
	wShowEvent = 130;
	
	}
	
	public static void loadImagePointer() {
	wSkill = 32;
	int goc = gocBegin;
//	for (int i = 0; i < mPosKill.Length; i++) {
//		mPosKill[i][0] = xPointKill
//				+ (Res.cos(Res.fixangle(goc)) * lSkill) / 1000;
//		mPosKill[i][1] = yPointKill
//				+ (Res.sin(Res.fixangle(goc)) * lSkill) / 1000;
//		goc -= 45;
//	}
//	xPaintSkill = GameCanvas.w - wSkill * 6;
//	yPaintSkill = GameCanvas.h - 24;
//	// Menu
//	mPosOther[0][0] = 8;
//	mPosOther[0][1] = 43;
//	// Chat
//	mPosOther[1][0] = 8;
//	mPosOther[1][1] = 73;
//	// change Tab
//	// mPosOther[2][0] = GameCanvas.w - 30;
//	// mPosOther[2][1] = GameCanvas.h - 31;
//	mPosOther[2][0] = GameCanvas.w - 27;
//	mPosOther[2][1] = GameCanvas.h - 145;
//	// change Focus
//	mPosOther[3][0] = GameCanvas.w - 27;
//	mPosOther[3][1] = GameCanvas.h - 175;
//	// tam giac
//	mPosOther[4][0] = GameCanvas.hw - 20;
//	mPosOther[4][1] = GameCanvas.h - 16;
//	setPosTouch();
//	// Change Touch
//	// mPosOther[4][0] = 78;
//	// mPosOther[4][1] = 41;
//	// mess
//	xMess = 45;
//	yMess = 45;
//	imgOther = new mImage[5];
//	for (int i = 0; i < imgOther.Length; i++) {
//		imgOther[i] = mImage.createImage("/point/other_" + i + ".png");
//		mSizeImgOther[i][0] = mImage.getImageWidth(imgOther[i].image);
//		mSizeImgOther[i][1] = mImage.getImageHeight(imgOther[i].image) / 2;
//	}
//	// Move
//	imgMove = new mImage[3];
//	for (int i = 0; i < imgMove.Length; i++) {
//		if (i != 1)
//			imgMove[i] = mImage.createImage("/point/move_" + i + ".png");
//	}
//	for (int i = 0; i < mPosMove.Length; i++) {
//		mPosMove[i][0] = xPointMove
//				+ (i < 2 ? -wArrowMove + wArrowMove * 2 * (i % 2) : 0);
//		mPosMove[i][1] = yPointMove
//				+ (i > 1 ? (-wArrowMove + wArrowMove * 2 * (i % 2)) : 0);
//	}
//	// Fire
//	imgFire = new mImage[2];
//	for (int i = 0; i < imgFire.Length; i++) {
//		imgFire[i] = mImage.createImage("/point/fire_" + i + ".png");
//	}
	//
	// imgdelay = mImage.createImage("/point/delay.png");
	fraClose = new FrameImage(Image.createImage("/point/close.png"), 14,
			14);
	fraCloseMenu = new FrameImage(Image.createImage("/point/closemenu.png"), 21, 21);
	fraBack = new FrameImage(Image.createImage("/point/buttonback.png"),
			53, 22);
	fraMenu = new FrameImage(Image.createImage("/point/buttonmenu.png"),
			32, 32);
	fraButton = new FrameImage(Image.createImage("/point/button.png"), 80,
			30);
	fraButton2 = new FrameImage(Image.createImage("/point/button2.png"),
			60, 19);
	fraContact = new FrameImage(Image.createImage("/point/contact.png"),
			26, 26);
	mfraIconQuick = new FrameImage[8];
//	for (int i = 0; i < mfraIconQuick.Length; i++) {
//		mfraIconQuick[i] = new FrameImage(mImage
//				.createImage("/point/quick_" + i + ".png"), 30, 30);
//	}
	
	// fraDap = new FrameImage(mImage.createImage("/point/dap.png"), 14,
	// 14);
	// fraView = new FrameImage(mImage.createImage("/point/view.png"), 15,
	// 15);
	}
	
	public void paintKillPlayer(mGraphics g) {
//	if (timeChange == 0) {
//		int y = yPaintSkill + PaintInfoGameScreen.hShowInGame;
//		if (GameScreen.ObjFocus != null
//				&& GameScreen.ObjFocus.typeBoss == MainObject.MON_CAPCHAR) {
//	
//			for (int i = 0; i < 5; i++) {
//				g.drawImage(AvMain.imgHotKey, xPaintSkill + i * wSkill,
//						y - 5, 0);
//				if (!GameCanvas.isTouch && TField.isQwerty) {
//					AvMain.Font3dWhite(g, mValueChar[i] + "", xPaintSkill
//							+ i * wSkill + 11, y, 2);
//				} else {
//					AvMain.Font3dWhite(g, mValueHotKey[i] + "", xPaintSkill
//							+ i * wSkill + 11, y, 2);
//				}
//			}
//			return;
//		}
//		y = 5 - PaintInfoGameScreen.hShowInGame;
//		for (int i = 0; i < Player.mhotkey[0].Length; i++) {
//			g.drawImage(AvMain.imgHotKey, xPaintSkill + i * wSkill,
//					yPaintSkill - y, 0);
//			HotKey t = Player.mhotkey[Player.levelTab][i];
//			DelaySkill delay = null;
//			if (t.type == HotKey.SKILL) {
//				Skill s = (Skill) MainListKill.getSkillFormId(t.id);
//				// if ((isLevelPoint || Player.isAutoFire)
//				// && t.id == Player.skillDefault) {
//				// g.setColor(0xffff0000);
//				// g.fillRoundRect(xPaintSkill + i * wSkill - 2,
//				// yPaintSkill - y - 2, 26, 26, 6, 6);
//				// }
//				if (s != null) {
//					s.paint(g, xPaintSkill + i * wSkill + 11, yPaintSkill
//							- y + 11, 3);
//				}
//				delay = Player.timeDelaySkill[t.id];
//			} else if (t.type == HotKey.POTION) {
//				if (MainTemplateItem.isload) {
//					MainItem item = MainItem.getItemInventory(
//							MainObject.CAT_POTION, (short) t.id);
//					if (item != null && item.typePotion < 2) {
//						item.paintItem(g, xPaintSkill + i * wSkill + 11,
//								yPaintSkill + 11 - y, MainTabNew.wOneItem,
//								0, 3);
//						delay = Player.timeDelayPotion[item.typePotion];
//					} else {
//						t.setHotKey(0, HotKey.NULL, (byte) 0);
//						MainItem.setAddHotKey(MainItem.MP, false);
//						MainItem.setAddHotKey(MainItem.HP, false);
//					}
//				}
//			}
//			if (t.type != HotKey.NULL) {
//				// if (t.type == delay.typeSkill) {
//				if (delay != null) {
//					if (delay.limit > 0) {
//						if (delay.value > 0) {
//							int hpaint = (delay.value * 20) / delay.limit;
//							if (hpaint < 1)
//								hpaint = 1;
//							g.drawRegion(AvMain.imgDelaySkill, 0, 0, 20,
//									hpaint, 0,
//									xPaintSkill + i * wSkill + 1,
//									yPaintSkill + 1 - y, 0);
//							int second = delay.value / 1000;
//							string strsecond = "";
//							if (second == 0) {
//								strsecond = "0." + (delay.value % 1000)
//										/ 100;
//							} else {
//								strsecond = "" + second;
//							}
//							mFont.tahoma_7b_white.drawString(g, strsecond,
//									xPaintSkill + i * wSkill + 11,
//									yPaintSkill + 5 - y, 2);
//						} else if (delay.value > -150) {
//							g.setColor(0xffeeeecc);
//							g.fillRoundRect(xPaintSkill + i * wSkill + 1,
//									yPaintSkill - y + 1, 20, 20, 4, 4);
//							// g.drawRect(GameCanvas.hw - 60 + i * wSkill,
//							// GameCanvas.h - GameCanvas.hCommand - 14
//							// - y, 21, 21);
//						}
//					}
//				}
//				// }
//			}
//			// }
//			if (!GameCanvas.isTouch) {
//				if (TField.isQwerty) {
//					mFont.tahoma_7b_white.drawString(g, mValueChar[i] + "",
//							xPaintSkill + i * wSkill + 12, yPaintSkill - y
//									- 11, 2);
//				} else {
//					mFont.tahoma_7b_white.drawString(g, mValueHotKey[i]
//							+ "", xPaintSkill + i * wSkill + 12,
//							yPaintSkill - y - 11, 2);
//				}
//			}
//		}
//	} else {
//		paintChangeSkill(g);
//	}
	
	}
	
	public void paintBuffPlayer(mGraphics g) {
//	for (int i = 0; i < GameScreen.player.vecBuff.size(); i++) {
//		MainBuff buff = (MainBuff) GameScreen.player.vecBuff.elementAt(i);
//	
//	}
	}
	
	private void paintChangeSkill(mGraphics g) {
//	for (int i = 0; i < 10; i++) {
//		int y = -5;
//		int valueTab = Player.levelTab;
//		if (i < 5) {
//			y = timeChange * 8;
//		} else {
//			y = 64 - timeChange * 8;
//			if (Player.levelTab == 0)
//				valueTab = 1;
//			else
//				valueTab = 0;
//		}
//		g.drawImage(AvMain.imgHotKey, xPaintSkill + (i % 5) * wSkill - 1, y
//				+ yPaintSkill - 1, 0);
//		HotKey t = Player.mhotkey[valueTab][i % 5];
//		if (t.type == HotKey.SKILL) {
//			Skill s = (Skill) MainListKill.getSkillFormId(t.id);
//			s.paint(g, xPaintSkill + i % 5 * wSkill + 11, y + yPaintSkill
//					+ 11, 3);
//		} else if (t.type == HotKey.POTION) {
//			MainItem item = MainItem.getItemInventory(
//					MainObject.CAT_POTION, (short) t.id);
//			if (item != null)
//				item.paintItem(g, xPaintSkill + i % 5 * wSkill + 11, y
//						+ yPaintSkill + 11, MainTabNew.wOneItem, 0, 3);
//		}
//	}
//	}
//	
//	public void paintInfoPlayer(mGraphics g, int x, int y, bool isborder,
//		mFont fontLv) {
//	if (isborder) {
//		g.drawRegion(AvMain.imgInfo, 0, 0, 16, 42, 0, x + 1, y + 2, 0);
//		g.drawRegion(AvMain.imgInfo, 0, 84, 16, 42, 0, x + 96, y + 2,
//				mGraphics.TOP | mGraphics.RIGHT);
//		for (int i = 0; i < 4; i++) {
//			g.drawRegion(AvMain.imgInfo, 0, 42, 16, 42, 0, x + 17 + 16 * i,
//					y + 2, 0);
//		}
//		x += 8;
//		y += 4;
//	}
//	g.drawImage(AvMain.imghpmp, x + 2, y + 3, 0);
//	g.setColor(0);
//	g.fillRect(x + 19, y + 4, 62, 7);
//	g.fillRect(x + 20, y + 3, 60, 1);
//	g.fillRect(x + 20, y + 11, 60, 1);
//	g.fillRect(x + 19, y + 16, 62, 7);
//	g.fillRect(x + 20, y + 15, 60, 1);
//	g.fillRect(x + 20, y + 23, 60, 1);
//	g.setColor(0xff37020e);
//	g.fillRect(x + 20, y + 4, 60, 7);
//	g.setColor(0xff0a2443);
//	g.fillRect(x + 20, y + 16, 60, 7);
//	int hpPaint = 0, mpPaint = 0;
//	if (GameScreen.player.hp > 0) {
//		hpPaint = (GameScreen.player.hp * 60) / GameScreen.player.maxHp;
//		if (hpPaint <= 0)
//			hpPaint = 1;
//		else if (hpPaint > 60)
//			hpPaint = 60;
//		g.drawRegion(AvMain.imgcolorhpmp, 0, 0, hpPaint, 7, 0, x + 20,
//				y + 4, 0);
//	}
//	if (GameScreen.player.mp > 0) {
//		mpPaint = (GameScreen.player.mp * 60) / GameScreen.player.maxMp;
//		if (mpPaint <= 0)
//			mpPaint = 1;
//		else if (mpPaint > 60)
//			mpPaint = 60;
//		g.drawRegion(AvMain.imgcolorhpmp, 0, 7, mpPaint, 7, 0, x + 20,
//				y + 16, 0);
//	
//	}
//	
//	fontLv.drawString(g, "Lv." + GameScreen.player.Lv + " + "
//			+ GameScreen.player.phantramLv / 10 + ","
//			+ GameScreen.player.phantramLv % 10 + "%", x + 3, y + 24, 0);
//	int kinhnghiem = 0;
//	if (GameScreen.player.phantramLv > 0) {
//		kinhnghiem = ((GameScreen.player.phantramLv / 10 * 77) / 100);
//		g.setColor(0xff359f2e);
//		g.fillRect(x + 3, y + 35, kinhnghiem, 2);
//	}
//	mFont.tahoma_7_white.drawString(g, GameScreen.player.hp + "/"
//			+ GameScreen.player.maxHp, x + 50, y + 2, 2);
//	mFont.tahoma_7_white.drawString(g, GameScreen.player.mp + "/"
//			+ GameScreen.player.maxMp, x + 50, y + 14, 2);
	// if (GameCanvas.currentScreen == GameCanvas.game)
	// paintInfoChar(g);
	}
	
	public void paintInfoChar(mGraphics g) {
//	GameCanvas.resetTrans(g);
//	int ypaint = yBeginInfo;
//	if (strInfoServer != null) {
//		g.setClip(GameCanvas.hw - wInfoServer / 2, ypaint, wInfoServer, 20);
//		for (int i = 0; i < (wInfoServer) / 140 + 1; i++) {
//			if (i == (wInfoServer) / 140) {
//				g.drawRegion(AvMain.imgBackInfo, 0, 0, wInfoServer % 140,
//						20, 0, GameCanvas.hw - wInfoServer / 2 + i * 140,
//						ypaint, 0);
//			} else
//				g.drawImage(AvMain.imgBackInfo, GameCanvas.hw - wInfoServer
//						/ 2 + i * 140, ypaint, 0);
//		}
//		mFont.tahoma_7b_yellow.drawString(g, strInfoServer, GameCanvas.hw
//				+ wInfoServer / 2 - xPaintInfo, ypaint + 4, 0);
//		ypaint += hImgInfo;
//	}
//	if (strInfoCharServer != null) {
//		g.setClip(GameCanvas.hw - wInfoServer / 2, ypaint, wInfoServer, 20);
//		for (int i = 0; i < (wInfoServer) / 140 + 1; i++) {
//			if (i == (wInfoServer) / 140) {
//				g.drawRegion(AvMain.imgBackInfo, 0, 0, wInfoServer % 140,
//						20, 0, GameCanvas.hw - wInfoServer / 2 + i * 140,
//						ypaint, 0);
//			} else
//				g.drawImage(AvMain.imgBackInfo, GameCanvas.hw - wInfoServer
//						/ 2 + i * 140, ypaint, 0);
//		}
//		mFont.tahoma_7b_white.drawString(g, strInfoCharServer,
//				GameCanvas.hw + wInfoServer / 2 - xPaintInfoChar,
//				ypaint + 4, 0);
//		ypaint += hImgInfo;
//	}
//	if (strInfoCharCline != null) {
//		g.setClip(GameCanvas.hw - 70, ypaint, 140, 20);
//		g.drawImage(AvMain.imgBackInfo, GameCanvas.hw - 70, ypaint
//				+ ydInfoChar, 0);
//		mFont.tahoma_7_white.drawString(g, strInfoCharCline, GameCanvas.hw,
//				ypaint + 4 + ydInfoChar, 2);
//		// ypaint += ydInfoChar + hImgInfo;
//	}
//	GameCanvas.resetTrans(g);
	}
	
	public void PaintIconPlayer(mGraphics g) {
//	int x = 102, y = 8 - PaintInfoGameScreen.hShowInGame;
//	if (GameCanvas.isSmallScreen) {
//		x = 90;
//		y = 7 - PaintInfoGameScreen.hShowInGame;
//	}
//	if (Player.diemTiemNang > 0) {
//		fralevelup.drawFrame(GameCanvas.gameTick / 4 % 2, x, y, 0, 3, g);
//	}
//	if (Player.diemKyNang > 0) {
//		fralevelup.drawFrame(2 + GameCanvas.gameTick / 4 % 2, x, y + 14, 0,
//				3, g);
//	}
//	if (Player.isAutoFire > -1) {
//		g.drawImage(imgauto, x + 1, y + 28, 3);
//	}
//	if (Player.typeX2 == 1) {
//		if (GameCanvas.gameTick % 200 < 100)
//			g.drawImage(imgxp, x + 1, y + 42, 3);
//		else {
//			mFont.tahoma_7_green.drawString(g, getTimex2(), x - 7, y + 36,
//					0);
//		}
//	}
	
	}
	
	public string getTimex2() {
//	if (Player.timeX2 > 0) {
//		if ((GameCanvas.timeNow - Player.timeSetX2) / 1000 > 60) {
//			Player.timeSetX2 += 1000 * 60;
//			Player.timeX2--;
//		}
//	}
//	return getStringTime(Player.timeX2);
//	}
//	
//	public static string getStringTime(int time) {
//	if (time >= 60) {
//		return time / 60 + "h" + time % 60 + "'";
//	} else {
//		return time + "'";
//	}
		return null;
	}
	
	// public void paintInfoServer(mGraphics g) {
	// if (strInfoServer != null) {
	// for (int i = 0; i < GameCanvas.w / 64 + 1; i++) {
	// g.drawImage(AvMain.imgcmdbar, i * 64, GameCanvas.h - 16, 0);
	// }
	// mFont.tahoma_7_white.drawString(g, strInfoServer, GameCanvas.w
	// - xPaintInfo, GameCanvas.h - 13, 0);
	// }
	// }
	
	public void updateInfoServer() {
//	if (GameScreen.VecInfoServer.size() > 0) {
//		if (strInfoServer == null) {
//	
//			strInfoServer = (string) GameScreen.VecInfoServer.elementAt(0);
//			GameCanvas.msgchat.addNewChat(T.tinden, T.text2kenhthegioi,
//					strInfoServer, ChatDetail.TYPE_SERVER, false);
//			int t = GameScreen.VecInfoServer.size();
//			if (t < 2) {
//				speedInfo = 2;
//			} else if (t < 5) {
//				speedInfo = 3;
//			} else {
//				speedInfo = 4;
//			}
//			xPaintInfo = 0;
//			xmaxInfo = mFont.tahoma_7b_white.getWidth(strInfoServer)
//					+ wInfoServer;
//			if (xmaxInfo < wInfoServer)
//				xmaxInfo = wInfoServer;
//		} else {
//			if (xPaintInfo >= xmaxInfo) {
//				timepaintServer++;
//				// if (timepaintServer >= 20
//				// || GameScreen.VecInfoServer.size() > 20) {
//				timepaintServer = 0;
//				strInfoServer = null;
//				GameScreen.VecInfoServer.removeElementAt(0);
//				// }
//			}
//			xPaintInfo += speedInfo;
//		}
//	}
	}
	
	public void updateInfoCharServer() {
//	if (GameScreen.VecInfoChar.size() > 0) {
//		if (strInfoCharServer == null) {
//			strInfoCharServer = (string) GameScreen.VecInfoChar
//					.elementAt(0);
//			int t = GameScreen.VecInfoChar.size();
//			if (t < 2) {
//				speedInfoChar = 2;
//			} else if (t < 5) {
//				speedInfoChar = 3;
//			} else {
//				speedInfoChar = 4;
//			}
//			xPaintInfoChar = 0;
//			xmaxInfoChar = mFont.tahoma_7b_white
//					.getWidth(strInfoCharServer)
//					+ wInfoServer;
//			if (xmaxInfoChar < wInfoServer)
//				xmaxInfoChar = wInfoServer;
//		} else {
//			if (xPaintInfoChar >= xmaxInfoChar) {
//				// timePaintInfoChar++;
//				// if (timePaintInfoChar >= 20
//				// || GameScreen.VecInfoChar.size() > 20) {
//				timePaintInfoChar = 0;
//				strInfoCharServer = null;
//				GameScreen.VecInfoChar.removeElementAt(0);
//				// }
//			}
//			xPaintInfoChar += speedInfoChar;
//		}
//	}
	}
	
	public void paintInfoFocus(mGraphics g) {
//	if (!isPaintInfoFocus && isLevelPoint)
//		return;
//	if (GameScreen.ObjFocus != null) {
//		int xp = 0;
//		int yp = yFocus - PaintInfoGameScreen.hShowInGame + 2;
//		xp = xFocus;
//		MainObject obj = GameScreen.ObjFocus;
//		// if (GameCanvas.isTouch) {
//		// xp -= 50 - mFont.tahoma_7b_white.getWidth(obj.name) / 2 + 14;
//		// }
//		if (obj.typeObject == MainObject.CAT_ITEM) {
//			AvMain.Font3dColor(g, obj.name, xp + 48, yp + 2, 1,
//					obj.colorName);
//		} else {
//			if (obj.typeObject == MainObject.CAT_PLAYER
//					|| obj.typeObject == MainObject.CAT_MONSTER) {
//				if (obj.myClan != null
//						&& obj.typeSpec == MainObject.SPEC_NORMAL) {
//					int wname = mFont.tahoma_7b_white.getWidth(obj.name) + 1;
//					obj.paintIconClan(g, xp + 48 - wname / 2, yp + 7, 2);
//					yp += 12;
//				}
//				if (obj.typeObject == MainObject.CAT_MONSTER) {
//					byte t = MainTabNew.COLOR_WHITE;
//					if (obj.Lv - GameScreen.player.Lv >= 3)
//						t = MainTabNew.COLOR_ORANGE;
//					else if (obj.Lv - GameScreen.player.Lv >= 1)
//						t = MainTabNew.COLOR_YELLOW;
//					AvMain.Font3dColorAndColor(g, obj.name, xp + 48,
//							yp + 2, 1, MainTabNew.COLOR_BLACK, t);
//				} else {
//					AvMain.Font3dWhite(g, obj.name, xp + 48, yp + 2, 1);
//				}
//				yp += 10;
//				AvMain.Font3dWhite(g, T.Lv + obj.Lv, xp + 48, yp + 2, 1);
//			} else
//				AvMain.Font3dWhite(g, obj.name, xp + 48, yp + 2, 1);
//		}
//		// g.drawImage(imgInfoFocus, xp + 65 - 14, yFocus + 15 - 14, 0);
//		// obj.paintAvatarFocus(g, xp + 65, yFocus + 15);
//		if (obj.typeObject == MainObject.CAT_PLAYER
//				|| obj.typeObject == MainObject.CAT_MONSTER
//				|| obj.typeObject == MainObject.CAT_NPC) {
//			g.setColor(0);
//			g.fillRect(xp - 4, yp + 15, 52, 5);
//			g.fillRect(xp - 4 + 1, yp + 14, 50, 1);
//			g.fillRect(xp - 4 + 1, yp + 20, 50, 1);
//			g.setColor(0xff37020e);
//			g.fillRect(xp - 4 + 1, yp + 15, 50, 5);
//			int hpPaint = 0;
//			if (obj.maxHp > 0) {
//				if (obj.hp > 0) {
//					hpPaint = (obj.hp * 50) / obj.maxHp;
//					if (hpPaint <= 0)
//						hpPaint = 1;
//					else if (hpPaint > 50)
//						hpPaint = 50;
//					g.drawRegion(AvMain.imgcolorhpmp, 0, 0, hpPaint, 5, 0,
//							xp - 4 + 1, yp + 15, 0);
//				}
//			}
//			mFont.tahoma_7_white.drawString(g, obj.hp + "/" + obj.maxHp,
//					xp - 4 + 26, yp + 20, 2);
//			// int t = 28;
//			// mFont.tahoma_7_white.drawString(g, T.Lv + obj.Lv, xp - 4 +
//			// 67,
//			// yFocus + t, 2);
//		}
//		// else if (obj.typeObject == MainObject.CAT_NPC) {
//		// g.setColor(0);
//		// g.fillRect(xp - 4, yp + 15, 52, 5);
//		// g.fillRect(xp - 4 + 1, yp + 14, 50, 1);
//		// g.fillRect(xp - 4 + 1, yp + 20, 50, 1);
//		// // g.setColor(0xff37020e);
//		// // g.fillRect(xp - 4 + 1, yFocus + 15, 50, 5);
//		// g.drawRegion(AvMain.imgcolorhpmp, 0, 0, 50, 5, 0, xp - 4 + 1,
//		// yp + 15, 0);
//		// mFont.tahoma_7_white.drawString(g, "100/100", xp - 4 + 26,
//		// yp + 20, 2);
//		// }
//	}
	}
	
	public void updateInfoChar() {
//	if (strInfoCharCline != null) {
//		timeInfoCharCline++;
//		if (timeInfoCharCline >= 120) {
//			timeInfoCharCline = 0;
//			strInfoCharCline = null;
//		}
//		if (ydInfoChar > 0)
//			ydInfoChar -= 2;
//	}
//	if (!GameCanvas.isTouch) {
//		if (timeChange > 0) {
//			timeChange++;
//			if (timeChange > 8) {
//				timeChange = 0;
//				Player.levelTab++;
//				if (Player.levelTab > 1)
//					Player.levelTab = 0;
//			}
//		}
//	}
//	if (timeNameMap > 0) {
//		if (timeNameMap == 20) {
//			vyNameMap = 10;
//		} else if (timeNameMap < 20) {
//			if (vyNameMap > -20)
//				vyNameMap -= 4;
//		}
//		if (yNameMap > -30) {
//			yNameMap += vyNameMap;
//		} else
//			timeNameMap = 0;
//	}
	}
	
	int timeEvent = 0, indexEvent, hShowEvent;
	public static int wShowEvent;
//	MainEvent eventShow = null;
	
	public void updateEvent() {
//	if (EventScreen.vecEventShow.size() > 0) {
//		if (eventShow == null) {
//			eventShow = (MainEvent) EventScreen.vecEventShow.elementAt(0);
//			timeEvent = 100;
//			hShowEvent = 0;
//		} else {
//			timeEvent--;
//			if (timeEvent <= 0) {
//				eventShow = null;
//				EventScreen.vecEventShow.removeElementAt(0);
//			}
//			if (hShowEvent < 35)
//				hShowEvent += 10;
//			if (hShowEvent > 35)
//				hShowEvent = 35;
//		}
//		if (GameCanvas.isPointSelect(GameCanvas.hw - wShowEvent / 2, 0,
//				wShowEvent, 35)) {
//			MainEvent ev = EventScreen.setEvent(eventShow.nameEvent,
//					(byte) eventShow.IDCmd);
//			if (ev != null) {
//				GameCanvas.mevent.doEvent(false, ev);
//			}
//			if (timeEvent > 40) {
//				timeEvent = 40;
//			}
//			GameCanvas.isPointerSelect = false;
//		}
//		if (GameCanvas.keyMyHold[11]) {
//			GameCanvas.clearKeyHold(11);
//			int t = EventScreen.setIndexEvent(eventShow.nameEvent,
//					(byte) eventShow.IDCmd);
//			if (t >= 0)
//				GameCanvas.mevent.idSelect = t;
//			GameCanvas.mevent.init();
//			GameCanvas.mevent.Show(GameCanvas.currentScreen);
//		}
//	} else {
//		if (hShowEvent > 0)
//			hShowEvent -= 20;
//	}
	}
	
	public void paintShowEvent(mGraphics g) {
//	if (eventShow != null || hShowEvent > 0) {
//		GameCanvas.resetTrans(g);
//		int x = GameCanvas.hw - wShowEvent / 2;
//		int index = 2;
//		if (GameCanvas.gameTick % 16 > 7)
//			index = 12;
//		AvMain.paintDialogNew(g, x, -5, wShowEvent, hShowEvent + 5, index);
//		if (eventShow != null) {
//			PaintInfoGameScreen.fraEvent.drawFrame(eventShow.IDCmd * 2,
//					x + 20, -35 + hShowEvent + 35 / 2 + 3, 0, 3, g);
//			mFont.tahoma_7b_white.drawString(g, eventShow.nameEvent,
//					x + 35, -35 + hShowEvent + 5, 0);
//			mFont.tahoma_7_white.drawString(g, eventShow.contentEvent,
//					x + 42, -35 + hShowEvent + 18, 0);
//	
//		}
//	}
	
	}
	
	public void paintPoiterAll(mGraphics g) {
	// paint Move
//	if (!GameCanvas.isTouch)
//		return;
//	// g.drawRect(xPointKill - lSkill - 25, yPointKill
//	// - lSkill - 25, lSkill * 2 + 50, lSkill * 2 + 50);
//	// g.drawRect(xPointMove - wArrowMove - 30, yPointMove
//	// - wArrowMove - 30, wArrowMove * 2 + 60,
//	// wArrowMove * 2 + 60);
//	// for (int i = 0; i < mKeyMove.Length; i++) {
//	// g.drawRect(mPosMove[i][0] - wPointArrow / 2, mPosMove[i][1]
//	// - wPointArrow / 2 , wPointArrow , wPointArrow );
//	// mFont.tahoma_7_black.drawString(g, i+"",mPosMove[i][0] - wPointArrow
//	// / 2, mPosMove[i][1]
//	// - wPointArrow / 2, 2);
//	// }
//	for (int i = 0; i < mPosOther.Length; i++) {
//		if (i == 3 && isLevelPoint)
//			continue;
//		int tsub = 0;
//		if (timePointer > 0 && keyPoint == 100 + i)// select 5
//			tsub = 1;
//		if (GameScreen.player.Action != MainObject.AC_DIE) {
//			int xp = mPosOther[i][0], yp = mPosOther[i][1];
//			if (i == 0) {
//				yp -= hShowInGame;
//			} else if (i == 1) {
//				xp -= hShowInGame;
//			} else if (i == 4) {
//				yp += hShowInGame;
//			} else {
//				xp += hShowInGame;
//			}
//			g.drawRegion(imgOther[i], 0, tsub * mSizeImgOther[i][1],
//					mSizeImgOther[i][0], mSizeImgOther[i][1], 0, xp, yp, 0);
//		}
//	}
//	if (GameScreen.player.Action == MainObject.AC_DIE) {
//		return;
//	}
//	
//	if (isLevelPoint) {
//		paintKillPlayer(g);
//	} else {
//		g.drawImage(imgMove[0], xPointMove - hShowInGame, yPointMove, 3);
//		for (int i = 0; i < 4; i++) {// 4 mui ten
//			if (timePointer > 0 && mKeyMove[i] == keyPoint) {// khi dc
//				// cham
//				g.drawRegion(imgMove[2], 0, 0, 32, 56, mRotateMove[i]
//						+ (i > 1 ? 4 : 0), mPosMove[i][0] - hShowInGame,
//						mPosMove[i][1], 3);
//	
//			}
//		}
//		// Paint Fire
//		int t = 0;
//		if (timePointer > 0 && keyPoint == 5)// select 5
//			t = 1;
//		g.drawImage(AvMain.imgHotKey, xPointKill + hShowInGame, yPointKill,
//				3);
//		if (timeChange == 0) {
//			if (GameScreen.ObjFocus != null
//					&& GameScreen.ObjFocus.typeBoss == MainObject.MON_CAPCHAR) {
//				for (int i = 0; i < 5; i++) {
//	
//					int tsub = 0;
//					if (timePointer > 0 && keyPoint == mValueHotKey[i])// select
//						// 5
//						tsub = 1;
//					int xp = 0, yp = 0;
//					if (i == 2) {
//						xp = xPointKill + hShowInGame;
//						yp = yPointKill;
//					} else {
//						xp = mPosKill[i - (i > 2 ? 1 : 0)][0] + hShowInGame;
//						yp = mPosKill[i - (i > 2 ? 1 : 0)][1];
//					}
//					g.drawRegion(imgFire[1], 0, tsub, 50, 50, 0, xp, yp, 3);
//					g.drawImage(AvMain.imgHotKey, xp, yp, 3);
//					AvMain.Font3dWhite(g, mValueHotKey[i] + "", xp, yp - 5,
//							2);
//				}
//				// g.drawRegion(imgFire[0], 0, t * 50, 50, 50, 0,
//				// xPointKill,
//				// yPointKill, 3);
//				return;
//			}
//			// int indexSkill =
//			// GameScreen.player.mUsedKill[Player.levelTab][2];
//			HotKey hotkey = Player.mhotkey[Player.levelTab][2];
//			if (GameScreen.player.checkGiaoTiep()) {
//				g.drawImage(AvMain.imgicongt, xPointKill + hShowInGame,
//						yPointKill, 3);
//			} else {
//				if (hotkey.type == HotKey.SKILL) {
//					Skill s = (Skill) MainListKill
//							.getSkillFormId(hotkey.id);
//					// s.paintSmall(g, xPointKill, yPointKill, 3);
//					if (s != null)
//						s.paint(g, xPointKill + hShowInGame, yPointKill, 3);
//				} else if (hotkey.type == HotKey.POTION) {
//					MainItem item = MainItem.getItemInventory(
//							MainObject.CAT_POTION/*
//												 * MainTemplateItem. POTION
//												 */, (short) hotkey.id);
//					if (item != null) {
//						item.paintItem(g, xPointKill + hShowInGame,
//								yPointKill, MainTabNew.wOneItem, 0, 3);
//					}
//				}
//			}
//			for (int i = 0; i < mPosKill.Length; i++) {
//				int tsub = 0;
//				if (timePointer > 0 && keyPoint == mKeySkill[i])// select
//					// 5
//					tsub = 1;
//				g.drawRegion(imgFire[1], 0, tsub * 50, 50, 50, 0,
//						mPosKill[i][0] + hShowInGame, mPosKill[i][1], 3);
//				g.drawImage(AvMain.imgHotKey, mPosKill[i][0] + hShowInGame,
//						mPosKill[i][1], 3);
//				hotkey = Player.mhotkey[Player.levelTab][i
//						+ (i > 1 ? 1 : 0)];
//				if (hotkey.type == HotKey.SKILL) {
//					Skill s = (Skill) MainListKill
//							.getSkillFormId(hotkey.id);
//					// s.paintSmall(g, mPosKill[i][0], mPosKill[i][1],
//					// 3);
//					if (s != null)
//						s.paint(g, mPosKill[i][0] + hShowInGame,
//								mPosKill[i][1], 3);
//				} else if (hotkey.type == HotKey.POTION) {
//					if (MainTemplateItem.isload) {
//						MainItem item = MainItem.getItemInventory(
//								MainObject.CAT_POTION/*
//													 * MainTemplateItem.
//													 * POTION
//													 */, (short) hotkey.id);
//						if (item != null) {
//							if (MainTemplateItem.isload)
//								item.paintItem(g, mPosKill[i][0]
//										+ hShowInGame, mPosKill[i][1],
//										MainTabNew.wOneItem, 0, 3);
//						} else {
//							hotkey.setHotKey(0, HotKey.NULL, (byte) 0);
//							MainItem.setAddHotKey(MainItem.MP, false);
//							MainItem.setAddHotKey(MainItem.HP, false);
//						}
//					}
//				}
//			}
//			for (int i = 0; i < 5; i++) {
//				HotKey t2 = Player.mhotkey[Player.levelTab][i];
//				if (t2.type != HotKey.NULL) {
//					int xp = 0, yp = 0;
//					if (i == 2) {
//						xp = xPointKill + hShowInGame;
//						yp = yPointKill;
//					} else {
//						xp = mPosKill[i - (i > 2 ? 1 : 0)][0] + hShowInGame;
//						yp = mPosKill[i - (i > 2 ? 1 : 0)][1];
//					}
//					DelaySkill delay = null;
//					if (t2.type == HotKey.SKILL)
//						delay = Player.timeDelaySkill[t2.id];
//					else if (t2.type == HotKey.POTION) {
//						if (MainTemplateItem.isload) {
//							MainItem item = MainItem.getItemInventory(
//									MainObject.CAT_POTION, (short) t2.id);
//							if (item != null && item.typePotion < 2)
//								delay = Player.timeDelayPotion[item.typePotion];
//						}
//					}
//					if (delay != null) {
//						if (delay.limit > 0) {
//							if (delay.value > 0) {
//								// if (delay.limit == 0)
//								// mSystem.out("delay=" + delay.limit);
//								int hpaint = (delay.value * 20)
//										/ delay.limit;
//								if (hpaint < 1)
//									hpaint = 1;
//								g.drawRegion(AvMain.imgDelaySkill, 0, 0,
//										20, hpaint, 0, xp - 10, yp - 10, 0);
//								int second = delay.value / 1000;
//								string strsecond = "";
//								if (second == 0) {
//									strsecond = "0." + (delay.value % 1000)
//											/ 100;
//								} else {
//									strsecond = "" + second;
//								}
//								mFont.tahoma_7b_white.drawString(g,
//										strsecond, xp, yp - 5, 2);
//							} else if (delay.value > -150) {
//								g.setColor(0xffeeeecc);
//								g.fillRoundRect(xp - 10, yp - 10, 20, 20,
//										4, 4);
//								// g.drawRect(xp - 11, yp - 11, 21, 21);
//							}
//						}
//					}
//				}
//			}
//		} else {
//			paintChangeTab(g);
//		}
//		g.drawRegion(imgFire[0], 0, t * 50, 50, 50, 0, xPointKill
//				+ hShowInGame, yPointKill, 3);
//	}
	}
	
	public void updatePoiterAll() {
	// try {
//	if (timePointer > 0)
//		timePointer--;
//	if (timeChange > 0) {
//		timeChange++;
//		if (timeChange > 6) {
//			timeChange = 0;
//			Player.levelTab++;
//			if (Player.levelTab > 1)
//				Player.levelTab = 0;
//		}
//	}
//	if (LoadMap.isShowEffAuto == LoadMap.EFF_PHOBANG_END)
//		return;
//	bool isMove = true;
//	if (GameCanvas.isPointerSelect) {// Kill
//		if (isLevelPoint) {
//			if (GameCanvas.isPoint(xPaintSkill + 11 - wSkill / 2,
//					yPaintSkill + 11 - wSkill / 2, 5 * wSkill, wSkill)) {
//				int tam = (GameCanvas.px - xPaintSkill + 11) / wSkill;
//				GameCanvas.isPointerSelect = false;
//				if (tam >= 0
//						&& tam < Player.mhotkey[Player.levelTab].Length) {
//					HotKey t = Player.mhotkey[Player.levelTab][tam];
//					if (PaintInfoGameScreen.isLevelPoint) {
//						if (t.type == HotKey.SKILL) {
//							// Player.skillDefault = t.id;
//							if (GameScreen.ObjFocus != null) {
//								GameScreen.player.setActionHotKey(tam,
//										false);
//							}
//						} else {
//							if (tam == 2)
//								keyPoint = 5;
//							else
//								keyPoint = mKeySkill[tam > 1 ? (tam - 1)
//										: tam];
//							GameCanvas.keyMyPressed[20 + keyPoint] = true;
//						}
//					}
//					timePointer = 3;
//					isMove = false;
//				}
//			}
//		} else {
//			if (GameCanvas.isPoint(xPointKill - wMainSkill / 2, yPointKill
//					- wMainSkill / 2, wMainSkill, wMainSkill)) {
//				GameCanvas.isPointerSelect = false;
//				keyPoint = 5;
//				timePointer = 3;
//				GameCanvas.keyMyPressed[25] = true;
//				GameCanvas.keyMyPressed[5] = true;
//				isMove = false;
//			} else {
//				for (int i = 0; i < mPosKill.Length; i++) {
//					if (GameCanvas.isPoint(mPosKill[i][0] - wSkill / 2,
//							mPosKill[i][1] - wSkill / 2, wSkill, wSkill)) {
//						GameCanvas.isPointerSelect = false;
//						keyPoint = mKeySkill[i];
//						GameCanvas.keyMyPressed[20 + keyPoint] = true;
//						timePointer = 3;
//						isMove = false;
//						break;
//					}
//				}
//			}
//		}
//		if (GameCanvas.isPointerSelect) {// Other
//			for (int i = 0; i < mPosOther.Length; i++) {
//				if (i == 3 && isLevelPoint)
//					continue;
//				if (GameCanvas.isPoint(mPosOther[i][0] - 2,
//						mPosOther[i][1] - 2, mSizeImgOther[i][0] + 4,
//						mSizeImgOther[i][1] + 4)) {
//					GameCanvas.isPointerSelect = false;
//					// keyPoint = 100 + i;
//					// timePointer = 2;
//					selectPointer(i);
//					isMove = false;
//					break;
//				}
//			}
//			if (GameCanvas.isPoint(MainTabNew.gI().xChar,
//					MainTabNew.gI().yChar, 90, 35)) {
//				GameCanvas.isPointerSelect = false;
//				selectPointer(0);
//				isMove = false;
//				return;
//			}
//			if (GameCanvas.isPoint(xMess - 4, yMess - 4, 24, 20)) {
//				GameCanvas.isPointerSelect = false;
//				selectPointer(-1);
//			}
//	
//			if (GameCanvas.isPoint(GameCanvas.w - 50,
//					GameCanvas.minimap.maxY * GameCanvas.minimap.wMini - 8,
//					50, 30)) {
//				GameCanvas.isPointerSelect = false;
//				selectPointer(-2);
//			} else if (GameCanvas.isPoint(GameCanvas.w
//					- GameCanvas.minimap.maxX * GameCanvas.minimap.wMini,
//					0, GameCanvas.minimap.maxX * GameCanvas.minimap.wMini,
//					GameCanvas.minimap.maxY * GameCanvas.minimap.wMini)) {
//				GameCanvas.isPointerSelect = false;
//				selectPointer(-4);
//			}
//			if (GameCanvas.isPoint(95, 0, 24, 40)) {
//				GameCanvas.isPointerSelect = false;
//				selectPointer(-3);
//			}
//			for (int i = 0; i < LoadMap.vecPointChange.size(); i++) {
//				Point p = (Point) LoadMap.vecPointChange.elementAt(i);
//				int xp = p.x - GameScreen.cameraMain.xCam;
//				int yp = p.y - GameScreen.cameraMain.yCam;
//				if (GameCanvas.isPoint(xp - 12, yp - 12, 24, 24)) {
//					// mSystem.out("ok chua");
//					GameScreen.player.toX = GameScreen.player.x;
//					GameScreen.player.toY = GameScreen.player.y;
//					posTam = GameCanvas.game.updateFindRoad(p.x
//							/ LoadMap.wTile, p.y / LoadMap.wTile,
//							GameScreen.player.x / LoadMap.wTile,
//							GameScreen.player.y / LoadMap.wTile, 16);
//					if (posTam != null && posTam.Length > 16) {
//						posTam = null;
//					}
//					GameScreen.player.posTransRoad = posTam;
//					Player.xFocus = p.x;
//					Player.yFocus = p.y;
//					Player.timeFocus = 9;
//					GameCanvas.isPointerSelect = false;
//					if (Player.isAutoFire == 1) {
//						Player.isAutoFire = 0;
//					}
//					break;
//				}
//			}
//		}
//	} else if (!isLevelPoint) {// Move
//		if (GameCanvas.isPointerDown || GameCanvas.isPointerMove) {
//			// Other
//			for (int i = 0; i < mPosOther.Length; i++) {
//				if (GameCanvas.isPoint(mPosOther[i][0] - 4,
//						mPosOther[i][1] - 4, mSizeImgOther[i][0] + 8,
//						mSizeImgOther[i][1] + 8)) {
//					keyPoint = 100 + i;
//					timePointer = 3;
//					break;
//				}
//			}
//	
//			if (GameCanvas.isPointLast(xPointMove - 2 * wArrowMove,
//					yPointMove - 2 * wArrowMove, wArrowMove * 4,
//					wArrowMove * 4)) {
//				int gocMove = CRes.angle(GameCanvas.px - xPointMove,
//						GameCanvas.py - yPointMove);
//				int value = 0;
//				if (gocMove > 45 && gocMove <= 135) {
//					value = 3;
//				} else if (gocMove > 135 && gocMove <= 225) {
//					value = 0;
//				} else if (gocMove > 225 && gocMove <= 315) {
//					value = 2;
//				} else {
//					value = 1;
//				}
//				GameCanvas.clearKeyHold();
//				GameCanvas.isPointerDown = true;
//				GameCanvas.isPointerSelect = false;
//				keyPoint = mKeyMove[value];
//				GameCanvas.keyMyHold[keyPoint] = true;
//				timePointer = 3;
//				isMove = false;
//				if (Player.isAutoFire == 1) {
//					Player.isAutoFire = 0;
//				}
//			}
//		}
//	}
//	if (isMove) {
//		updatePointMoveIngame();
//	}
//	if (isLevelPoint) {
//		if (GameCanvas.currentScreen == GameCanvas.game) {
//			if (GameCanvas.isPointerMove) {
//				if (!GameScreen.isMoveCamera
//						&& (CRes.abs(GameCanvas.px - GameCanvas.pxLast) > 36 || CRes
//								.abs(GameCanvas.py - GameCanvas.pyLast) > 36)) {
//					GameScreen.isMoveCamera = true;
//	
//				}
//				GameScreen.xMoveCam = GameCanvas.px - GameCanvas.pxLast;
//				GameScreen.yMoveCam = GameCanvas.py - GameCanvas.pyLast;
//				GameScreen.timeResetCam = 40;
//			} else if (GameCanvas.isPointerDown) {
//				GameScreen.xCur = GameScreen.cameraMain.xCam;
//				GameScreen.yCur = GameScreen.cameraMain.yCam;
//				GameScreen.xMoveCam = 0;
//				GameScreen.yMoveCam = 0;
//			}
//		}
//	}
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	}
	
	public short[] posTam;
	int timeMove;
	int xlast, ylast;
	
	public void updatePointMoveIngame() {
	// try {
//	if (GameCanvas.isPointerClick) {
//		if (!isLevelPoint) {
//			if ((GameCanvas.isPoint(xPointKill - lSkill - 25, yPointKill
//					- lSkill - 25, lSkill * 2 + 50, lSkill * 2 + 50) || GameCanvas
//					.isPoint(xPointMove - wArrowMove - 30, yPointMove
//							- wArrowMove - 30, wArrowMove * 2 + 60,
//							wArrowMove * 2 + 60))
//					&& GameScreen.player.Action != MainObject.AC_DIE) {
//				GameCanvas.isPointerSelect = false;
//				GameCanvas.isPointerDown = false;
//				return;
//			}
//		}
//		int xp = GameCanvas.px + GameScreen.cameraMain.xCam;
//		int yp = GameCanvas.py + GameScreen.cameraMain.yCam;
//		MainObject obj = null;
//		if (GameScreen.ObjFocus != null
//				&& GameScreen.ObjFocus.typeBoss == MainObject.MON_CAPCHAR) {
//	
//		} else {
//			if (MainObject.getDistance(xp, yp, GameScreen.player.x,
//					GameScreen.player.y) <= GameScreen.player.wFocus - 15
//					|| GameScreen.player.Action == MainObject.AC_DIE) {
//				obj = setObjectNear(xp, yp);
//				if (obj != null && obj.typeObject != MainObject.CAT_MONSTER) {
//					if (Player.isAutoFire == 1) {
//						Player.isAutoFire = 0;
//					}
//				}
//			}
//		}
//		if (isLevelPoint) {// mo touch
//			if (obj != null) {
//				obj.timeStand = 5;
//				GameScreen.ObjFocus = obj;
//				GameCanvas.isPointerSelect = false;
//				if (MainObject.getDistance(obj.x, obj.y,
//						GameScreen.player.x, GameScreen.player.y) <= GameScreen.player.wFocus) {
//					GameScreen.player.setPointFocus();
//					isPaintInfoFocus = true;
//					GameScreen
//							.addEffectKill(
//									EffectKill.EFF_FOCUS,
//									GameScreen.player.ID,
//									MainObject.CAT_PLAYER,
//									GameScreen.ObjFocus.ID,
//									GameScreen.ObjFocus.typeObject,
//									0,
//									GameScreen.ObjFocus.hp,
//									(byte) (GameScreen.ObjFocus.typeObject == MainObject.CAT_MONSTER ? 0
//											: 1));
//					posTam = null;
//				}
//				// else {
//				if (GameCanvas.isPointerSelect) {
//					int tile = GameCanvas.loadmap.getTile(xp, yp);
//					if (tile != LoadMap.T_MAP_NULL
//							&& tile != LoadMap.T_MAP_STAND) {
//						GameScreen.player.toX = GameScreen.player.x;
//						GameScreen.player.toY = GameScreen.player.y;
//						posTam = GameCanvas.game.updateFindRoad(xp
//								/ LoadMap.wTile, yp / LoadMap.wTile,
//								GameScreen.player.x / LoadMap.wTile,
//								GameScreen.player.y / LoadMap.wTile, 100);
//						if (posTam != null && posTam.Length > 100) {
//							posTam = null;
//						}
//						timeMove = 3;
//						GameCanvas.isPointerSelect = false;
//						Player.xFocus = xp;
//						Player.yFocus = yp;
//						// Player.timeFocus = 9;
//					} else {
//						posTam = null;
//					}
//				}
//				// }
//	
//			} else {
//				if (GameScreen.player.Action == MainObject.AC_DIE)
//					return;
//				int tile = GameCanvas.loadmap.getTile(xp, yp);
//				if (tile != LoadMap.T_MAP_NULL
//						&& tile != LoadMap.T_MAP_STAND) {
//					GameScreen.player.toX = GameScreen.player.x;
//					GameScreen.player.toY = GameScreen.player.y;
//					posTam = GameCanvas.game.updateFindRoad(xp
//							/ LoadMap.wTile, yp / LoadMap.wTile,
//							GameScreen.player.x / LoadMap.wTile,
//							GameScreen.player.y / LoadMap.wTile, 100);
//					if (posTam != null && posTam.Length > 100) {
//						posTam = null;
//					}
//					if (GameScreen.player.posTransRoad != null)
//						timeMove = 1;
//					else
//						timeMove = 3;
//					GameCanvas.isPointerSelect = false;
//					Player.xFocus = xp;
//					Player.yFocus = yp;
//	
//				} else {
//					posTam = null;
//					GameCanvas.isPointerSelect = false;
//				}
//			}
//		} else {// tat touch
//			if (obj != null) {
//				vecfocus.removeAllElements();
//				if (GameScreen.ObjFocus == obj) {
//					if (Player.mhotkey[Player.levelTab][2].type == HotKey.SKILL) {
//						GameCanvas.keyMyPressed[25] = true;
//						GameCanvas.keyMyPressed[5] = true;
//					}
//					timeMove = 0;
//					posTam = null;
//				} else {
//	//				mSystem.outz("0666666666666666666");
//					obj.timeStand = 5;
//					GameScreen.ObjFocus = obj;
//				}
//				GameCanvas.isPointerSelect = false;
//			}
//		}
//	}
//	if (isLevelPoint) {
//		if (timeMove > 0) {
//			if (timeMove == 1 && posTam != null) {
//				if (GameScreen.player.Action != MainObject.AC_DIE
//						&& GameScreen.player.Action != MainObject.AC_FIRE
//						&& GameScreen.player.currentQuest == null) {
//					GameScreen.player.xStopMove = 0;
//					GameScreen.player.yStopMove = 0;
//					if (GameScreen.player.posTransRoad != null) {
//						GameScreen.player.countAutoMove = 1;
//					}
//					GameScreen.player.resetMove();
//					GameScreen.player.posTransRoad = posTam;
//					posTam = null;
//					Player.timeFocus = 9;
//					if (Player.timeResetAuto <= 0) {
//						if (Player.isAutoFire == 1)
//							Player.isAutoFire = 0;
//					}
//				}
//			}
//			timeMove--;
//		}
//	}
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	}
	
	public static mVector vecfocus = new mVector();
	
//	public MainObject setObjectNear(int x, int y) {
//	MainObject objreturn = null, objtam = null;
//	int min = 40;
//	bool isok = false;
//	for (int i = 0; i < GameScreen.Vecplayers.size(); i++) {
//		MainObject obj = (MainObject) GameScreen.Vecplayers.elementAt(i);
//		if (obj == null || obj == GameScreen.player)
//			continue;
//		if (obj.Action != MainObject.AC_DIE
//				|| obj.typeObject != MainObject.CAT_MONSTER) {
//			int dis = MainObject.getDistance(x, y, obj.x, obj.y - obj.hOne
//					/ 4);
//			if (dis < min
//					|| (GameCanvas.loadmap.mapLang()
//							&& obj.typeObject == MainObject.CAT_NPC
//							&& dis < 40 && Player.isFocusNPC)) {
//				if (!isok) {
//					isok = true;
//					objtam = obj;
//				}
//				bool isgan = true;
//				for (int j = 0; j < vecfocus.size(); j++) {
//					MainObject objset = (MainObject) vecfocus.elementAt(j);
//					if (objset == null)
//						continue;
//					if (obj.ID == objset.ID) {
//						// mSystem.outz("vao roi size no =" +
//						// vecfocus.size());
//						isgan = false;
//						break;
//					}
//				}
//				if (isgan) {
//					objreturn = obj;
//					min = dis;
//				}
//				if (
//				// GameCanvas.loadmap.mapLang()
//				// &&
//				obj.typeObject == MainObject.CAT_NPC && Player.isFocusNPC) {
//					vecfocus.addElement(objreturn);
//					Player.isFocusNPC = false;
//					Player.timeFocusNPC = 0;
//					return objreturn;
//				}
//			}
//		}
//	}
//	if (isok && objreturn == null) {
//		vecfocus.removeAllElements();
//		objreturn = objtam;
//	}
//	if (objreturn != null)
//		vecfocus.addElement(objreturn);
//	else {
//		vecfocus.removeAllElements();
//	}
//	
//	return objreturn;
//	}
	
	public void paintChangeTab(mGraphics g) {
	int goc = gocBegin;
	if (timeChange > 0)
		goc -= timeChange * 30;
	for (int i = 0; i < 8; i++) {
		int xp = xPointKill + (Res.cos(Res.fixangle(goc)) * lSkill)
				/ 1000 + hShowInGame;
		int yp = yPointKill + (Res.sin(Res.fixangle(goc)) * lSkill)
				/ 1000;
		// g.drawRegion(imgFire[1], 0, 0, 30, 30, 0, xp, yp, 3);
//		g.drawImage(AvMain.imgHotKey, xp, yp, 3);
//		int valueTab = Player.levelTab;
//		if (i > 3) {
//			if (Player.levelTab == 0)
//				valueTab = 1;
//			else
//				valueTab = 0;
//		}
//		HotKey hotkey = Player.mhotkey[valueTab][i % 4
//				+ (i % 4 > 1 ? 1 : 0)];
//		if (hotkey.type == HotKey.SKILL) {
//			Skill s = (Skill) MainListKill.getSkillFormId(hotkey.id);
//			s.paint(g, xp, yp, 3);
//		} else if (hotkey.type == HotKey.POTION) {
//			MainItem item = MainItem.getItemInventory(
//					MainObject.CAT_POTION, (short) hotkey.id);
//			if (item != null)
//				item.paintItem(g, xp, yp, MainTabNew.wOneItem, 0, 3);
//		}
//		goc -= 45;
	}
	}
	
	public void selectPointer(int select) {
	switch (select) {
	case -4:
//		MiniMapFull_Screen.gI().Show();
		break;
	case -3:
//		if (Player.diemTiemNang > 0) {
//			GameCanvas.AllInfo.Show(GameCanvas.currentScreen);
//			GameCanvas.AllInfo.selectTab = 2;
//			return;
//		}
//		if (Player.diemKyNang > 0) {
//			GameCanvas.AllInfo.Show(GameCanvas.currentScreen);
//			GameCanvas.AllInfo.selectTab = 3;
//			return;
//		}
		break;
	case -2:
//		GameScreen.gI().doArea();
		break;
	case -1:
		// GameCanvas.start_Chat_Dialog();
//		GameCanvas.mevent.init();
//		GameCanvas.mevent.Show(GameCanvas.currentScreen);
		break;
	case 0:
//		if (GameScreen.isMoveCamera) {
//			GameScreen.isMoveCamera = false;
//		} else
//			GameScreen.gI().cmdMenu.perform();
		break;
	case 1:
//		ChatTextField.gI().setChat();
		break;
	case 2:
//		if (GameScreen.ObjFocus != null
//				&& GameScreen.ObjFocus.typeBoss == MainObject.MON_CAPCHAR) {
//		} else if (timeChange == 0)
//			timeChange = 1;
//		break;
	case 3:
//		if (GameScreen.ObjFocus != null)
//			Player.cmdNextFocus.perform();
		break;
	case 4:
//		if (!ChatTextField.gI().isShow)
//			GameCanvas.menu2.setAt_Quick();
        break;
	}
	}
	
	public void paintParty(mGraphics g) {
//	if (Player.party != null) {
//		// int yp = yParty;
//		for (int i = 0; i < Player.party.vecPartys.size(); i++) {
//			ObjectParty obj = (ObjectParty) Player.party.vecPartys
//					.elementAt(i);
//			if (obj.name.compareTo(GameScreen.player.name) == 0
//					|| obj.isRemove)
//				continue;
//			// if (obj.name.compareTo(GameScreen.player.party.nameMain)==0)
//			// mFont.tahoma_7b_violet.drawString(g, "(" + obj.Lv + ") "
//			// + obj.name, xParty, yp, 0);
//			// else
//			// mFont.tahoma_7b_white.drawString(g, "(" + obj.Lv + ") "
//			// + obj.name, xParty, yp, 0);
//			// g.setColor(0xff7b061e);
//			// g.fillRect(xParty, yp + 12, 40, 3);
//			// g.setColor(0xfff72849);
//			// g.fillRect(xParty, yp + 12, (40 * obj.hp) / obj.maxhp, 3);
//			// yp += GameCanvas.hText + 3;
//			// g.setColor(0xff7b061e);
//			// g.fillRect(obj.x-20,obj.y-60 , 40, 3);
//			// g.setColor(0xfff72849);
//			// g.fillRect(obj.x-20, obj.y- 60, (40 * obj.hp) / obj.maxhp,
//			// 3);
//			// yp += GameCanvas.hText + 3;
//		}
//	}
	}
	
	public void paintNameMap(mGraphics g) {
	if (timeNameMap > 0) {
		timeNameMap--;
		GuiCommunication.paintDialog(g, GameCanvas.hw - wNameMap / 2 - 10, yNameMap,
				wNameMap + 20, 35, 12);
		mFont.tahoma_7b_white.drawString(g, namemap, GameCanvas.hw,
				yNameMap + 7, 2);
//		mFont.tahoma_7_white.drawString(g, "- " + T.Area
//				+ LoadMap.getAreaPaint() + " -", GameCanvas.hw,
//				yNameMap + 20, 2);
	}
	}
	
	public static void setNameMap() {
	timeNameMap = 80;
	// namemap =
	// WorldMapScreen.namePos[WorldMapScreen.mTestPlayerPos[GameCanvas.loadmap.idMap]];
	// namemap = WorldMapScreen.namePos[GameCanvas.loadmap.idMap];
	namemap = "map";
//	if (WorldMapScreen.namePos != null) {
//		if (GameCanvas.loadmap.idMap < WorldMapScreen.namePos.Length)
//			namemap = WorldMapScreen.namePos[GameCanvas.loadmap.idMap];
//	}
	wNameMap = mFont.tahoma_7b_white.getWidth(namemap);
	yNameMap = GameCanvas.h / 8;
	vyNameMap = 0;
	if (wNameMap < 80)
		wNameMap = 80;
	}
	
	public void updateShowIngame() {
	if (isShowInGame) {
//		if (GameCanvas.isSmallScreen) {
//			if ((GameCanvas.timeNow - timeDoNotClick) / 1000 > 15) {
//				isShowInGame = false;
//			}
//		} else if ((GameCanvas.timeNow - timeDoNotClick) / 1000 > 2) {
//			isShowInGame = false;
//		}
		if (hShowInGame > 0) {
			hShowInGame -= 20;
			if (hShowInGame < 0)
				hShowInGame = 0;
		}
	} else {
		if (hShowInGame < 100) {
			hShowInGame += 10;
		}
	}
	
	}
	
	public static void setPosTouch() {
	if (isLevelPoint) {
		PaintInfoGameScreen.mPosOther[2][1] = GameCanvas.h - 31;
		PaintInfoGameScreen.mPosOther[4][0] = 0;
	} else {
		PaintInfoGameScreen.mPosOther[2][1] = GameCanvas.h - 145;
		PaintInfoGameScreen.mPosOther[4][0] = GameCanvas.hw - 20;
	}
	}

}
