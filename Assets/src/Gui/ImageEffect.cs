

using System.Collections;
public class ImageEffect
{
    public static mHashtable hashImageEff = new mHashtable();
	
	long timeRemove;
    int IdImage;
    mBitmap img;

	public ImageEffect(int Id) {
        this.IdImage = Id;
        mBitmap imgg = GameCanvas.loadImage("/eff/g" + Id + ".png");
        Cout.println(Id + "  ImageEffect  path  "+imgg);
        if (imgg != null) img = imgg;
        Cout.println(Id + "  ImageEffect  path  " + imgg.image);
        //		timeRemove = GameCanvas.timeNow;
    }

    public static mBitmap setImage(int Id)
    {
        ImageEffect img = (ImageEffect)hashImageEff.get("" + Id);

        if (img == null)
        {
            img = new ImageEffect(Id);
            if (img.img != null && img.img.image != null && img.img.image.texture != null)
            {
                hashImageEff.put("" + Id, img);
                return img.img;
            }
            else return null;
        }
        //		else
        //			img.timeRemove = GameCanvas.timeNow;
        return img.img;
    }

	public static void SetRemove() {
        IDictionaryEnumerator k = hashImageEff.GetEnumerator();
		while (k.MoveNext()) {
			string keyset = (string) k.Key.ToString();
			ImageEffect img = (ImageEffect) hashImageEff.get(keyset);
//			if ((GameCanvas.timeNow - img.timeRemove) / 1000 > 300) {
//				hashImageEff.remove(keyset);
//			}
		}
	}

	public static void SetRemoveAll() {
		hashImageEff.clear();
	}
}
