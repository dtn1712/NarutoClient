﻿
/*
 * This class: paint info char
 */
using System;
public class TabInfoChar : MainTabNew {

    //	private mBitmap[] arrayHuman=new mBitmap[25];
    public int xTabInfo = 0;
    private mBitmap imgnv, bt_Plus_0;

    ///1.	Khai Môn
    //	2.	 Hưu Môn 
    //	3.	Sinh Môn 
    //
    //	4.	Đỗ Môn 
    //
    //	5.	Cảnh Môn 
    //	 
    //	6.	Thương Môn 
    //
    //	7.	Kinh Môn 
    //
    //	8.	Tử Môn 

    public string[] info = new string[] { "Khai Môn", "Hưu Môn ", "Sinh Môn ", "Đỗ Môn", "Cảnh Môn", "Thương Môn", "Kinh Môn", "Tử Môn" };
    public int[] maptoMon = new int[] { 0, 7, 1, 2, 3, 4, 6, 5 };
    public string[] infoText = new string[]{
			"HP","Charka","Tấn công VL","Tấn công Phép","Phòng thủ VL",
			"Phòng thủ Phép","Tốc đánh","Bạo kích"};
    public Scroll infoPaintChar = new Scroll();
	
	public TabInfoChar(string name) {
        typeTab = MY_INFO;
        this.nameTab = name;
        tfPoint = new TField();
        xTabInfo = xTab - 22;
        for (int i = 0; i < xyTField.Length; i++)
        {
            xyTField[i][0] = xTabInfo - 10 + (i > 3 ? 165 : 55);//xTab-10,yTab-30+ GameCanvas.h / 5
            xyTField[i][1] = yTab + 37 * (i % 4 + 1) + 30;
        }
        xySo[0][0] = 91; xySo[0][1] = 30;
        xySo[1][0] = 80; xySo[1][1] = 30;
        xySo[2][0] = 77; xySo[2][1] = 63;
        xySo[3][0] = 77; xySo[3][1] = 75;
        xySo[4][0] = 77; xySo[4][1] = 87;
        xySo[5][0] = 77; xySo[5][1] = 96;
        xySo[6][0] = 86; xySo[6][1] = 108;
        xySo[7][0] = 92; xySo[7][1] = 75;
        for (int i = 0; i < xySo.Length; i++)
        {
            xySo[i][1] += 5;
        }
        tfPoint.x = xTab + 5;
        tfPoint.y = GameCanvas.h - 25;
        tfPoint.isFocus = false;

        tfPoint.width = wtab2 - 10;
        tfPoint.height = ITEM_HEIGHT + 2;
        tfPoint.setIputType(TField.INPUT_ALPHA_NUMBER_ONLY);
        //		tfPoint.doChangeToTextBox();
        tfPoint.setMaxTextLenght(40);
    }
	public int indexFocus = -1;
	public static int[][] xyTField = new int[8][];
	public static int[][] xySo = new int[8][];
	public static int[] point = new int[8];
	public int[] maptoSo = new int[]{2,3,4,5,0,1,7,6};

    public static void loadbegin(){
        for (int i = 0; i < xyTField.Length; i++)
			{
			 xyTField[i] = new int[2];
			}
         for (int i = 0; i < xySo.Length; i++)
			{
			 xySo[i] = new int[2];
			}
    }
	public void LoadImage()
	{
        bt_Plus_0 = GameCanvas.loadImage("/GuiNaruto/human/bt_Plus_0.png");
        imgnv = GameCanvas.loadImage("/GuiNaruto/human/imgNv.png");
        //		for(int i=0;i<25;i++)
        //		{
        //			arrayHuman[i]=GameCanvas.loadImage("/GuiNaruto/human/human_"+(i+1)+".png");
        //		}

    }
	public void init() {

	}

    private void PaintHuman(int x, int y, mGraphics g)
    {

        //		int wImage=Image.getWidth(arrayHuman[0]);
        //		int hImage=Image.getHeight(arrayHuman[0]);
        //		int index=0;
        //		for(int i=1;i<6;i++)
        //			for(int j=1;j<6;j++)
        //			{
        //				g.drawImage(arrayHuman[index],x+j*wImage,y+i*hImage,0);
        //				index++;
        //				
        //			}
    }
	
	
	private void drawline(mGraphics g){
		g.setColor(0xFFFFFF); /// 1
		g.drawLine(xTabInfo + 60, yTab+35+ GameCanvas.h / 5, xTabInfo+120, yTab+35+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); //// 2
		g.drawLine(xTabInfo + 140, yTab+35+ GameCanvas.h / 5, xTabInfo+200, yTab+35+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); /// 3
		g.drawLine(xTabInfo + 40, yTab+70+ GameCanvas.h / 5, xTabInfo+130, yTab+70+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); /// 4
		g.drawLine(xTabInfo + 40, yTab+82+ GameCanvas.h / 5, xTabInfo+130, yTab+82+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); /// 5
		g.drawLine(xTabInfo + 120, yTab+93+ GameCanvas.h / 5, xTabInfo+200, yTab+93+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); /// 6
		g.drawLine(xTabInfo + 40, yTab+100+ GameCanvas.h / 5, xTabInfo+130, yTab+100+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); /// 7
		g.drawLine(xTabInfo + 120, yTab+115+ GameCanvas.h / 5, xTabInfo+200, yTab+115+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); /// 8
		g.drawLine(xTabInfo + 140, yTab+78+ GameCanvas.h / 5, xTabInfo+200, yTab+78+ GameCanvas.h / 5);
	}
	
	private void pointGate(mGraphics g){
		g.setColor(0XFFFFFF); // 1
		g.fillRect(xTabInfo + 50, yTab+30+ GameCanvas.h / 5, 10, 10);
		mFont.tahoma_7_green.drawString(g, "10", xTabInfo + 50, yTab+30+ GameCanvas.h / 5, mGraphics.HCENTER|mGraphics.BOTTOM);
		g.setColor(0XFFFFFF); // 2
		g.fillRect(xTabInfo+200, yTab+25+ GameCanvas.h / 5, 10, 10);
		mFont.tahoma_7_green.drawString(g, "10", xTabInfo+205, yTab+30+ GameCanvas.h / 5, mGraphics.HCENTER|mGraphics.BOTTOM);
		g.setColor(0XFFFFFF); // 3
		g.fillRect(xTabInfo + 40,  yTab+70+ GameCanvas.h / 5, 10, 10);
		mFont.tahoma_7_green.drawString(g, "10", xTabInfo + 40,  yTab+70+ GameCanvas.h / 5, mGraphics.HCENTER|mGraphics.BOTTOM);
		g.setColor(0XFFFFFF); // 4
		g.fillRect(xTabInfo + 40, yTab+82+ GameCanvas.h / 5, 10, 10);
		g.setColor(0XFFFFFF); // 5
		g.fillRect(xTabInfo+200, yTab+93+ GameCanvas.h / 5, 10, 10);
		g.setColor(0XFFFFFF); // 6
		g.fillRect(xTabInfo + 40,  yTab+100+ GameCanvas.h / 5, 10, 10);
		g.setColor(0XFFFFFF); // 7
		g.fillRect(xTabInfo+200, yTab+115+ GameCanvas.h / 5, 10, 10);
		g.setColor(0XFFFFFF); // 8
		g.fillRect(xTabInfo+200, yTab+78+ GameCanvas.h / 5, 10, 10);
		
	}
	public static TField tfPoint;
	private void updatePoiter(){
        for (int i = 0; i < xyTField.Length; i++)
        {
            if (GameCanvas.isPointSelect(xyTField[i][0], xyTField[i][1], 20, 20))
            {
                indexFocus = i;
                tfPoint.name = info[indexFocus];
                tfPoint.setTextBox();
                GameCanvas.isPointerJustRelease = false;
            }
        }

    }
	
	public override void updateKey(){
        updatePoiter();
        tfPoint.update();
        infoPaintChar.updateKey();
        infoPaintChar.updatecm();
    }
    public override void  keypress(int keyCode)
    {
        if (tfPoint.isFocus)
            tfPoint.keyPressed(keyCode);
        base.keypress(keyCode);
    }
	public override void keyPress(int keyCode) {

	}
	public override void update() {
        // TODO Auto-generated method stub
        if (imgnv == null)
        {
            LoadImage();
        }
        if (tfPoint.isFocus)
        {
            tfPoint.update();
        }
        if (tfPoint.isOKReturn)
        {
            tfPoint.isOKReturn = false;
            int diem = 0;
            try
            {
                diem = int.Parse(tfPoint.getText());
                Service.gI().requestADD_BASE_POINT((sbyte)indexFocus, (short)diem);
                tfPoint.setText("");
            }
            catch (Exception e)
            {
                // TODO: handle exception
            }
            point[indexFocus] += diem;
            indexFocus = 0;
        }
    }
	
	public override void paint(mGraphics g) {
        //PaintHuman(xTabInfo-10,yTab,g);
        //drawline(g);
        g.drawImage(imgnv, xTabInfo + 30, yTab + 42, mGraphics.TOP | mGraphics.LEFT);
        paintListTFied(g);
        //pointGate(g);
        if (tfPoint.isFocus)
            tfPoint.paint(g);
        //xTab-widthSubFrame-5,yTab+10
        infoPaintChar.setStyle(9, 14, xTab - widthSubFrame - 5, yTab + 10, widthSubFrame - 10, heightSubFrame - 20, true, 1);
        infoPaintChar.setClip(g, xTab - widthSubFrame - 5, yTab + 10, widthSubFrame - 10, heightSubFrame - 20);
        mFont.tahoma_7_white.drawString(g, "Class: " + T.nameClass[Char.myChar().cClass], xTab - widthSubFrame - 5, yTab + 14, 0);

        for (int i = 0; i < infoText.Length; i++)
        {
            mFont.tahoma_7_white.drawString(g, infoText[i] + ": ", xTab - widthSubFrame - 5, yTab + 14 + 14 * (i + 1), 0);
            mFont.tahoma_7_green.drawString(g, Char.myChar().diemTN[maptoMon[i]] + "",
                    xTab - widthSubFrame - 5 + mFont.tahoma_7_white.getWidth(infoText[i] + ": "),
                    yTab + 14 + 14 * (i + 1), 0);
            if (Char.myChar().subTn != null && Char.myChar().subTn[maptoMon[i]] > 0)
                mFont.tahoma_7_blue.drawString(g, "   +" + Char.myChar().subTn[maptoMon[i]] + "",
                        xTab - widthSubFrame - 5 + mFont.tahoma_7_white.getWidth(infoText[i] + ": " + Char.myChar().diemTN[maptoMon[i]]),
                        yTab + 14 + 14 * (i + 1), 0);

        }
        GameCanvas.resetTrans(g);
    }
	public void paintListTFied(mGraphics g)
	{

        for (int i = 0; i < xyTField.Length; i++)
        {
            g.setColor(0x000000);
            g.fillRect(xyTField[i][0] - 5, xyTField[i][1], 30, 20);
            //ve duong line to o so
            g.setColor(0xfffffff);
            g.drawLine(xyTField[i][0] - 5, xyTField[i][1] + 21, xyTField[i][0] + 25, xyTField[i][1] + 21);
            if (i < 4)
                g.drawLine(xyTField[i][0] + 25, xyTField[i][1] + 21, xySo[maptoSo[i]][0] + xTabInfo + 30, xySo[maptoSo[i]][1] + yTab + 7 + 30);
            else
                g.drawLine(xySo[maptoSo[i]][0] + xTabInfo + 30, xySo[maptoSo[i]][1] + yTab + 7 + 30, xyTField[i][0] - 5, xyTField[i][1] + 21);
        }
        for (int i = 0; i < Char.myChar().diemTN.Length; i++)
        {
            //			g.drawImage(bt_Plus_0, xyTField[i][0]+10, xyTField[i][1]-10, mGraphics.VCENTER|mGraphics.HCENTER);
            g.drawImage(bt_Plus_0, xyTField[i][0] + 10, xyTField[i][1] + 2, mGraphics.VCENTER | mGraphics.HCENTER);


            mFont.tahoma_7_green.drawString(g, Char.myChar().diemTN[i] + "", xyTField[i][0] + 10, xyTField[i][1] + 7, 2);
            mFont.tahoma_6_white.drawString(g, info[i], xyTField[i][0] + 10, xyTField[i][1] - 10, 2);
        }
        mFont.tahoma_7.drawString(g, "Điểm: " + Char.myChar().totalTN, xTabInfo + 40, yTab + 32, 0);

    }
}
