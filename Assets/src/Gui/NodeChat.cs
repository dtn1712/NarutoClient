//package code.screen;
//
//import lib.Cout;
//import lib.TCanvas;
//import lib.mGraphics;
//import lib.mVector;
//import lib2.mFont;
//
//public class NodeChat {
//	public int type;
//	public const sbyte t_TEXT = 0;
//	public const sbyte t_EMO = 1;
//	public const sbyte t_TEXTEMO = 2;
//	
//	public string[][] textEmo;
//	public string[] text;
//	public string textdai,name;
//	public static string mtextConvertEmo = "zpzp";
//	public int hnode,wname;
//	public bool isMe;
//	public static int wnode = 100*TCanvas.TileZoom;
//	public mVector listEmoLine = new mVector();
//	
//	public static string[] maEmo = new string[]{ //14,
//			"z($)z","z:Oz","(highfive)","z8-)","z:Sz","(wait)","(oliver)","z(nod)","(facepalm)","z:^)",
//			"(muscle)","z:)z" ,"z|-)","(rofl)","(lala)","z(ske)","z:$z","z(n)z","z>o)","z;(z",
//			"(bandit)","z(y)z","z(ci)","z:?z","(giggle)","z(a)z","(makeup)","z>2)","(swear)","(wave)",
//			"zz(v)z","z(bow)","z(*)z","(emo)","z:]z","z:@z","(doh)","zz(6)","z:&z","(happy)",
//			"z;)z","|-(","z(t)z","(whew)","(rock)","z:|z","(yawn)",":P","(hande)","(tmi)z",
//			"(fingers)","z:Dz","(smirk)","(punch)","z:(z","z:*z","(wasntme)","8-|z","z::|","z:xz",
//			"(waiting)","(clap)","z(mm)","(headbang)"
//	};
//	
//	
//	public NodeChat(string textt, bool isMee,String namee)
//	{
//		this.textdai = textt;
//		this.isMe = isMee;
//		this.text  = mFont.tahoma_7_yellowsmall.splitFontArray((isMee==false?namee+": ":"")+textt, wnode);
//		this.type = t_TEXT;
//		int[] typetext = new int[text.Length];
//		if(text.Length>=1){
//			for (int m = 0; m < text.Length; m++) {
//				mVector listEmo = new mVector();
//				typetext[m] = t_TEXT;
//				for (int i = 0; i < maEmo.Length; i++) {
//					if(text[m].contains(maEmo[i])){
//						mVector nEmo = findAllEmoInText(text[m], maEmo[i], i);
//						for (int j = 0; j < nEmo.size(); j++) {
//							EmoText emo = (EmoText)nEmo.elementAt(j);
//							listEmo.addElement(emo);
//						}
//						typetext[m] = t_EMO;
//						if(this.type==t_TEXT)
//						this.type = t_EMO;
//					}
//				}
//				if(listEmo.size()>1)
//				listEmo = XepThuTuEmo(listEmo);
//				if(typetext[m] == t_EMO){
//					int lengEmo = 0;
//					for (int i = 0; i < listEmo.size(); i++) {
//						EmoText emo = (EmoText)listEmo.elementAt(i);
//						lengEmo+= maEmo[emo.loai].Length;
//					}
//					if(lengEmo<text[m].Length){
//						if(this.type==t_EMO)
//						this.type = t_TEXTEMO;
//						typetext[m] = t_TEXTEMO;
//						
//					}else if(lengEmo<text[m].Length){
//						if(this.type==t_TEXT)
//						this.type = t_EMO;
//						typetext[m] = t_EMO;
//					}
//				}
//			//	Cout.println(getClass(), m+"  typetext[m] "+typetext[m]);
//				if(typetext[m] == t_TEXTEMO){
//					string[] listext = slitFontWithEmo(text[m], listEmo);
//					EmoLineText emoline = new EmoLineText(t_TEXTEMO,listext, listEmo);
//					listEmoLine.addElement(emoline);
//				} else if(typetext[m] == t_TEXT){
//					string[] textline = new string[1];
//					textline[0] = text[m];
//					EmoLineText emoline = new EmoLineText(t_TEXT,textline, listEmo);
//					listEmoLine.addElement(emoline);
//				} 
//				else if(typetext[m] == t_EMO){
//					string[] textline = new string[1];
//					textline[0] = "";
//					EmoLineText emoline = new EmoLineText(t_EMO,textline, listEmo);
//					listEmoLine.addElement(emoline);
//				} 
//			}
//		}
//		bool[] isType = new bool[3];
//		for (int i = 0; i < typetext.Length; i++) {
//			if(typetext[i]==t_TEXT){
//				isType[t_TEXT] = true;
//			}
//			if(typetext[i]==t_EMO){
//				isType[t_EMO] = true;
//			}
//			if(typetext[i]==t_TEXTEMO){
//				isType[t_TEXTEMO] = true;
//			}
//		}
//		if(isType[t_TEXTEMO])
//			this.type = t_TEXTEMO;
//		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&!isType[t_EMO]){
//			this.type = t_TEXT;
//		}
//		else if(!isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
//			this.type = t_EMO;
//		}
//		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
//			this.type = t_TEXTEMO;
//		}
//
//	//	Cout.println(getClass(), t_TEXT+" "+isType[t_TEXT]+" "+ t_EMO+" "+isType[t_EMO]+" "+ t_TEXTEMO+" "+isType[t_TEXTEMO]);
//	//	Cout.println(getClass(), " this.type "+this.type);
//		if(this.text!=null)
//		this.hnode = mScreen.HSTRING*(this.text.Length)+mScreen.HSTRING/4;
//		this.name = namee;
//		wname = mFont.tahoma_7_yellow.getWidth(name);
//	}
//	public NodeChat(string textt, bool isMee,String namee,int wnodePainInfoGameScr)
//	{
//		this.textdai = textt;
//		this.isMe = isMee;
//		this.text  = mFont.tahoma_7_yellowsmall.splitFontArray((isMee==false?namee+": ":"")+textt, wnodePainInfoGameScr);
//		this.type = t_TEXT;
//		int[] typetext = new int[text.Length];
//		if(text.Length>=1){
//			for (int m = 0; m < text.Length; m++) {
//				mVector listEmo = new mVector();
//				typetext[m] = t_TEXT;
//				for (int i = 0; i < maEmo.Length; i++) {
//					if(text[m].contains(maEmo[i])){
//						mVector nEmo = findAllEmoInText(text[m], maEmo[i], i);
//						for (int j = 0; j < nEmo.size(); j++) {
//							EmoText emo = (EmoText)nEmo.elementAt(j);
//							listEmo.addElement(emo);
//						}
//						typetext[m] = t_EMO;
//						if(this.type==t_TEXT)
//						this.type = t_EMO;
//					}
//				}
//				if(listEmo.size()>1)
//				listEmo = XepThuTuEmo(listEmo);
//				if(typetext[m] == t_EMO){
//				int lengEmo = 0;
//					for (int i = 0; i < listEmo.size(); i++) {
//						EmoText emo = (EmoText)listEmo.elementAt(i);
//						lengEmo+= maEmo[emo.loai].Length;
//					}
//					if(lengEmo<text[m].Length){
//						if(this.type==t_EMO)
//						this.type = t_TEXTEMO;
//						typetext[m] = t_TEXTEMO;
//						
//					}else if(lengEmo<text[m].Length){
//						if(this.type==t_TEXT)
//						this.type = t_EMO;
//						typetext[m] = t_EMO;
//					}
//				}
//				if(typetext[m] == t_TEXTEMO){
//					string[] listext = slitFontWithEmo(text[m]+"   ", listEmo);
//					EmoLineText emoline = new EmoLineText(t_TEXTEMO,listext, listEmo);
//					listEmoLine.addElement(emoline);
//				} else if(typetext[m] == t_TEXT){
//					string[] textline = new string[1];
//					textline[0] = text[m];
//					EmoLineText emoline = new EmoLineText(t_TEXT,textline, listEmo);
//					listEmoLine.addElement(emoline);
//				} 
//				else if(typetext[m] == t_EMO){
//					string[] textline = new string[1];
//					textline[0] = "";
//					EmoLineText emoline = new EmoLineText(t_EMO,textline, listEmo);
//					listEmoLine.addElement(emoline);
//				} 
//			}
//		}
//		bool[] isType = new bool[3];
//		for (int i = 0; i < typetext.Length; i++) {
//			if(typetext[i]==t_TEXT){
//				isType[t_TEXT] = true;
//			}
//			if(typetext[i]==t_EMO){
//				isType[t_EMO] = true;
//			}
//			if(typetext[i]==t_TEXTEMO){
//				isType[t_TEXTEMO] = true;
//			}
//		}
//		if(isType[t_TEXTEMO])
//			this.type = t_TEXTEMO;
//		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&!isType[t_EMO]){
//			this.type = t_TEXT;
//		}
//		else if(!isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
//			this.type = t_EMO;
//		}
//		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
//			this.type = t_TEXTEMO;
//		}
//		if(this.text!=null)
//		this.hnode = mScreen.HSTRING*(this.text.Length)+mScreen.HSTRING/4;
//		this.name = namee;
//		wname = mFont.tahoma_7_yellow.getWidth(name);
//	}
////	posNV[4] + 4 +g.getImageWidth(imgTheBai[1])
//	public void paint(mGraphics g,int x,int y){
//		switch (type) {
//		case t_TEXT:
//			for (int j = 0; j < text.Length; j++) {
//				mFont.tahoma_7_yellowsmall.drawString(g, text[j],x,y+j*mScreen.HSTRING+1, 0,true);
//			}
//			break;
//		case t_TEXTEMO:
//			for (int i = 0; i < listEmoLine.size(); i++) {
//				EmoLineText emoline = (EmoLineText)listEmoLine.elementAt(i);
//				switch (emoline.loai) {
//				case t_TEXTEMO:
//					int indexemo=0;
//					for (int j = 0; j < emoline.listtext.Length; j++) {
//						if(emoline.listtext[j].Equals(mtextConvertEmo))
//						{
////						//	Cout.println(getClass(), " index Emove "+indexemo);
//							EmoText emo = (EmoText)emoline.listEmo.elementAt(indexemo);
//							try {
//								indexemo++;
//								g.drawImage(MainTabScreen.imgEmo[emo.loai],
//										emoline.wlist[j]+x,
//										y+i*mScreen.HSTRING+1, 0,true);
//							} catch (Exception e) {
//								// TODO: handle exception
//							}
//						}
//						else 
//						mFont.tahoma_7_yellowsmall.drawString(g,emoline.listtext[j],emoline.wlist[j]
//								/*(j>0?emoline.wlist[j-1]+g.getImageWidth(MainTabScreen.imgEmo[0])*(j):0)*/+  x,y+i*mScreen.HSTRING+1, 0,true);
//					}
////					for (int j = 0; j < emoline.wlist.Length; j++) {
////						
////					}
////					for (int j = 0; j < emoline.listtext.Length; j++) {
////						mFont.tahoma_7_yellowsmall.drawString(g,emoline.listtext[j],
////								(j>0?emoline.wlist[j-1]+g.getImageWidth(MainTabScreen.imgEmo[0])*(j):0)+  x,y+i*mScreen.HSTRING+1, 0,true);
////					}
////					for (int j = 0; j < emoline.listEmo.size(); j++) {
////						EmoText emo = (EmoText)emoline.listEmo.elementAt(j);
////						try {
////							g.drawImage(MainTabScreen.imgEmo_small[emo.loai],
////									emoline.wlist[j]+x+2*j,
////									y+i*mScreen.HSTRING+1, 0,true);
////						} catch (Exception e) {
////							// TODO: handle exception
////						}
////					}
//				break;
//				case t_TEXT:
//					mFont.tahoma_7_yellowsmall.drawString(g,emoline.listtext[0],x,y+i*mScreen.HSTRING+1, 0,true);
//					break;
//				case t_EMO:
//					for (int j = 0; j < emoline.listEmo.size(); j++) {
//						EmoText emo = (EmoText)emoline.listEmo.elementAt(j);
//						g.drawImage(MainTabScreen.imgEmo[emo.loai],x+
//								(j*mGraphics.getImageWidth(MainTabScreen.imgEmo[0])),
//								y+i*mScreen.HSTRING+1, 0,true);
//					}
//					break;
//				default:
//					break;
//				}
//			}
//			break;
//		case t_EMO:
//			for (int i = 0; i < listEmoLine.size(); i++) {
//				EmoLineText emoline = (EmoLineText)listEmoLine.elementAt(i);
//				for (int j = 0; j < emoline.listEmo.size(); j++) {
//					EmoText emo = (EmoText)emoline.listEmo.elementAt(j);
//					g.drawImage(MainTabScreen.imgEmo[emo.loai],
//							x+wnode-emoline.listEmo.size()*mGraphics.getImageWidth(MainTabScreen.imgEmo[0])-mScreen.HSTRING/2+(j*mGraphics.getImageWidth(MainTabScreen.imgEmo[0])),
//							y+i*mScreen.HSTRING+1, 0,true);
//				}
//			}
//			break;
//		default:
//			break;
//		}
//		
//	}
//	
//	public string[] slitFontWithEmo(string text,mVector nEmo){
//		mVector listtext = new mVector();
//		for (int i = 0; i < nEmo.size(); i++) {
//			EmoText emo = (EmoText)nEmo.elementAt(i);
//			text = text.replace(maEmo[emo.loai], mtextConvertEmo+" ");
//		}
//		string[] mangcat = text.split(mtextConvertEmo);	
//
////		for (int i = 0; i < nEmo.size(); i++) {
////			EmoText emo = (EmoText)nEmo.elementAt(i);
////			string dem = text.substring(indexcat, emo.vitri);
////			if(dem!=null){
////				listtext.addElement(dem.Length==0?mtextConvertEmo:dem);
////				indexcat = emo.vitri+maEmo[emo.loai].Length;
////			}
////			
////			if(indexcat==0&&emo.vitri==0){
////				listtext.addElement("");
////				indexcat = emo.vitri+maEmo[emo.loai].Length;
////			}
////			if(i==nEmo.size()-1){
////				int lencat = text.Length-(emo.vitri+maEmo[emo.loai].Length);
////				if(lencat>0){
////					string textend = text.substring(emo.vitri+maEmo[emo.loai].Length,emo.vitri+maEmo[emo.loai].Length+ lencat);
////					listtext.addElement(textend);
////				}
////			}
////		}
//		if(mangcat==null) return null;
//		else {
//			int nemo = nEmo.size();
//			for (int i = 0; i < mangcat.Length; i++) {
//				if(!(i==0&&mangcat[i].Length==0)){
//					listtext.addElement(mangcat[i]);
//					if(nemo>0){
//						nemo--;
//						listtext.addElement(mtextConvertEmo);
//					}
//				}
//				else {
//					if(nemo>0){
//						nemo--;
//						listtext.addElement(mtextConvertEmo);
//					}
//				}
//			}
//		}
//		string[] chuoitextt = new string[listtext.size()];
//		for (int i = 0; i < listtext.size(); i++) {
//			chuoitextt[i] = (string)listtext.elementAt(i);
//		}
//		return chuoitextt;
//	}
//	
//	public mVector findAllEmoInText(string text,String maemo,int loaiemo){
//		int index = 0;
//		mVector nemo = new mVector();
//		for (int i = 0; i < text.Length; i++) {
//			if(text[i)==maemo[0)&&i+maemo.Length<=text.Length){
//				bool isMa = true;
//				try {
//					for (int j = 0; j < maemo.Length; j++) {
//						if(maemo[j)!=text[i+j)){
//							isMa = false;
//						}
//					}
//				} catch (Exception e) {
//					// TODO: handle exception
//				//	Cout.println(getClass(), e.getMessage());
//				}
//				
//				if(isMa){
//					index = maemo.Length;
//					nemo.addElement(new EmoText(loaiemo,i));
//					i +=index-1;
//				}
//			}
//		}
//		return nemo;
//	}
//	public mVector XepThuTuEmo(mVector listEmo){
//		int vitri=0,loai=0;
//		for (int i = 0; i < listEmo.size()-1; i++) {
//			for (int j = i+1; j < listEmo.size(); j++) {
//				EmoText emoI =(EmoText)listEmo.elementAt(i);
//				EmoText emoJ =(EmoText)listEmo.elementAt(j);
//				if(emoI.vitri>emoJ.vitri){
//					vitri = emoI.vitri;
//					loai = emoI.loai;
//					//
//					emoI.loai = emoJ.loai;
//					emoI.vitri = emoJ.vitri;
//					
//					emoJ.loai = loai;
//					emoJ.vitri = vitri;
//				}
//				
//			}
//		}
//		return listEmo;
//	}
//}
