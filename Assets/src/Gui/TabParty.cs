

using System;
public class TabParty : Screen , IActionListener {
	public Command kich,giaitan,roi;
	
	public override void updateKey() {
        // TODO Auto-generated method stub
        if (kich != null && Party.gI().isLeader && Party.vCharinParty.size() >= 2)
        {
            if (getCmdPointerLast(kich))
            {
                kich.performAction();
            }
        }
        if (giaitan != null && Party.gI().isLeader && Party.vCharinParty.size() >= 2)
        {
            if (getCmdPointerLast(giaitan))
            {
                giaitan.performAction();
            }
        }
        if (roi != null && Party.vCharinParty.size() >= 2)
        {
            if (getCmdPointerLast(roi))
            {
                roi.performAction();
            }
        }

        ScrollResult s1 = scrMain.updateKey();
        base.updateKey();
    }
	public static Scroll scrMain = new Scroll();
	int x,y;
	public  int xstart, ystart, popupW = 163, popupH = 160, cmySK, cmtoYSK, cmdySK, cmvySK, cmyLimSK;
	public  int popupY, popupX, isborderIndex, indexRow=-1,widthGui=235,heightGui=240,indexSize=50,indexRowMax;
    public bool isLead = false;
	public override void update() {
        // TODO Auto-generated method stub
        //		if (hParty.size() == 0)
        //		Service.gI().requestinfoPartynearME(Type_Party.GET_INFOR_NEARCHAR, (short)Char.myChar().charID);
        if (GameCanvas.keyPressed[Key.NUM8])
        {
            indexRow++;
            if (indexRow >= GameScr.hParty.size())
                indexRow = GameScr.hParty.size() - 1;
            scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
        }
        else if (GameCanvas.keyPressed[Key.NUM2])
        {
            indexRow--;
            if (indexRow < 0)
                indexRow = 0;
            scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
        }
        scrMain.updatecm();

        if (GameScr.hParty.size() == 0 && scrMain.selectedItem != -1)
        {
            indexRow = scrMain.selectedItem;
            scrMain.selectedItem = -1;
            //		if(GameScr.hParty.size()==0){
            //			short PartyId = 0;
            //			short CharLeaderid = Char.myChar().idParty;
            //			byte membersize = 5;
            //			short[] charIDmeber =  new short[membersize];
            //			for(int i = 0; i < membersize; i++){
            //				charIDmeber[i] = (short)i;
            //			}
            //			GameScr.hParty.contains(PartyId+"");
            //			GameScr.hParty.put(PartyId+"",(Object)new Party(PartyId, charIDmeber, CharLeaderid));
            //		
            //		}
            //		Cout.println(getClass(), " iconContact.performAction() ");
            //		GameScr.gI().guiMain.menuIcon.iconContact.performAction();
        }
        else
            if (GameScr.hParty.size() != 0 && scrMain.selectedItem != -1)
            {
                indexRow = scrMain.selectedItem;
                scrMain.selectedItem = -1;
                setPartyCommand();
            }
        base.update();
    }
	public TabParty(int x,int y){
		this.x=GameCanvas.w/2-widthGui/2;//GameCanvas.w/2-GameScr.widthGui/2;
		this.y=y;
		this.popupX = x-widthGui/2;
		this.popupY = GameCanvas.h/2-heightGui/2;
		
	}
private void setPartyCommand() {
		
			Party party = (Party)GameScr.hParty.get(Char.myChar().idParty+"");
//		if(party == null)
//			return;
		if (indexRow == -1 )
			return;
		if(party != null){
					if(Char.myChar().charID == Party.idleader){
						 Char c = (Char) Party.vCharinParty.elementAt(indexRow);
                         if (c != null)
                         {
                             kich = new Command("Kích", this, 270192, null, 0, 0);
                             kich.setPos(popupX + popupW + loadImageInterface.btnTab.getWidth() - 10, popupY + loadImageInterface.btnTab.getHeight() * 2, loadImageInterface.btnTab, loadImageInterface.btnTab);
                             giaitan = new Command("Giải tán", this, 111111, null, 0, 0);
                             giaitan.setPos(popupX + popupW + loadImageInterface.btnTab.getWidth() - 10, popupY + loadImageInterface.btnTab.getHeight() * 2 + (Screen.cmdH * 4), loadImageInterface.btnTab, loadImageInterface.btnTab);
                             roi = new Command("Rời", this, 231291, null, 0, 0);
                             roi.setPos(popupX + popupW + loadImageInterface.btnTab.getWidth() - 10, popupY + loadImageInterface.btnTab.getHeight() * 2 + Screen.cmdH * 2, loadImageInterface.btnTab, loadImageInterface.btnTab);
                             giaitan.img = roi.img = kich.img = loadImageInterface.btnTab;
                             giaitan.imgFocus = roi.imgFocus = kich.imgFocus = loadImageInterface.btnTabFocus;
                             giaitan.w = roi.w = kich.w = Image.getWidth(loadImageInterface.btnTab);
                             giaitan.h = roi.h = kich.h = Image.getHeight(loadImageInterface.btnTabFocus);
                         }
                    }
                    else
                    {
                        kich = new Command("Kích", this, 270192, null, 0, 0);
                        kich.setPos(popupX + popupW + loadImageInterface.btnTab.getWidth() - 10, popupY + loadImageInterface.btnTab.getHeight() * 2, loadImageInterface.btnTab, loadImageInterface.btnTab);
                        giaitan = new Command("Giải tán", this, 111111, null, 0, 0);
                        giaitan.setPos(popupX + popupW + loadImageInterface.btnTab.getWidth() - 10, popupY + loadImageInterface.btnTab.getHeight() * 2 + (Screen.cmdH * 4), loadImageInterface.btnTab, loadImageInterface.btnTab);
                        roi = new Command("Rời", this, 231291, null, 0, 0);
                        roi.setPos(popupX + popupW + loadImageInterface.btnTab.getWidth() - 10, popupY + loadImageInterface.btnTab.getHeight() * 2 + Screen.cmdH * 2, loadImageInterface.btnTab, loadImageInterface.btnTab);
                        giaitan.img = roi.img = kich.img = loadImageInterface.btnTab;
                        giaitan.imgFocus = roi.imgFocus = kich.imgFocus = loadImageInterface.btnTabFocus;
                        giaitan.w = roi.w = kich.w = Image.getWidth(loadImageInterface.btnTab);
                        giaitan.h = roi.h = kich.h = Image.getHeight(loadImageInterface.btnTabFocus);
                    }
				//}
				
			} 
			else {
//				if(indexRow<GameScr.charnearByme.size()){
//					final Char c = (Char) GameScr.charnearByme.elementAt(indexRow);
//	//				if (c.charID != Char.myChar().charID) {
//						center = new Command(mResources.SELECT, 12009);
//	//				}
//				}
//			}
		}
		
	}
	public override void paint(mGraphics g) {
		// TODO Auto-generated method stub
        Paint.paintFrameNaruto(popupX, popupY, widthGui, heightGui, g, true);

        xstart = popupX + 5;
        ystart = popupY + 40;

        if (GameScr.hParty.size() == 0)
        {
            //			if(GameScr.charnearByme.size() == 0){
            //
            //				for(int i = 0; i < 15; i++){
            //					Char ch = new Char();
            //					ch.charID = (short)i;
            //					ch.cName = (i%2==0?"pkpro":"can1doithu")+" "+i;
            //					ch.idParty = (short)-1;
            //					GameScr.charnearByme.addElement(ch);
            //
            //				}
            //				
            mFont.tahoma_7_white.drawString(g, mResources.NOT_TEAM, popupX + widthGui / 2, popupY + 40, FontSys.CENTER);
            //			}else{
            //				g.setColor(0x001919);
            //				g.fillRect(xstart - 2, ystart - 2, widthGui - 6, indexSize * 5 + 8);

            //				resetTranslate(g);
            //				scrMain.setStyle(GameScr.charnearByme.size(), indexSize, xstart, ystart, widthGui - 3,heightGui-50, true, 1);
            //				scrMain.setClip(g, xstart, ystart, widthGui - 3, heightGui-50);
            //				indexRowMax = GameScr.charnearByme.size();
            //
            //				int yBGFriend =0;
            //				for(int i = 0; i < GameScr.charnearByme.size(); i++){
            //					Char c = (Char) GameScr.charnearByme.elementAt(i);
            //					Paint.PaintBGListQuest(xstart +35,ystart+yBGFriend,160,g);//new quest	
            //					if (indexRow == i) {
            //						Paint.PaintBGListQuestFocus(xstart +35,ystart+yBGFriend,160,g);
            ////						g.setColor(Paint.COLORLIGHT);
            ////						g.fillRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 20, indexSize - 4);
            ////						g.setColor(0xffffff);
            ////						g.drawRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 20, indexSize - 4);
            //					} 
            ////					else {
            ////						g.setColor(Paint.COLORBACKGROUND);
            ////						g.fillRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 20, indexSize - 4);
            ////						g.setColor(0xd49960);
            ////						g.drawRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 20, indexSize - 4);
            //
            //					yBGFriend+=50;
            ////					}
            //					if(c.idParty == -1){
            //						mFont.tahoma_7_white.drawString(g, c.cName, xstart + widthGui/2, ystart + i * indexSize + indexSize / 2 - 6, 2,true);
            //					}else{
            //						mFont.tahoma_7_red.drawString(g, c.cName, xstart + widthGui/2, ystart + i * indexSize + indexSize / 2 - 6, 2,true);
            //					}
            //				}
            //				GameCanvas.resetTrans(g);
            //			}
        }
        else
        {
            //			g.setColor(0x001919);
            //			g.fillRect(xstart - 2, ystart - 2, widthGui - 6, indexSize * 5 + 8);

            resetTranslate(g);
            scrMain.setStyle(Party.vCharinParty.size(), 50, xstart, ystart, widthGui - 3, heightGui - 50, true, 1);
            scrMain.setClip(g, xstart, ystart, widthGui - 3, heightGui - 50);
            //			Party party = (Party) hParty.get(Char.myChar().idParty+"");
            //			if(party != null){
            int yBGFriend = 0;
            indexRowMax = Party.vCharinParty.size();
            for (int i = 0; i < Party.vCharinParty.size(); i++)
            {
                Char c = (Char)Party.vCharinParty.elementAt(i);
                //					if (indexRow == i) {
                //						g.setColor(Paint.COLORLIGHT);
                //						g.fillRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 20, indexSize - 4);
                //						g.setColor(0xffffff);
                //						g.drawRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 20, indexSize - 4);
                //					} else {
                //						g.setColor(Paint.COLORBACKGROUND);
                //						g.fillRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 20, indexSize - 4);
                //						g.setColor(0xd49960);
                //						g.drawRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 20, indexSize - 4);
                //					}
                Paint.PaintBGListQuest(xstart + 35, ystart + yBGFriend, 160, g);//new quest	
                if (indexRow == i)
                {
                    //						g.setColor(Paint.COLORLIGHT);
                    //						g.fillRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
                    //						g.setColor(0xffffff);
                    //						g.drawRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
                    //						btnChat.paint(g);
                    //						btnUnfriend.paint(g);

                    Paint.PaintBGListQuestFocus(xstart + 35, ystart + yBGFriend, 160, g);
                }
                g.drawImage(loadImageInterface.charPic, xstart + 45, ystart + yBGFriend + 20, mGraphics.VCENTER | mGraphics.HCENTER);
                mFont.tahoma_7_white.drawString(g, "Level: " + c.clevel, xstart + 73, ystart + yBGFriend + 24, 0, true);
                if (c.isOnline)
                {
                    g.drawImage(loadImageInterface.imgName, xstart + 100, ystart + yBGFriend + 15, mGraphics.VCENTER | mGraphics.HCENTER, true);
                    mFont.tahoma_7_white.drawString(g, c.cName, xstart + 73, ystart + yBGFriend + 8, 0, true);
                }

                else
                {
                    g.drawImage(loadImageInterface.imgName, xstart + 100, ystart + yBGFriend + 15, mGraphics.VCENTER | mGraphics.HCENTER, true);
                    mFont.tahoma_7_white.drawString(g, c.cName, xstart + 73, ystart + yBGFriend + 8, 0, true);
                }

                if (c != null)
                {
                    if (c.isLeader)
                        mFont.tahoma_7_yellow.drawString(g, c.cName, xstart + 73, ystart + yBGFriend + 8, 0);
                    try
                    {
                        Part ph2 = GameScr.parts[c.head];
                        SmallImage.drawSmallImage(g, ph2.pi[Char.CharInfo[0][0][0]].id,
                                xstart + 44, ystart + yBGFriend + 20, 0, mGraphics.VCENTER | mGraphics.HCENTER);
                    }
                    catch (Exception e)
                    {
                        // TODO: handle exception
                    }
                    //						else
                    //							mFont.tahoma_7_red.drawString(g, "Hoahoa"/*c.cName*/, xstart + widthGui/2, ystart + i * indexSize + 20, 2);
                }

                yBGFriend += 50;
            }
        }

        GameCanvas.resetTrans(g);
        if (indexRow != -1 && Party.vCharinParty.size() >= 2)
        {
            if (Party.gI().isLeader)
            {
                if (kich != null) kich.paint(g);
                if (giaitan != null) giaitan.paint(g);
            }
            if (roi != null) roi.paint(g);
        }

        //paintNumCount(g);

        //	}

        //paint name box 
        Paint.PaintBoxName("DANH SÁCH NHÓM", popupX + widthGui / 2 - 50, popupY + 10, 100, g);
        //	}
        base.paint(g);
    }

	public  void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
	//Cout.println(getClass(), " iidAction "+idAction);
		switch (idAction) {
		case 111111: // giai tan
			Service.gI().removeParty(Type_Party.DISBAND_PARTY);
			break;
		case 270192: // kich ra khoi nhom
			Party party = (Party) GameScr.hParty.get(Char.myChar().idParty+"");
			if (((Char) Party.vCharinParty.elementAt(0)).charID == Char.myChar().charID) {
				if(indexRow<Party.vCharinParty.size()){
					 Char c = (Char) Party.vCharinParty.elementAt(indexRow);
					if (c.charID != Char.myChar().charID) {
						Service.gI().kickPlayeleaveParty((sbyte)Type_Party.KICK_OUT_PARTY, (short)c.charID);
	//					center = new Command(mResources.SELECT, 11080);
					}
				}
			}
			break;
		case 231291: // tu roi nhom
			Service.gI().leaveParty((sbyte)Type_Party.OUT_PARTY,(short)Char.myChar().charID);
			break;

		default:
			break;
		}
		
	}
	public void SetPosClose(Command cmdClose) {
		// TODO Auto-generated method stub
		scrMain.selectedItem = -1;
		cmdClose.setPos(popupX+widthGui-Image.getWidth(loadImageInterface.closeTab), popupY, loadImageInterface.closeTab, loadImageInterface.closeTab);
		
	}
}