

/*
 * this equip
 */
using System;
public class TabMySeftNew : MainTabNew
{
    int numW, numH, coutFc;
	public static int maxSize = 12;
	int h12, w5;
	public static int delta = 0;
	sbyte idSelect;
	// info
	int xStart, yStart;
	int[] mColorInfo;
	// list item
	mVector vecItemMenu = new mVector();
	public static string[] meffskill = new string[5];
	bool isList = false;
	int maxList, selectList, xList, yList, timeUpdateInfo;
	bool isShowInfo = false;
	
	private mBitmap[] arrayBGHuman=new mBitmap[25];
	private mBitmap imgButton_dressed,imgButton_dressed2,
	imgChien_luc,imgCoins1,imgCoins2,imgExp_tube,imgExp,imgLevel;

    int xBGHuman = MainTabNew.gI().xTab + MainTabNew.gI().widthFrame / 6 - 10;
    int yBGHuman = MainTabNew.gI().yTab + GameCanvas.h / 6 - 10;

	int wsize;
	
	int widthBGChar=130;
	int heightBGChar=100;
	
	Command dropAllItem,shop,improve;
	
	public TabMySeftNew(string nametab) {
		xBGHuman=xTab+16;
//		LoadImage();
		if (GameCanvas.isTouch)
			idSelect = -1;
		else
			idSelect = 0;
		xStart = xBegin + wblack / 2 - (numW * wOneItem) / 2 +(numW / 2);
		yStart = yBegin +10;
		wsize = wOneItem;
		typeTab = EQUIP;
		yBGHuman=yTab+15;
		this.nameTab = nametab;
		int xCmd=xBGHuman+widthBGChar+32;
		int yCmd=yBGHuman+27;
		
		dropAllItem= new Command("", this, Contans.BTN_USE_ITEM,null, 0,0);
		dropAllItem.setPos(xCmd, yCmd,imgButton_dressed,imgButton_dressed2);
		
		yCmd=yCmd+30;
		shop= new Command("", this, Contans.BTN_USE_ITEM,null, 0,0);
		shop.setPos(xCmd, yCmd,imgButton_dressed,imgButton_dressed2);
		
		yCmd=yCmd+30;
		improve= new Command("", this, Contans.BTN_USE_ITEM,null, 0,0);
		improve.setPos(xCmd, yCmd,imgButton_dressed,imgButton_dressed2);
		
	}
	public void init() {
        // TODO Auto-generated method stub
        //		if (GameCanvas.isTouch)
        //			idSelect = -1;
        //		else
        //			idSelect = 0;
        //		isList = false;
    }
	public void LoadImage()
	{
		for(int i=0;i<25;i++)
		{
			arrayBGHuman[i]=GameCanvas.loadImage("/GuiNaruto/imageBGChar/bgCharacter_01_"+(i+1)+".png");
		}
		imgButton_dressed=GameCanvas.loadImage("/GuiNaruto/myseft/button_dressed.png");
		imgButton_dressed2=GameCanvas.loadImage("/GuiNaruto/myseft/button_dressed2.png");;
		imgChien_luc=GameCanvas.loadImage("/GuiNaruto/myseft/chien_luc.png");
		imgCoins1=GameCanvas.loadImage("/GuiNaruto/myseft/coins1.png");
		imgCoins2=GameCanvas.loadImage("/GuiNaruto/myseft/coins2.png");
		imgExp_tube=GameCanvas.loadImage("/GuiNaruto/myseft/exp_tube.png");
		imgExp=GameCanvas.loadImage("/GuiNaruto/myseft/exp.png");
		imgLevel=GameCanvas.loadImage("/GuiNaruto/myseft/level.png");
		
	}
	
	//paint bg human
	private void PaintBGHuman(int x,int y,mGraphics g)
	{
		int wImage=Image.getWidth(arrayBGHuman[0]);
		int hImage=Image.getHeight(arrayBGHuman[0]);
		int index=0;
		for(int i=1;i<6;i++)
			for(int j=1;j<6;j++)
			{
				g.drawImage(arrayBGHuman[index],x+j*wImage,y+i*hImage,0);
				index++;
			}
	}
	
	//paint exp
	void PaintExp(int x,int y ,mGraphics g)
	{
        int Exppaint = (int)((Char.myChar().cEXP * mGraphics.getImageHeight(imgExp)) / Char.myChar().cMaxEXP);
        Exppaint = (Exppaint < 7 && Exppaint != 0 ? 7 : Exppaint);
        g.drawRegion(imgExp, mGraphics.getImageHeight(imgExp) - Exppaint, 0, Image.getWidth(imgExp),
                Exppaint,
                0, x + 3, y + 2 + (mGraphics.getImageHeight(imgExp) - Exppaint), mGraphics.TOP | mGraphics.LEFT);
        g.drawImage(imgExp_tube, x, y, 0);
        g.drawImage(imgLevel, x + Image.getWidth(imgExp) / 2 + 2, y + Image.getHeight(imgExp) + 10, mGraphics.VCENTER | mGraphics.HCENTER);
        mFont.tahoma_7b_white.drawString(g, Char.myChar().clevel + "", x + Image.getWidth(imgExp) / 2 - 1, y + Image.getHeight(imgExp) + 5, mGraphics.VCENTER | mGraphics.HCENTER);
    }
	
	void PaintItem(int x,int y ,mGraphics g)
	{
        //		g.drawImage(loadImageInterface.ImgItem,x,y-(loadImageInterface.ImgItem.getHeight()+2), 0);
        //		g.drawImage(loadImageInterface.ImgItem,x+(loadImageInterface.ImgItem.getWidth()+2)*6,y-(loadImageInterface.ImgItem.getHeight()+2), 0);
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 5; j++)
            {
                g.drawImage(loadImageInterface.ImgItem, x + (Image.getWidth(loadImageInterface.ImgItem)) * j, y + (Image.getHeight(loadImageInterface.ImgItem)) * i, 0);
            }

    }
	void PaintStrong(mGraphics g,int x,int y)
	{
        g.drawImage(imgChien_luc, x, y, mGraphics.VCENTER | mGraphics.HCENTER);
        mFont.tahoma_7b_white.drawString(g, "" + ((Char.myChar().diemTN[1] + Char.myChar().diemTN[2]) / 2), x, y - 2, 2);
    }
	void PaintGoldMoney(int x,int y ,mGraphics g)
	{
		
	}
	public override void paint(mGraphics g)
	{

        PaintBGHuman(xBGHuman - 16, yBGHuman, g);
        PaintExp(xBGHuman - 6, yBGHuman + 22, g);//paint exp
        PaintStrong(g, xBGHuman + 10 + widthBGChar / 2, yBGHuman + 20 + heightBGChar + 3);
        PaintItem(xBGHuman, yBGHuman + heightBGChar + Image.getHeight(imgChien_luc) + 10, g);

        int pointPk = 100;
        int PointSucKhoe = 200;

        //		g.setColor(color[1]);
        //		g.fillRect(xBegin + w5 * 2 + delta, yBegin + h12 / 4, 1, h12 * 8
        //				- h12 / 2);

        //		mFont.tahoma_7b_green.drawString(g, "Pk: " + pointPk,
        //				xBegin + 4, yBegin + h12 * 6 - h12 / 2 + 4, 0);
        //		mFont.tahoma_7b_red.drawString(g, T.suckhoe
        //				+ PointSucKhoe, xBegin + 4, yBegin + h12 * 6
        //				- h12 / 2 + GameCanvas.hText + 4, 0);

        // paint 12 item trang bi
        //		g.setColor(color[4]);
        try
        {
            for (byte i = 0; i < 10; i++)
            {// paint 12 item trang bi
                int xp = ((xBGHuman) + i % 5 * (24)) + ((i % 5 * 4) + 1), yp = ((yBGHuman + heightBGChar + Image.getHeight(imgChien_luc) + 10) + i / 5
                        * (28)) + 1;

                if (Char.myChar().arrItemBody != null)
                {
                    Item item = (Item)Char.myChar().arrItemBody[i];
                    if (item != null)
                    {
                        if (item.template.id > -1)
                        { // paints trang bi
                            item.paintItem(g, xp + wOneItem / 2 + 1, yp + wOneItem / 2 + 1);
                        }

                    }
                }
                if (idSelect > -1 && Focus == INFO && i == idSelect)
                {//focus
                    setPaintInfo();
                    if (itemFocus != null)
                        Paint.paintFocus(g,
                                (xBGHuman + ((idSelect % 5) * (Image.getWidth(loadImageInterface.ImgItem)))) + 4,
                                yBGHuman + heightBGChar + Image.getHeight(imgChien_luc) + 12
                                + (idSelect / 5) * (Image.getHeight(loadImageInterface.ImgItem)) + 2
                                , loadImageInterface.ImgItem.getWidth() - 9, loadImageInterface.ImgItem.getWidth() - 9, coutFc);
                    //					 	if(GameCanvas.gameTick % 10 == 0)
                    //							g.drawImage(loadImageInterface.imgFocusSelectItem1,
                    //									(xBGHuman+ ((idSelect % 5)*(Image.getWidth(loadImageInterface.ImgItem))))+2,
                    //									yBGHuman+heightBGChar+Image.getHeight(imgChien_luc)+10
                    //									+ (idSelect / 5) * (Image.getHeight(loadImageInterface.ImgItem) )+2 , 0);
                    //					 	else
                    //					 		g.drawImage(loadImageInterface.imgFocusSelectItem0,
                    //									(xBGHuman+ ((idSelect % 5)*(Image.getWidth(loadImageInterface.ImgItem))))+2,
                    //									yBGHuman+heightBGChar+Image.getHeight(imgChien_luc)+10
                    //									+ (idSelect / 5) * (Image.getHeight(loadImageInterface.ImgItem) )+2 , 0);
                    Paint.paintItemInfo(g, itemFocus, xTab - widthSubFrame - 5, yTab + 10);
                    //						Paint.SubFrame(xTab-widthSubFrame,yTab-30+ GameCanvas.h / 5,widthSubFrame,heightSubFrame, g);
                    //						cmdSelect.paint(g);
                    //						cmdXoaItem.paint(g);
                }
            }


        }
        catch (Exception e)
        {
           // e.printStackTrace();
        }

        //		if (Focus == INFO && idSelect >= 0) {// paint select
        //			int xp = xStart + idSelect % numW * (wsize), yp = yStart + idSelect
        //			/ numW * (wsize);
        ////			setPaintInfo();
        //		paintPopupContent(g, false);
        //		g.drawRect(xp, yp, (wsize), (wsize));
        //		g.setColor(color[2]);
        //		g.drawRect(xp + 1, yp + 1, (wsize) - 2, (wsize) - 2);
        //		
        //		}

        //paint background		
        //left col
        //		for(int i=0;i<5;i++)
        //		{
        //			g.drawImage(loadImageInterface.ImgItem,xTab + width/8+(loadImageInterface.ImgItem.getWidth()+2),yBegin-5+(loadImageInterface.ImgItem.getHeight()+2)*i, 0);
        //		}
        //		
        //		//right col
        //		for(int i=0;i<5;i++)
        //		{
        //			g.drawImage(loadImageInterface.ImgItem,xTab + width*4/8+(loadImageInterface.ImgItem.getWidth()+2),yBegin-5+(loadImageInterface.ImgItem.getHeight()+2)*i, 0);
        //		}
        //		
        //		//bottom rows
        //		for(int i=2;i<4;i++)
        //		{
        //			g.drawImage(loadImageInterface.ImgItem,xTab + width/8+(loadImageInterface.ImgItem.getWidth()+5)*i,yBegin-5+(loadImageInterface.ImgItem.getHeight()+2)*4, 0);
        //		}

        //		GameScr.currentCharViewInfo.paintChar(g, xTab + width/2, yBegin + h12 * 5 / 2 + 50 );
        GameScr.currentCharViewInfo.paintChar(g, xBGHuman + 74, yBGHuman + 120);

        dropAllItem.paint(g);
        shop.paint(g);
        improve.paint(g);

    }
	
	public override void updatePointer() {
		bool ismove = false;
        if (GameCanvas.gameTick % 4 == 0)
        {
            coutFc++;
            if (coutFc > 2)
                coutFc = 0;
        }
		
		if(imgButton_dressed==null)
			LoadImage();
		if (isList) {

			if (listContent != null) {
				if (GameCanvas.isPoint(listContent.x, listContent.y,
						listContent.maxW, listContent.maxH)) {
					listContent.update_Pos_UP_DOWN();
					ismove = true;
				}
			}
//			if (GameCanvas.isTouch && !ismove) {
//				list.updatePos_LEFT_RIGHT();
//				GameScreen.cameraSub.xCam = list.cmx;
//			}
			if (GameCanvas.isPointerClick /*&& !ismove*/) {
				if (GameCanvas.isPoint(xList, yList, (wsize) * maxList,
						wOneItem)) {
					sbyte select = (sbyte) ((/*GameScreen.cameraSub.xCam*/
							+ GameCanvas.px - xList) / (wsize));
					if (select >= 0 && select < vecItemMenu.size()) {
//						if (select == selectList) {
//							cmdChangeEquip.perform();
//						} else {
//							selectList = select;
//							timePaintInfo = 0;
//						}
//						listContent = null;
					}
					GameCanvas.isPointerClick = false;
				} else if (!GameCanvas.isPoint(0, GameCanvas.h
						- GameCanvas.hCommand, GameCanvas.w,
						GameCanvas.hCommand)) {
//					cmdCloseChange.perform();
					GameCanvas.isPointerClick = false;
				}

			}
		} else {
			if (listContent != null) {
				if (GameCanvas.isPoint(listContent.x, listContent.y,
						listContent.maxW, listContent.maxH)) {
					listContent.update_Pos_UP_DOWN();
					ismove = true;
				}
			}
			if (GameCanvas.isPointSelect(xStart, yStart, (wsize) * numW,
					(wsize) * numH)
					&& !ismove) {
				GameCanvas.isPointerClick = false;
				sbyte select = (sbyte) ((GameCanvas.px - xStart) / (wsize) + ((GameCanvas.py - yStart) / (wsize))
						* numW);
				if (select >= 0 && select < maxSize) {
					if (select == idSelect) {
						setPaintInfo();
//						cmdChange.perform();
					} else {
						idSelect = select;
						timePaintInfo = 0;

					}
					listContent = null;
					if (MainTabNew.Focus != MainTabNew.INFO)
						MainTabNew.Focus = MainTabNew.INFO;
				}

			}
		}
		int x=xBGHuman;
		int y=yBGHuman+heightBGChar+Image.getHeight(imgChien_luc)+10;
		int w=(Image.getWidth(loadImageInterface.ImgItem))*7;
		int h=(Image.getHeight(loadImageInterface.ImgItem))*2;
		
		if (GameCanvas.isPointSelect(x,y,w,h))
		{
			
			//khoang cach 2 pixel giua cac o
			int row=(GameCanvas.px - x)/ (Image.getWidth(loadImageInterface.ImgItem));
			int col=(GameCanvas.py - y) / (Image.getHeight(loadImageInterface.ImgItem));
			
			 row=(GameCanvas.px - x-(row-1)*2)/ (Image.getWidth(loadImageInterface.ImgItem));
			 col=(GameCanvas.py - y-(col-1)*2) / (Image.getHeight(loadImageInterface.ImgItem));
			
			if(row>6)
				row=6;
			if(col>1)
				col=1;

            idSelect = (sbyte)(row + col * 5);
			GameCanvas.clearKeyPressed();
			GameCanvas.clearKeyHold();
			
		}

		
	}
	
	Item itemFocus = null;
	public void setPaintInfo() {
//		Item item = Char.myChar().arrItemBody[idSelect];
		itemFocus = Char.myChar().arrItemBody[idSelect];
//		name = item.template.name;

	}
	
}
