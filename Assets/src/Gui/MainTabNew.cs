﻿using System;
public class MainTabNew : Screen , IActionListener {
    public sbyte typeTab;
    public static sbyte maxTypeTab = 12;
    public const sbyte INVENTORY = 0;
    public const sbyte EQUIP = 1;
    public const sbyte MY_INFO = 2;
    public const sbyte SKILLS = 3;
    public const sbyte QUEST = 4;
    public const sbyte CHAT = 5;
    public const sbyte GOLD = 6;
    public const sbyte CONFIG = 7;
    public const sbyte SHOP = 8;
    public const sbyte CHEST = 9;
    public const sbyte REBUILD = 10;
    public const sbyte FUNCTION = 11;
    public const sbyte CLAN_INVENTORY = 12;
    // 
    public const sbyte COLOR_WHITE = 0;
    public const sbyte COLOR_BLUE = 1;
    public const sbyte COLOR_YELLOW = 2;
    public const sbyte COLOR_VIOLET = 3;
    public const sbyte COLOR_ORANGE = 4;
    public const sbyte COLOR_GREEN = 5;
    public const sbyte COLOR_RED = 6;
    public const sbyte COLOR_BLACK = 7;
    public int widthFrame = 237, heightFrame = 230, widthSubFrame = 145, heightSubFrame = 230;
    //	public int widthFrame=205,heightFrame=200,widthSubFrame=140,heightSubFrame=200;
    public int xBegin, yBegin;//vi tri bat bau de paint thong tin 1 tab duoc select
    public string nameTab = "";
    //xTab: toa do x menu, yTab+GameCanvas.h/5: toa do y menu
   	public int xTab, yTab, wSmall, hSmall, numWSmall, numHSmall, xMoney,wtab2=176,xSub,
    yMoney, xChar, yChar, sizeFocus, numWBlack, numHBlack;
    public static int wbackground, hbackground;
    public static int longwidth = 0, xlongwidth, ylongwidth;
    public static int timeRequest = 15;
    public static int wblack, hblack, hMaxContent;
    public static sbyte wOneItem = 20, wOne5;//width of 1 item,width 1 item device 5
    public static bool is320 = false;
    public static sbyte Focus = 0;
    public const sbyte TAB = 0;
    public const sbyte INFO = 1;
    public static int timePaintInfo;
    public static MainTabNew instance;

    public string[] mContent, mSubContent, mPlusContent = null;
    public int[] mcolor, mSubColor, mPlusColor;
    public string name;
    public int wContent, colorName;
    public int xCon, yCon;

    iCommand cmdBack;
    Paint paint;

    public ListNew listContent = null;
    mBitmap imgStarRebuild;
    string[] cNameMenu = { "HÀNH TRANG", "TRANG BỊ", "NHÂN VẬT", "Kỹ năng" };

    int width = 256, height = 256;

    public static mBitmap[] imgTab = new mBitmap[14];
    public static MainTabNew gI()
    {
        if (instance == null)
        {
            instance = new MainTabNew();
        }
        return instance;
    }


    public MainTabNew()
    {
        paint = new Paint();
        //xet do rong cua item
        if (GameCanvas.isTouch)
        {
            wOneItem = 24;
        }
        else if (GameCanvas.w >= 240)
        {
            wOneItem = 24;
        }
        if (GameCanvas.h < 240 && wOneItem > 24)
            wOneItem = 24;


        hMaxContent = GameCanvas.h - GameCanvas.hCommand * 2;
        wOne5 = (sbyte)(wOneItem / 5);
        wbackground = GameCanvas.w / 32 + 1;
        hbackground = GameCanvas.h / 32 + 1;
        int tw = GameCanvas.w / wOneItem;
        if (tw > 9)
            tw = 9;
        int htam = (GameCanvas.h / 5 * 4 - GameCanvas.hCommand / 2);
        if (GameCanvas.isTouch)
            htam += GameCanvas.hCommand / 2;
        int th = htam / wOneItem;
        if (th > 8)
            th = 8;
        wSmall = (tw - 1) * wOneItem - (wOne5 * 3)
                + wOne5;//(GameCanvas.isSmallScreen ? wOne5 : 0);// khung nhá»�
        hSmall = th * wOneItem + wOne5;
        if (hSmall % 2 != 0)
            hSmall -= 1;
        if (GameCanvas.isTouch)
        {
            if (GameCanvas.w >= 380)
            {
                longwidth = 170;
                timeRequest = 5;
                hMaxContent = hSmall - wOneItem - wOne5;
                xTab = (GameCanvas.w - (tw) * wOneItem - longwidth) / 2;
                xlongwidth = GameCanvas.w - xTab - (longwidth);
                ylongwidth = yTab + GameCanvas.h / 5;
            }
            else if (GameCanvas.w > 315)
            {
                is320 = true;
                //				if (LoginScreen.indexInfoLogin == 1)
                //					LoginScreen.indexInfoLogin = 2;
                tw = 8;
                wSmall = (tw - 1) * wOneItem - (wOne5 * 3);
                longwidth = 130;
                timeRequest = 5;
                hMaxContent = hSmall - wOneItem - wOne5;
                xlongwidth = GameCanvas.w - xTab - (longwidth) + 5;
                ylongwidth = yTab + GameCanvas.h / 5;
            }
        }

        if (is320)
        {
            xTab = -5;
            xlongwidth = xTab + wSmall + wOneItem + wOne5 * 2;
            longwidth = GameCanvas.w - xlongwidth;
        }
        else
        {
            xTab = (GameCanvas.w - (tw) * wOneItem - longwidth) / 2;
        }

        yTab = 0;
        numWSmall = wSmall / 32;
        numHSmall = hSmall / 32;
        wblack = (wSmall) / wOneItem * wOneItem;
        hblack = ((hSmall) / wOneItem - 1) * wOneItem;
        numWBlack = wblack / 32;
        numHBlack = hblack / 32;
        xMoney = GameCanvas.w - (xTab - 9) - 72;
        if (GameCanvas.isTouch)
        {
            if (xMoney > GameCanvas.w - 112)
                xMoney = GameCanvas.w - 112;
        }
        yMoney = 5;
        //		if (GameCanvas.isSmallScreen)
        //			yMoney = 2;
        xChar = 0;// xTab - 9;
        yChar = GameCanvas.h / 10 - 21;
        //		if (GameCanvas.isSmallScreen)
        //			yChar += 4;
        sizeFocus = wOne5 + wOneItem;
        if (sizeFocus > 32)
            sizeFocus = 32;
        int xpaint2khung = (GameCanvas.w / 2 - wtab2 / 2 - widthSubFrame / 2 - 20);
        xTab = (xpaint2khung < 0 ? widthSubFrame : xpaint2khung + widthSubFrame) + 5;
        xSub = xTab - widthSubFrame - 10;
        yTab = (GameCanvas.h / 2 - heightSubFrame / 2 < 0 ? 0 : GameCanvas.h / 2 - heightSubFrame / 2);
    }

    /*
     * Paint tab main manu
     */
    public void paintTab(mGraphics g, string nameTab, int idSelect,
            mVector vec, bool isClan)
    {

        int size = vec.size();
        if (GameCanvas.lowGraphic)
        {//GameCanvas.lowGraphic paint color for low graphic

            paintRectLowGraphic(g, 0, 0, GameCanvas.w, GameCanvas.h, 0);
            paintRectLowGraphic(g, xMoney, yMoney, 22 * 3, 32, 1);
            paintRectLowGraphic(g, xTab, yTab
                    , wSmall, hSmall, 1);

        }
        else
        {//paint image for highGraphis

            Paint.paintFrameNaruto(xTab, yTab, wtab2, heightSubFrame + 2, g);

            Paint.PaintBoxName(cNameMenu[idSelect], xTab + wtab2 / 2 - 100 / 2, yTab, 100, g);

            Paint.SubFrame(xSub, yTab, widthSubFrame, heightSubFrame, g);

            if (true)
                return;
            //			

            for (int i = 0; i < numWSmall; i++)
            {// background all menu
                for (int j = 0; j < numHSmall; j++)
                {
                    g.drawImage(imgTab[0], xTab + i * 32, yTab + GameCanvas.h / 5 + j * 32, 0);
                }
            }


            // background money
            for (int i = 0; i < 3; i++)
            {
                g.drawImage(imgTab[1], xMoney + 22 * i, yMoney, 0);
            }

            // background menu trong
            for (int i = 0; i <= numWSmall; i++)
            {
                for (int j = 0; j <= numHSmall; j++)
                {
                    if (i == 0 || i == numWSmall || j == numHSmall || j == 0)
                    {
                        if (i == numWSmall)
                        {
                            if (j == numHSmall)
                            {
                                g.drawImage(imgTab[1], xTab + wOneItem + wOne5
                                        * 2 + wSmall - 32, yTab + GameCanvas.h
                                        / 5 + hSmall - 32, 0);
                            }
                            else
                                g.drawImage(imgTab[1], xTab + wOneItem + wOne5
                                        * 2 + wSmall - 32, yTab + j * 32
                                        + GameCanvas.h / 5, 0);
                        }
                        else
                        {
                            if (j == numHSmall)
                            {
                                g.drawImage(imgTab[1], xTab + wOneItem + wOne5
                                        * 2 + i * 32, yTab + GameCanvas.h / 5
                                        + hSmall - 32, 0);
                            }
                            else
                                g.drawImage(imgTab[1], xTab + wOneItem + wOne5
                                        * 2 + i * 32, yTab + j * 32
                                        + GameCanvas.h / 5, 0);
                        }
                    }
                }
            }
        }

        AvMain.Font3dWhite(g, nameTab,
                xTab + wOneItem + wOne5 * 2 + wSmall / 2, yTab + GameCanvas.h
                        / 5 + wOneItem / 2 - 6, 2);
        //		GameScreen.infoGame.paintInfoPlayer(g, 0, 0, !GameCanvas.isSmallScreen,
        //				GameCanvas.isSmallScreen ? mFont.tahoma_7_black
        //						: mFont.tahoma_7_white);

        g.drawRegion(imgTab[4], 0, 0, 14, 14, 0, xMoney + 4, yMoney + 2, 0);//money
        g.drawRegion(imgTab[4], 0, 14, 14, 14, 0, xMoney + 4, yMoney + 17, 0);//gold
        if (isClan)
        {//clan
            //			PaintInfoGameScreen.fraEvent.drawFrame(10, xMoney - 12,
            //					yMoney + 10, 0, 3, g);
            //			if (GameScreen.player.myClan != null) {
            //				mFont.tahoma_7_white.drawString(g, MainItem
            //						.getDotNumber(GameScreen.player.myClan.coin),
            //						xMoney + 19, yMoney + 3, 0);
            //				mFont.tahoma_7_white.drawString(g, MainItem
            //						.getDotNumber(GameScreen.player.myClan.gold),
            //						xMoney + 19, yMoney + 18, 0);
            //			}
        }
        else
        {
            //			mFont.tahoma_7_white.drawString(g, MainItem
            //					.getDotNumber(GameScreen.player.coin), xMoney + 19,
            //					yMoney + 3, 0);
            //			mFont.tahoma_7_white.drawString(g, MainItem
            //					.getDotNumber(GameScreen.player.gold), xMoney + 19,
            //					yMoney + 18, 0);
        }

        //background focus tab
        if (GameCanvas.lowGraphic)
        {//low graphic
            paintRectLowGraphic(g, xTab + wOne5, yTab + GameCanvas.h / 5
                    + ((wOneItem) * idSelect), wOne5 + wOneItem, wOneItem, 1);
        }
        else
        {
            if (wOne5 + wOneItem > 32)
            {// nen focus trai tab
                g.drawRegion(imgTab[1], 0, 0, wOneItem, wOneItem, 0, xTab
                        + wOne5, yTab + GameCanvas.h / 5
                        + ((wOneItem) * idSelect), 0);
                g.drawRegion(imgTab[1], 0, 0, wOne5, wOneItem, 0, xTab + wOne5
                        + wOneItem, yTab + GameCanvas.h / 5
                        + ((wOneItem) * idSelect), 0);
            }
            else
            {
                g.drawRegion(imgTab[1], 0, 0, wOne5 + wOneItem, wOneItem, 0,
                        xTab + wOne5, yTab + GameCanvas.h / 5
                                + ((wOneItem) * idSelect), 0);
            }
        }

        // khung focus tab left
        if (!GameCanvas.isTouch && Focus == TAB)
        {

            g.setColor(color[3]);
            g.drawRect(xTab + wOne5 + 1, yTab + GameCanvas.h / 5
                    + ((wOneItem) * idSelect) + 1, wOne5 + wOneItem - 3,
                    wOneItem - 5);
        }
        g.setColor(color[0]);

        //draw icon tab
        for (int i = 0; i < size; i++)
        {
            MainTabNew tab = (MainTabNew)vec.elementAt(i);
            int t = 0;
            int indexp = tab.typeTab;//type tab

            if (indexp > maxTypeTab)
                indexp = typeTab;
            g.drawRegion(imgTab[3], 0, indexp * 16, 16, 16, 0, xTab + wOne5
                    + wOne5 / 2 + wOneItem / 2 + t, yTab + GameCanvas.h / 5
                    + (wOneItem) / 2 + (wOneItem) * i, mGraphics.VCENTER
                    | mGraphics.HCENTER);
        }

        for (int i = 0; i < size; i++)
        {//background icon tab trai
            MainTabNew tab = (MainTabNew)vec.elementAt(i);
            int t = 0;//nhung tab duoc select thi icon se dich sang trai 1 pixex
            if (i != idSelect)
            {//paint khung nhung tab khong duoc chon
                g.drawRect(xTab + wOne5, yTab + GameCanvas.h / 5
                        + ((wOneItem) * i), wOne5 + wOneItem, (wOneItem));
            }
            else if (Focus == TAB || GameCanvas.isTouch)
            {//tinh tick khi tab duoc select
                t = -1 + (GameCanvas.gameTick / 2) % 3;
            }
            //xu ly rung icon tab
            int indexp = tab.typeTab;
            if (indexp > maxTypeTab)
                indexp = typeTab;
            g.drawRegion(imgTab[3], 0, indexp * 16, 16, 16, 0, xTab + wOne5
                    + wOne5 / 2 + wOneItem / 2 + t, yTab + GameCanvas.h / 5
                    + (wOneItem) / 2 + (wOneItem) * i, mGraphics.VCENTER
                    | mGraphics.HCENTER);
            //			if (tab.typeTab == MY_INFO && Player.diemTiemNang > 0) {
            //				PaintInfoGameScreen.fralevelup.drawFrame(
            //						GameCanvas.gameTick / 4 % 2, xTab + wOne5 + wOne5 / 2
            //								+ wOneItem + t - 4, yTab + GameCanvas.h / 5
            //								+ (wOneItem) + (wOneItem) * i - 6, 0, 3, g);
            //			} else if (tab.typeTab == SKILLS && Player.diemKyNang > 0) {
            //				PaintInfoGameScreen.fralevelup.drawFrame(
            //						2 + GameCanvas.gameTick / 4 % 2, xTab + wOne5 + wOne5
            //								/ 2 + wOneItem + t - 4, yTab + GameCanvas.h / 5
            //								+ (wOneItem) + (wOneItem) * i - 6, 0, 3, g);
            //			}
        }

        if (GameCanvas.lowGraphic)
        {
            paintRectLowGraphic(g, xTab + wOneItem + wOne5 * 3, yTab
                    + GameCanvas.h / 5 + wOneItem, wblack, hblack, 2);
        }
        else
        {
            for (int i = 0; i <= numWBlack; i++)
            {// nen den thong tin
                for (int j = 0; j <= numHBlack; j++)
                {
                    if (i == numWBlack)
                    {
                        if (j == numHBlack)
                        {
                            g.drawImage(imgTab[2], xTab + wOneItem + wOne5 * 3
                                    + wblack - 32, yTab + GameCanvas.h / 5
                                    + wOneItem + hblack - 32, 0);
                        }
                        else
                            g.drawImage(imgTab[2], xTab + wOneItem + wOne5 * 3
                                    + wblack - 32, yTab + GameCanvas.h / 5
                                    + wOneItem + j * 32, 0);
                    }
                    else
                    {
                        if (j == numHBlack)
                        {
                            g.drawImage(imgTab[2], xTab + wOneItem + wOne5 * 3
                                    + i * 32, yTab + GameCanvas.h / 5
                                    + wOneItem + hblack - 32, 0);
                        }
                        else
                            g.drawImage(imgTab[2], xTab + wOneItem + wOne5 * 3
                                    + i * 32, yTab + GameCanvas.h / 5
                                    + wOneItem + j * 32, 0);
                    }

                }
            }
        }
        if (longwidth > 0)
        {
            GameScr.resetTranslate(g);
            int indexPaint = 12;
            if (GameCanvas.lowGraphic)
            {
                paintRectLowGraphic(g, xlongwidth, ylongwidth, longwidth,
                        hSmall, indexPaint);
            }
            else
            {// man hinh menu ben phai
                int maxw = (longwidth) / 32, maxh = hSmall / 32;
                for (int i = 0; i <= maxw; i++)
                {
                    for (int j = 0; j <= maxh; j++)
                    {

                        indexPaint = 12;
                        if (j == 0)
                            indexPaint = 12;
                        if (i == maxw)
                        {
                            if (j == maxh)
                            {
                                g.drawImage(imgTab[indexPaint], xlongwidth
                                        + (longwidth) - 32, ylongwidth + hSmall
                                        - 32, 0);
                            }
                            else
                                g.drawImage(imgTab[indexPaint], xlongwidth
                                        + (longwidth) - 32,
                                        ylongwidth + j * 32, 0);
                        }
                        else
                        {
                            if (j == maxh)
                            {
                                g.drawImage(imgTab[indexPaint], xlongwidth + i
                                        * 32, ylongwidth + hSmall - 32, 0);
                            }
                            else
                                g.drawImage(imgTab[indexPaint], xlongwidth + i
                                        * 32, ylongwidth + j * 32, 0);
                        }

                    }

                }
            }
            for (int i = xlongwidth; i < xlongwidth + (longwidth); i += 6)
            {
                g.fillRect(i, ylongwidth + wOneItem, 4, 1);
            }


        }
    }

    public static int[] color = { 0xa89982, 0xbaaa92, 0xf8b848,
			0xebebeb, 0xa69780, 0xc7b59c, 0xb6a58e, 0x706354,
			0xe3d5be, 0xaf9f89, 0xf8f8a8 };
    static int[] colorLow = { 0xd6c7ae, 0x9c8c77, 0x706354, 0x887a67,
		0x4b4339 };

    public static void paintRectLowGraphic(mGraphics g, int x, int y, int w,
        int h, int indexColor)
    {
        g.setColor(colorLow[indexColor]);
        g.fillRect(x, y, w, h);

    }
    public virtual void init()
    {

    }
    public virtual void keypress(int keyCode)
    {
    }
    public void backTab()
    {

    }
    public override void keyPress(int keyCode)
    {
        keypress(keyCode);
        base.keyPress(keyCode);
    }
    //	public void paintPopupContent(mGraphics g, bool isOnlyName) {
    //		if (longwidth > 0) {
    //			paintContentNew(g, isOnlyName);
    //		} else {
    //			paintContent(g, isOnlyName);
    //		}
    //	}


    public void paintContentNew(mGraphics g, bool isOnlyName)
    {
        int begin = 4;
        // mFont fontname = setTextColorName(colorName);
        TabScreenNew.timeRepaint = 10;
        GameScr.resetTranslate(g);
        int xpaint = xlongwidth;
        int ypaint = ylongwidth;
        int yp = ypaint;
        g.setClip(xpaint + 1, yp + 1, (longwidth) - 2, hSmall - 2);
        if (!isOnlyName)
        {
            // fontname.drawString(g, name, xpaint + (longwidth) / 2, yp
            // + wOneItem / 2 - 5, 2);
            paintNameItem(g, xpaint + (longwidth) / 2, yp + wOneItem / 2 - 5,
                    longwidth, name, colorName);
            if (listContent != null)
            {
                g.setClip(xpaint, yp + wOneItem + 2, longwidth, hMaxContent
                        - wOneItem - 2);
                g.translate(0, -listContent.cmx);
            }
            yp += (wOneItem - GameCanvas.hText) + GameCanvas.hText / 4;

        }
        if (mPlusContent != null)
        {
            for (int j = 0; j < mPlusContent.Length; j++)
            {
                yp += GameCanvas.hText;
                setTextColor(mPlusColor[j]).drawString(g, mPlusContent[j],
                        xpaint + begin, yp + 2, 0);
            }
        }
        if (mContent != null)
        {
            for (int j = 0; j < mContent.Length; j++)
            {
                mFont m = null;
                if (mcolor != null)
                    m = setTextColor(mcolor[j]);
                else
                    m = mFont.tahoma_7_white;
                m.drawString(g, mContent[j], xpaint + begin, yp + 2 + (j + 1)
                        * GameCanvas.hText, 0);
                if (mSubContent != null)
                {
                    int t = m.getWidth(mContent[j]) + 5;
                    m = setTextColor(mSubColor[j]);
                    m.drawString(g, mSubContent[j], xpaint + t + begin, yp + 2
                            + (j + 1) * GameCanvas.hText, 0);
                }
            }
        }
        else
        {
            //			if (!isOnlyName) {
            //				// fontname.drawString(g, name, xpaint + (longwidth) / 2, yp
            //				// + wOneItem / 2 - 5, 0);
            ////				paintNameItem(g, xpaint + (longwidth) / 2, yp + wOneItem / 2
            ////						- 5, longwidth, name, colorName);
            ////				mFont.tahoma_7_white.drawString(g, T.danglaydulieu, xpaint
            ////						+ (longwidth) / 2,
            ////						yp + wOneItem + GameCanvas.hText + 3, 2);
            //			} else {
            if (isOnlyName)
                mFont.tahoma_7b_white.drawString(g, "Trang bi", xpaint + (longwidth)
                        / 2, yp + wOneItem / 2 - 5, 2);
            //			}
        }
        GameScr.resetTranslate(g);
    }

    public void paintPopupContent(mGraphics g, bool isOnlyName)
    {
        if (longwidth > 0)
        {
            paintContentNew(g, isOnlyName);
        }
        else
        {
            paintContent(g, isOnlyName);
        }
    }

    public void paintContent(mGraphics g, bool isOnlyName)
    { // paint noi dung 
        int begin = 4;
        // mFont fontname = setTextColorName(colorName);
        TabScreenNew.timeRepaint = 10;
        g.setClip(-g.getTranslateX(), -g.getTranslateY(), GameCanvas.w,
                GameCanvas.h);
        int th = 1;
        if (mContent != null)
            th = mContent.Length;
        if (mPlusContent != null)
            th += mPlusContent.Length;
        int h = (th + 1) * GameCanvas.hText + 8;
        if (h > hMaxContent)
            h = hMaxContent;
        if (xCon + wContent > GameCanvas.w)
            xCon = GameCanvas.w / 2 - wContent / 2;
        int yp = yCon;
        g.setColor(color[10]);
        g.fillRect(xCon - 1, yp - 1, wContent + 2, h + 2);
        g.setColor(color[2]);
        g.fillRect(xCon, yp, wContent + 1, h + 1);
        if (GameCanvas.lowGraphic)
        {
            paintRectLowGraphic(g, xCon, yp, wContent, h, 4);
        }
        else
        {
            int maxw = wContent / 32, maxh = h / 32;
            for (int i = 0; i <= maxw; i++)
            {
                for (int j = 0; j <= maxh; j++)
                {
                    if (i == maxw)
                    {
                        if (j == maxh)
                        {
                            g.drawImage(imgTab[12], xCon + wContent - 32, yp
                                    + h - 32, 0);
                        }
                        else
                            g.drawImage(imgTab[12], xCon + wContent - 32, yp
                                    + j * 32, 0);
                    }
                    else
                    {
                        if (j == maxh)
                        {
                            g.drawImage(imgTab[12], xCon + i * 32, yp + h - 32,
                                    0);
                        }
                        else
                            g.drawImage(imgTab[12], xCon + i * 32, yp + j * 32,
                                    0);
                    }

                }
            }
        }
        g.setClip(xCon + 1, yp + 1, wContent - 2, h - 2);
        if (!isOnlyName)
        {
            if (name != null)
                // fontname.drawString(g, name, xCon + begin, yp + 2, 0);
                paintNameItem(g, xCon + (wContent) / 2, yp + 2,
                        wContent, name, colorName);
            // if (cmY > 0) {
            if (listContent != null)
            {
                g.setClip(xCon, yp + GameCanvas.hText, wContent, hMaxContent
                        - GameCanvas.hText);
                g.translate(0, -listContent.cmx);
            }
            // }
        }
        if (mPlusContent != null)
        {
            for (int j = 0; j < mPlusContent.Length; j++)
            {
                yp += GameCanvas.hText;

                setTextColor(mPlusColor[j]).drawString(g, mPlusContent[j],
                        xCon + begin, yp + 2, 0);
            }
        }
        if (mContent != null)
        {
            for (int j = 0; j < mContent.Length; j++)
            {
                mFont m = null;
                if (mcolor != null)
                    m = setTextColor(mcolor[j]);
                else
                    m = mFont.tahoma_7_white;
                m.drawString(g, mContent[j], xCon + begin, yp + 2 + (j + 1)
                        * GameCanvas.hText, 0);
                if (mSubContent != null)
                {
                    int t = m.getWidth(mContent[j]) + 5;
                    m = setTextColor(mSubColor[j]);
                    m.drawString(g, mSubContent[j], xCon + t + begin, yp + 2
                            + (j + 1) * GameCanvas.hText, 0);
                }
            }
        }
        else
        {
            if (!isOnlyName)
            {
                if (name != null)
                {
                    //					fontname.drawString(g, name, xCon + begin, yp + 2, 0);
                    paintNameItem(g, xCon + (wContent) / 2, yp + 2,
                            wContent, name, colorName);
                }
                //				mFont.tahoma_7_white.drawString(g, T.danglaydulieu, xCon + 2,
                //						yp + GameCanvas.hText + 3, 0);
            }
            else
            {
                if (name != null)
                {
                    //					mFont.tahoma_7b_white.drawString(g, name, xCon + wContent
                    //							/ 2, yp + GameCanvas.hText / 4, 2);
                    paintNameItem(g, xCon + (wContent) / 2, yp + GameCanvas.hText / 4,
                            wContent, name, colorName);
                }
            }
        }
        GameScr.resetTranslate(g);
    }
    public void setPosCmd(mVector vecListCmd)
    {
        try
        {
            if (vecListCmd == null)
                return;
            int size = vecListCmd.size();
            if (size == 0)
                return;
            int yp = ylongwidth + hSmall;
            int xp = xlongwidth;

            if (size == 1)
            {
                iCommand cmd = (iCommand)vecListCmd.elementAt(0);
                if (MainTabNew.is320)
                {
                    cmd.setPos(xp + (longwidth) / 2, yp - 10,
                            PaintInfoGameScreen.fraButton, cmd.caption);
                }
                else
                {
                    cmd.setPos(xp + (longwidth) / 2, yp - 15, null, cmd.caption);
                }
            }
            else if (size == 2)
            {
                iCommand cmd = (iCommand)vecListCmd.elementAt(0);
                if (MainTabNew.is320)
                {
                    cmd.setPos(xp + longwidth / 4, yp - 10,
                            PaintInfoGameScreen.fraButton, cmd.caption);
                }
                else
                {
                    cmd.setPos(xp + longwidth / 4, yp - 15, null, cmd.caption);
                }
                iCommand cmd2 = (iCommand)vecListCmd.elementAt(1);

                if (MainTabNew.is320)
                {
                    cmd2.setPos(xp + longwidth / 4 * 3 + 2, yp - 10,
                            PaintInfoGameScreen.fraButton, cmd.caption);
                }
                else
                {
                    cmd2.setPos(xp + longwidth / 4 * 3 + 2, yp - 15, null,
                            cmd2.caption);
                }
            }
            else
            {
                for (int i = 0; i < size; i++)
                {
                    iCommand cmd = (iCommand)vecListCmd.elementAt(i);
                    if (i == size - 1 && size % 2 == 1)
                    {
                        if (MainTabNew.is320)
                        {
                            cmd.setPos(xp + longwidth / 2, yp - 10,
                                    PaintInfoGameScreen.fraButton, cmd.caption);
                        }
                        else
                        {
                            cmd.setPos(xp + (longwidth) / 2, yp - 15
                                    - ((size - 1) / 2) * 30 + (i / 2) * 30, null,
                                    cmd.caption);
                        }
                    }
                    else
                    {
                        if (MainTabNew.is320)
                        {
                            cmd.setPos(xp + longwidth / 4 + (i % 2)
                                    * (longwidth / 2 + 2), yp - 10
                                    - ((size - 1) / 2) * 22 + (i / 2) * 22,
                                    PaintInfoGameScreen.fraButton, cmd.caption);
                        }
                        else
                        {
                            cmd.setPos(xp + longwidth / 4 + (i % 2)
                                    * (longwidth / 2 + 2), yp - 15
                                    - ((size - 1) / 2) * 30 + (i / 2) * 30, null,
                                    cmd.caption);
                        }
                    }
                }
            }

        }
        catch (Exception e)
        {
            // TODO: handle exception
            //	e.printStackTrace();
        }

    }
    public static void paintNameItem(mGraphics g, int x, int y, int w, string name,
            int colorName)
    {
        if (mFont.tahoma_7b_yellow.getWidth(name) <= w)
        {
            mFont fontname = setTextColorName(colorName);
            fontname.drawString(g, name, x, y, 0);
        }
        else
        {
            if (nameCur.CompareTo(name.Trim()) != 0)
                getTextName(name);
            mFont fontname = setTextColor(colorName);
            fontname.drawString(g, namePaint[0], x, y - 6, 0);
            fontname.drawString(g, namePaint[1], x, y + 6, 0);
        }
    }

    static string nameCur = "";
    static string[] namePaint = new string[2];


    public static mFont setTextColor(int id)
    {
        switch (id)
        {
            case COLOR_WHITE:
                return mFont.tahoma_7_white;
            case COLOR_BLUE:
                return mFont.tahoma_7_blue;
            case COLOR_YELLOW:
                return mFont.tahoma_7_yellow;
            case COLOR_VIOLET:
            //			return mFont.tahoma_7_violet;
            case COLOR_ORANGE:
            //			return mFont.tahoma_7_orange;
            case COLOR_GREEN:
                return mFont.tahoma_7_green;
            case COLOR_RED:
                return mFont.tahoma_7_red;
            case COLOR_BLACK:
            //			return mFont.tahoma_7_black;
                break;
        }
        return mFont.tahoma_7_white;
    }

    public static void getTextName(string name)
    {

        nameCur = name.Trim();
        namePaint = new string[2];
        for (int i = 0; i < namePaint.Length; i++)
        {
            namePaint[i] = "";
        }
        string[] m = mFont.split(nameCur, " ");
        for (int i = 0; i < m.Length; i++)
        {
            if (i <= m.Length / 2)
            {
                namePaint[0] += m[i];
                if (i < m.Length / 2)
                    namePaint[0] += " ";
            }
            else
            {
                namePaint[1] += m[i];
                if (i < m.Length - 1)
                    namePaint[1] += " ";
            }
        }
    }


    public static mFont setTextColorName(int id)
    {
        switch (id)
        {
            case COLOR_WHITE:
                return mFont.tahoma_7b_white;
            case COLOR_BLUE:
                return mFont.tahoma_7b_blue;
            case COLOR_YELLOW:
                return mFont.tahoma_7b_yellow;
            case COLOR_VIOLET:
            //			return mFont.tahoma_7b_violet;
            case COLOR_ORANGE:
            //			return mFont.tahoma_7b_orange;
            case COLOR_GREEN:
                return mFont.tahoma_7b_green;
            case COLOR_BLACK:
                //			return mFont.tahoma_7b_black;
                break;
        }
        return mFont.tahoma_7b_white;
    }


    public virtual void perform(int idAction, Object p)
    {
        // TODO Auto-generated method stub

    }
}
