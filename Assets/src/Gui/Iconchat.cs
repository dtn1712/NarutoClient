

public class Iconchat {
	public int zoneCol = 6;
	public int columns = 6, rows;
	public Scroll scrMain = new Scroll();
	public int xstart, ystart, popupW = 140, popupH = 160;
	public int indexSize = 28, indexTitle = 0, indexSelect = -1, indexRow = -1, indexRowMax, indexMenu = 0, indexCard = -1;
	
	public Iconchat(int x, int y){
		this.popupX = x;
		this.popupY = y;
	}
	
	public void paint(mGraphics g){
		if (!GameScr.isPaintZone)
			return;
		GameScr.resetTranslate(g);

		paintFrameIconchat(g);
		int r = indexSelect / zoneCol;
		int c = indexSelect % zoneCol;
		
		rows = loadImageInterface.imgEmo.Length / zoneCol;
		if (loadImageInterface.imgEmo.Length % zoneCol > 0)
			rows += 1;
		if (rows < 5)
			rows = 5;

		scrMain.setStyle(rows, indexSize, xstart, ystart, columns * indexSize + 2, 5 * indexSize + 2, true, 6);
		scrMain.setClip(g,  xstart, ystart, columns * indexSize + 2, 5 * indexSize + 2);
//		scrMain.setClip(g);
		g.drawImage(loadImageInterface.imgEmo[0], xstart +10, ystart + 10, 0);
		int index = 0;
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < zoneCol; j++) {
				if (index >= loadImageInterface.imgEmo.Length)
					continue;
				if (loadImageInterface.imgEmo[index] != null){
					g.drawImage(loadImageInterface.imgEmo[index], xstart + j * 28 + 28 / 2 - 8, ystart + i * 28 + 28 / 2 - 8, 0,true);
				}
				index++;
			}
		if (indexSelect >= 0) {
			g.setColor(0xFFFFFF);
			g.drawRect(xstart + (c * indexSize), ystart + (r * indexSize), indexSize, indexSize);
		}
		GameScr.resetTranslate(g);
	}
	
	public void Updatecm()
	{
		//scrMain.updatecm();
	}
	
	public void updateKeySelectIconChat() {
        if (GameCanvas.currentDialog != null)
        {
            return;
        }
        if (GameCanvas.isTouch)
        {
            ScrollResult r = scrMain.updateKey();
            scrMain.updatecm();
            if (r.isDowning || r.isFinish)
            {
                indexSelect = scrMain.selectedItem;
            }

        }

	}
	public int popupY, popupX;
	private void paintFrameIconchat(mGraphics g){
		xstart = popupX + 3;
		ystart = popupY + 32;
		g.setColor(0x001919);
		g.fillRect(xstart - 1, ystart - 1, columns * indexSize + 3, 5 * indexSize + 3);
		
		;
	}
}
