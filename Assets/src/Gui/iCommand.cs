

public class iCommand {
	public string caption, clickCaption = "";
	public string[] subCaption;
	public IAction action;
	public Screen Pointer;
	public sbyte indexMenu, subIndex = -1;
	public bool isSelect = false;
	int nframe, beginframe;
	FrameImage fraImgCaption, fraImageCmd;
	int wimgCaption, himgCaption, wstr, wimgCmd, himgCmd;
	public int xCmd = -1, yCmd = -1, frameCmd, xdich, ydich;
	public static int wButtonCmd = 70, hButtonCmd = 24;
	public int IdGiaotiep = 0;

	/**
	 * @param caption
	 * @param action
	 */
	public iCommand(string caption, IAction action) {
		//super();
		this.caption = caption;
		this.action = action;
	}

	public iCommand(string caption, int type) {
		//super();
		this.caption = caption;
		this.indexMenu = (sbyte) type;
	}

	public iCommand(string caption, int type, Screen pointer) {
		//super();
		this.caption = caption;
		this.indexMenu = (sbyte) type;
		this.Pointer = pointer;
	}

	public iCommand(string caption, int type, int subType, Screen pointer) {
		//super();
		this.caption = caption;
		this.indexMenu = (sbyte) type;
		this.subIndex = (sbyte) subType;
		this.Pointer = pointer;
	}

	public iCommand(string caption, int type, int subIndex) {
		//super();
		this.caption = caption;
		this.indexMenu = (sbyte) type;
		this.subIndex = (sbyte) subIndex;
	}

	public void setFraCaption(FrameImage fra) {
		fraImgCaption = fra;
		wimgCaption = fraImgCaption.frameWidth;
		himgCaption = fraImgCaption.frameHeight;
		nframe = fraImgCaption.nFrame;
		beginframe = 0;
		wstr = mFont.tahoma_7b_white.getWidth(caption);
	}

	public void setFraCaption(FrameImage fra, int nframe, int beginframe) {
		fraImgCaption = fra;
		wimgCaption = fraImgCaption.frameWidth;
		himgCaption = fraImgCaption.frameHeight;
		this.nframe = nframe;
		this.beginframe = beginframe;
		
		wstr = mFont.tahoma_7b_white.getWidth(caption);
	}

	public void setPos(int x, int y, FrameImage fra, string caption) {
		this.caption = caption;
		xCmd = x;
		yCmd = y;
		fraImageCmd = fra;
		if (fraImageCmd != null) {
			wimgCmd = fraImageCmd.frameWidth;
			//System.out.println("wimgCommand ----> "+wimgCmd);
			himgCmd = fraImageCmd.frameHeight;
			//System.out.println("himgCmd ----> "+himgCaption);
			if (wimgCmd < 28)
				wimgCmd = 28;
			if (himgCmd < 28)
				himgCmd = 28;
		} else {
			wimgCmd = 70;
			himgCmd = hButtonCmd;
		}
	}

	public void setPos_ShowName(int x, int y, FrameImage fra,
			string clickcaption, int xdich, int ydich) {
		this.caption = "";
		this.clickCaption = clickcaption;
		xCmd = x;
		yCmd = y;
		this.xdich = xdich;
		this.ydich = ydich;
		fraImageCmd = fra;
		if (fraImageCmd != null) {
			wimgCmd = fraImageCmd.frameWidth;
			himgCmd = fraImageCmd.frameHeight;
			if (wimgCmd < 28)
				wimgCmd = 28;
			if (himgCmd < 28)
				himgCmd = 28;
		} else {
			wimgCmd = 70;
			himgCmd = hButtonCmd;
		}
	}

	public void setPosXY(int x, int y) {
		// if (CRes.abs(x - xCmd) > 5)
		xCmd = x;
		// if (CRes.abs(y - yCmd) > 5)
		yCmd = y;
	}

	public void perform() {
		if (action != null) {
//			action.perform();
//			GameCanvas.isPointerSelect = false;
			GameCanvas.clearKeyHold();
			GameCanvas.clearKeyPressed();
		} else {
			if (Pointer != null) {
				//System.out.println("PERFORM ICOMMAND ----> ");
				Pointer.commandPointer(indexMenu, subIndex);
			} else {
				if (GameCanvas.currentDialog != null) {
//					GameCanvas.currentDialog.commandTab(indexMenu, subIndex);
					GameCanvas.isPointerClick = false;
					GameCanvas.clearKeyHold();
					GameCanvas.clearKeyPressed();
					return;
				}
				if (ChatTextField.gI().isShow) {
//					ChatTextField.gI().commandTab(indexMenu, subIndex);
					return;
				}
//				}else if (GameCanvas.subDialog != null) {
//					GameCanvas.subDialog.commandTab(indexMenu, subIndex);
//					return;
				} 
			}
			GameCanvas.isPointerClick = false;
			GameCanvas.clearKeyHold();
			GameCanvas.clearKeyPressed();
		}

	

	public void update() {
	}

	public void paint(mGraphics g, int x, int y) {
		if (isPosCmd()) {
			if (fraImageCmd != null) {
				fraImageCmd.drawFrame(frameCmd, xCmd, yCmd, 0, 3, g);
			} else {
				paintbutton(g, xCmd, yCmd);
			}
			paintCaptionImage(g, xCmd, yCmd - 6, 2);
		} else {
			if (fraImageCmd != null) {
				fraImageCmd.drawFrame(frameCmd, x, y, 0, 3, g);
			} else {
				paintbutton(g, x, y);
			}
//			paintbutton(g, x, y);
			paintCaptionImage(g, x, y - 6, 2);
		}
	}

	public void paintCaptionImage(mGraphics g, int x, int y, int pos) {
		
		if (caption == null) {
			return;
		}
		int ximg = 0;
		if (fraImgCaption != null) {
			if (pos == 2) {
				fraImgCaption.drawFrame(beginframe + (GameCanvas.gameTick / 2)
						% nframe, x - wimgCaption / 2 - wstr / 2, y
						+ himgCaption / 2 - 3, 0, 3, g);
				ximg = wimgCaption / 2;
			} else if (pos == 0) {
				fraImgCaption.drawFrame(beginframe + (GameCanvas.gameTick / 2)
						% nframe, x + wimgCaption / 2, y + himgCaption / 2 - 4,
						0, 3, g);
				ximg = wimgCaption + 6;
			}
		}
//			AvMain.Font3dColor(g, caption, x + ximg, y, pos,
//					MainTabNew.COLOR_WHITE);
	}

	public void paintClickCaption(mGraphics g, int x, int y, int pos) {
		if (frameCmd == 1 && clickCaption.Length > 0) {
			AvMain.Font3dColor(g, clickCaption, x + xdich, y + ydich, pos,
					MainTabNew.COLOR_WHITE);
		}
	}

	public void paintCaptionImageMenu(mGraphics g, int x, int y, int pos) {
		int ximg = 0;
		if (fraImgCaption != null) {
			if (pos == 2) {
				fraImgCaption.drawFrame(beginframe + (GameCanvas.gameTick / 2)
						% nframe, x - wimgCaption / 2 - wstr / 2, y
						+ himgCaption / 2, 0, 3, g);
				ximg = wimgCaption / 2;
			} else if (pos == 0) {
				fraImgCaption.drawFrame(beginframe + (GameCanvas.gameTick / 2)
						% nframe, x + wimgCaption / 2, y + himgCaption / 2, 0,
						3, g);
				ximg = wimgCaption + 6;
			}
		}
//			AvMain.Font3dColorAndColor(g, caption, x + ximg, y, pos,
//					MainTabNew.COLOR_BLACK, MainTabNew.COLOR_WHITE);
	}

	public void updatePointer() {
		if (isPosCmd()) {
			if (GameCanvas.isPointerDown /*|| GameCanvas.isPointerMove*/) {
				if (GameCanvas.isPoint(xCmd - wimgCmd / 2 - 5, yCmd - himgCmd
						/ 2 - 5, wimgCmd + 10, himgCmd + 10)) {
					frameCmd = 1;
					perform();
					GameCanvas.isPointerClick = false;
				} else{
					frameCmd = 0;
				}
			} else {
				frameCmd = 0;
			}
//			if (GameCanvas.isPointSelect(xCmd - wimgCmd / 2 - 5, yCmd - himgCmd
//					/ 2 - 5, wimgCmd + 10, himgCmd + 10)) {
//				perform();
//				GameCanvas.isPointerClick = false;
//				frameCmd = 0;
//			}
		}
	}
	
	

	public void updatePointer(int cmx, int cmy) {
		if (isPosCmd()) {
			if (GameCanvas.isPointerDown /*|| GameCanvas.isPointerMove*/) {
				if (GameCanvas.isPoint(xCmd - wimgCmd / 2 - 3 - cmx, yCmd
						- himgCmd / 2 - 3 - cmy, wimgCmd + 6, himgCmd + 6)) {
					frameCmd = 1;
				} else
					frameCmd = 0;
			} else {
				frameCmd = 0;
			}
			if (GameCanvas.isPointSelect(xCmd - wimgCmd / 2 - 3 - cmx, yCmd
					- himgCmd / 2 - 3 - cmy, wimgCmd + 6, himgCmd + 6)) {
				perform();
				GameCanvas.isPointerClick = false;
				frameCmd = 0;
			}
		}
	}
	public void updatePointerShow(int cmx, int cmy) {
		if (isPosCmd()) {
			if (GameCanvas.isPointerDown /*|| GameCanvas.isPointerMove*/) {
				if (GameCanvas.isPoint(xCmd - wimgCmd / 2 - 3 - cmx, yCmd
						- himgCmd / 2 - 3 - cmy, wimgCmd + 6, himgCmd + 6)) {
					frameCmd = 1;
				} else
					frameCmd = 0;
			} else {
				frameCmd = 0;
			}
		}
	}
	public void paintbutton(mGraphics g, int x, int y) {
		if (GameCanvas.isTouch) {
			PaintInfoGameScreen.fraButton.drawFrame(frameCmd, x, y, 0, 3, g);
		} else {
//			AvMain.paintDialog(g, x - wButtonCmd / 2, y - hButtonCmd / 2,
//					wButtonCmd, hButtonCmd, (isSelect ? 1 : 2));
		}
	}

	public bool isPosCmd() {
		if (xCmd > 0 && yCmd > 0)
			return true;
		return false;
	}
}
