

/*
 * this class use to display function game
 */
	public class TabConfig : MainTabNew{
	mVector vecConfig = new mVector();
	int w2, wFocus;
	int idSelect, hItem;
	Command cmdSelect, cmdlogout, cmdSetAuto, cmdHelp, cmdSetting,
	cmdChucNang, cmdShowFullMini, cmdChatWorld, cmdDiamond;
	public static Command cmdEvent, cmdKeypad, cmdShowClan;

	public TabConfig(string name, mVector vec, sbyte type) {
		typeTab = type;
		this.nameTab = name;
		this.vecConfig = vec;
		xBegin = base.xTab + wOneItem + wOne5 * 3;
        yBegin = base.yTab + GameCanvas.h / 5 + wOneItem;
		w2 = wblack / 2;
		wFocus = wblack - wOne5 * 2;
		if (wFocus > 130)
			wFocus = 130;
		hItem = GameCanvas.hCommand;
		if (GameCanvas.isTouch)
			hItem = 28;
		
		
		cmdSelect = new Command(T.select, 0, this);
		cmdlogout = new Command(T.logout, 4, this);
		// if (TemMidlet.DIVICE == 0)
		// cmdlogout.caption = T.exit + " Game";
		cmdKeypad = new Command(T.chuyensang, 7, this);
		cmdHelp = new Command(T.help, 10, this);
		cmdSetting = new Command(T.auto, 11, this);
		cmdChucNang = new Command(T.chucnang, 12, this);
		cmdEvent = new Command(T.eventt, 13, this);
		cmdShowClan = new Command(T.clan, 14, this);
		cmdShowFullMini = new Command(T.minimap, 15, this);
		cmdChatWorld = new Command(T.textkenhthegioi, 16, this);
		cmdDiamond = new Command(T.naptien + " " + T.gold, 19, this);
		init();
	}
	
	public void init() {
		// TODO Auto-generated method stub
		mVector mcmdTest = new mVector();
		if (typeTab == FUNCTION) {
			mcmdTest.addElement(cmdHelp);
			cmdEvent.caption = T.eventt;
			mcmdTest.addElement(cmdEvent);
			mcmdTest.addElement(cmdShowFullMini);
			mcmdTest.addElement(cmdChatWorld);
			// mcmdTest.addElement(GameScreen.gI().cmdSetWeather);
		}
		vecConfig = mcmdTest;
		int hmax = vecConfig.size() * hItem - (hblack);
		if (hmax < 0)
			hmax = 0;
		idSelect = 0;

	}
	public override void paint(mGraphics g) {
		//g.setClip(xBegin, yBegin, wblack, hblack);
		
		//paint background focus
		if (Focus == INFO && idSelect > -1)
			paintFocus(g);
		
		//draw name command
		for (int i = 0; i < vecConfig.size(); i++) {
			Command cmd = (Command) vecConfig.elementAt(i);
			mFont.tahoma_7b_white.drawString(g, cmd.caption, xBegin + w2,
					yBegin + hItem / 2 + i * hItem - 6, 2);
			//draw line
			if (i < vecConfig.size() - 1) {
				g.setColor(color[1]);
				g.fillRect(xBegin + 8, yBegin + (i + 1) * hItem - 1,
						wblack - 16, 1);
			}
		}
	}
	
	/*
	 * Paint focus command
	 */
	public void paintFocus(mGraphics g) {
		g.setColor(color[5]);
		g.fillRect(xBegin + w2 - wFocus / 2 - 1, yBegin + idSelect * hItem
				+ hItem / 2 - 11, wFocus + 2, 22);
		if (GameCanvas.lowGraphic) {
			MainTabNew.paintRectLowGraphic(g, xBegin + w2 - wFocus / 2, yBegin
					+ idSelect * hItem + hItem / 2 - 10, wFocus, 20, 4);
		} else {
			for (int i = 0; i <= wFocus / 24; i++) {
				int t = xBegin + w2 - wFocus / 2 + 24 * i;
				if (i == wFocus / 24) {
					t = xBegin + w2 + wFocus / 2 - 24;
				}
				g.drawRegion(imgTab[8], 0, 0, 24, 20, 0, t, yBegin + idSelect
						* hItem + hItem / 2 - 10, 0);
			}
		}
	}
}
