

using System;
public class GuiChatClanWorld : IActionListener {
	
	public const sbyte CHAT_WORLD=0;
	public const sbyte CHAT_CLAN=1;
	
	FatherChat []fatherChat;
	string[] listNameTab ;
	
	public int x,y;
	int selectTab;
	int nTab;
	int distanceTab=5;//distance between 2 tab
	int widthTab=30;
	int heightTab=25;
	public Command bntOpen;
	int yButtonTab=GameCanvas.h-50;//y of button tab
	public bool moveClose,moveOpen;
	int v;//gia toc tang toc do move
	public static int xMove;
	int limMove=FatherChat.popw-10;
	
	public int xBtnMove=FatherChat.popw+10;
	
	public GuiChatClanWorld(int x,int y, string[]arrayName) {
		
		this.x=x;
		this.y=y;
		this.nTab=arrayName.Length;
		this.listNameTab=arrayName;
		fatherChat= new FatherChat[arrayName.Length];
		fatherChat[0]=new GuiChatWorld(this.x,this.y);
		fatherChat[1]=new GuiChatClan(this.x,this.y);
		
		bntOpen= new Command("", this, Contans.BUTTON_OPEN, null, 0, 0);
        bntOpen.setPos(0, y + 130 / 2 - Image.getHeight(loadImageInterface.imgShortQuest) / 2 + 25, loadImageInterface.imgShortQuest, loadImageInterface.imgShortQuest);
    }
	
	public void ActionPerformSend()
	{
		fatherChat[selectTab].ActionPerformSend();
	}
	public void ActionPerformIconChat()
	{
		fatherChat[selectTab].ActionPerformIconChat();
	}
	public void Update()
	{
		fatherChat[selectTab].Update();
		
		if(moveClose)//move to right
		{
			if(xMove>limMove)
			{
//				xMove=limMove;
				v=0;
			}
			else
			{
				
				xMove+=v;
				v++;
			}
		}else//move to left
		{
			
			if(xMove<=-30)
			{
//				xMove=-10;
				v=0;
				xMove =Image.getWidth(loadImageInterface.imgShortQuest)- this.x-xBtnMove - 35;
			}
			else
			{
				xMove+=v;
				v--;
			}
		}
		if(v!=0)
			Move();
		
	}
	public void UpdateKey()
	{

        if (moveClose)
		fatherChat[selectTab].UpdateKey();
		if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(bntOpen)) {
			if (bntOpen != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (bntOpen != null)
				{
					bntOpen.performAction();
				}
			}
		}
	}
	public void KeyPress(int keyCode)
	{
		fatherChat[selectTab].KeyPress(keyCode);
	}
	public void Close()
	{
	}
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		fatherChat[selectTab].perform(idAction,p);
		switch (idAction) {
		case Contans.BUTTON_OPEN:
			if(!moveClose)
			{
				Cout.println(" -----> mo chat");
				GameScr.gI().guiMain.menuIcon.indexpICon = Contans.BUTTON_OPEN; //truong hop mo chat ko upda menuicon
			
				moveClose=true;
				bntOpen.setPos(x+xBtnMove-Image.getWidth(loadImageInterface.imgShortQuest) + 35, y+130/2-Image.getHeight(loadImageInterface.imgShortQuest)/2 +25, loadImageInterface.imgShortQuest_Close, loadImageInterface.imgShortQuest_Close);
			}
			else
			{
                if (fatherChat[selectTab].iconChat != null) return;
				GameScr.gI().guiMain.menuIcon.indexpICon = 0;//truong hop dong chat ko upda menuicon
				
				moveClose=false;
				bntOpen.setPos(x+xBtnMove-Image.getWidth(loadImageInterface.imgShortQuest) + 35, y+130/2-Image.getHeight(loadImageInterface.imgShortQuest)/2 +25, loadImageInterface.imgShortQuest, loadImageInterface.imgShortQuest);
			}
			break;

		default:
			break;
		}
			
	}
	/*
	 * Paint background tab  working and done
	 */
	public void paintTabBig(mGraphics g, int x, int y,int w,int h) {
		
//		g.setColor(0x584848);
		g.drawImage(loadImageInterface.imgChatButton, x, y, 0);

		
	}
	/*
	 * Paint background tab is selected
	 */
	public void paintTabFocus(mGraphics g, int x, int y,int w,int h) {
//		g.setColor(0x454545);
//		g.fillRect(x, y, w, h);
		g.drawImage(loadImageInterface.imgChatButtonFocus, x, y, 0);

	}
	
	/*
	 * execute touch
	 */
	public void updatePointer() {
		if (GameCanvas.isPointerClick) 
		{
			//index select tab
			for(int i=0;i<nTab;i++)
			{
				if (GameCanvas.isPointer(this.x+(this.widthTab+this.distanceTab)*i+xMove,this.yButtonTab,this.widthTab,this.heightTab)) 
				{
					selectTab=i;
					GameCanvas.isPointerClick = false;
					break;
				}
			}
		}
	}

	public void resetTab(bool isResetCmy) {
		TabScreenNew.timeRepaint = 10;
		int t = 0;
		
	}
	
	private void Move()
	{
		bntOpen.x=this.x+xBtnMove-Image.getWidth(loadImageInterface.imgShortQuest)+xMove + 35;
	}
	public void Paint(mGraphics g)
	{
		PaintFrameChat(g);
		//paint background tab
		for(int i=0;i<nTab;i++)
		{
			paintTabBig(g,this.x+(this.widthTab+this.distanceTab)*i+xMove ,this.yButtonTab,this.widthTab+this.distanceTab , this.heightTab);
			mFont.tahoma_7_white.drawString(g,this.listNameTab[i] , this.x+(this.widthTab+this.distanceTab)*i+this.widthTab/2+xMove - 7 , this.heightTab/4+this.yButtonTab-4 ,0);
		}
		//paint background tab focus
		paintTabFocus(g,this.x+(this.widthTab+this.distanceTab)*selectTab+xMove ,this.yButtonTab, this.widthTab+this.distanceTab , this.heightTab);
		mFont.tahoma_7_white.drawString(g,this.listNameTab[selectTab] , this.x+(this.widthTab+this.distanceTab)*selectTab+this.widthTab/2+xMove - 7 ,this.heightTab/4+this.yButtonTab-4,0);

		bntOpen.paint(g);
		fatherChat[selectTab].paintContentChatWorld(g);
		GameScr.resetTranslate(g);
	}
	
	private void PaintFrameChat(mGraphics g)
	{
		//first row
		g.drawImage(loadImageInterface.imgChatConner, x+xMove, y, mGraphics.TOP | mGraphics.LEFT);
		for(int i=1;i<18;i++)
		{
			g.drawImage(loadImageInterface.imgChatRec, x+i*(Image.getWidth(loadImageInterface.imgChatRec))+xMove, y, 0);
		}
		g.drawRegion(loadImageInterface.imgChatConner, 0, 0,Image.getWidth(loadImageInterface.imgChatConner),
				Image.getHeight(loadImageInterface.imgChatConner), Sprite.TRANS_MIRROR, x +18*(Image.getWidth(loadImageInterface.imgChatConner))+xMove, y,mGraphics.TOP | mGraphics.LEFT);
		
		//paint all rac of frame
		for(int i=1;i<20;i++)
			for(int j=0;j<19;j++)
			{
				g.drawImage(loadImageInterface.imgChatRec, x+j*(Image.getWidth(loadImageInterface.imgChatRec))+xMove, y+Image.getHeight(loadImageInterface.imgChatRec)*i, 0);
			}
		
		
		g.drawImage(loadImageInterface.imgChatConner_1, x+xMove, y+Image.getHeight(loadImageInterface.imgChatRec)*20, mGraphics.TOP | mGraphics.LEFT);
		for(int i=1;i<18;i++)
		{
			g.drawImage(loadImageInterface.imgChatRec_1, x+i*(Image.getWidth(loadImageInterface.imgChatRec_1))+xMove, y+Image.getHeight(loadImageInterface.imgChatRec)*20, 0);
		}
		
		g.drawRegion(loadImageInterface.imgChatConner_1, 0, 0,Image.getWidth(loadImageInterface.imgChatConner_1),
				Image.getHeight(loadImageInterface.imgChatConner_1), Sprite.TRANS_MIRROR, x +18*(Image.getWidth(loadImageInterface.imgChatConner_1))+xMove,y+Image.getHeight(loadImageInterface.imgChatRec)*20,mGraphics.TOP | mGraphics.LEFT);
	}
}
