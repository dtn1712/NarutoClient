
public class TabQuest : MainTabNew {

	public static string[] nameItemQuest = null;
	public const sbyte WORKING_QUEST=0;
	public const sbyte DONE_QUEST=1;
	
	int idSelect, maxSize, hmax;
	static TabQuest me;
	// new Quest
	string[] str = { "đang làm", "đã xong" };
	int xdich, wMainTab, xselect, HQuest;//wMainTab: width tab working and done
	int tabSelect = 0;// tab working, tab done
	int xMap, yMap;

	mVector vecListCmd = new mVector();

	public TabQuest(string name) {
		typeTab = QUEST;
		me = this;
		this.nameTab = name;
		HQuest = GameCanvas.hText;
		wMainTab = wOneItem * 3;
		if (GameCanvas.isTouch)
			HQuest = GameCanvas.hCommand;
		xBegin = base.xTab + wOneItem + wOne5 * 3;
        yBegin = base.yTab + GameCanvas.h / 5 + wOneItem;
		init();

	}
	
	public override void init() {
		// TODO Auto-generated method stub
		xdich = (wblack - wMainTab * 2) / 2;//toa do tab working and done in tab quest
		base.init();
	}
	
	public void paint(mGraphics g) {
        int xp = base.xTab + wOneItem + wOne5 * 3 + xdich, yp = base.yTab//toa do draw tab
		+ GameCanvas.h / 5 + wOne5;
		
		g.drawImage(loadImageInterface.imgCharPoint,xTab+10+widthFrame/2,yTab-20+ GameCanvas.h / 5+heightFrame/2,mGraphics.HCENTER|mGraphics.VCENTER);
		//paint 2 tab columms: tab: working and done
//		paintTabBig(g, xp, yp - 1, wMainTab);
//		mFont.tahoma_7_white.drawString(g, str[WORKING_QUEST], xp + wMainTab / 2, yp+ wOneItem / 2 - 7, 2);
//		
//		paintTabBig(g, xp + wMainTab, yp - 1, wMainTab);
//		mFont.tahoma_7_white.drawString(g, str[DONE_QUEST], xp + wMainTab + wMainTab/ 2, yp + wOneItem / 2 - 7, 2);
//		
		
		if(tabSelect==DONE_QUEST)
			xp += wMainTab;
		
		xselect = xp;
//		paintTabFocus(g, xselect, yp - 1, wMainTab);//paint tab focus
		
		//paint string tab is selected
		mFont f = mFont.tahoma_7b_white;
		if (Focus == TAB)
			f = mFont.tahoma_7_white;
//		f.drawString(g, str[tabSelect], xselect + wMainTab / 2, yp + wOneItem/ 2 - 7, 2);
	}
	
	/*
	 * Paint background tab  working and done
	 */
	public void paintTabBig(mGraphics g, int x, int y, int wMainTab) {
		if (GameCanvas.lowGraphic) {
			paintRectLowGraphic(g, x, y, wMainTab, wOneItem - wOne5 + 1, 3);
		} else {
			for (int i = 0; i <= wMainTab / 32; i++) {
				if (i == 0)
					g.drawRegion(imgTab[11], 0, 0, 32, wOneItem - wOne5 + 1, 0,
							x, y, 0);
				else if (i == wMainTab / 32)
					g.drawRegion(imgTab[11], 0, 0, 32, wOneItem - wOne5 + 1, 2,
							x + wMainTab - 32, y, 0);
				else
					g.drawRegion(imgTab[10], 0, 0, 32, wOneItem - wOne5 + 1, 0,
							x + i * 32, y, 0);

			}
		}
	}
	
	/*
	 * Paint background tab is selected
	 */
	public void paintTabFocus(mGraphics g, int x, int y, int wMainTab) {
		if (GameCanvas.lowGraphic) {
			paintRectLowGraphic(g, x, y, wMainTab, 32, 2);
		} else {
			for (int i = 0; i <= wMainTab / 32; i++) {
				if (i == 0)
					g.drawImage(imgTab[9], x, y, 0);
				else if (i == wMainTab / 32)
					g.drawRegion(imgTab[9], 0, 0, 32, 32, 2, x + wMainTab - 32,
							y, 0);
				else
					g.drawImage(imgTab[2], x + i * 32, y, 0);

			}
		}
	}
	
	/*
	 * execute touch
	 */
	public void updatePointer() {
		if (GameCanvas.isPointerClick) {
            int xp = base.xTab + wOneItem + wOne5 * 3 + xdich, yp = base.yTab
					+ GameCanvas.h / 5 + wOne5;
			if (GameCanvas.isPointer(xp, yp, wMainTab, wOneItem - wOne5 + 1)) {
				if (tabSelect != 0) {
					tabSelect = 0;
					resetTab(true);
				}
				GameCanvas.isPointerClick = false;
			} else if (GameCanvas.isPointer(xp + wMainTab, yp, wMainTab, wOneItem
					- wOne5 + 1)) {
				if (tabSelect != 1) {
					tabSelect = 1;
					resetTab(true);
				}
				GameCanvas.isPointerClick = false;
			}
		}
	}
	
	public void resetTab(bool isResetCmy) {
		TabScreenNew.timeRepaint = 10;
		int t = 0;
		hmax = t * HQuest - (hblack) + 5;
		if (hmax < 0)
			hmax = 0;
		wMainTab = wOneItem * 3;

	}
}
