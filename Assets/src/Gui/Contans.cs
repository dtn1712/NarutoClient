
public class Contans {
 
	//friend gui
	public const sbyte UN_FRIEND=11;
	public const sbyte CHAT_PRIVATE=12;
	
	//chat friend gui
	public const sbyte BUTTON_SEND=13;
	public const sbyte BUTTON_ICON_CHAT=14;
	
	//chat world gui
	public const int BUTTON_SEND_CHAT_WORLD=21;
	public const int BUTTON_ICON_CHAT_WORLD=15;
	
	//menu
	public const int MENU_LIST_FRIEND=16;
	public const int MENU_LIST_PARTY=17;
	public const int MENU_CHAT_PRIVATE=18;
	public const int MENU_CHAT_WORLD=19;
	public const int MENU_BAG=20;
	public const int MENU_QUEST=23;
	
	//quest
	public const int BUTTON_OPEN=22;
	
	//tab bag
	public const int BUTTON_TAB_BAG=24;
	public const int BUTTON_TAB_INFO_CHAR=25;
	public const int BUTTON_TAB_MYSEFT_NEW=26;
	
	//char info
	public const int CHAR_BROAD=27;
	
	//tab
	public const int CMD_TAB_CLOSE = 28;
	
	//trade:type trade
	public const sbyte INVITE_TRADE = 0;
	public const sbyte ACCEPT_INVITE_TRADE = 1;
	public const sbyte MOVE_ITEM_TRADE = 2;
	public const sbyte TRADE = 3;
	public const sbyte CANCEL_TRADE = 4;
	public const sbyte END_TRADE = 5;
	public const sbyte LOCK_TRADE = 6;//khoa giao dich
	public const sbyte MOVE_MONEY = 7;//xu giao dich
	
	//command trade
	public const int BTN_SELECT= 29;
	public const int BTN_lEFT_ARROW= 30;
	public const int BTN_RIGHT_ARROW = 31;
	public const int BTN_TRADE = 32;
	
	//
	public const int BTN_USE_ITEM = 33;
	public const int BTN_DROP_ITEM = 34;
    public const int BTN_SALE_ITEM = 35;
	
	//menu icon
	public const int ICON_CHAR= 35;
	public const int ICON_MISSION = 36;
	public const int ICON_SHOP = 37;
	public const int ICON_TRADE = 38;
	public const int ICON_CONTACT = 39;
	public const int ICON_TEAM = 42;
	public const int ICON_GIAOTIEP = 43;
	public const int ICON_LOGOUT = 44;
	public const int ICON_IMPROVE=45;
    public const int ICON_DEO_CO = 46;
	
	
	//sub icon friend
	public const int ICON_SUB_FRIEND = 40;
	public const int ICON_SUB_TEAM = 41;
	//skill 
	public const int SKILL0= 42;
	public const int SKILL1= 43;
	public const int SKILL2= 44;
	public const int SKILL3= 45;
	public const int SKILL4= 46;
}
