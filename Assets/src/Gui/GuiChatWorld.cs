
//this class use to paint gui chat world
using System;
public class GuiChatWorld : FatherChat {

   
    public GuiChatWorld(int x,int y)
    {
    	
    	this.popx=x;
    	this.popy=y;
    	initTfied();//init text field
    	tfChar.width =  145;
    	int xBtnChat=popx+tfChar.width;
    	int yBtnChat=tfChar.y;
    	btnChat = new Command("",this,Contans.BUTTON_SEND_CHAT_WORLD,null,0,0);
    	btnChat.setPos(xBtnChat,yBtnChat+2,loadImageInterface.btnSendChat,loadImageInterface.btnSendChatFocus);
    	
    	btnIconChat = new Command("",this,Contans.BUTTON_ICON_CHAT_WORLD,null,0,0);
    	btnIconChat.setPos(xBtnChat+btnChat.w+2 ,yBtnChat+3,loadImageInterface.imgEmo[7],loadImageInterface.imgEmo[7]);
    	
    }
	
    private void Move()
    {
    	int xBtnChat=popx+tfChar.width+3;
    	btnChat.x=xBtnChat+GuiChatClanWorld.xMove - 5;
    	btnIconChat.x=xBtnChat+btnChat.w+3+GuiChatClanWorld.xMove - 7;
    	tfChar.x=popx+GuiChatClanWorld.xMove;
    }
	//execute perform send button
	public override void ActionPerformSend()
	{
		
		string text=tfChar.getText();
		if (text.Equals(""))
			return;
		tfChar.setText("");
		Service.gI().chatGlobal(text,(sbyte)Type_Chat.CHAT_WORLD);//send to server
		
//		tfChar.clearall();
	}
	public override void ActionPerformIconChat()
	{
		if(iconChat==null)
		{
            iconChat = new Iconchat(btnIconChat.x - 160 + GuiChatClanWorld.xMove, btnIconChat.y - 190);
            iconChat.scrMain.selectedItem = -1;
            GameScr.isPaintZone = true;
		}else iconChat = null;
	}
		
	public override void Update()
	{
		if(scrMain!=null &&ChatWorld.listNodeChat.size()>0)
		{

			scrMain.cmtoY = -(poph-tfChar.height-30)+ChatWorld.update();
		}
		tfChar.update();
		if(iconChat!=null)
		{
			iconChat.Updatecm();	
		}else if(scrMain!=null)
		{
			scrMain.updatecm();
		}
		Move();
	}
	
	public override void UpdateKey()
	{
		//press button send
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(btnChat)) {
			if (btnChat!= null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (btnChat != null)
					btnChat.performAction();
			}
		}
		
		//press button icon chat
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(btnIconChat)) {
			if (btnIconChat!= null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (btnIconChat != null)
					btnIconChat.performAction();
			}
		}
		
		//execute gui icon chat
        if (iconChat != null)
        {
            iconChat.updateKeySelectIconChat();
            if (iconChat.indexSelect >= 0)
            {
               // Cout.println(getClass(), "nullll  " + tfChar.getText() + NodeChat.maEmo[iconChat.indexSelect]);
                tfChar.setText(tfChar.getText() + NodeChat.maEmo[iconChat.indexSelect]);
                iconChat.indexSelect = -1;
                GameScr.isPaintZone = false;
                iconChat = null;
                GameCanvas.clearPointerEvent();
            }
            if (iconChat != null && GameCanvas.isPointerJustRelease && !GameCanvas.isPointer(iconChat.xstart, iconChat.ystart, iconChat.columns * indexSize + 2, 5 * indexSize + 2))
            {
                //Cout.println(getClass(), iconChat.indexSelect + "nulllllllllllllllll");
                iconChat.indexSelect = -1;
                GameScr.isPaintZone = false;
                iconChat = null;
            }
        }
		
		if (GameCanvas.isTouch &&scrMain!=null &&iconChat==null) {
			  scrMain.updateKey();
		}
	}
	
	public override void KeyPress(int keyCode)
	{
		tfChar.keyPressed(keyCode);
	}
	
	//init text field
	private void initTfied(){
		
		tfChar = new TField();
//		tfChar.name = "Chat riêng";
		tfChar.width = GameScr.popupW-20;
		tfChar.height =(GameScr.ITEM_HEIGHT + 2)-3;
		tfChar.x = popx;
        Cout.println("*tfChar.heightttttttttttttttttt===" + tfChar.height);
		tfChar.y = popy+poph-2*tfChar.height-2;
		tfChar.isFocus = false;
		tfChar.setIputType(TField.INPUT_TYPE_ANY);
//		tfChar.m = GameMidlet.instance;
//		tfChar.c = MotherCanvas.instance;
//		tfChar.color = 0xffffff;
	}
	
	
	public override void paintContentChatWorld(mGraphics g) {
        GameScr.resetTranslate(g);
        if (scrMain != null && ChatWorld.listNodeChat.size() > 0)
        {
            scrMain.setStyle(ChatWorld.listNodeChat.size(), indexSize, popx + GuiChatClanWorld.xMove, popy - 10, popw, poph - tfChar.height - 40, true, 2);
            scrMain.setClip(g);

            ChatWorld.Paint(g, popx + GuiChatClanWorld.xMove, popy - 20, popw - 45, scrMain);
        }


        GameScr.resetTranslate(g);
        tfChar.paint(g);//text field chat world
        if (iconChat != null)
            iconChat.paint(g);//paint icon chat

        GameScr.resetTranslate(g);
        btnChat.paint(g);//button chat world
        btnIconChat.paint(g);//paint button icon chat

    }
	public override void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case Contans.BUTTON_SEND_CHAT_WORLD: // event click button [send] in gui chat world
			ActionPerformSend();
			tfChar.isFocus = false;
			tfChar.clear();
//			scrMain.clear();
			break;
			
		case Contans.BUTTON_ICON_CHAT_WORLD: // event click button [icon] in gui chat world
				
				ActionPerformIconChat();
			
			break;

		default:
			break;
		}
		}

}
