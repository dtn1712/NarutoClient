
/*
 * this class inventory tab
 */
using System;
public class TabBag : MainTabNew{

    int maxw, maxh, indexPaint = 12, winfo = 140;
    int numW = 5, numH = 6, numHPaint, maxSize = 60;
//	int[] xItem = new int[Char.myChar().arrItemBag.Length], yItem = new int[Char.myChar().arrItemBag.Length];
	int[] xItem = new int[9], yItem = new int[9];
	public ListNew listContent = null;
	ListNew list;
	public int idSelect = 0;
    Command cmdXoaItem;//drop item
    Command cmdSelect, cmdSale;//use
	int hcmd = 0;
    mBitmap imgCoins1, imgCoins2;
    public int coutFc;


	public TabBag(string name)
	{
        //		if (GameCanvas.isTouch)
        //			idSelect = -1;
        //		else
        //			idSelect = 0;
        idSelect = -1;
        typeTab = INVENTORY;
        this.nameTab = name;
        xBegin = base.xTab + wOneItem / 2 + 4;
        yBegin = base.yTab + 30 + wOneItem - 5;
        maxw = (wblack - 8) / 32;
        maxh = (hblack - 8) / 32;
        cmdSelect = new Command("Sử dụng", this, Contans.BTN_USE_ITEM, null, 0, 0);
        cmdSelect.setPos(xBegin - widthSubFrame - 21, yBegin + heightSubFrame * 2 / 3 - 10, loadImageInterface.img_use, loadImageInterface.img_use_focus);
        cmdSale = new Command("Bán ", this, Contans.BTN_SALE_ITEM, null, 0, 0);
        cmdSale.setPos(xBegin - widthSubFrame / 2 - 26 - Image.getWidth(loadImageInterface.img_use_focus) / 2,
                yBegin + heightSubFrame * 2 / 3 - 14 - Image.getHeight(loadImageInterface.img_use_focus), loadImageInterface.img_use, loadImageInterface.img_use_focus);

        cmdXoaItem = new Command("Vứt bỏ", this, 36, null, 0, 0);
        cmdXoaItem.setPos(xBegin + Image.getWidth(loadImageInterface.img_use) - widthSubFrame - 13, yBegin + heightSubFrame * 2 / 3 - 10, loadImageInterface.img_use, loadImageInterface.img_use_focus);
        init();

    }
	private void LoadImage()
	{
		imgCoins1=GameCanvas.loadImage("/GuiNaruto/myseft/coins1.png");
		imgCoins2=GameCanvas.loadImage("/GuiNaruto/myseft/coins2.png");
	}
	int cItem = 0, rItem = 0;
	public override void paint(mGraphics g) 
	{
        g.setColor(color[1]);
        int dem = 0;

        //money
        g.drawImage(imgCoins1, xBegin - 7, yBegin - Image.getHeight(imgCoins1) - 1, 0);
        mFont.tahoma_7b_white.drawString(g, Char.myChar().xu + "",
                xBegin + 18, yBegin - Image.getHeight(imgCoins1) / 2 - 4, 0);

        //gold
        g.drawImage(imgCoins2, xBegin + (Image.getWidth(loadImageInterface.ImgItem)) * 5 + 9, yBegin - Image.getHeight(imgCoins2) - 1, mGraphics.TOP | mGraphics.RIGHT);
        mFont.tahoma_7b_white.drawString(g, Char.myChar().luong + "",
                xBegin + (Image.getWidth(loadImageInterface.ImgItem) + 2) * 5 - Image.getWidth(imgCoins1) + 34, yBegin - Image.getHeight(imgCoins1) / 2 - 4, 0);
        //paint image item
        for (int i = 0; i < numW; i++)
            for (int j = 0; j < numH; j++)
            {
                g.drawImage(loadImageInterface.ImgItem, xBegin + (Image.getWidth(loadImageInterface.ImgItem)) * i, yBegin + (Image.getHeight(loadImageInterface.ImgItem)) * j + 2, 0);
            }

        // paint item
        if (Char.myChar().arrItemBag != null)
            for (int i = 0; i < Char.myChar().arrItemBag.Length; i++)
            {
                dem++;
                if (i > 29) return;
                Item it = Char.myChar().arrItemBag[i];
                if (it == null) continue;
                int r = it.indexUI / numW;
                int c = it.indexUI - (r * numW);


                if (it != null)
                {
                    //				it.paintItem(g, xBegin+(Image.getWidth(loadImageInterface.ImgItem)+2)*c + 13, 
                    //						yBegin+(Image.getHeight(loadImageInterface.ImgItem)+2)*r + 13);
                    //				System.out.println(" LOCAT ----> "+(((xBegin)+(Image.getWidth(loadImageInterface.ImgItem)+2)*c + 13)));
                    it.paintItem(g, xBegin + (Image.getWidth(loadImageInterface.ImgItem)) * c + 14,
                            (yBegin + (Image.getHeight(loadImageInterface.ImgItem)) * r + 14) + 2);
                }



                //			if (!GameCanvas.menu2.isShowMenu && GameCanvas.currentDialog == null) {
                if (Focus == INFO && timePaintInfo > timeRequest)
                {
                    ////					GameCanvas.menu2.isShowMenu = true;

                    //paintNameItem(g, xTab-widthSubFrame ,yTab-30+ GameCanvas.h / 5, longwidth, name, colorName);
                    //					paintPopupContent(g, false);

                }
                //			}
            }
        if (idSelect > -1 && Focus == INFO)
        {//focus
            setPaintInfo();
            Paint.paintFocus(g,
                     (xBegin + ((idSelect % numW) * (Image.getWidth(loadImageInterface.ImgItem)))) + 11 - loadImageInterface.ImgItem.getWidth() / 4,
                     yBegin + (idSelect / numW) * (Image.getHeight(loadImageInterface.ImgItem)) + 13 - loadImageInterface.ImgItem.getWidth() / 4
                    , loadImageInterface.ImgItem.getWidth() - 9, loadImageInterface.ImgItem.getWidth() - 9, coutFc);
            //			if(GameCanvas.gameTick%10==0)
            //				g.drawImage(loadImageInterface.imgFocusSelectItem0, (xBegin + ((idSelect % numW)*(Image.getWidth(loadImageInterface.ImgItem))))+14 ,
            //			yBegin
            //						+ (idSelect / numW) * (Image.getHeight(loadImageInterface.ImgItem) ) +16, mGraphics.VCENTER|mGraphics.HCENTER);
            //			else
            //				g.drawImage(loadImageInterface.imgFocusSelectItem1, (xBegin + ((idSelect % numW)*(Image.getWidth(loadImageInterface.ImgItem))))+14 , yBegin
            //						+ (idSelect / numW) * (Image.getHeight(loadImageInterface.ImgItem) ) +16, mGraphics.VCENTER|mGraphics.HCENTER);
            //			Paint.SubFrame(xTab-widthSubFrame,yTab,widthSubFrame,heightSubFrame, g);
            Paint.paintItemInfo(g, itemFocus, xTab - widthSubFrame - 5, yTab + 10);
            cmdSelect.paint(g);
            cmdXoaItem.paint(g);
            cmdSale.paint(g);
            //			paint.paintItemInfo(g, itemFocus, xTab-widthSubFrame + 10,yTab-25+ GameCanvas.h / 5);
            //			paintNameItem(g, xTab-widthSubFrame + 20,(yTab-30+ GameCanvas.h / 5) + 20, longwidth, name, colorName);
            //			paintNameItem(g, xTab-widthSubFrame + 20,(yTab-30+ GameCanvas.h / 5) + 30, longwidth, "Táº¥n cÃ´ng: 1000", colorName);
            //			paintNameItem(g, xTab-widthSubFrame + 20,(yTab-30+ GameCanvas.h / 5) + 40, longwidth, "Äá»™ bá»n: 10", colorName);
            //			paintNameItem(g, xTab-widthSubFrame + 20,(yTab-30+ GameCanvas.h / 5) + 50, longwidth, "táº¯ng sÃ¡t thÆ°Æ¡ng váº­t lÃ½ 10%", colorName);
            //			paintPopupContent(g, false);
        }

        //		paintRect(g);

    }
	
	
	
	public Item getItem(Item[] item){
		for(int i = 0; i < item.Length; i++){
			Item it = item[i];
			return it;
		}
		return null;
	}
	public override void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
        switch (idAction)
        {
            case Contans.BTN_USE_ITEM:
              //  Cout.println("su dung");
                Service.gI().useItem((sbyte)idSelect, Char.myChar().arrItemBag[idSelect]);
                idSelect = -1;
                //			Service.gI().giveUpItem((byte)idSelect);
                break;
            case Contans.BTN_SALE_ITEM:
                if (Char.myChar().arrItemBag != null && idSelect > -1 && idSelect < Char.myChar().arrItemBag.Length
                && Char.myChar().arrItemBag[idSelect].template != null)
                    GameCanvas.startYesNoDlg("Bạn có muốn bán " + Char.myChar().arrItemBag[idSelect].template.name + " không?",
                            new Command("Có", this, 37, null, 0, 0),
                            new Command("Không", GameCanvas.instance, GameCanvas.cEndDgl, null, 0, 0));

                break;
            case 36:

                //Cout.println("hoi vuetttttttttttt ");
                if (Char.myChar().arrItemBag != null && idSelect > -1 && idSelect < Char.myChar().arrItemBag.Length
                && Char.myChar().arrItemBag[idSelect].template != null)
                    GameCanvas.startYesNoDlg("Bạn có muốn vứt bỏ " + Char.myChar().arrItemBag[idSelect].template.name + " không?",
                            new Command("Có", this, Contans.BTN_DROP_ITEM, null, 0, 0),
                            new Command("Không", GameCanvas.instance, GameCanvas.cEndDgl, null, 0, 0));
                break;
            case Contans.BTN_DROP_ITEM:
                //Cout.println("ok vuetttttttttttt ");
                //			Service.gI().useItem((byte)idSelect, Char.myChar().arrItemBag[idSelect]);
                Service.gI().giveUpItem((sbyte)idSelect);
                GameCanvas.endDlg();
                idSelect = -1;
                break;
            case 37: //ok ban item
               
                GameCanvas.endDlg();
                break;
            default:
                break;
        }
    }
	public override void updatePointer() {
        if (imgCoins1 == null)
        {
            LoadImage();
        }
        if (GameCanvas.gameTick % 4 == 0)
        {
            coutFc++;
            if (coutFc > 2)
                coutFc = 0;
        }
        if (GameCanvas.isPointerJustRelease)
            if (GameCanvas.isPointerJustRelease && GameCanvas.isPointSelect(xBegin, yBegin, numW * (Image.getWidth(loadImageInterface.ImgItem) + 2),
                    numH * (Image.getHeight(loadImageInterface.ImgItem) + 4)))
            {
                GameCanvas.isPointerJustRelease = false;
                int row = (GameCanvas.px - xBegin) / (Image.getWidth(loadImageInterface.ImgItem));
                int col = (GameCanvas.py - yBegin) / (Image.getWidth(loadImageInterface.ImgItem));

                row = (GameCanvas.px - xBegin - (row - 1) * 2) / (Image.getWidth(loadImageInterface.ImgItem));
                col = (GameCanvas.py - yBegin - (col - 1) * 2) / (Image.getWidth(loadImageInterface.ImgItem));


                if (row > 6)
                    row = 6;
                if (col > 5)
                    col = 5;

                int select = row + col * numW;

                int size = 0;
                if (typeTab == INVENTORY && Char.myChar().arrItemBag != null)
                {
                    size = Char.myChar().arrItemBag.Length;

                }
                Cout.println(idSelect + "  select  " + select);
                if (select >= 0 && select < size)
                {
                    GameCanvas.isPointerClick = false;
                    if (select == idSelect)
                    {


                    }
                    else
                    {
                        timePaintInfo = 0;
                        idSelect = select;
                        listContent = null;
                    }
                    if (MainTabNew.Focus != MainTabNew.INFO)
                        MainTabNew.Focus = MainTabNew.INFO;
                }
                else
                {

                    idSelect = -1;
                    Cout.println("------------- set - 1");
                    timePaintInfo = 0;
                    listContent = null;
                }

            }

        //		if (timePaintInfo == MainTabNew.timeRequest) {
        //			setPaintInfo();
        //		}
    }
	
	
	public override void updateKey() {
		// TODO Auto-generated method stub
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdSelect)) {
			if (cmdSelect != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdSelect != null)
					cmdSelect.performAction();
			}
		}
		
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdXoaItem)) {
			if (cmdXoaItem != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdXoaItem != null)
					cmdXoaItem.performAction();
			}
		}
        if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdSale))
        {
            if (cmdSale != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                Screen.keyTouch = -1;
                if (cmdSale != null)
                    cmdSale.performAction();
            }
        }
    }
	Item itemFocus = null;
    public void setPaintInfo()
    {
        if (idSelect < Char.myChar().arrItemBag.Length)
        {
            Item item = Char.myChar().arrItemBag[idSelect];
            itemFocus = Char.myChar().arrItemBag[idSelect];
            if (item.template != null)
                name = item.template.name;
        }

    }
}
