

using System;
public class GuiMain : Screen , IActionListener{
	public static int cmdBarX, cmdBarY, cmdBarW, cmdBarLeftW, cmdBarRightW, cmdBarCenterW, hpBarX, hpBarY, hpBarW, mpBarW, expBarW, lvPosX,
	moneyPosX, hpBarH, girlHPBarY;
	public static int xL, yL; // left
	public static int xC, yC;
	public static int xR, yR; // right
	public static int xF, yF; // fire
	public static int xU, yU; // up
    public static int xCenter, yCenter;
    Command btnchangeSkill;
    Command btnBatTestSkill;
    Command btnChangeFocus;
    Command bntCharBoard;
    public Command bntAttack;
    public Command bntAttack_1;
    public Command bntAttack_2;
    public Command bntAttack_3;
    public Command bntAttack_4;
	
	int xMove,limMove=45;
	public bool moveClose,moveOpen;
	int v;//gia toc tang toc do move
	int y=GameCanvas.h-43;
	int timeoff=10;//30s
	long timestart;
    public static bool isTestSkill, isPaintOjectMap = true;
    public static int indexSkillTest = 0;
    public int[][] xySkill = new int[5][];
    public static mVector vecItemOther = new mVector();
	
	//menu icon
	public MenuIcon menuIcon= new MenuIcon(GameCanvas.hw-215,GameCanvas.h-50);
	
	public GuiMain()
	{
        int xAttack = GameCanvas.w - 50/*loadImageInterface.imgAttack_1.getWidth()*/- 10;
        int yAttack = GameCanvas.h - 50/*loadImageInterface.imgAttack_1.getHeight()*/- 10;

        for (int i = 0; i < xySkill.Length; i++)
        {
            xySkill[i] = new int[2];
        }
        bntCharBoard = new Command("", this, Contans.CHAR_BROAD, null, 0, 0);
        bntCharBoard.setPos(1, 1, loadImageInterface.imgCharacter_info, loadImageInterface.imgCharacter_info);

        btnchangeSkill = new Command("ChangeKill", this, 20, null, 0, 0);
        btnchangeSkill.setPos(GameCanvas.w - 60, 1, loadImageInterface.img_use, loadImageInterface.img_use_focus);

        btnBatTestSkill = new Command("BatTest", this, 21, null, 0, 0);
        btnBatTestSkill.setPos(GameCanvas.w - 120, 1, loadImageInterface.img_use, loadImageInterface.img_use_focus);



        xySkill[0][0] = xAttack + loadImageInterface.imgAttack_1.getWidth() / 2; xySkill[0][1] = yAttack + loadImageInterface.imgAttack_1.getHeight() / 2;
        bntAttack = new Command("", this, Contans.CHAR_BROAD, null, 0, 0);
        bntAttack.setPos(xAttack, yAttack, loadImageInterface.imgAttack_1, loadImageInterface.imgAttack_1);

        xAttack = xAttack - Image.getWidth(loadImageInterface.imgAttack);
        yAttack = yAttack + Image.getHeight(loadImageInterface.imgAttack_1) / 2;
        xySkill[1][0] = xAttack + loadImageInterface.imgAttack.getWidth() / 2; xySkill[1][1] = yAttack + loadImageInterface.imgAttack.getHeight() / 2;
        bntAttack_1 = new Command("", this, Contans.CHAR_BROAD, null, 0, 0);
        bntAttack_1.setPos(xAttack, yAttack, loadImageInterface.imgAttack, loadImageInterface.imgAttack);

        xAttack = xAttack + 5;
        yAttack = yAttack - Image.getHeight(loadImageInterface.imgAttack) - 5;
        xySkill[2][0] = xAttack + loadImageInterface.imgAttack.getWidth() / 2; xySkill[2][1] = yAttack + loadImageInterface.imgAttack.getHeight() / 2;
        bntAttack_2 = new Command("", this, Contans.CHAR_BROAD, null, 0, 0);
        bntAttack_2.setPos(xAttack, yAttack, loadImageInterface.imgAttack, loadImageInterface.imgAttack);

        xAttack = xAttack + Image.getWidth(loadImageInterface.imgAttack) / 3 + 10;
        yAttack = yAttack - Image.getHeight(loadImageInterface.imgAttack) + 5;
        xySkill[3][0] = xAttack + loadImageInterface.imgAttack.getWidth() / 2; xySkill[3][1] = yAttack + loadImageInterface.imgAttack.getHeight() / 2;
        bntAttack_3 = new Command("", this, Contans.CHAR_BROAD, null, 0, 0);
        bntAttack_3.setPos(xAttack, yAttack, loadImageInterface.imgAttack, loadImageInterface.imgAttack);

        xAttack = xAttack + Image.getWidth(loadImageInterface.imgAttack) + 5;
        yAttack = yAttack;
        xySkill[4][0] = xAttack + loadImageInterface.imgAttack.getWidth() / 2; xySkill[4][1] = yAttack + loadImageInterface.imgAttack.getHeight() / 2;
        bntAttack_4 = new Command("", this, Contans.CHAR_BROAD, null, 0, 0);
        bntAttack_4.setPos(xAttack, yAttack, loadImageInterface.imgAttack, loadImageInterface.imgAttack);


    }
	public static int indexshow;
	public void update()
	{
        //		updateKey();
        //		updatePointer();
        //		if(GameScr.isShowFocus){
        //			if(GameScr.ypaintFocus<0)
        //			indexshow++;
        //			GameScr.ypaintFocus+=indexshow;
        //			if(GameScr.ypaintFocus>0) GameScr.ypaintFocus = 0;
        //		}else {
        //			if(GameScr.ypaintFocus>-loadImageInterface.imgfocusActor.getHeight())
        //			indexshow++;
        //			GameScr.ypaintFocus-=indexshow;
        //			if(GameScr.ypaintFocus<-loadImageInterface.imgfocusActor.getHeight()) GameScr.ypaintFocus = -loadImageInterface.imgfocusActor.getHeight();
        //		}
        for (int i = 0; i < vecItemOther.size(); i++)
        {
            ItemThucAn item = (ItemThucAn)vecItemOther.elementAt(i);
            item.update();
        }
		if(moveClose)
		{
			if(xMove>limMove)
			{
				v=0;
			}
			else
			{
				xMove+=v;
				v++;
			}
			
			
		}else
		{
			
			if(xMove<=0)
			{
				v=0;
			}
			else
			{
				xMove+=v;
				v--;
			}
            if ((mSystem.currentTimeMillis() - timestart) / 1000 > timeoff)
            {
                moveClose = true;
                if (menuIcon.subMenu != null) menuIcon.cmdClose.performAction();
            }
        }
        if (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY)
            return;
        if (GameCanvas.gameScr != null && GameCanvas.gameScr.guiChatClanWorld != null && !GameCanvas.gameScr.guiChatClanWorld.moveClose)
            menuIcon.update();
    }
	
	
	public void UpdateKey()
	{
        if (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY)
            return;
        bool isPress = false;
        if (!MenuIcon.isShowTab && !GameScr.gI().guiChatClanWorld.moveClose && moveClose)
        {
            isPress = updateKeyTouchControl();
            //			updateRightCOntrol();
        }
        if (!GameCanvas.gameScr.guiChatClanWorld.moveClose && menuIcon.indexpICon == 0 &&/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(bntCharBoard))
        {

            if (bntCharBoard != null)
            {
                if (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY)
                {
                    GameCanvas.startCommandDlg("Bạn có muốn hồi sinh tại chỗ (10xu)?",
                            new Command("Hồi sinh", GameCanvas.instance, GameCanvas.cHoiSinh, null),
                            new Command("Về làng", GameCanvas.instance, GameCanvas.cVeLang, null));
                    return;
                }
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                Screen.keyTouch = -1;
                isPress = true;
                if (bntCharBoard != null)
                {
                    bntCharBoard.performAction();
                }
            }
        }
        if (LoginScr.isTest)
        {
            if (menuIcon.indexpICon == 0 &&/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(btnchangeSkill))
            {

                if (bntCharBoard != null)
                {
                    Cout.println("btnchangeSkill  " + GameCanvas.isPointerJustRelease);
                    GameCanvas.isPointerJustRelease = false;
                    GameCanvas.keyPressed[5] = false;
                    Screen.keyTouch = -1;
                    isPress = true;
                    if (btnchangeSkill != null)
                    {
                        btnchangeSkill.performAction();
                    }
                }
            }
            if (menuIcon.indexpICon == 0 &&/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(btnBatTestSkill))
            {

                if (bntCharBoard != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    GameCanvas.keyPressed[5] = false;
                    Screen.keyTouch = -1;
                    isPress = true;
                    if (btnBatTestSkill != null)
                    {
                        btnBatTestSkill.performAction();
                    }
                }
            }
        }


        if (!GameCanvas.gameScr.guiChatClanWorld.moveClose && (!moveClose||MenuIcon.lastTab.size()>0))
            menuIcon.updateKey();
        if (!isPress && GameCanvas.isPointerJustRelease && GameCanvas.isPointerClick && !moveClose)
        {
            moveClose = true;
            if (menuIcon.subMenu != null) menuIcon.cmdClose.performAction();
        }
        //		GameCanvas.clearKeyPressed();
    }
	public void Paint(mGraphics g)
	{
        //GameScr.paintCmdBar(g);
        int iMppaint = (int)((Char.myChar().cMP * (mGraphics.getImageWidth(loadImageInterface.imgMp) - 19))//pháº§n dÆ° ko paint
                / Char.myChar().cMaxMP);
        g.drawRegion(loadImageInterface.imgMp, 0, 0,
                iMppaint + 19,
                mGraphics.getImageHeight(loadImageInterface.imgMp),
                0, bntCharBoard.x + 36, bntCharBoard.y + 12, mGraphics.TOP | mGraphics.LEFT, false);

        int hppaint = (((Char.myChar().cHP > Char.myChar().cMaxHP ? Char.myChar().cMaxHP : Char.myChar().cHP) * mGraphics.getImageWidth(loadImageInterface.imgBlood)) / Char.myChar().cMaxHP);
        g.drawRegion(loadImageInterface.imgBlood, 0, 0,
                hppaint,
                mGraphics.getImageHeight(loadImageInterface.imgBlood),
                0, bntCharBoard.x + 56, bntCharBoard.y + 21, mGraphics.TOP | mGraphics.LEFT, false);
        int Exppaint = (int)((Char.myChar().cEXP * mGraphics.getImageWidth(loadImageInterface.imgExp)) / Char.myChar().cMaxEXP);
        g.drawRegion(loadImageInterface.imgExp, 0, 0,
                Exppaint,
                mGraphics.getImageHeight(loadImageInterface.imgExp),
                0, bntCharBoard.x + 54, bntCharBoard.y + 35, mGraphics.TOP | mGraphics.LEFT, false);
        g.drawRegion(loadImageInterface.icn_Mail, 0, 0,
                21,
                21,
                0, 10, 65, mGraphics.TOP | mGraphics.LEFT, false);
        if (ChatPrivate.nTinChuaDoc > 0)
        {

            mFont.tahoma_7_red.drawStringBorder(g, ChatPrivate.nTinChuaDoc + "", 28,
                    68, 1);
        }
        //		g.setColor(0xff00);
        //		g.fillRect( GameCanvas.w-60,38, 60, 20);
        if (TileMap.mapName != null)
        {
            int www = mFont.tahoma_7_yellow.getWidth(TileMap.mapName) + 8;
            g.drawImage(MsgDlg.imgpopup[0], GameCanvas.w - (www < 40 ? 40 : www), 30, mGraphics.TOP | mGraphics.LEFT);
            mFont.tahoma_7_yellow.drawString(g, TileMap.mapName, GameCanvas.w - 4, 40, 1);
        }

        //		g.drawImage(loadImageInterface.imgMp, bntCharBoard.x+36, bntCharBoard.y+12);
        //		g.drawImage(loadImageInterface.imgBlood, bntCharBoard.x+56, bntCharBoard.y+21);
        //		g.drawImage(loadImageInterface.imgExp, bntCharBoard.x+54, bntCharBoard.y+35);

        if (LoginScr.isTest)
        {
            btnchangeSkill.paint(g);
            btnBatTestSkill.paint(g);
        }
        bntCharBoard.paint(g);

        Part ph = GameScr.parts[Char.myChar().head];
        SmallImage.drawSmallImage(g, ph.pi[Char.CharInfo[0][0][0]].id,
                30, 30, 0, mGraphics.VCENTER | mGraphics.HCENTER);

        if (Char.myChar() != null && Char.myChar().cName != null)
            mFont.tahoma_7b_white.drawString(g, Char.myChar().cName, bntCharBoard.x + 60, bntCharBoard.y + 45, 0);
        mFont.tahoma_7_white.drawString(g, Char.myChar().cHP + "/" + Char.myChar().cMaxHP,
                 bntCharBoard.x + 95, bntCharBoard.y + 20, 2);
        mFont.tahoma_7_white.drawString(g, Char.myChar().cMP + "/" + Char.myChar().cMaxMP,
                 bntCharBoard.x + 95, bntCharBoard.y + 9, 2);
        mFont.tahoma_7_white.drawString(g, Char.myChar().cEXP + "%",
                 bntCharBoard.x + 95, bntCharBoard.y + 31, 2);
        mFont.tahoma_7_white.drawString(g, Char.myChar().clevel + "",
                 bntCharBoard.x + 49, bntCharBoard.y + 44, 2);

        if (moveClose && !MenuIcon.isShowTab)//khi menu hien thi
        {
            paintTouchControl(g);
            bntAttack.paint(g);
            bntAttack_1.paint(g);
            bntAttack_2.paint(g);
            bntAttack_3.paint(g);
            bntAttack_4.paint(g);
            for (int i = 0; i < Char.myChar().mQuickslot.Length; i++)
            {
                QuickSlot ql = Char.myChar().mQuickslot[i];
                if (ql.quickslotType != -1)
                {
                    SkillTemplate skillIndexx = (SkillTemplate)SkillTemplates.hSkilltemplate.get("" + ql.idSkill);
                    if (skillIndexx != null)
                    {
                        //						gettimeCooldown
                        if (ql.canUse())
                            SmallImage.drawSmallImage(g, skillIndexx.iconTron,
                                    xySkill[i][0],
                                    xySkill[i][1], 0, mGraphics.VCENTER | mGraphics.HCENTER, true);
                        else
                        {
                            SmallImage.drawSmallImage(g, skillIndexx.iconTron,
                                    xySkill[i][0],
                                    xySkill[i][1], 0, mGraphics.VCENTER | mGraphics.HCENTER, true, 50);
                            mFont.tahoma_7_white.drawString(g, ql.gettimeCooldown(), xySkill[i][0],
                                    xySkill[i][1] - 6, 2, true);
                        }
                    }
                }
            }
        }

        GameScr.resetTranslate(g);
        if ((Char.myChar().npcFocus != null && Char.myChar().npcFocus.template.typeKhu != -1) || Char.myChar().mobFocus != null || Char.myChar().charFocus != null
                || Char.myChar().itemFocus != null)
        {

            int hppaintFocus = 0;
            if (Char.myChar().npcFocus != null) hppaintFocus = mGraphics.getImageWidth(loadImageInterface.imgBlood);
            else if (Char.myChar().charFocus != null)
            {
                hppaintFocus = (((Char.myChar().charFocus.cHP > Char.myChar().charFocus.cMaxHP ? Char.myChar().charFocus.cMaxHP : Char.myChar().charFocus.cHP)
                        * mGraphics.getImageWidth(loadImageInterface.imgBlood)) / Char.myChar().charFocus.cMaxHP);
                //				
                //				hppaintFocus = Char.myChar().charFocus.cHP*mGraphics.getImageWidth(loadImageInterface.imgBlood)/Char.myChar().charFocus.cMaxHP;
            }
            else if (Char.myChar().mobFocus != null)
            {
                Char.myChar().mobFocus.hp = (Char.myChar().mobFocus.hp < 0 ? 0 : Char.myChar().mobFocus.hp);
                hppaintFocus = (((Char.myChar().mobFocus.hp > Char.myChar().mobFocus.maxHp ? Char.myChar().mobFocus.maxHp : Char.myChar().mobFocus.hp)
                        * mGraphics.getImageWidth(loadImageInterface.imgBlood)) / Char.myChar().mobFocus.maxHp);
                //				hppaintFocus = Char.myChar().mobFocus.hp*mGraphics.getImageWidth(loadImageInterface.imgBlood)/Char.myChar().mobFocus.maxHp;
            }
            g.drawRegion(loadImageInterface.imgBlood, 0, 0,
                    hppaintFocus,
                    mGraphics.getImageHeight(loadImageInterface.imgBlood),
                    0, GameCanvas.w / 2 + 24, loadImageInterface.imgfocusActor.getHeight() / 2 - 6/*+GameScr.ypaintFocus*/, mGraphics.TOP | mGraphics.LEFT, false);

            g.drawImage(loadImageInterface.imgfocusActor, GameCanvas.w / 2 + 43, loadImageInterface.imgfocusActor.getHeight() / 2/*+GameScr.ypaintFocus*/, mGraphics.VCENTER | mGraphics.HCENTER);
            if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.template.name != null && Char.myChar().npcFocus.template.typeKhu != -1)
            {
                mFont.tahoma_7b_white.drawString(g, Char.myChar().npcFocus.template.name, GameCanvas.w / 2 + 28, loadImageInterface.imgfocusActor.getHeight() / 2 + 10/*+GameScr.ypaintFocus*/, 0);

                try
                {
                    Part ph2 = GameScr.parts[Char.myChar().npcFocus.template.headId];
                    SmallImage.drawSmallImage(g, ph2.pi[Char.CharInfo[0][0][0]].id,
                            GameCanvas.w / 2,
                            loadImageInterface.imgfocusActor.getHeight() / 2/*+GameScr.ypaintFocus*/, 0, mGraphics.VCENTER | mGraphics.HCENTER);
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }
            }
            else if (Char.myChar().charFocus != null && Char.myChar().charFocus.cName != null)
            {
                mFont.tahoma_7b_white.drawString(g, Char.myChar().charFocus.cName, GameCanvas.w / 2 + 28, loadImageInterface.imgfocusActor.getHeight() / 2 + 10/*+GameScr.ypaintFocus*/, 0);
                mFont.tahoma_7_white.drawString(g, Char.myChar().charFocus.cHP + "/" + Char.myChar().charFocus.cMaxHP,
                        GameCanvas.w / 2 + 68, loadImageInterface.imgfocusActor.getHeight() / 2 - 7/*+GameScr.ypaintFocus*/, 2);
                mFont.tahoma_7b_white.drawString(g, Char.myChar().charFocus.clevel + "", GameCanvas.w / 2 + 17,
                        loadImageInterface.imgfocusActor.getHeight() / 2 + 11/*+GameScr.ypaintFocus*/, 2);
                try
                {
                    Part ph2 = GameScr.parts[Char.myChar().charFocus.head];
                    SmallImage.drawSmallImage(g, ph2.pi[Char.CharInfo[0][0][0]].id,
                            GameCanvas.w / 2,
                            loadImageInterface.imgfocusActor.getHeight() / 2/*+GameScr.ypaintFocus*/, 0, mGraphics.VCENTER | mGraphics.HCENTER);
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }

            }
            else if (Char.myChar().mobFocus != null && Char.myChar().mobFocus.mobName != null)
            {
                mFont.tahoma_7_white.drawString(g, Char.myChar().mobFocus.hp + "/" + Char.myChar().mobFocus.maxHp,
                        GameCanvas.w / 2 + 68, loadImageInterface.imgfocusActor.getHeight() / 2 - 7/*+GameScr.ypaintFocus*/, 2);
                try
                {
                    mFont.tahoma_7b_white.drawString(g, Char.myChar().mobFocus.mobName, GameCanvas.w / 2 + 28, loadImageInterface.imgfocusActor.getHeight() / 2 + 10/*+GameScr.ypaintFocus*/, 0);
                    mFont.tahoma_7b_white.drawString(g, Char.myChar().mobFocus.level + "", GameCanvas.w / 2 + 17,
                            loadImageInterface.imgfocusActor.getHeight() / 2 + 11/*+GameScr.ypaintFocus*/, 2);
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }

            }
            else if (Char.myChar().itemFocus != null && Char.myChar().itemFocus.template != null)
            {
                mFont.tahoma_7b_white.drawString(g, Char.myChar().itemFocus.template.name, GameCanvas.w / 2 + 28, loadImageInterface.imgfocusActor.getHeight() / 2 + 10/*+GameScr.ypaintFocus*/, 0);

            }

        }
        for (int i = 0; i < vecItemOther.size(); i++)
        {
            ItemThucAn item = (ItemThucAn)vecItemOther.elementAt(i);
            item.paint(g, 20 * (i) + 14, bntCharBoard.y + 85);
        }
        menuIcon.y = this.y + xMove;
        menuIcon.paint(g);

    }

	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
        switch (idAction)
        {
            case Contans.CHAR_BROAD:

                if (!moveClose)
                {
                    moveClose = true;
                }
                else
                {
                    moveClose = false;
                }
               // Cout.println(MenuIcon.isShowTab + "moveClose   ===== " + moveClose);
                if (!moveClose)
                    timestart = mSystem.currentTimeMillis();
                break;
            case 20:
                indexSkillTest = (indexSkillTest + 1) % GameScr.sks.Length;
                isPaintOjectMap = !isPaintOjectMap;
                break;
            case 21:
                isTestSkill = !isTestSkill;
                break;

            default:
                break;
        }
    }
	public static void paintCmdBar(mGraphics g) {
		Char.myChar().cHP  = 100;
		Char.myChar().cMaxHP = 100;
		Char.myChar().cMP = 50;
		Char.myChar().cMaxMP = 50;
		
		int exp=2;
		int expMax=100;
	
		g.setClip(0, cmdBarY - 4, GameCanvas.w, 100);

		// // PAINT HP
		int hpWidth = Char.myChar().cHP * hpBarW / Char.myChar().cMaxHP;
		Cout.println(" paint  "+loadImageInterface.imgBlood);
		//hp
		if (hpWidth > hpBarW)
			hpWidth = 0;
		g.drawRegion(loadImageInterface.imgBlood,0,0,hpWidth,Image.getHeight(loadImageInterface.imgBlood), 0,hpBarX + 4, hpBarY - 2, 0);

		//mp
		hpWidth=Char.myChar().cMP * Image.getWidth(loadImageInterface.imgExp) / Char.myChar().cMaxMP;
		g.drawRegion(loadImageInterface.imgExp,0,0,hpWidth,Image.getHeight(loadImageInterface.imgExp), 0,hpBarX + 4, hpBarY -12, 0);

		//exp
		hpWidth=exp * Image.getWidth(loadImageInterface.imgExp) / expMax;
		g.drawRegion(loadImageInterface.imgExp,0,0,hpWidth,Image.getHeight(loadImageInterface.imgExp), 0,hpBarX + 4, hpBarY +12, 0);

	}
	
	public static void loadCmdBar() {
//		if (imgCmdBar == null) {
//			imgCmdBar = new Image[2];
//			for (int i = 0; i < 2; i++)
//				imgCmdBar[i] = GameCanvas.loadImage("/u/c" + i + ".png");
//		}
//		cmdBarLeftW = mGraphics.getImageWidth(imgCmdBar[0]);
//		cmdBarRightW = mGraphics.getImageWidth(imgCmdBar[1]);
//		cmdBarCenterW = gW - cmdBarLeftW - cmdBarRightW + 1;

		hpBarX = 78 - 15 + 10;
		hpBarY = cmdBarY;
		hpBarW = GameScr.gW - 84 - 30;
		expBarW = GameScr.gW - 44 - 4;
		hpBarH = 20;

//		if (GameCanvas.w > 176) {
//			cmdBarCenterW -= 50;
//			hpBarW -= 50;
//			expBarW -= 50;
//			hpBarX += 15;
//			hpBarW -= 15;
//		}
		loadInforBar();
	}
	
	public static void loadInforBar() {
		if (!GameCanvas.isTouch)
			return;
		hpBarW = 83;
		mpBarW = 57;
		hpBarX = 52;
		hpBarY = 10 + Info.hI;
		expBarW = GameScr.gW - 61;
	}
	
	public static void paintTouchControl(mGraphics g) {
        try
        {
            GameScr.resetTranslate(g);
            //			// ---CHAT
            //			g.drawImage(GameScr.imgChat, xC + 17, yC + 17, mGraphics.HCENTER | mGraphics.VCENTER);
            //			

            // ---LEFT
            //			g.drawImage(imgButton, xL, yL, 0);
            g.drawRegion(loadImageInterface.imgMoveNormal, 0, 0, Image.getWidth(loadImageInterface.imgMoveNormal), Image.getHeight(loadImageInterface.imgMoveNormal), Sprite.TRANS_MIRROR, xL, yL,
                    mGraphics.HCENTER | mGraphics.VCENTER);
            if (keyTouch == 4)
            {
                //				g.drawImage(imgButton2, xL, yL, 0);
                g.drawRegion(loadImageInterface.imgMoveFocus, 0, 0, Image.getWidth(loadImageInterface.imgMoveNormal), Image.getHeight(loadImageInterface.imgMoveNormal), Sprite.TRANS_ROT180, xL, yL, mGraphics.HCENTER | mGraphics.VCENTER);
            }

            // ----RIGHT
            //			g.drawImage(imgButton, xR, yR, 0);
            g.drawRegion(loadImageInterface.imgMoveNormal, 0, 0, Image.getWidth(loadImageInterface.imgMoveNormal), Image.getHeight(loadImageInterface.imgMoveNormal), 0, xR, yR, mGraphics.HCENTER
                    | mGraphics.VCENTER);
            if (keyTouch == 6)
            {
                //				g.drawImage(imgButton2, xR, yR, 0);
                g.drawRegion(loadImageInterface.imgMoveFocus, 0, 0, Image.getWidth(loadImageInterface.imgMoveNormal), Image.getHeight(loadImageInterface.imgMoveNormal), 0, xR, yR,
                        mGraphics.HCENTER | mGraphics.VCENTER);
            }

            // ----UP
            //			g.drawImage(imgButton, xU, yU, 0);
            g.drawRegion(loadImageInterface.imgMoveNormal, 0, 0, Image.getWidth(loadImageInterface.imgMoveNormal), Image.getHeight(loadImageInterface.imgMoveNormal), Sprite.TRANS_MIRROR_ROT90, xU,
                    yU, mGraphics.HCENTER | mGraphics.VCENTER);
            if (keyTouch == 3)
            {
                //				g.drawImage(imgButton2, xU, yU, 0);
                g.drawRegion(loadImageInterface.imgMoveFocus, 0, 0, Image.getWidth(loadImageInterface.imgMoveFocus), Image.getHeight(loadImageInterface.imgMoveFocus), Sprite.TRANS_MIRROR_ROT90, xU,
                        yU, mGraphics.HCENTER | mGraphics.VCENTER);
            }
            // CENTER
            g.drawImage(loadImageInterface.imgMoveCenter, xCenter, yCenter, mGraphics.HCENTER | mGraphics.VCENTER);

        }
        catch (Exception e)
        {
            // TODO: handle exception
           // e.printStackTrace();
        }
        //		if (!GameCanvas.isTouch || (GameCanvas.menu.showMenu && GameCanvas.isTouchControlSmallScreen))
        //			return;
        //		if (GameCanvas.currentDialog != null || ChatPopup.currentMultilineChatPopup != null || GameCanvas.menu.showMenu || isPaintPopup())
        //			return;

    }
	
	public static bool updateKeyTouchControl() {
		bool isPress = false;
        keyTouch = -1;
        //		if (GameCanvas.isPointerHoldIn(TileMap.posMiniMapX, TileMap.posMiniMapY, TileMap.wMiniMap, TileMap.hMiniMap)) {
        //			if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease) {
        //				doShowMap();
        //				isPress = true;
        //			}
        //		}

        //		if ( isNotPaintTouchControl())
        //			return;
        if (GameCanvas.isPointerHoldIn(xU - Image.getHeight(loadImageInterface.imgMoveNormal) / 2, yU - Image.getWidth(loadImageInterface.imgMoveNormal) / 2,
                Image.getHeight(loadImageInterface.imgMoveNormal), Image.getWidth(loadImageInterface.imgMoveNormal) / 2))
        {
            keyTouch = 3;
            GameCanvas.keyHold[2] = true;
            //			resetAuto();
            isPress = true;
        }
        else if (GameCanvas.isPointerDown)
        {
            GameCanvas.keyHold[2] = false;
        }

        if (GameCanvas.isPointerHoldIn(xU - Image.getHeight(loadImageInterface.imgMoveNormal) / 2 - Image.getWidth(loadImageInterface.imgMoveNormal) / 2,
                yU - Image.getWidth(loadImageInterface.imgMoveNormal) / 2,
                Image.getWidth(loadImageInterface.imgMoveNormal) - Image.getHeight(loadImageInterface.imgMoveNormal) / 2,
                Image.getWidth(loadImageInterface.imgMoveNormal) - Image.getHeight(loadImageInterface.imgMoveNormal) / 2))
        {

            GameCanvas.keyHold[1] = true;
            //			resetAuto();
            isPress = true;
        }
        else if (GameCanvas.isPointerDown)
            GameCanvas.keyHold[1] = false;

        if (GameCanvas.isPointerHoldIn(xU + Image.getHeight(loadImageInterface.imgMoveNormal) / 2,
                yU - Image.getWidth(loadImageInterface.imgMoveNormal) / 2,
                Image.getWidth(loadImageInterface.imgMoveNormal) - Image.getHeight(loadImageInterface.imgMoveNormal) / 2,
                Image.getWidth(loadImageInterface.imgMoveNormal) - Image.getHeight(loadImageInterface.imgMoveNormal) / 2))
        {

            GameCanvas.keyHold[3] = true;
            //			resetAuto();
            isPress = true;
        }
        else if (GameCanvas.isPointerDown)
            GameCanvas.keyHold[3] = false;

        if (GameCanvas.isPointerHoldIn(xL - Image.getWidth(loadImageInterface.imgMoveNormal) / 2, yL - Image.getHeight(loadImageInterface.imgMoveNormal) / 2,
                Image.getWidth(loadImageInterface.imgMoveNormal), Image.getHeight(loadImageInterface.imgMoveNormal)))
        {
            keyTouch = 4;
            GameCanvas.keyHold[4] = true;
            //			resetAuto();
            isPress = true;
        }
        else if (GameCanvas.isPointerDown)
            GameCanvas.keyHold[4] = false;

        if (GameCanvas.isPointerHoldIn(xR - Image.getWidth(loadImageInterface.imgMoveNormal) / 2, yR - Image.getHeight(loadImageInterface.imgMoveNormal) / 2,
                Image.getWidth(loadImageInterface.imgMoveNormal), Image.getHeight(loadImageInterface.imgMoveNormal)))
        {
            keyTouch = 6;
            GameCanvas.keyHold[6] = true;
            //			resetAuto();
            isPress = true;
        }
        else if (GameCanvas.isPointerDown)
        {
            GameCanvas.keyHold[6] = false;
        }


        if (GameCanvas.isPointerHoldIn(xF, yF, 54, 54))
        {
            keyTouch = 5;
            if (GameCanvas.isPointerJustRelease)
            {
                GameCanvas.keyPressed[5] = true;
                isPress = true;
            }
        }

        if (GameCanvas.isPointerClick && GameCanvas.isPoint(0, 68, 30, 30))
        {
            GameCanvas.isPointerClick = false;
            TabChat.gI().switchToMe();
            if (ChatPrivate.nTinChuaDoc > 0)
            {
                for (int i = 0; i < ChatPrivate.vOtherchar.size(); i++)
                {
                    OtherChar other = (OtherChar)ChatPrivate.vOtherchar.elementAt(i);
                    if (other.nTinChuaDoc > 0)
                    {
                        TabChat.gI().indexRow = i;
                        ChatPrivate.nTinChuaDoc -= other.nTinChuaDoc;
                        ChatPrivate.nTinChuaDoc = (ChatPrivate.nTinChuaDoc < 0 ? 0 : ChatPrivate.nTinChuaDoc);
                        other.nTinChuaDoc = 0;
                        //Cout.println("TabChat.gI().indexRow   " + TabChat.gI().indexRow);
                        break;
                    }
                }
            }
        }


        if (GameCanvas.isPointerJustRelease)
        {
            GameCanvas.keyHold[1] = false;
            GameCanvas.keyHold[2] = false;
            GameCanvas.keyHold[3] = false;
            GameCanvas.keyHold[4] = false;
            GameCanvas.keyHold[6] = false;

            // GameCanvas.keyHold[5] = false;

        }
        return isPress;

    }
	
	public void updateRightCOntrol(){
//		bool isPress = false;
		if(Screen.getCmdPointerLast(bntAttack)){
			Cout.println("PAIT SKILL");
			keyTouch = 5;
//			if (GameCanvas.isPointerJustRelease) {
				GameCanvas.keyPressed[5] = true;
//				System.out.println("ATTACK");
//				isPress = true;
//			}
		}
	}
}
