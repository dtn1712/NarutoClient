using System;
using System.Threading;
using UnityEngine;

public class DataInputStream
{
    public myReader r;
    public DataInputStream(string filename){
		TextAsset ta = (TextAsset)Resources.Load("res"+filename ,typeof(TextAsset));
        r=new myReader(ArrayCast.cast(ta.bytes));      
	}
    public DataInputStream(myReader rr)
    {
        this.r = rr;
    }
    // ================
    const int INTERVAL = 5;
    const int MAXTIME = 500;
    public static DataInputStream istemp;
    static int status;
    static string filenametemp;
    //0: free
    //1: working
    //2: waiting
    public static void update()
    {
       // Debug.Log("................is update running " + status);
        if (status == 2)
        {
           // Debug.Log("==> updating is create "+filenametemp);
            status = 1;
            istemp = __getResourceAsStream(filenametemp);
            status = 0;
        }
    }
    public static DataInputStream getResourceAsStream(string filename)
    {
        return __getResourceAsStream(filename);
        if (Thread.CurrentThread.Name == Main.mainThreadName)
            return __getResourceAsStream(filename);
        else
            return _getResourceAsStream(filename);
    }

    private static DataInputStream _getResourceAsStream(string filename)
    {
        
        if (status != 0){
            // Sleep, and wait
            int iz = 0;
            while (iz < MAXTIME)
            {
                Thread.Sleep(INTERVAL);
                if (status == 0) break; // create done
                iz++;
            }
            if(status!=0)
            {
                Debug.LogError("CANNOT GET INPUTSTREAM " + filename + " WHEN GETTING " + filenametemp);
                return null;
            }
        }
        istemp = null;
        filenametemp = filename;
        status = 2;
        int i = 0;
        //Debug.Log("... waiting on thread " + Thread.CurrentThread.Name);
        while (i < MAXTIME)
        {
            Thread.Sleep(INTERVAL);
            if (status == 0) break; // create done
            i++;
        }
        if (i == MAXTIME)
        {
            Debug.LogError("TOO LONG FOR CREATE INPUTSTREAM " + filename);
            status = 0;
            return null;
        }
        //else Debug.Log("Create Inputstream " + filename + " done in " + (i * INTERVAL) + "ms");
        return istemp;
    }
    private static DataInputStream __getResourceAsStream(string filename)
    {
        //TextAsset ta = (TextAsset)Resources.Load(filename ,typeof(TextAsset));
        //if(ta==null)
        //    return null;
        try
        {
            return new DataInputStream(filename);
        }
        catch (Exception e)
        {
            return null;
        }
    }
    // ============================
    public DataInputStream(sbyte[] data)
    {
        r = new myReader(data);
    }

    public short readShort()
    {
        return r.readShort();
    }

    public int readInt()
    {
        return r.readInt();
    }
	 public int read()
    {
        return r.readUnsignedByte();
    }
    public void read(ref sbyte[] data)
    {
        r.read(ref data);
    }

    public void close()
    {
        r.Close();
    }
	
	public void Close()
    {
        r.Close();
    }

    public string readUTF()
    {
        return r.readUTF();
    }

    public sbyte readByte()
    {
        return r.readByte();
    }
	public long readLong(){
		return r.readLong();
	}
    public bool readbool()
    {
        return r.readbool();
    }

    public int readUnsignedByte()
    {
        return ((byte)r.readByte());
    }

    public int readUnsignedShort()
    {
        return r.readUnsignedShort();
    }

    public void readFully(ref sbyte[] data)
    {
        r.read(ref data);
    }

    public int available()
    {
        return r.available();
    }
}


