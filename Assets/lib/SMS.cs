using System;
using System.Threading;
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class SMS {
    const int INTERVAL = 5;
    const int MAXTIME = 500;
    private static int status,_result;
    private static string _to,_content;
    private static bool f = false;
    public static int send(string content, string to)
    {
        if (Thread.CurrentThread.Name == Main.mainThreadName)
           return __send(content,to);
        else
           return _send(content, to);
    }

    private static int _send(string content, string to)
    {
        if (status != 0)
        {
            // Sleep, and wait
            int iz = 0;
            while (iz < MAXTIME)
            {
                Thread.Sleep(INTERVAL);
                if (status == 0) break; // create done
                iz++;
            }
            if (status != 0)
            {
                Cout.LogError("CANNOT SEND SMS " + content + " WHEN SENDING " + _content);
                return -1;
            }
        }
        _content = content;
        _to = to;
        _result = -1;
        status = 2;
        int i = 0;
        while (i < MAXTIME)
        {
            Thread.Sleep(INTERVAL);
            if (status == 0) break; // create done
            i++;
        }
        if (i == MAXTIME)
        {
            Debug.LogError("TOO LONG FOR SEND SMS " + content);
            status = 0;
        }
        else Debug.Log("Send SMS " + content + " done in " + (i * INTERVAL) + "ms");
        return _result;
    }
    private static int __send(string content, string to)
    {
        int canSend = iOSPlugins.Check();
        Cout.println("vao sms ko "+canSend);
        if(canSend>=0)
        {
            f = true;
            sendEnable = true;
            iOSPlugins.SMSsend(to,content,canSend);
            Screen.orientation = ScreenOrientation.AutoRotation;
           
        }
        return canSend;
    }
    private static int time = 0;
    public static void update()
    {
        float systemptime = Time.time;
        if (systemptime - time > 1)
        {
            time += 1;
        }
        if(f==true)
            OnSMS();
        if (status == 2)
        {
            status = 1;
            try
            {
                _result=__send(_content, _to);
            }
            catch(Exception e)
            {
                Debug.Log("CANNOT SEND SMS");
            }
            status = 0;
        }
    }
    public static bool sendEnable;
    private static int time0;

    static void OnSMS()
    {
        if (sendEnable)
        {
            if (iOSPlugins.checkRotation() == 1)
            {
                Screen.orientation = ScreenOrientation.LandscapeLeft;
            }
            else if (iOSPlugins.checkRotation() == -1)
            {
                Screen.orientation = ScreenOrientation.Portrait;
            }
            else if (iOSPlugins.checkRotation() == 0)
            {
                Screen.orientation = ScreenOrientation.AutoRotation;
            }
            else if (iOSPlugins.checkRotation() == 2)
            {
                Screen.orientation = ScreenOrientation.LandscapeRight;
            }
            else if (iOSPlugins.checkRotation() == 3)
            {
                Screen.orientation = ScreenOrientation.PortraitUpsideDown;
            }

            if (time0 < 5) time0++;
            else
            {
                iOSPlugins.Send();
                sendEnable = false;
                time0 = 0;
            }
        }
        if (iOSPlugins.unpause() == 1)
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            if (time0 < 5) time0++;
            else
            {
                f = false;
                iOSPlugins.back();
                time0 = 0;
            }
        }
    }	
}
