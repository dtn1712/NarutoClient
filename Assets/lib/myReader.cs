using System;
using System.IO;
using System.Text;
using UnityEngine;
using System.Threading;

public class myReader
{
	public sbyte[] buffer;
	private int posRead = 0,posMark=0;
	
	private static string fileName;
	private static int status = 0;
	
	//public static myReader tempReader;
	//const int INTERVAL=5;
	//const int MAXTIME=500;
	
	public myReader ()
	{
		
	}
	
	public myReader (sbyte[] data)
	{
		buffer = data;
	}
	
	public myReader( string filename)
	{
		//Debug.Log("GET DataStream FROM FILE: "+filename);
		TextAsset ta = (TextAsset)Resources.Load(filename ,typeof(TextAsset));
			
        //Debug.Log("SIZE: " + ta.bytes.Length);
				
		buffer = mSystem.convertToSbyte(ta.bytes);
		
//		if(Thread.CurrentThread.Name==Main.mainThreadName)
//		{
//			//Debug.Log("GET DataStream FROM FILE: "+filename);
//			TextAsset ta = (TextAsset)Resources.Load(filename ,typeof(TextAsset));
//			
//        	//Debug.Log("SIZE: " + ta.bytes.Length);
//			buffer = mSystem.convertToSbyte(ta.bytes);
//		}
//		else
//		{
//			status = 1;
//			fileName = filename;
//		}
	}
	
//	public static myReader createReader(string filename)
//	{
//		if(Thread.CurrentThread.Name==Main.mainThreadName)
//		{
//			tempReader = new myReader();
//			
//			Debug.Log("GET DataStream FROM FILE: "+filename);
//			TextAsset ta = (TextAsset)Resources.Load(filename ,typeof(TextAsset));
//        	Debug.Log("SIZE: " + ta.bytes.Length);
//			tempReader.buffer = mSystem.convertToSbyte(ta.bytes);
//		}
//		else
//		{
//			status = 1;
//			fileName = filename;
//			
//			int i=0;
//			while(i<MAXTIME)
//			{
//				Thread.Sleep(INTERVAL);
//				if(status==0) break; // create done
//				i++;
//			}
//			if(i==MAXTIME)Debug.LogError("TOO LONG FOR CREATE READER");
//			else Debug.Log("Create READER done in "+(i*INTERVAL)+"ms");
//			
//		}
//		
//		return tempReader;
//	}
	
//	public static void update()
//	{
//		if( status == 1)
//		{
//			if(Thread.CurrentThread.Name==Main.mainThreadName)
//			{
//				status = 0;
//				//Debug.Log("GET DataStream FROM FILE: "+fileName);
//				TextAsset ta = (TextAsset)Resources.Load(fileName ,typeof(TextAsset));
//        		//Debug.Log("SIZE: " + ta.bytes.Length);
//				buffer = mSystem.convertToSbyte(ta.bytes);
//			}
//			
//		}
//	}
	
	public sbyte readSByte()
	{
		if ( posRead < buffer.Length)
			return buffer[posRead++];
		else
		{
			posRead = buffer.Length;
			return 0;
		}
	}
	
	public sbyte readsbyte()
	{
		return readSByte();
	}
	
	public sbyte readByte()
	{
		return readSByte();
	}
	public void mark(int readlimit){
		posMark=posRead;
		
	}
	public void reset(){
		posRead=posMark;
	}
	public byte readUnsignedByte()
	{
		return convertSbyteToByte(readSByte());
	}
	
	public short readShort()
	{
		
		short res = 0;
		for (int i = 0; i < 2; i++) {
			res <<= 8;
			res |= (short)(0xff & buffer[posRead++]);
		}
		return res;
	}
	
	public ushort readUnsignedShort()
	{
		
		ushort res = 0;
		for (int i = 0; i < 2; i++) {
			res <<= 8;
			res |= (ushort)(0xff & buffer[posRead++]);
		}
		return res;
	}
	
	public int readInt()
	{

		int res = 0;
		for (int i = 0; i < 4; i++) {
			res <<= 8;
			res |= (0xff & buffer[posRead++]);
		}
		return res;
		
	}
	
	public long readLong()
	{
		
		long res = 0;
		for (int i = 0; i < 8; i++) {
			res <<= 8;
			res |= (0xff & buffer[posRead++]);
		}
		return res;
	}
	
	public bool readBool()
	{
		return ((readSByte() > 0) ? true : false);
	}
	
	public bool readbool()
	{
		return ((readSByte() > 0) ? true : false);
	}
	
	public string readString()
	{
		short len = readShort();
		
		byte[] temp = new byte[len];
		
		for(int i = 0; i < len; i++)
		{
			temp[i] = convertSbyteToByte(readSByte());	
		}
		
		System.Text.UTF8Encoding utf = new System.Text.UTF8Encoding();
		
		return utf.GetString(temp);
		              
	}
	
	public string readStringUTF()
	{
		short len = readShort();
		
		byte[] temp = new byte[len];
		
		for(int i = 0; i < len; i++)
		{
			temp[i] = convertSbyteToByte(readSByte());	
		}
		
		System.Text.UTF8Encoding utf = new System.Text.UTF8Encoding();
		
		return utf.GetString(temp);
		              
	}
	
	public string readUTF()
	{
		return readStringUTF();
	}
	
	public int read()
	{
		if( posRead < buffer.Length)
			return readSByte();
		
		return -1;
	}
	
	public int read(ref sbyte[] data)
	{
		if ( data == null)
			return 0;	
		
		int mumRead = 0;
		
		for( int i = 0; i < data.Length; i++)
		{
			data[i] = readSByte();
			
			if(posRead > buffer.Length)
				return -1;
					
			mumRead++;
		}
		
		return mumRead;
	}
	
	public void readFully(ref sbyte[] data)
	{
		if ( data == null || data.Length + posRead > buffer.Length)
			return;
		
		for( int i = 0; i < data.Length; i++)
		{
			data[i] = readSByte();
		}
		
	}
	
//	public void readFully(ref sbyte[] data, int offset, int lenght)
//	{
//		
//	}
	
	public int available()
	{
		return buffer.Length - posRead;
	}
	
	public static byte convertSbyteToByte( sbyte var)
	{
		if ( var > 0)
			return (byte)var;
		
		return (byte)(var + 256);
	}
	
	public static byte[] convertSbyteToByte( sbyte[] var)
	{
		byte[] temp = new byte[var.Length];
		for(int i = 0; i < var.Length; i++)
		{
			if ( var[i] > 0)
				temp[i] =  (byte)var[i];
		
			else temp[i] = (byte)(var[i] + 256);
		}
		
		return temp;
	}
	
	public void Close()
	{
		buffer = null;	
	}
	
	public void close()
	{
		buffer = null;	
	}

    public void read(ref sbyte[] data, int arg1, int arg2)
    {
        if (data == null)
            return;
        //posRead > buffer.Length
        for (int i = 0; i < arg2; i++)
        {
            data[i+arg1] = readSByte();
            if (posRead > buffer.Length)
                return;
        }
    }
}

