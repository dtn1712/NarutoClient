﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


class Net
{
    public static WWW www;
    public static Command h;
    public static void update()
    {
        if (www != null)
        {
            if (www.isDone)
            {
                string text = "";
                if (www.error == null || www.error.Equals(""))
                    text = www.text;
                www = null;
                if (h != null) h.performAction();
            }
        }
    }
    public static void connectHTTP(string link, Command h)
    {
        if (www != null)
        {
            Cout.LogError("GET HTTP BUSY");
        }
        Cout.LogWarning("REQUEST " + link);
        www = new WWW(link);
        Net.h = h;
    }
}

