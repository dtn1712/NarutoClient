using System;
using System.IO;
using System.Text;
using UnityEngine;
using System.Net.Sockets;

public class myWriter
{
	public sbyte[] buffer = new sbyte[2048];
	private int posWrite = 0;
	private int lenght = 2048;
	
	public myWriter ()
	{
		
	}	
	public void writeSByte( sbyte value)
	{
		checkLenght(0);
		buffer[posWrite++] = value;
	}
	
	public  void writeSByteUncheck( sbyte value)
	{
		buffer[posWrite++] = value;
	}
	
	public  void writeByte( sbyte value)
	{
		writeSByte(value);
	}
	
	public  void writeByte( int value)
	{
		writeSByte((sbyte)value);
	}
	
	public  void writeUnsignedByte( byte value)
	{
		writeSByte((sbyte)value);
	}
	
	public  void writeUnsignedByte( byte[] value)
	{
		checkLenght(value.Length);
		for( int i = 0; i < value.Length; i++)
		{
			writeSByteUncheck((sbyte)value[i]);	
		}
	}
	
	public void writeSByte( sbyte[] value)
	{
		checkLenght(value.Length);
		for( int i = 0; i < value.Length; i++)
		{
			writeSByteUncheck(value[i]);	
		}
	}
	
	public  void writeShort( short value)
	{
		checkLenght(2);
		for (int i = 1; i >= 0; i--) {
			writeSByteUncheck((sbyte) (value >> i * 8));
		}
	}
	
	public  void writeShort( int value)
	{
		checkLenght(2);
		short temp = (short)value;
		for (int i = 1; i >= 0; i--) {
			writeSByteUncheck((sbyte) (temp >> i * 8));
		}
	}
	
	public  void writeUnsignedShort( ushort value)
	{
		checkLenght(2);
		for (int i = 1; i >= 0; i--) {
			writeSByteUncheck((sbyte) (value >> i * 8));
		}
	}
	
	public  void writeInt( int value)
	{
		checkLenght(4);
		for (int i = 3; i >= 0; i--) {
			writeSByteUncheck((sbyte) (value >> i * 8));
		}
		
	}
	
	public  void writeLong( long value)
	{
		checkLenght(8);
		for (int i = 7; i >= 0; i--) {
			writeSByteUncheck((sbyte) (value >> i * 8));
		}
	}
	
	public  void writebool( bool value)
	{
		writeSByte((value ? (sbyte)1 : (sbyte)0));
	}
	
	public  void writeBool( bool value)
	{
		writeSByte((value ? (sbyte)1 : (sbyte)0));
	}
	
	public  void writeString( string value)
	{
		
		char[] tempC = value.ToCharArray();
		
		writeShort((short)tempC.Length);
		
		checkLenght(tempC.Length);
		
		for( int i = 0; i < tempC.Length; i++)
		{
			writeSByteUncheck((sbyte)tempC[i]);
		}
	}
	
	public  void writeUTF( string value)
	{
		
		Encoding unicode = Encoding.Unicode;
		
		Encoding utf8 = Encoding.GetEncoding(65001);
			
		byte[] tempC = unicode.GetBytes(value);
		
		byte[] tempC2 = Encoding.Convert(unicode, utf8, tempC);
		
		
		writeShort((short)tempC2.Length);
		
		checkLenght(tempC2.Length);
		
		for( int i = 0; i < tempC2.Length; i++)
		{
			sbyte sb = unchecked((sbyte)tempC2[i]);
			writeSByteUncheck(sb);	
		} 
	}
	
	public  void write( sbyte value)
	{
		writeSByte(value);
	}
	
	public  void write( sbyte[] value)
	{
		writeSByte(value);
	}
	
	public sbyte[] getData()
	{
		if ( posWrite <= 0)
			return null;
		
		sbyte[] temp = new sbyte[posWrite];
		for(int i = 0; i < posWrite; i++)
		{
			temp[i] = buffer[i];
		}
		
		return temp;
	}
	
	public void checkLenght(int ltemp)
	{
		if ( posWrite + ltemp > lenght)
		{
			sbyte[] temp = new sbyte[lenght + 1024 + ltemp];
			
			for( int i = 0; i < lenght; i++)
			{
				temp[i] = buffer[i];	
			}
			
			buffer = null;
			buffer = temp;
			
			lenght += (1024 + ltemp);
		}
	}
	
	static void convertString(string[] args)
  	{
    	string inputFile = args[0];
    	string outputFile = args[1];
    	using (StreamReader reader = new StreamReader(inputFile, Encoding.Unicode))
    	{
      		using (StreamWriter writer = new StreamWriter(outputFile, false, Encoding.UTF8))
      		{
       	 		CopyContents(reader, writer);
      		}
    	}
  	}

  	static void CopyContents(TextReader input, TextWriter output)
  	{
		
    	char[] buffer = new char[8192];
    	int len;
    	while ((len = input.Read(buffer, 0, buffer.Length)) != 0)
    	{
      		output.Write(buffer, 0, len);
    	}
		
		output.Flush();
		
		string a  = output.ToString();
		Debug.Log(a);
  	}
	
	public byte convertSbyteToByte( sbyte var)
	{
		if ( var > 0)
			return (byte)var;
		
		return (byte)(var + 256);
	}
	
	public byte[] convertSbyteToByte( sbyte[] var)
	{
		byte[] temp = new byte[var.Length];
		for(int i = 0; i < var.Length; i++)
		{
			if ( var[i] > 0)
				temp[i] =  (byte)var[i];
		
			else temp[i] = (byte)(var[i] + 256);
		}
		
		return temp;
	}
	
	public void Close()
	{
		buffer = null;	
	}
	
	public void close()
	{
		buffer = null;	
	}
}


