using System;
using System.Collections;

public class mHashtable
{
    public Hashtable h = new Hashtable();

    public Object get(Object k)
    {
        return h[k];
    }

    public void clear()
    {		
        h.Clear();
    }
	public IDictionaryEnumerator GetEnumerator(){
		return h.GetEnumerator();	
	}
	public int size(){
		return h.Count;
	}
    public void put(Object k, Object v)
    {
        if(h.ContainsKey(k))h.Remove(k);
        h.Add(k,v);
    }

    public void remove(Object k)
    {						
         h.Remove(k);
    }
	 public void Remove(string key)
    {				
         h.Remove(key);
		
    }
	public bool containsKey(Object key){
		return(h.ContainsKey(key));
	}
}


