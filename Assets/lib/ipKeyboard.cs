﻿using UnityEngine;
using System;
public class ipKeyboard
{
    private static TouchScreenKeyboard tk;
    public static int TEXT = 0;
    public static int NUMBERIC = 1;
    public static int PASS = 2;
    private static Command act;
    public static void openKeyBoard(string caption, int type, string text, Command action)
    {
        act = action;      
        TouchScreenKeyboardType t = (type == 0||type==2) ? TouchScreenKeyboardType.ASCIICapable : TouchScreenKeyboardType.NumberPad;
        TouchScreenKeyboard.hideInput = false;
        tk = TouchScreenKeyboard.Open(text,t,false,false,type==2,false,caption);
    }
    public static void update()
    {
        try
        {      
        if(tk==null)return;
        if(tk.done)
        {
            if(act!=null)act.performAction();
            tk.text = "";
            tk = null;
        }
        }
        catch (Exception e) { }
    }

   
}

