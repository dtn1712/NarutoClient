

public abstract class TCanvas  {

	public static TCanvas instance;
	public static int realWidth,realHeight;
	public static bool bRun;
	public static float Tile,TileW; //ti le
	public static int TileZoom = 1,indexZoom;
	public static float[] mangpixelTheoTile = new float[]{240f,480f,960f,1280f};
	public static float[] mangpixelTheoTileW = new float[]{320f,640f,960f,1280f};
	private void checkZoomLevel(int w, int h) {
		
		if (w >= 1280 && h >= 960) {
			w = w / 3;
			h = h / 3;
			mGraphics.zoomLevel = 4;
		} else if (w >= 1280 && h >= 960) {
			w = w / 3;
			h = h / 3;
			mGraphics.zoomLevel = 4;
		}
		if (w >= 720 && h >= 960) {
			w = w / 3;
			h = h / 3;
			mGraphics.zoomLevel = 3;
		} else if (w >= 960 && h >= 720) {
			w = w / 3;
			h = h / 3;
			mGraphics.zoomLevel =3;
		}
		if (w > 400 && h > 600) {
			w = w / 2;
			h = h / 2;
			mGraphics.zoomLevel = 2;
		} else if (w > 600 && h > 400) {
			w = w / 2;
			h = h / 2;
			mGraphics.zoomLevel = 2;
		}
		Cout.println(this.GetType().Name,"mGraphics.zoomLevel   "+mGraphics.zoomLevel +" w "+ w +" h  "+h);
		
	}
	
	public TCanvas() {
		realWidth=getWidthL();
		realHeight=getHeightL();

        Cout.println(TileZoom + "  " + getWidthL() + " getHeightL() =====   " + getHeightL());
		setFullScreenMode(true);

        checkZoomLevel(realWidth, realHeight);
	}
	public int getHeightL() {
		//System.out.println("DONT USE getHeight, PLEASE USE getHeightz()");

        return (int)(ScaleGUI.HEIGHT);
	}

	public int getWidthL() {
        return (int)(ScaleGUI.WIDTH);
	}
	public static string getPlatformName() {
		return "AndroidDevice";
	}
	public int getHeightz() {
        return (realHeight / mGraphics.zoomLevel) + (realHeight % mGraphics.zoomLevel == 0 ? 0 : 1);
		
	}

	public int getWidthz() {//640

        return (realWidth / mGraphics.zoomLevel) + (realWidth % mGraphics.zoomLevel == 0 ? 0 : 1);	
	}
	public void run() {
		
	}

	protected void onDraw() {}
	public void setFullScreenMode(bool b) {
		
	}
	public void start()
	{
		//new Thread(this).start();	
	}
	public bool hasPointerEvents() {
		return true;
	}
//	public bool onTouchEvent(MotionEvent event) {
//		int action = event.getAction();
//
//		switch (action) {
//		case MotionEvent.ACTION_DOWN:
//			onPointerPressed(
//					(int) (event.getX())/mGraphics.zoomLevel,
//					(int) (event.getY())/mGraphics.zoomLevel);
//			break;
//		case MotionEvent.ACTION_MOVE:
//			onPointerDragged(
//					(int) (event.getX())/mGraphics.zoomLevel,
//					(int) (event.getY())/mGraphics.zoomLevel);
//			break;
//		case MotionEvent.ACTION_UP:
//			onPointerReleased(
//					(int) (event.getX())/mGraphics.zoomLevel,
//					(int) (event.getY())/mGraphics.zoomLevel);
//			break;
//		}
//		return true;
//	}
	
	public void pointerDragged(int x, int y) {
		x = x / mGraphics.zoomLevel;
		y = y / mGraphics.zoomLevel;
		onPointerDragged(x, y);
	}

	public void pointerPressed(int x, int y) {
		x = x / mGraphics.zoomLevel;
		y = y / mGraphics.zoomLevel;
		onPointerPressed(x, y);
	}

	public void pointerReleased(int x, int y) {
		x = x / mGraphics.zoomLevel;
		y = y / mGraphics.zoomLevel;
		onPointerReleased(x, y);
	}

    public virtual void onPointerDragged(int x, int y){
    }
    public virtual void onPointerPressed(int x, int y){
    }
    public virtual void onPointerReleased(int x, int y){
    }
    public virtual void update(){}
    public virtual void paint(mGraphics g){}
	
	public void surfaceChanged() {
	}

	public void surfaceCreated() {
		
	}

	public void surfaceDestroyed() {
		bRun = false;
	}

}
