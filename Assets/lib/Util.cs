﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Util
{
    static mRandom randomm = new mRandom();

    public static void onLoadMapComplete()
    {
        GameCanvas.endDlg();
    }

    public void onLoading()
    {
        GameCanvas.startWaitDlg("Đang tải dữ liệu");
    }

    public static int randomNumber(int max)
    {
        return randomm.nextInt(max);
    }

    public static int random(int a, int b)
    {
        return a + randomm.nextInt(b - a);

    }

    public static sbyte[] readByteArray(Message msg)
    {
        try
        {
            int lengh = msg.reader().available();
            sbyte[] data = new sbyte[lengh];
            msg.reader().read(ref data);
            return data;
        }
        catch (Exception e)
        {
        }
        return null;
    }

    public static sbyte[] readByteArray(DataInputStream dos)
    {
        try
        {
            short lengh = dos.readShort();
            sbyte[] data = new sbyte[lengh];
            dos.read(ref data);
            return data;
        }
        catch (Exception ex)
        {
          
        }
        return null;
    }

    public static string replace(String text, String regex, String replacement)
    {
        System.Text.StringBuilder sBuffer = new System.Text.StringBuilder();
        int pos = 0;
        while ((pos = text.IndexOf(regex)) != -1)
        {
            sBuffer.Append(mSystem.substring(text,0, pos) + replacement);
            text = mSystem.substring(text,pos + regex.Length);
        }
        sBuffer.Append(text);
        return sBuffer.ToString();
    }

    public static string numberToString(String number)
    {
        string value = "", value1 = "";
        if (number.Equals(""))
            return value;

        if (number[0] == '-')
        {
            value1 = "-";
            number = mSystem.substring(number,1);
        }
        for (int i = number.Length - 1; i >= 0; i--)
        {
            if ((number.Length - 1 - i) % 3 == 0
                    && (number.Length - 1 - i) > 0)
                value = number[i] + "." + value;
            else
                value = number[i] + value;
        }
        return value1 + value;
    }

    public static void sendMsDK(String syntax, short port)
    {
        //		GameMidlet.sendSMS(syntax, "sms://" + port, new Command("",GameCanvas.gI(),88827, null), new Command("", GameCanvas.gI(),88828, null));
        //		ThreadManager.sendSMS(syntax, "sms://" + port, new Command("",GameCanvas.gI(),88827, null), new Command("", GameCanvas.gI(),88828, null));
    }

    //	public static void downloadGame(String url) {
    //		try {
    //			GameMidlet.instance.platformRequest(url);
    //		} catch (Exception e) {
    //		//	e.printStackTrace();
    //		} finally {
    //			GameMidlet.instance.notifyDestroyed();
    //		}
    //	}


    public static String getTime(int timeRemainS)
    {
        int timeRemainM = 0;
        if (timeRemainS > 60)
        {
            timeRemainM = timeRemainS / 60;
            timeRemainS = timeRemainS % 60;
        }
        int timeRemainH = 0;
        if (timeRemainM > 60)
        {
            timeRemainH = timeRemainM / 60;
            timeRemainM = timeRemainM % 60;
        }
        int timeRemainD = 0;
        if (timeRemainH > 24)
        {
            timeRemainD = timeRemainH / 24;
            timeRemainH = timeRemainH % 24;
        }
        String s = "";
        if (timeRemainD > 0)
        {
            s += timeRemainD;
            s += "d";
            s += timeRemainH + "h";

        }
        else if (timeRemainH > 0)
        {
            s += timeRemainH;
            s += "h";
            s += timeRemainM + "'";
        }
        else
        {
            if (timeRemainM > 9)
                s += timeRemainM;
            else
                s += "0" + timeRemainM;
            s += ":";

            if (timeRemainS > 9)
                s += timeRemainS;
            else
                s += "0" + timeRemainS;
        }
        return s;
    }

    public static void sleep(int time)
    {
        try
        {
            System.Threading.Thread.Sleep(time);
        }
        catch (Exception e) { }
    }

    public static string[] split(String original, String separator)
    {
        mVector nodes = new mVector();
        int index = original.IndexOf(separator);
        while (index >= 0)
        {
            nodes.addElement(mSystem.substring(original,0, index));
            original = mSystem.substring(original,index + separator.Length);
            index = original.IndexOf(separator);
        }
        nodes.addElement(original);
        string[] result = new string[nodes.size()];
        if (nodes.size() > 0)
        {
            for (int loop = 0; loop < nodes.size(); loop++)
            {
                result[loop] = (String)nodes.elementAt(loop);
            }

        }
        return result;
    }

    public static void openUrl(string url)
    {
        try
        {
        }
        catch (Exception e)
        {
        }
    }

    public static int distance(int x1, int y1, int x2, int y2)
    {
        return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public static int sqrt(int a)
    {
        int x, x1;

        if (a <= 0)
            return 0;
        x = (a + 1) / 2;
        do
        {
            x1 = x;
            x = x / 2 + a / (2 * x);
        } while (Math.abs(x1 - x) > 1);
        return x;
    }

    
     public static sbyte[] readByteArray(Message msg,int leng) {
		try {
//			int lengh = msg.reader().readInt();
			sbyte[] data = new sbyte[leng];
			msg.reader().read(ref data);
			return data;
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return null;
    }
}
