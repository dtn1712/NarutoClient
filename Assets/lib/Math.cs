﻿public class Math
{
    public static int abs(int i)
    {
        return i > 0 ? i : -i;
    }
    public static int sqrt(int a)
    {
        int x, x1;
        if (a <= 0)
            return 0;
        x = (a + 1) / 2;
        do
        {
            x1 = x;
            x = x / 2 + a / (2 * x);
        } while (Math.abs(x1 - x) > 1);
        return x;
    }

    public static int min(int x, int y)
    {
        return x < y ? x : y;
    }
    public static int max(int x, int y)
    {
        return x > y ? x : y;
    }
	public static int pow(int data,int x){
		int t=1;
		for(int i=0;i<x;i++){
			t*=data;
		}
		return t;
	}
}

