using System;
using System.Collections.Generic;
using UnityEngine;
public class ScaleGUI
{
	public static bool scaleScreen;
	public static float WIDTH,HEIGHT;
	static List<Matrix4x4> stack = new List<Matrix4x4> ();
	//public static int numScale = 1;
	static public void initScaleGUI()
	{
        WIDTH = UnityEngine.Screen.width;
        HEIGHT = UnityEngine.Screen.height;

        Cout.println("string[: Screen.w=" + WIDTH + " Screen.h=" + HEIGHT);
		scaleScreen=false;
		if(Screen.width > 1200)// IPAD 2
		{
			//scaleScreen=true;
			//WIDTH=Screen.width/2;
			//HEIGHT=Screen.height/2;
		//	numScale = 2;
		}
		//else if( Screen.width > 480)
			//numScale = 2;
		/*
        if(Screen.width==768)// IPAD
		{
			scaleScreen=true;
			WIDTH=Screen.width/2;
			HEIGHT=Screen.height/2;
		}
		if(Screen.width==640) // IPHONE 4
		{
			scaleScreen=true;
			WIDTH=Screen.width/2;
			HEIGHT=Screen.height/2;
		}
         * */
		//WIDTH=320;
		//HEIGHT=480;
		//scaleScreen=true;
	}
    static public void BeginGUI() {
		if(!scaleScreen)return;
        stack.Add (GUI.matrix);
        Matrix4x4 m = new Matrix4x4 ();
        var w = (float)Screen.width;
        var h = (float)Screen.height;
        var aspect = w / h;
        var scale = 1f;
        var offset = Vector3.zero;
        if(aspect < (WIDTH/HEIGHT)) { //Screen is taller
            scale = (Screen.width/WIDTH);

        } else { // Screen is wider
            scale = (Screen.height/HEIGHT);
        }
        m.SetTRS(offset,Quaternion.identity,Vector3.one*scale);
        GUI.matrix *= m;
    }

    static public void EndGUI() {
		if(!scaleScreen)return;
        GUI.matrix = stack[stack.Count - 1];
        stack.RemoveAt (stack.Count - 1);
    }
	
	public static float scaleX(float x)
	{
		if(!scaleScreen)return x;
		x = (x*WIDTH)/Screen.width;
		return x;
	}
	public static float scaleY(float y)
	{
		if(!scaleScreen)return y;
		y = (y*HEIGHT)/Screen.height;
		return y;
	}
}


