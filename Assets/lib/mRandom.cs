using System;

public class mRandom {
	public Random r;
	public mRandom(){
		r=new Random();
	}
	public mRandom(long c){
		r=new Random();		
	}
	public int nextInt(){
		return r.Next();
	}
	public int nextInt(int n){
		return r.Next(n);
	}
	public int random(int a, int b) {
		return a + (r.Next(b - a));
	}
}
