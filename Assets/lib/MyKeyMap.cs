﻿
using System.Collections;
using UnityEngine;

public class MyKeyMap
{
    private static Hashtable h=new Hashtable();
    static MyKeyMap()
    {
        h.Add(KeyCode.A, (int)'a');
        h.Add(KeyCode.B, (int)'b');
        h.Add(KeyCode.C, (int)'c');
        h.Add(KeyCode.D, (int)'d');
        h.Add(KeyCode.E, (int)'e');
        h.Add(KeyCode.F, (int)'f');
        h.Add(KeyCode.G, (int)'g');
        h.Add(KeyCode.H, (int)'h');
        h.Add(KeyCode.I, (int)'i');
        h.Add(KeyCode.J, (int)'j');
        h.Add(KeyCode.K, (int)'k');
        h.Add(KeyCode.L, (int)'l');
        h.Add(KeyCode.M, (int)'m');
        h.Add(KeyCode.N, (int)'n');
        h.Add(KeyCode.O, (int)'o');
        h.Add(KeyCode.P, (int)'p');
        h.Add(KeyCode.Q, (int)'q');
        h.Add(KeyCode.R, (int)'r');
        h.Add(KeyCode.S, (int)'s');
        h.Add(KeyCode.T, (int)'t');
        h.Add(KeyCode.U, (int)'u');
        h.Add(KeyCode.V, (int)'v');
        h.Add(KeyCode.W, (int)'w');
        h.Add(KeyCode.X, (int)'x');
        h.Add(KeyCode.Y, (int)'y');
        h.Add(KeyCode.Z, (int)'z');
        h.Add(KeyCode.Alpha0, (int)'0');
        h.Add(KeyCode.Alpha1, (int)'1');
        h.Add(KeyCode.Alpha2, (int)'2');
        h.Add(KeyCode.Alpha3, (int)'3');
        h.Add(KeyCode.Alpha4, (int)'4');
        h.Add(KeyCode.Alpha5, (int)'5');
        h.Add(KeyCode.Alpha6, (int)'6');
        h.Add(KeyCode.Alpha7, (int)'7');
        h.Add(KeyCode.Alpha8, (int)'8');
        h.Add(KeyCode.Alpha9, (int)'9');
        h.Add(KeyCode.Space, (int)' ');
        h.Add(KeyCode.F1,-21 );
        h.Add(KeyCode.F2, -22);       
        h.Add(KeyCode.Equals,-25);
        h.Add(KeyCode.Minus,45);
        h.Add(KeyCode.F3, -23);     
        h.Add(KeyCode.UpArrow, -1);
        h.Add(KeyCode.DownArrow, -2);
        h.Add(KeyCode.LeftArrow, -3);
        h.Add(KeyCode.RightArrow, -4);
        h.Add(KeyCode.Backspace, -8);
        h.Add(KeyCode.Return, -5);				
		h.Add(KeyCode.Period, (int)'.');
		h.Add(KeyCode.At, (int)'@');
		h.Add(KeyCode.Tab,-26);
    }
    public static int map(KeyCode k)
    {
        object v = h[k];
        if(v==null) return 0;
        return (int)v;
    }
}

