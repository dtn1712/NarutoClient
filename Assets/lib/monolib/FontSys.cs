using UnityEngine;
using System.Collections;
using System;
public class FontSys {

	public static int LEFT = 0;
	public static int RIGHT = 1;
	public static int CENTER = 2;
	public static int RED = 0;

	public static int YELLOW = 1;
	public static int GREEN = 2;
	public static int FATAL = 3;
	public static int MISS = 4;
	public static int ORANGE = 5;
	public static int ADDMONEY = 6;
	public static int MISS_ME = 7;
	public static int FATAL_ME = 8;
	public static int HP = 9;
	public static int MP = 10;

	private int space;	
	private Image imgFont;
	private string strFont;
	private int [][]fImages;
    public  int yAddFont;
	 public static int []colorJava=new  int[]{
		0x000000,//cho GI
		0xff0000,
		0x637dff,
		0xffffff,
		0xffaa00,
		0x532905,
		0x005325,
		0x00cc00,
		0x70b474,
		0xffeacc,
		0x000000,
		0x00ffff,
		0x005325,
		0xffff00,
		0x555555,
		0xff7777,
		0x0080ff,
		0x84c729,
		0xefebef,
		0x7a1125,
		0xffdf2f,
		0xe44a55,
		0x3ef0e3,
		0x474747,
		0xf59c38,

        0xff0000,//big red
        0xffaa00,//yellow
        0x00cc00,//green
        0xffffff,//white
        0x637dff,//blue
        0xf59c38,//orange
	  
		
	};
	public Font myFont;
	private int height = 0;
    private int wO = 0;

    public int cl1, cl2;
	public Color color1= Color.white;
	public Color color2= Color.gray;
	public sbyte id;
	public int fstyle = 0;
	public string st1 = "áàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬÉÈẺẼẸÊẾỀỂỄỆÍÌỈĨỊÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴĐ";
    public string st2 = "¸µ¶·¹¨¾»¼½Æ©ÊÇÈÉËÐÌÎÏÑªÕÒÓÔÖÝ×ØÜÞãßáâä«èåæçé¬íêëìîóïñòô­øõö÷ùýúûüþ®¸µ¶·¹¡¾»¼½Æ¢ÊÇÈÉËÐÌÎÏÑ£ÕÒÓÔÖÝ×ØÜÞãßáâä¤èåæçé¥íêëìîóïñòô¦øõö÷ùýúûüþ§";
    public static void init(){}

	public FontSys(sbyte id,int cl1,int cl2)
    {       
		string name="big";
        if (id <= 8 || id >= 30)
        {
            name = "big";
            yAddFont = mGraphics.zoomLevel == 1 ? -3 : -2;
        }
        else
        {
            name = "mini";
            if (mGraphics.zoomLevel == 1)
                yAddFont = -1;
        }
		this.id=id;
        this.cl1 = cl1;
        this.cl2 = cl2;
        name = "FontSys/x" + TCanvas.TileZoom + "/" + name;
		myFont = (Font)Resources.Load(name);
        this.color1 = setColor(this.cl1);
        this.color2 = setColor(this.cl2);     
        wO = getWidthExactOf("o");
	}
    public FontSys(string name, sbyte ID, int cl1, int size)
    {

        //name = name.Replace("UVNTHANGVU_2", "UVNAICAPNANG");
        //name = name.Replace("tahoma_7b", "UVNAICAPNANG");
        //name = name.Replace("tahoma_8b", "UVNAICAPNANG");
        name += "_" + size;
        if ((ID >= 10 && ID <= 18) || (ID >= 20 && ID <= 24))
        {
            //			name = "chelthm7";
            if (TCanvas.TileZoom == 1)
                this.yAddFont = 0;
        }
        // else if(ID ==10)
        // {
        // name = "chelthm9";
        // }
        else if (ID >= 25 && ID <= 30)
        {
            //			name = "staccato";
        }
        this.yAddFont = 0 * TCanvas.TileZoom;
        if (size == 6)
            this.yAddFont = 2 * TCanvas.TileZoom;
        if (ID == 11)
            this.yAddFont = -1 * TCanvas.TileZoom;
        if (ID == 12)
            this.yAddFont = -1 * TCanvas.TileZoom;
        //if (ID == 30)
        //{
        //    this.yAddFont = 4 * TCanvas.TileZoom;
        //}
        if (ID == 49)
        {
            this.yAddFont = 3 * TCanvas.TileZoom;
        }
        if (size == 7)
            this.yAddFont = 1 * TCanvas.TileZoom;
        //if (ID == 24)
        //    this.yAddFont = 1;

        //if (ID == 3)
        //    this.yAddFont = 3;

        if (size == 8)
            this.yAddFont = 4 * TCanvas.TileZoom;
        if (TCanvas.TileZoom == 1) this.yAddFont -= 2;
        this.id = ID;
        this.cl1 = cl1;
        name = "FontSys/x" + mGraphics.zoomLevel+ "/" + name;

        myFont = (Font)Resources.Load(name);
        this.color1 = setColor(this.cl1);
        this.color2 = setColor(this.cl2);
        wO = getWidthExactOf("o");
    }
    public FontSys(sbyte id, int cl1)
    {
        string name = "big";
        if (id <= 8 || id >= 30)
        {
            name = "big";
            yAddFont = mGraphics.zoomLevel == 1 ? -3 : -2;
        }
        else
        {
            name = "mini";
            if (mGraphics.zoomLevel == 1)
                yAddFont = -1;
        }
        this.id = id;
        this.cl1 = cl1;
        name = "FontSys/x" + mGraphics.zoomLevel + "/" + name;
        myFont = (Font)Resources.Load(name);
        this.color1 = setColor(this.cl1);
        this.color2 = setColor(this.cl2);
        wO = getWidthExactOf("o");
    }
    public Color setColor(int rgb)
    {
        int blue = rgb & 0xFF;
        int green = (rgb >> 8) & 0xFF;
        int red = (rgb >> 16) & 0xFF;
        float b = (float)blue / 256;
        float g = (float)green / 256;
        float r = (float)red / 256;
        Color cl = new Color(r, g, b);
        return cl;
    }
   
    public Color bigColor(int id)
    {
        Color[] bColor = { Color.red, Color.yellow, Color.green, Color.white, setColor(0x009dd4), Color.red };
        return bColor[id - 25];    
      
    }                               
	public void setTypePaint(mGraphics g, string st, int x, int y, int align,sbyte idFont)
    {		
		sbyte currentId=id;
		if(idFont>0)
			currentId=idFont;
		x-=1;       
        this.color1 = setColor(this.cl1);
        this.color2 = setColor(this.cl2);
		_drawString(g,st, x, y , align);	
	}
	
	public Color setColorFont(sbyte id)
    {
        return setColor(colorJava[id]);	   
	}
	public void drawString(mGraphics g, string st, int x, int y, int align)
    {
        setTypePaint(g, st, x, y + yAddFont, align, 0);		
	}
	public void drawString(mGraphics g, string st, int x, int y, int align, FontSys font)
    {
		setTypePaint(g,st,x,y+1,align,font.id);
		setTypePaint(g,st,x,y,align,0);	
	}
	//da cop --------------------
	public mVector splitFontVector(string src, int lineWidth) {
		mVector lines = new mVector();
		//lineWidth-=5;
		string line = "";
		for (int i = 0; i < src.Length; i++) {
            if (src[i] == '\n' || src[i] == '\b')
            {
				lines.addElement(line);
				line = "";
			} else {
				line += src[i];
				if (getWidth(line) > lineWidth) {					
					int j = 0;
					for (j = line.Length - 1; j >= 0; j--) {
						if (line[j] == ' ') {
							break;
						}
					}
					if (j < 0)
						j = line.Length - 1;
					//lines.addElement(line.Substring(0, j));
                    lines.addElement(mSystem.substring(line,0,j));
					i = i - (line.Length - j) + 1;
					line = "";
				}
				if (i == src.Length - 1 && !line.Trim().Equals(""))
					lines.addElement(line);
			}
		}
		return lines;
	}
	public string splitFirst(string str)
    {
		string line = "";
		bool isSplit = false;
		for (int i = 0; i < str.Length; i++) {
			if (!isSplit) {
				string strEnd = str.Substring(i);
				if (compare(strEnd, " "))
					line += str[i] + "-";
				else
					line += strEnd;
				isSplit = true;
			} else {
				if (str[i] == ' ')
					isSplit = false;
			}
		}
		return line;
	}
    public string[] splitStrInLine(string src, int lineWidth)
    {
        ArrayList list = splitStrInLineA(src, lineWidth);
        string[] strs = new string[list.Count];
        for (int i = 0; i < list.Count; i++)
        {
            strs[i] = (string)list[i];
        }
        return strs;
    }
    public ArrayList splitStrInLineA(string src, int lineWidth)
    {
        ArrayList list = new ArrayList();
        int start = 0, end = 0;
        int srclen = src.Length;

        if (srclen < 5)
        {
            list.Add(src);
            return list;
        }
        string tem = "";
        try
        {
            while (true)
            {
                while (getWidthNotExactOf(tem) < lineWidth)
                {
                    tem += src[end];
                    end++;
                    if (src[end] == '\n')
                        break;
                    if (end >= srclen - 1)
                    {
                        end = srclen - 1;
                        break;
                    }
                }
                if (end != srclen - 1 && (src[end + 1] != ' '))
                {
                    int endAnyway = end;
                    while (true)
                    {
                        if (src[end + 1] == '\n')
                            break;
                        if (src[end + 1] == ' ' && src[end] != ' ')
                            break;
                        if (end == start)
                            break;
                        end--;
                    }
                    if (end == start)
                        end = endAnyway;
                }
                string s = src.Substring(start, end + 1 - start);
                if (s[0] == '\n') s = s.Substring(1, s.Length - 1);
                if (s[s.Length - 1] == '\n') s = s.Substring(0, s.Length - 1);
                list.Add(s);
                if (end == srclen - 1) break;
                start = end + 1;
                while (start != srclen - 1 && src[start] == ' ')
                    start++;
                if (start == srclen - 1) break;
                end = start;
                tem = "";
            }
        }
        catch (Exception e)
        {
           Cout.LogWarning("EXCEPTION WHEN REAL SPLIT " + src + "\n" + "end=" + end + "\n" + e.Message + "\n" + e.StackTrace);
            list.Add(src);
        }
        return list;
    }
	public string[] splitFontArray(string src, int lineWidth) {		
		mVector lines = splitFontVector(src, lineWidth);
		string[] arr = new string[lines.size()];
		for (int i = 0; i < lines.size(); i++) {
		    arr[i] = (string) lines.elementAt(i);
		}
		return arr;
	}
	public bool compare(string strSource, string str) {
		for (int i = 0; i < strSource.Length; i++) {
			if ((""+strSource[i]).Equals(str))
				return true;
		}
		return false;
	}
	public int getWidth(string s)
	{
        return getWidthExactOf(s)/mGraphics.zoomLevel;
	}

    //public static mVector listTimeGetFont = new mVector();
    public int getWidthExactOf(string s)
    {
		try
        {
            //if null return default font(arial)

            GUIStyle style = new GUIStyle();
            style.font = myFont;
            GUIContent content = new GUIContent();
            content.text = s;
            Vector2 dem = style.CalcSize(content);
            int dem222 = (int)dem.x+20;
            return (int)dem222;

        }  catch (Exception e){
            return getWidthNotExactOf(s);
        }
    }
	public int getWidthNotExactOf(string s)
    {
        return s.Length*wO;
    }
	public int getHeight()
	{
        if (height > 0) return (height/mGraphics.zoomLevel);
		GUIStyle style = new GUIStyle();
        style.font = myFont;    
	    try
	    {
            height = (int)(style.CalcSize(new GUIContent("Adg")).y) + 2;
	    }
	    catch (Exception e)
	    {
	       Cout.LogError("FAIL GET HEIGHT "+e.StackTrace);
	        height = 20;
	    }
	    return (height/mGraphics.zoomLevel);
	}
	 public void _drawString(mGraphics g, string st, int x0, int y0, int align) {
         //y0 += yAddFont;
		GUIStyle style = new GUIStyle(GUI.skin.label);	
		style.font = myFont;          
		float x=0,y=0;
		switch (align)
		{
		case 0:
				x = x0;
				y = y0;
				style.alignment = TextAnchor.UpperLeft;
				break;
        case 1:
                x = x0-GameCanvas.w;
                y = y0;
                style.alignment = TextAnchor.UpperRight;
                break;
		case 2:
        case 3:
				x = x0-GameCanvas.w/2;
				y = y0;
				style.alignment = TextAnchor.UpperCenter;
				break;		
		}	
        {
			int wstr=getWidth(st);
            style.normal.textColor = color1;
            g.drawString(st, (int)x, (int)y, style,wstr);
        }           
    }
     public static string[] splitStringSv(string _text, string _searchStr)
     {        
         int count = 0, pos = 0;
         int searchStringLength = _searchStr.Length;
         int aa = _text.IndexOf(_searchStr, pos);
         while (aa != -1)
         {
             pos = aa + searchStringLength;
             aa = _text.IndexOf(_searchStr, pos);
             count++;
         }
         string[] sb = new string[count + 1];
         // Search for search
         int searchStringPos = _text.IndexOf(_searchStr);
         int startPos = 0;
         // Iterate to add string
         int index = 0;
         while (searchStringPos != -1)
         {
             sb[index] = _text.Substring(startPos, searchStringPos - startPos);
             startPos = searchStringPos + searchStringLength;
             searchStringPos = _text.IndexOf(_searchStr, startPos);
             index++;
         }
         sb[index] = _text.Substring(startPos, _text.Length - startPos);
         return sb;
     }
	public void reloadImage(){
		
	}
	public void freeImage(){
	}
}

