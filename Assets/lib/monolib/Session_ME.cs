using System;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class Session_ME : ISession
{

    protected static Session_ME instance = new Session_ME();

    public Session_ME()
    {
        //Debug.Log("init Session_ME");
    }
    public void clearSendingMessage()
    {
        sender.sendingMessage.Clear();
    }

    public static Session_ME gI()
    {      
        return instance;
    }

    private static NetworkStream dataStream;
    private static BinaryReader dis;
    private static BinaryWriter dos;

    public static IMessageHandler messageHandler;
    //	private static Socket sc;
    private static TcpClient sc;
    public static bool connected, connecting;
    private static Sender sender = new Sender();
    public static Thread initThread;
    public static Thread collectorThread;
    public static Thread sendThread;
    public static int sendByteCount;
    public static int recvByteCount;
    static bool getKeyComplete;
    public static sbyte[] key = null;
    private static sbyte curR, curW;
    static int timeConnected;
    public  static string strRecvByteCount = "";

    public static bool isCancel;
    string host;
    int port;

    public bool isConnected()
    {
        return connected;
    }

    public void setHandler(IMessageHandler msgHandler)
    {
        messageHandler = msgHandler;
    }

    public void connect(string host, int port)
    {

        Debug.LogError("connect ...!"+connected+"  ::  "+connecting);      
        if (connected || connecting)
        {
            return;
        }
        else
        {

            this.host = host;
            this.port = port;

            getKeyComplete = false;
            sc = null;

            Debug.Log("connecting...!");
            Debug.Log("host: " + host);
            Debug.Log("port: " + port);

            initThread = new Thread(new ThreadStart(NetworkInit));
            initThread.Start();
        }
    }

    private void NetworkInit()
    {

        //isCancel = false;

        //new Thread(() =>
        //{
        //    try
        //    {
        //        Thread.Sleep(20000);
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    if (connecting)
        //    {
        //        try
        //        {
        //            sc.Close();
        //        }
        //        catch (Exception)
        //        {
        //        }
        //        isCancel = true;
        //        connecting = false;
        //        connected = false;
        //        messageHandler.onConnectionFail();

        //    }
        //}
        //).Start();

        //connecting = true;
        //Thread.CurrentThread.Priority = System.Threading.ThreadPriority.Lowest;
        //connected = true;
        //try
        //{
        //    doConnect(host, port);
        //    messageHandler.onConnectOK();

        //}
        //catch (Exception)
        //{
        //    try
        //    {
        //        Thread.Sleep(500);
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    if (isCancel)
        //        if (messageHandler != null)
        //        {
        //            close();
        //            messageHandler.onConnectionFail();
        //        }
        //}
      
        isCancel = false;
        connecting = true;
        Thread.CurrentThread.Priority = System.Threading.ThreadPriority.Highest;
        connected = true;      
        try
        {
            doConnect(host, port);
            messageHandler.onConnectOK();

        }
        catch (Exception)
        {
            if (messageHandler != null)
            {
                close();
                messageHandler.onConnectionFail();
            }
        }
    }
	public  int timeOut;
    public void doConnect(string host, int port)
    {
		timeOut = (int)(mSystem.currentTimeMillis () / 1000);
        sc = new TcpClient();
        sc.Connect(host, port);
        dataStream = sc.GetStream();
        dis = new BinaryReader(dataStream, new System.Text.UTF8Encoding());
        dos = new BinaryWriter(dataStream, new System.Text.UTF8Encoding());
        new Thread(new ThreadStart(sender.run)).Start();

        MessageCollector myCollect = new MessageCollector();      
        collectorThread = new Thread(new ThreadStart(myCollect.run));      
        collectorThread.Start();      
        timeConnected = currentTimeMillis();

        connecting = false;
        doSendMessage(new Message(-27));
		timeOut = 0;
    }


    public void sendMessage(Message message)
    {

       // Out.Log("Add message: "+ message.command);
        sender.AddMessage(message);
    }

    static private void doSendMessage(Message m)
    {
        sbyte[] data = m.getData();
     
        try
        {
            if (getKeyComplete)
            {
                sbyte b = (writeKey(m.command));
                dos.Write(b);
            }
            else
                dos.Write(m.command);
           if (data != null)
            {
                int size = data.Length;
                //Debug.Log("size message: "+ size);
                if (getKeyComplete)
                {
                    int byte1 = writeKey((sbyte)(size >> 8));
                    dos.Write((sbyte)byte1);
                    int byte2 = writeKey((sbyte)(size & 0xFF));
                    dos.Write((sbyte)byte2);
                }
                else
                    dos.Write((ushort)size);

                if (getKeyComplete)
                {
                    for (int i = 0; i < data.Length; i++)
                    {

                        sbyte temp = writeKey(data[i]);
                        dos.Write(temp);

                        //Debug.Log("tempWK: " + temp);
                        //Debug.Log(".... ");
                    }
                }
                sendByteCount += (5 + data.Length);

            }
            else
            {
                //Debug.Log("no data!");
                if (getKeyComplete)
                {
                    int size2 = 0;
                    int byte1 = writeKey((sbyte)(size2 >> 8));
                    dos.Write((sbyte)byte1);
                    int byte2 = writeKey((sbyte)(size2 & 0xFF));
                    dos.Write((sbyte)byte2);
                }
                else
                    dos.Write((ushort)0);
                sendByteCount += 5;
            }

            dos.Flush();

           // Cout.Log("Send message: "+ m.command + " complete");

        }
        catch (Exception e)
        {
            //Debug.Log(e.StackTrace);
        }
    }

    public static sbyte readKey(sbyte b)
    {
        sbyte i = (sbyte)((key[curR++] & 0xff) ^ (b & 0xff));
        if (curR >= key.Length)
            curR %= (sbyte)key.Length;
        return i;
    }

    public static sbyte writeKey(sbyte b)
    {
        sbyte i = (sbyte)((key[curW++] & 0xff) ^ (b & 0xff));
        if (curW >= key.Length)
            curW %= (sbyte)key.Length;
        return i;
    }

    public class Sender
    {
        public List<Message> sendingMessage;

        public Sender()
        {
            sendingMessage = new List<Message>();
        }

        public void AddMessage(Message message)
        {
            sendingMessage.Add(message);
        }

        public void run(){           
                while (connected){
                    try{
                    if (getKeyComplete)
                        while (sendingMessage.Count > 0)
                        {
                            Message m = (Message)sendingMessage[0];
                            doSendMessage(m);
                            sendingMessage.RemoveAt(0);
//						if(m.command==-32)
//                            Cout.LogError("<><><><>><><><><><><><Send msg  "+m.command);
                        }
                    try
                    {
                        Thread.Sleep(5);
                    }
                    catch (Exception e)
                    {
                        Cout.LogError(e.ToString());
                    }
                     }
                    catch (Exception)
                    {
                        Debug.Log("error send message! ");
                    }
				
                }

           
        }

    }

    class MessageCollector
    {
    
        public void run()
        {
            Message message;          
            try
            {
                while (connected)
                {
                    message = readMessage();
                    if (message != null)
                    {
                        try
                        {
                            if (message.command == -27)
                            {
                                getKey(message);
                            }
                            else
                            {
                               // Debug.Log("Process recieve msg!");
                                onRecieveMsg(message);
                                //messageHandler.onMessage(message);
								
                            }							
							
                        }
                        catch (Exception)
                        {
							Cout.LogError2("LOI NHAN  MESS THU 1");
                        }
                    }
                    else
                    {
                        break;
                    }
                    try
                    {
                        Thread.Sleep(5);
                    }
                    catch (Exception)
                    {
						Cout.println("LOI NHAN  MESS THU 2");
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log("error read message!");
                Debug.Log(e.Message.ToString());
            }
            if (connected)
            {
				Debug.Log("error read message!");
                if (messageHandler != null)
                {
                    if (currentTimeMillis() - timeConnected > 500)
                        messageHandler.onDisconnected();
                    else
                        messageHandler.onConnectionFail();
                   
                }
                if (sc != null)
                    cleanNetwork();
            }
          
        }

        private void getKey(Message message)
        {
            //Debug.Log("---GET KEY----");
            try
            {
                sbyte keySize = message.reader().readSByte();
                key = new sbyte[keySize];
                //Debug.Log("keySize: " + keySize); 
                for (int i = 0; i < keySize; i++)
                {
                    key[i] = message.reader().readSByte();
                    //Debug.Log("key" + i + " : " + key[i]);
                }
                for (int i = 0; i < key.Length - 1; i++)
                    key[i + 1] ^= key[i];
                getKeyComplete = true;

            }
            catch (Exception)
            {
            }

        }
        //private Message readMessage2(sbyte cmd){				
        //    int size;
        //    int b1 = readKey(dis.ReadSByte()) + 128;
        //    int b2 = readKey(dis.ReadSByte()) + 128;
        //    int b3 = readKey(dis.ReadSByte()) + 128;
        //    size = (b3 * 256 + b2) * 256 + b1;
        //    Cout.LogError("SIZE = "+size);
        //    sbyte[] data = new sbyte[size];
             
        //        int byteRead = 0;
        //        byte[] dataTemp = dis.ReadBytes(size);
              
        //        System.Buffer.BlockCopy(dataTemp, 0, data, 0, size);
        //        recvByteCount += (5 + size);
        //        int Kb = (recvByteCount + sendByteCount);
        //        strRecvByteCount = Kb / 1024 + "." + Kb % 1024 / 102 + "Kb";
        //     if (getKeyComplete)
        //            for (int i = 0; i < data.Length; i++)
        //            {
        //                data[i] = readKey((sbyte)data[i]);
        //            }
        //        Message msg = new Message(cmd, data);
			
        //    return msg;
        //}
        private Message readMessage()
        {
            try
            {
                // read message command
                sbyte cmd = dis.ReadSByte();
                if (getKeyComplete)
                    cmd = readKey(cmd);
                // read size of data	
               // Cout.println("................cmd= " + cmd);				
                //if (cmd == -32 || cmd == -66 || cmd == 11 || cmd == -67 || cmd == -74)
                //{
                //    return readMessage2(cmd);
                //}
                // read size of data
                int size;
                if (getKeyComplete)
                {
                    if (cmd == Cmd.LOGIN || cmd == Cmd.REQUEST_IMAGE)
                    {
                        //cmd = dis.ReadSByte();
                        //cmd = readKey(cmd);
                        sbyte[] b = { dis.ReadSByte(), dis.ReadSByte(), dis.ReadSByte(), dis.ReadSByte() };

                        size = ((readKey(b[0]) & 0xff) << 24) | ((readKey(b[1]) & 0xff) << 16) | ((readKey(b[2]) & 0xff) << 8) | (readKey(b[3]) & 0xff);
                      // size = readKey(b[3]) & 0xFF | (readKey(b[2]) & 0xFF) << 8 | (readKey(b[1]) & 0xFF) << 16 | (readKey(b[0]) & 0xFF) << 24;
                       // Cout.println(size + " Read cmd 4byte  " + size2);
                    }
                    else
                    {
                        sbyte b1 = dis.ReadSByte();
                        sbyte b2 = dis.ReadSByte();
                        size = (readKey(b1) & 0xff) << 8 | readKey(b2) & 0xff;
                    }
                }
                else
                {
                    sbyte b1 = dis.ReadSByte();
                    sbyte b2 = dis.ReadSByte();
                    size = (b1 & 0xff << 8 | b2 & 0xff);
                }

                //Cout.println("................size  = " + size);	
                //Debug.Log("mesage size: " + size);

                sbyte[] data = new sbyte[size];
                byte[] dataTemp = dis.ReadBytes(size);
                //byte[] dataTemp=new byte[size];
                //dis.Read(dataTemp,0,dataTemp.Length);
                System.Buffer.BlockCopy(dataTemp, 0, data, 0, size);
                recvByteCount += (5 + size);
                int Kb = (recvByteCount + sendByteCount);
                strRecvByteCount = Kb / 1024 + "." + Kb % 1024 / 102 + "Kb";
                /*while (len != -1 && byteRead < size) {
					
                    for( int i = 0; i < size; i++)
                    {
                        data[i] = dis.ReadSByte();	
                    }
                    len = size;
					
                    if (len > 0) {
                        byteRead += len;
                        recvByteCount += (5 + byteRead);
                        int Kb = (Session_ME.recvByteCount + Session_ME.sendByteCount);
                        strRecvByteCount = Kb / 1024 + "." + Kb % 1024 / 102 + "Kb";
                    }
                }*/
                if (getKeyComplete)
                    for (int i = 0; i < data.Length; i++)
                    {
                        data[i] = readKey((sbyte)data[i]);
                    }
                Message msg = new Message(cmd, data);

                //Debug.Log("recived message: "+cmd + " complete");

                return msg;

            }
            catch (Exception e)
            {
                Debug.Log(e.StackTrace.ToString());

            }

            return null;
        }

    }

    public static mVector recieveMsg = new mVector();

    public static void onRecieveMsg(Message msg){		
        if (Thread.CurrentThread.Name == Main.mainThreadName)
            messageHandler.onMessage(msg);
        else
           recieveMsg.addElement(msg);
    }   
    public static void update(){
		if (Session_ME.gI ().timeOut != 0) {
			if((mSystem.currentTimeMillis () / 1000 - Session_ME.gI().timeOut) > 10)
			{
				Session_ME.gI ().timeOut = 0;
				if(sc!=null)
					cleanNetwork();
				return;
			}

		}
        while(recieveMsg.size() > 0){
            Message msg = (Message)recieveMsg.elementAt(0);
           
            if (msg == null)
            {
                recieveMsg.removeElementAt(0);
                return;
            }            
            messageHandler.onMessage(msg);		
			recieveMsg.removeElementAt(0);         

        }
		
    }
  
    public void close()
    {
        recieveMsg.removeAllElements();
        cleanNetwork();
    }

    private static void cleanNetwork()
    {
        key = null;
        curR = 0;
        curW = 0;
        try
        {
            connected = false;
            connecting = false;
            if (sc != null)
            {
                sc.Close();
                sc = null;
            }
            if (dataStream != null)
            {
                dataStream.Close();
                dataStream = null;
            }
            if (dos != null)
            {
                dos.Close();
                dos = null;
            }
            if (dis != null)
            {
                dis.Close();
                dis = null;
            }
            sendThread = null;
            collectorThread = null;

        }
        catch (Exception) { }

    }


    public static int currentTimeMillis()
    {
        return Environment.TickCount;
        //        DateTime Jan1970 = new DateTime(1970, 1, 1, 0, 0,0,DateTimeKind.Utc);
        //        TimeSpan javaSpan = DateTime.UtcNow - Jan1970;
        //        return javaSpan.TotalMilliseconds;
    }

    public static byte convertSbyteToByte(sbyte var)
    {
        if (var > 0)
            return (byte)var;

        return (byte)(var + 256);
    }

    public static byte[] convertSbyteToByte(sbyte[] var)
    {
        byte[] temp = new byte[var.Length];
        for (int i = 0; i < var.Length; i++)
        {
            if (var[i] > 0)
                temp[i] = (byte)var[i];

            else temp[i] = (byte)(var[i] + 256);
        }

        return temp;
    }


}


