using UnityEngine;
using System.Collections;
using System;
public class mGraphics {
    public static int HCENTER = 1;
    public static int VCENTER = 2;
    public static int LEFT = 4;
    public static int RIGHT = 8;
    public static int TOP = 16;
    public static int BOTTOM = 32;
    private float r, g, b,a;
	public int clipX,clipY,clipW,clipH;
	bool isClip=false,isTranslate=true;
	int translateX, translateY;
    public static int zoomLevel = 1;
    public const int BASELINE = 64;
    public const int SOLID = 0;
    public const int DOTTED = 1;

	public const int TRANS_MIRROR = 2;
    public const int TRANS_MIRROR_ROT180 = 1;
    public const int TRANS_MIRROR_ROT270 = 4;
    public const int TRANS_MIRROR_ROT90 = 7;
    public const int TRANS_NONE = 0;
    public const int TRANS_ROT180 = 3;
    public const int TRANS_ROT270 = 6;
    public const int TRANS_ROT90 = 5;   
	public static Hashtable cachedTextures=new Hashtable();
    public static int addYWhenOpenKeyBoard;
    public bool isDrawLine;
	void cache(string key, Texture value)
	{
		if(cachedTextures.Count>400){
			cachedTextures.Clear();			
		}
        if (value.width * value.height < GameCanvas.w * GameCanvas.h)
			cachedTextures.Add(key,value);		
	}
	
	public mGraphics (){}
	
	public void translate(int tx,int ty)
	{
        tx *= zoomLevel;
        ty *= zoomLevel;
		translateX+=tx;
		translateY+=ty;
		isTranslate=true;
		if(translateX==0&&translateY==0)isTranslate=false;
	}
	public int getTranslateX()
	{
		return translateX/zoomLevel;
	}
	public int getTranslateY()
	{
		return (translateY/zoomLevel)+addYWhenOpenKeyBoard;
	}

    private int clipTX, clipTY;
	public void setClip(int x,int y,int w,int h)
	{
//		if (x <= 0) {
//			x -= 100;
//			w+=200;
//		}
//		if (y <= 0) {
//			y -= 100;
//			h+=200;
//		}
        x *= zoomLevel;
        y *= zoomLevel;
        w *= zoomLevel;
        h *= zoomLevel;
	    clipTX = translateX;
	    clipTY = translateY;
		clipX=x;
		clipY=y;
		clipW=w;
		clipH=h;
		isClip=true;      
        
	}
    public void fillRect(int x, int y, int w, int h, int color, int alpha)
    {
        //x *= zoomLevel;
        //y *= zoomLevel;
        //w *= zoomLevel;
        //h *= zoomLevel;
        float a = 0.5f;
        //if (alpha == 90)
        //    a = 0.7f;
        setColor(color,a);     
        fillRect(x, y, w, h);
    }
    public void disableBlending()
    {
        a = 256f;
    }
    public void drawLine(int x1, int y1, int x2, int y2,bool isUclip)
    {
        for (int ii = 0; ii < zoomLevel; ii++)
        {
            _drawLine(x1 + ii, y1 + ii, x2 + ii, y2 + ii);
            if (ii > 0)
            {
                _drawLine(x1 + ii, y1, x2 + ii, y2);
                _drawLine(x1, y1 + ii, x2, y2 + ii);

            }
        }
        //x1 *= zoomLevel;
        //y1 *= zoomLevel;
        //x2 *= zoomLevel;
        //y2 *= zoomLevel;
        //if (isTranslate)
        //{
        //    x1 += translateX;
        //    y1 += translateY;
        //    x2 += translateX;
        //    y2 += translateY;
        //}
        //lineMaterial.SetPass(0);
        //GL.PushMatrix();
        //GL.Begin(GL.LINES);
        //GL.Color(new Color(r, g, b, a));      
        //for (int i = 0; i < zoomLevel; i++){         

        //    GL.Vertex(new Vector2(x1 + i, y1 + i));
        //    GL.Vertex(new Vector2(x2 + i, y2 + i));     
        //    if (i > 0){     

        //       GL.Vertex(new Vector2(x1 + i, y1));
        //       GL.Vertex(new Vector2(x2 + i, y2));         

        //       GL.Vertex(new Vector2(x1, y1 + i));
        //       GL.Vertex(new Vector2(x2, y2 + i)); 
        //    }
        //}      
        //GL.End();
        //GL.PopMatrix();        
    }
    public void drawLine(int x1,int y1,int x2,int y2){
        for (int ii = 0; ii < zoomLevel; ii++){            
            _drawLine(x1 + ii, y1 + ii, x2 + ii, y2 + ii);
            if (ii > 0)
            {
                _drawLine(x1 + ii, y1, x2 + ii, y2);
                _drawLine(x1, y1 + ii, x2, y2 + ii);
              
            }
        }     
    }
    public void drawLine(int x1, int y1, int x2, int y2,int t)
    {
        for (int ii = 0; ii < t; ii++)
        {
            _drawLine(x1 + ii, y1 + ii, x2 + ii, y2 + ii);
            if (ii > 0)
            {
                _drawLine(x1 + ii, y1, x2 + ii, y2);
                _drawLine(x1, y1 + ii, x2, y2 + ii);

            }
        }
    }

    public mVector totalLine = new mVector();
    public void drawlineGL()
    {
        lineMaterial.SetPass(0);
        GL.PushMatrix();
        GL.Begin(GL.LINES);       
        for (int i = 0; i < totalLine.size(); i++)
        {
            mLine line = (mLine)totalLine.elementAt(i);
            GL.Color(new Color(line.r, line.g, line.b, line.a));
            int x1 = line.x1 *zoomLevel;
            int y1 = line.y1 * zoomLevel;
            int x2 = line.x2 * zoomLevel;
            int y2 = line.y2 * zoomLevel;
            if (isTranslate)
            {
                x1 += translateX;
                y1 += translateY;
                x2 += translateX;
                y2 += translateY;
            }
            for (int ii = 0; ii < zoomLevel; ii++)
            {
                GL.Vertex(new Vector2(x1 + ii, y1 + ii));
                GL.Vertex(new Vector2(x2 + ii, y2 + ii));
                if (ii > 0)
                {
                    GL.Vertex(new Vector2(x1 + ii, y1));
                    GL.Vertex(new Vector2(x2 + ii, y2));

                    GL.Vertex(new Vector2(x1, y1 + ii));
                    GL.Vertex(new Vector2(x2, y2 + ii));
                }
            }      
        }
        GL.End();
        GL.PopMatrix();       
        totalLine.removeAllElements();
    }
    public void test()
    {


        GL.PushMatrix();
        lineMaterial.SetPass(0);
        GL.LoadPixelMatrix();


       // SpriteRenderer 
     //   GL.LoadOrtho();
      //  GL.Color(Color.red);
      //  GL.Begin(GL.TRIANGLES);
       // drawImagaByDrawTexture(AvMain.imgButtonBar,10,100);
        
        GL.Vertex(new Vector2(10,10));
        GL.Vertex(new Vector2(100, 100));
        GL.End();
        GL.PopMatrix();
    }
    private Material lineMaterial;
    public void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            lineMaterial = new Material("Shader \"Lines/Colored Blended\" {" +
            "SubShader { Pass { " +
            " Blend SrcAlpha OneMinusSrcAlpha " +
            " ZWrite Off Cull Off Fog { Mode Off } " +
            " BindChannels {" +
            " Bind \"vertex\", vertex Bind \"color\", color }" +
            "} } }");
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
        }
    }
    public void drawLine2(int x1, int y1, int x2, int y2, float r, float b, float g, float a)
    {      
        _drawLine2(x1, y1, x2, y2,r,b,g,a);       
    }
    public void _drawLine2(int x1, int y1, int x2, int y2,float r, float b, float g,float a)
    {
        x1 *= zoomLevel;
        y1 *= zoomLevel;
        x2 *= zoomLevel;
        y2 *= zoomLevel;
        if (isTranslate)
        {
            x1 += translateX;
            y1 += translateY;
            x2 += translateX;
            y2 += translateY;
        }
        lineMaterial.SetPass(0);
        GL.PushMatrix();
        GL.Begin(GL.LINES);
        GL.Color(new Color(r,g,b,a));
        GL.Vertex(new Vector2(x1, y1));
        GL.Vertex(new Vector2(x2, y2));
        GL.End();

        GL.PopMatrix();
    }
    public void _drawLine(int x1, int y1, int x2, int y2)
    {
        if (y1 == y2)
        {
            if (x1 > x2)
            {
                int tem = x2;
                x2 = x1;
                x1 = tem;
            }
            fillRect(x1, y1, x2 - x1, 1);
            return;
        }
        if (x1 == x2)
        {
            if (y1 > y2)
            {
                int tem = y2;
                y2 = y1;
                y1 = tem;
            }
            fillRect(x1, y1, 1, y2 - y1);
            return;
        }

        x1 *= zoomLevel;
        y1 *= zoomLevel;
        x2 *= zoomLevel;
        y2 *= zoomLevel;
        if (isTranslate)
        {
            x1 += translateX;
            y1 += translateY;
            x2 += translateX;
            y2 += translateY;
        }
        Texture2D rgb_texture;
        string key = "dl" + r + g + b;
        rgb_texture = (Texture2D)cachedTextures[key];
        if (rgb_texture == null)
        {
            rgb_texture = new Texture2D(1, 1);
            Color rgb_color = new Color(r, g, b);
            rgb_texture.SetPixel(0, 0, rgb_color);
            rgb_texture.Apply();
            cache(key, rgb_texture);
        }
        Vector2 start = new Vector2(x1, y1);
        Vector2 end = new Vector2(x2, y2);
        Vector2 d = end - start;
        float a = Mathf.Rad2Deg * Mathf.Atan(d.y / d.x);
        if (d.x < 0)
            a += 180;
        int width2 = (int)Mathf.Ceil(1 / 2);
        GUIUtility.RotateAroundPivot(a, start);
        //=============================
        int cx = 0, cy = 0, cw = 0, ch = 0;
        if (isClip)
        {
            cx = clipX; cy = clipY; cw = clipW; ch = clipH;
            if (isTranslate)
            {
                cx += clipTX;
                cy += clipTY;
            }
        }
        if (isClip)
            GUI.BeginGroup(new Rect(cx, cy, cw, ch));
        Graphics.DrawTexture(new Rect(start.x - cx, start.y - width2 - cy, d.magnitude, 1), rgb_texture);
        if (isClip)
            GUI.EndGroup();

        GUIUtility.RotateAroundPivot(-a, start);
    }
	 public Color setColorMiniMap(int rgb)
    {   int blue = rgb & 0xFF;
        int green = (rgb >> 8) & 0xFF;
        int red = (rgb >> 16) & 0xFF;
        float b = (float)blue / 256;
        float g = (float)green / 256;
        float r = (float)red / 256;       
        Color cl = new Color(r,g,b);		
        return cl;
    }

     //public static Color setColor(int rgb)
     //{
     //    int blue = rgb & 0xFF;
     //    int green = (rgb >> 8) & 0xFF;
     //    int red = (rgb >> 16) & 0xFF;
     //    float b = (float)blue / 256;
     //    float g = (float)green / 256;
     //    float r = (float)red / 256;
     //    Color cl = new Color(r, g, b);
     //    return cl;
     //}
	public float []getRGB(Color cl){
		float red=(256*cl.r);
		float green=256*cl.g;
		float blue=256*cl.b;
		float []arr=new float[3];
		arr[0]=red;
		arr[1]=green;
		arr[2]=blue;
		return arr;
	}
	public void drawRect(int x,int y,int w,int h){      
		int xx=1;
		fillRect(x,y,w,xx);
		fillRect(x,y,xx,h);
		fillRect(x+w,y,xx,h+1);
		fillRect(x,y+h,w+1,xx);
	}
    public void drawRect(int x, int y, int w, int h,bool isUclip)
    {
        int xx = 1;
        fillRect(x, y, w, xx);
        fillRect(x, y, xx, h);
        fillRect(x + w, y, xx, h + 1);
        fillRect(x, y + h, w + 1, xx);
    }   
    public void fillRect(int x, int y, int w, int h){
        x *= zoomLevel;
        y *= zoomLevel;
        w *= zoomLevel;
        h *= zoomLevel;
          if (w < 0 || h < 0)
            return;
        if (isTranslate){
            x += translateX;
            y += translateY;
        }
        int ww = 1, hh = 1;
        Texture2D rgb_texture;
        string key = "fr" + ww + hh + r + g + b+a;
        rgb_texture = (Texture2D)cachedTextures[key];
        if (rgb_texture == null){
            rgb_texture = new Texture2D(ww, hh);
            Color rgb_color = new Color(r, g, b,a);
            rgb_texture.SetPixel(0, 0, rgb_color);
            rgb_texture.Apply();
            cache(key, rgb_texture);
        }      
        int cx = 0, cy = 0, cw = 0, ch = 0;
        if (isClip){
            cx = clipX; cy = clipY; cw = clipW; ch = clipH;
            if (isTranslate){
                cx += clipTX;
                cy += clipTY;
            }
        }     
        if (isClip)
            GUI.BeginGroup(new Rect(cx, cy, cw, ch));
        GUI.DrawTexture(new Rect(x - cx, y - cy, w, h), rgb_texture);
        if (isClip)
            GUI.EndGroup();      
    }
    public void fillRect(int x, int y, int w, int h, bool isClip)
    {
        x *= zoomLevel;
        y *= zoomLevel;
        w *= zoomLevel;
        h *= zoomLevel;
        if (w < 0 || h < 0)
            return;
        if (isTranslate)
        {
            x += translateX;
            y += translateY;
        }
        int ww = 1, hh = 1;
        Texture2D rgb_texture;
        string key = "fr" + ww + hh + r + g + b + a;
        rgb_texture = (Texture2D)cachedTextures[key];
        if (rgb_texture == null)
        {
            rgb_texture = new Texture2D(ww, hh);
            Color rgb_color = new Color(r, g, b, a);
            rgb_texture.SetPixel(0, 0, rgb_color);
            rgb_texture.Apply();
            cache(key, rgb_texture);
        }
        int cx = 0, cy = 0, cw = 0, ch = 0;
        if (isClip)
        {
            cx = clipX; cy = clipY; cw = clipW; ch = clipH;
            if (isTranslate)
            {
                cx += clipTX;
                cy += clipTY;
            }
        }
        if (isClip)
            GUI.BeginGroup(new Rect(cx, cy, cw, ch));
        GUI.DrawTexture(new Rect(x - cx, y - cy, w, h), rgb_texture);
        if (isClip)
            GUI.EndGroup();
    }
    public void setColor(int rgb){
        int blue = rgb & 0xFF;
        int green = (rgb >> 8) & 0xFF;
        int red = (rgb >> 16) & 0xFF;
        b = (float)blue/256;
        g = (float)green / 256;
        r = (float)red / 256;
		a = 255f;
    }
	public void setColor(Color color){		
		b=color.b;
		g=color.g;
		r=color.r;	
	}
    private int currentBGColor;
    public void setBgColor(int rgb){
        if(rgb!=currentBGColor){
            currentBGColor = rgb;
            int blue = rgb & 0xFF;
            int green = (rgb >> 8) & 0xFF;
            int red = (rgb >> 16) & 0xFF;
            b = (float)blue / 256;
            g = (float)green / 256;
            r = (float)red / 256;			
            Main.main.GetComponent<Camera>().backgroundColor=new Color(r,g,b);
        }
    }
    public void fillCircle(int x, int y , int a, int b)
    {
    }
    public void drawString(string s, int x, int y, GUIStyle style){
        x *= zoomLevel;
        y *= zoomLevel;
		if(isTranslate){
			x+=translateX;
			y+=translateY;
		}	
		int cx=0,cy=0,cw=0,ch=0;
		if(isClip){
			cx=clipX;cy=clipY;cw=clipW;ch=clipH;
			if(isTranslate){
                cx += clipTX;
                cy += clipTY;
			}
		}
		if(isClip)
			GUI.BeginGroup(new Rect(cx,cy,cw,ch));		
		GUI.Label(new Rect(x-cx, y-cy, ScaleGUI.WIDTH, 100), s, style);			  	
		if(isClip)
			GUI.EndGroup();		
	}
	 public void setColor(int rgb, float alpha){
        //0.1f-0.9f
        int blue = rgb & 0xFF;
        int green = (rgb >> 8) & 0xFF;
        int red = (rgb >> 16) & 0xFF;
        b = (float)blue / 256;
        g = (float)green / 256;
        r = (float)red / 256;
        a = alpha / 100; ;
    }
	public void drawString(string s, int x, int y, GUIStyle style,int wString){
        x *= zoomLevel;
        y *= zoomLevel;
        wString *= zoomLevel;
		if(isTranslate){
			x+=translateX;
			y+=translateY;
		}	
		int cx=0,cy=0,cw=0,ch=0;
		if(isClip){
			cx=clipX;cy=clipY;cw=clipW;ch=clipH;
			if(isTranslate){
                cx += clipTX;
                cy += clipTY;
			}
		}
		if(isClip)
			GUI.BeginGroup(new Rect(cx,cy,cw,ch));		
		int aa = 0;
		if (wString > ScaleGUI.WIDTH)
			aa = wString;
		GUI.Label(new Rect(x - cx, y - cy - 4, ScaleGUI.WIDTH + aa, 100), s, style);		  	
		if(isClip)
			GUI.EndGroup();		
	}
	 void UpdatePos(int anchor){
        Vector2 cornerPos = new Vector2(0, 0);

        //overwrite the items position
        switch (anchor)
        {
            case 3: //VCENTER|HCENTER:
                cornerPos = new Vector2(size.x / 2, size.y / 2);
                break;
            case 20: //TOP|LEFT:
                cornerPos = new Vector2(0, 0);
                break;
            case 17: //TOP|HCENTER:
                cornerPos = new Vector2(Screen.width / 2, 0);
                break;
            case 24: //TOP|RIGHT:
                cornerPos = new Vector2(Screen.width, 0);
                break;
            case 6: //VCENTER|LEFT:
                cornerPos = new Vector2(0, size.y / 2);
                break;
            case 10: //VCENTER|RIGHT:
                cornerPos = new Vector2(Screen.width, Screen.height / 2);
                break;
            case 36: //BOTTOM|LEFT:
                cornerPos = new Vector2(0, Screen.height);
                break;
            case 33: //BOTTOM|HCENTER:
                cornerPos = new Vector2(Screen.width / 2, Screen.height);
                break;
            case 40: //BOTTOM|RIGHT:
                cornerPos = new Vector2(Screen.width, Screen.height);
                break;
            default:
                break;
        }
        pos = cornerPos + relativePosition;
        rect = new Rect(pos.x - size.x * 0.5f, pos.y - size.y * 0.5f, size.x, size.y);
        pivot = new Vector2(rect.xMin + rect.width * 0.5f, rect.yMin + rect.height * 0.5f);
    }
	Vector2 pos = new Vector2(0, 0);
	Rect rect;
    Matrix4x4 matrixBackup;
	Vector2 pivot;
	public Vector2 size = new Vector2(128, 128);
	public Vector2 relativePosition = new Vector2(0, 0);
    public void drawRegion(mBitmap arg0, int x0, int y0, int w0, int h0, int arg5, int x, int y, int arg8) {

        x *= zoomLevel;
        y *= zoomLevel;
        x0 *= zoomLevel;
        y0 *= zoomLevel;
        w0 *= zoomLevel;
        h0 *= zoomLevel;
        _drawRegion(arg0.image, x0, y0, w0, h0, arg5, x, y, arg8);
	}
    public void drawRegion(mBitmap arg0, int x0, int y0, int w0, int h0, int arg5, int x, int y, int arg8, bool isUclip)
    {
        x *= zoomLevel;
        y *= zoomLevel;
        x0 *= zoomLevel;
        y0 *= zoomLevel;
        w0 *= zoomLevel;
        h0 *= zoomLevel;
        _drawRegion(arg0.image, x0, y0, w0, h0, arg5, x, y, arg8);
    }
    public void _drawRegionRotateOpacity2(mBitmap image, float x0, float y0, int w, int h, int transform, int x, int y, int anchor, int rotate, int opacity)
    { // chua opacity
        if (image == null) return;
        if (isTranslate)
        {
            x += translateX;
            y += translateY;
        }
        float nw = w;
        float nh = h;
        float dx = 0;
        float dy = 0;
        float tx = 0, ty = 0;
        float f = 1, dxf = 0;
        int yyy = 1;
        float fx = 0, fy = 0;
        int tempX = 0;
        int tempY = 0;
        if ((anchor & VCENTER) == VCENTER && (anchor & LEFT) == LEFT)
        {
            //x -= w / 2; //xoa giữa
            y -= h / 2;//xoa giữa
        }
        else if ((anchor & VCENTER) == VCENTER && (anchor & HCENTER) == HCENTER)
        {
            x -= w / 2; //xoa giữa
            y -= h / 2;//xoa giữa
        }
        matrixBackup = GUI.matrix;
        size = new Vector2(w, h);
        relativePosition = new Vector2(x, y);
        //UpdatePos(20);
        size = new Vector2(w, h);
        if ((anchor & VCENTER) == VCENTER && (anchor & LEFT) == LEFT)
        {
            UpdatePos(6);
            // pivot = new Vector2(0,0.5f);
            // x -= w / 2; 
        }
        else if ((anchor & VCENTER) == VCENTER && (anchor & HCENTER) == HCENTER)
        {
            UpdatePos(3);//3xoay ngay giua 20 xoay goc
        }

        GUIUtility.RotateAroundPivot(rotate, pivot);

        GUI.color = new Color32(255, 255, 255, (byte)opacity);
        Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy),
                                image.image.texture,
                             new Rect((float)(x0 + dx + dxf) / image.image.texture.width,
                                 ((float)(image.image.texture.height - nh - (y0 + dy))) / image.image.texture.height,
                 (float)nw / image.image.texture.width, (float)nh / image.image.texture.height), 0, 0, 0, 0);
        //if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 || transform == TRANS_MIRROR_ROT90)
        //{

        GUI.color = new Color32(255, 255, 255, 255);
        GUI.matrix = matrixBackup;
        //}
    }
    public void _drawRegionOpacity2(Image image, float x0, float y0, int w, int h, int transform, int x, int y, int anchor, int opacity)
    {

        if (image == null) return;
        if (isTranslate)
        {
            x += translateX;
            y += translateY;
        }
        Texture2D rgb_texture;
        string key = "dg" + x0 + y0 + w + h + transform + image.GetHashCode();
        rgb_texture = (Texture2D)cachedTextures[key];

        if (rgb_texture == null)
        {
            Image imgRegion = Image.createImage(image, (int)x0, (int)y0, w, h, transform);
            rgb_texture = imgRegion.texture;
            cache(key, rgb_texture);
        }

        //=============================

        int cx = 0, cy = 0, cw = 0, ch = 0;

        float nw = w;
        float nh = h;
        float tx = 0, ty = 0;

        if ((anchor & HCENTER) == HCENTER)
            tx -= nw / 2;
        if ((anchor & VCENTER) == VCENTER)
            ty -= nh / 2;
        if ((anchor & RIGHT) == RIGHT)
            tx -= nw;
        if ((anchor & BOTTOM) == BOTTOM)
            ty -= nh;

        x += (int)tx;
        y += (int)ty;

        if (isClip)
        {
            cx = clipX; cy = clipY; cw = clipW; ch = clipH;
            if (isTranslate)
            {
                cx += clipTX;
                cy += clipTY;
            }
        }
        if (isClip)
            GUI.BeginGroup(new Rect(cx, cy, cw, ch));
        GUI.color = new Color32(255, 255, 255, (byte)opacity);
        GUI.DrawTexture(new Rect(x - cx, y - cy, w, h), rgb_texture);
        GUI.color = new Color32(255, 255, 255, 255);
        if (isClip)
            GUI.EndGroup();
    }
    public void _drawRegionOpacity(Image image, float x0, float y0, int w, int h, int transform, int x, int y, int anchor, int opacity)
    {

        if (image == null) return;
        if (isTranslate)
        {
            x += translateX;
            y += translateY;
        }
        float nw = w;
        float nh = h;
        float dx = 0;
        float dy = 0;
        float tx = 0, ty = 0;
        float f = 1, dxf = 0;
        int yyy = 1;
        if ((anchor & HCENTER) == HCENTER)
        {
            tx -= nw / 2;
        }
        if ((anchor & VCENTER) == VCENTER)
        {
            ty -= nh / 2;
        }
        if ((anchor & RIGHT) == RIGHT)
        {
            tx -= nw;
        }
        if ((anchor & BOTTOM) == BOTTOM)
        {
            ty -= nh;
        }
        if ((anchor & TOP) == TOP && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
        {
            tx = nh / 2 - nw / 2 - 1;
            ty = nw / 2 - nh / 2;
        }
        if ((anchor & BOTTOM) == BOTTOM && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
        {
            ty = -nw / 2 - nh / 2;
            tx = nh / 2 - nw / 2 + -1;
        }
        if ((anchor & TOP) == TOP && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
        {
            tx = (nh / 2 - nw / 2 - nh);
            ty = nw / 2 - nh / 2;
        }
        if ((anchor & BOTTOM) == BOTTOM && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
        {
            ty = -nw / 2 - nh / 2;
            tx = (nh / 2 - nw / 2 - nh);
        }
        x += (int)tx;
        y += (int)ty;
        int cx = 0, cy = 0, cw = 0, ch = 0;
        if (isClip)
        {
            cx = clipX; cy = clipY; cw = clipW; ch = clipH;
            if (isTranslate)
            {
                cx += clipTX;
                cy += clipTY;
            }
            Rect r1 = new Rect(x, y, w, h);
            Rect r2 = new Rect(cx, cy, cw, ch);
            Rect r3 = intersectRect(r1, r2);
            if (r3.width <= 0 || r3.height <= 0) return;
            nw = r3.width;
            nh = r3.height;
            dx = r3.x - r1.x;
            dy = r3.y - r1.y;
        }
        float fx = 0, fy = 0;
        if (transform == 2)
        {
            fx += nw;
            f = -1;
            if (isClip)
            {
                if (cx > x)
                    dxf = -dx;
                else if (cx + cw < x + w)
                    dxf = -(cx + cw - x - w);
            }
        }
        else if (transform == TRANS_MIRROR_ROT180)
        {
            yyy = -1;
            fy += nh;
        }
        else if (transform == TRANS_ROT180)
        {
            yyy = -1;
            fy += nh;
            f = -1;
            fx += nw;
        }
        int tempX = 0;
        int tempY = 0;
        if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 || transform == TRANS_MIRROR_ROT90) // rotate 90, 180, 270 degree
        {
            matrixBackup = GUI.matrix;
            size = new Vector2(w, h);
            relativePosition = new Vector2(x, y);
            UpdatePos(3);
            if (transform == TRANS_ROT270)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(0, pivot);
            }
            else if (transform == TRANS_ROT90)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(90, pivot);
            }
            if (transform == TRANS_ROT270)
            {
                GUIUtility.RotateAroundPivot(270, pivot);
            }
            else if (transform == TRANS_MIRROR_ROT270)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(90, pivot);
                yyy = -1;
                fy += nh;
            }
            else if (transform == TRANS_MIRROR_ROT90)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(270, pivot);
                yyy = -1;
                fy += nh;
            }
        }
        //      GUI.DrawTextureWithTexCoords(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy), image.texture,
        //                           new Rect((float)(x0 + dx + dxf) / image.texture.width, ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
        //               (float)nw / image.texture.width, (float)nh / image.texture.height));
        Color32 colordem = GUI.color;
        GUI.color = new Color32(255, 255, 255, (byte)opacity);
       // GUI.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy), image.texture);
       // GUI.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy), image.texture);
        Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy),
                                image.texture,
                             new Rect((float)(x0 + dx + dxf) / image.texture.width,
                                 ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
                 (float)nw / image.texture.width, (float)nh / image.texture.height), 0, 0, 0, 0, GUI.color);
        //Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy),
        //                        image.texture,
        //                     new Rect((float)(x0 + dx + dxf) / image.texture.width,
        //                         ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
        //         (float)nw / image.texture.width, (float)nh / image.texture.height), 0, 0, 0, 0);

        GUI.color = colordem;
        if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 || transform == TRANS_MIRROR_ROT90)
        {
            GUI.matrix = matrixBackup;
        }
    }
    public void _drawRegionScale(Image image, float x0, float y0, int w, int h, int transform, int x, int y, int anchor, int scale)
    {

        if (image == null) return;
        if (isTranslate)
        {
            x += translateX;
            y += translateY;
        }
        float nw = w;
        float nh = h;
        float dx = 0;
        float dy = 0;
        float tx = 0, ty = 0;
        float f = 1, dxf = 0;
        int yyy = 1;
        float a = scale;
        float tilez = a / 100;
        if ((anchor & HCENTER) == HCENTER)
        {
            tx -= nw / 2;
        }
        if ((anchor & VCENTER) == VCENTER)
        {
            ty -= nh / 2;
        }
        if ((anchor & RIGHT) == RIGHT)
        {
            tx -= nw;
        }
        if ((anchor & BOTTOM) == BOTTOM)
        {
            ty -= nh;
        }
        if ((anchor & TOP) == TOP && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
        {
            tx = nh / 2 - nw / 2 - 1;
            ty = nw / 2 - nh / 2;
        }
        if ((anchor & BOTTOM) == BOTTOM && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
        {
            ty = -nw / 2 - nh / 2;
            tx = nh / 2 - nw / 2 + -1;
        }
        if ((anchor & TOP) == TOP && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
        {
            tx = (nh / 2 - nw / 2 - nh);
            ty = nw / 2 - nh / 2;
        }
        if ((anchor & BOTTOM) == BOTTOM && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
        {
            ty = -nw / 2 - nh / 2;
            tx = (nh / 2 - nw / 2 - nh);
        }
        x += (int)tx;
        y += (int)ty;
        int cx = 0, cy = 0, cw = 0, ch = 0;
        if (isClip)
        {
            cx = clipX; cy = clipY; cw = clipW; ch = clipH;
            if (isTranslate)
            {
                cx += clipTX;
                cy += clipTY;
            }
            Rect r1 = new Rect(x, y, w, h);
            Rect r2 = new Rect(cx, cy, cw, ch);
            Rect r3 = intersectRect(r1, r2);
            if (r3.width <= 0 || r3.height <= 0) return;
            nw = r3.width;
            nh = r3.height;
            dx = r3.x - r1.x;
            dy = r3.y - r1.y;
        }
        float fx = 0, fy = 0;
        if (transform == 2)
        {
            fx += nw;
            f = -1;
            if (isClip)
            {
                if (cx > x)
                    dxf = -dx;
                else if (cx + cw < x + w)
                    dxf = -(cx + cw - x - w);
            }
        }
        else if (transform == TRANS_MIRROR_ROT180)
        {
            yyy = -1;
            fy += nh;
        }
        else if (transform == TRANS_ROT180)
        {
            yyy = -1;
            fy += nh;
            f = -1;
            fx += nw;
        }
        int tempX = 0;
        int tempY = 0;
        if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 || transform == TRANS_MIRROR_ROT90) // rotate 90, 180, 270 degree
        {
            matrixBackup = GUI.matrix;
            size = new Vector2(w, h);
            relativePosition = new Vector2(x, y);
            UpdatePos(3);
            if (transform == TRANS_ROT270)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(0, pivot);
            }
            else if (transform == TRANS_ROT90)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(90, pivot);
            }
            if (transform == TRANS_ROT270)
            {
                GUIUtility.RotateAroundPivot(270, pivot);
            }
            else if (transform == TRANS_MIRROR_ROT270)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(90, pivot);
                yyy = -1;
                fy += nh;
            }
            else if (transform == TRANS_MIRROR_ROT90)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(270, pivot);
                yyy = -1;
                fy += nh;
            }
        }
        //      GUI.DrawTextureWithTexCoords(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy), image.texture,
        //                           new Rect((float)(x0 + dx + dxf) / image.texture.width, ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
        //               (float)nw / image.texture.width, (float)nh / image.texture.height));
        
        nw *= tilez;
        nh *= tilez;
        x += (int)(nw * tilez); //chi cho truong hop vcenter|hcenter
        y += (int)(nh * tilez);
        Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy),
                                image.texture,
                             new Rect((float)(x0 + dx + dxf) / (image.texture.width * tilez),
                                 ((float)(image.texture.height * tilez - nh - (y0 + dy))) / (image.texture.height * tilez),
                 (float)nw / (image.texture.width * tilez), (float)nh / (image.texture.height * tilez)), 0, 0, 0, 0);
        if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 || transform == TRANS_MIRROR_ROT90)
        {
            GUI.matrix = matrixBackup;
        }   
    }
	public void _drawRegion(Image image, float x0, float y0, int w, int h, int transform, int x, int y, int anchor){

		if (image == null) return;      
		if (isTranslate){
			x += translateX;
			y += translateY;
		}
        
        x0 = (x0 < 0 ? 0 : x0);
        y0 = (y0 < 0 ? 0 : y0);

        w = (x0 + w > image.getRealImageWidth() ? (int)(image.getRealImageWidth() - x0) : w);
        h = (y0 + h > image.getRealImageHeight() ? (int)(image.getRealImageHeight() - y0) : h);
         
		float nw = w;
		float nh = h;
		float dx = 0;
		float dy = 0;
		float tx = 0, ty = 0;
		float f = 1, dxf = 0;
		int yyy = 1;	
		if ((anchor & HCENTER) == HCENTER){
			tx -= nw / 2 ;
		}
		if ((anchor & VCENTER) == VCENTER){
			ty -= nh / 2 ;
		}
		if ((anchor & RIGHT) == RIGHT){
			tx -= nw ;
		}
		if ((anchor & BOTTOM) == BOTTOM){
			ty -= nh ;
		}
        if ((anchor & TOP) == TOP && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
        {
            tx = nh / 2 - nw / 2 - 1;
            ty = nw / 2 - nh / 2;
        }
        if ((anchor & BOTTOM) == BOTTOM && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
        {
            ty = -nw / 2 - nh/2;
            tx = nh / 2 - nw / 2 + -1;
        }
        if ((anchor & BOTTOM) == BOTTOM && (anchor & LEFT) == LEFT && transform == TRANS_ROT180)
        {
            ty = - nh;
            tx = 0;
        }
        if ((anchor & TOP) == TOP && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
        {
            tx = (nh / 2 - nw / 2 - nh);
            ty = nw / 2 - nh / 2;
        }
        if ((anchor & BOTTOM) == BOTTOM && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
        {
            ty = -nw / 2 - nh / 2;
            tx = (nh / 2 - nw / 2 - nh);
        }
		x += (int)tx;
		y += (int)ty;
		int cx = 0, cy = 0, cw = 0, ch = 0;
		if (isClip){  
			cx = clipX; cy = clipY; cw = clipW; ch = clipH;          
			if (isTranslate){
				cx += clipTX;
				cy += clipTY;
			}
			Rect r1 = new Rect(x, y, w, h);
			Rect r2 = new Rect(cx, cy, cw, ch);
			Rect r3 = intersectRect(r1, r2);
			if (r3.width <= 0 || r3.height <= 0) return;           
			nw = r3.width;
			nh = r3.height;
			dx = r3.x - r1.x;
			dy = r3.y - r1.y;
		}
		float fx = 0, fy = 0;
		if (transform == 2){
			fx += nw;
			f = -1;
			if (isClip){
				if (cx > x)
					dxf = -dx;
				else if (cx + cw < x + w)
					dxf = -(cx + cw - x - w);
			}
		}
		else if (transform == TRANS_MIRROR_ROT180){
			yyy = -1;
			fy += nh;
		}
		else if (transform == TRANS_ROT180){
			yyy = -1;
			fy += nh;
			f = -1;
			fx += nw;
		}
		int tempX = 0;
		int tempY = 0;
		if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 || transform == TRANS_MIRROR_ROT90) // rotate 90, 180, 270 degree
		{
			matrixBackup = GUI.matrix;
			size = new Vector2(w, h);
			relativePosition = new Vector2(x, y);
			UpdatePos(3);
            if (transform == TRANS_ROT270)
            {
				size = new Vector2(w, h-1);
				UpdatePos(3);
				GUIUtility.RotateAroundPivot(0, pivot);     
			}else if (transform == TRANS_ROT90){
				size = new Vector2(w, h-1);
				UpdatePos(3);
				GUIUtility.RotateAroundPivot(90, pivot);     
			}
			if (transform == TRANS_ROT270){
				GUIUtility.RotateAroundPivot(270, pivot);              
			}else if (transform == TRANS_MIRROR_ROT270){ 
				size = new Vector2(w, h-1);
				UpdatePos(3);
				GUIUtility.RotateAroundPivot(90, pivot);   
				yyy = -1;
				fy += nh; 
			}else if (transform == TRANS_MIRROR_ROT90){				
				size = new Vector2(w, h-1);
				UpdatePos(3);
				GUIUtility.RotateAroundPivot(270, pivot);   
				yyy = -1;
				fy += nh;
			}
		}
  //      GUI.DrawTextureWithTexCoords(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy), image.texture,
  //                           new Rect((float)(x0 + dx + dxf) / image.texture.width, ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
  //               (float)nw / image.texture.width, (float)nh / image.texture.height));
        
        Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy),
                                image.texture,
                             new Rect((float)(x0 + dx + dxf) / image.texture.width,
                                 ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
                 (float)nw / image.texture.width, (float)nh / image.texture.height), 0, 0, 0, 0);
        if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 || transform == TRANS_MIRROR_ROT90)
        {
            GUI.matrix = matrixBackup;
        }   
	}
    public void drawRegionRotate(Image image, float x0, float y0, int w, int h, int transform, int x, int y, int anchor,int rotate2)
    {
        if (image == null) return;
        if (isTranslate)
        {
            x += translateX;
            y += translateY;
        }
        float nw = w;
        float nh = h;
        float dx = 0;
        float dy = 0;
        float tx = 0, ty = 0;
        float f = 1, dxf = 0;
        int yyy = 1;
        float fx = 0, fy = 0;
        int tempX = 0;
        int tempY = 0;
        if ((anchor & VCENTER) == VCENTER && (anchor & LEFT) == LEFT)
        {
            //x -= w / 2; //xoa giữa
            y -= h / 2;//xoa giữa
        }
        else if ((anchor & VCENTER) == VCENTER && (anchor & HCENTER) == HCENTER)
        {
            x -= w / 2; //xoa giữa
            y -= h / 2;//xoa giữa
        }
        matrixBackup = GUI.matrix;
        size = new Vector2(w, h);
        relativePosition = new Vector2(x, y);
        //UpdatePos(20);
        size = new Vector2(w, h);
        if ((anchor & VCENTER) == VCENTER && (anchor & LEFT) == LEFT)
        {
            UpdatePos(6);
           // pivot = new Vector2(0,0.5f);
           // x -= w / 2; 
        }
        else if ((anchor & VCENTER) == VCENTER && (anchor & HCENTER) == HCENTER)
        {
            UpdatePos(3);//3xoay ngay giua 20 xoay goc
        }

        GUIUtility.RotateAroundPivot(rotate2, pivot);
        
        Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy),
                                image.texture,
                             new Rect((float)(x0 + dx + dxf) / image.texture.width,
                                 ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
                 (float)nw / image.texture.width, (float)nh / image.texture.height), 0, 0, 0, 0);
        //if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 || transform == TRANS_MIRROR_ROT90)
        //{
            GUI.matrix = matrixBackup;
        //}
    }
    public void setClip2(int x,int y,int w,int h)
    {
        GUI.BeginGroup(new Rect(x,y,w,h) );
    }
    public void enClip()
    {
        GUI.EndGroup();
    }
    public void drawRegionGui2(Image image, float x0, float y0, float w, float h, int transform, float x, float y, int anchor)
    {
        Rect a = new Rect(x, y, w,h);//vi tri ve len man hinh: do lon w h
        Rect b = new Rect(0,0,(float)(w/image.texture.width),1);
        GUI.DrawTextureWithTexCoords(a,image.texture,b);
       // Debug.Log((w / image.texture.width)+".");
    }
    public void drawRegionT(Texture2D imageTexture, float x0, float y0, int w, int h,int transform, float x, float y,float wScale,float hScale, int anchor)
 {
  if (transform == 0) {
      GUI.DrawTextureWithTexCoords (new Rect (x0, y0, w, h), imageTexture, new Rect (x / imageTexture.width, 
                                                                            y / imageTexture.height, wScale / imageTexture.width,
                                                                            hScale / imageTexture.height));
    }
  if (transform == 2) {
   GUI.DrawTextureWithTexCoords (new Rect (x0, y0, w, h), imageTexture, new Rect ((x + wScale)/ imageTexture.width, 
                                                                                  y / imageTexture.height, -wScale / imageTexture.width,
                                                                                  hScale / imageTexture.height));
    }
 }
    public Color clTrans;
    public void drawRegionGui(Image image, float x0, float y0, int w, int h, int transform, float x, float y, int anchor)
    {
        GUI.color = setColorMiniMap(0x0c5414);

        x *= zoomLevel;
        y *= zoomLevel;
        x0 *= zoomLevel;
        y0 *= zoomLevel;
        w *= zoomLevel;
        h *= zoomLevel;
        //if (isTranslate)
        //{
        //    x += translateX;
        //    y += translateY;
        //}

        //float nw = w;
        //float nh = h;
        //float dx = 0;
        //float dy = 0;
        //float tx = 0, ty = 0;
        //float f = 1, dxf = 0;
        //if ((anchor & HCENTER) == HCENTER)
        //    tx -= nw / 2;
        //if ((anchor & VCENTER) == VCENTER)
        //    ty -= nh / 2;
        //if ((anchor & RIGHT) == RIGHT)
        //    tx -= nw;
        //if ((anchor & BOTTOM) == BOTTOM)
        //    ty -= nh;
        //x += tx;
        //y += ty;
        //float cx = 0, cy = 0, cw = 0, ch = 0;
        //if (isClip)
        //{
        //    cx = clipX; cy = clipY; cw = clipW; ch = clipH;
        //    if (isTranslate)
        //    {
        //        cx += clipTX;
        //        cy += clipTY;
        //    }
        //    Rect r1 = new Rect(x, y, w, h);
        //    Rect r2 = new Rect(cx, cy, cw, ch);
        //    Rect r3 = intersectRect(r1, r2);
        //    if (r3.width <= 0 || r3.height <= 0) return;

        //    //drawRectDisableClip((int)r3.x-1,(int)r3.y-1,(int)r3.width+2,(int)r3.height+2);
        //    nw = r3.width;
        //    nh = r3.height;

        //    dx = r3.x - r1.x;
        //    dy = r3.y - r1.y;
        //}
        //float fx = 0, wx0 = 0;
        //if (transform == 2)
        //{
        //    fx += nw;
        //    wx0 = x0 * 2;
        //    f = -1;
        //    if (isClip)
        //    {
        //        if (cx > x)
        //            dxf = -dx;
        //        else if (cx + cw < x + w)
        //            dxf = -(cx + cw - x - w);
        //    }
        //}
     
        //GUI.BeginGroup(new Rect(x + dx, y + dy, nw, nh));
        //GUI.DrawTexture(new Rect(-x0 + wx0 + dxf + fx, -y0 - dy, image.texture.width * f, image.texture.height), image.texture, ScaleMode.StretchToFill);
        //GUI.EndGroup();
        
        //GUI.color = new Color(1, 1, 1, 1);


        
    }
    public void drawRegion2(Image image, float x0, float y0, int w, int h, int transform, int x, int y, int anchor){

        GUI.color = image.colorBlend;
        if (isTranslate)
        {
            x += translateX;
            y += translateY;
        }
        Texture2D rgb_texture;
        string key = "dg" + x0 + y0 + w + h + transform + image.GetHashCode();
        rgb_texture = (Texture2D)cachedTextures[key];

        if (rgb_texture == null)
        {
            Image imgRegion = Image.createImage(image, (int)x0, (int)y0, w, h, transform);
            rgb_texture = imgRegion.texture;
            cache(key, rgb_texture);
        }

        //=============================

        int cx = 0, cy = 0, cw = 0, ch = 0;

        float nw = w;
        float nh = h;
        float tx = 0, ty = 0;

        if ((anchor & HCENTER) == HCENTER)
            tx -= nw / 2;
        if ((anchor & VCENTER) == VCENTER)
            ty -= nh / 2;
        if ((anchor & RIGHT) == RIGHT)
            tx -= nw;
        if ((anchor & BOTTOM) == BOTTOM)
            ty -= nh;

        x += (int)tx;
        y += (int)ty;

        if (isClip)
        {
            cx = clipX; cy = clipY; cw = clipW; ch = clipH;
            if (isTranslate)
            {
                cx += clipTX;
                cy += clipTY;
            }
        }
        if (isClip)
            GUI.BeginGroup(new Rect(cx, cy, cw, ch));

        GUI.DrawTexture(new Rect(x - cx, y - cy, w, h), rgb_texture);

        if (isClip)
            GUI.EndGroup();
        GUI.color = new Color(1, 1, 1, 1);
        //=============================
    }
    public void drawImagaByDrawTexture(Image image, float x, float y)
    {
        x *= zoomLevel;
        y *= zoomLevel;

        GUI.DrawTexture(new Rect(x + translateX, y + translateY, image.getRealImageWidth(), image.getRealImageHeight()),image.texture);
    }
    public void drawImage(mBitmap image, int x, int y, int anchor)
    {
		if(image==null)
			return;
        drawRegion(image,0,0,getImageWidth(image),getImageHeight(image),0,x,y,anchor);
    }
    public void drawImage(mBitmap image, int x, int y, int anchor,bool isUClip)
    {
        if (image == null)
            return;
        drawRegion(image, 0, 0, getImageWidth(image), getImageHeight(image), 0, x, y, anchor);
    }
    public void drawImage(mBitmap image, int x, int y)
    {
        if (image == null)
            return;
        drawRegion(image, 0, 0, getImageWidth(image), getImageHeight(image), 0, x, y, TOP | LEFT);
    }
    public void drawRoundRect(int x, int y, int w, int h, int arcWidth, int arcHeight){
		drawRect(x,y,w,h);
    }
    public void fillRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight){
		fillRect(x,y,width,height);		
    }
    public void fillRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight,bool isUclip)
    {
        fillRect(x, y, width, height);
    }
	public void reset(){		
		isClip=false;			
		isTranslate=false;
		translateX=0;
		translateY=0;
	}
	public Rect intersectRect(Rect r1, Rect r2){
        float tx1 = r1.x;
        float ty1 = r1.y;
        float rx1 = r2.x;
        float ry1 = r2.y;
        float tx2 = tx1; tx2 += r1.width;
        float ty2 = ty1; ty2 += r1.height;
        float rx2 = rx1; rx2 += r2.width;
        float ry2 = ry1; ry2 += r2.height;
        if (tx1 < rx1) tx1 = rx1;
        if (ty1 < ry1) ty1 = ry1;
        if (tx2 > rx2) tx2 = rx2;
        if (ty2 > ry2) ty2 = ry2;
        tx2 -= tx1;
        ty2 -= ty1;
        if (tx2 < -30000) tx2 = -30000;
        if (ty2 < -30000) ty2 = -30000;
        return new Rect(tx1, ty1, (int)tx2, (int)ty2);        
    }

    public void drawImageScale(Image image, int x, int y, int w, int h,int tranform){
        GUI.color = Color.red;
        x *= zoomLevel;
        y *= zoomLevel;
        w *= zoomLevel;
        h *= zoomLevel;
        if (image != null)
       	Graphics.DrawTexture(new Rect(x+translateX, y+translateY, (tranform==0?w:-w), h), image.texture);		
    }
    public void drawImageSimple(Image image, int x, int y){
        x *= zoomLevel;
        y *= zoomLevel;
        if(image!=null)
        Graphics.DrawTexture(new Rect(x, y, image.w, image.h), image.texture);
    }
    public static int getImageWidth(mBitmap image)
    {
        if (image == null) return 1;
        return getImageWidth(image.image);
    }
    public static int getImageHeight(mBitmap image)
    {
        if (image == null) return 1;
        return getImageHeight(image.image);
    }
    public static int getImageWidth(Image image)
    {
        if (image == null) return 1;
        return image.getWidth();
    }
    public static int getImageHeight(Image image)
    {
        if (image == null) return 1;
        return image.getHeight();
    }
    public void drawRegionRect(Image arg0, int x0, int y0, int w0, int h0, int arg5, int x, int y, int arg8, int wRect, int hRect, int body, bool isOpacity)
    {
        //x *= zoomLevel;
        //y *= zoomLevel;
        //x0 *= zoomLevel;
        //y0 *= zoomLevel;
        //w0 *= zoomLevel;
        //h0 *= zoomLevel;
        y -= (body == 1 ? (zoomLevel - 1) : 0);
       // _drawRegion(arg0, x0, y0, w0, h0, arg5, x, y, arg8);
        if(isOpacity)
            _drawRegionRectOpacity(arg0, x0, y0, w0, h0, arg5, x, y, arg8,  wRect, hRect,true);
        else
            _drawRegionRect(arg0, x0, y0, w0, h0, arg5, x, y, arg8, wRect, hRect);
    }
    private void _drawRegionRectOpacity(Image image, float x0, float y0, int w, int h, int transform, int x, int y, int anchor, int wRect, int hRect, bool p)
    {

        if (image == null) return;
        if (isTranslate)
        {
            x += translateX;
            y += translateY;
        }
        Texture2D rgb_texture;
        string key = "dg" + x0 + y0 + w + h + transform + image.GetHashCode();
        rgb_texture = (Texture2D)cachedTextures[key];

        if (rgb_texture == null)
        {
            Image imgRegion = Image.createImage(image, (int)x0, (int)y0, w, h, transform);
            rgb_texture = imgRegion.texture;
            imgRegion = null;
            cache(key, rgb_texture);
        }

        //=============================

        int cx = 0, cy = 0, cw = 0, ch = 0;

        float nw = w;
        float nh = h;
        float tx = 0, ty = 0;

        if ((anchor & HCENTER) == HCENTER)
            tx -= nw / 2;
        if ((anchor & VCENTER) == VCENTER)
            ty -= nh / 2;
        if ((anchor & RIGHT) == RIGHT)
            tx -= nw;
        if ((anchor & BOTTOM) == BOTTOM)
            ty -= nh;

        x += (int)tx;
        y += (int)ty;

        if (isClip)
        {
            cx = clipX; cy = clipY; cw = clipW; ch = clipH;
            if (isTranslate)
            {
                cx += clipTX;
                cy += clipTY;
            }
        }
        if (isClip)
            GUI.BeginGroup(new Rect(cx, cy, cw, ch));
        GUI.color = new Color32(255, 255, 255, 100);
        GUI.DrawTexture(new Rect(x - cx, y - cy, w, h-hRect), rgb_texture);
        rgb_texture = null;
        GUI.color = new Color32(255, 255, 255, 255);
        if (isClip)
            GUI.EndGroup();
    }
    private void _drawRegionRectOpacity(Image image, float x0, float y0, int w, int h, int transform, int x, int y, int anchor, int wRect, int hRect,bool isUclip,int opacity)
    {

        if (image == null) return;
        if (isTranslate)
        {
            x += translateX;
            y += translateY;
        }
        float nw = w;
        float nh = h;
        float dx = 0;
        float dy = 0;
        float tx = 0, ty = 0;
        float f = 1, dxf = 0;
        int yyy = 1;
        if ((anchor & HCENTER) == HCENTER)
        {
            tx -= nw / 2;
        }
        if ((anchor & VCENTER) == VCENTER)
        {
            ty -= nh / 2;
        }
        if ((anchor & RIGHT) == RIGHT)
        {
            tx -= nw;
        }
        if ((anchor & BOTTOM) == BOTTOM)
        {
            ty -= nh;
        }
        if ((anchor & TOP) == TOP && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
        {
            tx = nh / 2 - nw / 2 - 1;
            ty = nw / 2 - nh / 2;
        }
        if ((anchor & BOTTOM) == BOTTOM && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
        {
            ty = -nw / 2 - nh / 2;
            tx = nh / 2 - nw / 2 + -1;
        }
        if ((anchor & TOP) == TOP && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
        {
            tx = (nh / 2 - nw / 2 - nh);
            ty = nw / 2 - nh / 2;
        }
        if ((anchor & BOTTOM) == BOTTOM && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
        {
            ty = -nw / 2 - nh / 2;
            tx = (nh / 2 - nw / 2 - nh);
        }
        x += (int)tx;
        y += (int)ty;
        int cx = 0, cy = 0, cw = 0, ch = 0;
        if (isClip)
        {
            cx = clipX; cy = clipY; cw = clipW; ch = clipH;
            if (isTranslate)
            {
                cx += clipTX;
                cy += clipTY;
            }
            Rect r1 = new Rect(x, y, w, h);
            Rect r2 = new Rect(cx, cy, cw, ch);
            Rect r3 = intersectRect(r1, r2);
            if (r3.width <= 0 || r3.height <= 0) return;
            nw = r3.width;
            nh = r3.height;
            dx = r3.x - r1.x;
            dy = r3.y - r1.y;
        }
        float fx = 0, fy = 0;
        if (transform == 2)
        {
            fx += nw;
            f = -1;
            if (isClip)
            {
                if (cx > x)
                    dxf = -dx;
                else if (cx + cw < x + w)
                    dxf = -(cx + cw - x - w);
            }
        }
        else if (transform == TRANS_MIRROR_ROT180)
        {
            yyy = -1;
            fy += nh;
        }
        else if (transform == TRANS_ROT180)
        {
            yyy = -1;
            fy += nh;
            f = -1;
            fx += nw;
        }
        int tempX = 0;
        int tempY = 0;
        if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 || transform == TRANS_MIRROR_ROT90) // rotate 90, 180, 270 degree
        {
            matrixBackup = GUI.matrix;
            size = new Vector2(w, h);
            relativePosition = new Vector2(x, y);
            UpdatePos(3);
            if (transform == TRANS_ROT270)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(0, pivot);
            }
            else if (transform == TRANS_ROT90)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(90, pivot);
            }
            if (transform == TRANS_ROT270)
            {
                GUIUtility.RotateAroundPivot(270, pivot);
            }
            else if (transform == TRANS_MIRROR_ROT270)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(90, pivot);
                yyy = -1;
                fy += nh;
            }
            else if (transform == TRANS_MIRROR_ROT90)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(270, pivot);
                yyy = -1;
                fy += nh;
            }
        }
        //      GUI.DrawTextureWithTexCoords(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy), image.texture,
        //                           new Rect((float)(x0 + dx + dxf) / image.texture.width, ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
        //               (float)nw / image.texture.width, (float)nh / image.texture.height));
        //image.texture.alphaIsTransparency = true;
        //image.texture.anisoLevel = 10;
        Color32 colordem = GUI.color;
        GUI.color = new Color32(255, 255, 255, (byte)opacity);
        // GUI.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy), image.texture);
        // GUI.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy), image.texture);
        Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy - hRect),
                                image.texture,
                             new Rect((float)(x0 + dx + dxf) / image.texture.width,
                                 ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
                 (float)nw / image.texture.width, (float)nh / image.texture.height), 0, 0, 0, 0, GUI.color);
        //Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy),
        //                        image.texture,
        //                     new Rect((float)(x0 + dx + dxf) / image.texture.width,
        //                         ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
        //         (float)nw / image.texture.width, (float)nh / image.texture.height), 0, 0, 0, 0);

        GUI.color = colordem;
        //Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy - hRect),
        //                        image.texture,
        //                     new Rect((float)(x0 + dx + dxf) / image.texture.width,
        //                         ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
        //         (float)nw / image.texture.width, (float)nh / image.texture.height), 0, 0, 0, 0);
        if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 || transform == TRANS_MIRROR_ROT90)
        {
            GUI.matrix = matrixBackup;
        }
    }
    private void _drawRegionRect(Image image, float x0, float y0, int w, int h, int transform, int x, int y, int anchor, int wRect, int hRect)
    {

        if (image == null) return;
        if (isTranslate)
        {
            x += translateX;
            y += translateY;
        }
        float nw = w;
        float nh = h;
        float dx = 0;
        float dy = 0;
        float tx = 0, ty = 0;
        float f = 1, dxf = 0;
        int yyy = 1;
        if ((anchor & HCENTER) == HCENTER)
        {
            tx -= nw / 2;
        }
        if ((anchor & VCENTER) == VCENTER)
        {
            ty -= nh / 2;
        }
        if ((anchor & RIGHT) == RIGHT)
        {
            tx -= nw;
        }
        if ((anchor & BOTTOM) == BOTTOM)
        {
            ty -= nh;
        }
        if ((anchor & TOP) == TOP && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
        {
            tx = nh / 2 - nw / 2 - 1;
            ty = nw / 2 - nh / 2;
        }
        if ((anchor & BOTTOM) == BOTTOM && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
        {
            ty = -nw / 2 - nh / 2;
            tx = nh / 2 - nw / 2 + -1;
        }
        if ((anchor & TOP) == TOP && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
        {
            tx = (nh / 2 - nw / 2 - nh);
            ty = nw / 2 - nh / 2;
        }
        if ((anchor & BOTTOM) == BOTTOM && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
        {
            ty = -nw / 2 - nh / 2;
            tx = (nh / 2 - nw / 2 - nh);
        }
        x += (int)tx;
        y += (int)ty;
        int cx = 0, cy = 0, cw = 0, ch = 0;
        if (isClip)
        {
            cx = clipX; cy = clipY; cw = clipW; ch = clipH;
            if (isTranslate)
            {
                cx += clipTX;
                cy += clipTY;
            }
            Rect r1 = new Rect(x, y, w, h);
            Rect r2 = new Rect(cx, cy, cw, ch);
            Rect r3 = intersectRect(r1, r2);
            if (r3.width <= 0 || r3.height <= 0) return;
            nw = r3.width;
            nh = r3.height;
            dx = r3.x - r1.x;
            dy = r3.y - r1.y;
        }
        float fx = 0, fy = 0;
        if (transform == 2)
        {
            fx += nw;
            f = -1;
            if (isClip)
            {
                if (cx > x)
                    dxf = -dx;
                else if (cx + cw < x + w)
                    dxf = -(cx + cw - x - w);
            }
        }
        else if (transform == TRANS_MIRROR_ROT180)
        {
            yyy = -1;
            fy += nh;
        }
        else if (transform == TRANS_ROT180)
        {
            yyy = -1;
            fy += nh;
            f = -1;
            fx += nw;
        }
        int tempX = 0;
        int tempY = 0;
        if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 || transform == TRANS_MIRROR_ROT90) // rotate 90, 180, 270 degree
        {
            matrixBackup = GUI.matrix;
            size = new Vector2(w, h);
            relativePosition = new Vector2(x, y);
            UpdatePos(3);
            if (transform == TRANS_ROT270)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(0, pivot);
            }
            else if (transform == TRANS_ROT90)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(90, pivot);
            }
            if (transform == TRANS_ROT270)
            {
                GUIUtility.RotateAroundPivot(270, pivot);
            }
            else if (transform == TRANS_MIRROR_ROT270)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(90, pivot);
                yyy = -1;
                fy += nh;
            }
            else if (transform == TRANS_MIRROR_ROT90)
            {
                size = new Vector2(w, h - 1);
                UpdatePos(3);
                GUIUtility.RotateAroundPivot(270, pivot);
                yyy = -1;
                fy += nh;
            }
        }
        //      GUI.DrawTextureWithTexCoords(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy), image.texture,
        //                           new Rect((float)(x0 + dx + dxf) / image.texture.width, ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
        //               (float)nw / image.texture.width, (float)nh / image.texture.height));
        //image.texture.alphaIsTransparency = true;
        //image.texture.anisoLevel = 10;
        Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy-hRect),
                                image.texture,
                             new Rect((float)(x0 + dx + dxf) / image.texture.width,
                                 ((float)(image.texture.height - nh - (y0 + dy))) / image.texture.height,
                 (float)nw / image.texture.width, (float)nh / image.texture.height), 0, 0, 0, 0);
        if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 || transform == TRANS_MIRROR_ROT90)
        {
            GUI.matrix = matrixBackup;
        }   
    }
    public void drawRegionOpacity(Image arg0, int x0, int y0, int w0, int h0,
            int arg5, int x, int y, int arg8, int opacity, bool isUseSetClip)
    {
        //x *= zoomLevel;
        //y *= zoomLevel;
        //x0 *= zoomLevel;
        //y0 *= zoomLevel;
        //w0 *= zoomLevel;
        //h0 *= zoomLevel;
        //a = opacity / 100;
        _drawRegionOpacity2(arg0, x0, y0, w0, h0, arg5, x, y, arg8, opacity);
        //disableBlending();
    }
    public void drawRegionOpacity(Image arg0, int x0, int y0, int w0, int h0,
            int arg5, int x, int y, int arg8, int opacity)
    {
        //x *= zoomLevel;
        //y *= zoomLevel;
        //x0 *= zoomLevel;
        //y0 *= zoomLevel;
        //w0 *= zoomLevel;
        //h0 *= zoomLevel;
        //a = opacity/100;

        _drawRegionOpacity2(arg0, x0, y0, w0, h0, arg5, x, y, arg8, opacity);
        //_drawRegionOpacity(arg0, x0, y0, w0, h0, arg5, x, y, arg8, opacity);
        //disableBlending();
        
    }
    public void drawRegionRotateOpacity(mBitmap arg0, int x0, int y0, int w0, int h0,
            int arg5, int x, int y, int arg8, int rotate, int opacity)
    {
        x *= zoomLevel;
        y *= zoomLevel;
        x0 *= zoomLevel;
        y0 *= zoomLevel;
        w0 *= zoomLevel;
        h0 *= zoomLevel;
        a = opacity / 100;

        _drawRegionRotateOpacity2(arg0, x0, y0, w0, h0, arg5, x, y, arg8,rotate, opacity);
        //_drawRegionOpacity(arg0, x0, y0, w0, h0, arg5, x, y, arg8, opacity);
        //disableBlending();

    }
    public void drawRegionScale(Image arg0, int x0, int y0, int w0, int h0,
            int arg5, int x, int y, int arg8, int tileScale,bool Uclip)
    {
        //x *= zoomLevel;
        //y *= zoomLevel;
        //x0 *= zoomLevel;
        //y0 *= zoomLevel;
        //w0 *= zoomLevel;
        //h0 *= zoomLevel;
        //a = opacity/100;

        _drawRegionScale(arg0, x0, y0, w0, h0, arg5, x, y, arg8, tileScale);
        //_drawRegionOpacity(arg0, x0, y0, w0, h0, arg5, x, y, arg8, opacity);
        //disableBlending();

    }
    public static Color transParentColor = new Color(1,1,1,0);
    public static bool isNotTranColor(Color color)
    {
        if (color == Color.clear || (color==transParentColor))
            return false;
        return true;
    }
    public static Image blend(Image img0, float level, int rgb)
    {
       
        //Image temp = Image.createImage(img0);
        //int blue = rgb & 0xFF;
        //int green = (rgb >> 8) & 0xFF;
        //int red = (rgb >> 16) & 0xFF;
        //float b = (float)blue / 256;
        //float g = (float)green / 256;
        //float r = (float)red / 256;      
        //Color cl = new Color(r, g, b);

        //temp.colorBlend = cl;
        //return temp;

        int blue = rgb & 0xFF;
        int green = (rgb >> 8) & 0xFF;
        int red = (rgb >> 16) & 0xFF;
        float bb = (float)blue / 256;
        float gg = (float)green / 256;
        float rr = (float)red / 256;
        Color color = new Color(rr, gg, bb);
        Color[] rgbdata = img0.texture.GetPixels();
        float R = (float)color.r;
        float B = (float)color.g;
        float G = (float)color.b;

        for (int i = 0; i < rgbdata.Length; i++)
        {
            Color pixel = rgbdata[i];
            if (isNotTranColor(pixel))
            {
                float r = (R - pixel.r) * level + pixel.r;
                float g = (B - pixel.g) * level + pixel.g;
                float b = (G - pixel.b) * level + pixel.b;
                if (r > 255)
                    r = 255;
                if (r < 0)
                    r = 0;
                if (g > 255)
                    g = 255;
                if (g < 0)
                    g = 0;
                if (b < 0)
                    b = 0;
                if (b > 255)
                    b = 255;
                rgbdata[i].r = r;
                rgbdata[i].g = g;
                rgbdata[i].b = b;
            }
        }
        Image ii = Image.createImage(img0.getRealImageWidth(), img0.getRealImageHeight());       
        ii.texture.SetPixels(rgbdata);
        Image.setTextureQuality(ii.texture);
        ii.texture.Apply();

        Cout.LogError2("BLEND ----------------------------------------------------");
        return ii;
    }
	public static int getIntByColor(Color cl){
		float r = (float)cl.r * 255;
		float b = (float)cl.b * 255;
		float g = (float)cl.g * 255;
		return (((int)r & 0x0ff) << 16) | (((int)g & 0x0ff) << 8) | ((int)b & 0x0ff);
	}

    public void fillTriangle(int x1,int x2,int x3,int x4,int x5,int x6) { }

    public void drawRegionScalse(mBitmap arg0, int x0, int y0, int w0, int h0, int arg5, int x, int y, int arg8,bool isUclip, float tileScale)
    {

        x *= zoomLevel;
        y *= zoomLevel;
        x0 *= zoomLevel;
        y0 *= zoomLevel;
        w0 *= zoomLevel;
        h0 *= zoomLevel;
        _drawRegion(arg0.image, x0, y0, w0, h0, arg5, x, y, arg8);
    }

    public void drawRegion(mBitmap img, int x0, int y0, int w0, int h0, int arg5, int x, int y, int anchor, bool isUclip, int opacity)
    {

        _drawRegionOpacity2(img.image, x0, y0, w0, h0, arg5, x, y, anchor, opacity);
    }
}


