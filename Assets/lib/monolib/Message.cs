using System;
using UnityEngine;
using System.IO;
using System.Net.Sockets;

public class Message
{
	
	public sbyte command;
	
	private myReader dis = null; 
	private myWriter dos = null; 
	
    public Message(int command) {
        this.command = (sbyte) command;
		dos = new myWriter();
    }
    public Message() {
		dos = new myWriter();
    }

    public Message(sbyte command) {
        this.command = (sbyte)command;
		dos = new myWriter();
    }

    public Message(sbyte command, sbyte[] data) {
        this.command = (sbyte)command;
		dis = new myReader(data);
    }

    public sbyte[] getData() {
		return dos.getData();
    }

    public myReader reader() {
        return dis;
    }

    public myWriter writer() {
        return dos;
    }

    public void cleanup() {
//        try {
//            if (dis != null)
//                dis.Close();
//			dis = null;
//            if (dos != null)
//                dos.Close();
//			dos = null;
//        } catch (IOException) {
//        }
    }
	
}


