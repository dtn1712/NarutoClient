﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

   public class mLine{
       public int x1, x2, y1, y2;
       public float r, b, g, a;
       public mLine(int x1, int y1, int x2, int y2, float r, float b, float g, float a)
       {
           this.x1 = x1;
           this.y1 = y1;
           this.x2 = x2;
           this.y2 = y2;
           this.r = r;
           this.b = b;
           this.g = g;
           this.a = a;
       }

       public mLine(int x1, int y1, int x2, int y2, int cl)
       {
           this.x1 = x1;
           this.y1 = y1;
           this.x2 = x2;
           this.y2 = y2;
           setColor(cl);
       }

       public void setColor(int rgb)
       {
           int blue = rgb & 0xFF;
           int green = (rgb >> 8) & 0xFF;
           int red = (rgb >> 16) & 0xFF;
           b = (float)blue / 256;
           g = (float)green / 256;
           r = (float)red / 256;
           a = 255f;
       }
   }

