using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Threading;
public class Image 
{
	const int INTERVAL=5;
	const int MAXTIME=500;
	
	public Texture2D texture = new Texture2D(1, 1);
	public static Image imgTemp;
	public static string filenametemp;
	public static byte[] datatemp;
	public static Image imgSrcTemp;
	public static int xtemp,ytemp,wtemp,htemp,transformtemp;
	public int w,h,width,height;
	public static int status;
    public Color colorBlend = Color.black;
    public static mHashtable listImgLoad = new mHashtable();
	// 0:free 
	// 1:working 
	// 2:waitingCreateEmptyImage 
	// 3:waitingCreateImageFromFileName
	// 4:waitingCreateImageFromArray
	// 5:waitingCreateImageFromPartImage

	public static Image createEmptyImage()
	{
        return __createEmptyImage();
		if(Thread.CurrentThread.Name==Main.mainThreadName)
			return __createEmptyImage();
		else
			return _createEmptyImage();
	}
	public static Image createImageOLD(string filename)
	{
		//return _createImageBySprite (filename);
		        return __createImage(filename);
				if(Thread.CurrentThread.Name==Main.mainThreadName)
					return __createImage(filename);
				else
					return _createImage(filename);
	}
	public static Image createImage(string filename)
	{
        
		return _createImageByPNG (filename);
//        return __createImage(filename);
//		if(Thread.CurrentThread.Name==Main.mainThreadName)
//			return __createImage(filename);
//		else
//			return _createImage(filename);
	}
	//----Sprite
	public static Image createImageSprite(string filename)
	{
		return _createImageByPNG(filename);
	}

	public static Image _createImageByPNG(string path){
        try
        {
            Texture2D spr = Resources.Load(path) as Texture2D;
            if (spr == null)
                throw new System.Exception("NULL POINTER EXCEPTION AT Image _createImageByPNG " + path);
            Image img = new Image();

            img.texture = spr;
            img.w = spr.width;
            img.h = spr.height;
            return img;
        }
        catch (Exception e)
        {
            return null;
        }
        //try
        //{
        //    Image img = new Image();
        //    //string texture = "Assets/Resources/"+path;
        //    //Texture2D inputTexture = Resources.Load(texture, typeof(Texture2D)) as Texture2D;
        //    TextAsset imageasset = (TextAsset)Resources.Load(path, typeof(TextAsset));
        //    if (imageasset == null || imageasset.bytes == null || imageasset.bytes.Length == 0)//
        //    {
        //        Cout.println(imageasset + "  TRT  2222----Create Image=:  " + path + " fail");
        //        Cout.println(imageasset.bytes + "  TRT  333----Create Image=:  " + path + " fail");
        //        Cout.println(imageasset.bytes.Length + "  TRT 444----Create Image=:  " + path + " fail");
        //        //return null;
        //        //  Cout.LogError("TRT----Create Image=:  "+filename+" fail");
        //        throw new System.Exception("NULL POINTER EXCEPTION AT Image __createImage");
        //        //  Cout.LogError("22222 TRT----Create Image=:  " + filename + " fail");
        //    }
        //   // img.texture = inputTexture;
        //    img.texture.LoadImage(imageasset.bytes);
        //    img.w = img.texture.width;
        //    img.h = img.texture.height;
        //    setTextureQuality(img);

        //    // Out.println("load img thanh cong >> "+filename);
        //    return img;
        //}
        //catch (Exception e)
        //{
        //    return null;
        //}
	}

	//----Sprite-------
	public static Image createImage (byte[] imageData)
	{
        return __createImage(imageData);
		if(Thread.CurrentThread.Name==Main.mainThreadName)
			return __createImage(imageData);
		else
			return _createImage(imageData);
	}

	public static Image createImage(Image src, int x,int y,int w,int h,int transform)
	{
        return __createImage(src, x, y, w, h, transform);
		if(Thread.CurrentThread.Name==Main.mainThreadName)
			return __createImage(src,x,y,w,h,transform);
		else
			return _createImage(src,x,y,w,h,transform);
	}
    public static Image createImage(int w, int h)
    {
        return __createImage(w, h);
        if (Thread.CurrentThread.Name == Main.mainThreadName)
            return __createImage(w, h);
        else
            return _createImage( w, h);
    }
    public static Image createImage(Image img)
    {
        Image temp = Image.createImage(img.w, img.h);
        temp.texture = img.texture;
        temp.texture.Apply();
        return temp;
    }
	 public static Image createImage(sbyte[] imageData, int offset, int lenght)
    {
         //if(imageData==null)
          //   throw new System.Exception("NULL POINTER ");
        if (offset + lenght > imageData.Length)
            return null;
        byte[] temp = new byte[lenght];

        for (int i = 0; i < lenght; i++)
            temp[i] = convertSbyteToByte(imageData[i + offset]);

        return createImage(temp);

    }
	public static byte convertSbyteToByte(sbyte var)
    {
        if (var > 0)
            return (byte)var;

        return (byte)(var + 256);
    }
	 public static byte[] convertArrSbyteToArrByte(sbyte[] var)
    {
        byte[] temp = new byte[var.Length];
        for (int i = 0; i < var.Length; i++)
        {
            if (var[i] > 0)
                temp[i] = (byte)var[i];

            else temp[i] = (byte)(var[i] + 256);
        }

        return temp;
    }
	public static Image createRGBImage(int []rbg,int w,int h,bool bl){
		Image temp=Image.createImage(w,h);		
		Color []t=new Color[rbg.Length];
     //   temp.texture= new Texture2D (w, h, TextureFormat.RGBA32, false);
		for(int i=0;i<t.Length;i++)
			t[i]=setColorFromRBG(rbg[i]);		
		temp.texture.SetPixels(0,0,w,h,t);
		temp.texture.Apply();
		return temp;
	}
	 public static Color setColorFromRBG(int rgb)
    {   int blue = rgb & 0xFF;
        int green = (rgb >> 8) & 0xFF;
        int red = (rgb >> 16) & 0xFF;
        float b = (float)blue / 256;
        float g = (float)green / 256;
        float r = (float)red / 256;       
        Color cl = new Color(r,g,b);
        return cl;
    }
	public static void update()
	{
		if(status==2)
		{
			status=1;
			imgTemp=__createEmptyImage();
			status=0;
		}
		else if(status==3)
		{
			status=1;
			imgTemp=__createImage(filenametemp);
			
			status=0;
		}
		else if(status==4)
		{
			status=1;
			imgTemp=__createImage(datatemp);
			status=0;
		}
		else if(status==5)
		{
			status=1;
			imgTemp=__createImage(imgSrcTemp,xtemp,ytemp,wtemp,htemp,transformtemp);
			status=0;
		}
        else if (status == 6)
        {
            status = 1;
            imgTemp = __createImage(wtemp, htemp);
            status = 0;
        }
	}
	private static Image _createEmptyImage()
	{
		if(status!=0)
		{
			Cout.LogError("CANNOT CREATE EMPTY IMAGE WHEN CREATING OTHER IMAGE");
			return null;
		}
		imgTemp=null;
		status=2;
		int i=0;
		while(i<MAXTIME)
		{
			Thread.Sleep(INTERVAL);
			if(status==0) break; // create done
			i++;
		}
		if(i==MAXTIME)
		{
		    Cout.LogError("TOO LONG FOR CREATE EMPTY IMAGE");
            status = 0;
		}
		//else Debug.Log("Create Empty Image done in "+(i*INTERVAL)+"ms");
		return imgTemp;
	}
	private static Image _createImage(string filename)
	{
		if(status!=0)
		{
			Cout.LogError("CANNOT CREATE IMAGE "+filename+" WHEN CREATING OTHER IMAGE");
			return null;
		}
		imgTemp=null;
		filenametemp=filename;
		status=3;
		int i=0;
		while(i<MAXTIME)
		{
			Thread.Sleep(INTERVAL);
			if(status==0) break; // create done
			i++;
		}
		if(i==MAXTIME)
		{
		    Cout.LogError("TOO LONG FOR CREATE IMAGE "+filename);
            status = 0;
		}
		//else Debug.Log("Create Image "+ filename+" done in "+(i*INTERVAL)+"ms");
		return imgTemp;
	}
	
	private static Image _createImage (byte[] imageData)
	{
		if(status!=0)
		{
			Cout.LogError("CANNOT CREATE IMAGE(FromArray) WHEN CREATING OTHER IMAGE");
			return null;
		}
		imgTemp=null;
		datatemp=imageData;
		status=4;
		int i=0;
		while(i<MAXTIME)
		{
			Thread.Sleep(INTERVAL);
			if(status==0) break; // create done
			i++;
		}
		if(i==MAXTIME)
		{
		    Cout.LogError("TOO LONG FOR CREATE IMAGE(FromArray)");
            status = 0;
		}
		//else Debug.Log("Create Image(FromArray) done in "+(i*INTERVAL)+"ms");
		return imgTemp;
	}
	private static Image _createImage(Image src, int x,int y,int w,int h,int transform)
	{
        if(status!=0)
		{
			Cout.LogError("CANNOT CREATE IMAGE(FromSrcPart) WHEN CREATING OTHER IMAGE");
			return null;
		}
		imgTemp=null;
		imgSrcTemp=src;
		xtemp=x;
		ytemp=y;
		wtemp=w;
		htemp=h;
		transformtemp=transform;			
		status=5;
		int i=0;
		while(i<MAXTIME)
		{
			Thread.Sleep(INTERVAL);
			if(status==0) break; // create done
			i++;
		}
		if(i==MAXTIME)
		{
		    Cout.LogError("TOO LONG FOR CREATE IMAGE(FromSrcPart)");
            status = 0;
		}
		//else Debug.Log("Create Image(FromSrcPart) done in "+(i*INTERVAL)+"ms");
		return imgTemp;
	}
    private static Image _createImage(int w, int h)
    {
        if (status != 0)
        {
            Cout.LogError("CANNOT CREATE IMAGE(w,h) WHEN CREATING OTHER IMAGE");
            return null;
        }
        imgTemp = null;
        wtemp = w;
        htemp = h;
        status = 6;
        int i = 0;
        while (i < MAXTIME)
        {
            Thread.Sleep(INTERVAL);
            if (status == 0) break; // create done
            i++;
        }
        if (i == MAXTIME)
        {
            Cout.LogError("TOO LONG FOR CREATE IMAGE(w,h)");
            status = 0;
        }
        //else Debug.Log("Create Image(w,h) done in " + (i * INTERVAL) + "ms");
        return imgTemp;
    }
	
    //---
    public static byte []loadData(string filename)
    {
        Image img = new Image();
        TextAsset imageasset = (TextAsset)Resources.Load(filename, typeof(TextAsset));
        if (imageasset == null || imageasset.bytes == null || imageasset.bytes.Length == 0)
        {
            //  Cout.LogError("TRT----Create Image=:  "+filename+" fail");
            throw new System.Exception("NULL POINTER EXCEPTION AT Image __createImage "+filename);
            //  Cout.LogError("22222 TRT----Create Image=:  " + filename + " fail");
        }

      
        sbyte[] data2 = ArrayCast.cast(imageasset.bytes);
        Debug.LogError("CHIEU DAI MANG BYTE IMAGE CREAT = " + data2.Length);
        return imageasset.bytes;
      //  img.texture.LoadImage(imageasset.bytes);

    }
    //---
    private static Image __createImage(string path)
	{
        try
        {
            Texture2D spr = Resources.Load(path) as Texture2D;
            if (spr == null)
                throw new System.Exception("NULL POINTER EXCEPTION AT Image _createImageByPNG " + path);
            Image img = new Image();

            img.texture = spr;
            img.w = spr.width;
            img.h = spr.height;
            return img;
        }
        catch (Exception e)
        {
            return null;
        }
	}
	private static Image __createImage (byte[] imageData)
	{
        //Debug.Log("CREATE IMAGE FROM BYTE[]: size="+imageData.Length);
		if(imageData==null||imageData.Length==0)
		{
			Cout.LogError("Create Image from byte array fail");
			return null;
		}
        Image img = new Image();
        try
        {   img.texture.LoadImage(imageData);
            img.w = img.texture.width;
            img.h = img.texture.height;
			setTextureQuality(img);
        }catch(Exception e)
        {
            Cout.LogError("CREAT IMAGE FROM ARRAY FAIL \n"+Environment.StackTrace);
        }
	    return img;
	}
	private static Image __createImage(Image src, int x,int y,int w,int h,int transform)
	{

		Image img=new Image();
		img.texture=new Texture2D(w,h);	

		y=src.texture.height-y-h;
		for(int i=0;i<w;i++)
			for(int j=0;j<h;j++)
		{
			int ii=i;
			if(transform==2)ii=w-i;
			int jj=j;
            Color rgb = src.texture.GetPixel(x + ii, y + jj);
			img.texture.SetPixel(i,j,rgb);
		}
		img.texture.Apply();
		img.w=img.texture.width;
		img.h=img.texture.height;
		setTextureQuality(img);
		return img;	
	}
	private static Image __createEmptyImage()
	{
		return new Image();
	}

    public static Image __createImage(int w, int h)
    {
        //Image img = new Image();
        //img.texture = new Texture2D(w, h, TextureFormat.RGB24,false);
        //Color []c=img.texture.GetPixels();
        //for(int i=0;i<c.Length;i++){
        //    c[i]=Color.Lerp(c[i],new Color(1,1,1,0),1);
        //}
        //img.texture.SetPixels(c);
        //img.texture.Apply();
        //setTextureQuality(img);
        //img.w = w;
      //  img.h = h;      
        Image img = new Image();
        img.texture = new Texture2D(w, h, TextureFormat.RGBA32, false);     
        setTextureQuality(img);
        img.w = w;
        img.h = h;
        img.texture.Apply();
        return img;
    }
	 public static int getImageWidth(Image image)
    {
        return image.getWidth();
    }
    public static int getImageHeight(Image image)
    {
        return image.getHeight();
    }
	public int getWidth()
	{
        return (w == 0 ? 2 : w) / mGraphics.zoomLevel;
	}
	public int getHeight()
    {
        return (h == 0 ? 2 : h) / mGraphics.zoomLevel;
	}
    public static int getWidth(mBitmap img)
    {
        if (img == null) return 1;
        return img.getWidth();
    }
    public static int getHeight(mBitmap img)
    {
        if (img == null) return 1;
        return img.getHeight();
    }
	 private static void setTextureQuality(Image img)
    {
        setTextureQuality(img.texture);
    }
    public static void setTextureQuality(Texture2D texture)
    {
        texture.anisoLevel = 0;
        texture.filterMode = FilterMode.Point;
        texture.mipMapBias = 0;
        texture.wrapMode = TextureWrapMode.Clamp;	
		
    }
	public Color []getColor(){
		return texture.GetPixels();	
	}
	public int getRealImageWidth(){
		return w;
	}
	public int getRealImageHeight(){
		return h;
	}
	
	public void getRGB(int []data,int x1,int x2,int x3,int x4,int x5,int x6){
		Color []cl=texture.GetPixels();	
		
//		float red=(256*cl.r);
//		float green=256*cl.g;
//		float blue=256*cl.b;
//		float []arr=new float[3];
//		arr[0]=red;
//		arr[1]=green;
//		arr[2]=blue;
//		data=(int)arr;
	}

    public static bool CheckTonTaiAnhDuoiReSource(string path)
    {
        try
        {
            Texture2D spr = Resources.Load(path) as Texture2D;
            if (spr == null)
            {
                return false;
            }
            spr = null;
            return true;
        }
        catch (Exception)
        {

            return false;
        }
        return false;
    }

    public static Image loadImageFromAsset(string url)
    {
        return createImage(url);
    }

    public  static mBitmap getResizedBitmap(Image image)
    {
        return new mBitmap(image);
    }
}
