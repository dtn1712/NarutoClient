using System;
using System.Text;
using UnityEngine;
using System.Collections;
using System.Threading;
public class Rms {

    public static string hotkeyKill = "hotkey";
    public static string gamepad = "gamepad";
    public static string typeRMS = "typeRMS";
    public static bool isStore = false;/// true thi ko save rms
    public static sbyte versionImg = -1;
    public static int status; // 0: nothing, 1: working, 2: waitingSave, 3: waitingLoad
    public static sbyte[] data;
    public static string filename;
    const int INTERVAL = 5;
    const int MAXTIME = 500;
    public static void saveRMS(string filename, sbyte[] data)
    {
        if (Thread.CurrentThread.Name == Main.mainThreadName)
            __saveRMS(filename, data);
        else
            _saveRMS(filename, data);
    }
    public static sbyte[] loadRMS(string filename)
    {
        if (Thread.CurrentThread.Name == Main.mainThreadName) {

						return __loadRMS (filename);
				} else {

						return _loadRMS (filename);
				}
    }

    public static string loadRMSString(string fileName)
    {
        sbyte[] data = Rms.loadRMS(fileName);
        if (data == null)
            return null;
        DataInputStream dis = new DataInputStream(data);
        try
        {
            string t = dis.readUTF();
            dis.close();
            return t;
        }
        catch (Exception e)
        {
            Cout.println(e.StackTrace);
        }
        return null;
    }
    public static byte[] convertSbyteToByte(sbyte[] var)
    {
        byte[] temp = new byte[var.Length];
        for (int i = 0; i < var.Length; i++)
        {
            if (var[i] > 0)
                temp[i] = (byte)var[i];

            else temp[i] = (byte)(var[i] + 256);
        }

        return temp;
    }
    public static void saveRMSString(string filename, string data)
    {

        DataOutputStream dos = new DataOutputStream();
        try
        {
            dos.writeUTF(data);
            Rms.saveRMS(filename, dos.toByteArray());
            dos.close();
        }
        catch (Exception e)
        {
            Cout.println(e.StackTrace);
        }
    }

    private static void _saveRMS(string filename, sbyte[] data)
    {
        if (Rms.status != 0)
        {
            Debug.LogError("Cannot save RMS " + filename + " because current is saving " + Rms.filename);
            return;
        }
        Rms.filename = filename;
        Rms.data = data;
        status = 2; // waiting save
        int i = 0;
        while (i < MAXTIME)
        {
            Thread.Sleep(INTERVAL);
            if (status == 0) break; //save done
            i++;
        }
        if (i == MAXTIME) Debug.LogError("TOO LONG TO SAVE RMS " + filename);       
    }
    private static sbyte[] _loadRMS(string filename)
    {
        if (Rms.status != 0)
        {
            Debug.LogError("Cannot load RMS " + filename + " because current is loading " + Rms.filename);
            return null;
        }
        Rms.filename = filename;
        Rms.data = null;
        status = 3; // waiting load
        int i = 0;
        while (i < MAXTIME)
        {
            Thread.Sleep(INTERVAL);
            if (status == 0) break; //load done
            i++;
        }
        if (i == MAXTIME) Debug.LogError("TOO LONG TO LOAD RMS " + filename);    
        return Rms.data;
    }

    // Call this in Main.update();
    public static void update()
    {
        if (Rms.status == 2)
        {
            Rms.status = 1;
            __saveRMS(Rms.filename, Rms.data);
            Rms.status = 0;
        }
        else if (Rms.status == 3)
        {
            Rms.status = 1;
            Rms.data = __loadRMS(Rms.filename);
            Rms.status = 0;
        }
    }

    public static int loadRMSInt(string file)
    {
        sbyte[] b = loadRMS(file);
        return b == null ? -1 : (int)b[0];
    }
    public static void saveRMSInt(string file, int x)
    {
        try
        {
            saveRMS(file, new sbyte[] { (sbyte)x });
        }
        catch (Exception e)
        {
        }
    }
    public static string GetiPhoneDocumentsPath(){
       if (Main.isPC)
            return Application.persistentDataPath;     
        string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);       
        path = path.Substring(0, path.LastIndexOf('/'));
        return path + "/Documents";
    }
    private static void __saveRMS(string filename, sbyte[] data){
       // Debug.Log("Thong tin save: " + GetiPhoneDocumentsPath() + "/" + filename + mGraphics.zoomLevel);
        System.IO.FileStream f = new System.IO.FileStream(GetiPhoneDocumentsPath() + "/" + filename, System.IO.FileMode.Create);     
        f.Write(ArrayCast.cast(data), 0, data.Length);
        f.Flush();
        f.Close();
    }
    private static sbyte[] __loadRMS(string filename)
    {
        try
        {
            //Debug.Log("Thong tin load: " + GetiPhoneDocumentsPath() + "/" + filename + mGraphics.zoomLevel);

            System.IO.FileStream f = new System.IO.FileStream(GetiPhoneDocumentsPath() + "/" + filename, System.IO.FileMode.Open);         
            byte[] data = new byte[f.Length];
            f.Read(data, 0, data.Length);
            f.Close();
            sbyte[] DATA = ArrayCast.cast(data);
            return ArrayCast.cast(data);
        }
        catch (Exception e) {       

            return null;
        }
    }
	public static void deleteAllRecord(){      
		foreach(System.IO.FileInfo file in new System.IO.DirectoryInfo(GetiPhoneDocumentsPath () + "/").GetFiles())
			file.Delete();

	}
    public static string ByteArrayToString(byte[] ba)
    {
        string hex = BitConverter.ToString(ba);
        return hex.Replace("-", "");
    }

    public static byte[] StringToByteArray(string hex)
    {
        int NumberChars = hex.Length;
        byte[] bytes = new byte[NumberChars / 2];
        for (int i = 0; i < NumberChars; i += 2)
            bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
        return bytes;
    }   
	public static void deleteRecordByName(string path){
		try{
			System.IO.File.Delete(GetiPhoneDocumentsPath() + "/" + path);
		}
		catch (Exception e) { }
	}  
	public static void deleteRecordCompareToName() {//clear RMS CompareTo Name
		try {
			System.IO.DirectoryInfo  di= new System.IO.DirectoryInfo(GetiPhoneDocumentsPath () + "/");
			System.IO.FileInfo[] fiArr = di.GetFiles();
			for(int i=0;i<fiArr.Length;i++){
				if(CRes.CheckDelRMS(fiArr[i].Name))
					fiArr[i].Delete();
			}		
			
		} catch (Exception e1) {}
	}


    public static void clearRMS()
    {
       
    }

    public static void saveRMSData(string filename, sbyte[] data)
    {

        if (Thread.CurrentThread.Name == Main.mainThreadName)
            __saveRMS(filename, data);
        else
            _saveRMS(filename, data);
    }
}

