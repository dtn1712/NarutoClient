﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class MyAudioClip
{
    public string name;
    public AudioClip clip;
    public long timeStart = 0;
    public MyAudioClip(string filename)
    {
        this.clip = (AudioClip) Resources.Load(filename);
        this.name = filename;
    }

    public void Play()
    {
        //Debug.Log("PLAY: "+name);
//        if(isPlaying())
//        {
//            Debug.LogWarning("Skip "+name);
//            return;
//        }
        Main.main.GetComponent<AudioSource>().PlayOneShot(clip);
        timeStart = mSystem.currentTimeMillis();
    }

    public bool isPlaying()
    {
		return false;
        //return mSystem.getCurrentTimeMillis() - timeStart < (long)(clip.Length*1000);
    }

}

