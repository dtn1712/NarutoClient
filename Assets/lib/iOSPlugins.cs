using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class iOSPlugins
{

    public static string devide;
    public static string Myname;
    //  public static string info="";
    /* Interface to native implementation */
    [DllImport("__Internal")]
    private static extern void _SMSsend(string tophone, string withtext, int n);

    [DllImport("__Internal")]
    private static extern int _unpause();

    [DllImport("__Internal")]
    private static extern int _checkRotation();

    [DllImport("__Internal")]
    private static extern int _back();

    [DllImport("__Internal")]
    private static extern int _Send();

    [DllImport("__Internal")]
    private static extern void _purchaseItem(string itemID, string userName, string gameID);

    /* Public interface for use inside C# / JS code */
    public static int Check()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            return checkCanSendSMS();
        }
        devide = iPhoneSettings.generation.ToString();
        //devide = "iphone4";
        string isPhone = "" + devide[2];
        if (isPhone == "h" && devide.Length > 6)
        {
            Myname = SystemInfo.operatingSystem.ToString();
            //Myname = "iPhone OS 5.0";
            string version = "" + Myname[10];
            if (version != "2" && version != "3")
            {
                //PlayerSettings.useOSAutorotation = true;
               
                return 0;
            }
            else
            {
                return 1;
            }
        }
        else
        {
            Cout.println(devide+"  loai");
            if (devide == "Unknown" && ScaleGUI.WIDTH * ScaleGUI.HEIGHT < 1024 * 768)
            {
                return 0;
            }
            return -1;
        }
    }
    public static int checkCanSendSMS()
    {
        if (iPhoneSettings.generation == iPhoneGeneration.iPhone3GS || iPhoneSettings.generation == iPhoneGeneration.iPhone4
           || iPhoneSettings.generation == iPhoneGeneration.iPhone4S || iPhoneSettings.generation == iPhoneGeneration.iPhone5)
        {
            return 0;
        }
        else
            return -1;
    }
    // Starts lookup for some bonjour registered service inside specified domain
    public static void SMSsend(string phonenumber, string bodytext, int n)
    {
        // Call plugin only when running on real device
        if (Application.platform != RuntimePlatform.OSXEditor)
            _SMSsend(phonenumber, bodytext, n);
    }
    public static void back()
    {
        if (Application.platform != RuntimePlatform.OSXEditor)
            _back();
    }
    public static void Send()
    {
        if (Application.platform != RuntimePlatform.OSXEditor)
        {
            _Send();
            //  Canvas.endDlg();
            // Canvas.startOKDlg(info);
        }
    }
    public static int unpause()
    {
        if (Application.platform != RuntimePlatform.OSXEditor)
            return _unpause();
        else
            return 0;
    }
    public static int checkRotation()
    {
        if (Application.platform != RuntimePlatform.OSXEditor)
            return _checkRotation();
        else
            return 0;
    }


    public static void purchaseItem(string itemID, string userName, string gameID)
    {
        if (Application.platform != RuntimePlatform.OSXEditor)
            _purchaseItem(itemID, userName, gameID);
    }
}
