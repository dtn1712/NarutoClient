

using System;
using System.IO;
public class mFont {

	public const int LEFT = 0;
	public const int RIGHT = 1;
	public const int CENTER = 2;
	public const int RED = 0;

	public const int YELLOW = 1;
	public const int GREEN = 2;
	public const int FATAL = 3;
	public const int MISS = 4;
	public const int ORANGE = 5;
	public const int ADDMONEY = 6;
	public const int MISS_ME = 7;
	public const int FATAL_ME = 8;
	public const int HP = 9;
	public const int MP = 10;
	public const int XP = 11;
	public const int WHITE = 12;
	public const int NAMEITEMXANH = 13;
	public const int NAMEITEMTRANG = 14;

	private int space;
	private int height;
	private Image imgFont;
	private string strFont;
    private int[][] fImages;

	// ===============================================
	public static string str = " 0123456789+-*='_?.,<>/[]{}!@#$%^&*():aáàảãạâấầẩẫậăắằẳẵặbcdđeéèẻẽẹêếềểễệfghiíìỉĩịjklmnoóòỏõọôốồổỗộơớờởỡợpqrstuúùủũụưứừửữựvxyýỳỷỹỵzwAÁÀẢÃẠĂẰẮẲẴẶÂẤẦẨẪẬBCDĐEÉÈẺẼẸÊẾỀỂỄỆFGHIÍÌỈĨỊJKLMNOÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢPQRSTUÚÙỦŨỤƯỨỪỬỮỰVXYÝỲỶỸỴZW";
	public static mFont tahoma_7b_redsmall;
	public static mFont tahoma_7b_whitesmall; 
	public static mFont tahoma_7b_whitesmall_size6; 
	public static mFont tahoma_7b_red; // 1
	public static mFont tahoma_7b_bluesmall;
	public static mFont tahoma_7b_blue ; // 2
	public static mFont tahoma_7b_white;// 3
	public static mFont tahoma_7b_yellow ; // 4
	public static mFont tahoma_7b_yellowMainTab ; // 4
	public static mFont tahoma_7b_yellowsmall ; // 4
	public static mFont tahoma_7b_dark ; // 5
	public static mFont tahoma_7b_dark_small ; // 5
	public static mFont tahoma_7b_popup ; // 5
	public static mFont tahoma_7b_yellowsmall_size6 ; // 4
    public static mFont tahoma_7b_violetsmall;
    public static mFont tahoma_7b_black_small; // 5

	public static mFont tahoma_7b_green2 ;// 6
	public static mFont tahoma_7b_green ; // 7
	public static mFont tahoma_7_button;
	public static mFont tahoma_7b_greensmall;
	public static mFont tahoma_7b_focus ;//8
	public static mFont tahoma_7b_unfocus; //9
	public static mFont tahoma_7; //10
	public static mFont tahoma_7_blue1 ; //11
	public static mFont tahoma_7_green2 ;//12
	public static mFont tahoma_7_yellow;//13
	public static mFont tahoma_7_yellow_xu;//13
	public static mFont tahoma_7_grey;//14
	public static mFont tahoma_7_red ;//15
	public static mFont tahoma_7_blue ;//16
	public static mFont tahoma_7_green ;//17
	public static mFont tahoma_7_white ;//18
	public static mFont tahoma_7_white_size6 ;//18
    public static mFont tahoma_7_white_size7;//18
    public static mFont tahoma_7_yellow_tahoma;
	public static mFont tahoma_8b;//19
    public static mFont tahoma_8btf, tahoma_8btf_unfocus;
	public static mFont number_yellow_7b_size6;
	public static mFont number_red_7b_size6;
	public static mFont number_gray;//23
	public static mFont number_orange ;//24

    public static mFont number_yellow, number_yellow_xp;
    public static mFont number_red, number_red_hp;
    public static mFont number_green, number_green_mp;
	
	public static mFont bigNumber_red;//25
	public static mFont bigNumber_While; // 26
	public static mFont bigNumber_yellow ;//27	
	public static mFont bigNumber_green ;//28
	public static mFont bigNumber_orange;//29
	public static mFont bigNumber_blue ; //30
	
	public static mFont nameFontRed=tahoma_7b_red;
	public static mFont nameFontYellow=tahoma_7_yellow;
	public static mFont nameFontGreen=tahoma_7_green;
	public static mFont  tahoma_7_redsmall;
	public static mFont tahoma_7_whitesmall;// 3
	public static mFont tahoma_7_bluesmall;
	public static mFont tahoma_7_yellowsmall;
    public static mFont tahoma_7_greensmall;
    public static mFont tahoma_7_xamsmall;
    public static mFont tahoma_7_xam;
    public static mFont tahoma_7_orange;
    public static mFont tahoma_7b_cyansmall;
    public static mFont tahoma_6_white;
    public static mFont tahoma_10b;
    public string namefont = "";
	    
	public static void loadBegin(){
//		  tahoma_7b_red = new mFont(str, "/font/tahoma_7b_red", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",11,0xffff0000); // 1
//		  tahoma_7b_whitesmall = new mFont(str, "/font/tahoma_7b_white", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xffffffff); // 1
//		  tahoma_7b_whitesmall_size6 = new mFont(str, "/font/tahoma_7b_white", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",6,0xffffffff); // 1
//		   tahoma_7b_blue = new mFont(,11,0xff637dff); // 2
//		  tahoma_7b_white = new mFont(str, "/font/tahoma_7b_white", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",11,0xffffffff); // 3
//		  tahoma_7b_yellow = new mFont(,10,0xffffaa00); // 4
//		  tahoma_7b_yellowsmall = new mFont(,8,0xffffe21e); // 4
//		  tahoma_7b_dark = new mFont(,10,0xff532905); // 5
//		  tahoma_7b_dark_small = new mFont(,8,0xff532905); // 5
//		  tahoma_7b_bluesmall= new mFont(,8,0xff637dff); // 2
//		  tahoma_7b_redsmall = new mFont(str, "/font/tahoma_7b_red", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xffff0000); // 1
//		  tahoma_7b_yellowMainTab = new mFont(,9,0xffffaa00); // 4
//		  tahoma_7b_popup = new mFont(str, "/font/tahoma_7b_brown", "/font/tahoma_7b", 0,"font/UVNTHANGVU_2.ttf",11,0xff000000); // 5
//		  tahoma_7b_yellowsmall_size6 = new mFont(,6,0xffffe21e); // 4
//		  
//		  
//		  tahoma_7b_green2 = new mFont(str, "/font/tahoma_7b_green2", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",11,0xff005325); // 6
//		  tahoma_7b_green = new mFont(str, "/font/tahoma_7b_green", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",9,0xff00cc00); // 7
//		  tahoma_7b_focus = new mFont(str, "/font/tahoma_7b_focus", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xff70b474); //8
//		  tahoma_7b_greensmall = new mFont(str, "/font/tahoma_7b_green", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xff00cc00); // 7
//		  tahoma_7_button = new mFont(str, "/font/tahoma_7", "/font/tahoma_7", 0,"font/tahoma_7.ttf",9,0xff000000); //10
//			  
//		  tahoma_7b_unfocus = new mFont(str, "/font/tahoma_7b_unfocus", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xffffeacc); //9
//		  tahoma_7 = new mFont(str, "/font/tahoma_7", "/font/tahoma_7", 0,"font/tahoma_7.ttf",8,0xff000000); //10
//		  tahoma_7_blue1 = new mFont(str, "/font/tahoma_7_blue1", "/font/tahoma_7", 0,"font/tahoma_7.ttf",8,0xff00ffff); //11
//		  tahoma_7_green2 = new mFont(str, "/font/tahoma_7_green2", "/font/tahoma_7", 0,"font/tahoma_7.ttf",8,0xff005325);//12
//		  tahoma_7_yellow = new mFont(str, "/font/tahoma_7_yellow", "/font/tahoma_7", 0,"font/tahoma_7.ttf",8,0xffffff00);//13
//		  tahoma_7_yellow_xu = new mFont(str, "/font/tahoma_7_yellow", "/font/tahoma_7", 0,"font/tahoma_7.ttf",8,0xffffff00);//13
//		  tahoma_7_grey = new mFont(str, "/font/tahoma_7_grey", "/font/tahoma_7", 0,"font/tahoma_7.ttf",8,0xff555555);//14
//		  tahoma_7_red = new mFont(str, "/font/tahoma_7_red", "/font/tahoma_7", 0,"font/tahoma_7.ttf",8,0xffff7777);//15
//		  tahoma_7_blue = new mFont(str, "/font/tahoma_7_blue", "/font/tahoma_7", 0,"font/tahoma_7.ttf",8,0xff0080ff);//16
//		  tahoma_7_green = new mFont(str, "/font/tahoma_7_green", "/font/tahoma_7", 0,"font/tahoma_7.ttf",8,0xff20eb5f);//17 20eb5f
//		  tahoma_7_white = new mFont(str, "/font/tahoma_7_white", "/font/tahoma_7", 0,"font/tahoma_7.ttf",8,0xffefebef);//18
//		  tahoma_7_white_size6 = new mFont(str, "/font/tahoma_7_white", "/font/tahoma_7", 0,"font/tahoma_7.ttf",6,0xffead69a);//18
//		  tahoma_7_white_size7 = new mFont(str, "/font/tahoma_7_white", "/font/tahoma_7", 0,"font/tahoma_7.ttf",7,0xffefebef);//18
//		   tahoma_8b = new mFont(str, "/font/tahoma_8b", "/font/tahoma_8b", -1,"font/tahoma_8b.ttf",9,0xffeecc66);//19
//		  tahoma_8btf = new mFont(str, "/font/tahoma_8b", "/font/tahoma_8b", -1,"font/tahoma_8b.ttf",8,0xffeecc66);//19
//		  
//		  tahoma_7_bluesmall = new mFont(str, "/font/tahoma_7_blue", "/font/tahoma_7", 0,"font/tahoma_7.ttf",7,0xff0080ff);//16
//		  tahoma_7_yellowsmall= new mFont(str, "/font/tahoma_7_yellow", "/font/tahoma_7", 0,"font/tahoma_7.ttf",7,0xffffff00);//16
//		  
//		  tahoma_7_whitesmall = new mFont(str, "/font/tahoma_7_white", "/font/tahoma_7", 0,"font/tahoma_7.ttf",7,0xffefebef);//18
//		  tahoma_7_redsmall = new mFont(str, "/font/tahoma_7_red", "/font/tahoma_7", 0,"font/tahoma_7.ttf",7,0xffff7777);//15
//		  number_yellow_7b_size6 = new mFont(" 0123456789+-", "/font/number_yellow", "/font/number", 0,"font/UVNTHANGVU_2.ttf",16,0xffffdf2f);//20
//			 
//		 
//		  number_yellow = new mFont(" 0123456789+-", "/font/number_yellow", "/font/number", 0,"font/tahoma_7.ttf",8,0xffffdf2f);//20
//		  number_red = new mFont(" 0123456789+-", "/font/number_red", "/font/number", 0,"font/tahoma_7.ttf",8,0xffe44a55);//21
//		  number_red_7b_size6 = new mFont(" 0123456789+-", "/font/number_red", "/font/number", 0,"font/UVNTHANGVU_2.ttf",12,0xffff0012);//21
//		  number_green = new mFont(" 0123456789+-", "/font/number_green", "/font/number", 0,"font/tahoma_7.ttf",8,0xff3ef0e3);//22
//		  number_gray = new mFont(" 0123456789+-", "/font/number_gray", "/font/number", 0,"font/tahoma_7.ttf",8,0xff474747);//23
//		  number_orange = new mFont(" 0123456789+-", "/font/number_orange", "/font/number", 0,"font/tahoma_7.ttf",8,0xfff59c38);//24
//
//		
//		  bigNumber_red = new mFont(" 0123456789+-", "/font/number_red", "/font/number", 0,"font/tahoma_8b.ttf",12,0xffff0000);//25
//		  bigNumber_While = new mFont(str, "/font/tahoma_7b_white", "/font/tahoma_7b", 0,"font/tahoma_8b.ttf",12,0xffffffff); // 26
//		  bigNumber_yellow = new mFont(" 0123456789+-", "/font/number_yellow", "/font/number", 0,"font/tahoma_8b.ttf",12,0xffffaa00);//27	
//		  bigNumber_green = new mFont(" 0123456789+-", "/font/number_green", "/font/number", 0,"font/tahoma_8b.ttf",12,0xff00cc00);//28
//		  bigNumber_orange = new mFont(" 0123456789+-", "/font/number_orange", "/font/number", 0,"font/tahoma_8b.ttf",12,0xfff59c38);//29
//		  bigNumber_blue = new mFont(str, "/font/tahoma_7_blue1", "/font/tahoma_7", 0,"font/tahoma_8b.ttf",12,0xff637dff); //30

		////---------------------------font moi
		  tahoma_7b_red               = new mFont("tahoma_7b",1,11,0xff0000); // 1
		  tahoma_7b_whitesmall        = new mFont("tahoma_7b",2,8,0xffffff); // 1
		  tahoma_7b_whitesmall_size6  = new mFont("tahoma_7b",3,6,0xffffff); // 1
		   tahoma_7b_blue             = new mFont("tahoma_7b",3,11,0x637dff); // 2
		  tahoma_7b_white             = new mFont("tahoma_7b",4,11,0xffffff); // 3
          tahoma_7b_yellow =            new mFont("tahoma_7b", 5, 9, 0xf6e634); // 4
		  tahoma_7b_yellowsmall       = new mFont("tahoma_7b",6,8,0xffe21e); // 4
		  tahoma_7b_dark              = new mFont("tahoma_7b",7,9,0x532905); // 5
		  tahoma_7b_dark_small        = new mFont("tahoma_7b",8,8,0x532905); // 5
          tahoma_7b_bluesmall = new mFont("tahoma_7b", 9, 8, 0x637dff); // 2
		  tahoma_7b_redsmall          = new mFont("tahoma_7b",10,8,0xff0000); // 1
		  tahoma_7b_yellowMainTab     = new mFont("tahoma_7b",11,9,0xffaa00); // 4
          tahoma_7b_popup = new mFont("UVNAiCap_R", 12, 9, 0x102B2F); // 5
		  tahoma_7b_yellowsmall_size6 = new mFont("tahoma_7b",13,6,0xffe21e); // 4
		  
		  
		  tahoma_7b_green2            = new mFont("tahoma_7b",14,9,0x005325); // 6
		  tahoma_7b_green             = new mFont("tahoma_7b",15,9,0x00cc00); // 7
		  tahoma_7b_focus             = new mFont("tahoma_7b",16,8,0x70b474); //8
		  tahoma_7b_greensmall        = new mFont("tahoma_7b",17,8,0x00cc00); // 7
		  tahoma_7_button             = new mFont("tahoma_7b",18,9,0x000000); //10
			  
		  tahoma_7b_unfocus           = new mFont("tahoma_7b",19,8,0xffeacc); //9
          tahoma_7 = new mFont("tahoma_7b", 20, 8, 0x000000); //10
          tahoma_7_blue1 = new mFont("tahoma_7b", 21, 8, 0x00ffff); //11
          tahoma_7_green2 = new mFont("tahoma_7b", 22, 8, 0x005325);//12
          tahoma_7_yellow = new mFont("tahoma_7b", 23, 8, 0xffff00);//13
          tahoma_7_yellow_xu = new mFont("tahoma_7b", 24, 8, 0xffff00);//13
          tahoma_7_grey = new mFont("tahoma_7b", 25, 8, 0x555555);//14
          tahoma_7_red = new mFont("tahoma_7b", 26, 8, 0xff7777);//15
          tahoma_7_blue = new mFont("tahoma_7b", 27, 8, 0x0080ff);//16
          tahoma_7_green = new mFont("tahoma_7b", 28, 8, 0x20eb5f);//17 20eb5f
          tahoma_7_white = new mFont("tahoma_7b", 29, 8, 0xefebef);//18
          tahoma_7_white_size6 = new mFont("tahoma_7b", 30, 6, 0xead69a);//18
          tahoma_7_white_size7 = new mFont("tahoma_7b", 31, 7, 0xefebef);//18
		   tahoma_8b                  = new mFont("tahoma_7b",32,9,0xeecc66);//19
		  tahoma_8btf                 = new mFont("tahoma_7b",33,8,0xeecc66);//19
		  
		  tahoma_7_bluesmall          = new mFont("tahoma_7b",34,7,0x0080ff);//16
          tahoma_7_yellowsmall = new mFont("tahoma_7b", 35, 7, 0xffff00);//16

          tahoma_7_whitesmall = new mFont("tahoma_7b", 36, 7, 0xefebef);//18
          tahoma_7_redsmall = new mFont("tahoma_7b", 37, 7, 0xff0000);//15
          number_yellow_7b_size6 = new mFont("UTMAZUKI", 38, 12, 0xffdf2f);//20
			 
		 
		  number_yellow               = new mFont("tahoma_7b",39,12,0xffdf2f);//20
          number_red = new mFont("tahoma_7b", 40, 12, 0xff0000);//21
          number_green = new mFont("tahoma_7b", 42, 12, 0x3ef0e3);//22
          number_yellow_xp = new mFont("tahoma_7b", 39, 12, 0xffdf2f);//20
          number_red_hp = new mFont("tahoma_7b", 40, 12, 0xff0000);//21
          number_green_mp = new mFont("tahoma_7b", 42, 12, 0x3ef0e3);//22

          number_red_7b_size6 = new mFont("UTMAZUKI", 41, 12, 0xff0012);//21
		  number_gray                 = new mFont("tahoma_7b",43,12,0x474747);//23
		  number_orange               = new mFont("tahoma_7b",44,12,0xf59c38);//24
          tahoma_8btf_unfocus         = new mFont("tahoma_7b",45, 8, 0xffffff);//19

          tahoma_7b_violetsmall = new mFont("tahoma_7b", 46, 8, 0xd105e9); // 4
          tahoma_7b_black_small = new mFont("tahoma_7b", 47, 8, 0x000000); // 5
          tahoma_7_greensmall = new mFont("tahoma_7b", 48, 7, 0x20eb5f);//17 20eb5f

          tahoma_7_yellow_tahoma = new mFont("tahoma_7b", 49, 6, 0xffff00);//18
          tahoma_7_xamsmall = new mFont("tahoma_7b", 50, 7, 0x7c7b77);//17 20eb5f

          tahoma_7_xam = new mFont("tahoma_7b", 51, 8, 0x7c7b77);//18
          tahoma_7_orange = new mFont("tahoma_7b", 52, 8, 0xfa9168);//18
          tahoma_7b_cyansmall = new mFont("tahoma_7b", 55, 8, 0x00ffff); // 7
          tahoma_6_white = new mFont("tahoma_7b", 53, 6, 0xefebef);//18
          tahoma_10b = new mFont("tahoma_7b", 54, 10, 0xffff00);//18
		
		 // bigNumber_red               = new mFont("tahoma_7b",45,12,0xffff0000);//25
		  //bigNumber_While             = new mFont("tahoma_7b",46,12,0xffffffff); // 26
		  //bigNumber_yellow            = new mFont("tahoma_7b",47,12,0xffffaa00);//27	
		  //bigNumber_green             = new mFont("tahoma_7b",48,12,0xff00cc00);//28
		 // bigNumber_orange            = new mFont("tahoma_7b",49,12,0xfff59c38);//29
		 // bigNumber_blue              = new mFont("tahoma_7b",50,12,0xff637dff); //30
		
		  nameFontRed=tahoma_7b_red;
		  nameFontYellow=tahoma_7_yellow;
		  nameFontGreen=tahoma_7_green;
	}
	string pathImage;
	public FontSys fontSys;
	public string name;

	public mFont( sbyte ID, int color) {
        fontSys = new FontSys(ID, color);
	}
	public mFont( string namefont, sbyte ID,int size, int color) {

        //size = (size == 8 ? 7 : size);
        //		size = (size==9?7:size);
        size = (size == 10 ? 11 : size);
        //fontSys = new FontSys(namefont + "_size" + size, ID, color, size);
        name = ID + "_" + namefont + "_" + size;
        fontSys = new FontSys(namefont , ID, color, size);
	}
    public void SetColor(int color) {
        fontSys.cl1 = color;
    }
	public mFont(string strFont, string pathImage, string pathData, int space, string pathSysFontFile, int SysFontSize, int SysFontColor) {
		
	}

	public void reloadImage() {
		this.imgFont = GameCanvas.loadImage(pathImage+".png").image;
	}

	public void freeImage() {
		this.imgFont = null;
	}

	public int getHeight() {
        return fontSys.getHeight();
	}

	public int getWidth(string st) {
        if (st == null) return 1;
        st = st.Replace(" ",".");
        if (fontSys == null) {
            return 5;
        }
        return fontSys.getWidth(st);
	}
	
	public void drawString(mGraphics g, string st, int x, int y, int align, mFont font) {
		if(font!=null)
		{
			font.drawString(g, st, x+1, y, align);
			font.drawString(g, st, x, y+1, align);
		}
		drawString(g, st, x, y, align);
	}
	
	public void drawString(mGraphics g, string st, int x, int y, int align) {
        if (st == null) return;
        fontSys.drawString(g, st, x, y, align);
	}
	public void drawString(mGraphics g, string st, int x, int y, int align,bool isUclip) {
        fontSys.drawString(g, st, x, y, align);
	}


	public mVector splitFontVector(string src, int lineWidth) {
		mVector lines = new mVector();
		string line = "";
		for (int i = 0; i < src.Length; i++) {
			if (src[i] == '\n'|| src[i]=='\b') {
				lines.addElement(line);
				line = "";
			} else {
				line += src[i];
				if (getWidth(line) > lineWidth) {
					// System.out.println(line);
					int j = 0;
					for (j = line.Length - 1; j >= 0; j--) {
						if (line[j] == ' ') {
							break;
						}
					}
					if (j < 0)
						j = line.Length - 1;
					lines.addElement(mSystem.substring(line,0, j));
					i = i - (line.Length - j) + 1;
					line = "";
				}
				if (i == src.Length - 1 && !line.Trim().Equals(""))
					lines.addElement(line);
			}
		}
		return lines;
	}

	public string splitFirst(string str) {
		string line = "";
		bool isSplit = false;
		for (int i = 0; i < str.Length; i++) {
			if (!isSplit) {
				string strEnd = mSystem.substring(str,i, str.Length);
				if (compare(strEnd, " "))
					line += str[i] + "-";
				else
					line += strEnd;
				isSplit = true;
			} else {
				if (str[i] == ' ')
					isSplit = false;
			}
		}
		return line;
	}

	public string[] splitFontArray(string src, int lineWidth) {
		mVector lines = splitFontVector(src, lineWidth);
        string[] arr = new string[lines.size()];
		for (int i = 0; i < lines.size(); i++) {
			arr[i] = lines.elementAt(i).ToString();
		}
		return arr;
	}

	public bool compare(string strSource, string str) {
		for (int i = 0; i < strSource.Length; i++) {
			if ((strSource[i]).Equals(str))
				return true;
		}
		return false;
	}
	public void drawStringBorder(mGraphics g, string st, int x, int y, int align) {
        if (fontSys != null)
		{
			float[] hsv = new float[3];
//			Color.colorToHSV(fontSys.color, hsv);
			hsv[2] *= 0.4f;
			int colorDark = 0x8f9292;//Color.(hsv);
			int colorr = fontSys.cl1;
            fontSys.cl1 = colorDark;
            fontSys.color1 = fontSys.setColor(colorDark);
			drawString(g, st, x-1, y-1, align);


            fontSys.cl1 = colorr;
            fontSys.color1 = fontSys.setColor(colorr);

			drawString(g, st, x, y, align);
		}
		else drawString( g,  st,  x,  y,  align);
		
	}
    public void drawStringBorderDoDay(mGraphics g, string st, int x, int y, int align, int color, int doday, bool isUClip)
    {

        if (fontSys != null)
        {
            float[] hsv = new float[3];
            //			Color.colorToHSV(fontSys.color, hsv);
            hsv[2] *= 0.4f;
            int colorDark = color;//Color.(hsv);
            int colorr = fontSys.cl1;
            fontSys.cl1 = colorDark;
            fontSys.color1 = fontSys.setColor(colorDark);
            //drawString(g, st, x - 1, y - 1, align);


            if (doday >= 0)
            {
                for (int i = 0; i < (TCanvas.TileZoom + doday) * 2 + 1; i++)
                {
                    for (int j = 0; j < (TCanvas.TileZoom + doday) * 2 + 1; j++)
                    {
                        drawString(g, st, x + i - (TCanvas.TileZoom + doday), y + j - (TCanvas.TileZoom + doday), align);
                    }
                }
            }
            else
            {
                drawString(g, st, x - 1, y - 1, align);
                drawString(g, st, x - 1, y + 1, align);
                drawString(g, st, x + 1, y - 1, align);
                drawString(g, st, x + 1, y + 1, align);

                drawString(g, st, x, y - 1, align);
                drawString(g, st, x, y + 1, align);
                drawString(g, st, x - 1, y, align);
                drawString(g, st, x + 1, y, align);
            }


            fontSys.cl1 = colorr;
            fontSys.color1 = fontSys.setColor(colorr);

            drawString(g, st, x, y, align);
        }
        else drawString(g, st, x, y, align);
    }
	public void drawStringBorder(mGraphics g, string st, int x, int y, int align,int cll) {
		if(fontSys!=null)
		{
			float[] hsv = new float[3];
			//Color.colorToHSV(fontSys.color, hsv);
			hsv[2] *= 0.4f;
			int colorDark = cll;//Color.(hsv);
            int colorr = fontSys.cl1;
            fontSys.cl1 = colorDark;
            fontSys.color1 = fontSys.setColor(colorDark);
			drawString(g, st, x+1, y+1, align);
			drawString(g, st, x+1, y, align);

            fontSys.cl1 = colorr;
            fontSys.color1 = fontSys.setColor(colorr);
			drawString(g, st, x, y, align);
		}
		else drawString( g,  st,  x,  y,  align);
		
	}
	public void drawStringBorder(mGraphics g, string st, int x, int y, int align, mFont font2) {
		drawStringBorder( g,  st,  x,  y,  align);		
	}
    public void drawStringBorder(mGraphics g, string st, int x, int y, int align, int color, bool isUclip)
    {
        drawStringBorder(g,   st,  x,  y,  align, color);
    }
	public void drawStringBorder(mGraphics g, string st, int x, int y, int align, mFont font2,bool isUclip) {
		drawStringBorder( g,  st,  x,  y,  align);		
	}
	public static string[] split(string original, string separator) {
		mVector nodes = new mVector();
		int index = original.IndexOf(separator);
		while (index >= 0) {
			nodes.addElement(mSystem.substring(original,0, index));
			original = mSystem.substring(original,index + separator.Length);
			index = original.IndexOf(separator);
		}
		nodes.addElement(original);
		string[] result = new string[nodes.size()];
		if (nodes.size() > 0) {
			for (int loop = 0; loop < nodes.size(); loop++) {
				result[loop] = (string) nodes.elementAt(loop);

			}
		}
		return result;
	}
    public void setFontSize(int size)
    {
    }
    public void ResetFontSize()
    {
    }
}
