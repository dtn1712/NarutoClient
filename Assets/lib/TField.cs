
using System.Collections;
using System;
using System.Threading;

public class TField : IActionListener{

    public static GameCanvas c;
    public static GameMidlet m;
    public bool isPaintImg;
    public bool isOKReturn;
    public static bool isUpdateTF = false;
    public bool isCloseKey = true;
	public bool isFocus;
    public int height;
	public int x, y, width, range;
	public bool lockArrow = false,justReturnFromTextBox;
    public bool paintFocus = true, isSmallFont;
    public const sbyte KEY_LEFT = 14;
    public const sbyte KEY_RIGHT = 15;
    public const sbyte KEY_CLEAR = 19;
	public static int typeXpeed = 2;
	private static readonly int[] MAX_TIME_TO_CONFIRM_KEY = { 30, 14, 11, 9, 6, 4, 2 };
	private static int CARET_HEIGHT = 0;
	private static readonly int CARET_WIDTH = 1;
	private static readonly int CARET_SHOWING_TIME = 5;
    private static int TEXT_GAP_X = 4 * TCanvas.TileZoom;
	private static readonly int MAX_SHOW_CARET_COUNER = 10;
	public static readonly int INPUT_TYPE_ANY = 0;
	public static readonly int INPUT_TYPE_NUMERIC = 1;
	public static readonly int INPUT_TYPE_PASSWORD = 2;
	public static readonly int INPUT_ALPHA_NUMBER_ONLY = 3;
	private static string[] print = { " 0", ".,@?!_1\"/$-():*+<=>;%&~#%^&*{}[];\'/1", "abc2áàảãạâấầẩẫậăắằẳẵặ2", "def3đéèẻẽẹêếềểễệ3", "ghi4íìỉĩị4", "jkl5", "mno6óòỏõọôốồổỗộơớờởỡợ6", "pqrs7", "tuv8úùủũụưứừửữự8", "wxyz9ýỳỷỹỵ9", "*", "#" };
	private static string[] printA = { "0", "1", "abc2", "def3", "ghi4", "jkl5", "mno6", "pqrs7", "tuv8", "wxyz9", "0", "0" };
	private static string[] printBB = { " 0", "er1", "ty2", "ui3", "df4","gh5", "jk6", "cv7", "bn8", "m9", "0", "0", "qw!", "as?", "zx","op.", "l," };
	private string text = "";
	private string passwordText = "";
	public string paintedText = "";
	public bool isChat;
	public int caretPos = 0;
	private int counter = 0;
	private int maxTextLenght = 500;
	private int offsetX = 0;
	private static int lastKey = -1984;
	private int keyInActiveState = 0;
	private int indexOfActiveChar = 0;
	private int showCaretCounter = MAX_SHOW_CARET_COUNER;
	private int inputType = INPUT_TYPE_ANY;
	public static bool isQwerty=true;
	public static int typingModeAreaWidth;
	public static int mode = 0;
    public static long timeChangeMode;
	public static readonly string []modeNotify = { "abc", "Abc", "ABC", "123" };
	public static readonly int NOKIA = 0;
	public static readonly int MOTO = 1;
	public static readonly int ORTHER = 2, BB = 3;	
	public static int changeModeKey = 11;
	public static readonly sbyte abc = 0, Abc = 1, ABC = 2, number123 = 3;
    public static TField currentTField;
	public bool isTfield,isPaintMouse=true;
	public string name = "";
    public string title = "", strnull;
	public string strInfo;
    public static int heightKeyBoard;
    int xCamText = 0;
    public static mBitmap imgClear;
    public mBitmap img;
    public static void loadBegin()
    {
        TEXT_GAP_X = 4 * TCanvas.TileZoom;
        //imgTf = GameCanvas.loadImage("/hd/tf.png");
    }
    public  void commandPointer(int index, int subIndex)
    {
        switch (index)
        {
            case 0:
                if (isFocus)
                    clear();
                break;
        }
    }
    public void setMaxLeng(int a){
    }
    public static bool setNormal(char ch)
    {
        if (ch == '@' || ch == '.' || ch == '_' || ch == '-') return true;
		if ((ch < '0' || ch > '9') && (ch < 'A' || ch > 'Z') && (ch < 'a' || ch > 'z')) {
			return false;
		}
		return true;
	}
	public static void setVendorTypeMode(int mode) {
		if (mode == MOTO) {
			print[0] = "0";
			print[10] = " *";
			print[11] = "#";
			changeModeKey = '#';
		} else if (mode == NOKIA) {
			print[0] = " 0";
			print[10] = "*";
			print[11] = "#";
			changeModeKey = '#';
		} else if (mode == ORTHER) {
			print[0] = "0";
			print[10] = "*";
			print[11] = " #";
			changeModeKey = '*';
		}
	}
	//public iCommand cmdChat2;
    public Command cmdClear,cmdDoneAction;
	public bool isNotUseChangeTextBox;
	public void init() {

        CARET_HEIGHT = 14 * TCanvas.TileZoom;
        cmdClear = new Command("Xóa", this, 1000, null);
        height = 19;
	}
    public bool isFocusedz()
    {
        return isFocus;
    }
    int timeDelayKyCode;
    public void clearKeyWhenPutText(int keyCode)
    {
        if (keyCode != -8) return;

        if (timeDelayKyCode > 0)
            return;
        if (timeDelayKyCode <= 0)
        {
            timeDelayKyCode = 0;
        }     
      
            clear();
      
    }

    public Command setCmdClear()
    {
        return cmdClear;
    }
    //public TField(mScreen parentScr) {
    //    text = "";
    //    this.parentScr = parentScr;
    //    init();
    //}
  
    public void setheightText()
    {
        height = 19;
        if (GameCanvas.isTouch)
            height = 19;
    }
    public TField()
    {
        text = "";
        init();
        setFocus(false);
       // setheightText();
    }

    public TField(int x, int y)
    {
        text = "";
        this.x = x;
        this.y = y;
        init();
        setFocus(false);
        setheightText();

    }

    public TField(int x, int y, int width)
    {
        text = "";
        this.x = x;
        this.y = y;
        this.width = width;
        init();
        setFocus(false);
        setheightText();

    }
  
    public void clearAllText()
    {
        text = "";
        if (kb != null)
            kb.text = "";
        caretPos = 0;
        setOffset(0);
        setPasswordTest();

    }
    public void setStringNull(string str) {
		strnull = str;
	}
	public void clear() {
		if (caretPos > 0 && text.Length > 0) {
		    text = text.Substring(0, caretPos - 1);

			caretPos--;
			setOffset(0);
			setPasswordTest();
            if (kb != null)
                kb.text = text;          
		}
      
	}
	public void clearAll(){
		if (caretPos > 0 && text.Length > 0) {
            text = text.Substring(0, text.Length - 1);
			caretPos--;
			setOffset();
			setPasswordTest();
			setFocusWithKb(true);
            if(kb!=null)
			kb.text="";
			
		}
	
	}
	public void setOffset() {
        if (paintedText == null || mFont.tahoma_7 == null)
            return;
        if (inputType == INPUT_TYPE_PASSWORD)
            paintedText = passwordText;
        else
            paintedText = text;
        mFont f = mFont.tahoma_7;
        if (isSmallFont) f = mFont.tahoma_7_white_size6;
        if (offsetX < 0 && f.getWidth(paintedText) + offsetX < width - TEXT_GAP_X - 13 - typingModeAreaWidth)
            offsetX = width - 10 - typingModeAreaWidth - f.getWidth(paintedText);
        if (offsetX + f.getWidth(paintedText.Substring(0, caretPos)) <= 0)
        {
            offsetX = -f.getWidth(paintedText.Substring(0, caretPos));
            offsetX = offsetX + 40;
        }
        else if (offsetX + f.getWidth(paintedText.Substring(0, caretPos)) >= width - 12 - typingModeAreaWidth)
            offsetX = width - 10 - typingModeAreaWidth - f.getWidth(paintedText.Substring(0, caretPos)) - 2 * TEXT_GAP_X;
        if (offsetX > 0)
            offsetX = 0;
       
	}

	private void keyPressedAny(int keyCode) {
		string[] print;
		if (inputType == INPUT_TYPE_PASSWORD || inputType == INPUT_ALPHA_NUMBER_ONLY)
			print = printA;
		else
			print = TField.print;
		if (keyCode == lastKey) {
			indexOfActiveChar = (indexOfActiveChar + 1) % print[keyCode - '0'].Length;

			char c = print[keyCode - '0'][indexOfActiveChar];

			if (mode == 0)
                c = char.ToLower(c);
			else if (mode == 1) {
                c = char.ToUpper(c);
			} else if (mode == 2) {
                c = char.ToUpper(c);
			} else {
				c = print[keyCode - '0'][(print[keyCode - '0'].Length - 1)];
			}
			string ttext = text.Substring(0, caretPos - 1) + c;
			if (caretPos < text.Length)
				ttext = ttext + text.Substring(caretPos, text.Length);
			text = ttext;
			keyInActiveState = MAX_TIME_TO_CONFIRM_KEY[typeXpeed];
			setPasswordTest();
		} else {
			if (text.Length < maxTextLenght) {
				if (mode == 1 && lastKey != -1984)
					mode = 0;
				indexOfActiveChar = 0;
				char c = print[keyCode - '0'][indexOfActiveChar];
				if (mode == 0)
					c = char.ToLower(c);
				else if (mode == 1) {
					c = char.ToUpper(c);
				} else if (mode == 2) {
					c = char.ToUpper(c);
				} else {
					c = print[keyCode - '0'][print[keyCode - '0'].Length - 1];
				}
				string ttext = text.Substring(0, caretPos) + c;
				if (caretPos < text.Length)
					ttext = ttext + text.Substring(caretPos, text.Length);
				text = ttext;
				keyInActiveState = MAX_TIME_TO_CONFIRM_KEY[typeXpeed];
				caretPos++;
				setPasswordTest();
				setOffset();
			}
		}
		lastKey = keyCode;
	}

    private void keyPressedAscii(int keyCode)
    {

        if (!isFocus&&Main.isPC) return;
        if (inputType == INPUT_TYPE_PASSWORD
                || inputType == INPUT_ALPHA_NUMBER_ONLY)
        {
            //if ((keyCode < '0' || keyCode > '9')
            //        && (keyCode < 'A' || keyCode > 'Z')
            //        && (keyCode < 'a' || keyCode > 'z'))
            //{
            //    return;
            //}
        }

        if (text.Length < maxTextLenght)
        {
            string ttext = text.Substring(0, caretPos) + (char)keyCode;
            if (caretPos < text.Length)
                ttext = ttext + text.Substring(caretPos, text.Length - caretPos);
            text = ttext;
            caretPos++;
            setPasswordTest();        
            setOffset(0);
        }
        if (kb != null)
            kb.text = text;
		
	}
//	public string Substring(int xbg,int xend){
	
//}
	int holdCount;
    //private bool openVirtual;

	
	public static void setMode() {
		mode++;
		if (mode > 3)
			mode = 0;
		lastKey = changeModeKey;
		timeChangeMode =  (Environment.TickCount/ 1000);
		
	}
    public static int  changeDau;
    private int indexDau = -1, indexTemplate = 0, indexCong = 0;
    private long timeDau = 0;
    private static string printDau = "aáàảãạâấầẩẫậăắằẳẵặeéèẻẽẹêếềểễệiíìỉĩịoóòỏõọôốồổỗộơớờởỡợuúùủũụưứừửữựyýỳỷỹỵ";
    private void setDau()
    {
        timeDau = Environment.TickCount / 100;
        if (indexDau == -1)
        {
            for (int i = caretPos; i > 0; i--)
            {
                char str = text[(i - 1)];
                for (int j = 0; j < printDau.Length; j++)
                {
                    char strDau = printDau[(j)];
                    if (str == strDau)
                    {
                        indexTemplate = j;
                        indexCong = 0;
                        indexDau = (i - 1);
                        return;
                    }
                }
            }
            indexDau = -1;
        }
        else
        {
            indexCong++;
            if (indexCong >= 6)
                indexCong = 0;
            string fir = text.Substring(0, indexDau);
            string last = text.Substring(indexDau + 1);
            string them = printDau.Substring(indexTemplate + indexCong, 1);
            text = fir + them + last;
        }
    }
    public static bool isOpenTextBox = false;
    public  bool keyPressed(int keyCode){

        if (Main.isPC)
        {
            if (keyCode == -8)
            {
                clearKeyWhenPutText(-8);
                return true;
            }
            if (keyCode == -5)
            {
                isOKReturn = true;
                justReturnFromTextBox = true;
                isUpdateTF = false;
                isFocus = false;
                return true;
            }
            
        }
        if (keyCode == 8 || keyCode == -8 || keyCode == 204)
        {
            clear();
           
            return true;
        }

        if (!GameCanvas.isTouch)
        {
            
        }
        if (isQwerty)
        {  if (keyCode >= 32)
            {
                keyPressedAscii(keyCode);
              
                return false; // swallow
            }
        }

        if (keyCode == changeDau && inputType == INPUT_TYPE_ANY)
        {
            setDau();
            return false;
        }
        if (keyCode == '*')
            keyCode = '9' + 1;
        if (keyCode == '#')
            keyCode = '9' + 2;
       
        if (keyCode >= '0' && keyCode <= '9' + 2)
        {
            if (inputType == INPUT_TYPE_ANY || inputType == INPUT_TYPE_PASSWORD || inputType == INPUT_ALPHA_NUMBER_ONLY)
            {
                keyPressedAny(keyCode);
              
            }
            else if (inputType == INPUT_TYPE_NUMERIC)
            {
                keyPressedAscii(keyCode);
                keyInActiveState = 1;
            }
        }
        else
        {
            indexOfActiveChar = 0;
            lastKey = -1984;
            if (keyCode == KEY_LEFT && !lockArrow)
            {
                if (caretPos > 0)
                {
                    caretPos--;
                    setOffset(0);
                    showCaretCounter = MAX_SHOW_CARET_COUNER;
                    return false;
                }
            }
            else if (keyCode == KEY_RIGHT && !lockArrow)
            {
                if (caretPos < text.Length)
                {
                    caretPos++;
                    setOffset(0);
                    showCaretCounter = MAX_SHOW_CARET_COUNER;
                    return false;
                }
            }
            else if (keyCode == KEY_CLEAR)
            {
                clear();
                return false;
            }
            else
            {
                lastKey = keyCode;
            }
        }

        return true; // Not swallow
		
    }
  
    public void setOffset(int index)
    {
        if (inputType == INPUT_TYPE_PASSWORD)
            paintedText = passwordText;
        else
            paintedText = text;
        int bb = mFont.tahoma_8b.getWidth(paintedText.Substring(0, caretPos));
        if (index == -1)
        {
            if ((bb + offsetX) < 15 && caretPos > 0 && caretPos < paintedText.Length)
                offsetX += (mFont.tahoma_8b.getWidth(paintedText.Substring(caretPos, 1)));
        }
        else if (index == 1)
        {
            if ((bb + offsetX) > width - 25 && caretPos < paintedText.Length && caretPos > 0)
                offsetX -= (mFont.tahoma_8b.getWidth(paintedText.Substring(caretPos - 1, 1)));
        }
        else
        {
            offsetX = -(bb - (width - 12));
           
        }
        if (offsetX > 0)
            offsetX = 0;
        else if (offsetX < 0)
        {
            int aa = mFont.tahoma_8b.getWidth(paintedText) - (width - 12);
            if (offsetX < -aa)
                offsetX = -aa;
           
        }
        
    }  	
    int timeFocus = 0;
    public  void paint(mGraphics g){
        g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
        y += 2;
        bool isFocus = isFocusedz();
        if (inputType == INPUT_TYPE_PASSWORD)
            paintedText = passwordText;
        else
            paintedText = text;
        if (img != null)
            paintInputTfOther(g, isFocus, x, y - 2 * TCanvas.TileZoom + mGraphics.getImageHeight(img) / 4, width, height,2+ TEXT_GAP_X + offsetX + x - 1 * TCanvas.TileZoom, y + (height - mFont.tahoma_8btf.getHeight()) / 2 - 2 * TCanvas.TileZoom, paintedText, name);
        else
        {
            paintInputTf(g, isFocus, x, y - 2 * TCanvas.TileZoom, width, height, 2+TEXT_GAP_X /*+ offsetX*/ + x - 1 * TCanvas.TileZoom, y + (height - mFont.tahoma_8btf.getHeight()) / 2 - 2 * TCanvas.TileZoom, paintedText, name);
        }
        g.setClip(x + 3 * TCanvas.TileZoom, y + 1 * TCanvas.TileZoom, width - 4 * TCanvas.TileZoom, height - 2 * TCanvas.TileZoom);
        if (isFocusedz())
        {
            if (keyInActiveState == 0 && (showCaretCounter > 0 || (counter / CARET_SHOWING_TIME) % 3 == 0))
            {
                g.setColor(0xffffff);
                if (img == null)
                {
                    if(!isSmallFont)
                     g.fillRect(TEXT_GAP_X - 8  + offsetX + x + mFont.tahoma_8btf.getWidth(mSystem.substring(paintedText,0, caretPos)) - CARET_WIDTH, y + (height - CARET_HEIGHT) / 2 , CARET_WIDTH, CARET_HEIGHT);
                    else g.fillRect(TEXT_GAP_X - 8  + offsetX + x + mFont.tahoma_7_white_size6.getWidth(mSystem.substring(paintedText, 0, caretPos)) - CARET_WIDTH,
                       y + height / 2 - CARET_HEIGHT / 2 + (isSmallFont == true ? 2  : 0),
                            CARET_WIDTH, CARET_HEIGHT - (isSmallFont == true ? 4  : 0));
                   
                }
                else
                {
                    int wtext = mFont.tahoma_8btf.getWidth(mSystem.substring(paintedText,0, caretPos)) / 2;
                    g.fillRect(TEXT_GAP_X + 1  + offsetX + x + mGraphics.getImageWidth(img) / 2 + (wtext > width / 2 ? width / 2 : wtext) - CARET_WIDTH,
                        y + (height - CARET_HEIGHT) / 2 - 5 ,
                        CARET_WIDTH, CARET_HEIGHT);
                }

            }
        }
        GameCanvas.resetTrans(g);

        y -= 2;
        //if (text != null && text.Length > 0 && GameCanvas.isTouch && isFocus && img == null)
        //{
        //    g.drawImage(imgClear, x + width - 13 * TCanvas.TileZoom, y + height / 2, mGraphics.VCENTER | mGraphics.HCENTER);
        //}
	 }
    public void paint(mGraphics g,bool isUclip)
    {
        g.setClip(0, 0, GameCanvas.w, GameCanvas.h);

        height = 15 * TCanvas.TileZoom;
        bool isFocus = isFocusedz();
        if (inputType == INPUT_TYPE_PASSWORD)
            paintedText = passwordText;
        else
            paintedText = text;
        if (img != null)
            paintInputTfOther(g, isFocus, x, y - 2 * TCanvas.TileZoom + mGraphics.getImageHeight(img) / 4, width, height, TEXT_GAP_X + offsetX + x - 1 * TCanvas.TileZoom, y + (height - mFont.tahoma_8btf.getHeight()) / 2 - 2 * TCanvas.TileZoom, paintedText, name);
        else
        {
            paintInputTf(g, isFocus, x, y - 2 * TCanvas.TileZoom, width, height, TEXT_GAP_X /*+ offsetX*/ + x - 1 * TCanvas.TileZoom, y + (height - mFont.tahoma_8btf.getHeight()) / 2 - 2 * TCanvas.TileZoom, paintedText, name);
        }
        g.setClip(x + 3 * TCanvas.TileZoom, y + 1 * TCanvas.TileZoom, width - 4 * TCanvas.TileZoom, height - 2 * TCanvas.TileZoom);
        if (isFocusedz())
        {
            if (keyInActiveState == 0 && (showCaretCounter > 0 || (counter / CARET_SHOWING_TIME) % 3 == 0))
            {
                g.setColor(0xffffff);
                if (img == null)
                {
                    if (!isSmallFont)
                        g.fillRect(TEXT_GAP_X + 1 * TCanvas.TileZoom + offsetX + x + mFont.tahoma_8btf.getWidth(mSystem.substring(paintedText, 0, caretPos)) - CARET_WIDTH, y + (height - CARET_HEIGHT) / 2, CARET_WIDTH, CARET_HEIGHT);
                    else g.fillRect(TEXT_GAP_X + 1 * TCanvas.TileZoom + offsetX + x + mFont.tahoma_7_white_size6.getWidth(mSystem.substring(paintedText, 0, caretPos)) - CARET_WIDTH,
                       y + height / 2 - CARET_HEIGHT / 2 + (isSmallFont == true ? 2 * TCanvas.TileZoom : 0),
                            CARET_WIDTH, CARET_HEIGHT - (isSmallFont == true ? 4 * TCanvas.TileZoom : 0));

                }
                else
                {
                    int wtext = mFont.tahoma_8btf.getWidth(mSystem.substring(paintedText, 0, caretPos)) / 2;
                    g.fillRect(TEXT_GAP_X + 1 * TCanvas.TileZoom + offsetX + x + mGraphics.getImageWidth(img) / 2 + (wtext > width / 2 ? width / 2 : wtext) - CARET_WIDTH,
                        y + (height - CARET_HEIGHT) / 2 - 5 * TCanvas.TileZoom,
                        CARET_WIDTH, CARET_HEIGHT);
                }

            }
        }
        GameCanvas.resetTrans(g);
        //if (text != null && text.Length > 0 && GameCanvas.isTouch && isFocus && img == null)
        //{
        //    g.drawImage(imgClear, x + width - 13 * TCanvas.TileZoom, y + height / 2, mGraphics.VCENTER | mGraphics.HCENTER);
        //}
    }
    //private bool isFocused() {
    //    return isFocus;
    //}
    public void paintInputTfOther(mGraphics g, bool isz, int x, int y, int w, int h, int xText, int yText, string text,
			string info) {
		// g.setColor(0x777777);
		g.setColor(0);
        if(!isPaintImg)
		g.drawImage(img, x+mGraphics.getImageWidth(img)/2, y, mGraphics.VCENTER|mGraphics.HCENTER);
		// g.drawRect(x + 1, y + 1, w - 2, h - 2);
		g.setClip(x + 3*TCanvas.TileZoom, y + 1*TCanvas.TileZoom -mScreen.HSTRING/2, w - 4*TCanvas.TileZoom, h);

		if (text != null && !text.Equals(""))
			mFont.tahoma_8btf.drawString(g, text, xText+mGraphics.getImageWidth(img)/2, yText, 2);
		else if (info != null) {
			if (isz)
				mFont.tahoma_7b_focus.drawString(g, info, xText+3*TCanvas.TileZoom+mGraphics.getImageWidth(img)/2, yText, 2);
			else
				mFont.tahoma_7b_unfocus.drawString(g, info, xText+3*TCanvas.TileZoom+mGraphics.getImageWidth(img)/2, yText, 2);
		}
	}
	public void paintInputTf(mGraphics g, bool isz, int x, int y, int w, int h, int xText, int yText, string text,
			string info) {
		// g.setColor(0x777777);
		g.setColor(0);
		if (isz&&!isPaintImg) {
			g.drawRegion(loadImageInterface.imgTf2, 0, 0 /** (Image.getHeight(loadImageInterface.imgTf2))*/, Image.getWidth(loadImageInterface.imgTf2), (Image.getHeight(loadImageInterface.imgTf2)), 0, x, y, mGraphics.TOP|mGraphics.LEFT);
			for (int i = 0; i < (w - 2 * Image.getWidth(loadImageInterface.imgTf3)) / Image.getWidth(loadImageInterface.imgTf3); i++)
				g.drawRegion(loadImageInterface.imgTf3, 0, 0/*4 * (Image.getHeight(imgTf)/3)*/, Image.getWidth(loadImageInterface.imgTf3), (Image.getHeight(loadImageInterface.imgTf3)), 0, x + Image.getWidth(loadImageInterface.imgTf3) + i * Image.getWidth(loadImageInterface.imgTf3), y,  mGraphics.TOP|mGraphics.LEFT,true);
			
			g.drawRegion(loadImageInterface.imgTf3, 0, 0/* * (Image.getHeight(imgTf)/3)*/,
					 (w - 2 *Image.getWidth(loadImageInterface.imgTf3)) % Image.getWidth(loadImageInterface.imgTf3),
					 (Image.getHeight(loadImageInterface.imgTf3)), 0,
					 x + w - Image.getWidth(loadImageInterface.imgTf3)-(	 (w - 2 *Image.getWidth(loadImageInterface.imgTf3)) % Image.getWidth(loadImageInterface.imgTf3)),
					 y,  mGraphics.TOP|mGraphics.LEFT,true);
			
			g.drawRegion(loadImageInterface.imgTf2, 0, 0 /** (Image.getHeight(loadImageInterface.imgTf2))*/,
					Image.getWidth(loadImageInterface.imgTf2), (Image.getHeight(loadImageInterface.imgTf2)),
					2, x + w - Image.getWidth(loadImageInterface.imgTf2), y,  mGraphics.TOP|mGraphics.LEFT,true);
			
		} else if(!isPaintImg) {
			g.drawRegion(loadImageInterface.imgTf0, 0, 0 /** (Image.getHeight(imgTf)/3)*/, Image.getWidth(loadImageInterface.imgTf0), (Image.getHeight(loadImageInterface.imgTf0)), 0, x, y,  mGraphics.TOP|mGraphics.LEFT);
			for (int i = 0; i < (w - 2 *Image.getWidth(loadImageInterface.imgTf0)) / Image.getWidth(loadImageInterface.imgTf0); i++)
				g.drawRegion(loadImageInterface.imgTf1, 0,  0, Image.getWidth(loadImageInterface.imgTf0), (Image.getHeight(loadImageInterface.imgTf0)), 0, x + Image.getWidth(loadImageInterface.imgTf0) + i * Image.getWidth(loadImageInterface.imgTf0), y,  mGraphics.TOP|mGraphics.LEFT);
			g.drawRegion(loadImageInterface.imgTf0, 0, 0/*2 * (Image.getHeight(imgTf)/3)*/,
					 Image.getWidth(loadImageInterface.imgTf0), (Image.getHeight(loadImageInterface.imgTf0)),
                    2, x + w - Image.getWidth(loadImageInterface.imgTf0), y, mGraphics.TOP | mGraphics.LEFT);
			g.drawRegion(loadImageInterface.imgTf1, 0, 0/*1 * (Image.getHeight(imgTf)/3)*/, 
					(w - 2 *Image.getWidth(loadImageInterface.imgTf0)) % Image.getWidth(loadImageInterface.imgTf0),
					(Image.getHeight(loadImageInterface.imgTf1)), 0,
                    x + w - Image.getWidth(loadImageInterface.imgTf0)
					-((w - 2 *Image.getWidth(loadImageInterface.imgTf0)) % Image.getWidth(loadImageInterface.imgTf0)),
					y,  mGraphics.TOP|mGraphics.LEFT,true);
		
		}
		// g.drawRect(x + 1, y + 1, w - 2, h - 2);
		g.setClip(x + 3*TCanvas.TileZoom, y + 1*TCanvas.TileZoom, w - 4*TCanvas.TileZoom, h);

		if (text != null && !text.Equals("")){
			//mFont.tahoma_8btf.drawString(g, text, xText, yText+2*TCanvas.TileZoom, 0);
            if (!isSmallFont)
            {
                if (!isFocus)
                    mFont.tahoma_8btf.drawString(g, text, xText, yText - 1 * TCanvas.TileZoom, 0, true);
                else
                    mFont.tahoma_8btf_unfocus.drawString(g, text, xText, yText - 1 * TCanvas.TileZoom, 0, true);
            }
            else {
                if (!isFocus)
                    mFont.tahoma_7_white_size6.drawString(g, text, xText, yText + 1 * TCanvas.TileZoom, 0, true);
                else
                    mFont.tahoma_7_white_size6.drawString(g, text, xText, yText + 1 * TCanvas.TileZoom, 0, true);
            }
		}else if (info != null) {
            if (!isSmallFont)
            {
                if (isz)
                    mFont.tahoma_7b_focus.drawString(g, info, xText + 1 * TCanvas.TileZoom, yText + 2 * TCanvas.TileZoom, 0);
                else
                    mFont.tahoma_7b_unfocus.drawString(g, info, xText + 1 * TCanvas.TileZoom, yText + 2 * TCanvas.TileZoom, 0);
            }
            else
            {
                if (isz)
                    mFont.tahoma_7_white_size6.drawString(g, info, xText + 1 * TCanvas.TileZoom, yText + 2 * TCanvas.TileZoom, 0);
                else
                    mFont.tahoma_7_white_size6.drawString(g, info, xText + 1 * TCanvas.TileZoom, yText + 2 * TCanvas.TileZoom, 0);
            }
		}
	}
	public string subString(string str,int index,int indexTo){	
		if(index>=0&&indexTo>str.Length-1)
			return str.Substring(index);
			if(index<0||index>str.Length-1||indexTo<0||indexTo>str.Length-1)
			return "";		
		string temp="";
        for (int i = index; i < indexTo; i++)
            temp+= str[i];
		return temp;
	}
	private void setPasswordTest() {		
		if (inputType == INPUT_TYPE_PASSWORD) {
			passwordText = "";
			for (int i = 0; i < text.Length; i++)
				passwordText = passwordText + "*";
			if (keyInActiveState > 0 && caretPos > 0)
				passwordText = passwordText.Substring(0, caretPos - 1) + text[caretPos - 1] + passwordText.Substring(caretPos, passwordText.Length);
		}
	}
	int cout;
    public int timePutKeyClearAll,timeClearFirt;   
   public  void update(){
       height = 15 * TCanvas.TileZoom;
       if(isFocus)
       isUpdateTF = isFocus;
		//sua cho pc-------------------------------

//		if(cout>0){
//			if(cout==1){
//				if(isChat){
//					setText("");
//					ChatTextField.isShow = false;
//				}else{
//					//if (isChangeFocus)
//					isFocus = false; setFocus(false);
//				}
//				GameCanvas.clearAll();
//				Cout.LogWarning("trung tf--------------------");
//			}
//			cout--;
//		}
//		if(cout<=0)
//			cout=0;
		//----------------------------sua cho pc
       if (Main.isPC)
       {
           if (timeDelayKyCode > 0)
               timeDelayKyCode--;
           if (timeDelayKyCode <= 0) timeDelayKyCode = 0;
       }
       if (kb != null && currentTField == this)
       {
           if (kb.text.Length < 40)
           {
               setText(kb.text);
           }
            if (kb.done)//tam dong------------------------------------------------------------
           {
				//GameCanvas.clearAll();
				//cout=10;
               //   if (cmdDoneAction != null) cmdDoneAction.performAction();
           }
       }
       
       counter++;
       if (keyInActiveState > 0)
       {
           keyInActiveState--;
           if (keyInActiveState == 0)
           {
               indexOfActiveChar = 0;
               if (mode == 1 && lastKey != changeModeKey && isFocus)
                   mode = 0;
               lastKey = -1984;
               setPasswordTest();
           }
       }
       if (showCaretCounter > 0)
           showCaretCounter--;
          if (GameCanvas.isPointerJustRelease&&GameCanvas.isPointer(x,y,width,height))//nho coi laiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
       {
           setTextBox();

       }

       if (indexDau != -1)
       {
           if (Environment.TickCount / 100 - timeDau > 5)
           {
               indexDau = -1;
           }
       }	    

       if(isNotUseChangeTextBox&&!Main.isPC)
		{
			isOpenTextBox=false;
			isFocus=false;
		}
			//if (isFocus)
       //{
       //    string str;
       //    if (inputType == INPUT_TYPE_PASSWORD)
       //        str = passwordText;
       //    else
       //        str = text;
       //    // xCamText = mFont.tahoma_7_black.getWidth(str) - width + 12;
       //    xCamText = -width / 2 + caretPos * 5 + 4;
       //    int max = mFont.tahoma_7_black.getWidth(str) - width + 8;
       //    if (xCamText > max)
       //        xCamText = max;
       //    if (xCamText < 0)
       //        xCamText = 0;

       //}
       //else
       //    xCamText = 0;
       //counter++;
       //if (keyInActiveState > 0)
       //{
       //    keyInActiveState--;
       //    if (keyInActiveState == 0 || mode > 2)
       //    {
       //        indexOfActiveChar = 0;
       //        if (isFocus && mode == 1 && lastKey != changeModeKey)
       //            mode = 0;
       //        lastKey = -1984;
       //        setPasswordTest();
       //    }
       //}
       //if (showCaretCounter > 0)
       //    showCaretCounter--;

       //if (indexDau != -1)
       //{
       //    if (mSystem.currentTimeMillis() / 100 - timeDau > 5)
       //    {
       //        indexDau = -1;
       //    }
       //}
       //if (isFocus)
       //    if (GameCanvas.keyMyPressed[4])
       //    {
       //        // if (inputType != INPUT_TYPE_PASSWORD) {
       //        caretPos--;
       //        if (caretPos < 0)
       //            caretPos = 0;
       //        setOffset(-1);
       //        // }
       //        GameCanvas.keyMyPressed[4] = false;
       //    }
       //    else if (GameCanvas.keyMyPressed[6])
       //    {
       //        // if (inputType != INPUT_TYPE_PASSWORD) {
       //        caretPos++;
       //        if (caretPos > getText().Length)
       //            caretPos = getText().Length;
       //        setOffset(1);
       //        // }
       //        GameCanvas.keyMyPressed[6] = false;
       //    }





    }   
	public bool resetFc;
    public void setTextBox()   
	{
        //tam dong 
        //if(GameCanvas.isPointerHoldIn(x+width-20, y, 40, height)){          
        //    clearAllText();
        //    isFocus = true;
        //}else if (GameCanvas.isPointerHoldIn(x, y, width-20, height)){				
        //    setFocusWithKb(true);
        //}
        //else
        //    setFocus(false);

        //
        if (GameCanvas.isPointerClick /*&& GameCanvas.isPoint(0, 0, GameCanvas.w, GameCanvas.h - getHeight())*/)
        {
            if (isFocus && GameCanvas.isPointer(x + width - 26 * mGraphics.zoomLevel, y, 20 * TCanvas.TileZoom, 20 * mGraphics.zoomLevel))
            {
                clearAllText();
                isFocus = false;
                isUpdateTF = false;
            }
            else if (GameCanvas.isPointer(x, y, width, height))
            {
                setFocusWithKb(true);
                doChangeToTextBox();
                GameCanvas.isPointerClick = false;
                //GameCanvas.clearAll();
            }
            else
            {
                isFocus = false;
                isUpdateTF = false;
            }
        }
        isFocus = true; ;
    }
    public void setFocus(bool isFocus)
    {
        //isFocus = true;
        if (this.isFocus != isFocus)
            mode = 0;
        lastKey = -1984;
        timeChangeMode = (int)(DateTime.Now.Ticks / 1000);
        this.isFocus = isFocus;
        if (isFocus)
        {
            currentTField = this;
            if (kb != null) kb.text = currentTField.text;
           
        }

    }
    public bool showSubTextField = true;
    public static TouchScreenKeyboard kb;
    public void setFocusWithKb(bool isFocus){       
		 if (this.isFocus != isFocus)
            mode = 0;
        lastKey = -1984;
        timeChangeMode = (int)(DateTime.Now.Ticks / 1000);
        this.isFocus = isFocus;
        if (isFocus)
        {
            currentTField = this;
        }
        else
        {
            if(currentTField==this) currentTField = null;
        }
        if (Thread.CurrentThread.Name == Main.mainThreadName && currentTField != null )
        {
            isFocus=true;
            TouchScreenKeyboard.hideInput = !currentTField.showSubTextField;
            TouchScreenKeyboardType type;
            type = TouchScreenKeyboardType.ASCIICapable;
            if (inputType == INPUT_TYPE_NUMERIC) type = TouchScreenKeyboardType.NumberPad;
            bool isPass = false;
            if (inputType == INPUT_TYPE_PASSWORD)
                isPass = true;
			
           kb = TouchScreenKeyboard.Open(currentTField.text, type, false, false, isPass, false, currentTField.name);
            if(kb!=null)
           kb.text = currentTField.text;           
		   Cout.LogWarning("SHOW KEYBOARD FOR " + currentTField.text);
        }
    }
	public string getText() {
		return text;
	}
	public void clearKb(){
		if(kb!=null)
			kb.text="";
	}
	public void setText(string text) {
		if (text == null)
			return;
		lastKey = -1984;
		keyInActiveState = 0;
		indexOfActiveChar = 0;
		this.text = text;
		this.paintedText = text;		
		//if(text=="")
//			TouchScreenKeyboard.Clear();
		setPasswordTest();
		caretPos = text.Length;
		setOffset();
	
	}
    public void insertText(string text)
    {
       
		this.text = this.text.Substring(0, caretPos) + text + this.text.Substring(caretPos);
		setPasswordTest();
		caretPos += text.Length;
		setOffset();
	}

	public int getMaxTextLenght() {
		return maxTextLenght;
	}

	public void setMaxTextLenght(int maxTextLenght) {
		this.maxTextLenght = maxTextLenght;
	}

	public int getIputType() {
		return inputType;
	}

	public void setIputType(int iputType) {
		this.inputType = iputType;
	}
	// BB: hold 16 button: a - z
    public static int[][] BBKEY = {new int [] { 32, 48 },new int [] { 49, 69 },new int [] { 50, 84 },
			new int []{ 51, 85 },new int [] { 52, 68 },new int [] { 53, 71 },new int [] { 54, 74 },new int [] { 55, 67 },
			new int []{ 56, 66 }, new int []{ 57, 77 },new int [] { 42, 128 },new int [] { 35, 137 },new int [] { 33, 113 },
			new int []{ 63, 97 },new int [] { 64, 121, 122 }, new int []{ 46, 111 }, new int []{ 44, 108 } 
			};
    public static int xDu;
    public bool isChangeFocus;
	
	
    public static int getHeight()
    {
        if (GameCanvas.isTouch)
            return 28;
        return 20;
    }
    public void updatePoiter()
    {
        if (GameCanvas.isPointerClick /*&& !GameCanvas.menu.showMenu*/)
        {
            setTextBox();
        }
    }
    public void setPoiter()
    {
        isOpenTextBox = true;
        doChangeToTextBox();
    }
	//----

    public void doChangeToTextBox() {
        //GameCanvas.clearAllPointerEvent();
        //if (ChatTextField.isShow)
        //{
        //    ChatTextField.gI().openKeyIphone();
        //}
        //else if (GameCanvas.subDialog != null && GameCanvas.subDialog == GameCanvas.msgchat)
        //{
        //    GameCanvas.msgchat.openKeyIphone();
        //}
	
	}
    public void perform(int idAction, Object p)
    {
        switch (idAction)
        {
            case 1000:
                clear();
                break;

            default:
                break;
        }

    }
}
