
using System.Collections;
using System.Threading;
using System;

public class mVector
{
    private ArrayList a;
    public mVector()
    {
        a = new ArrayList();
    }

    public mVector(ArrayList a)
    {
        this.a = a;
    }

    public void addElement(Object o)
    {
        a.Add(o);
    }
    public void add(Object o)
    {
        a.Add(o);
    }
    public bool contains(Object o)
    {
        if (a.Contains(o))
            return true;
        return false;

    }
    public int size()
    {
        if (a == null)
            return 0;
        return a.Count;
    }

    public Object elementAt(int index)
    {
        if (index > -1 && index < a.Count)
            return a[index];
        else
            return null;

    }
    //	    public void set(int index, Object obj)
    //	    {
    //	        if (index > -1 && index < a.size())
    //	            a.setElementAt(obj, index);
    //	    }
    public void setElementAt(Object obj, int index)
    {
        if (index > -1 && index < a.Count)
            a[index] = obj;
    }
    public int indexOf(Object o)
    {
        return a.IndexOf(o);
    }

    public void removeElementAt(int index)
    {
        if (index > -1 && index < a.Count)
            a.RemoveAt(index);
    }

    public void removeElement(Object o)
    {
        a.Remove(o);
    }
    public void removeAllElements()
    {
        a.Clear();
    }

    public void insertElementAt(Object o, int i)
    {
        a.Insert(i, o);
    }
    public Object firstElement()
    {
        return a[0];
    }

    public Object lastElement()
    {
        return a[a.Count - 1];
    }
}
